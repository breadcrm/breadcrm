VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Log"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'Const vdbConn = "DSN=bakery;UID=bakeryuser;PWD=bakery2003"

Public Function SetEnvironment(ByVal env As String) As Variant
    ReadINI env
End Function

Public Function LogAction(ByVal strUserID As String, _
                        ByVal sActionType As String, _
                        ByVal sActionSummery As String, _
                        ByVal sCustomer As String, _
                        Optional ByVal strIP As String = "" _
                        )
'
Dim vSno As Variant
Dim vSql As String
Dim vResponse As New Collection

On Error GoTo LogActionErrHandler

Dim oDB As ADODB.Connection

Set oDB = New ADODB.Connection

Dim Customer_Col_Val As String

If sCustomer = "" Then
    Customer_Col_Val = "NULL"
Else
    Customer_Col_Val = sCustomer
End If

Let vSql = "INSERT INTO [LogsAdmin] ([UserID],[ActionType],[ActionSummery],[CNo],[IP]) " & _
           "VALUES(" & strUserID & ",'" & sActionType & "','" & Replace(sActionSummery, "'", "''") & "'," & Customer_Col_Val & ",'" & strIP & "')"
              
oDB.Open vdbConn
oDB.Execute vSql
oDB.Close

Set oDB = Nothing

vResponse.Add False, Key:="error"


Set LogAction = vResponse

Exit Function
LogActionErrHandler:
oDB.Close
Set oDB = Nothing
LogAction = "ERROR: " & Err.Description
End Function

Public Function GetLogRecords( _
                                ByVal strCName As String, _
                                ByVal strUNo As String, _
                                ByVal strUName As String, _
                                ByVal strActionType As String, _
                                ByVal dtDateFrom As Date, _
                                ByVal dtDateTo As Date, _
                                ByVal lStartRowIndex As Long, _
                                ByVal lMaximumRows As Long, _
                                ByVal strActionFrom As String _
                             )
'
Dim vSno As Variant
Dim vSql As String
Dim vResponse As New Collection
Dim arLogs As Variant

On Error GoTo GetLogRecordsErrHandler

Dim oDB As ADODB.Connection

Set oDB = New ADODB.Connection

Dim oCmd As ADODB.Command
Dim oRs As ADODB.Recordset

Set oCmd = New ADODB.Command
Set oRs = New ADODB.Recordset

Dim objParam As ADODB.Parameter
With oCmd
    .ActiveConnection = vdbConn
    .CommandType = 4 'adCmdStoredProc
    .CommandText = "CRM_GetLogs"
    
    If Len(Trim(strCName)) = 0 Then
        Set objParam = .CreateParameter("CName", adVarChar, adParamInput, 50, Null)
        objParam.Attributes = adParamNullable
    Else
        Set objParam = .CreateParameter("CName", adVarChar, adParamInput, 50, Trim(strCName))
    End If
    .Parameters.Append objParam
    
    
    If Len(Trim(strUNo)) = 0 Then
        Set objParam = .CreateParameter("@UNo", adInteger, adParamInput, , Null)
        objParam.Attributes = adParamNullable
    Else
        Set objParam = .CreateParameter("@UNo", adInteger, adParamInput, , Trim(strUNo))
        objParam.Value = CInt(strUNo)
    End If
    .Parameters.Append objParam
    
    
    If Len(Trim(strUName)) = 0 Then
        Set objParam = .CreateParameter("@UName", adVarChar, adParamInput, 50, Null)
        objParam.Attributes = adParamNullable
    Else
        Set objParam = .CreateParameter("@UName", adVarChar, adParamInput, 50, Trim(strUName))
    End If
    .Parameters.Append objParam
    
    
    If Len(Trim(strActionType)) = 0 Then
        Set objParam = .CreateParameter("@ActionType", adVarChar, adParamInput, 25, Null)
        objParam.Attributes = adParamNullable
    Else
        Set objParam = .CreateParameter("@ActionType", adVarChar, adParamInput, 25, Trim(strActionType))
    End If
    .Parameters.Append objParam
    
    Set objParam = .CreateParameter("@DateFrom", adDBTimeStamp, adParamInput, , dtDateFrom)
    .Parameters.Append objParam
    
    Set objParam = .CreateParameter("@DateTo", adDBTimeStamp, adParamInput, , dtDateTo)
    .Parameters.Append objParam
    
    Set objParam = .CreateParameter("@StartRowIndex", adInteger, adParamInput, , lStartRowIndex)
    .Parameters.Append objParam
    
    Set objParam = .CreateParameter("@MaximumRows", adInteger, adParamInput, , lMaximumRows)
    .Parameters.Append objParam
    
     If Len(Trim(strActionFrom)) = 0 Then
        Set objParam = .CreateParameter("@ActionFrom", adVarChar, adParamInput, 25, Null)
        objParam.Attributes = adParamNullable
    Else
        Set objParam = .CreateParameter("@ActionFrom", adVarChar, adParamInput, 25, Trim(strActionFrom))
    End If
    .Parameters.Append objParam
    
End With

oDB.Open vdbConn
oDB.CommandTimeout = 6000
oRs.Open oCmd, , adOpenForwardOnly, adLockReadOnly, -1

If Not (oRs.BOF And oRs.EOF) Then
  arLogs = oRs.GetRows()
Else
  arLogs = "Fail"
End If

oRs.Close
oDB.Close

Set oCmd = Nothing
Set oRs = Nothing
Set oDB = Nothing

vResponse.Add arLogs, "LogRecords"
Set GetLogRecords = vResponse

Exit Function

GetLogRecordsErrHandler:
oDB.Close
Set oDB = Nothing
GetLogRecords = "ERROR: " & Err.Description
End Function
