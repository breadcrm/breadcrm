Attribute VB_Name = "Module"
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

Public vdbConn As String

Public Function ReadINI(ByVal env As String) As String
On Error GoTo ReadINIErrHandler

Dim a As String
Dim s As String * 256 's can contain 255 characters
Dim RetVal As String
    
a = GetPrivateProfileString("SERVER", env, "ERROR", s, Len(s), App.Path & "\dbcon.ini")

'get the value
RetVal = Left$(s, a)
vdbConn = RetVal

'return the value to the caller
ReadINI = RetVal

Exit Function

ReadINIErrHandler:
    ReadINI = "Error"
End Function
