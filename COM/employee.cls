VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "employee"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private objNewmail As CDONTS.NewMail
Private oDB As ADODB.Connection
Private oRs As ADODB.Recordset
Private oRsCheck As ADODB.Recordset
Private sFrom As Variant
Private sTo As Variant
Private sSubject As Variant
Private sMessage As Variant

Private vSql As Variant
Private vRecordCount As Integer
Private vPagecount As Integer
'Const vdbConn = "DSN=bakery;UID=bakeryuser;PWD=bakery2003"

Public Function SetEnvironment(ByVal env As String) As Variant
    ReadINI env
End Function

Public Function SaveEmployee(ByVal vUtype As String, ByVal vJobType As String, ByVal vFNo As Variant, ByVal vDesig As String, ByVal vFName As String, ByVal vSname As String, ByVal vNickName As String, ByVal vAddressI As String, ByVal vAddressII As String, ByVal vTown As String, ByVal vPostCode As String, ByVal vTelNo As String, ByVal vMob As String, ByVal vFax As String, ByVal vEmail As String, ByVal vOfficeExt As String, ByVal vNINo As String, ByVal vDOB As Variant, ByVal vSalary As Variant, ByVal vUName As String, ByVal vpword As String, ByVal vS_Date As Variant, ByVal vStatus As String, ByVal vBonus As Variant) As Variant
'Save employee information
Dim vEno As Variant
Dim vResponse As New Collection

On Error GoTo SaveEmployeeErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

Let vSql = "select * from EmployeeMaster where NINo = '" & Replace(vNINo, "'", "''") & "'"
oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic
If Not oRs.EOF Then
    vResponse.Add "Fail", "Sucess"
    vResponse.Add "0", "Eno"
    oRs.Close
Else
    vResponse.Add "OK", "Sucess"
    oRs.Close

    oRs.Open "EmployeeMaster", oDB, adOpenDynamic, adLockOptimistic
    oRs.AddNew
      
    oRs("UType") = vUtype
    oRs("JobType") = vJobType
    oRs("FNo") = vFNo
    oRs("Desig") = vDesig
    oRs("FName") = vFName
    oRs("SName") = vSname
    
    oRs("NickName") = vNickName
    oRs("AddressI") = vAddressI
    oRs("AddressII") = vAddressII
    
    oRs("Town") = vTown
    oRs("PostCode") = UCase(vPostCode)
    oRs("TelNo") = vTelNo
    
    oRs("Mob") = vMob
    oRs("Fax") = vFax
    oRs("Email") = vEmail
    
    
    oRs("OfficeExt") = vOfficeExt
    oRs("NINo") = vNINo
    oRs("DOB") = vDOB
    oRs("Salary") = vSalary
    oRs("UName") = vUName
    oRs("Pword") = vpword
    oRs("S_Date") = vS_Date
    oRs("Bonus") = vBonus
    oRs("Status") = vStatus
    
    oRs.Update
    oRs.Close
    
    Let vSql = "select max(ENo) as ENo from EmployeeMaster"
    oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic
    vEno = oRs.Fields("ENo")
    oRs.Close
    
    vResponse.Add vEno, "Eno"

End If

oDB.Close
Set oRs = Nothing
Set oDB = Nothing

vResponse.Add False, Key:="error"

Set SaveEmployee = vResponse

Exit Function
SaveEmployeeErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
SaveEmployee = "ERROR: " & Err.Description
End Function
Public Function DisplayEmployee(ByVal vFilter As String, ByVal vFilterValue As String, ByVal vsessionpage As Long, ByVal vPageSize As Long) As Variant
'Display the Employees

Dim vResponse As New Collection
Dim vRecordCount As Long
Dim vPagecount As Long

Dim arEmployees As Variant

On Error GoTo DisplayEmployeeErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

If vFilterValue <> "" Then
    If vFilter = "No" Then
        vSql = "select count(1) from EmployeeMaster a where status = 'A' and ENo =" & vFilterValue & ""
    ElseIf vFilter = "Name" Then
        vSql = "select count(1) from EmployeeMaster a where status = 'A' and FName like '%" & vFilterValue & "%' or SName like '%" & vFilterValue & "%'"
    End If
Else
    vSql = "select count(1) from EmployeeMaster where status = 'A'"
End If

oRs.Open vSql, oDB, adOpenKeyset, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vRecordCount = oRs.Fields(0)
Else
    vRecordCount = 0
End If
oRs.Close

If vFilterValue <> "" Then
    If vFilter = "No" Then
        vSql = "select * from EmployeeMaster a where status = 'A' and ENo =" & vFilterValue & ""
    ElseIf vFilter = "Name" Then
        vSql = "select * from EmployeeMaster a where status = 'A' and FName like '%" & vFilterValue & "%' or SName like '%" & vFilterValue & "%'"
    End If
Else
    vSql = "select * from EmployeeMaster where status = 'A'"
End If

oRs.PageSize = vPageSize
oRs.Open vSql, vdbConn, adOpenKeyset, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vPagecount = Round(vRecordCount / vPageSize + 0.49)
    If vPagecount >= vsessionpage Then
        If vsessionpage > 0 Then
            oRs.AbsolutePage = vsessionpage
            arEmployees = oRs.GetRows(vPageSize)
        Else
            arEmployees = oRs.GetRows()
        End If
    Else
        arEmployees = "Fail"
    End If
End If
oRs.Close

vResponse.Add arEmployees, "Employees"
vResponse.Add vRecordCount, "Recordcount"
vResponse.Add vPagecount, "Pagecount"
vResponse.Add False, Key:="error"
Set DisplayEmployee = vResponse

Set oRs = Nothing
oDB.Close
Set oDB = Nothing
Exit Function

DisplayEmployeeErrHandler:
    Set oRs = Nothing
    oDB.Close
    Set oDB = Nothing
    DisplayEmployee = "ERROR: " & Err.Description
End Function
Public Function UpdateEmployee(ByVal vUtype As String, ByVal vJobType As String, ByVal vFNo As Variant, ByVal vDesig As String, ByVal vFName As String, ByVal vSname As String, ByVal vNickName As String, ByVal vAddressI As String, ByVal vAddressII As String, ByVal vTown As String, ByVal vPostCode As String, ByVal vTelNo As String, ByVal vMob As String, ByVal vFax As String, ByVal vEmail As String, ByVal vOfficeExt As String, ByVal vNINo As String, ByVal vDOB As Variant, ByVal vSalary As Variant, ByVal vUName As String, ByVal vpword As String, ByVal vS_Date As Variant, ByVal vStatus As String, ByVal vEno As Long, ByVal vBonus As Variant) As Variant
'Update employee information

On Error GoTo UpdateEmployeeErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
Set oRsCheck = New ADODB.Recordset
oDB.Open vdbConn
vSql = "select * from EmployeeMaster where ENo = " & vEno & ""
oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic

vSql = "select count(1) from EmployeeMaster where NINo = '" & Replace(vNINo, "'", "''") & "' and eno <> " & vEno
oRsCheck.Open vSql, oDB, adOpenDynamic, adLockOptimistic
If oRsCheck(0) = 0 Then
    oRs.Fields("NINo") = vNINo
End If
oRsCheck.Close

oRs.Fields("email") = vEmail
oRs.Fields("UType") = vUtype
oRs.Fields("JobType") = vJobType
oRs.Fields("FNo") = vFNo
oRs.Fields("Desig") = vDesig
oRs.Fields("FName") = vFName
oRs.Fields("SName") = vSname
oRs.Fields("NickName") = vNickName
oRs.Fields("AddressI") = vAddressI
oRs.Fields("AddressII") = vAddressII
oRs.Fields("Town") = vTown
oRs.Fields("PostCode") = UCase(vPostCode)
oRs.Fields("TelNo") = vTelNo
oRs.Fields("Mob") = vMob
oRs.Fields("Fax") = vFax
oRs.Fields("Email") = vEmail
oRs.Fields("OfficeExt") = vOfficeExt
oRs.Fields("DOB") = vDOB
oRs.Fields("Salary") = vSalary
oRs.Fields("UName") = vUName
oRs.Fields("Pword") = vpword
oRs.Fields("S_Date") = vS_Date
oRs.Fields("Bonus") = vBonus
oRs.Fields("Status") = vStatus

oRs.Update
oRs.Close
oDB.Close
Set oRs = Nothing
Set oDB = Nothing

UpdateEmployee = "Ok"

Exit Function
UpdateEmployeeErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
UpdateEmployee = "ERROR: " & Err.Description
End Function
Public Function DisplayEmployeeDetail(ByVal vEno As Long) As Variant
'Obtaining employee details
Dim vResponse As New Collection
Dim vEmployeeDetail As Variant
Dim vEmployeeSalaryDetail As Variant
On Error GoTo DisplayEmployeeDetailErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "Select * from EmployeeMaster where Eno = " & vEno & ""
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vEmployeeDetail = oRs.GetRows()
Else
    vEmployeeDetail = "Fail"
End If
oRs.Close

vSql = "Select b.* from EmployeeMaster a,EmployeeSalaryAmends b where a.Eno = b.Eno and a.Eno = " & vEno & ""
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vEmployeeSalaryDetail = oRs.GetRows()
Else
    vEmployeeSalaryDetail = "Fail"
End If
oRs.Close

oDB.Close
vResponse.Add vEmployeeDetail, "EmployeeDetail"
vResponse.Add vEmployeeSalaryDetail, "EmployeeSalaryDetail"
vResponse.Add False, Key:="error"

Set DisplayEmployeeDetail = vResponse

Exit Function
DisplayEmployeeDetailErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
DisplayEmployeeDetail = "ERROR: " & Err.Description
End Function

Public Function DeleteEmployee(ByVal vEno As Long) As Variant
'Delete employee information

On Error GoTo DeleteEmployeeErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn
vSql = "select * from EmployeeMaster where ENo = " & vEno & ""
oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic
oRs.Fields("Status") = "D"
oRs.Update
oRs.Close
oDB.Close
Set oRs = Nothing
Set oDB = Nothing

DeleteEmployee = "Ok"

Exit Function
DeleteEmployeeErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
DeleteEmployee = "ERROR: " & Err.Description
End Function
Public Function Update_Password(ByVal vEmpNo As Integer, ByVal strPassword As String) As String
'Update inventory
Dim i As Long
 
On Error GoTo ErrHandler
Set oDB = New ADODB.Connection
oDB.Open vdbConn
 
vSql = "Update EmployeeMaster " & _
       " Set Pword = '" & Replace(strPassword, "'", "''") & "' " & _
       " Where Eno = " & vEmpNo
oDB.Execute vSql
 
Set oDB = Nothing
Update_Password = "OK"
Exit Function
ErrHandler:
Set oDB = Nothing
Update_Password = "ERROR: " & Err.Description
End Function

