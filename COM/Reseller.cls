VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Reseller"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private objNewmail As CDONTS.NewMail
Private oDB As ADODB.Connection
Private oRs As ADODB.Recordset
Private oRsCheck As ADODB.Recordset
Private sFrom As Variant
Private sTo As Variant
Private sSubject As Variant
Private sMessage As Variant
Private vsqlCount As Variant
Private vSql As Variant
Private vRecordCount As Integer
Private vPagecount As Integer
'Const vdbConn = "DSN=bakery;UID=bakeryuser;PWD=bakery2003"

Public Function SetEnvironment(ByVal env As String) As Variant
    ReadINI env
End Function

Public Function SaveReseller(ByVal strResellerName As String, ByVal strAddress As String, ByVal strTown As String, ByVal strPostcode As String, ByVal strTelephone As String, ByVal strFax As String, ByVal strPriceBand As Integer, ByVal strInvoiceFooter As String, ByVal strVATRegNo As String, ByVal strNotes As String, ByVal strLogo As String, ByVal strOldCode As String, ByVal strFactor As Double) As Variant
'Save Reseller information
Dim vRID As Variant
Dim vResponse As New Collection

On Error GoTo SaveResellerErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

Let vSql = "select * from Reseller where (ResellerName = '" & Replace(strResellerName, "'", "''") & "' or Logo='" & strLogo & "') and Status='A'"
oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic
If Not oRs.EOF Then
    vResponse.Add "Fail", "Sucess"
    vResponse.Add "0", "RID"
    oRs.Close
Else
    vResponse.Add "OK", "Sucess"
    oRs.Close

    oRs.Open "Reseller", oDB, adOpenDynamic, adLockOptimistic
    oRs.AddNew
      
    oRs.Fields("ResellerName") = strResellerName
    oRs.Fields("Address") = strAddress
    oRs.Fields("Town") = strTown
    oRs.Fields("Postcode") = strPostcode
    oRs.Fields("Telephone") = strTelephone
    oRs.Fields("Fax") = strFax
    oRs.Fields("PTNo") = strPriceBand
    oRs.Fields("InvoiceFooter") = strInvoiceFooter
    oRs.Fields("VATRegNo") = strVATRegNo
    oRs.Fields("Notes") = strNotes
    oRs.Fields("Logo") = strLogo
    oRs.Fields("OCCode") = strOldCode
    oRs.Fields("Factor") = strFactor
    
    oRs.Update
    oRs.Close
    
    Let vSql = "select max(RID) as RID from Reseller"
    oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic
    vRID = oRs.Fields("RID")
    oRs.Close
    
    vResponse.Add vRID, "RID"

End If

oDB.Close
Set oRs = Nothing
Set oDB = Nothing

vResponse.Add False, Key:="error"

Set SaveReseller = vResponse

Exit Function
SaveResellerErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
SaveReseller = "ERROR: " & Err.Description
End Function
Public Function DisplayReseller(ByVal vFilter As String, ByVal vFilterValue As String, ByVal vsessionpage As Long, ByVal vPageSize As Long) As Variant
'Display the Resellers

Dim vResponse As New Collection
Dim vRecordCount As Long
Dim vPagecount As Long

Dim arResellers As Variant

On Error GoTo DisplayResellerErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

If vFilterValue <> "" Then
    If vFilter = "RID" Then
        vSql = "select count(1) from Reseller a where status = 'A' and RID =" & vFilterValue & ""
    ElseIf vFilter = "ResellerName" Then
        vSql = "select count(1) from Reseller a where status = 'A' and RID like '%" & vFilterValue & "%'"
    End If
Else
    vSql = "select count(1) from Reseller where status = 'A'"
End If

oRs.Open vSql, oDB, adOpenKeyset, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vRecordCount = oRs.Fields(0)
Else
    vRecordCount = 0
End If
oRs.Close

If vFilterValue <> "" Then
    If vFilter = "RID" Then
        vSql = "select RID,ResellerName,PTNo,Logo,Factor from Reseller a where status = 'A' and RID =" & vFilterValue & ""
    ElseIf vFilter = "ResellerName" Then
        vSql = "select RID,ResellerName,PTNo,Logo,Factor from Reseller a where status = 'A' and ResellerName like '%" & vFilterValue & "%'"
    End If
Else
    vSql = "select RID,ResellerName,PTNo,Logo,Factor from Reseller where status = 'A'"
End If

oRs.PageSize = vPageSize
oRs.Open vSql, vdbConn, adOpenKeyset, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vPagecount = Round(vRecordCount / vPageSize + 0.49)
    If vPagecount >= vsessionpage Then
        If vsessionpage > 0 Then
            oRs.AbsolutePage = vsessionpage
            arResellers = oRs.GetRows(vPageSize)
        Else
            arResellers = oRs.GetRows()
        End If
    Else
        arResellers = "Fail"
    End If
End If
oRs.Close

vResponse.Add arResellers, "Resellers"
vResponse.Add vRecordCount, "Recordcount"
vResponse.Add vPagecount, "Pagecount"
vResponse.Add False, Key:="error"
Set DisplayReseller = vResponse

Set oRs = Nothing
oDB.Close
Set oDB = Nothing
Exit Function

DisplayResellerErrHandler:
    Set oRs = Nothing
    oDB.Close
    Set oDB = Nothing
    DisplayReseller = "ERROR: " & Err.Description
End Function

Public Function UpdateReseller(ByVal vRID As Long, ByVal strResellerName As String, ByVal strAddress As String, ByVal strTown As String, ByVal strPostcode As String, ByVal strTelephone As String, ByVal strFax As String, ByVal strPriceBand As Integer, ByVal strInvoiceFooter As String, ByVal strVATRegNo As String, ByVal strNotes As String, ByVal strLogo As String, ByVal strOldCode As String, ByVal strFactor As Double) As Variant
'Update Reseller information

On Error GoTo UpdateResellerErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
Set oRsCheck = New ADODB.Recordset
oDB.Open vdbConn
vSql = "select * from Reseller where RID = " & vRID & ""
oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic

vSql = "select * from Reseller where (ResellerName = '" & Replace(strResellerName, "'", "''") & "' or Logo='" & strLogo & "') and Status='A' and RID <> " & vRID
oRsCheck.Open vSql, oDB, adOpenDynamic, adLockOptimistic
If Not oRsCheck.EOF Then
    UpdateReseller = "Fail"
    oRsCheck.Close
    Set oRsCheck = Nothing
Else
    oRs.Fields("ResellerName") = strResellerName
    oRs.Fields("Address") = strAddress
    oRs.Fields("Town") = strTown
    oRs.Fields("Postcode") = strPostcode
    oRs.Fields("Telephone") = strTelephone
    oRs.Fields("Fax") = strFax
    oRs.Fields("PTNo") = strPriceBand
    oRs.Fields("InvoiceFooter") = strInvoiceFooter
    oRs.Fields("VATRegNo") = strVATRegNo
    oRs.Fields("Notes") = strNotes
    oRs.Fields("Logo") = strLogo
    oRs.Fields("OCCode") = strOldCode
    oRs.Fields("Factor") = strFactor
    oRs.Update
    UpdateReseller = "Ok"
End If
oRs.Close
oDB.Close
Set oRs = Nothing
Set oDB = Nothing

Exit Function
UpdateResellerErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
UpdateReseller = "ERROR: " & Err.Description
End Function
Public Function DisplayResellerDetail(ByVal vRID As Long) As Variant
'Obtaining Reseller details
Dim vResponse As New Collection
Dim vResellerDetail As Variant
Dim vResellerSalaryDetail As Variant
On Error GoTo DisplayResellerDetailErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "Select RID, ResellerName, Address, Town, Postcode, Telephone, Fax, PTNo, Logo, InvoiceFooter, VATRegNo, Notes, OCCode,Factor from Reseller where RID = " & vRID & ""
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vResellerDetail = oRs.GetRows()
Else
    vResellerDetail = "Fail"
End If
oRs.Close
oDB.Close
vResponse.Add vResellerDetail, "ResellerDetail"
vResponse.Add False, Key:="error"

Set DisplayResellerDetail = vResponse

Exit Function
DisplayResellerDetailErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
DisplayResellerDetail = "ERROR: " & Err.Description
End Function

Public Function DeleteReseller(ByVal vRID As Long) As Variant
'Delete Reseller information

On Error GoTo DeleteResellerErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn
vSql = "select * from Reseller where RID = " & vRID & ""
oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic
oRs.Fields("Status") = "D"
oRs.Update

vSql = "update CustomerMaster set ResellerID=0,PricePercentage=0 where ResellerID = " & vRID
oDB.Execute vSql

oRs.Close
oDB.Close
Set oRs = Nothing
Set oDB = Nothing

DeleteReseller = "Ok"

Exit Function
DeleteResellerErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
DeleteReseller = "ERROR: " & Err.Description
End Function

Public Function UpdateCustomerPriceBand(ByVal vRID As Long, ByVal ResellerPriceBand As Long) As Variant
'Update customer price band when reseller price band changed

On Error GoTo UpdateCustomerPriceBandErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "update CustomerMaster set PTNo=" & ResellerPriceBand & " where ResellerID = " & vRID
oDB.Execute vSql

oRs.Close
oDB.Close
Set oRs = Nothing
Set oDB = Nothing

UpdateCustomerPriceBand = "Ok"

Exit Function
UpdateCustomerPriceBandErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
UpdateCustomerPriceBand = "ERROR: " & Err.Description
End Function

Public Function ListResellers() As Variant
'List of resellers

Dim vResponse As New Collection

Dim arResellers As Variant

On Error GoTo ListResellersErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "select RID,ResellerName,PTNo,Factor from Reseller where status = 'A'"

oRs.Open vSql, vdbConn, adOpenKeyset, adLockReadOnly

If Not (oRs.BOF And oRs.EOF) Then
   arResellers = oRs.GetRows()
Else
   arResellers = "Fail"
End If
oRs.Close

vResponse.Add arResellers, "Resellers"
Set ListResellers = vResponse

Set oRs = Nothing
oDB.Close
Set oDB = Nothing
Exit Function

ListResellersErrHandler:
    Set oRs = Nothing
    oDB.Close
    Set oDB = Nothing
    ListResellers = "ERROR: " & Err.Description
End Function

Public Function DailyInvoiceResellerList(ByVal OrdDate As String) As Variant
Dim vResponse As New Collection

Dim aDailyInvoiceResellerList As Variant
Dim vInvoicesCount As Long

On Error GoTo GetInvoiceTotalErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "select Distinct a.ResellerID,a.ResellerInvoiceID,b.ResellerName,b.Address,b.Town,b.Postcode,a.OrdDate from ResellerInvoice a , Reseller b Where a.ResellerID=b.RID AND datediff(d,a.OrdDate,convert(datetime,'" & OrdDate & "',103)) = 0 and a.Resellerid = b.RId"
oRs.Open vSql, oDB, adOpenKeyset, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    aDailyInvoiceResellerList = oRs.GetRows()
Else
    aDailyInvoiceResellerList = "Fail"
End If
oRs.Close

vResponse.Add aDailyInvoiceResellerList, "DailyInvoiceResellerList"
Set DailyInvoiceResellerList = vResponse

oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Exit Function

GetInvoiceTotalErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
DailyInvoiceResellerList = "ERROR: " & Err.Description
End Function


Public Function GetResellerInvoiceDetails(ByVal ResellerID As Long, ByVal OrdDate As String, Optional ByVal dtype As String, Optional ByVal InvoiceNo As Long) As Variant
Dim vResponse As New Collection
Dim vRecordCount As Long
Dim vPagecount As Long
Dim strType As String

Dim aInvoiceDetails As Variant

On Error GoTo GetResellerInvoiceDetailsErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn
strType = Replace(Replace(Replace(Replace(dtype, "Morning", "1"), "Noon", "2"), "Evening", "3"), "All", "")

vSql = "exec dbo.sp_ResellerInvoice " & ResellerID & ",'" & Format(CDate(OrdDate), "YYYY/mm/dd") & "','" & strType & "'," & InvoiceNo
       
Set oRs = oDB.Execute(vSql)

If Not oRs.EOF Then
   aInvoiceDetails = oRs.GetRows()
Else
   aInvoiceDetails = "Fail"
End If
oRs.Close

vResponse.Add aInvoiceDetails, "InvoiceDetails"
vResponse.Add False, Key:="error"
Set GetResellerInvoiceDetails = vResponse

oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Exit Function

GetResellerInvoiceDetailsErrHandler:

oDB.Close
Set oRs = Nothing
Set oDB = Nothing
GetResellerInvoiceDetails = "ERROR: " & Err.Description
End Function


Public Function ResellerInvoiceView(ByVal ResellerInvoiceID As Long) As Variant
Dim vResponse As New Collection
Dim vRecordCount As Long
Dim vPagecount As Long
Dim strType As String

Dim aInvoiceDetails As Variant

On Error GoTo ResellerInvoiceViewErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "exec dbo.sp_ResellerInvoiceDetails " & ResellerInvoiceID
       
Set oRs = oDB.Execute(vSql)

If Not oRs.EOF Then
   aInvoiceDetails = oRs.GetRows()
Else
   aInvoiceDetails = "Fail"
End If
oRs.Close

vResponse.Add aInvoiceDetails, "InvoiceDetails"
vResponse.Add False, Key:="error"
Set ResellerInvoiceView = vResponse

oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Exit Function

ResellerInvoiceViewErrHandler:

oDB.Close
Set oRs = Nothing
Set oDB = Nothing
ResellerInvoiceView = "ERROR: " & Err.Description
End Function

Public Function DailyCreditResellerList(ByVal OrdDate As String) As Variant
Dim vResponse As New Collection

Dim aDailyCreditResellerList As Variant

On Error GoTo GetInvoiceTotalErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "select Distinct a.ResellerID,a.ResellerCreditID,b.ResellerName,b.Address,b.Town,b.Postcode,a.CreditDate from ResellerCredit a , Reseller b Where a.ResellerID=b.RID AND datediff(d,a.CreditDate,convert(datetime,'" & OrdDate & "',103)) = 0 and a.Resellerid = b.RId"

oRs.Open vSql, oDB, adOpenKeyset, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    aDailyCreditResellerList = oRs.GetRows()
Else
    aDailyCreditResellerList = "Fail"
End If
oRs.Close

vResponse.Add aDailyCreditResellerList, "DailyCreditResellerList"
Set DailyCreditResellerList = vResponse

oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Exit Function

GetInvoiceTotalErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
DailyCreditResellerList = "ERROR: " & Err.Description
End Function

Public Function GetResellerCreditNoteDetails(ByVal ResellerID As Long, ByVal OrdDate As String) As Variant
Dim vResponse As New Collection
Dim vRecordCount As Long
Dim vPagecount As Long

Dim aInvoiceDetails As Variant

On Error GoTo GetResellerCreditNoteDetailsErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "exec dbo.sp_ResellerCreditNote " & ResellerID & ",'" & Format(CDate(OrdDate), "YYYY/mm/dd") & "'"

Set oRs = oDB.Execute(vSql)

If Not oRs.EOF Then
   aInvoiceDetails = oRs.GetRows()
Else
   aInvoiceDetails = "Fail"
End If
oRs.Close
vResponse.Add aInvoiceDetails, "InvoiceDetails"
vResponse.Add False, Key:="error"
Set GetResellerCreditNoteDetails = vResponse

oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Exit Function

GetResellerCreditNoteDetailsErrHandler:

oDB.Close
Set oRs = Nothing
Set oDB = Nothing
GetResellerCreditNoteDetails = "ERROR: " & Err.Description
End Function

Public Function GetResellerCreditNoteInvNos(ByVal RCreditNo As Long) As String

Dim vSql As String
Dim InvNos As String

On Error GoTo GetResellerCreditNoteInvNosErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "SELECT dbo.fnGetResellerInvNos(" & RCreditNo & ") InvNos"

Set oRs = oDB.Execute(vSql)

If Not oRs.EOF Then
   InvNos = oRs(0).Value
Else
   InvNos = ""
End If

GetResellerCreditNoteInvNos = InvNos

oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Exit Function

GetResellerCreditNoteInvNosErrHandler:

oDB.Close
Set oRs = Nothing
Set oDB = Nothing
GetResellerCreditNoteInvNos = "ERROR: " & Err.Description
End Function


Public Function FindResellerInvoice(ByVal RID As Long, ByVal Rname As String, ByVal ordno As Long, Optional ByVal fromdt As String, Optional ByVal todt As String, Optional ByVal vsessionpage As Long, Optional ByVal vPageSize As Long) As Variant
'Find Reseller Invoices

Dim vResponse As New Collection
Dim vRecordCount As Long
Dim vPagecount As Long

Dim arResellers As Variant

On Error GoTo FindResellerInvoiceErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn
Dim sqlTemp As Variant
sqlTemp = ""
vsqlCount = ""
vSql = ""
vsqlCount = "select count(1) from Reseller R, ResellerInvoice RI where R.RID=RI.ResellerID and R.Status='A'"
vSql = "select RI.ResellerInvoiceID,RI.OrdDate,R.RID,R.ResellerName from Reseller R, ResellerInvoice RI where R.RID=RI.ResellerID and R.Status='A'"
If RID <> 0 Then
    sqlTemp = sqlTemp & " and R.RID=" & RID & " "
End If

If Rname <> "" Then
    sqlTemp = sqlTemp & " and R.ResellerName like '%" & Rname & "%' "
End If

If ordno <> 0 Then
    sqlTemp = sqlTemp & " and RI.ResellerInvoiceID=" & ordno & " "
End If

If fromdt <> "" And todt <> "" Then
     If IsDate(fromdt) And IsDate(todt) Then
        sqlTemp = sqlTemp & " and RI.OrdDate between convert(datetime,'" & fromdt & "',103) and convert(datetime,'" & todt & "' ,103)"
    End If
End If
vsqlCount = vsqlCount & sqlTemp
vSql = vSql & sqlTemp

oRs.Open vsqlCount, oDB, adOpenKeyset, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vRecordCount = oRs.Fields(0)
Else
    vRecordCount = 0
End If
oRs.Close

oRs.PageSize = vPageSize
oRs.Open vSql, vdbConn, adOpenKeyset, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vPagecount = Round(vRecordCount / vPageSize + 0.49)
    If vPagecount >= vsessionpage Then
        If vsessionpage > 0 Then
            oRs.AbsolutePage = vsessionpage
            arResellers = oRs.GetRows(vPageSize)
        Else
            arResellers = oRs.GetRows()
        End If
    Else
        arResellers = "Fail"
    End If
End If
oRs.Close

vResponse.Add arResellers, "Invoices"
vResponse.Add vRecordCount, "Recordcount"
vResponse.Add vPagecount, "Pagecount"
vResponse.Add False, Key:="error"
Set FindResellerInvoice = vResponse

Set oRs = Nothing
oDB.Close
Set oDB = Nothing
Exit Function

FindResellerInvoiceErrHandler:
    Set oRs = Nothing
    oDB.Close
    Set oDB = Nothing
    FindResellerInvoice = "ERROR: " & Err.Description
End Function

Public Function FindResellerCredit(ByVal RID As Long, ByVal Rname As String, ByVal ordno As Long, Optional ByVal fromdt As String, Optional ByVal todt As String, Optional ByVal vsessionpage As Long, Optional ByVal vPageSize As Long) As Variant
'Find Reseller Credits

Dim vResponse As New Collection
Dim vRecordCount As Long
Dim vPagecount As Long

Dim arResellers As Variant

On Error GoTo FindResellerCreditErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn
Dim sqlTemp As Variant
sqlTemp = ""
vsqlCount = ""
vSql = ""
vsqlCount = "select count(1) from(select distinct RC.ResellerCreditID,RC.CreditDate,R.RID,R.ResellerName from Reseller R, ResellerCredit RC, ResellerInvoice RI,ResellerCreditOrder RCO,ResellerInvoiceOrder RIO where R.Status='A' and R.RID = RC.ResellerID and RC.ResellerCreditID = RCO.ResellerCreditID and RCO.OrdNo = RIO.OrdNo and RIO.ResellerInvoiceID = RI.ResellerInvoiceID "
vSql = "select distinct RC.ResellerCreditID,RC.CreditDate,R.RID,R.ResellerName from Reseller R, ResellerCredit RC, ResellerInvoice RI,ResellerCreditOrder RCO,ResellerInvoiceOrder RIO where R.Status='A' and R.RID = RC.ResellerID and RC.ResellerCreditID = RCO.ResellerCreditID and RCO.OrdNo = RIO.OrdNo and RIO.ResellerInvoiceID = RI.ResellerInvoiceID "
If RID <> 0 Then
    sqlTemp = sqlTemp & " and R.RID=" & RID & " "
End If

If Rname <> "" Then
    sqlTemp = sqlTemp & " and R.ResellerName like '%" & Rname & "%' "
End If

If ordno <> 0 Then
    sqlTemp = sqlTemp & " and RC.ResellerCreditID=" & ordno & " "
End If

If fromdt <> "" And todt <> "" Then
     If IsDate(fromdt) And IsDate(todt) Then
        sqlTemp = sqlTemp & " and RC.CreditDate between convert(datetime,'" & fromdt & "',103) and convert(datetime,'" & todt & "' ,103)+1"
    End If
End If
vsqlCount = vsqlCount & sqlTemp
vSql = vSql & sqlTemp

vsqlCount = vsqlCount & " group by RC.ResellerCreditID,RC.CreditDate,R.RID,R.ResellerName) A"
vSql = vSql & " group by RC.ResellerCreditID,RC.CreditDate,R.RID,R.ResellerName"

oRs.Open vsqlCount, oDB, adOpenKeyset, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vRecordCount = oRs.Fields(0)
Else
    vRecordCount = 0
End If
oRs.Close

oRs.PageSize = vPageSize
oRs.Open vSql, vdbConn, adOpenKeyset, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vPagecount = Round(vRecordCount / vPageSize + 0.49)
    If vPagecount >= vsessionpage Then
        If vsessionpage > 0 Then
            oRs.AbsolutePage = vsessionpage
            arResellers = oRs.GetRows(vPageSize)
        Else
            arResellers = oRs.GetRows()
        End If
    Else
        arResellers = "Fail"
    End If
End If
oRs.Close

vResponse.Add arResellers, "Credit"
vResponse.Add vRecordCount, "Recordcount"
vResponse.Add vPagecount, "Pagecount"
vResponse.Add False, Key:="error"
Set FindResellerCredit = vResponse

Set oRs = Nothing
oDB.Close
Set oDB = Nothing
Exit Function

FindResellerCreditErrHandler:
    Set oRs = Nothing
    oDB.Close
    Set oDB = Nothing
    FindResellerCredit = "ERROR: " & Err.Description
End Function

Public Function CustomersInReseller(ByVal ResellerID As Long) As Variant
Dim vResponse As New Collection

Dim vCustomers As String

On Error GoTo ErrorHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "Select CNo,CName from CustomerMaster where Status = 'A' and ResellerID=" & ResellerID & " Order by CName"
oRs.Open vSql, oDB, adOpenKeyset, adLockReadOnly
vCustomers = ""
If Not (oRs.BOF And oRs.EOF) Then
  Do While Not oRs.EOF
    If (vCustomers = "") Then
         vCustomers = oRs(0).Value & " - " & oRs(1).Value
    Else
        vCustomers = vCustomers & "<br>" & oRs(0).Value & " - " & oRs(1).Value
    End If
    oRs.MoveNext
  Loop
End If
oRs.Close

oDB.Close
Set oRs = Nothing
Set oDB = Nothing
      
vResponse.Add vCustomers, "Customers"
Set CustomersInReseller = vResponse
Exit Function

ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
CustomersInReseller = "ERROR: " & Err.Description
End Function
