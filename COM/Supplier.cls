VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Supplier"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private objNewmail As CDONTS.NewMail
Private oDB As ADODB.Connection
Private oRs As ADODB.Recordset
Private oRsCheck As ADODB.Recordset
Private sFrom As Variant
Private sTo As Variant
Private sSubject As Variant
Private sMessage As Variant

Private vSql As Variant
Private vRecordCount As Integer
Private vPagecount As Integer
'Const vdbConn = "DSN=bakery;UID=bakeryuser;PWD=bakery2003"

Public Function SetEnvironment(ByVal env As String) As Variant
    ReadINI env
End Function

Public Function SaveSupplier(ByVal vName As String, ByVal vAddressI As String, ByVal vAddressII As String, ByVal vTown As String, ByVal vPostCode As String, ByVal vTelNo As String, ByVal vFax As String, ByVal vEmail As String, ByVal vURL As String, ByVal vPTerms As String, ByVal vC_Name As String, ByVal vC_JTitle As String, ByVal vStatus As String) As Variant
'Save Supplier information
Dim vSno As Variant
Dim vResponse As New Collection

On Error GoTo SaveSupplierErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

Let vSql = "select * from SupplierMaster where Name = '" & Replace(vName, "'", "''") & "'"
oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic
If Not oRs.EOF Then
    vResponse.Add "Fail", "Sucess"
    vResponse.Add "0", "Sno"
    oRs.Close
Else
    vResponse.Add "OK", "Sucess"
    oRs.Close

    oRs.Open "SupplierMaster", oDB, adOpenDynamic, adLockOptimistic
    oRs.AddNew
      
    oRs("Name") = vName
    oRs("AddressI") = vAddressI
    oRs("AddressII") = vAddressII
    oRs("Town") = vTown
    oRs("PostCode") = UCase(vPostCode)
    oRs("TelNo") = vTelNo
    oRs("Fax") = vFax
    oRs("Email") = vEmail
    oRs("URL") = vURL
    oRs("PTerms") = vPTerms
    oRs("C_Name") = vC_Name
    oRs("C_JTitle") = vC_JTitle
    oRs("Status") = vStatus
    oRs.Update
    oRs.Close
    
    Let vSql = "select max(Sno) as Sno from SupplierMaster"
    oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic
    vSno = oRs.Fields("Sno")
    oRs.Close
    
    vResponse.Add vSno, "Sno"

End If

oDB.Close
Set oRs = Nothing
Set oDB = Nothing

vResponse.Add False, Key:="error"

Set SaveSupplier = vResponse

Exit Function
SaveSupplierErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
SaveSupplier = "ERROR: " & Err.Description
End Function
Public Function DisplaySupplier(ByVal vSname As String, ByVal vScode As Integer, ByVal vIName As String, ByVal vsessionpage As Long, ByVal vPageSize As Long) As Variant
'Display the Suppliers

Dim vResponse As New Collection
Dim vRecordCount As Long
Dim vPagecount As Long

Dim arSuppliers As Variant

On Error GoTo DisplaySupplierErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "select count(1) from SupplierMaster where status = 'A' "
If vSname <> "" Then
    vSql = vSql & " and Name like '%" & vSname & "%'"
End If
If vScode <> 0 Then
    vSql = vSql & " and SNo = " & vScode & ""
End If
If vIName <> "" Then
    vSql = vSql & " and  sno in (select sno from SupIngMaster where ino in (select ino from ingredientmaster where iName like '%" & vIName & "%'))"
End If

oRs.Open vSql, oDB, adOpenKeyset, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vRecordCount = oRs.Fields(0)
Else
    vRecordCount = 0
End If
oRs.Close

vSql = "select * from SupplierMaster where status = 'A' "
If vSname <> "" Then
    vSql = vSql & " and Name like '%" & vSname & "%'"
End If
If vScode <> 0 Then
    vSql = vSql & " and SNo = " & vScode & ""
End If
If vIName <> "" Then
    vSql = vSql & " and  sno in (select sno from SupIngMaster where ino in (select ino from ingredientmaster where iName like '%" & vIName & "%'))"
End If

oRs.PageSize = vPageSize
oRs.Open vSql, vdbConn, adOpenKeyset, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vPagecount = Round(vRecordCount / vPageSize + 0.49)
    If vPagecount >= vsessionpage Then
        If vsessionpage > 0 Then
            oRs.AbsolutePage = vsessionpage
            arSuppliers = oRs.GetRows(vPageSize)
        Else
            arSuppliers = oRs.GetRows()
        End If
    Else
        arSuppliers = "Fail"
    End If
End If
oRs.Close

vResponse.Add arSuppliers, "Suppliers"
vResponse.Add vRecordCount, "Recordcount"
vResponse.Add vPagecount, "Pagecount"
vResponse.Add False, Key:="error"
Set DisplaySupplier = vResponse

Set oRs = Nothing
oDB.Close
Set oDB = Nothing
Exit Function

DisplaySupplierErrHandler:
    Set oRs = Nothing
    oDB.Close
    Set oDB = Nothing
    DisplaySupplier = "ERROR: " & Err.Description
End Function
Public Function UpdateSupplier(ByVal vName As String, ByVal vAddressI As String, ByVal vAddressII As String, ByVal vTown As String, ByVal vPostCode As String, ByVal vTelNo As String, ByVal vFax As String, ByVal vEmail As String, ByVal vURL As String, ByVal vPTerms As String, ByVal vC_Name As String, ByVal vC_JTitle As String, ByVal vStatus As String, ByVal vSno As Integer) As Variant
'Update Supplier information

On Error GoTo UpdateSupplierErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
Set oRsCheck = New ADODB.Recordset
oDB.Open vdbConn
vSql = "select * from SupplierMaster where Sno = " & vSno & ""
oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic

If vName <> oRs.Fields("Name") Then
    Let vSql = "select * from SupplierMaster where Name = '" & Replace(vName, "'", "''") & "' and Sno <> " & vSno & ""
    oRsCheck.Open vSql, oDB, adOpenDynamic, adLockOptimistic
    If oRsCheck.EOF Then
        oRs.Fields("Name") = vName
    End If
    oRsCheck.Close
End If

oRs.Fields("Email") = vEmail
oRs.Fields("AddressI") = vAddressI
oRs.Fields("AddressII") = vAddressII
oRs.Fields("Town") = vTown
oRs.Fields("PostCode") = UCase(vPostCode)
oRs.Fields("TelNo") = vTelNo
oRs.Fields("Fax") = vFax
oRs.Fields("Email") = vEmail
oRs.Fields("URL") = vURL
oRs.Fields("PTerms") = vPTerms
oRs.Fields("C_Name") = vC_Name
oRs.Fields("C_JTitle") = vC_JTitle
oRs.Fields("Status") = vStatus

oRs.Update
oRs.Close
oDB.Close
Set oRs = Nothing
Set oDB = Nothing

UpdateSupplier = "Ok"

Exit Function
UpdateSupplierErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
UpdateSupplier = "ERROR: " & Err.Description
End Function
Public Function DisplaySupplierDetail(ByVal vSno As Long) As Variant
'Obtaining Supplier details
Dim vResponse As New Collection
Dim vSupplierDetail As Variant
Dim vSupplierIngDetail As Variant
On Error GoTo DisplaySupplierDetailErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "select * from SupplierMaster where sno = " & vSno
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vSupplierDetail = oRs.GetRows()
Else
    vSupplierDetail = "Fail"
End If
oRs.Close

vSql = "select catname = (select ITName from IngTypeMaster where itno=a.itno) ,a.ino,iname,ITName=(select ITName from IngTypeMaster where itno=a.itno),QUnit,UPrice, Facility =case when (select count(1) from SupFacIngMaster where status = 'A' and fno=15 and ino=a.ino and sno=b.sno) > 0 "
vSql = vSql & "  then (select fname from facilitymaster where fno=15) else space(0) end + case when (select count(1) from SupFacIngMaster where status = 'A' and fno=18  and ino=a.ino and sno=b.sno) > 0 then  case when (select count(1) from SupFacIngMaster where status = 'A' and fno=15 and ino=a.ino and sno=b.sno) > 0 "
vSql = vSql & "  then ',' else space(0) end + (select fname from facilitymaster where fno=18 ) else space(0) end+case when (select count(1) from SupFacIngMaster where status = 'A' and fno=90 and ino=a.ino and sno=b.sno) > 0 then case when (select count(1) from SupFacIngMaster where status = 'A' and fno=18  and ino=a.ino and sno=b.sno) > 0  "
vSql = vSql & "  or (select count(1) from SupFacIngMaster where status = 'A' and fno=15 and ino=a.ino and sno=b.sno) > 0 then ',' else           space(0) end +(select fname from facilitymaster where fno=90) else space(0) end + case when (select count(1) from SupFacIngMaster where status = 'A' and fno=11 and ino=a.ino and sno=b.sno) > 0 then  "
vSql = vSql & "  case when (select count(1) from SupFacIngMaster where status = 'A' and fno=90 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=18  and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=15 and ino=a.ino and sno=b.sno) > 0 then ',' else space(0) "
vSql = vSql & "  end + (select fname from facilitymaster where fno=11) else space(0) end+case when (select count(1) from SupFacIngMaster where status = 'A' and fno=11 and ino=a.ino and sno=b.sno) > 0 then  case when (select count(1) from SupFacIngMaster where status = 'A' and fno=11 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from   "
vSql = vSql & "  SupFacIngMaster where status = 'A' and fno=90 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=18  and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=15 and ino=a.ino and sno=b.sno) > 0 then       ',' else space(0) end  "
vSql = vSql & "  +(select fname from facilitymaster where fno=11) else space(0) end + case when (select count(1) from SupFacIngMaster where status = 'A' and fno=21 and ino=a.ino and sno=b.sno) > 0 then  case when (select count(1) from SupFacIngMaster where status = 'A' and fno=11 and ino=a.ino and sno=b.sno) > 0  "
vSql = vSql & "  or (select count(1) from SupFacIngMaster where status = 'A' and fno=11 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=90 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=18  and ino=a.ino and sno=b.sno) > 0 or  "
vSql = vSql & "  (select count(1) from SupFacIngMaster where status = 'A' and fno=15 and ino=a.ino and sno=b.sno) > 0  then ',' else space(0) end +  (select fname from facilitymaster where fno=21) else space(0) end+case when (select count(1) from SupFacIngMaster where status = 'A' and fno=22 and ino=a.ino and sno=b.sno) > 0  "
vSql = vSql & "  then case when (select count(1) from SupFacIngMaster where status = 'A' and fno=21 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=11 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=11 and ino=a.ino and sno=b.sno) > 0 or  "
vSql = vSql & "  (select count(1) from SupFacIngMaster where status = 'A' and fno=90 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=18  and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=15 and ino=a.ino and sno=b.sno) > 0 then ',' else  space(0)  "
vSql = vSql & "  end +(select fname from facilitymaster where fno=22) else space(0) end + case when (select count(1) from SupFacIngMaster where status = 'A' and fno=23 and ino=a.ino and sno=b.sno) > 0 then  case when (select count(1) from SupFacIngMaster where status = 'A' and fno=22 and ino=a.ino and sno=b.sno) > 0 or "
vSql = vSql & "  (select count(1) from SupFacIngMaster where status = 'A' and fno=21 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=11 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and  fno=11 and ino=a.ino and sno=b.sno) > 0 or "
vSql = vSql & "  (select count(1) from SupFacIngMaster where status = 'A' and fno=90 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=18  and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=15 and ino=a.ino and sno=b.sno) > 0 then "
vSql = vSql & "  ',' else space(0) end + (select fname from facilitymaster where fno=23) else space(0) end+case when (select count(1) from SupFacIngMaster where status = 'A' and fno=30 and ino=a.ino and sno=b.sno) > 0 then case when (select count(1) from SupFacIngMaster where status = 'A' and fno=23 and ino=a.ino  "
vSql = vSql & "   and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=22 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=21 and ino=a.ino and sno=b.sno) > 0  or (select count(1) from SupFacIngMaster where status = 'A' and fno=11 and ino=a.ino and  "
vSql = vSql & "   sno=b.sno) > 0  or (select count(1) from SupFacIngMaster where status = 'A' and fno=11 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=90 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=18  and ino=a.ino and sno=b.sno) > 0  "
vSql = vSql & "  or (select count(1) from SupFacIngMaster where status = 'A' and fno=15 and ino=a.ino and sno=b.sno) > 0  then  ',' else space(0) end +(select fname from facilitymaster where fno=30) else       space(0) end + case when (select count(1) from SupFacIngMaster where status = 'A' and fno=31 and ino=a.ino and sno=b.sno) > 0  "
vSql = vSql & "   then case when (select count(1) from SupFacIngMaster where status = 'A' and fno=30 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=23 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=22 and ino=a.ino and sno=b.sno) > 0 or "
vSql = vSql & "   (select count(1) from SupFacIngMaster where status = 'A' and fno=21 and ino=a.ino and sno=b.sno) > 0 or  (select count(1) from SupFacIngMaster where status = 'A' and fno=11 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=11 and ino=a.ino and sno=b.sno) > 0 or (select count(1) "
vSql = vSql & "   from SupFacIngMaster where status = 'A' and fno=90 and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=18  and ino=a.ino and sno=b.sno) > 0 or (select count(1) from SupFacIngMaster where status = 'A' and fno=15 and ino=a.ino and sno=b.sno) > 0  then ',' else       space(0) end + "
vSql = vSql & "  (select fname from facilitymaster where fno=31) else space(0) end, OrderReq = case when often='D' then 'Daily' when often='W' then 'Weekly' when often='M' then 'Monthly' when often='Q' then '3 Months' when often='H' then '6 Months' when often='Y' then 'Yearly' end,a.utype  "
vSql = vSql & "  from IngredientMaster a,SupIngMaster b where a.status='A' and b.status='A' and a.ino=b.ino and sno= " & vSno

oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vSupplierIngDetail = oRs.GetRows()
Else
    vSupplierIngDetail = "Fail"
End If
oRs.Close

oDB.Close
vResponse.Add vSupplierDetail, "SupplierDetail"
vResponse.Add vSupplierIngDetail, "SupplierIngDetail"
vResponse.Add False, Key:="error"

Set DisplaySupplierDetail = vResponse

Exit Function
DisplaySupplierDetailErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
DisplaySupplierDetail = "ERROR: " & Err.Description
End Function
Public Function UpdateSupplierIng(ByVal vIno As Integer, ByVal vSno As Integer, ByVal vQty As Variant, ByVal vPrice As Variant, ByVal vOften As String, ByVal vFacarray As Variant) As Variant
'Save Supplier ing information
Dim vArray As Variant
Dim i As Integer
On Error GoTo UpdateSupplierIngErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

Let vSql = "select * from SupIngMaster where INo = " & vIno & " and Sno = " & vSno
oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic
If Not oRs.EOF Then
    oRs.Fields("Ino") = vIno
    oRs.Fields("Sno") = vSno
    oRs.Fields("QUnit") = vQty
    oRs.Fields("UPrice") = vPrice
    oRs.Fields("Often") = vOften
    If vPrice = 0 Then
        oRs.Fields("Status") = "D"
    Else
        oRs.Fields("Status") = "A"
    End If
    oRs.Update
    oRs.Close
Else
    oRs.AddNew
    oRs("Ino") = vIno
    oRs("Sno") = vSno
    oRs("QUnit") = vQty
    oRs("UPrice") = vPrice
    oRs("Often") = vOften
    oRs.Update
    oRs.Close

End If

vSql = "update SupFacIngMaster set status = 'D' where INo=" & vIno & " and Sno=" & vSno
oDB.Execute vSql

If Not IsArray(vFacarray) Then


    Let vSql = "select * from SupFacIngMaster where INo=" & vIno & " and Sno=" & vSno & " and FNo=" & vFacarray
    oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic
    If Not oRs.EOF Then
        
        oRs.Fields("INo") = vIno
        oRs.Fields("Sno") = vSno
        oRs.Fields("FNo") = vFacarray
        oRs.Fields("Status") = "A"
        oRs.Update
    Else
        
        oRs.AddNew
        oRs("INo") = vIno
        oRs("Sno") = vSno
        oRs("FNo") = vFacarray(i)
        oRs.Update
    End If
    oRs.Close

Else

For i = 0 To UBound(vFacarray)

    Let vSql = "select * from SupFacIngMaster where INo=" & vIno & " and Sno=" & vSno & " and FNo=" & vFacarray(i)
    oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic
    If Not oRs.EOF Then
        
        oRs.Fields("INo") = vIno
        oRs.Fields("Sno") = vSno
        oRs.Fields("FNo") = vFacarray(i)
        oRs.Fields("Status") = "A"
        oRs.Update
    Else
        
        oRs.AddNew
        oRs("INo") = vIno
        oRs("Sno") = vSno
        oRs("FNo") = vFacarray(i)
        oRs.Update
    End If
    oRs.Close

Next

End If


oDB.Close
Set oRs = Nothing
Set oDB = Nothing

UpdateSupplierIng = "Ok"

Exit Function
UpdateSupplierIngErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
UpdateSupplierIng = "ERROR: " & Err.Description
End Function
Public Function DeleteSupplier(ByVal vSno As Long) As Variant
'Delete employee information

On Error GoTo DeleteSupplierErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn
vSql = "select * from SupplierMaster where SNo = " & vSno & ""
oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic
oRs.Fields("Status") = "D"
oRs.Update
oRs.Close
oDB.Close
Set oRs = Nothing
Set oDB = Nothing

DeleteSupplier = "Ok"

Exit Function
DeleteSupplierErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
DeleteSupplier = "ERROR: " & Err.Description
End Function
Public Function DeleteSupplierIng(ByVal vSno As Long, ByVal vIno As Long) As Variant
'Delete employee information

On Error GoTo DeleteSupplierIngErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn
vSql = "select * from SupIngMaster where INo = " & vIno & " and Sno = " & vSno
oRs.Open vSql, oDB, adOpenDynamic, adLockOptimistic
oRs.Fields("Status") = "D"
oRs.Update
oRs.Close
oDB.Close
Set oRs = Nothing
Set oDB = Nothing

DeleteSupplierIng = "Ok"

Exit Function
DeleteSupplierIngErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
DeleteSupplierIng = "ERROR: " & Err.Description
End Function
Public Function DisplaySupIngFac(ByVal vSno As Long, ByVal vIno As Long) As Variant
'obtaining ing list
Dim vSupIngFac As Variant
Dim vResponse As New Collection
On Error GoTo DisplaySupIngFacErrHandler

Set oRs = New ADODB.Recordset

If vIno > 0 Then
    Let vSql = "select *,selected=case when (select count(1) from SupFacIngMaster where status = 'A' and INo = " & vIno & " and Sno = " & vSno & " and FNo = a.fno)>0 then 'Selected' else '' end from FacilityMaster a where status = 'A' order by 3 asc"
Else
    Let vSql = "select *,'' from FacilityMaster where status = 'A' order by 2 asc"
End If

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vSupIngFac = oRs.GetRows()
Else
    vSupIngFac = "Fail"
End If
oRs.Close

vResponse.Add vSupIngFac, "SupIngFac"
vResponse.Add False, Key:="error"
Set DisplaySupIngFac = vResponse
Set oRs = Nothing
Exit Function

DisplaySupIngFacErrHandler:
Set oRs = Nothing
DisplaySupIngFac = "ERROR: " & Err.Description
End Function
