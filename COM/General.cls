VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "General"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' Written by Ellis Dee
Option Explicit
Option Compare Text

Private objNewmail As CDONTS.NewMail
Private oDB As ADODB.Connection
Private oRs As ADODB.Recordset
Private oRsCheck As ADODB.Recordset

Private sFrom As Variant
Private sTo As Variant
Private sSubject As Variant
Private sMessage As Variant

Private vSql As Variant
Private vRecordCount As Integer
Private vPagecount As Integer
'Const vdbConn = "DSN=bakery;UID=bakeryuser;PWD=bakery2003"

Public Function SetEnvironment(ByVal env As String) As Variant
    ReadINI env
End Function

Public Function Login(ByVal vUName As String, ByVal vpword As String) As Variant
'Login authenticity code using email address and password

Dim vResponse As New Collection
Dim vEmployeeDetail As Variant
Dim vSucess As Variant

On Error GoTo LoginErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "select * from dbo.EmployeeMaster where  Utype in ('S','A','U','N') and UName = '" & vUName & "' and pword = '" & vpword & "'"
oRs.Open vSql, oDB, adOpenDynamic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vEmployeeDetail = oRs.GetRows()
    vSucess = "True"
Else
    vEmployeeDetail = "Fail"
    vSucess = "Fail"
End If
oRs.Close
oDB.Close

vResponse.Add vEmployeeDetail, "EmployeeDetail"
vResponse.Add vSucess, "Sucess"
vResponse.Add False, Key:="error"
Set Login = vResponse

Set oRs = Nothing
Set oDB = Nothing
Exit Function

LoginErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Login = "ERROR: " & Err.Description
End Function
Public Function Passwordmail(ByVal vEmail As String, ByVal vFrom As String, ByVal vClient As String) As Variant
'Sending password as an email to the user
Dim vTitle As Variant
Dim vFirstName As Variant
Dim vUserName As Variant
Dim vPassword As Variant
On Error GoTo PasswordmailErrHandler

Set objNewmail = CreateObject("CDONTS.Newmail")
Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
Let vSql = "select * from EmployeeMaster where Utype in ('A','U') and Email = '" & vEmail & "';"
oDB.Open vdbConn
Set oRs = oDB.Execute(vSql)
If Not oRs.EOF Then
    vFirstName = oRs.Fields("FName")
    vUserName = oRs.Fields("UName")
    vPassword = oRs.Fields("Pword")
    'Customer Mail
    sFrom = vFrom
    sTo = vEmail
    sSubject = "Your employee Details"
    sMessage = "Dear" & " " & vFirstName & "." & vbCrLf _
    & vbCrLf _
    & "Your username is " & " " & vUserName & vbCrLf _
    & vbCrLf _
    & "Your password is " & " " & vPassword & vbCrLf _
    & vbCrLf _
    & vClient & " Customer Service"
    objNewmail.Send sFrom, sTo, sSubject, sMessage
    Passwordmail = "OK"
Else
    Passwordmail = ""
End If
oRs.Close
oDB.Close
Set objNewmail = Nothing
Set oRs = Nothing
Set oDB = Nothing
Exit Function
PasswordmailErrHandler:
oDB.Close

Set oRs = Nothing
Set oDB = Nothing
Passwordmail = "ERROR: " & Err.Description
End Function
Public Function DisplayIng() As Variant
'obtaining ing list
Dim vIngredients As Variant
Dim vResponse As New Collection
On Error GoTo DisplayIngErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select * from IngredientMaster where status = 'A' order by 3 asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vIngredients = oRs.GetRows()
Else
    vIngredients = "Fail"
End If
oRs.Close

vResponse.Add vIngredients, "Ingredients"
vResponse.Add False, Key:="error"
Set DisplayIng = vResponse
Set oRs = Nothing
Exit Function

DisplayIngErrHandler:
Set oRs = Nothing
DisplayIng = "ERROR: " & Err.Description
End Function
Public Function DisplayIngType() As Variant
'obtaining ing list
Dim vIngredientType As Variant
Dim vResponse As New Collection
On Error GoTo DisplayIngTypeErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select * from IngTypeMaster a where a.status = 'A' order by 3 asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vIngredientType = oRs.GetRows()
Else
    vIngredientType = "Fail"
End If
oRs.Close

vResponse.Add vIngredientType, "IngredientType"
vResponse.Add False, Key:="error"
Set DisplayIngType = vResponse
Set oRs = Nothing
Exit Function

DisplayIngTypeErrHandler:
Set oRs = Nothing
DisplayIngType = "ERROR: " & Err.Description
End Function

Public Function GetPriceTypeValues() As Variant
'obtaining ing list
Dim vPriceTypeValue As Variant
Dim vResponse As New Collection
On Error GoTo DisplayIngTypeErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select Val from PriceTypeMaster where Status = 'A' and PTName='A1'"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vPriceTypeValue = oRs(0).Value
Else
    vPriceTypeValue = 0
End If
oRs.Close

vResponse.Add vPriceTypeValue, "PriceTypeValue"
vResponse.Add False, Key:="error"
Set GetPriceTypeValues = vResponse
Set oRs = Nothing
Exit Function

DisplayIngTypeErrHandler:
Set oRs = Nothing
GetPriceTypeValues = "ERROR: " & Err.Description
End Function

Public Function DisplayDoughType() As Variant
'obtaining ing list
Dim vDoughType As Variant
Dim vResponse As New Collection
On Error GoTo DisplayDoughTypeErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select * from DoughMaster where status='A' order by 2 asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vDoughType = oRs.GetRows()
Else
    vDoughType = "Fail"
End If
oRs.Close

vResponse.Add vDoughType, "DoughType"
vResponse.Add False, Key:="error"
Set DisplayDoughType = vResponse
Set oRs = Nothing
Exit Function

DisplayDoughTypeErrHandler:
Set oRs = Nothing
DisplayDoughType = "ERROR: " & Err.Description
End Function

'This was writtedn by Ramanan on 4th Feb 2010
'Lets get the Dough Names using Dough Types (Direct or 48 Hours)
Public Function DisplayDoughName(ByVal vDoughType As Integer) As Variant
'obtaining ing list
Dim vDoughName As Variant
Dim vResponse As New Collection
On Error GoTo DisplayDoughNameErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select * from DoughMaster where status='A' and DTNo = " & vDoughType & " order by 2 asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vDoughName = oRs.GetRows()
Else
    vDoughName = "Fail"
End If
oRs.Close

vResponse.Add vDoughName, "DoughName"
vResponse.Add False, Key:="error"
Set DisplayDoughName = vResponse
Set oRs = Nothing
Exit Function

DisplayDoughNameErrHandler:
Set oRs = Nothing
DisplayDoughName = "ERROR: " & Err.Description
End Function

'This was writtedn by Ramanan on 17th Feb 2010
'Lets get the Dough Names using Dough Types (Direct or 48 Hours) and it is used in Dough Mixing Report
'This was modified by Ramanan on 2nd March to include doughs with mixing method 1 and 3
Public Function DisplayDoughNameDoughMixingReport(ByVal vDoughType As Integer) As Variant
'obtaining ing list
Dim vDoughName As Variant
Dim vResponse As New Collection
On Error GoTo DisplayDoughNameDoughMixingReportErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select * from DoughMaster where status='A' and DoughMixingMethod IN (1,3) and DTNo = " & vDoughType & " order by 2 asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vDoughName = oRs.GetRows()
Else
    vDoughName = "Fail"
End If
oRs.Close

vResponse.Add vDoughName, "DoughName"
vResponse.Add False, Key:="error"
Set DisplayDoughNameDoughMixingReport = vResponse
Set oRs = Nothing
Exit Function

DisplayDoughNameDoughMixingReportErrHandler:
Set oRs = Nothing
DisplayDoughNameDoughMixingReport = "ERROR: " & Err.Description
End Function




'This was writtedn by Ramanan on 5th Feb 2010
'Lets get the Dough Names with second mix using Dough Types (Direct or 48 Hours)
Public Function DisplayDoughNameWithSecondMix(ByVal vDoughType As Integer) As Variant
'obtaining ing list
Dim vDoughName As Variant
Dim vResponse As New Collection
On Error GoTo DisplayDoughNameWithSecondMixErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select distinct a.MainDoughNo,b.DName from DoughMaster a inner join DoughMaster b ON a.MainDoughNo = b.DNo where a.status='A' and a.IsSecondMixCategory = 1 and b.DTNo = " & vDoughType & " order by 2 asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vDoughName = oRs.GetRows()
Else
    vDoughName = "Fail"
End If
oRs.Close

vResponse.Add vDoughName, "DoughName"
vResponse.Add False, Key:="error"
Set DisplayDoughNameWithSecondMix = vResponse
Set oRs = Nothing
Exit Function

DisplayDoughNameWithSecondMixErrHandler:
Set oRs = Nothing
DisplayDoughNameWithSecondMix = "ERROR: " & Err.Description
End Function

'This was writtedn by Ramanan on 15th Feb 2010
'Lets get the Dough Names of Two Stage Mixing (Direct or 48 Hours)
Public Function DisplayDoughNameTwoStageMixing(ByVal vDoughType As Integer) As Variant
'obtaining ing list
Dim vDoughName As Variant
Dim vResponse As New Collection
On Error GoTo DisplayDoughNameTwoStageMixingErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select * from DoughMaster where status='A' and DoughMixingMethod = 3 and DTNo = " & vDoughType & " order by 2 asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vDoughName = oRs.GetRows()
Else
    vDoughName = "Fail"
End If
oRs.Close

vResponse.Add vDoughName, "DoughName"
vResponse.Add False, Key:="error"
Set DisplayDoughNameTwoStageMixing = vResponse
Set oRs = Nothing
Exit Function

DisplayDoughNameTwoStageMixingErrHandler:
Set oRs = Nothing
DisplayDoughNameTwoStageMixing = "ERROR: " & Err.Description
End Function

'This is developed by Ramanan on 2nd March 2010
'This is to identify a dough whether it is a main dough with out second mix category (0)
'or main dough with second mix category (1)
'or two stage mixing category (3)
Public Function DoughMixingReportType(ByVal vDNo As Integer) As Variant
Dim vResponse As New Collection
Dim aReportType As Variant

On Error GoTo ErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn
oDB.CommandTimeout = 6000
vSql = "exec dbo.spGetDoughStatus " & vDNo
Set oRs = oDB.Execute(vSql)

If Not (oRs.BOF And oRs.EOF) Then
    aReportType = oRs.GetRows()
Else
    aReportType = "Fail"
End If
oRs.Close

vResponse.Add aReportType, "ReportType"
vResponse.Add False, Key:="error"

Set DoughMixingReportType = vResponse

oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Exit Function

ErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
DoughMixingReportType = "ERROR: " & Err.Description
End Function


Public Function DisplayFacility() As Variant
'obtaining ing list
Dim vFacility As Variant
Dim vResponse As New Collection
On Error GoTo DisplayFacilityErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select * from FacilityMaster where status='A' order by 2 asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vFacility = oRs.GetRows()
Else
    vFacility = "Fail"
End If
oRs.Close

vResponse.Add vFacility, "Facility"
vResponse.Add False, Key:="error"
Set DisplayFacility = vResponse
Set oRs = Nothing
Exit Function
DisplayFacilityErrHandler:
Set oRs = Nothing
DisplayFacility = "ERROR: " & Err.Description
End Function



Public Function DisplayTypeDes() As Variant
'obtaining ing list
Dim vType1Detail, vType2Detail, vType3ForPackingDetail, vType4ForPackingDetail As Variant
Dim vResponse As New Collection
On Error GoTo DisplayTypeErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select * from Type1 where Status='A' order by Type1Des asc"
oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vType1Detail = oRs.GetRows()
Else
    vType1Detail = "Fail"
End If
oRs.Close

Let vSql = "select * from Type2 where Status='A' order by Type2Des asc"
oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vType2Detail = oRs.GetRows()
Else
    vType2Detail = "Fail"
End If
oRs.Close

Let vSql = "select * from ProductType3ForPacking where Status='A' order by Type3ForPacking asc"
oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vType3ForPackingDetail = oRs.GetRows()
Else
    vType3ForPackingDetail = "Fail"
End If
oRs.Close

Let vSql = "select * from ProductType4ForPacking where Status='A' order by Type4ForPacking asc"
oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vType4ForPackingDetail = oRs.GetRows()
Else
    vType4ForPackingDetail = "Fail"
End If
oRs.Close

vResponse.Add vType1Detail, "Type1"
vResponse.Add vType2Detail, "Type2"
vResponse.Add vType3ForPackingDetail, "Type3ForPacking"
vResponse.Add vType4ForPackingDetail, "Type4ForPacking"

vResponse.Add False, Key:="error"
Set DisplayTypeDes = vResponse
Set oRs = Nothing
Exit Function
DisplayTypeErrHandler:
Set oRs = Nothing
DisplayTypeDes = "ERROR: " & Err.Description
End Function

Public Function DisplaySupplier() As Variant
'obtaining ing list
Dim vSupplier As Variant
Dim vResponse As New Collection
On Error GoTo DisplaySupplierErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select * from SupplierMaster where status='A' order by 2 asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vSupplier = oRs.GetRows()
Else
    vSupplier = "Fail"
End If
oRs.Close

vResponse.Add vSupplier, "Supplier"
vResponse.Add False, Key:="error"
Set DisplaySupplier = vResponse
Set oRs = Nothing
Exit Function
DisplaySupplierErrHandler:
Set oRs = Nothing
DisplaySupplier = "ERROR: " & Err.Description
End Function
Public Function DisplayVec() As Variant
'obtaining ing list
Dim vVec As Variant
Dim vResponse As New Collection
On Error GoTo DisplayVecErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select * from VehicleMaster where status='A' order by 1 asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vVec = oRs.GetRows()
Else
    vVec = "Fail"
End If
oRs.Close

vResponse.Add vVec, "Vec"
vResponse.Add False, Key:="error"
Set DisplayVec = vResponse
Set oRs = Nothing
Exit Function
DisplayVecErrHandler:
Set oRs = Nothing
DisplayVec = "ERROR: " & Err.Description
End Function
Public Function Display_InventoryCount(ByVal strDate As String, ByVal vFacNo As Long) As Variant

'obtaining ing list

Dim vResponse As New Collection

Dim arIngredient As Variant

On Error GoTo ErrHandler

Set oDB = New ADODB.Connection

Set oRs = New ADODB.Recordset

oDB.Open vdbConn

Let vSql = "select ino,iname,utype,(select qty from stockcount where ino=a.ino and fno = " & vFacNo & " and datediff(d,scdate,convert(datetime,'" & strDate & "',103))=0 ) from IngredientMaster a where ino in(select ino from SupFacIngMaster where status='A' and fno = " & vFacNo & ")"
oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then

    arIngredient = oRs.GetRows()

Else

    arIngredient = "Fail"

End If

oRs.Close

oDB.Close

vResponse.Add arIngredient, "Ingredient"

Set Display_InventoryCount = vResponse

Set oRs = Nothing

Exit Function

ErrHandler:

Set oRs = Nothing

Set oDB = Nothing

Display_InventoryCount = "ERROR: " & Err.Description

End Function
Public Function Update_InventoryCount(ByVal strDate As String, ByVal vFacNo As Long, arUpdate As Variant) As String
'Update inventory
Dim i As Long

On Error GoTo ErrHandler
Set oDB = New ADODB.Connection
oDB.Open vdbConn

vSql = "delete stockcount where datediff(d,scdate,convert(datetime,'" & strDate & "',103)) = 0 and FNo = " & vFacNo
oDB.Execute vSql

If IsArray(arUpdate) Then
  For i = 0 To UBound(arUpdate, 2)
    If Trim(arUpdate(0, i)) <> "" Then
      vSql = "Insert into stockcount(ScDate,FNo,INo,Qty) " & _
             " Values(convert(DateTime, '" & strDate & "' , 103)," & vFacNo & "," & arUpdate(0, i) & "," & arUpdate(1, i) & ")"
      oDB.Execute vSql
    End If
  Next
End If

Set oDB = Nothing
Update_InventoryCount = "OK"
Exit Function
ErrHandler:
Set oDB = Nothing
Update_InventoryCount = "ERROR: " & Err.Description
End Function

Public Function DisplaySubFacIng(ByVal sno As Integer, ByVal fno As Integer) As Variant
'obtaining ing list
Dim vIngredients As Variant
Dim vResponse As New Collection
On Error GoTo DisplaySubFacIngErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "SELECT a.* from IngredientMaster a, SupFacIngMaster b,SupIngMaster c  where a.ino=c.ino and c.sno = " & sno & " and a.ino=b.ino and b.sno = " & sno & " and b.fNo = " & fno & " and a.status = 'A' and c.status = 'A' order by 3 asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vIngredients = oRs.GetRows()
Else
    vIngredients = "Fail"
End If
oRs.Close

vResponse.Add vIngredients, "Ingredients"
vResponse.Add False, Key:="error"
Set DisplaySubFacIng = vResponse
Set oRs = Nothing
Exit Function

DisplaySubFacIngErrHandler:
Set oRs = Nothing
DisplaySubFacIng = "ERROR: " & Err.Description
End Function

Public Function ListProductionCategory() As Variant
Dim vProductionCategory As Variant
Dim vResponse As New Collection
On Error GoTo ListProductionCategoryErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select PCNo,ProductionCategory,Status from ProductionCategoryMaster where status='A' order by ProductionCategory asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vProductionCategory = oRs.GetRows()
Else
    vProductionCategory = "Fail"
End If
oRs.Close

vResponse.Add vProductionCategory, "ProductionCategory"
vResponse.Add False, Key:="error"
Set ListProductionCategory = vResponse
Set oRs = Nothing
Exit Function

ListProductionCategoryErrHandler:
Set oRs = Nothing
ListProductionCategory = "ERROR: " & Err.Description
End Function

Public Function ListConsolidationType() As Variant
Dim vConsolidationType As Variant
Dim vResponse As New Collection
On Error GoTo ListConsolidationTypeErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select CTNo,ConsolidationType,Status from ConsolidationTypeMaster where status='A' order by ConsolidationType asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vConsolidationType = oRs.GetRows()
Else
    vConsolidationType = "Fail"
End If
oRs.Close

vResponse.Add vConsolidationType, "ConsolidationType"
vResponse.Add False, Key:="error"
Set ListConsolidationType = vResponse
Set oRs = Nothing
Exit Function

ListConsolidationTypeErrHandler:
Set oRs = Nothing
ListConsolidationType = "ERROR: " & Err.Description
End Function

Public Function ListDoughType() As Variant
'obtaining ing list
Dim vDoughType As Variant
Dim vResponse As New Collection
On Error GoTo ListDoughTypeErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select * from DoughTypeMaster where status='A' order by 1 asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vDoughType = oRs.GetRows()
Else
    vDoughType = "Fail"
End If
oRs.Close

vResponse.Add vDoughType, "DoughType"
vResponse.Add False, Key:="error"
Set ListDoughType = vResponse
Set oRs = Nothing
Exit Function

ListDoughTypeErrHandler:
Set oRs = Nothing
ListDoughType = "ERROR: " & Err.Description
End Function

Function BinarySearch(arr As Variant, search As Variant, Optional lastEl As Variant) As Long
    Dim index As Long
    Dim first As Long
    Dim last As Long
    Dim middle As Long
    Dim inverseOrder As Boolean
    
    ' account for optional arguments
    If IsMissing(lastEl) Then lastEl = UBound(arr)
    
    first = LBound(arr)
    last = lastEl

    ' deduct direction of sorting
    inverseOrder = (arr(first) > arr(last))

    ' assume searches failed
    BinarySearch = first - 1
    
    Do
        middle = (first + last) \ 2
        If arr(middle) = search Then
            BinarySearch = middle
            Exit Do
        ElseIf ((arr(middle) < search) Xor inverseOrder) Then
            first = middle + 1
        Else
            last = middle - 1
        End If
    Loop Until first > last
End Function

Function TwoDimensionalArrayBinarySearch(arr As Variant, search As Variant, Optional lastEl As Variant) As Long
    Dim index As Long
    Dim first As Long
    Dim last As Long
    Dim middle As Long
    Dim inverseOrder As Boolean
    
    ' account for optional arguments
    If IsMissing(lastEl) Then lastEl = UBound(arr, 2)
    
    first = LBound(arr, 2)
    last = lastEl

    ' deduct direction of sorting
    inverseOrder = (arr(first) > arr(last))

    ' assume searches failed
    TwoDimensionalArrayBinarySearch = first - 1
    
    Do
        middle = (first + last) \ 2
        If arr(middle) = search Then
            TwoDimensionalArrayBinarySearch = middle
            Exit Do
        ElseIf ((arr(middle) < search) Xor inverseOrder) Then
            first = middle + 1
        Else
            last = middle - 1
        End If
    Loop Until first > last
End Function

Public Sub BubbleSort(ByRef pvarArray As Variant)
On Error Resume Next

Dim i As Long
Dim iMin As Long
Dim iMax As Long
Dim varSwap As Variant
Dim blnSwapped As Boolean

iMin = LBound(pvarArray)
iMax = UBound(pvarArray) - 1
Do
    blnSwapped = False
    For i = iMin To iMax
        If pvarArray(i) > pvarArray(i + 1) Then
            varSwap = pvarArray(i)
            pvarArray(i) = pvarArray(i + 1)
            pvarArray(i + 1) = varSwap
            blnSwapped = True
        End If
    Next
    iMax = iMax - 1
Loop Until Not blnSwapped
End Sub

Public Sub TestSort()
    Dim lngRow As Long
    Dim lngCol As Long
    Dim MyArray() As Variant
    Dim strFind As String
    
    Randomize Timer
    Debug.Print "Row, Column (Generally a fixed array)"
    ReDim MyArray(10 To 20, 2 To 3)
    For lngRow = LBound(MyArray, 1) To UBound(MyArray, 1)
        MyArray(lngRow, 2) = Chr(Int((90 - 65 + 1) * Rnd + 65))
        MyArray(lngRow, 3) = Int((199 - 100 + 1) * Rnd + 100)
        Debug.Print lngRow & ": " & MyArray(lngRow, 2) & ", " & MyArray(lngRow, 3)
    Next
    QuickSort MyArray, 2, 2
    Debug.Print "Sorted"
    For lngRow = LBound(MyArray, 1) To UBound(MyArray, 1)
        Debug.Print lngRow & ": " & MyArray(lngRow, 2) & ", " & MyArray(lngRow, 3)
    Next
    Debug.Print "Array index of first 'Q': " & BinarySearch2(MyArray, 2, 2, "Q")
    Debug.Print
    Debug.Print "Column, Row (Generally a dynamic array)"
    ReDim MyArray(2 To 3, 10 To 20)
    For lngRow = LBound(MyArray, 2) To UBound(MyArray, 2)
        MyArray(2, lngRow) = Chr(Int((90 - 65 + 1) * Rnd + 65))
        MyArray(3, lngRow) = Int((199 - 100 + 1) * Rnd + 100)
        Debug.Print lngRow & ": " & MyArray(2, lngRow) & ", " & MyArray(3, lngRow)
    Next
    QuickSort MyArray, 1, 2
    Debug.Print "Sorted"
    For lngRow = LBound(MyArray, 2) To UBound(MyArray, 2)
        Debug.Print lngRow & ": " & MyArray(2, lngRow) & ", " & MyArray(3, lngRow)
    Next
    Debug.Print "Array index of first 'Q': " & BinarySearch2(MyArray, 1, 2, "Q")
    Erase MyArray
End Sub

' Sort a 2-dimensional array on either dimension
' Omit plngLeft & plngRight; they are used internally during recursion
' Sample usage to sort on column 4
' Dim MyArray(1 to 1000, 1 to 5) As Long
' QuickSort MyArray, 2, 4
' Dim MyArray(1 to 5, 1 to 1000) As Long
' QuickSort MyArray, 1, 4
Public Sub QuickSort(pArray As Variant, pbytDimension As Byte, plngColumn As Long, Optional ByVal plngLeft As Long, Optional ByVal plngRight As Long)
    Dim i As Long
    Dim lngFirst As Long
    Dim lngLast As Long
    Dim vFirst As Variant
    Dim vMid As Variant
    Dim vLast As Variant
    Dim lDim(1 To 2) As Long
    Dim bytCol As Byte
    Dim bytRow As Byte
    
    bytRow = -pbytDimension + 3
    bytCol = pbytDimension
    If plngRight = 0 Then
        plngLeft = LBound(pArray, bytRow)
        plngRight = UBound(pArray, bytRow)
    End If
    lngFirst = plngLeft
    lngLast = plngRight
    lDim(bytRow) = (plngLeft + plngRight) \ 2
    lDim(bytCol) = plngColumn
    vMid = pArray(lDim(1), lDim(2))
    Do
        lDim(bytRow) = lngFirst
        lDim(bytCol) = plngColumn
        Do While pArray(lDim(1), lDim(2)) < vMid And lngFirst < plngRight
            lngFirst = lngFirst + 1
            lDim(bytRow) = lngFirst
        Loop
        lDim(bytRow) = lngLast
        Do While vMid < pArray(lDim(1), lDim(2)) And lngLast > plngLeft
            lngLast = lngLast - 1
            lDim(bytRow) = lngLast
        Loop
        If lngFirst <= lngLast Then
            For i = LBound(pArray, bytCol) To UBound(pArray, bytCol)
                lDim(bytCol) = i
                lDim(bytRow) = lngFirst
                vFirst = pArray(lDim(1), lDim(2))
                lDim(bytRow) = lngLast
                vLast = pArray(lDim(1), lDim(2))
                pArray(lDim(1), lDim(2)) = vFirst
                lDim(bytRow) = lngFirst
                pArray(lDim(1), lDim(2)) = vLast
            Next
            lngFirst = lngFirst + 1
            lngLast = lngLast - 1
        End If
    Loop Until lngFirst > lngLast
    If plngLeft < lngLast Then QuickSort pArray, pbytDimension, plngColumn, plngLeft, lngLast
    If lngFirst < plngRight Then QuickSort pArray, pbytDimension, plngColumn, lngFirst, plngRight
End Sub

' Simple binary search. Be sure array is sorted first.
' Sample usage to locate ID from column 1
' Dim MyArray(1 to 1000, 1 to 5) As Long
' lngIndex = BinarySearch(MyArray, 2, 1, lngIDToFind)
' Dim MyArray(1 to 5, 1 to 1000) As Long
' lngIndex = BinarySearch(MyArray, 1, 1, lngIDToFind)
Public Function BinarySearch2(pArray As Variant, pbytDimension As Byte, plngColumn As Long, pvarFind As Variant) As Long
    Dim lngFirst As Long
    Dim lngMid As Long
    Dim lngLast As Long
    Dim lDim(1 To 2) As Long
    Dim bytCol As Byte
    Dim bytRow As Byte
    
    bytRow = -pbytDimension + 3
    bytCol = pbytDimension
    lDim(bytCol) = plngColumn
    BinarySearch2 = -1
    lngMid = -1
    lngFirst = LBound(pArray, bytRow)
    lngLast = UBound(pArray, bytRow)
    Do While lngFirst <= lngLast
        lngMid = (lngFirst + lngLast) \ 2
        lDim(bytRow) = lngMid
        If pArray(lDim(1), lDim(2)) > pvarFind Then
            lngLast = lngMid - 1
        ElseIf pArray(lDim(1), lDim(2)) < pvarFind Then
            lngFirst = lngMid + 1
        Else
            Exit Do
        End If
    Loop
    ' Make sure this is the first match in array
    Do While lngMid > lngFirst
        lDim(bytRow) = lngMid - 1
        If pArray(lDim(1), lDim(2)) <> pvarFind Then Exit Do
        lngMid = lngMid - 1
    Loop
    ' Set return value if match was found
    If lngMid > -1 Then
        lDim(bytRow) = lngMid
        If pArray(lDim(1), lDim(2)) = pvarFind Then BinarySearch2 = lngMid
    End If
End Function


Public Function ListMotherIngradient() As Variant
Dim vMotherIngradient As Variant
Dim vResponse As New Collection
On Error GoTo ListMotherIngradientErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select * from IngredientMaster where Status='A' and IsMotherIngradient=1 order by IName asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vMotherIngradient = oRs.GetRows()
Else
    vMotherIngradient = "Fail"
End If
oRs.Close

vResponse.Add vMotherIngradient, "MotherIngradient"
vResponse.Add False, Key:="error"
Set ListMotherIngradient = vResponse
Set oRs = Nothing
Exit Function

ListMotherIngradientErrHandler:
Set oRs = Nothing
ListMotherIngradient = "ERROR: " & Err.Description
End Function

Public Function ListDeliveryTypes() As Variant
'obtaining ing list
Dim vDeliveryTypes As Variant
Dim vResponse As New Collection
On Error GoTo ListDeliveryTypesErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select * from TripMaster where status='A' order by 1 asc"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vDeliveryTypes = oRs.GetRows()
Else
    vDeliveryTypes = "Fail"
End If
oRs.Close

vResponse.Add vDeliveryTypes, "DeliveryTypes"
vResponse.Add False, Key:="error"
Set ListDeliveryTypes = vResponse
Set oRs = Nothing
Exit Function
ListDeliveryTypesErrHandler:
Set oRs = Nothing
ListDeliveryTypes = "ERROR: " & Err.Description
End Function

Public Function GetBakingOrder() As Variant

Dim vBakingOrder As Variant
Dim vResponse As New Collection
On Error GoTo GetBakingOrderErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "SELECT * FROM BakingOrder ORDER BY ID"

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vBakingOrder = oRs.GetRows()
Else
    vBakingOrder = "Fail"
End If
oRs.Close

vResponse.Add vBakingOrder, "BakingOrder"
vResponse.Add False, Key:="error"
Set GetBakingOrder = vResponse
Set oRs = Nothing
Exit Function
GetBakingOrderErrHandler:
Set oRs = Nothing
GetBakingOrder = "ERROR: " & Err.Description
End Function

Public Function SaveInvoiceFooter(ByVal footerText As String) As Variant

Dim vResponse As New Collection

On Error GoTo SaveInvoiceFooterErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

  vSql = "exec InsertFooterText '" & footerText & "'"
  vResponse.Add "OK", "Sucess"
oDB.Execute (vSql)

oDB.Close

Set oDB = Nothing
vResponse.Add False, Key:="error"

Set SaveInvoiceFooter = vResponse


Exit Function
SaveInvoiceFooterErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
SaveInvoiceFooter = "ERROR: " & Err.Description
End Function





Public Function GetFooterText() As Variant
Dim vResponse As New Collection
Dim vFooter As Variant

On Error GoTo ErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

Let vSql = "SELECT TOP 1 FooterText FROM InvoiceFooter Order by CreatedDate DESC"
'vSql = "EXEC GetCustomerManagementMessageByCNo " & nCustNo

oRs.Open vSql, oDB, adOpenKeyset, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vFooter = oRs.GetRows()
Else
    vFooter = "Fail"
End If
oRs.Close

vResponse.Add vFooter, "FooterMessage"
vResponse.Add False, Key:="error"
Set GetFooterText = vResponse

oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Exit Function

ErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
GetFooterText = "ERROR: " & Err.Description
End Function

Public Function LoginInternal(ByVal vpasscode As String) As Variant
'Login authenticity code using email address and password

Dim vResponse As New Collection
Dim vSucess As Variant

On Error GoTo LoginInternalErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "select * from LoginInternalUsers where status='A' and passcode= '" & vpasscode & "'"
oRs.Open vSql, oDB, adOpenDynamic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vSucess = "True"
Else
    vSucess = "Fail"
End If
oRs.Close
oDB.Close

vResponse.Add vSucess, "Sucess"
vResponse.Add False, Key:="error"
Set LoginInternal = vResponse

Set oRs = Nothing
Set oDB = Nothing
Exit Function

LoginInternalErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
LoginInternal = "ERROR: " & Err.Description
End Function

Public Function GetPriceTypeName(ByVal vPTNo As Long) As Variant
'obtaining ing list
Dim vPriceTypeName As Variant
Dim vResponse As New Collection
On Error GoTo GetPriceTypeNameErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "select PTName from dbo.PriceTypeMaster where PTno=" & vPTNo

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vPriceTypeName = oRs(0).Value
Else
    vPriceTypeName = 0
End If
oRs.Close

vResponse.Add vPriceTypeName, "PriceTypeName"
vResponse.Add False, Key:="error"
Set GetPriceTypeName = vResponse
Set oRs = Nothing
Exit Function

GetPriceTypeNameErrHandler:
Set oRs = Nothing
GetPriceTypeValues = "ERROR: " & Err.Description
End Function

Public Function GetGroupName(ByVal vGno As Long) As Variant
'obtaining ing list
Dim vGroupName As Variant
Dim vResponse As New Collection
On Error GoTo GetGroupNameErrHandler

Set oRs = New ADODB.Recordset

Let vSql = "Select GName from GroupMaster where Status = 'A' and GNo=" & vGno

oRs.Open vSql, vdbConn, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vGroupName = oRs(0).Value
Else
    vGroupName = 0
End If
oRs.Close

vResponse.Add vGroupName, "GroupName"
vResponse.Add False, Key:="error"
Set GetGroupName = vResponse
Set oRs = Nothing
Exit Function

GetGroupNameErrHandler:
Set oRs = Nothing
GetGroupName = "ERROR: " & Err.Description
End Function
