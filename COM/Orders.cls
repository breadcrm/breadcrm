VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Orders"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private objNewmail As CDONTS.NewMail
Private oDB As ADODB.Connection
Private oRs As ADODB.Recordset
Private oRs1 As ADODB.Recordset
Private oRs2 As ADODB.Recordset

Private vSql As Variant
Private vRecordCount As Integer
Private vPagecount As Integer
'Const vdbConn = "DSN=bakery;UID=bakeryuser;PWD=bakery2003"

Public Function SetEnvironment(ByVal env As String) As Variant
    ReadINI env
End Function

Public Function Display_TypicalOrderList(ByVal vPageSize As Long, ByVal vsessionpage As Long, ByVal strCustName As String, ByVal strCustNo As String, ByVal strDate As String) As Variant
'obtaining Typical order list
Dim vResponse As New Collection
Dim arTypicalOrder As Variant
Dim vRecordCount As Long
Dim vPagecount As Long
Dim vsqlCount As String

On Error GoTo ErrorHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "select ordNo,Cno,Cname = (select cname from customermaster where cno=a.cno),Tname =(select tname from tripmaster where tno=a.tno),SOrder=(select sorder from customermaster where cno=a.cno) from Temp_ordermaster a where convert(varchar,orddate,103) = convert(varchar,'" & strDate & "',103) "
vsqlCount = "select count(1) from Temp_ordermaster a where convert(varchar,orddate,103) = convert(varchar,'" & strDate & "',103)"

   
If Trim(strCustName) <> "" Then
  vSql = vSql & " and (select cname from customermaster where cno=a.cno) like '%" & strCustName & "%'"
  vsqlCount = vsqlCount & " and (select cname from customermaster where cno=a.cno) like '%" & strCustName & "%'"
End If

If Trim(strCustNo) <> "" Then
  If IsNumeric(strCustNo) Then
    vSql = vSql & " and a.cno = " & strCustNo
    vsqlCount = vsqlCount & " and a.cno = " & strCustNo
  End If
End If

vSql = vSql & " Order by 3"
oRs.Open vsqlCount, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
  vRecordCount = oRs.Fields(0)
Else
  vRecordCount = 0
End If
oRs.Close

oRs.PageSize = vPageSize
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
  vPagecount = Round(vRecordCount / vPageSize + 0.49)
  oRs.AbsolutePage = vsessionpage
  arTypicalOrder = oRs.GetRows(vPageSize)
Else
  vRecordCount = 0
  vPagecount = 0
  arTypicalOrder = "Fail"
End If

oRs.Close
oDB.Close

Set oRs = Nothing
Set oDB = Nothing

vResponse.Add vRecordCount, "Recordcount"
vResponse.Add vPagecount, "Pagecount"
vResponse.Add arTypicalOrder, "TypicalOrder"
Set Display_TypicalOrderList = vResponse
Exit Function

ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Display_TypicalOrderList = "ERROR: " & Err.Description
End Function
Public Function Display_TypicalOrderDetails(ByVal vOrderNo As Long) As Variant
'obtaining Typical order list
Dim vResponse As New Collection
Dim arTypicalOrder As Variant
Dim arTypicalOrderDetails As Variant
Dim arTempOrderMasterDeliveryCharge As Variant

On Error GoTo ErrorHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

        
        
       '" Week_VNo = (select Week_VNo from dbo.fnCustomerMaster(a.OrdDate) where cno = a.cno), " & _
       '" WeekEnd_VNo= (select WeekEnd_VNo from dbo.fnCustomerMaster(a.OrdDate) where cno=a.cno)," & _

vSql = "select ordNo,Orddate,datename(dw,Orddate),Tname=(select tname from tripmaster where tno=a.tno),smporder= case when SmpOrder='Y' then 'Yes' else 'No' end, " & _
       " CName= (select CName from customermaster where cno=a.cno), " & _
       " OCCode= (select cno from customermaster where cno=a.cno), " & _
       " CAddressI= (select CAddressI from customermaster where cno=a.cno), " & _
       " CAddressII= (select CAddressII from customermaster where cno=a.cno), " & _
       " CTown= (select CTown from customermaster where cno=a.cno), " & _
       " CPostCode= (select CPostCode from customermaster where cno=a.cno), " & _
       " CTelNo= (select CTelNo from customermaster where cno=a.cno), " & _
       " C_Name= (select C_Name from customermaster where cno=a.cno), " & _
       " C_TelNo= (select C_TelNo from customermaster where cno=a.cno), " & _
       " D_Name= (select D_Name from customermaster where cno=a.cno), " & _
       " D_TelNo= (select D_TelNo from customermaster where cno=a.cno)," & _
       " AmdDate, " & _
       " pono, " & _
       " POrder = (select POrder from customermaster where cno=a.cno), " & _
       " Week_VNo = (select cast(VNo_Mon as varchar(8)) + ',' + cast(VNo_Tue as varchar(8))+ ',' + cast(VNo_Wed as varchar(8))+ ',' + cast(VNo_Thu as varchar(8))+ ',' + cast(VNo_Fri as varchar(8))  from customermaster where cno=a.cno) , " & _
       " WeekEnd_VNo= (select cast(VNo_Sat as varchar(8))  + ',' + cast(VNo_Sun as varchar(8))  from customermaster where cno=a.cno), " & _
       " Comment= (select Comment from customermaster where cno=a.cno) " & _
       " ,GNo= (select GNo from customermaster where cno=a.cno) " & _
       " ,PricelessInvoice = (select PricelessInvoice from customermaster where cno=a.cno) " & _
       " ,MinimumOrder = (select MinimumOrder from customermaster where cno=a.cno) " & _
       " ,MinimumGroupOrder=(select MinimumGroupOrder from GroupMaster x, customermaster y where x.GNo=y.GNo and y.CNo=a.cno) " & _
       " ,deliverychargepno = (select deliverychargepno from customermaster where cno=a.cno), AccountManager= (select AccountManager from customermaster where cno=a.cno)" & _
       " from Temp_ordermaster a " & _
       " where ordno = " & vOrderNo

'commented by selva on 19 June 2014
',IsStopStatus

oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
  arTypicalOrder = oRs.GetRows()
Else
  arTypicalOrder = "Fail"
End If
oRs.Close


vSql = "select a.pno,Pname = (select pname from productmaster where pno=a.pno),TQty = (select qty from Standingorder where cno=b.cno and pno=a.pno and wno=b.wno and tno=b.tno and status='A') ,a.Qty,a.status, a.Price,LateProd = (select LateProd from productmaster where pno=a.pno),lastthreemonths=(select COUNT(*) from OrderMaster x INNER JOIN OrderDetail y on x.OrdNo=y.OrdNo where x.cno=b.CNo and x.OrdDate>=DATEADD(MONTH, -3, GETDATE())  and pno=a.PNo),MulProd=(select MulProd from productmaster where pno=a.pno),ismultipleproduct=(select ismultipleproduct from productmaster where pno=a.pno),multiproductqty=(select sum(y.Qty*x.MulProd) from productmaster x,MultipleProducts y where x.pno=y.MultiPNo and y.pno=a.pno),LatePro2 = dbo.Fn_Chec48ProductOverMultiProduct(a.pno)  from Temp_orderdetail a,Temp_ordermaster b where  pno not in(select pno from productmaster where status='D') and  a.ordno=b.ordno and a.ordno = " & vOrderNo & " order by 2 asc"
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
  arTypicalOrderDetails = oRs.GetRows()
Else
  arTypicalOrderDetails = "Fail"
End If
oRs.Close

vSql = "select NotApplyDeliveryCharge from Temp_OrderMaster_deliverycharge where TempOrderNo=" & vOrderNo
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
  arTempOrderMasterDeliveryCharge = oRs.GetRows()
Else
  arTempOrderMasterDeliveryCharge = "Fail"
End If
oRs.Close

oDB.Close
Set oRs = Nothing
Set oDB = Nothing

vResponse.Add arTypicalOrder, "TypicalOrder"
vResponse.Add arTypicalOrderDetails, "TypicalOrderDetails"
vResponse.Add arTempOrderMasterDeliveryCharge, "TempOrderMasterDeliveryCharge"

Set Display_TypicalOrderDetails = vResponse
Exit Function

ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Display_TypicalOrderDetails = "ERROR: " & Err.Description
End Function
Public Function Update_TypicalOrderDetails(ByVal vOrderNo As Long, ByVal arUpdate As Variant, ByVal strPo As String, ByVal vnotapplydeliverycharge As Integer) As String
'Updating Temp typical order
Dim vResponse As New Collection
Dim i As Integer
Dim lngCNo, vCNo As Long
Dim lngOutOfLondon As Boolean
Dim lngPTNo As Long
Dim lngMinimumOrder As Long
Dim lngMinimumGroupOrder As Long
Dim lngdelcharge As Long
lngdelcharge = 0

Dim lngdelchargevalue As Long
lngdelchargevalue = 0

Dim dblPrice As Double
Dim dbldelPrice As Double
Dim dblcurrentdelPrice As Double
Dim dblcurrentdelPricezero As Long
Dim strPNoDelivery As String
dbldelPrice = 0
On Error GoTo ErrorHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
Set oRs1 = New ADODB.Recordset
Set oRs2 = New ADODB.Recordset
oDB.Open vdbConn
oDB.CommandTimeout = 6000
dblcurrentdelPricezero = 0
If IsArray(arUpdate) Then
  For i = 0 To UBound(arUpdate, 2)
    vSql = "select * from Temp_OrderDetail where OrdNo = " & vOrderNo & " and Pno = '" & arUpdate(0, i) & "'"
    oRs.Open vSql, oDB, adOpenKeyset, adLockPessimistic
    If Not (oRs.BOF And oRs.EOF) Then
        If ((arUpdate(0, i) = "99-91-3523" Or arUpdate(0, i) = "99-91-1092") And arUpdate(1, i) = 0) Then
            dblcurrentdelPricezero = 1
        End If
      If Trim(oRs("Qty")) <> Trim(arUpdate(1, i)) Then
        If IsNumeric(arUpdate(1, i)) Then
          oRs("Qty") = arUpdate(1, i)
          oRs("Status") = "Y"
          oRs.Update
          'Multi Product
          vSql = "Select cno from Temp_OrderMaster where OrdNo = " & vOrderNo
          oRs1.Open vSql, oDB, adOpenStatic, adLockReadOnly
          If Not (oRs1.BOF And oRs1.EOF) Then
                vCNo = oRs1(0)
                vSql = "SELECT * FROM MultipleProducts WHERE PNo='" & arUpdate(0, i) & "'"
                oRs2.Open vSql, oDB, adOpenStatic, adLockReadOnly
                If Not (oRs2.BOF And oRs2.EOF) Then
                    vSql = "delete from temp_OrderDetailForMultipleProduct where OrdNo=" & vOrderNo & " and MultiPNo='" & arUpdate(0, i) & "'"
                    oDB.Execute (vSql)
                    Do While Not oRs2.EOF
                        vSql = "exec dbo.sp_InsertTemp_OrderDetailForMultipleProducts " & vOrderNo & ",'" & oRs2("MultiPNo") & "','" & arUpdate(0, i) & "'," & arUpdate(1, i) & "," & oRs2("Qty") & "," & vCNo
                        oDB.Execute (vSql)
                        oRs2.MoveNext
                    Loop
                    
                End If
                oRs2.Close
            End If
            oRs1.Close
          'vSql = "exec dbo.sp_UpdateTemp_OrderDetailForMultipleProduct " & vOrderNo & ",'" & arUpdate(0, i) & "'," & arUpdate(1, i)
          'oDB.Execute (vSql)
        End If
      End If
    End If
    oRs.Close
  Next
End If

vSql = " Update Temp_OrderMaster " & _
       " Set Status = 'C'," & _
       " PONo = '" & Replace(strPo, "'", "''") & "'," & _
       " AmdDate = convert(datetime,'" & Day(Date) & "/" & Month(Date) & "/" & Year(Date) & "',103) " & _
       " Where OrdNo = " & vOrderNo
       
oDB.Execute vSql, , adCmdText

'Add Automatic Delivery Charges Begin

  If (vnotapplydeliverycharge = 0) Then
        
  'vSql = "select b.CNo,b.OutOfLondon,b.PTNo,isnull(b.MinimumOrder,0) MinimumOrder,a.Price,MinimumGroupOrder=isnull((select MinimumGroupOrder from GroupMaster x, customermaster y where x.GNo=y.GNo and y.CNo=a.cno),0) from Temp_OrderMaster a, customermaster b Where a.cno = b.cno And a.ordno=" & vOrderNo
  vSql = "select b.CNo,b.deliverychargepno,b.PTNo,b.MinimumOrder,a.Price,MinimumGroupOrder=(select MinimumGroupOrder from GroupMaster x, customermaster y where x.GNo=y.GNo and y.CNo=a.cno) from Temp_OrderMaster a, customermaster b Where a.cno = b.cno And a.ordno=" & vOrderNo
  oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
  If Not (oRs.BOF And oRs.EOF) Then
    lngCNo = oRs("CNo").Value
    strPNoDelivery = oRs("deliverychargepno").Value
    lngPTNo = oRs("PTNo").Value
    'lngMinimumOrder = oRs("MinimumOrder").Value
    'lngMinimumGroupOrder = oRs("MinimumGroupOrder").Value
    If (IsNull(oRs("MinimumOrder").Value) And IsNull(oRs("MinimumGroupOrder").Value)) Then
        lngdelchargevalue = 1
    End If
    
    If (strPNoDelivery <> "") Then
        If IsNull(oRs("MinimumOrder").Value) Then
            If IsNull(oRs("MinimumGroupOrder").Value) Then
                lngdelcharge = 1
                
            Else
                lngMinimumOrder = oRs("MinimumGroupOrder").Value
            End If
        Else
            lngMinimumOrder = oRs("MinimumOrder").Value
        End If
        
        dblPrice = oRs("Price").Value
        
    '    If (lngOutOfLondon) Then
    '        strPNoDelivery = "99-91-3523"
    '    Else
    '        strPNoDelivery = "99-91-1092"
    '    End If
        
        vSql = "select Price from ProductPriceMaster where PNo='" & strPNoDelivery & "' and PTNo=" & lngPTNo
        oRs2.Open vSql, oDB, adOpenStatic, adLockReadOnly
        If Not (oRs2.BOF And oRs2.EOF) Then
            dbldelPrice = oRs2("Price").Value
        End If
        oRs2.Close
        
        'vSql = "select Price from Temp_OrderDetail where Status='Y' and PNo='" & strPNoDelivery & "' and OrdNo=" & vOrderNo
        vSql = "select Price from Temp_OrderDetail where PNo='" & strPNoDelivery & "' and OrdNo=" & vOrderNo
        oRs2.Open vSql, oDB, adOpenStatic, adLockReadOnly
        If ((Not (oRs2.BOF And oRs2.EOF)) And dblcurrentdelPricezero = 0) Then
            dblcurrentdelPrice = oRs2("Price").Value
        Else
            dblcurrentdelPrice = 0
        End If
        oRs2.Close
            
        If ((((dblPrice - dblcurrentdelPrice) < lngMinimumOrder)) And dblPrice > 0 And lngdelcharge = 0) Then
        
            vSql = "select OrdNo from Temp_OrderDetail where OrdNo=" & vOrderNo & " and Pno='" & strPNoDelivery & "'"
            oRs1.Open vSql, oDB, adOpenStatic, adLockReadOnly
            If Not (oRs1.BOF And oRs1.EOF) Then
                vSql = "select * from Temp_OrderDetail where OrdNo=" & vOrderNo & " and Pno='" & strPNoDelivery & "'"
                oRs2.Open vSql, oDB, adOpenDynamic, adLockOptimistic
                oRs2.Fields("Price") = dbldelPrice
                oRs2("Status") = "Y"
                oRs2("Qty") = 1
                oRs2.Update
                oRs2.Close
            Else
                oRs2.Open "Temp_OrderDetail", oDB, adOpenDynamic, adLockOptimistic
                oRs2.AddNew
                oRs2("OrdNo") = vOrderNo
                oRs2("PNo") = strPNoDelivery
                oRs2("Qty") = 1
                oRs2("Price") = dbldelPrice
                oRs2("Status") = "Y"
                oRs2.Update
                oRs2.Close
            End If
        Else
            vSql = "select * from Temp_OrderDetail where OrdNo=" & vOrderNo & " and Pno='" & strPNoDelivery & "'"
            oRs2.Open vSql, oDB, adOpenDynamic, adLockOptimistic
            If (Not oRs2.EOF) Then
                oRs2.Fields("Qty") = 0
                oRs2.Fields("Price") = 0
                oRs2.Fields("Status") = "N"
                oRs2.Update
            End If
            oRs2.Close
      End If
    Else
        If (lngdelchargevalue <> 1) Then
        vSql = "update temp_orderdetail set Qty=0 where OrdNo=" & vOrderNo & " and Pno in ('99-91-3523','99-91-1092')"
        oDB.Execute (vSql)
        End If
    End If
    
    End If
    oRs.Close
Else
    vSql = "update temp_orderdetail set Qty=0 where OrdNo=" & vOrderNo & " and Pno in ('99-91-3523','99-91-1092')"
    oDB.Execute (vSql)
End If

'Add Automatic Delivery Charges End

oDB.Close
Set oRs = Nothing
Set oRs1 = Nothing
Set oRs2 = Nothing
Set oDB = Nothing
Update_TypicalOrderDetails = "OK"
Exit Function

ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Update_TypicalOrderDetails = "ERROR: " & Err.Description
End Function
Public Function Display_AdditionalTypicalOrderList(ByVal vPageSize As Long, ByVal vsessionpage As Long, ByVal strProdCode As String, ByVal strProdName As String, ByVal vOrderNo As Long) As Variant
'obtaining Typical order list
Dim vResponse As New Collection
Dim arProduct As Variant
Dim vRecordCount As Long
Dim vPagecount As Long
Dim vsqlCount As String

On Error GoTo ErrorHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

If vOrderNo > 0 Then
  vSql = "select pno,Pname,LateProd from productmaster a where status='A' and not exists(select pno  from  Temp_OrderDetail where  pno=a.pno and status = 'Y' and ordno = " & vOrderNo & ") "
  vsqlCount = "select count(1) from productmaster a where status='A' and not exists(select pno  from  Temp_OrderDetail where  pno=a.pno and status = 'Y' and ordno = " & vOrderNo & ") "
Else
  vSql = "select pno,Pname,LateProd from productmaster a where status='A' "
  vsqlCount = "select count(1) from productmaster a where status='A' "
End If

If Trim(strProdCode) <> "" Then
  vSql = vSql & " and Pno like '%" & strProdCode & "%'"
  vsqlCount = vsqlCount & " and Pno like '%" & strProdCode & "%'"
End If
   
If Trim(strProdName) <> "" Then
  vSql = vSql & " and Pname like '%" & strProdName & "%'"
  vsqlCount = vsqlCount & " and Pname like '%" & strProdName & "%'"
End If

vSql = vSql & " Order by 2"

oRs.Open vsqlCount, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
  vRecordCount = oRs.Fields(0)
Else
  vRecordCount = 0
End If
oRs.Close

oRs.PageSize = vPageSize
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
  vPagecount = Round(vRecordCount / vPageSize + 0.49)
  oRs.AbsolutePage = vsessionpage
  arProduct = oRs.GetRows(vPageSize)
Else
  vRecordCount = 0
  vPagecount = 0
  arProduct = "Fail"
End If

oRs.Close
oDB.Close

Set oRs = Nothing
Set oDB = Nothing

vResponse.Add vRecordCount, "Recordcount"
vResponse.Add vPagecount, "Pagecount"
vResponse.Add arProduct, "Product"
Set Display_AdditionalTypicalOrderList = vResponse
Exit Function

ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Display_AdditionalTypicalOrderList = "ERROR: " & Err.Description
End Function
Public Function Update_AdditionalTypicalOrderItem(ByVal vOrderNo As Long, ByVal strProdCode As String, ByVal vQty As Double) As String
'Updating Temp typical order
Dim vResponse As New Collection
Dim oRs1 As Variant
Dim i As Integer
On Error GoTo ErrorHandler
Dim vCNo As Long

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
Set oRs1 = New ADODB.Recordset
Set oRs2 = New ADODB.Recordset

oDB.Open vdbConn
oDB.CommandTimeout = 6000

vSql = "Select * from Temp_OrderDetail where OrdNo = " & vOrderNo & " and pno = '" & strProdCode & "'"
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vSql = "Update Temp_OrderDetail set Qty = " & vQty & ",Status = 'Y' where OrdNo = " & vOrderNo & " and pno = '" & strProdCode & "'"
    oDB.Execute vSql, , adCmdText
    
    'Multi Product
    vSql = "exec dbo.sp_UpdateTemp_OrderDetailForMultipleProduct " & vOrderNo & ",'" & strProdCode & "'," & vQty
    oDB.Execute (vSql)
Else
    vSql = "Insert into Temp_OrderDetail(OrdNo,PNo,Qty,Status) Values(" & vOrderNo & ",'" & strProdCode & "'," & vQty & ",'Y')"
    oDB.Execute vSql, , adCmdText
    'Multi Product
    vSql = "Select cno from Temp_OrderMaster where OrdNo = " & vOrderNo
    oRs1.Open vSql, oDB, adOpenStatic, adLockReadOnly
    If Not (oRs1.BOF And oRs1.EOF) Then
        vCNo = oRs1(0)
        vSql = "SELECT * FROM MultipleProducts WHERE PNo='" & strProdCode & "'"
        oRs2.Open vSql, oDB, adOpenStatic, adLockReadOnly
        If Not (oRs2.BOF And oRs2.EOF) Then
            vSql = "delete from temp_OrderDetailForMultipleProduct where OrdNo=" & vOrderNo & " and MultiPNo='" & strProdCode & "'"
            oDB.Execute (vSql)
            Do While Not oRs2.EOF
                vSql = "exec dbo.sp_InsertTemp_OrderDetailForMultipleProducts " & vOrderNo & ",'" & oRs2("MultiPNo") & "','" & strProdCode & "'," & vQty & "," & oRs2("Qty") & "," & vCNo
                oDB.Execute (vSql)
                oRs2.MoveNext
            Loop
        End If
        oRs2.Close
    End If
    oRs1.Close
End If
oRs.Close
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Update_AdditionalTypicalOrderItem = "OK"
Exit Function

ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Update_AdditionalTypicalOrderItem = "ERROR: " & Err.Description
End Function
Public Function Display_FindTypicalOrderList(ByVal vPageSize As Long, ByVal vsessionpage As Long, ByVal strCustName As String, ByVal strCustNo As String, ByVal vSDate As String, ByVal vEDate As String) As Variant
'obtaining Typical order list
Dim vResponse As New Collection
Dim arOrder As Variant
Dim vRecordCount As Long
Dim vPagecount As Long
Dim vsqlCount As String

On Error GoTo ErrorHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "select ordNo,OrdDate,Cno,Cname = (select cname from customermaster where cno=a.cno),Tname =(select tname from tripmaster where tno=a.tno),SOrder=(select sorder from customermaster where cno=a.cno) from Temp_ordermaster a where orddate >= convert(datetime,'" & vSDate & "',103) and orddate< convert(datetime,'" & vEDate & "',103)+ 1"
vsqlCount = "select count(1) from Temp_ordermaster a where orddate >= convert(datetime,'" & vSDate & "',103) and orddate< convert(datetime,'" & vEDate & "',103)+ 1"
   
If Trim(strCustName) <> "" Then
  vSql = vSql & " and (select cname from customermaster where cno=a.cno) like '%" & strCustName & "%'"
  vsqlCount = vsqlCount & " and (select cname from customermaster where cno=a.cno) like '%" & strCustName & "%'"
End If

If Trim(strCustNo) <> "" Then
  If IsNumeric(strCustNo) Then
    vSql = vSql & " and a.cno = " & strCustNo
    vsqlCount = vsqlCount & " and a.cno = " & strCustNo
  End If
End If

vSql = vSql & " Order by 3"
oRs.Open vsqlCount, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
  vRecordCount = oRs.Fields(0)
Else
  vRecordCount = 0
End If
oRs.Close

oRs.PageSize = vPageSize
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
  vPagecount = Round(vRecordCount / vPageSize + 0.49)
  oRs.AbsolutePage = vsessionpage
  arOrder = oRs.GetRows(vPageSize)
Else
  vRecordCount = 0
  vPagecount = 0
  arOrder = "Fail"
End If

oRs.Close
oDB.Close

Set oRs = Nothing
Set oDB = Nothing

vResponse.Add vRecordCount, "Recordcount"
vResponse.Add vPagecount, "Pagecount"
vResponse.Add arOrder, "Order"
Set Display_FindTypicalOrderList = vResponse
Exit Function

ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Display_FindTypicalOrderList = "ERROR: " & Err.Description
End Function
Public Function Display_CustomerList(ByVal vPageSize As Long, ByVal vsessionpage As Long, ByVal strCustName As String, ByVal strCustNo As String) As Variant
'obtaining Customer Search List
Dim vResponse As New Collection
Dim arCustomer As Variant
Dim vRecordCount As Long
Dim vPagecount As Long
Dim vsqlCount As String
Dim vSqlTemp As String

On Error GoTo ErrorHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "Select CNo,CName,OrderType from CustomerMaster Where status = 'A' "
vsqlCount = "Select Count(1) From CustomerMaster Where status = 'A' "

If Trim(strCustName) <> "" Then
  vSqlTemp = " and CName like '%" & strCustName & "%' "
End If

If Trim(strCustNo) <> "" Then
  If IsNumeric(strCustNo) Then
    If Trim(vSqlTemp) <> "" Then
      vSqlTemp = vSqlTemp & " And CNo = " & strCustNo
    Else
      vSqlTemp = vSqlTemp & " and CNo = " & strCustNo
    End If
  End If
End If
   
If Trim(vSqlTemp) <> "" Then
  vSql = vSql & vSqlTemp
  vsqlCount = vsqlCount & vSqlTemp
End If

vSql = vSql & " Order by 2"

oRs.Open vsqlCount, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
  vRecordCount = oRs.Fields(0)
Else
  vRecordCount = 0
End If
oRs.Close

oRs.PageSize = vPageSize
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
  vPagecount = Round(vRecordCount / vPageSize + 0.49)
  oRs.AbsolutePage = vsessionpage
  arCustomer = oRs.GetRows(vPageSize)
Else
  vRecordCount = 0
  vPagecount = 0
  arCustomer = "Fail"
End If

oRs.Close
oDB.Close

Set oRs = Nothing
Set oDB = Nothing

vResponse.Add vRecordCount, "Recordcount"
vResponse.Add vPagecount, "Pagecount"
vResponse.Add arCustomer, "Customer"
Set Display_CustomerList = vResponse
Exit Function

ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Display_CustomerList = "ERROR: " & Err.Description
End Function
Public Function Display_SampleOrderDetails(ByVal vCustNo As Long) As Variant
'obtaining Typical order list
Dim vResponse As New Collection
Dim arCustomer As Variant
Dim arTrip As Variant

On Error GoTo ErrorHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "select CName,OCCode,CAddressI,CAddressII,CTown,CPostCode,CTelNo,C_Name,C_TelNo,D_Name,D_TelNo  from customermaster where cno = " & vCustNo
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
  arCustomer = oRs.GetRows()
Else
  arCustomer = "Fail"
End If
oRs.Close

vSql = "select TNO,Tname  From TripMaster where status = 'A'"
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
  arTrip = oRs.GetRows()
Else
  arTrip = "Fail"
End If
oRs.Close

oDB.Close
Set oRs = Nothing
Set oDB = Nothing

vResponse.Add arCustomer, "Customer"
vResponse.Add arTrip, "Trip"

Set Display_SampleOrderDetails = vResponse
Exit Function

ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Display_SampleOrderDetails = "ERROR: " & Err.Description
End Function
Public Function Add_Order(ByVal vCustNo As Long, ByVal strDate As String, ByVal vTripNo As Long, ByVal strSampleTag As String) As Variant
'Adding new order
Dim vResponse As New Collection
Dim i As Integer
Dim vWno As Long
Dim vOrderNo As Long
On Error GoTo ErrorHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

If IsDate(strDate) Then
  vWno = Weekday(strDate, vbMonday)
End If
  
 ' With oRs
  '  .Open "Temp_OrderMaster", oDB, adOpenKeyset, adLockPessimistic
 '   .AddNew
 '   .Fields("OrdDate") = strDate
 '   .Fields("WNo") = vWno
 '   .Fields("TNo") = vTripNo
 '   .Fields("SmpOrder") = strSampleTag
'    .Fields("CNo") = vCustNo
 '   .Update
 '   .Close
'  End With
  
  
   vSql = "insert into Temp_OrderMaster(OrdDate,WNo,TNo,SmpOrder,CNo) values (convert(datetime,'" & strDate & "',103)," & vWno & "," & vTripNo & ",'" & strSampleTag & "'," & vCustNo & ")"
    vResponse.Add "OK", "Sucess"
    oDB.Execute (vSql)
  
  vSql = "Select max(ordno) From Temp_OrderMaster"
  oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
  If Not IsNull(oRs(0)) Then
    vOrderNo = oRs(0)
  Else
    vOrderNo = 0
  End If
oDB.Close

vResponse.Add vOrderNo, "OrderNo"

Set oRs = Nothing
Set oDB = Nothing
Set Add_Order = vResponse
Exit Function

ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Add_Order = "ERROR: " & Err.Description
End Function

Public Function Display_LastOrderDate() As Variant
'Adding new order
Dim strDate As Variant
On Error GoTo ErrorHandler
 
Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn
 
vSql = "select max(OrdDate) from ordermaster "
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.EOF And oRs.BOF) Then
  If Not IsNull(oRs(0)) Then
    strDate = Day(oRs(0)) & "/" & Month(oRs(0)) & "/" & Year(oRs(0))
   Else
    strDate = ""
  End If
End If
oRs.Close
Set oRs = Nothing
Set oDB = Nothing
Display_LastOrderDate = strDate
Exit Function
 
ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Display_LastOrderDate = "ERROR: " & Err.Description
End Function
Public Function Display_OrderDateCheck(ByVal strDate As String, ByVal strTag As String) As String
 

On Error GoTo ErrorHandler
 
Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn
 
If Trim(strTag) = "yes" Then
  vSql = "select Status = case when (select count(1) from ordermaster where datediff(d,orddate,convert(datetime,'" & strDate & "',103)) =0 ) > 0 then 'Y' else 'N' end "
Else
  vSql = "select Status = case when (select count(1) from Temp_ordermaster where datediff(d,orddate,convert(datetime,'" & strDate & "',103)) =0 ) > 0 then 'Y' else 'N' end "
End If
 
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.EOF And oRs.BOF) Then
  If Trim(oRs(0)) = "Y" Then
    Display_OrderDateCheck = "OK"
  Else
    Display_OrderDateCheck = "Fail"
  End If
Else
  Display_OrderDateCheck = "Fail"
End If
oRs.Close
Set oRs = Nothing
Set oDB = Nothing
Exit Function
 
ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Display_OrderDateCheck = "ERROR: " & Err.Description
End Function
Public Function OrderExistCheck(ByVal vCustNo As Long, ByVal strDate As String, ByVal vTripNo As Long, ByVal strSampleOrderTag As String) As Variant
 
Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn
 
On Error GoTo ErrHandler
 
  If Trim(strSampleOrderTag) = "Y" Then
    vSql = "select count(1) from temp_ordermaster where cno = " & vCustNo & " and tno =  " & vTripNo & " and wno = (select wno from weekmaster where wname =(select datename(dw,convert(datetime,'" & strDate & "',103)))) and datediff(d,orddate,convert(datetime,'" & strDate & "',103))=0 and smporder='Y'"
  Else
    vSql = "select count(1) from temp_ordermaster where cno = " & vCustNo & " and tno = " & vTripNo & " and wno = (select wno from weekmaster where wname =(select datename(dw,convert(datetime,'" & strDate & "',103)))) and datediff(d,orddate,convert(datetime,'" & strDate & "',103))=0 and smporder='N'"
  End If
  
  oRs.Open vSql, oDB, adOpenDynamic, adLockReadOnly
  If Not (oRs.BOF And oRs.EOF) Then
    If oRs(0) > 0 Then
      OrderExistCheck = "Fail"
    Else
      OrderExistCheck = "OK"
    End If
  End If
  oRs.Close
  
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
 
Exit Function
 
ErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
OrderExistCheck = "ERROR: " & Err.Description
End Function


Public Function Log_OrderAmend(ByVal OrderNo As Long, ByVal EmpNo As Long, ByVal delDate As String) As Variant
'Adding new order
Dim vResponse As New Collection
On Error GoTo ErrorHandler

Dim IsProcessedDate As String

IsProcessedDate = IsProcessedForTheDate(Replace(delDate, "'", "''"))

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn
  
With oRs
  .Open "OrderAmendLog", oDB, adOpenKeyset, adLockPessimistic
  .AddNew
  .Fields("OrdNo") = OrderNo
  .Fields("ENo") = EmpNo
  .Fields("AfterProcessAmend") = IsProcessedDate
  .Update
  .Close
End With
  
oDB.Close

Set oRs = Nothing
Set oDB = Nothing

Log_OrderAmend = "OK"

Exit Function

ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
Log_OrderAmend = "ERROR: " & Err.Description
End Function


Public Function GetCustomerIdByOrderId(ByVal OrderId As Long) As Integer
On Error GoTo GetCustomerIdByOrderIdErrHandler

Dim cno As Integer

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn
vSql = "select CNo from Temp_OrderMaster where OrdNo = " & OrderId
oRs.Open vSql, oDB, adOpenStatic, adLockOptimistic

If (Not (oRs.BOF And oRs.EOF)) Then
    cno = oRs("CNo").Value
End If

GetCustomerIdByOrderId = cno

oRs.Close
oDB.Close
Set oRs = Nothing
Set oDB = Nothing

Exit Function

GetCustomerIdByOrderIdErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
GetCustomerIdByOrderId = ""
    
End Function

Public Function IsProcessedForTheDate(ByVal strDate As String) As String
On Error GoTo IsProcessedForTheDateErrHandler

Dim result As String
result = "N"

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset

oDB.Open vdbConn

vSql = "SELECT TOP 1 OrdNo FROM OrderMaster WHERE DATEDIFF(D,OrdDate,CONVERT(DATETIME,'" & strDate & "',103)) = 0"
oRs.Open vSql, oDB, adOpenStatic, adLockOptimistic

If (Not (oRs.BOF And oRs.EOF)) Then
    result = "Y"
End If

IsProcessedForTheDate = result

oRs.Close
oDB.Close
Set oRs = Nothing
Set oDB = Nothing

Exit Function

IsProcessedForTheDateErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
IsProcessedForTheDate = ""
    
End Function

Public Function ClearAmendOrderQty(ByVal vOrderNo As Long, ByVal strDate As String) As String

On Error GoTo ErrorHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

'vSql = "select PNo from Temp_OrderDetail where Qty>0 and OrdNo=" & vOrderNo
vSql = "select PNo from Temp_OrderDetail where OrdNo=" & vOrderNo
oRs.Open vSql, oDB, adOpenKeyset, adLockReadOnly

If Not (oRs.BOF And oRs.EOF) Then
  Do While Not oRs.EOF
    vSql = "update Temp_OrderDetail set Qty=0,Status='Y' where OrdNo=" & vOrderNo & " and PNo='" & oRs(0).Value & "'"
    oDB.Execute vSql, , adCmdText
    'Multi Product
    vSql = "exec dbo.sp_UpdateTemp_OrderDetailForMultipleProduct " & vOrderNo & ",'" & oRs(0).Value & "'," & 0
    oDB.Execute (vSql)
    oRs.MoveNext
  Loop
  vSql = "update Temp_OrderMaster set AmdDate='" & strDate & "' where OrdNo=" & vOrderNo
  oDB.Execute vSql, , adCmdText
End If
oRs.Close
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
ClearAmendOrderQty = "OK"
Exit Function

ErrorHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
ClearAmendOrderQty = "ERROR: " & Err.Description
End Function


Public Function SaveTempOrderMasterDeliveryCharge(ByVal TempOrderNo As Long, ByVal NotApplyDeliveryCharge As Integer) As Variant

Dim vResponse As New Collection

On Error GoTo ErrHandler

Set oDB = New ADODB.Connection
Set oRs = New ADODB.Recordset
oDB.Open vdbConn

vSql = "select *  from Temp_OrderMaster_deliverycharge where TempOrderNo=" & TempOrderNo
oRs.Open vSql, oDB, adOpenStatic, adLockReadOnly
If Not (oRs.BOF And oRs.EOF) Then
    vSql = "update Temp_OrderMaster_deliverycharge set NotApplyDeliveryCharge=" & NotApplyDeliveryCharge & " where TempOrderNo=" & TempOrderNo
    vResponse.Add "OK", "Sucess"
    oDB.Execute (vSql)
Else
    vSql = "insert into Temp_OrderMaster_deliverycharge(TempOrderNo,NotApplyDeliveryCharge) values (" & TempOrderNo & "," & NotApplyDeliveryCharge & ")"
    vResponse.Add "OK", "Sucess"
    oDB.Execute (vSql)
End If
oRs.Close
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
vResponse.Add False, Key:="error"

Set SaveTempOrderMasterDeliveryCharge = vResponse


Exit Function
ErrHandler:
oDB.Close
Set oRs = Nothing
Set oDB = Nothing
SaveTempOrderMasterDeliveryCharge = "ERROR: " & Err.Description
End Function
