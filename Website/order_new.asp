<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
session("sessionAddProdCode")=""
Dim Obj,ObjCustomer
Dim arTypicalOrder
Dim strCustomerName,strCustNo,strDate
Dim vPageSize
Dim strDirection
Dim vpagecount
Dim vCurrentPage
Dim i
Dim arSplit,UType


vPageSize = 100
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vCurrentPage	= Request.Form("CurrentPage") 

UType	= Request("UType") 

strCustomerName = trim(replace(Request.Form("txtCustName"),"'","''"))
strCustNo = trim(replace(Request.Form("txtCustNo"),"'","''"))
strDate = trim(replace(trim(Request.Form("txtDate")),"'","''"))

if strCustNo <> "" Then
	if  not IsNumeric(strCustNo) Then
		strCustNo = 0
	End if
End if


If strDate = "" Then
	strDate = Day(date()+1) & "/" & Month(date()+1) & "/" & Year(date()+1)  
End if

If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if

if strDate <> "" Then
	arSplit = Split(strDate,"/")
	strDate = Right( "0" & arSplit(0),2) & "/" & Right( "0" & arSplit(1),2) & "/" & arSplit(2)	
End if
stop
Set Obj = CreateObject("Bakery.Orders")
Obj.SetEnvironment(strconnection)
Set ObjCustomer = obj.Display_TypicalOrderList(vPageSize,vCurrentPage,strCustomerName,strCustNo,strDate)
arTypicalOrder = ObjCustomer("TypicalOrder")
vpagecount  = ObjCustomer("Pagecount")

Set Obj = Nothing
Set ObjCustomer = Nothing
%>


<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT LANGUAGE=javascript>
<!--
function initValueCal(theForm,theForm2){
	y=theForm.deldate.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm2.value=t.getDate() +'/'+eval((t.getMonth())+1) +'/'+ t.getFullYear();
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
		
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal(document.frmOrderSearch,document.frmOrderSearch.txtDate)">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
	<b><font size="3">
	<%if (UType="N") then%>
	View order
	<%else%>
	Amend order
	<%end if%>
    <br>&nbsp;</font></b>
	  <form name="frmOrderSearch" method="post">
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          <td><b>Customer Code:</b></td>
          <td><b>Customer Name:</b></td>
          <td><b>Delivery Date:</b></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><input type="text" name="txtCustNo"  value="<%=strCustNo%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td><input type="text" name="txtCustName" value="<%=strCustomerName%>"  size="20" style="font-family: Verdana; font-size: 8pt"></td>          
           <TD width="375">
	    <%
		dim arrayMonth
		arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
	    %>
		<select name="day" onChange="setCal(this.form,document.frmOrderSearch.txtDate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.frmOrderSearch.txtDate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.frmOrderSearch.txtDate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt id=deldate  name="txtDate" value="<%=strDate%>" onFocus="blur();" onChange="setCalCombo(this.form,document.frmOrderSearch.txtDate)">
      <IMG id=f_trigger_fr onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  </TD>
		  <td><input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td><b>&nbsp;</b></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
      </form>
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtDate",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr",
				singleClick    :    true
			});
	</SCRIPT>
      <%if vCurrentPage <> 1 then%>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">      
				<tr>
					<td height="20"></td>
				</tr><%				
				if IsArray(arTypicalOrder) Then%>
					<tr>
					  <td align="right"><b>Page <%=vCurrentPage%> of <%=vpagecount%></b></td>          
					</tr><%
				End if%>	
			</table> 
      <%end if%>

      <table border="0" bgcolor="#999999" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
        <tr>
          <td width="15%" bgcolor="#CCCCCC"><b>Customer Code</b></td>
          <td width="25%" bgcolor="#CCCCCC"><b>Customer Name</b></td>          
          <td width="10%" bgcolor="#CCCCCC"><b>Trip</b></td>
          <td width="20%" bgcolor="#CCCCCC"><b>Standing Order</b></td>
          <td width="15%" bgcolor="#CCCCCC" align="center"><b>Action</b></td>
        </tr><%        
						if IsArray(arTypicalOrder) Then 
								for i = 0 to UBound(arTypicalOrder,2)
								if (i mod 2 =0) then
									strBgColour="#FFFFFF"
								else
									strBgColour="#F3F3F3"
								end if
								%>
									<tr bgcolor="<%=strBgColour%>">
										<td><%=arTypicalOrder(1,i)%></td>
										<td><%=arTypicalOrder(2,i)%></td>										
										<td><%=arTypicalOrder(3,i)%></td>
										<td><%										
												if Trim(arTypicalOrder(4,i)) = "Y" Then
													Response.Write "Yes"
												Else
													Response.Write "No"
												End if%>
										</td>										
										<form method="POST" action="order_new1.asp" id="frmTypical<%=i%>" name="frmTypical<%=i%>">
											<td align="center">
											<%if (UType="N") then%>
												<input type="submit" value="View Order" name="B1" style="font-family: Verdana; font-size: 8pt">
												<INPUT type="hidden" id="confirm" name="confirm" value="No">
												<INPUT type="hidden" name="UType" value="N">
											<%else%>
												<input type="submit" value="Amend Order" name="B1" style="font-family: Verdana; font-size: 8pt">
											<%end if%>
											</td>
											<input type="hidden" name="OrderNo" value="<%=arTypicalOrder(0,i)%>">
										</form>
									</tr><%
								Next
						else%>
						<tr><td>Sorry no items found</td></tr><%		
						End if%>       
      </table>
    </td>
  </tr>
</table>

	
	<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		<FORM Name="Navigate">
			<tr>					
				<td width="187"><%				
		
					if vCurrentPage <> 1 then%>
						<input type="button" value="FirstPage" Onclick="document.frmFirst.submit()" id=button1 name=button1 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) > 1 then%>
						<input type="button" value="PreviousPage" Onclick="document.frmPrevious.submit()" id=button3 name=button3 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%				
					if clng(vCurrentPage) < vPagecount then%>
						<input type="button" value="NextPage" Onclick="document.frmNext.submit()" id=button5 name=button5 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) <> vPagecount and vPagecount > 0  then%>
						<input type="button" value="LastPage" Onclick="document.frmLast.submit()" id=button7 name=button7 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>					
			</tr>
		</FORM>
	</table>
  </center>
</div>

	<FORM METHOD="post" NAME="frmFirst">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="First">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">				
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<INPUT TYPE="hidden" NAME="txtDate"				VALUE="<%=strDate%>">		 
	</FORM>

	<FORM METHOD="post" NAME="frmPrevious">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Previous">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">	
		<INPUT TYPE="hidden" NAME="txtDate"				VALUE="<%=strDate%>">		 	
	</FORM>

	<FORM METHOD="post" NAME="frmNext">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Next">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">	
		<INPUT TYPE="hidden" NAME="txtDate"				VALUE="<%=strDate%>">		 	
	</FORM>

	<FORM METHOD="post" NAME="frmLast">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Last">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">	
		<INPUT TYPE="hidden" NAME="txtDate"				VALUE="<%=strDate%>">		 	
	</FORM>


</body>
</html><%
if IsArray(arTypicalOrder) Then Erase arTypicalOrder
%>