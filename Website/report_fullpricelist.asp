<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
set detail=objBakery.ListPriceTypesforProduct()	
vPriceTypesArray=detail("PriceType") 
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function ExportToExcelFile(){
	vtxtpno=document.frmSearch.txtpno.value
	vtxtpname=document.frmSearch.txtpname.value
	vpricebandid=document.frmSearch.hidpricebandids.value
	document.location="ExportToExcelFilereport_fullpricelist.asp?txtpno=" + vtxtpno + "&txtpname=" + vtxtpname + "&pricebandid=" + vpricebandid; 
}
function CheckSearch(frm)
{
	if (frm.pricebandid.value == "")
	{
		alert("Please select a Price Band");
		frm.pricebandid.focus();
		return  false;
	}
	return  true;
}
//-->
</Script>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#ffffff" text="#000000" style="FONT-SIZE: 8pt; FONT-FAMILY: Verdana">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
  <%
  pno=trim(replace(Request("txtpno"),"'","''"))
  pname=trim(replace(Request("txtpname"),"'","''"))
  strpricebandids=trim(Request("pricebandid"))
  arpricebandids=Split(strpricebandids,",")
  	if (IsArray(arpricebandids)) then
		for j =0 to ubound(arpricebandids) 
			intpricebandid=Trim(arpricebandids(j))
			if intpricebandid=1 then
				strA=1
			elseif intpricebandid=2 then
				strB=1
			elseif intpricebandid=3 then
				strC=1
			elseif intpricebandid=4 then
				strD=1
			elseif intpricebandid=5 then
				strA1=1
			elseif intpricebandid=7 then
				strB1=1
			elseif intpricebandid=8 then
				strB2=1
			elseif intpricebandid=9 then
				strB3=1
			elseif intpricebandid=10 then
				strB4=1
			elseif intpricebandid=11 then
				strB5=1
			elseif intpricebandid=12 then
				strB6=1
			elseif intpricebandid=13 then
				strB7=1
			elseif intpricebandid=14 then
				strB8=1
			elseif intpricebandid=15 then
				strB9=1
			elseif intpricebandid=17 then
				strG=1
			elseif intpricebandid=19 then
				strB10=1
			elseif intpricebandid=20 then
				strB11=1
			elseif intpricebandid=21 then
				strB12=1
			elseif intpricebandid=22 then
				strB13=1
			elseif intpricebandid=23 then
				strB14=1
			elseif intpricebandid=24 then
				strB15=1
			elseif intpricebandid=25 then
				strD1=1
			elseif intpricebandid=26 then
				strD2=1
			elseif intpricebandid=27 then
				strD3=1
			elseif intpricebandid=28 then
				strD4=1
			elseif intpricebandid=29 then
				strD5=1
			elseif intpricebandid=30 then
				strH=1
			elseif intpricebandid=31 then
				strI=1
			end if
		next
	end if
  %>
<table border="0" width="95%" cellspacing="0" cellpadding="0" style="FONT-SIZE: 8pt; FONT-FAMILY: Verdana">
  <tr>
    <td width="100%"><b><font size="3">Report - Full Price List<br>
      &nbsp;</font></b>
      <table border="0" cellspacing="0" cellpadding="2" style="FONT-SIZE: 8pt; FONT-FAMILY: Verdana">
        <tr>
          <td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
		  <td><nobr><b>Price Band:&nbsp;&nbsp;&nbsp;</b></nobr></td>
          <td><b>Product code:</b></td>
          <td><b>Product name:</b></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <form name="frmSearch" method="post" action="report_fullpricelist.asp" onSubmit="return CheckSearch(this)">
		  <input type="hidden" name="hidpricebandids" value="<%=strpricebandids%>">
		  <td valign="top">
		   <select size="5" name="pricebandid" style="font-family: Verdana; font-size: 8pt; width:70px" multiple="multiple">
		  <%
			if IsArray(vPriceTypesArray) Then
				for i = 0 to ubound(vPriceTypesArray,2)
					arpricebandids=Split(strpricebandids,",")
					if (IsArray(arpricebandids)) then
						flag="true"
						for j =0 to ubound(arpricebandids) 
							if trim(vPriceTypesArray(0,i))= Trim(arpricebandids(j)) then%>
							%>
							<option value="<%=vPriceTypesArray(0,i)%>" selected="selected"><%=vPriceTypesArray(1,i)%></option>
							<%
							flag="false"
							end if
						next
						if flag="true" then
						%>
							<option value="<%=vPriceTypesArray(0,i)%>"><%=vPriceTypesArray(1,i)%></option>				
						<%
						end if
					else
				%>  				
					 <option value="<%=vPriceTypesArray(0,i)%>" <%if strpricebandids<>"" then%><%if cint(strpricebandids)= vPriceTypesArray(0,i) then response.write " Selected"%><%end if%>><%=vPriceTypesArray(1,i)%></option>
			  <%
					end if
			   next                 
			End if
			%>     
		  </select>
		  </td>
          <td valign="top"><input name="txtpno" style="FONT-SIZE: 8pt; FONT-FAMILY: Verdana" value="<%=pno%>"></td>
          <td valign="top"><input name="txtpname" size="50" maxlength="200" style="FONT-SIZE: 8pt; FONT-FAMILY: Verdana" value="<%=pname%>"></td>
          <td valign="top">
            <input type="submit" value="Search" name="Search" style="FONT-SIZE: 8pt; FONT-FAMILY: Verdana">
	      </td>
          </form>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
  </center>
</div>
<%
if (Request("Search")<>"") then

dim objBakery
dim recarray
dim retcol
dim totpages, totrecs
dim vSessionpage
dim pno, pname
dim i
set retcol = objBakery.FullPriceReport(pno,pname,1,0)
totrecs = retcol("RecordCount")
totpages = retcol("pagecount")
recarray = retcol("ProductPrices")
%>
<div align="center">
  <center>
       <table border="0" cellspacing="0" cellpadding="0" width="95%" align="center">
	    <tr bgcolor="#FFFFFF">
          <td height="25" align="left"><b><a href="javascript:ExportToExcelFile()"><font color="#0000FF" size="2">Export to Excel File</font></a></b></td>
       </tr>
	   <tr>
	   <td align="left">
	   <table border="0" style="FONT-SIZE: 8pt; FONT-FAMILY: Verdana" cellspacing="1" cellpadding="2" bgcolor="#999999">
        
        <tr>
          <td bgcolor="#cccccc" width="10"><b>No</b></td>
		  <td bgcolor="#cccccc" width="100"><nobr><b>Product Code&nbsp;</b></nobr></td>
          <td bgcolor="#cccccc" width="400"><nobr><b>Product Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></nobr></td>
          <% if (strA=1) then %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price A (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB=1) then 
		  %>
          <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strC=1) then 
		  %>
          <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price C (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strD=1) then 
		  %>
          <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price D (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strA1=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price A1 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB1=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B1 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB2=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B2 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB3=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B3 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB4=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B4 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB5=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B5 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB6=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B6 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB7=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B7 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB8=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B8 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB9=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B9 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB10=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B10 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB11=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B11 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB12=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B12 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB13=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B13 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB14=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B14 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strB15=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price B15 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strG=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price G (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strD1=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price D1 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strD2=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price D2 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strD3=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price D3 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strD4=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price D4 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strD5=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price D5 (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strH=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price H (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  if (strI=1) then 
		  %>
		  <td bgcolor="#cccccc" align="center"><nobr><b>&nbsp;Price I (�)&nbsp;</b></nobr></td>
		  <%
		  end if
		  %>
        </tr>
		<%		
        if isarray(recarray) then
          for i=0 to ubound(recarray,2)
		  
		  %>
            <tr bgcolor="#FFFFFF">
			  <td><%=i+1%>.&nbsp;</td>
              <td><%=recarray(0,i)%></td>
              <td width="400"><%=recarray(1,i)%></td>
			  <% if (strA=1) then %>
			  <td align="center"><%if not isnull(recarray(2,i)) then response.Write(formatnumber(recarray(2,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(4,i)) then response.Write(formatnumber(recarray(4,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strC=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(5,i)) then response.Write(formatnumber(recarray(5,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strD=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(6,i)) then response.Write(formatnumber(recarray(6,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strA1=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(3,i)) then response.Write(formatnumber(recarray(3,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB1=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(7,i)) then response.Write(formatnumber(recarray(7,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB2=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(8,i)) then response.Write(formatnumber(recarray(8,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB3=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(9,i)) then response.Write(formatnumber(recarray(9,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB4=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(10,i)) then response.Write(formatnumber(recarray(10,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB5=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(11,i)) then response.Write(formatnumber(recarray(11,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB6=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(12,i)) then response.Write(formatnumber(recarray(12,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB7=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(13,i)) then response.Write(formatnumber(recarray(13,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB8=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(14,i)) then response.Write(formatnumber(recarray(14,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB9=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(15,i)) then response.Write(formatnumber(recarray(15,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB10=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(16,i)) then response.Write(formatnumber(recarray(16,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB11=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(17,i)) then response.Write(formatnumber(recarray(17,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB12=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(18,i)) then response.Write(formatnumber(recarray(18,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB13=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(19,i)) then response.Write(formatnumber(recarray(19,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB14=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(20,i)) then response.Write(formatnumber(recarray(20,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strB15=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(21,i)) then response.Write(formatnumber(recarray(21,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strG=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(22,i)) then response.Write(formatnumber(recarray(22,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strD1=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(23,i)) then response.Write(formatnumber(recarray(23,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strD2=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(24,i)) then response.Write(formatnumber(recarray(24,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strD3=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(25,i)) then response.Write(formatnumber(recarray(25,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strD4=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(26,i)) then response.Write(formatnumber(recarray(26,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strD5=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(27,i)) then response.Write(formatnumber(recarray(27,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strH=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(28,i)) then response.Write(formatnumber(recarray(28,i),2)) else  response.Write("0.00") end if%></td>
			  <%
			  end if
			  if (strI=1) then
			  %>
			  <td align="center"><%if not isnull(recarray(29,i)) then response.Write(formatnumber(recarray(29,i),2)) else  response.Write("0.00") end if%></td>
		   	  <%
			  end if
			  %>
		   </tr>
			<%
          next
        else%>
          <tr>
          <td  colspan="6">
            <b><font size="2">No records found...</font></b>
          </td>
          </tr>
        <%end if%>
      </table>
	  </td>
	  </tr>
	  </table>
  </center>
</div>
 <% end if%>
 <br><br>
</div>
</body>
</html>
<%
set object = nothing
%>