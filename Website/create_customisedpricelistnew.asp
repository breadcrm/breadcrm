<%@ Language=VBScript%>
<%option Explicit%>
<%Response.Buffer=true%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>
<%
dim objBakery
dim recarray3
dim retcol
dim i
dim ptno, ptname, dno

if Request.Form("submit") = "Save" and instr(1,lcase(Request.ServerVariables("HTTP_REFERER")),"create_customisedpricelistnew.asp") > 0 then
  set objBakery = server.CreateObject("Bakery.MgmReports")
  objBakery.SetEnvironment(strconnection)
  ptname = Request.Form("txtptname")
  'Response.Write "ptname = " & ptname & "<br>"
  if trim(ptname) <> "" then
  set retcol = objBakery.InsertPriceList(mid(ptname,1,3))
  recarray3 = retcol("PriceList")
  if isarray(recarray3) then
    ptno = recarray3(0,0)
  end if
  Response.Redirect "report_customisedpricelist.asp"
  end if
end if
%>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;<br><br>
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Create Customised Price List<br>
      &nbsp;</font></b>
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <form name="frmplist" method="post" action="create_customisedpricelistnew.asp">
          <td><b>Price List Name (Max 3 Chars) : </b></td>
          <td colspan="2">
            <input type="text" name="txtptname" size="42" value="<%=ptname%>" maxlength="3" style="font-family: Verdana; font-size: 8pt">
          </td>
          <td><input type="submit" name="submit" value="Save"></td>
          <td></td>
          </form>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
     <td>&nbsp;
     </td>
     <td>&nbsp;
     </td>
  </tr>
</table>
  </center>
</div>
</body>
</html>