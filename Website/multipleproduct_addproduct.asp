<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim Obj,ObjProduct,ObjResult
Dim arProduct
Dim strProdName,strProdCode
Dim vPageSize
Dim strDirection
Dim vpagecount
Dim vCurrentPage
Dim i
Dim vOrderNo
Dim vQty
Dim strAddProdCode
stop
vPageSize = 999999
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vCurrentPage	= Request.Form("CurrentPage") 
strProdName = Request.Form("txtProdName")
strProdCode = Request.Form("txtProdCode")

ProdCode= trim(Request.Form("ProdCode"))
LateProd= trim(Request.Form("LateProd"))

If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if

Set Obj = CreateObject("Bakery.Product")
Obj.SetEnvironment(strconnection)

if Trim(Request.Form("Update")) <> "" Then
		strAddProdCode = Request.Form("txtProdCode") 
		strQty = Request.Form("txtQty")	
		
		if (Trim(strQty) <> "" And IsNumeric(strQty)) Then					
			if (strQty>0) then
				set ObjResult = obj.IsMultipleProduct(ProdCode,strAddProdCode) 		
				if (ObjResult("Sucess")="True") then
					strstatus="ExistMultipleProduct"
				else
					set ObjResult = obj.InsertMultipleProduct(ProdCode,strAddProdCode,strQty,LateProd)
					if (ObjResult("Sucess")="False") then
						strstatus="notsameprefixes"
					else
						ProdCode=ObjResult("PNo")
						
						%>
						<form method="POST" action="product_new.asp" name="frmproductnew">
						  <input type="hidden"  name="pno"  value="<%=ProdCode%>">         
						</form>
						<SCRIPT LANGUAGE="JavaScript">
						 document.frmproductnew.submit();
						</SCRIPT>						
						<%
						strProdCode=""
					end if		
				end if
			else
				strstatus="Qtyzero"
			end if	
		else
			strstatus="SelectQty"
		end if
End if  

Set ObjProduct = obj.Display_All_Products_Exclude_Multiple_Product(vPageSize,vCurrentPage,strProdCode,strProdName,ProdCode)
arProduct = ObjProduct("Product")
vpagecount  = ObjProduct("Pagecount")
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Add Multiple Product - <%=ProdCode%><br>&nbsp;</font></b>	
      	<form name="frmAddSearch" method="post">
	  	<Input type="hidden" name="ProdCode" value="<%=ProdCode%>">
		<Input type="hidden" name="LateProd" value="<%=LateProd%>">
		<table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
	  	<tr>
			<td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
			<td><b>Product code:</b></td>
			<td><b>Product Name:</b></td>
			<td></td>
	  	</tr>
	  	<tr>
			<td></td>
			<td><input type="text" name="txtProdCode" value="<%=strProdCode%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
			<td><input type="text" name="txtProdName" value="<%=strProdName%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
			<td><input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
	  	</tr>
	  	<tr>
			<td><b>&nbsp;</b></td>
			<td></td>
			<td></td>
			<td></td>
	  	</tr>
		</table>
      	</Form>  
    	<%
		if (strstatus="SelectQty") then
		%>
		<p align="center"><font size="2" color="#FF0000">Please enter the quantity.</font></p>
		<% 
		elseif (strstatus="Qtyzero") then
		%>
		<p align="center"><font size="2" color="#FF0000">Please enter the quantity grater than zero.</font></p>
		<%
		elseif (strstatus="ExistMultipleProduct") then
		%>
		<p align="center"><font size="2" color="#FF0000">This product already added.</font></p>
		<% 
		elseif (strstatus="notsameprefixes") then
		%>
		<p align="center"><font size="2" color="#FF0000">
		
		This product basket can only contain the following prefixes:</font></p>
		  <%
		  arrPrefixes=split(ProdCode,"-")
		  strPrefixes=arrPrefixes(0)
		  set vListofMultipleProducts= Obj.ListofMultipleProductCodesPrefixes(strPrefixes)
			vPrefixArray = vListofMultipleProducts("ProductCodesPrefixes")
			if isarray(vPrefixArray) then
			%>
			<table width="300" border="0" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" align="center" bgcolor="#CCCCCC">
		  	<tr bgcolor="#999999">
				<td width="30"><strong>FNo</strong></td>
				<td><strong>Facility</strong></td>
		  	</tr>
		  	<%
			For i = 0 to UBound(vPrefixArray,2)
				if (i mod 2 =0) then
					strBgColour="#FFFFFF"
				else
					strBgColour="#F3F3F3"
				end if
			%>
		<tr bgcolor="<%=strBgColour%>">
		<td><%=vPrefixArray(0,i)%></b></td>
		<td><%=vPrefixArray(1,i)%></td>
		</tr>
		<%
		next
		%>
		</table>
		<br>
		<%
		end if
		end if 
		%>
       <table border="0" bgcolor="#999999" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
       <tr>
          <td width="25%" bgcolor="#CCCCCC"><b>Product Code</b></td>
          <td bgcolor="#CCCCCC"><b>Product Name</b></td>
		   <td width="25%" bgcolor="#CCCCCC"><b>Qty</b></td>
          <td width="25%" bgcolor="#CCCCCC"><b>Action</b></td>
        </tr>
	   <%
	   If (not (strProdCode= "" and strProdName="")) and IsArray(arProduct) Then
	        
		For I = 0 to UBound(arProduct,2)
		if (I mod 2 =0) then
			strBgColour="#FFFFFF"
		else
			strBgColour="#F3F3F3"
		end if
		%>        
			<FORM name="frmAddProd<%=i%>" method="post">
			 <Input type="hidden" name="ProdCode" value="<%=ProdCode%>">
			<Input type="hidden" name="LateProd" value="<%=arProduct(2,i)%>">
				<tr bgcolor="<%=strBgColour%>">
				  <td><%=arProduct(0,i)%></td>
				  <td><%=arProduct(1,i)%></td>
				  <td><input type="text" name="txtQty" size="7" maxlength="6" style="font-family: Verdana; font-size: 8pt"></td>
				  
				  <td><input type="submit" value="Add Product" name="B1" style="font-family: Verdana; font-size: 8pt"></td>							  
				</tr>
				<input type="hidden"  name="txtProdCode"  value="<%=arProduct(0,i)%>">
				<input type="hidden"  name="Update"  value="True">
			</Form>
			<%						
		Next
		else
		%>
		<td Colspan="4" bgcolor="#FFFFFF" align="center"><font size="2" color="#FF0000">Sorry no items found</font></td>
		<%	
		End if
		%>        
        <tr>
           <td colspan="4" align="center" bgcolor="#CCCCCC">
		   <input onClick="Javascript:document.back.submit();" type="button" value="Go Back" name="Go Back" style="font-family: Verdana; font-size: 8pt">
		 	</td> 
        </tr>
		 <form method="POST" action="product_new.asp" name="back">
			  <input type="hidden"  name="pno"  value="<%=ProdCode%>">         
		 </form>
      </table>
    </td>
  </tr>
</table>

	<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		<FORM Name="Navigate">
		<Input type="hidden" name="vDateDiff" value="<%=strDateDiff%>">
			<tr>					
				<td width="187"><%				
		
					if vCurrentPage <> 1 then%>
						<input type="button" value="FirstPage" Onclick="document.frmFirst.submit()" id=button1 name=button1 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) > 1 then%>
						<input type="button" value="PreviousPage" Onclick="document.frmPrevious.submit()" id=button3 name=button3 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%				
					if clng(vCurrentPage) < vPagecount then%>
						<input type="button" value="NextPage" Onclick="document.frmNext.submit()" id=button5 name=button5 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) <> vPagecount and vPagecount > 0  then%>
						<input type="button" value="LastPage" Onclick="document.frmLast.submit()" id=button7 name=button7 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>					
			</tr>
		</FORM>
	</table>
  </center>
</div>


	<FORM METHOD="post" NAME="frmFirst">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="First">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">				
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"			VALUE="<%=strProdCode%>">
		<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
		<Input type="hidden" name="vDateDiff" value="<%=strDateDiff%>">
	</FORM>

	<FORM METHOD="post" NAME="frmPrevious">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Previous">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"			VALUE="<%=strProdCode%>">
		<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
		<Input type="hidden" name="vDateDiff" value="<%=strDateDiff%>">
	</FORM>

	<FORM METHOD="post" NAME="frmNext">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Next">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"			VALUE="<%=strProdCode%>">
		<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
		<Input type="hidden" name="vDateDiff" value="<%=strDateDiff%>">
	</FORM>

	<FORM METHOD="post" NAME="frmLast">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Last">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"			VALUE="<%=strProdCode%>">
		<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
		<Input type="hidden" name="vDateDiff" value="<%=strDateDiff%>">
	</FORM>
  </center>
</div>


</body>

</html><%
Set Obj = Nothing
Set ObjProduct = Nothing
If IsArray(arProduct) Then Erase arProduct
%>