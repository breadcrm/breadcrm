<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Invoice</title>
<style type="text/css">
<!--
br.page { page-break-before: always; height:1px }
-->
</style>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0">
<%
dim objBakery,retcol,recarray1,TotalVAT,TotalGoods,TotalInvoice,intI,recarray2,strResellerID,strOrdDate,intN,strLogo,strAddress
dim strRCreditNo, strCreditDate
dim invNos
strResellerID=request("ResellerID")
strCreditDate=request("CreditDate")
strRCreditNo=request("RCreditNo")

TotalVAT=0
TotalGoods=0
TotalInvoice=0

strLogo = "<b><font face=""Monotype Corsiva"" size=""5"">THE BREAD FACTORY</font></b>"
strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
strAddress = strAddress & "<br>" & vbCrLf
strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
strAddress = strAddress & "Watford, Herts WD17 1LA" & vbCrLf
strAddress = strAddress & "" & vbCrLf
strAddress = strAddress & "<BR>" & vbCrLf
strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
strAddress = strAddress & "</p></font>"

set objBakery = server.CreateObject("Bakery.Reseller")
objBakery.SetEnvironment(strconnection)
invNos = objBakery.GetResellerCreditNoteInvNos(strRCreditNo)
set retcol = objBakery.DisplayResellerDetail(strResellerID)
recarray1 = retcol("ResellerDetail")
set retcol=nothing
if isarray(recarray1) then
TotalVAT=0
TotalGoods=0
TotalInvoice=0
set retcol = objBakery.GetResellerCreditNoteDetails(strResellerID,strCreditDate)
recarray2 = retcol("InvoiceDetails")
if isarray(recarray2) then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<div align="center">
<center>
<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" style="background-repeat:no-repeat">
<tr>
<td width="100%" valign="top">
<table border="0" width="95%" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%" valign="top" >
<font face="Verdana" size="1">VAT Reg No. XXXXXX</font>
<%=strAddress%>
</td>
<td width="46%" valign="top" align="center">
<img src="images/BakeryLogo.gif" width="225" height="77"><br>

</td>
<td width="28%" valign="top">
<p align="center">
<font face="Verdana" size="1">THIS IS AN INVOICE</font><br>
<font face="Verdana" size="1">PLEASE KEEP SAFE AND PASS TO YOUR ACCOUNTS DEPARTMENT</font></b><font size="1">
</font>
</td>
</tr>
<tr><td height="10"></td></tr>
</table>
<table border="0" width="95%" cellspacing="0" cellpadding="0"  align="center">
<tr>
<td width="33%"></td>
<td width="33%">
<p align="center"><font size="3" face="Verdana"><b>Reseller Credit<br>
</b>NUMBER: <%=strRCreditNo%></font></td>
<td width="34%"></td>
</tr>
<tr><td height="10"></td></tr>
</table>
<table border="0" width="95%" cellspacing="0" cellpadding="0"  align="center">
<tr>
<td valign="top">
<font face="Verdana" size="2"><b>
Invoice No(s): <%=invNos%>
<br />
<% if recarray1(0, intI)<>"" then%><%=recarray1(0, intI)%><br><%end if%>
<% if recarray1(1, intI)<>"" then%><%=recarray1(1, intI)%><br><%end if%>
<% if recarray1(2, intI)<>"" then%><b><%=recarray1(2, intI)%></b><br><%end if%>
<% if recarray1(3, intI)<>"" then%><%=recarray1(3, intI)%><br><%end if%>
<% if recarray1(4, intI)<>"" then%><%=recarray1(4, intI)%><br><%end if%>
<% if recarray1(5, intI)<>"" then%><%=recarray1(5, intI)%><br><%end if%>
</b></font>
</td>
<td width="33%" valign="top" align="right"><font face="Verdana" size="2"><b>
Date: <%= Day(strCreditDate) & "/" & Month(strCreditDate) & "/" & Year(strCreditDate) %>
</b></font></td>
</tr>
<tr><td height="10"></td></tr>
</table>

<table border="1" align="center" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
<tr>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Quantity</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product code</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product name</b></td>
<%if (recarray1(16, intI)<>"1") then%>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Unit price</b></td>
<td width="20%" align="center" bgcolor="#CCCCCC" height="20"><b>Total price</b></td>
<%end if%>
</tr><%
for intN=0 to ubound(recarray2, 2)
%>
<tr>
<td align="center"><%=recarray2(2, intN)%>&nbsp;</td>
<td align="center"><%=recarray2(0, intN)%>&nbsp;</td>
<td align="center"><%=recarray2(1, intN)%>&nbsp;</td>
<%if isnull(recarray2(3, intN)) then%>
<td align="center">0.00</td><%
else%>
<td align="center"><%=FormatNumber(recarray2(3, intN), 2)%>&nbsp;</td><%
end if
if isnull(recarray2(4, intN)) then%>
<td width="20%" align="center">0.00</td><%
else%>
<td width="20%" align="center"><%=FormatNumber(recarray2(4, intN), 2)%>&nbsp;</td><%
end if%>
</tr>
<%
if not isnull(recarray2(4, intN)) then
	TotalGoods=TotalGoods+cdbl(recarray2(4, intN))	
end if
if not isnull(recarray2(5, intN)) then
	TotalVAT=TotalVAT+cdbl(recarray2(5, intN))	
end if
TotalInvoice=TotalVAT+ TotalGoods

next
%>
</table>
<table border="0" width="95%" cellspacing="0" cellpadding="0" align="center">
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">Goods total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%"><p align="center"><font size="3" face="Verdana"><b><%=FormatNumber(TotalGoods, 2)%></b></font></p></td>
</tr>
</table>

</td>
</tr>
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">VAT total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%"><p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(TotalVAT, 2)%></b></font></p></td>
</tr>
</table>

</td>
</tr>
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">Invoice total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%"><p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(TotalInvoice, 2)%></b></font></p>
</td>
</tr>
</table>
</td>
</tr>
</table>

<table border="0" width="95%" cellspacing="0" cellpadding="0"  align="center">
<tr>
	<td width="100%" height="10"></td>
</tr>
<tr>
<td width="100%">
<font face="Verdana" size="1">
<b>IMPORTANT!!</b><br>
Shortages must be reported on day of delivery. To ensure next day delivery, ring before 14:00 Mon - Fri
</font>
</td>
</tr>
<tr><td height="10"></td></tr>
<tr>
<td width="100%" align="center"><font face="Verdana" size="1"><%=strglobalinvoicefootermessage%></font></td>
</tr>
<tr><td height="10"></td></tr>
</table>

</td>
</tr>
</table>
<%
end if
end if
%>
</center>
</div>
</body>
</html>