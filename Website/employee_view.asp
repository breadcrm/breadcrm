<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->

<%
vEno= request.form("eno")
if vEno = "" then response.redirect "employee_find.asp"

set object = Server.CreateObject("bakery.employee")
object.SetEnvironment(strconnection)
set DisplayEmployeeDetail= object.DisplayEmployeeDetail(vEno)
vRecArray = DisplayEmployeeDetail("EmployeeDetail")
vRecSalaryArray = DisplayEmployeeDetail("EmployeeSalaryDetail")
set DisplayEmployeeDetail= nothing
set object = nothing

Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayFacility()	
vFacarray =  detail("Facility") 
set detail = Nothing
set object = Nothing

if not isarray(vRecArray) then response.redirect "employee_find.asp"

%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt" align="center">
<tr>
    <td width="100%"><b><font size="3">View employee - <%=vrecarray(5,0)%>&nbsp;<%=vrecarray(6,0)%><br>&nbsp;</font></b>
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td valign="top"><table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="100%">
        <tr>
          <td><b>User Type&nbsp;</b></td>
          <td>
          <%if vrecarray(1,0)= "S" then%>
          Super Administrator
          <%elseif vrecarray(1,0)= "A" then%>
          Administrator
          <%elseif vrecarray(1,0)= "U" then%>
          System User
          <%elseif vrecarray(1,0)= "N" then%>
          Basic View Only
          <%end if%>
          <b></b></td>
        </tr>
        <tr>
          <td><b>Type&nbsp;</b></td>
          <td><%=vrecarray(2,0)%></td>
        </tr>
        <tr>
          <td><b>Location&nbsp;</b></td>
          <td>
				  <%
				  for i = 0 to ubound(vFacarray,2)
                  if vrecarray(3,0)= vFacarray(0,i) then
                  	response.write vFacarray(1,i)
                  end if
                  next
                  %>
           </td>
        </tr>
        <tr>
          <td><b>Position&nbsp;</b></td>
          <td><%=vrecarray(4,0)%></td>
        </tr>
        <tr>
          <td>First name</td>
          <td><%=vrecarray(5,0)%></td>
        </tr>
        <tr>
          <td>Last name</td>
          <td><%=vrecarray(6,0)%></td>
        </tr>
        <tr>
          <td>Nick name</td>
          <td><%=vrecarray(7,0)%></td>
        </tr>
        <tr>
          <td>Address1</td>
          <td><%=vrecarray(8,0)%></td>
        </tr>
        <tr>
          <td>Address2</td>
          <td><%=vrecarray(9,0)%></td>
        </tr>
        <tr>
          <td>Town</td>
          <td><%=vrecarray(10,0)%></td>
        </tr>
        <tr>
          <td>Postcode</td>
          <td><%=vrecarray(11,0)%></td>
        </tr>
        <tr>
          <td>Telephone</td>
          <td><%=vrecarray(12,0)%></td>
        </tr>
        <tr>
          <td>Mobile</td>
          <td><%=vrecarray(13,0)%></td>
        </tr>
        <tr>
          <td>Fax</td>
          <td><%=vrecarray(14,0)%></td>
        </tr>
        <tr>
          <td>Email</td>
          <td><a href="mailto:<%=vrecarray(15,0)%>"><%=vrecarray(15,0)%></a>&nbsp;</td>
        </tr>
        <tr>
          <td>Office Extension</td>
          <td><%=vrecarray(16,0)%></td>
        </tr>
        <tr>
          <td>NI Number</td>
          <td><%=vrecarray(17,0)%></td>
        </tr>
        <tr>
          <td>Date of birth</td>
          <td><%=day(vrecarray(18,0)) & "/" & month(vrecarray(18,0)) & "/" & year(vrecarray(18,0))%></td>
        </tr>
        <tr>
          <td>Salary (�s)</td>
          <td><%=vrecarray(19,0)%></td>
        </tr>
        <tr>
          <td>Bonus (�s)</td>
          <td><%=vrecarray(24,0)%></td>
        </tr>
        <tr>
          <td>System Username</td>
          <td><%=vrecarray(20,0)%></td>
        </tr>
        <tr>
          <td>Password</td>
          <td><%=vrecarray(21,0)%></td>
        </tr>
        <tr>
          <td>Start Date</td>
          <td><%=day(vrecarray(22,0)) & "/" & month(vrecarray(22,0)) & "/" & year(vrecarray(22,0))%></td>
        </tr>
        <tr>
          <td>Status</td>
          <td>
          <%if vrecarray(23,0)= "A" then%>
          Active
          <%else%>
          Deleted
          <%end if%> </td>
        </tr>        
		</table>

 
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2" align="center">
        <tr>
          <td colspan="6"><font size="2"><b>Employee Salary Increment History</b></font></td>
        </tr>
        <tr>
          <td bgcolor="#CCCCCC"><b>Date</b></td>
          <td bgcolor="#CCCCCC"><b>Salary</b></td>

       </tr>
<% 
	if IsArray(vRecSalaryArray ) Then
		For i = 0 To ubound(vRecSalaryArray ,2)
%>         
        
        
        <tr>
          <td><%=day(vRecSalaryArray(1,i)) & "/" & month(vRecSalaryArray(1,i)) & "/" & year(vRecSalaryArray(1,i))%></td>
          <td><%=vRecSalaryArray (2,i)%></td>
        </tr>
<%
		Next
	Else
%>

        <tr>
          <td>Sorry no items found</td>
        </tr>
<%
	End if
%>        
        
        </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>