<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
vNoIngredient = 15
vDno = Request("Dno") 
Set obj = Server.CreateObject("Bakery.Inventory")
obj.SetEnvironment(strconnection)
set ObjInventory = obj.DoughTypeView(vDno)	
arDough =  ObjInventory("Dough") 
arDoughInt =  ObjInventory("Ingredients") 
Set obj = Nothing
Set ObjInventory = Nothing
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<table border="0" align="center" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3"><br>View Dough<br><br><br></font></b>
    	
	
        <table align="center" bgcolor="#CCCCCC" border="0" width="550" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">
        <tr bgcolor="#FFFFFF" height="25">
          	<td width="220"><b>Dough Name</b></td>
          	<td><%=arDough(1,0)%></td>
		</tr>
        <tr bgcolor="#FFFFFF" height="25">         
         	<td><b>Dough Type&nbsp;</b></td>
         	<td><%=arDough(3,0)%></td>
        </tr>
		<tr bgcolor="#FFFFFF" height="25">
			<td><b>Second Mix Category&nbsp;</b></td>
			<td><% if (arDough(4,0)) then%>Yes<%else%>No<%end if%></td>
		</tr>
		<% if (arDough(4,0)) then%>
		<tr bgcolor="#FFFFFF" height="25">
			<td><b>Main Dough&nbsp;</b></td>
			<td><%=arDough(5,0)%></td>
		</tr>
		<tr bgcolor="#FFFFFF" height="25">
			<td><strong>Amount of main dough to create<br>1 kilo of second mix&nbsp;</strong></td>
			<td><%=arDough(6,0)%></td>
		</tr>
		<%end if%>
	    <tr bgcolor="#CCCCCC" height="25">
          <td><b>Ingredient</b></td>
          <td ><b>Quantities required to produce 1 kg of dough</b></td>
        </tr>
		<%
		dbTotal=0	
		if IsArray(arDoughInt) Then	
					For xM = 0 to ubound(arDoughInt,2)
						if (arDoughInt(2,xM)="Y") then
						%>
						<tr bgcolor="#FFFFFF" height="25">
							<td><%=arDoughInt(1,xM)%></td>
							<td><%=arDoughInt(3,xM)%></td>
						</tr>
						<%
						end if
						dbTotal=dbTotal+formatnumber(arDoughInt(3,xM),4)					
					Next		
        End if
		
		if (arDough(4,0)) then
			dbTotal=dbTotal+arDough(6,0)
		end if
		%> 
		<tr bgcolor="#CCCCCC" height="25">
			<td><strong>Total&nbsp;</strong></td>
			<td><%=dbTotal%>&nbsp;kg</td>
		</tr>        
       </table> 
    	<p align="center"><input type="button" value=" Back " onClick="history.back()" name="B1" style="font-family: Verdana; font-size: 8pt"></p>
	</td>
  </tr>
</table>
</body>
</html>
<%
if IsArray(arDough) Then erase arDough 
if IsArray(arDoughInt) Then erase arDoughInt 
%>