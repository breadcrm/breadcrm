<% 
Option Explicit %>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<html>
<body>
<%
stop
Dim objConn, objRS, strSQL
Dim x, curValue

Set objConn = Server.CreateObject("ADODB.Connection")
'objConn.Open "DRIVER={Microsoft Excel Driver (*.xls)}; IMEX=1; HDR=NO; "&_
	'"Excel 8.0; DBQ=" & Server.MapPath("excelfile.xls") & "; "

'if using xslx (Excel 2007) use this instead:
objConn.Open "Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};"&_
	"DBQ=" & "D:\Website\Bakery\CRM\ProductListXSL\productupload_format.xlsx" & ";"

strSQL = "SELECT * FROM A1:Q10000"
Set objRS=objConn.Execute(strSQL)
Response.Write("<table border=""1"">")
Response.Write("<tr>")
For x=0 To objRS.Fields.Count-1
   Response.Write("<th>" & objRS.Fields(x).Name & "</th>")
Next
Response.Write("</tr>")
Do Until objRS.EOF
	Response.Write("<tr>")
	For x=0 To objRS.Fields.Count-1
		curValue = objRS.Fields(x).Value
		If IsNull(curValue) Then
			curValue="N/A"
		End If
		curValue = CStr(curValue)
		Response.Write("<td>" & curValue & "</td>")
	Next
	Response.Write("</tr>")
	objRS.MoveNext
Loop
objRS.Close
Response.Write("</table>")
objConn.Close
Set objRS=Nothing
Set objConn=Nothing
%>
</body>
</html>
