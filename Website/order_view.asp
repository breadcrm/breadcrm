<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" --><%
Dim Obj,ObjOrder
Dim arTypicalOrder
Dim arTypicalOrderDetails
Dim vOrderNo
Dim i

if Trim(Request.Form("OrderNo")) <> "" Then
	vOrderNo = Request.Form("OrderNo")
Else
	vOrderNo = Request.QueryString("OrderNo")
End if	

Set Obj = CreateObject("Bakery.Orders")
Obj.SetEnvironment(strconnection)
Set ObjOrder = obj.Display_TypicalOrderDetails(vOrderNo)
arTypicalOrder = ObjOrder("TypicalOrder")
arTypicalOrderDetails = ObjOrder("TypicalOrderDetails")
Set Obj = Nothing
Set ObjOrder = Nothing
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">View order<br>
      &nbsp;</font></b><%
      if IsArray(arTypicalOrder) Then%>      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="400">
        <tr>
          <td><b>Date&nbsp;</b></td>
          <td>
						<%
						if IsDate(arTypicalOrder(1,0)) Then
							Response.Write Day(arTypicalOrder(1,0)) & "/" & Month(arTypicalOrder(1,0)) & "/" & Year(arTypicalOrder(1,0))						 							 	
						End if%>                 
          </td>
        </tr>
        <tr>
          <td><b>Day&nbsp;</b></td>
          <td><%=arTypicalOrder(2,0)%></td>
        </tr>
        <tr>
          <td>Delivery time</td>
          <td><%=arTypicalOrder(3,0)%></td>
        </tr>
        <tr>
          <td>Sample order</td>
          <td><%=arTypicalOrder(4,0)%></td>
        </tr>
        <tr>
          <td><b>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b></td>
          <td><%=arTypicalOrder(5,0)%></td>
        </tr>
        <tr>
          <td>Code</td>
          <td><%=arTypicalOrder(6,0)%></td>
        </tr>
        <tr>
          <td>Address</td>
          <td><%=arTypicalOrder(7,0)%></td>
        </tr>
        <tr>
          <td></td>
          <td><%=arTypicalOrder(8,0)%></td>
        </tr>
        <tr>
          <td>Town</td>
          <td><%=arTypicalOrder(9,0)%></td>
        </tr>
        <tr>
          <td>Postcode</td>
          <td><%=arTypicalOrder(10,0)%></td>
        </tr>
        <tr>
          <td>Telephone</td>
          <td><%=arTypicalOrder(11,0)%></td>
        </tr>
        <tr>
          <td>Contact Name</td>
          <td><%=arTypicalOrder(12,0)%></td>
        </tr>
        <tr>
          <td>Contact Phone</td>
          <td><%=arTypicalOrder(13,0)%></td>
        </tr>
        <tr>
          <td>Name</td>
          <td><%=arTypicalOrder(14,0)%></td>
        </tr>
        <tr>
          <td>Phone</td>
          <td><%=arTypicalOrder(15,0)%></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td></td>
        </tr>
        <tr>
          <td>Purchase Order</td>
          <td><%=arTypicalOrder(17,0)%></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table><%
   End if%>   

      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="33%" bgcolor="#CCCCCC"><b>Product Code</b></td>
          <td width="33%" bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td width="33%" bgcolor="#CCCCCC"><b>Qty</b></td>
        </tr><%
        
         if IsArray(arTypicalOrderDetails) Then
					for i = 0 to UBound(arTypicalOrderDetails,2)%>						
						<tr>
						  <td width="33%"><%=arTypicalOrderDetails(0,i)%></td>
						  <td width="33%"><%=arTypicalOrderDetails(1,i)%></td>
						  <td width="33%"><%=arTypicalOrderDetails(3,i)%></td>
						</tr><%
					Next
				End if%>			
        <tr>
          <td width="133%" colspan="3">
            <table border="0" width="100%">
              <tr>
                <td width="33%" align="center"></td>
                <td width="33%" align="center"><input type="button" value="Go Back" name="Go Back" onClick="document.location.href='order_find.asp'" style="font-family: Verdana; font-size: 8pt"></td>
                 <form method="POST" action="order_finish.asp">
                 <td width="34%" align="center"></td>
                 </form>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>


  </center>
</div>


</body>

</html>
