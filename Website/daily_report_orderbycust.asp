<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Response.Buffer
Dim intI
Dim intJ
Dim intF
Dim intN
Dim intQ
Dim intP
Dim intD
Dim intRow
Dim delType
Dim delDate
Dim facilityNo
Dim arOrdSheetCustomerDetails
Dim arCustomerList
Dim arOrdSheetCustomerList
Dim intTotal
intN = 0
intJ = 0
intQ = 0
intP = 1
intTotal = 0
intF = 0
intD = 0

delDate= request.form("deldate")
facilityNo= request.form("facility")
arrayFacility=split(facilityNo,",")
delType=Request.Form("deltype")
If delType = "1" Then
	delType = "Morning"
ElseIf delType = "2" Then
	delType = "Noon"
ElseIf delType = "3" Then
	delType = "Evening"
End If
if delDate= "" or facilityNo = "" or delType="" then response.redirect "daily_report.asp"
%>

<html>
<head>
<title>ORDERING SHEET BY CUSTOMER</title>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<p>&nbsp;</p>
<p align="center"><font size="3"><b>ORDERING SHEET BY CUSTOMER - <%if deltype="All" then%>(Morning, Noon, Evening)<%else%>(<%=deltype%>)<%end if%></b></font></p>
<%

if isarray(arrayFacility) then 
for i = 0 to ubound(arrayFacility)
set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
set DisplayReport= object.Display_OrderingSheetByCustomer(trim(arrayFacility(i)), delType, delDate)
arOrdSheetCustomerDetails = DisplayReport("OrdSheetCustomerDetails")
arCustomerList = DisplayReport("CustomerList")
arOrdSheetCustomerList = DisplayReport("OrdSheetCustomerList")
set DisplayReport= nothing
set object = nothing

if IsArray(arCustomerList) And IsArray(arOrdSheetCustomerList) Then
	intP = 1
	For intI = 0 To ubound(arCustomerList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetCustomerList, 2)
			if (arCustomerList(0, intI) = arOrdSheetCustomerList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
				End if
				if intN = 0 Then
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) And intRow + 5 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetCustomerList, 2) Then
					intTotal = 0
					intRow = intRow + 1
				End if
				if intI <= ubound(arCustomerList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arCustomerList, 2) Then
						intP = intP + 1
					End if
				End if
				intQ = 0				
			End if
			Response.Flush()
		Next
		intD = intD + 1
		Response.Flush()
	Next
Else
	intP = 1
End if

intN = 1
intJ = 0
intQ = 0
intF = 0
intD = 0
intTotal = 0

if IsArray(arOrdSheetCustomerDetails) Then
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
<td>

<b><%= arOrdSheetCustomerDetails(0, 0) %><br>
To the attention of: <%= arOrdSheetCustomerDetails(1, 0) %><br>
By fax number: <%= arOrdSheetCustomerDetails(2, 0) %></b><br><br>
Date of report: <%= Day(Now()) & "/" & Month(Now()) & "/" & Year(Now()) %><br>
To be produced and ready to be delivered on <%= Day(FormatDateTime(delDate, 2)) & "/" & Month(FormatDateTime(delDate, 2)) & "/" & Year(FormatDateTime(delDate, 2)) %> <%if deltype="Morning" then%> before 05:00 AM<%end if%><br><br>
Number of pages for this report: <%= intP %><br><br>
</td>
</tr>
</table><%
End if
if IsArray(arCustomerList) And IsArray(arOrdSheetCustomerList) Then
	For intI = 0 To ubound(arCustomerList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		
		if( arCustomerList(0, intI) = 2737) then
		stop
		end if 
		
        %>
        <table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
	        <tr>
	        <td height="20"><b>Customer Code: <%= arCustomerList(0, intI) %></b></td>
	        <td height="20"><b>Customer Name: <%= arCustomerList(1, intI) %></b></td>
	        <td height="20" align="right"><b>Van No.: <%= arCustomerList(2, intI) %></b></td>
	        </tr>
        </table><%
		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetCustomerList, 2)
			if (arCustomerList(0, intI) = arOrdSheetCustomerList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
%>
<table align="center" border="1" width="90%" cellspacing="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000" cellpadding="0"><%
				End if
				if intN = 0 Then
				
%>
	<tr>
	<td width="11%" height="20"><b>Product code</b></td>
	<td width="56%" height="20"><b>Product name</b></td>
	<td width="8%" height="20"><b>Quantity</b></td>
	</tr><%
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
				
		if (arCustomerList(0, intI) = arOrdSheetCustomerList(0, intJ)) Then		'Add TH
	%>
	<tr>
	<td width="11%" height="20"><%= arOrdSheetCustomerList(4, intJ) %></td>
	<td width="56%" height="20"><%= arOrdSheetCustomerList(5, intJ) %></td>
	<td width="8%" height="20"><%= arOrdSheetCustomerList(6, intJ) %></td>
	</tr><%
				intTotal = intTotal + arOrdSheetCustomerList(6, intJ)
				
			End IF	'add Th
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) And intRow + 5 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			'if intJ = ubound(arOrdSheetCustomerList, 2) Or intRow > PrintPgSize Then
            if intJ = ubound(arOrdSheetCustomerList, 2) Or intRow > PrintPgSize Then
			intF = 1
			'Added by thilina
				'if (arCustomerList(0, intI) = arOrdSheetCustomerList(0, intJ)) Then
				    'intF = 0
				'Else
				   ' intF = 1
				'End IF
				'-------------
				if intJ = ubound(arOrdSheetCustomerList, 2) Then
				'if (arCustomerList(0, intI) = arOrdSheetCustomerList(0, intJ)) Then 'Add Th
%>
	<tr>
	<td width="67%" height="20" colspan="2" align="right"><b>Total&nbsp; </b></td>
	<td width="8%" height="20"><b><%= intTotal %>&nbsp;</b></td>
	</tr><%
					intTotal = 0
					
					'End If 'Add Th
					intRow = intRow + 1
				End if
%>
</table>
<br>
<%
				if intI <= ubound(arCustomerList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arCustomerList, 2) Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
					End if
				End if
				intQ = 0
			End if
			Response.Flush()
		Next
		intD = intD + 1
		Response.Flush()
	Next
%>

<%	
else
%>
<table align="center" border="0" width="90%" cellspacing="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000" cellpadding="0">
	<tr>
	<td width="100%" height="20" colspan="3"><b>Sorry no records found</b></td>
	</tr>
</table><%
End if
%>
<p><hr></p>
<% if i<ubound(arrayFacility) then%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%
end if
next
end if
%>
</body>
</html><%
If IsArray(arOrdSheetCustomerDetails) Then erase arOrdSheetCustomerDetails
If IsArray(arCustomerList) Then erase arCustomerList
If IsArray(arOrdSheetCustomerList) Then erase arOrdSheetCustomerList
%>