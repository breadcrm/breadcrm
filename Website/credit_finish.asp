<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%
vinvoiceno = replace(request.form("invoiceno"),"'","''")
vnumberofmultiproducts = replace(request.form("numberofmultiproducts"),"'","''")

vtotal = replace(request.form("total"),"'","''")
vCustNo= request.form("vCustNo")
invoiceno= request.form("vinvoiceno")
strDelDate= request.form("vDelDate")

strnameofpersonmakingcomplaint=request("nameofpersonmakingcomplaint")
if vinvoiceno = "" then response.redirect "credit_new.asp"

strQtyFlag = False
strReasonFlag = False
for i = 0 to vtotal
	vismultipleproduct=request.form("ismultipleproduct" & i & "")
	vmultipleproductcount=request.form("multipleproductcount" & i & "")
	vQno = replace(request.form("Qno" & i & ""),"'","") 
	
	vQty = replace(replace(replace(request.form("Qty" & i & ""),"'",""), "-", "vQty"), ".", "vQty")
	vTotQty = replace(replace(replace(request.form("TotalQty" & i & ""),"'",""), "-", "vTotQty "), ".", "vTotQty")
	vActQty = replace(replace(replace(request.form("QtyCredited" & i & ""),"'",""), "-", "vActQty "), ".", "vActQty ")

	vRson = replace(request.form("Reason" & i & ""),"'","''")

	if trim(vQty) <> "" Then
		vNewQty = cint(vActQty)+cint(vQty)	
	End if 

	if vNewQty<>"" and vTotQty<>"" then
		if (clng(vNewQty) > clng(vTotQty)) then
			strQtyFlag = False
			Exit For
		else
			strQtyFlag = True
			if vRson = "Select" or vRson = "" then
				strReasonFlag = False
				Exit For			
			else
				strReasonFlag = True					
			end if
		end if
	end if
	'
'	response.write(vnumberofmultiproducts)
'	response.end
	'For Multiple Products Begin
	if ((CInt(vismultipleproduct)=1) and ((vnumberofmultiproducts>0) and (strQtyFlag = False or strReasonFlag = False))) then
		for a = 0 to vnumberofmultiproducts
			vQno = replace(request.form("Qno" & i & a & ""),"'","") 

			vQty = replace(replace(replace(request.form("Qty" & i & a & ""),"'",""), "-", "vQty"), ".", "vQty")
			vTotQty = replace(replace(replace(request.form("TotalQty" & i & a & ""),"'",""), "-", "vTotQty "), ".", "vTotQty")
			vActQty = replace(replace(replace(request.form("QtyCredited" & i & a & ""),"'",""), "-", "vActQty "), ".", "vActQty ")
		
			vRson = replace(request.form("Reason" & i & a & ""),"'","''")
			
			if trim(vQty) <> "" Then
				vNewQty = cint(vActQty)+cint(vQty)	
			End if 
			'response.write ("*m-" & vTotQty & "-" & vNewQty  & "<br>")
			if vNewQty<>"" and vTotQty<>"" then
				if (clng(vNewQty) > clng(vTotQty)) then
					strQtyFlag = False
					Exit For
				else
					strQtyFlag = True
					if vRson = "Select" or vRson = "" then
						if (strReasonFlag = False) then
							strReasonFlag = False
							Exit For
						end if		
					else
						strReasonFlag = True					
					end if
				end if
			end if
			vNewQty = ""
			
			' Multiproduct inside
			for b = 0 to vmultipleproductcount
			vQno = replace(request.form("Qno" & i & b & ""),"'","") 

			vQty = replace(replace(replace(request.form("Qty" & i & b & ""),"'",""), "-", "vQty"), ".", "vQty")
			vTotQty = replace(replace(replace(request.form("TotalQty" & i & b & ""),"'",""), "-", "vTotQty "), ".", "vTotQty")
			vActQty = replace(replace(replace(request.form("QtyCredited" & i & b & ""),"'",""), "-", "vActQty "), ".", "vActQty ")
		
			vRson = replace(request.form("Reason" & i & b & ""),"'","''")
			
			if trim(vQty) <> "" Then
				vNewQty = cint(vActQty)+cint(vQty)	
			End if 
			'response.write ("*mmm-" & vTotQty & "-" & vNewQty  & "<br>")
			if vNewQty<>"" and vTotQty<>"" then
				if (clng(vNewQty) > clng(vTotQty)) then
					strQtyFlag = False
					Exit For
				else
					strQtyFlag = True
					if vRson = "Select" or vRson = "" then
						if (strReasonFlag = False) then
							strReasonFlag = False
							Exit For
						end if		
					else
						strReasonFlag = True					
					end if
				end if
			end if
			vNewQty = ""
			next
			'Multiproduct end
		next
	end if
	
	'For Multiple Products End
	
	vNewQty = ""

next
'response.write ("Final - " & strQtyFlag & "<br>")
'response.end
if strQtyFlag = False then
	Response.Redirect "credit_new1.asp?vCustNo=" & vCustNo & "&vinvoiceno=" & invoiceno & "&error=Qty"
end if
if strReasonFlag = False then
	Response.Redirect "credit_new1.asp?vCustNo=" & vCustNo & "&vinvoiceno=" & invoiceno & "&error=reason"
end if

set object = Server.CreateObject("bakery.credit")
object.SetEnvironment(strconnection)
vCreditNo = object.SaveCredit(vinvoiceno)

if isnumeric(vCreditNo) then
	if 	strQtyFlag = True then
		strDetails="Credited Invoice Delivery Date: " & strDelDate
		strProductsAmended=""
		for i = 0 to vtotal
			vmultipleproductcount=request.form("multipleproductcount" & i & "")
			vismultipleproduct=request.form("ismultipleproduct" & i & "")
			vPno = replace(request.form("Pno" & i & ""),"'","''") 
			vQty = replace(request.form("Qty" & i & ""),"'","''") 
			MainvQty=vQty
			if (MainvQty="") then
				MainvQty=0
			end if
			vTotalQty = replace(request.form("TotalQty" & i & ""),"'","''") 
			vRson = replace(request.form("Reason" & i & ""),"'","''")
			vOther = replace(request.form("Other" & i & ""),"'","''")
			vComplaintResponsibility = replace(request.form("ComplaintResponsibility" & i & ""),"'","''")
			
			if (vComplaintResponsibility="") then
				vComplaintResponsibility=0
			end if
			
			if not (isnumeric(vComplaintResponsibility)) then
			 vComplaintResponsibility=0
			end if
			

			if 	vQty <>"" and vPno <> "" and vRson<>"Select" and vQty <>"0" then
				SaveCreditDetail = object.SaveCreditDetail(vCreditNo,vPno, vQty,vRson,vOther,vComplaintResponsibility,strnameofpersonmakingcomplaint)
				if (strProductsAmended="") then
					strProductsAmended=strProductsAmended & "; Products Credited: " & vPno
				else
					strProductsAmended=strProductsAmended & ", " & vPno
				end if	
			end if
			
			'For Multiple Products Begin
			if (CInt(vismultipleproduct)=1) then
			'response.write(vmultipleproductcount)
				if (vmultipleproductcount>0) then
				vPnoMain = vPno
				vQtyMain = vTotalQty
			
				'multiproduct
				vwarnning=true
				newqtymulold=1000
				
				for b = 0 to vmultipleproductcount-1
					vPno = replace(request.form("Pno" & i & b & ""),"'","''") 
					vQty = replace(request.form("Qty" & i & b & ""),"'","''") 
					vsubTotalQty = replace(request.form("TotalQty" & i & b & ""),"'","''") 
					vRson = replace(request.form("Reason" & i & b & ""),"'","''")
					vOther = replace(request.form("Other" & i & b & ""),"'","''")
					vComplaintResponsibility = replace(request.form("ComplaintResponsibility" & i & b & ""),"'","''")
					if (vComplaintResponsibility="") then
						vComplaintResponsibility=0
					end if
					
					if not (isnumeric(vComplaintResponsibility)) then
					 vComplaintResponsibility=0
					end if
							
					if 	vQty <>"" and vPno <> "" and vRson<>"Select" then
						if (vQty="") then
							vQty=0
						end if
						if (vQtyMain="") then
							vQtyMain=0
						end if		
					
						veneteredvalue=vsubTotalQty/vQtyMain
						
						newqtymul=vQty\veneteredvalue
						
						if (newqtymulold>newqtymul) then
							newqtymulold=newqtymul
						end if
						'response.write("vsubTotalQty-" & vsubTotalQty & "<br>")
						'response.write("vQtyMain-" & vQtyMain & "<br>")
						'response.write("veneteredvalue-" & veneteredvalue & "<br>")
						'response.write("vQty-" & vQty & "<br>")
						'response.write("veneteredvalue-" & veneteredvalue & "<br>")
						'response.write("newqtymul-" & newqtymul & "<br>")
						'response.write("newqtymulold-" & newqtymulold & "<br>")
						'response.write("newqtymul-" & newqtymul & "<br>")
						
						'response.end
							
						if (CInt(vQty)<CInt(veneteredvalue)) then
							vwarnning=false
						end if	
					else
						vwarnning=false	
					end if
				next
				
				if (vwarnning=true) then
					if 	newqtymulold <>"" and vPnoMain <> "" and vRson<>"Select" and newqtymulold <>"0" then
						totmainqty=CInt(newqtymulold)+CInt(MainvQty)
						
						if (MainvQty>0 and newqtymulold>0) then
							SaveCreditDetail = object.UpdateMultipleProductCreditDetail(vCreditNo,vPnoMain,totmainqty)
						else
							SaveCreditDetail = object.SaveCreditDetail(vCreditNo,vPnoMain, totmainqty,vRson,vOther,vComplaintResponsibility,strnameofpersonmakingcomplaint)
						end if
						
						if (strProductsAmended="") then
							strProductsAmended=strProductsAmended & "; Products Credited: " & vPnoMain
						else
							strProductsAmended=strProductsAmended & ", " & vPnoMain
						end if
				
						for b = 0 to vmultipleproductcount-1
						vPno = replace(request.form("Pno" & i & b & ""),"'","''") 
						vQty = replace(request.form("Qty" & i & b & ""),"'","''") 
						vsubTotalQty = replace(request.form("TotalQty" & i & b & ""),"'","''") 
						vRson = replace(request.form("Reason" & i & b & ""),"'","''")
						vOther = replace(request.form("Other" & i & b & ""),"'","''")	
						vComplaintResponsibility = replace(request.form("ComplaintResponsibility" & i & b & ""),"'","''")						
						if (vComplaintResponsibility="") then
							vComplaintResponsibility=0
						end if
						
						if not (isnumeric(vComplaintResponsibility)) then
						 vComplaintResponsibility=0
						end if
						if 	vQty <>"" and vPno <> "" and vRson<>"Select" then
							if (vQty="") then
								vQty=0
							end if
							if (vQtyMain="") then
								vQtyMain=0
							end if
							
							veneteredvalue=vsubTotalQty/vQtyMain
							
							newsubQty=vQty-(newqtymulold*veneteredvalue)
							
							if (newsubQty>0) then
								SaveCreditDetail = object.SaveCreditDetail(vCreditNo,vPno, newsubQty,vRson,vOther,vComplaintResponsibility,strnameofpersonmakingcomplaint)	
								
								if (strProductsAmended="") then
									strProductsAmended=strProductsAmended & "; Products Credited: " & vPno
								else
									strProductsAmended=strProductsAmended & ", " & vPno
								end if						
							end if
							
						end if
					next
						
					end if	
				else 'vwarnning=false
					for b = 0 to vmultipleproductcount-1
						vPno = replace(request.form("Pno" & i & b & ""),"'","''") 
						vQty = replace(request.form("Qty" & i & b & ""),"'","''") 
						vRson = replace(request.form("Reason" & i & b & ""),"'","''")
						vOther = replace(request.form("Other" & i & b & ""),"'","''")
						vComplaintResponsibility = replace(request.form("ComplaintResponsibility" & i & b & ""),"'","''")
						
						if (vComplaintResponsibility="") then
							vComplaintResponsibility=0
						end if
						
						if not (isnumeric(vComplaintResponsibility)) then
						 vComplaintResponsibility=0
						end if
			
						if 	vQty <>"" and vPno <> "" and vRson<>"Select" and vQty <>"0" then
							
							SaveCreditDetail = object.SaveCreditDetail(vCreditNo,vPno, vQty,vRson,vOther,vComplaintResponsibility,strnameofpersonmakingcomplaint)
							if (strProductsAmended="") then
								strProductsAmended=strProductsAmended & "; Products Credited: " & vPno
							else
								strProductsAmended=strProductsAmended & ", " & vPno
							end if	
						end if
					next
				end if
				end if
				'multiproduct end
			end if
			
			'For Multiple Products End
			
		next
		strDetails=strDetails & strProductsAmended
		LogAction "Invoice Credited",strDetails,vCustNo
	end if
end if

set object = nothing
'response.end
response.redirect "credit_finish1.asp?crno="&vCreditNo

%>