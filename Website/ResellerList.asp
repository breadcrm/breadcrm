<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%

dim strCustomerReseller,CustomerResellerCol
vPageSize = 100

vFiltervalue = replace(Request.Form("Filtervalue"),"'","''")
vFilter =  replace(Request.Form("Filter") ,"'","''")

vpagecount = Request.Form("pagecount") 

select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(vpagecount) then	
			session("Currentpage")  = session("Currentpage")+1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage")-1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select
vSessionpage = session("Currentpage")

vsessionpage = 0

Set obj = server.CreateObject("bakery.Reseller")
obj.SetEnvironment(strconnection)
if request.form("delete")="yes" then
	vEno=request.form("eno")
	delete = obj.DeleteReseller(vEno)
	stop
end if

Set objUser = obj.DisplayReseller(vFilter, vFilterValue, vsessionpage, vPageSize)

arResellers =  objUser("Resellers")
vpagecount =   objUser("Pagecount") 
vRecordCount = objUser("Recordcount")

Set objUser = Nothing
Set obj = Nothing
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script LANGUAGE="javascript" src="includes/script.js"></script>


<script Language="JavaScript">
<!--
function validate(frm){
	if(confirm("Would you like to delete this reseller?")){
		 document.frmDelete.submit();		
	}
}
//-->
</Script>


<SCRIPT LANGUAGE="Javascript">
<!--
function SearchValidation(){
	if(document.frmForm.Filtervalue.value != ""){ 
		if(CheckEmpty(document.frmForm.Filtervalue.value) == "true"){ 
			alert("Please enter a valid name");
			document.frmForm.Filtervalue.focus();
			return false;
		}
		if(document.frmForm.Filter.selectedIndex == 0){
			if(isInt(document.frmForm.Filtervalue.value) == false){
				alert("Please enter a valid Reseller id");
				return false;
			}
		}
	}
	return true;
}
//-->
</SCRIPT>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;

<table border="0" align="center" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Find Reseller</font><br></b>


<table align="center" border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 10pt" bordercolor="#111111">
<form method="post" action="ResellerList.asp" name="frmForm">
    <tr> 
      <td><b>Search</b> </td>
      <td>
        <select name="Filter">
          <option value=""> - - - Select - - -</option>
		  <option value="RID" <%if Trim(vFilter) = "RID" Then Response.Write "selected"%>>Reseller ID</option>
          <option value="ResellerName" <%if Trim(vFilter) = "ResellerName" Then Response.Write "selected"%>>Reseller Name</option>
        </select>
      </td>
      <td>
        <input type="text" name="Filtervalue" value="<%=Request.Form("Filtervalue")%>" size="20">
      </td>
      <td>
        <input type="submit" name="Submit" value="Submit" onClick="return SearchValidation();">
      </td>
    </tr>
</form>
</table>
<br>
<p><b><a href="Reseller_add.asp"><font size="2" color="#000066">Add New Reseller</font></a></font></b></p>
<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" cellspacing="1" cellpadding="4" bgcolor="#666666">

      <%if vsessionpage<> 0 then%>
  <tr bgcolor="#FFFFFF">
    <td align="right" colspan="7" width="671">		
		<%
		If isArray(arResellers) Then
			If (session("Currentpage") * vPageSize) > vRecordCount Then
				vMiddleVal  = vRecordCount
			Else
				vMiddleVal  = (session("Currentpage") * vPageSize)
			End if

			vFirstVal = (session("Currentpage") * vPageSize) - (vPageSize - 1)
			Response.Write "Resellers " & vFirstVal & " To " & vMiddleVal & " of " & vRecordCount
		End if
		%>
    </td>
  </tr>
	<%end if%>
	<tr>
	  <td width="75" bgcolor="#CCCCCC"><b>Reseller ID</b></td>
	  <td width="100" bgcolor="#CCCCCC"><b>Reseller Name</b></td>
	  <td width="150" bgcolor="#CCCCCC"><b>Company Logo</b></td>
	  <td width="280" bgcolor="#CCCCCC"><b>Customer Code with Name</b></td>
	  <td width="70" bgcolor="#CCCCCC"><b>Price Band</b></td>
	  <td bgcolor="#CCCCCC" align="center"><b>Action</b></td>
	</tr>
<% 
	if IsArray(arResellers) Then
		arrPriceBand=array ("","A","B","C","D","A1","F","","","","","","","","","","","","F1")
		dim objBakery1
		set objBakery1 = server.CreateObject("Bakery.Reseller")
		objBakery1.SetEnvironment(strconnection)
		For i = 0 To ubound(arResellers,2)
		set CustomerResellerCol = objBakery1.CustomersInReseller(arResellers(0,i))
		strCustomerReseller = CustomerResellerCol("Customers")
%>        
        <tr bgcolor="#FFFFFF">
          <td align="center" valign="top"><%=arResellers(0,i)%></td>
          <td valign="top"><%=arResellers(1,i)%></td>
		  <td align="left" valign="top"><img src="images/ResellerLogos/<%=arResellers(3,i)%>"></td>
		  <td align="left" valign="top"><%=strCustomerReseller%></td>
          <td align="center" valign="top"><%=arrPriceBand(arResellers(2,i))%>
		  <% if arrPriceBand(arResellers(2,i))="F" or arrPriceBand(arResellers(2,i))="F1" then%>
		  	: <%=arResellers(4,i)%>
		  <%end if%>
		  </td>
		  <td valign="top">
		  <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse">
		  <form method="POST" action="Reseller_view.asp">
		  <td align="left" valign="top" width="60">
          <input type = "hidden" name="eno" value = "<%=arResellers(0,i)%>">
		  <input type="submit" value="View" name="B1" style="font-family: Verdana; font-size: 8pt">
		  </td>
          </form>
		  <form method="post" action="Reseller_add.asp">
		  <td align="center" valign="top" width="60">
		  	<input type = "hidden" name="RID" value = "<%=arResellers(0,i)%>">
			<input type="submit" value="Edit" name="B1" style="font-family: Verdana; font-size: 8pt">
		  </td>
          </form>
		  <form method="POST" name="frmDelete" action="ResellerDelete.asp">
		  <td align="right" valign="top" width="60">
          <input type = "hidden" name="name" value = "<%=arResellers(5,i)%>&nbsp;<%=arResellers(6,i)%>">
		  <input type = "hidden" name="delete" value = "yes">
		  <input type = "hidden" name="RID" value = "<%=arResellers(0,i)%>">
		  <input type="submit" onClick="return confirm('Do you want to delete this record?')" value="Delete" name="B1" style="font-family: Verdana; font-size: 8pt">
		  </td>
		  </form>
		  </table>
		  </td>
        </tr>
<%
		Next
			set CustomerResellerCol = nothing
		set objBakery1 = nothing
	Else
%>

        <tr>
          <td colspan="7" align="center" height="25" bgcolor="#FFFFFF">Sorry no resellers found</td>
        </tr>
<%
	End if
%>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<%
If IsArray(arResellers) Then Erase arResellers
%>