<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/top.inc" -->
<%
'stop
Dim intI
Dim intJ
Dim intN
Dim intRow
Dim intF
Dim intTotal
intN = 0
intTotal = 0

pCode = ""
deldate= request.form("deldate")
'This is haed coded but need to replace the id for new facility.
facility= 34
dtype=Request.Form("deltype")
if deldate= "" or facility = "" or dtype = "" then response.redirect "daily_report.asp"

set object = Server.CreateObject("bakery.daily")
set col1= object.DailySourDoughOrderSheetViewReport(deldate,facility,dtype)
vRecArray = col1("SourDoughOrdersheet")

set maxColCount= object.DailySourDoughGailOrderSheetViewReportMaxColumn(deldate,facility,dtype)
vMaxColCountArray = maxColCount("MaxColumnCount")

MaxColumnCount = 0
if isarray(vMaxColCountArray) then
	MaxColumnCount = vMaxColCountArray(0,0) - 1
end if

if isarray(vrecarray) then
  totrecs = ubound(vrecarray,2)+1
else
  totrecs = 0
end if
curpage=0
totpages=0
if totrecs <> 0 then
  if clng(totrecs) mod 20  = 0 then
    totpages = clng(totrecs)/20
    curpage=1
  else
    totpages = fix(clng(totrecs)/20) +  1
    curpage=1
  end if
end if

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
set object = nothing
intF = 1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round((ubound(vRecArray, 2)/PrintPgSize) + 0.49)
	End If
End If
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px} input#btnEmail{display: none;}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<p>&nbsp;</p>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3">48 Hours Report of GAIL's central Kitchen<br>
      &nbsp;</font></b>
      <table border="0" width="85%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>Facility: </b><%=Facilityname %><br>
			<b>To the Attention of: </b><%=attn%><br>
			<b>By Fax Number: </b><%=fax%><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
            Number of pages for this report: <%=intF%><br><br>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
	<td width="100%">
		<table border="0" width="85%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; margin-left:75px">
			<tr>
				<td>
					<form method = "post" action="ExportToExcelFile_48hours_report_gail_central_kitchen.asp" name="form1">
						<input type="hidden" name="ex_deldate" value="<%=deldate%>">
						<input type="hidden" name="ex_dtype" value="<%=dtype%>">
						<input type="submit" id="btnEmail" value="Send Report" name="B1" style="font-family: Verdana; font-size: 8pt">
					</form>
				</td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<%
if isarray(vRecArray) then 
	for intI = 1 to intF
%>
<table align="center" border="2" width="854" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
<%
		if intI = 1 then
		%>
              <tr>
              <td width="99" height="20"><b>Product code</b></td>
              <td width="401" height="20"><b>Product</b></td>
              <td width="77" height="20"><b>Size</b></td>
              <td width="70" height="20"><b>Total Production</b></td>
              </tr><%
		end if
        For intJ = 1 To 41 'PrintPgSize - changed by selva 21 Nov 2006
			if intN <= ubound(vRecArray, 2) Then
			pCode = vRecArray(0,intN)
			set object1 = Server.CreateObject("bakery.daily")
			set col1Customers= object1.DailySourDoughGailOrderSheetViewReport(pCode,deldate)
			vCustomersArray = col1Customers("SourDoughOrdersheet")
		%>
            <tr>
            <td width="99" height="20"><%=vRecArray(0,intN)%></td>
            <td width="451" height="20"><%=vRecArray(1,intN)%></td>
            <td width="77" height="20"><%=vRecArray(2,intN)%></td>
            <td width="70" height="20"><%=vRecArray(3,intN)%></td>
			<%
			if isarray(vCustomersArray) then
				For intC = 0 to MaxColumnCount
				if intC <= ubound(vCustomersArray, 2) Then
			%>
				<td width="90" height="20"><%=vCustomersArray(0,intC)%> (<%=vCustomersArray(1,intC)%>)</td>
			<%
				Else
				%>
				<td width="90" height="20"></td>
				<%
				End If
				Next
			end if
			set object1 = nothing
			%>
            </tr>
			<%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
           <tr>
            <td width="401" colspan="3" height="20" align="right"><b>Total&nbsp; </b></td>
            <td width="70" height="20"><b><%=intTotal%>&nbsp;</b></td>
            </tr>
			<%
			End if
			intN = intN + 1
		Next
%>
</table>
<%
	if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%
	End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2">Sorry no items found</td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
</body>
</html>