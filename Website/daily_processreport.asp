<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%
Dim vNoDays
Dim DelDay,DelMonth,DelYear
Dim SysDay,SysMonth,SysYear
Dim arSplit
Dim ErrorTag
Dim ToolBox
Dim arNo 
dim delDatePDF
dim sDay,sMonth,sYear
stop
repdate = replace(request.form("deldate"),"'","''")
deldate = replace(request.form("deldate"),"'","''")
sDay = Request.Form("day")
sMonth = Request.Form("month")
sYear = Request.Form("year")

if Trim(repdate) = "" or trim(deldate)  = ""   then response.redirect "LoginInternalPage.asp"

Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayTypeDes()	
vType1Array =  detail("Type3ForPacking")
vType2Array =  detail("Type4ForPacking") 

arSplit = Split(deldate,"/")

DelDay = CInt(arSplit(0))
DelMonth = CInt(arSplit(1))
DelYear = CInt(arSplit(2))

delDatePDF=DelDay &"/" & DelMonth  &"/" &  DelYear

SysDay = CInt(Day(date()))  
SysMonth = CInt(Month(Date()))
SysYear = CInt(year(Date()))

arNo = Array(31,28,31,30,31,30,31,31,30,31,30,31)
ErrorTag = True
ToolBox=false
if DelMonth = SysMonth and  DelYear = SysYear Then	
	if DelDay - SysDay  > 5 Then		
		ErrorTag = False
	End if
	
	if DelDay - SysDay=1 Then		
		ToolBox = true
	End if	
	
Elseif  DelMonth - SysMonth = 1 and  DelYear = SysYear Then 'This is for only next month		
	if (cint(arNo(SysMonth-1)) - SysDay) + DelDay   > 5 Then		
		ErrorTag = False
	End if
	if (cint(arNo(SysMonth-1)) - SysDay) + DelDay=1 Then
		ToolBox = true
	End if			 	
	
Elseif DelMonth > SysMonth And  DelYear = SysYear Then		
		ErrorTag = False		
Elseif DelYear - SysYear = 1 And DelMonth = 1 and  SysMonth = 12 Then			
	if (CInt(arNo(SysMonth-1)) - SysDay)  +  DelDay > 5 Then
		ErrorTag = False		
	End if
	if (CInt(arNo(SysMonth-1)) - SysDay)  +  DelDay=1 Then
		ToolBox = true
	End if
Elseif DelYear > SysYear  Then		 	
	ErrorTag = False
End if

if Trim(repdate) = "" or Trim(deldate)  = ""   then response.redirect "LoginInternalPage.asp"
if ErrorTag = false  Then response.redirect "daily_process.asp?ErrorTag=NoOfDayTag"

LogAction "Daily Process Start..", "Date:" & deldate , ""

set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
processorders = object.processorders(repdate , deldate)
set object = nothing


'Generate PDF Docs

Dim pdfFileGenArg

pdfFileGenArg = CStr(DelYear)

If DelMonth < 10 Then
    pdfFileGenArg = pdfFileGenArg & "-0" & Cstr(DelMonth)
Else
    pdfFileGenArg = pdfFileGenArg & "-" & Cstr(DelMonth)
End if
If DelDay < 10 Then
    pdfFileGenArg = pdfFileGenArg & "-0" & Cstr(DelDay)
Else
    pdfFileGenArg = pdfFileGenArg & "-" & Cstr(DelDay)
End if

'On Error GoTo 0

Set SoapRequest = Server.CreateObject("MSXML2.XMLHTTP") 

ScriptPath = Request.ServerVariables("SCRIPT_NAME")
hostName = Request.ServerVariables("SERVER_NAME")

i = InStrRev(ScriptPath, "/")
If i > 0 Then    
	hostAddress = Left(ScriptPath, i - 1)
End If

SoapURL = "http://" & hostName & hostAddress & "/PDFGenerator/BakeryPDFGenSrv.asmx/GenerateDailyPDFDocs"
SoapRequest.Open "POST", SoapURL , False 
SoapRequest.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
SoapRequest.Send "deldate=" & pdfFileGenArg

if (ToolBox=true) then
	'Tool Box 
	Dim objHttp, strQuery
	set objHttp = Server.CreateObject("Msxml2.ServerXMLHTTP")
	objHttp.SetTimeouts 600000, 600000, 15000, 15000
	'strQuery = "http://bakery.adsgrp.com/ToolBox/Export.aspx?syear=2014&smonth=10&sdate=25"
	strQuery = "http://" & hostName & hostAddress & "/ToolBox/Export.aspx?syear=" & DelYear & "&smonth=" & DelMonth & "&sdate=" & DelDay
	objHttp.open "GET", strQuery, false
	objHttp.send
	Set objHttp = Nothing   
end if 
    
%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<script language="javascript">

function NewWindowOpen(deldate)
{
	window.open('ExportToCSV_File_Daily_Electronic_Invoices.asp?deldate='+ deldate,'_blank','left=300 top=200 height=215 width=475')
}

/*
function PdfGen(deldate)
{
	window.open('PDFGenerator/GeneratePDFDocs.aspx?deldate='+ deldate,'_blank','left=300 top=200 height=215 width=475');
	return false;
}

function PdfSend(deldate)
{
	window.open('PDFGenerator/SendPDFDocs.aspx?deldate='+ deldate,'_blank','left=300 top=200 height=215 width=475');
	return false;
}
*/

</script>
<title></title>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Process Daily Report for <%=RepDate%><br>
      &nbsp;</font></b>
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">

		<%if processorders = "OK" then
			LogAction "Daily Process end..", "Date:" & deldate , ""
		%>
		 <tr>
          <td colspan="2">Your orders are now processed for the delivery on <%=DelDate%></td>
        </tr>       
        <%else%>
		 <tr>
          <td colspan="2">You do not have any orders to process or you have already processed order for this date <%=RepDate%></td>
        </tr>
        <%end if%>
      </table>

    </td>
  </tr>
</table>
      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <form name="form6" target="_blank" action="daily_all_reports.asp" method="post">
          <td bgcolor="#E1E1E1" height="21">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="View all daily reports (excluding invoices & credits)" name="B1" style="font-family: Verdana; font-size: 8pt">          </td>
          </form>
          <td bgcolor="#E1E1E1" height="50" align="center" >
             <form name="form7" target="_blank" action="daily_all_reports_invoices_print.asp" method="post">
         
			<input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="All Invoices and Credit Notes" name="B2" style="font-family: Verdana; font-size: 8pt">          
			</form>
			
			<!--Daily PDF-->
			<form name="form7" target="_blank" action="daily_all_reports_invoices_PDF.asp" method="post">         
			<input type="hidden" name="delDatePDF" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=delDatePDF%>">			
            <input type="submit" value="All Invoices & Credit Notes in PDF"  size="20" style="font-family: Verdana; font-size: 8pt" >           
          	</form>
			<!-- Generate Daily PDF Files 
			
			<input type="button" value="Send Invoice & Credit PDF Files to Groups" onclick="return PdfSend(<%= "'" & pdfFileGenArg & "'" %>)" size="20" style="font-family: Verdana; font-size: 8pt" >
			
			-->
			</td>
          
		 
        </tr>
		<!--
		<tr>
		 <form name="form7" target="_blank" action="daily_all_reports_invoices.asp" method="post">
          <td bgcolor="#E1E1E1" height="21">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="View all Invoices and Credit Notes" name="B2" style="font-family: Verdana; font-size: 8pt">          </td>
          </form>
        </tr>
		-->
		<!--<tr>
           <form name="form7" target="_blank">
          <td bgcolor="#E1E1E1" height="50" colspan="2" align="center" valign="bottom">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="button" onClick="NewWindowOpen('<%=DelDate%>')" value="Generate Electronic Invoices" name="B3" style="font-family: Verdana; font-size: 8pt">          </td>
          </form>
        </tr>-->
		
		<tr>
			<td height="50" valign="bottom"><b>Other Reports</b></td>
		</tr>
		<table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
		<tr>
		<td valign="top">
		<table>
		<tr>
          <form name="form6" target="_blank" action="daily_all_reports_3rd_Party.asp" method="post">
          <td bgcolor="#ffffff" height="50">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="<%="           3rd Party Reports          "%>" name="B1" style="font-family: Verdana; font-size: 8pt">          </td>
          </form>
        </tr>
		
		<tr>
          <form name="form6" target="_blank" action="daily_all_reports_Internal_Production.asp" method="post">
          <td bgcolor="#ffffff" height="50">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="<%="    Internal Production Reports   "%>" name="B1" style="font-family: Verdana; font-size: 8pt">          </td>
          </form>
        </tr>
		
		<tr>
          <form name="form6" target="_blank" action="daily_all_reports_Packing_Sheet.asp" method="post">
          <td bgcolor="#ffffff" height="50">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="<%="       Packing Sheet Reports       "%>" name="B1" style="font-family: Verdana; font-size: 8pt">          </td>
          </form>
        </tr>
		
		<tr>
          <form name="form6" target="_blank" action="daily_all_reports_Driver_Packing_Sheet.asp" method="post">
          <td bgcolor="#ffffff" height="50">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="<%="  Driver Packing Sheet Reports  "%>" name="B13" style="font-family: Verdana; font-size: 8pt"></td>
          </form>
         
        </tr>
		
		<!--<tr>
          <form name="form6" target="_blank" action="daily_all_reports_Office.asp" method="post">
          <td bgcolor="#ffffff" height="50">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="<%="             Office Reports             "%>" name="B12" style="font-family: Verdana; font-size: 8pt"></td>
          </form>
        </tr>
        <tr>-->
         <!-- <form name="form6" target="_blank" action="daily_all_reports_Packing_Sheet1.asp" method="post">
          <td bgcolor="#ffffff" height="50">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="<%="             Packing Sheet 1           "%>" name="B12" style="font-family: Verdana; font-size: 8pt"></td>
          </form>
        </tr>-->
        <!-- <tr>
          <form name="form6" target="_blank" action="daily_all_reports_Packing_Sheet2.asp" method="post">
          <td bgcolor="#ffffff" height="50">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="<%="             Packing Sheet 2           "%>" name="B12" style="font-family: Verdana; font-size: 8pt"></td>
          </form>
        </tr>-->
		</table>
		</td>
		<td valign="top">
			<!--<table>
				<tr>
				  <form name="form7" target="_blank" action="doughmixingreport_one_click_reports.asp" method="post">
				  <td bgcolor="#ffffff" height="50">
					<input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
					<input type="hidden" name="dtype" size="20" style="font-family: Verdana; font-size: 8pt" value="1">
					<input type="hidden" name="facility" size="20" style="font-family: Verdana; font-size: 8pt" value="0">
					<input type="submit" value="<%="Direct Dough Mixing Report"%>" name="B1" style="font-family: Verdana; font-size: 8pt">          </td>
				  </form>
				</tr>
				<tr>
				  <form name="form7" target="_blank" action="doughmixingreport_one_click_reports.asp" method="post">
				  <td bgcolor="#ffffff" height="50">
					<input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
					<input type="hidden" name="dtype" size="20" style="font-family: Verdana; font-size: 8pt" value="2">
					<input type="hidden" name="facility" size="20" style="font-family: Verdana; font-size: 8pt" value="0">
					<input type="submit" value="<%="48 Hours Dough Mixing Report"%>" name="B1" style="font-family: Verdana; font-size: 8pt">          </td>
				  </form>
				</tr>
				<tr>
				  <form name="form7" target="_blank" action="doughmixingreport_one_click_reports.asp" method="post">
				  <td bgcolor="#ffffff" height="50">
					<input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
					<input type="hidden" name="dtype" size="20" style="font-family: Verdana; font-size: 8pt" value="1">
					<input type="hidden" name="facility" size="20" style="font-family: Verdana; font-size: 8pt" value="11">
					<input type="submit" value="<%="Stanmore English Dough Mixing Report"%>" name="B1" style="font-family: Verdana; font-size: 8pt">          </td>
				  </form>
				</tr>
			</table>
			-->
			<table>
				<tr>
				  <form name="form7" target="_blank" action="DoughMixingReportNew1.asp?pageFrom=Process" method="post">
				  <td bgcolor="#ffffff" height="50">
					<input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
					<input type="hidden" name="dtype" size="20" style="font-family: Verdana; font-size: 8pt" value="1">
					<input type="hidden" name="facility" size="20" style="font-family: Verdana; font-size: 8pt" value="0">
					<input type="hidden" name="DoughType" size="20" style="font-family: Verdana; font-size: 8pt" value="-1">
					<input type="hidden" name="day" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=sDay%>">
					<input type="hidden" name="month" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=sMonth%>">
					<input type="hidden" name="year" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=sYear%>">
					<input type="submit" value="<%="Direct Dough Mixing Report"%>" name="B1" style="font-family: Verdana; font-size: 8pt">          </td>
				  </form>
				</tr>
				<tr>
				  <form name="form7" target="_blank" action="DoughMixingReportNew1.asp?pageFrom=Process" method="post">
				  <td bgcolor="#ffffff" height="50">
					<input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
					<input type="hidden" name="dtype" size="20" style="font-family: Verdana; font-size: 8pt" value="2">
					<input type="hidden" name="facility" size="20" style="font-family: Verdana; font-size: 8pt" value="0">
					<input type="hidden" name="DoughType" size="20" style="font-family: Verdana; font-size: 8pt" value="-1">
					<input type="hidden" name="day" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=sDay%>">
					<input type="hidden" name="month" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=sMonth%>">
					<input type="hidden" name="year" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=sYear%>">
					<input type="submit" value="<%="48 Hours Dough Mixing Report"%>" name="B1" style="font-family: Verdana; font-size: 8pt">          </td>
				  </form>
				</tr>
				<!--<tr>
				  <form name="form7" target="_blank" action="DoughMixingReportNew1.asp?pageFrom=Process" method="post">
				  <td bgcolor="#ffffff" height="50">
					<input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
					<input type="hidden" name="dtype" size="20" style="font-family: Verdana; font-size: 8pt" value="1">
					<input type="hidden" name="facility" size="20" style="font-family: Verdana; font-size: 8pt" value="11">
					<input type="hidden" name="DoughType" size="20" style="font-family: Verdana; font-size: 8pt" value="-1">
					<input type="hidden" name="day" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=sDay%>">
					<input type="hidden" name="month" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=sMonth%>">
					<input type="hidden" name="year" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=sYear%>">
					<input type="submit" value="<%="Stanmore English Dough Mixing Report"%>" name="B1" style="font-family: Verdana; font-size: 8pt">          </td>
				  </form>
				</tr>-->
				<!--<tr>
				  <form name="form7" target="_blank" action="daily_report_packing_sheet_filter_types.asp" method="post">
				  <td style="font-family: Verdana; font-size: 8pt" >
				  
				   Type 3:<select size="1" name="Type1" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vType1Array,2)%>  				
                              <option value="<%=vType1Array(0,i)%>"><%=vType1Array(1,i)%></option>
                  	<%next%> 
                    </select>
				  
				 
				  Type4: 
		            <select size="1" name="Type2" style="font-family: Verdana; font-size: 8pt">
					        <option value="">Select</option>
					        <%for i = 0 to ubound(vType2Array,2)%>  				
                                      <option value="<%=vType2Array(0,i)%>"><%=vType2Array(1,i)%></option>
                  	        <%next%> 
                   </select>
				  
				 
				  <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
					<input type="hidden" name="deltype" size="20" style="font-family: Verdana; font-size: 8pt" value="All">
					<input type="submit" value="<%="PP Packing Sheet Report"%>" name="B1" style="font-family: Verdana; font-size: 8pt">

				  
				  
		    
					</td>
				  </form>
				</tr>
				<tr>
				  <form name="form7" target="_blank" action="daily_report_Burger_Bun.asp" method="post">
				  <td bgcolor="#ffffff" style="font-family: Verdana; font-size: 8pt">
				  Type3: 
		      <select size="1" name="Type3" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vType1Array,2)%>  				
                              <option value="<%=vType1Array(0,i)%>"><%=vType1Array(1,i)%></option>
                  	<%next%> 
           </select>
		    Type4: 
		    <select size="1" name="Type4" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vType2Array,2)%>  				
                              <option value="<%=vType2Array(0,i)%>"><%=vType2Array(1,i)%></option>
                  	<%next%> 
           </select>
					<input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
					<input type="hidden" name="deltype" size="20" style="font-family: Verdana; font-size: 8pt" value="All">
					
					<input type="submit" value="<%="Burger Bun Packing sheet Report"%>" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
				  </form>
				</tr>-->
				 <!--<tr>
          <form name="form6" target="_blank" action="daily_all_reports_Packing_Sheet1_Code18.asp" method="post">
          <td bgcolor="#ffffff" height="50">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="<%="             Packing Sheet 1 (code 18)           "%>" name="B12" style="font-family: Verdana; font-size: 8pt"></td>
          </form>
        </tr>-->
				 <!--<tr>
          <form name="form6" target="_blank" action="daily_all_reports_Packing_Sheet2_Code18.asp" method="post">
          <td bgcolor="#ffffff" height="50">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="<%="             Packing Sheet 2 (code 18)           "%>" name="B12" style="font-family: Verdana; font-size: 8pt"></td>
          </form>
        </tr>-->
        <!-- <tr>
          <form name="form6" target="_blank" action="daily_all_reports_Packing_Sheet1_Code22.asp" method="post">
          <td bgcolor="#ffffff" height="50">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="<%="             Packing Sheet 1 (code 22)           "%>" name="B12" style="font-family: Verdana; font-size: 8pt"></td>
          </form>
        </tr>-->
        
         <!--<tr>
          <form name="form6" target="_blank" action="daily_all_reports_Packing_Sheet2_Code22.asp" method="post">
          <td bgcolor="#ffffff" height="50">
            <input type="hidden" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=DelDate%>">
            <input type="submit" value="<%="             Packing Sheet 2 (code 22)           "%>" name="B12" style="font-family: Verdana; font-size: 8pt"></td>
          </form>
        </tr>-->
			</table>
		</td>
    </table>
  </center>
</div>
</body>
</html>
