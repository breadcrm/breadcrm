<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%

INo= request.form("ing")
SNo= request.form("sup")
FNo= replace(request.form("facility"),"'","''")
Units= replace(request.form("Unit"),"'","''")
Qty= replace(request.form("Qty"),"'","''")
Price= replace(request.form("Price"),"'","''")
IDate = replace(request.form("indate"),"'","''")
vUpdate = request.form("Update")
SINo = request.form("intNumber")

if (not isnumeric(Qty)) or (not isnumeric(Units)) or (not isnumeric(Price)) then response.redirect "inventory_in.asp"

set object = Server.CreateObject("bakery.Inventory")
object.SetEnvironment(strconnection)
if trim(vUpdate) = "Update" then
	StockInUpdate = object.UpdateStockIn(SINo,INo,SNo,FNo,Units,Qty,price,IDate ) 
else
	StockIn= object.StockIn(INo, SNo,FNo,Units,Qty,Price,IDate)
End if

set object = nothing

if vUpdate = "Update" then
	if StockInUpdate <> "Ok" then
		set object = nothing
		response.redirect "inventory_in.asp?status=errors"
	else
		set object = nothing
		response.redirect "inventory_in.asp?status=okay"
	end if
else
	if StockIn <> "Ok" then
		set object = nothing
		response.redirect "inventory_in.asp?status=error"
	else
		set object = nothing
		response.redirect "inventory_in.asp?status=ok"
	end if
end if
%>
<html>

<head>
<meta http-equiv="Content-Language" content="en-gb">
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Stock In</title>
</head>

<body>

<p><b><font size="3">Inventory in entry has been made</font></b></p>
<p>&nbsp;</p>

<form method="POST" action="inventory_in.asp">
 <p>
  <input type="submit" value="Add Again" name="B1"><input type="button" value="Close Window" name="B2" onClick="javascript:window.close();">
  </p>
</form>

</body>

</html>