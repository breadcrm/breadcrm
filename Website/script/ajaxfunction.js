var xmlhttp = false;

//Check if we are using IE.
try {
//If the javascript version is greater than 5.
xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
}
catch (e) {
//If not, then use the older active x object.
try {
//If we are using IE.
xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
}
catch (E) {
//Else we must be using a non-IE browser.
xmlhttp = false;
}
   }

//If we are using a non-IE browser, create a JavaScript instance of the object.
if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
xmlhttp = new XMLHttpRequest();
}

function autocompletenew (thevalue, e){

theObject = document.getElementById("autocmpl");

theObject.style.visibility = "visible";
theObject.style.width = "400px";

var posx = 0;
var posy = 0;

posx = (findPosX (document.getElementById("productname")) + 1);
posy = (findPosY (document.getElementById("productname")) + 23);

theObject.style.left = posx + "px";
theObject.style.top = posy + "px";
theObject.style.position="absolute";

var theextrachar = e.which;

if (theextrachar == undefined){
theextrachar = e.keyCode;
}

//The location we are loading the page into.
var objID = "autocmpl";

//Take into account the backspace.

if (thevalue.length == 0)
{
var serverPage = "Includes/autocomp.asp";
} 
else 
{
var serverPage = "Includes/autocomp.asp" + "?sstring=" + unescape(thevalue).replace("&","`");
}

var obj = document.getElementById(objID);
xmlhttp.open("GET", serverPage);

      xmlhttp.onreadystatechange = function() {
if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
obj.innerHTML = xmlhttp.responseText;
}
}
xmlhttp.send(null);
}
function findPosX(obj){
var curleft = 0;
if (obj.offsetParent){
while (obj.offsetParent){
curleft += obj.offsetLeft
obj = obj.offsetParent;
}
} else if (obj.x){
curleft += obj.x;
}
return curleft;
}

function findPosY(obj){
var curtop = 0;
if (obj.offsetParent){
while (obj.offsetParent){
curtop += obj.offsetTop
obj = obj.offsetParent;
}
} else if (obj.y){
curtop += obj.y;
}
return curtop;
}

function setvalue (thevalue){
	
thevalue=thevalue.replace('@@', '"');
acObject = document.getElementById("autocmpl");

acObject.style.visibility = "hidden";
acObject.style.height = "0px";
acObject.style.width = "0px";

document.getElementById("productname").value = thevalue;
}