    
	var NoOffFirstLineMenus=9;
	var LowBgColor="#cccccc";
	var HighBgColor="#999999";
	var FontLowColor="black";
	var FontHighColor="White";
	var BorderColor="#666666";
	var BorderWidthMain=1;
	var BorderWidthSub=1;
 	var BorderBtwnMain=1;
	var BorderBtwnSub=1;
	var FontFamily="arial";
	var FontSize=9;
	var FontBold=0;
	var FontItalic=0;
	var MenuTextCentered="left";
	var MenuCentered="left";
	var MenuVerticalCentered="top";
	var ChildOverlap=0;
	var ChildVerticalOverlap=0;
	var StartTop=0;
	var StartLeft=0;
	var VerCorrect=0;
	var HorCorrect=3;
	var LeftPaddng=5;
	var TopPaddng=2;
	var FirstLineHorizontal=1;
	var MenuFramesVertical=0;
	var DissapearDelay=1000;
	var UnfoldDelay=0100;
	var TakeOverBgColor=1;
	var FirstLineFrame="";
	var SecLineFrame="";	
	var DocTargetFrame="";	
	var TargetLoc="MenuPos";
	var MenuWrap=1;		
	var RightToLeft=0;	
	var BottomUp=0;		
	var UnfoldsOnClick=0;	
	var BaseHref="";	

	var Arrws=[BaseHref+"", 0, 0,BaseHref+"", 0, 0,BaseHref+"", 0, 0,BaseHref+"", 0, 0];
	var MenuUsesFrames=0;		
	var RememberStatus=0;			
	var PartOfWindow=.7;		
	var BuildOnDemand=0;	
	var MenuSlide="progid:DXImageTransform.Microsoft.GradientWipe(duration=0.6,wipeStyle=0) progid:DXImageTransform.Microsoft.Alpha(opacity=100)"
	var MenuShadow="";
	var MenuOpacity="";
	function BeforeStart(){return}
	function AfterBuild(){return}
	function BeforeFirstOpen(){return}
	function AfterCloseAll(){return}

	Menu1=new Array("Orders", "", "", 4, 20, 105, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu1_1=new Array("Amend Order", "order_new.asp","", 0,20,175, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu1_2=new Array("New/Sample Order", "order_sample.asp", "", 0,20,175, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu1_3=new Array("New Order after Processing", "ordersampleprocessed.asp", "", 0,20,175, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu1_4=new Array("Update Orders", "updateorders.asp", "", 0,20,175, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu2=new Array("Credits", "", "", 3, 20, 105, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_1=new Array("New Credit", "credit_new.asp","", 0,20,120, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_2=new Array("Find Credit", "credit_find.asp", "", 0,20,120, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_3=new Array("Invoice", "report_invoice.asp", "", 0,20,120, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu3=new Array("Products", "", "", 2, 20, 115, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu3_1=new Array("New Product", "product_new.asp", "", 0,20,120, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu3_2=new Array("Find Product", "product_find.asp", "", 0,20,120, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu4=new Array("Customers", "", "", 4, 20, 115, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_1=new Array("New Customer", "customer_new.asp","", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_2=new Array("Find Customer", "customer_find.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_3=new Array("Set Customer Profile", "customer_profile.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_4=new Array("Add Customer Group", "CustomerGroupAdd.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu5=new Array("Suppliers", "", "", 3, 20, 115, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu5_1=new Array("New Supplier", "supplier_new.asp","", 0,20,215, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu5_2=new Array("Find / Edit Supplier", "supplier_find.asp", "", 0,20,215, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu5_3=new Array("Add Ingredients supplied by supplier", "supplier_profile.asp", "", 0,20,215, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu6=new Array("Inventory", "", "", 7, 20, 115, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_1=new Array("Inventory Change", "inventory_change.asp","", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_2=new Array("Add Dough Type", "inventory_doughs.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_3=new Array("Add Ingredients", "inventory_ingredients.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_4=new Array("Ingredients", "inventory_ingredientslist.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_5=new Array("Dough Types", "inventory_ingredientsdoughtype.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_6=new Array("In", "inventory_inListing.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_7=new Array("Internal Movement", "inventory_internalmovement.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu7=new Array("Employees", "", "", 3, 20, 115, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu7_1=new Array("Add Employee", "employee_add.asp","", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu7_2=new Array("Find Employee", "employee_find.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu7_3=new Array("Change Password", "employee_changePassword.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		
	Menu8=new Array("Daily Reports", "", "", 2, 20, 115, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_1=new Array("Process Report", "LoginInternalPage.asp","", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_2=new Array("View Report", "daily_report.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		
	
	Menu9=new Array("Logout", "logout.asp", "", 0, 20, 85, "", "", "", "", "", "", -1, -1, -1, "", "");
	