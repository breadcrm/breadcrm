
HM_Array1 = [
[120,      // menu width
0,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["Amend Order","order_new.asp",1,0,1],
//["Find Order","order_find.asp",1,0,1],
["New/Sample Order","order_sample.asp",1,0,1],
["New Order after processsing","ordersampleprocessed.asp",1,0,1],
["Update Orders","updateorders.asp",1,0,1],
]


HM_Array2 = [
[120,      // menu width
71,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["New Credit","credit_new.asp",1,0,1],
["Find Credit","credit_find.asp",1,0,1],
["Invoice","report_invoice.asp",1,0,1],
]



HM_Array3 = [
[120,      // menu width
146,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["New Product","product_new.asp",1,0,1],
["Find Product","product_find.asp",1,0,1],
]


HM_Array4 = [
[140,      // menu width
228,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["Find Customer","customer_find.asp",1,0,1],
["Set Customer Profile","customer_profile.asp",1,0,1],
["Add Customer Group","CustomerGroupAdd.asp",1,0,1],
]

HM_Array5 = [
[125,      // menu width
313,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["New Supplier","supplier_new.asp",1,0,1],
["Find / Edit Supplier","supplier_find.asp",1,0,1],
["Add Ingredients supplied by supplier","supplier_profile.asp",1,0,1],
]


HM_Array6 = [
[120,      // menu width
402,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["Inventory Change","inventory_change.asp",1,0,1],
["Add Dough Type","inventory_doughs.asp",1,0,1],
["Add Ingredients","inventory_ingredients.asp",1,0,1],
["Ingredients","inventory_ingredientslist.asp",1,0,1],
["Dough Types","inventory_ingredientsdoughtype.asp",1,0,1],
["In","inventory_inListing.asp",1,0,1],
["Internal Movement","inventory_internalmovement.asp",1,0,1],
]


HM_Array7 = [
[120,      // menu width
489,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["Add Employee","employee_add.asp",1,0,1],
["Find Employee","employee_find.asp",1,0,1],
["Change Password","employee_changePassword.asp",1,0,1],
]



HM_Array8 = [
[120,      // menu width
576,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["Process Report","LoginInternalPage.asp",1,0,1],
["View Report","daily_report.asp",1,0,1],
]

HM_Array9 = [
[150,      // menu width
701,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["Logout","logout.asp",1,0,1],
]