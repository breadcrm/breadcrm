
HM_Array1 = [
[120,      // menu width
0,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["Amend Order","order_new.asp",1,0,1],
//["Find Order","order_find.asp",1,0,1],
["New/Sample Order","order_sample.asp",1,0,1],
["New Order after processsing","ordersampleprocessed.asp",1,0,1],
["Update Orders","updateorders.asp",1,0,1],
]


HM_Array2 = [
[120,      // menu width
71,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["New Credit","credit_new.asp",1,0,1],
["Find Credit","credit_find.asp",1,0,1],
["Invoice","report_invoice.asp",1,0,1],
]



HM_Array3 = [
[120,      // menu width
146,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["New Product","product_new.asp",1,0,1],
["Find Product","product_find.asp",1,0,1],
]


HM_Array4 = [
[140,      // menu width
228,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["New Customer","customer_new.asp",1,0,1],
["Find Customer","customer_find.asp",1,0,1],
["Set Customer Profile","customer_profile.asp",1,0,1],
["Add Customer Group","CustomerGroupAdd.asp",1,0,1],
]

HM_Array5 = [
[125,      // menu width
313,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["New Supplier","supplier_new.asp",1,0,1],
["Find / Edit Supplier","supplier_find.asp",1,0,1],
["Add Ingredients supplied by supplier","supplier_profile.asp",1,0,1],
]


HM_Array6 = [
[120,      // menu width
402,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["Inventory Change","inventory_change.asp",1,0,1],
["Add Dough Type","inventory_doughs.asp",1,0,1],
["Add Ingredients","inventory_ingredients.asp",1,0,1],
["Ingredients","inventory_ingredientslist.asp",1,0,1],
["Dough Types","inventory_ingredientsdoughtype.asp",1,0,1],
["In","inventory_inListing.asp",1,0,1],
["Internal Movement","inventory_internalmovement.asp",1,0,1],
]


HM_Array7 = [
[120,      // menu width
489,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["Add Employee","employee_add.asp",1,0,1],
["Find Employee","employee_find.asp",1,0,1],
["Change Password","employee_changePassword.asp",1,0,1],
]



HM_Array8 = [
[120,      // menu width
576,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["Process Report","LoginInternalPage.asp",1,0,1],
["View Report","daily_report.asp",1,0,1],
]


HM_Array9 = [
[150,      // menu width
681,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["Credits","mon_report_credit.asp",1,0,1],
["Customer","mon_report_customer.asp",1,0,1],
["Product","mon_report_product.asp",1,0,1],
["Sample","mon_report_sample.asp",1,0,1],
["Delivery Size","mon_report_turnover.asp",1,0,1],
["Facility","mon_report_facilities.asp",1,0,1],
["Sales Report","#",1,0,1],
]

HM_Array9_7 = [
[],
["Facility by Production Type","sale_facility_by_productiontype.asp",1,0,0],
["Customer by Production Type","sale_customer_by_productiontype.asp",1,0,0],
["Customers by Product","sale_customer_by_product.asp",1,0,0],
["Products by Customers","sale_product_by_customer.asp",1,0,0],
["Products by Devivery Time","sale_product_by_delivery_time.asp",1,0,0],
["Production Type","sale_production_type.asp",1,0,0],
["Delivery Time","sale_delivery_time.asp",1,0,0],
]



HM_Array10 = [
[150,      // menu width
765,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["Full Price List","report_fullpricelist.asp",1,0,1],
["Product Cost","report_productcost.asp",1,0,1],
["Inventory Management","#",1,0,1],
["Sales","report_sales.asp",1,0,1],
["Order Size","report_ordersize.asp",1,0,1],
["Delivery Count","report_delivery.asp",1,0,1],
["Special Price","report_specialprice.asp",1,0,1],
["Invoice","report_invoice.asp",1,0,1],
["Address & Phone list","report_address.asp",1,0,1],
["Employee","report_employee.asp",1,0,1],
["Phone List","report_phonelist.asp",1,0,1],
]

HM_Array11 = [
[150,      // menu width
980,       // left_position
29,       // top_position
"#ffffff",   // font_color
"#000000",   // mouseover_font_color
"#999999",   // background_color
"#D7D7D7",   // mouseover_background_color
"#000000",   // border_color
"#000000",    // separator_color
0,         // top_is_permanent
0,         // top_is_horizontal
0,         // tree_is_horizontal
1,         // position_under
1,         // top_more_images_visible
1,         // tree_more_images_visible
"null",    // evaluate_upon_tree_show
"null",    // evaluate_upon_tree_hide
,          // right_to_left
],     // display_on_click
["Logout","logout.asp",1,0,1],
]


//HM_Array10_1 = [
//[],
//["Full Price List",                    		"report_fullpricelist.asp",1,0,0],
//]

HM_Array10_3 = [
[],
["Inventory Analysis",                    		"report_inventoryanalysis.asp",1,0,0],
["Inventory History",                    			"report_inventoryhistory.asp",1,0,0],
]


