    
	var NoOffFirstLineMenus=8;
	var LowBgColor="#cccccc";
	var HighBgColor="#999999";
	var FontLowColor="black";
	var FontHighColor="White";
	var BorderColor="#666666";
	var BorderWidthMain=1;
	var BorderWidthSub=1;
 	var BorderBtwnMain=1;
	var BorderBtwnSub=1;
	var FontFamily="arial";
	var FontSize=9;
	var FontBold=0;
	var FontItalic=0;
	var MenuTextCentered="left";
	var MenuCentered="left";
	var MenuVerticalCentered="top";
	var ChildOverlap=0;
	var ChildVerticalOverlap=0;
	var StartTop=0;
	var StartLeft=0;
	var VerCorrect=0;
	var HorCorrect=0; //3
	var LeftPaddng=5;
	var TopPaddng=2;
	var FirstLineHorizontal=1;
	var MenuFramesVertical=0;
	var DissapearDelay=1000;
	var UnfoldDelay=0100;
	var TakeOverBgColor=1;
	var FirstLineFrame="";
	var SecLineFrame="";	
	var DocTargetFrame="";	
	var TargetLoc="MenuPos";
	var MenuWrap=1;		
	var RightToLeft=0;	
	var BottomUp=0;		
	var UnfoldsOnClick=0;	
	var BaseHref="";	

	var Arrws=[BaseHref+"", 0, 0,BaseHref+"", 0, 0,BaseHref+"", 0, 0,BaseHref+"", 0, 0];
	var MenuUsesFrames=0;		
	var RememberStatus=0;			
	var PartOfWindow=.7;		
	var BuildOnDemand=0;	
	var MenuSlide="progid:DXImageTransform.Microsoft.GradientWipe(duration=0.6,wipeStyle=0) progid:DXImageTransform.Microsoft.Alpha(opacity=100)"
	var MenuShadow="";
	var MenuOpacity="";
	function BeforeStart(){return}
	function AfterBuild(){return}
	function BeforeFirstOpen(){return}
	function AfterCloseAll(){return}

	Menu1=new Array("View Order", "order_new.asp?UType=N", "", 0, 20, 100, "", "", "", "", "", "", -1, -1, -1, "", "");
	
		
	Menu2=new Array("Credits", "", "", 2, 20, 100, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_1=new Array("Find Credit", "credit_find.asp", "", 0,20,120, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_2=new Array("Invoice", "report_invoice.asp", "", 0,20,120, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu3=new Array("Find Product", "product_find.asp?UType=N", "", 0, 20, 120, "", "", "", "", "", "", -1, -1, -1, "", "");
		
	Menu4=new Array("Find Customer", "customer_find.asp", "", 0, 20, 130, "", "", "", "", "", "", -1, -1, -1, "", "");
		
	Menu5=new Array("Find Supplier", "supplier_find.asp", "", 0, 20, 150, "", "", "", "", "", "", -1, -1, -1, "", "");
		
	Menu6=new Array("Daily Reports", "", "", 1, 20, 150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_1=new Array("View Report", "daily_report.asp", "", 0,20,150, "", "", "", "", "", "", -1, -1, -1, "", "");
		
	Menu7=new Array("Monthly Reports", "", "", 4, 20, 145, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu7_1=new Array("Credits", "mon_report_credit.asp","", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu7_2=new Array("Customer Brief", "mon_report_customer_brief.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu7_3=new Array("Product Brief", "mon_report_product_brief.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu7_4=new Array("Product Full", "mon_report_product.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		
	Menu8=new Array("Logout", "logout.asp", "", 0, 20, 100, "", "", "", "", "", "", -1, -1, -1, "", "");
	