    
	var NoOffFirstLineMenus=10;
	var LowBgColor="#cccccc";
	var HighBgColor="#999999";
	var FontLowColor="black";
	var FontHighColor="White";
	var BorderColor="#666666";
	var BorderWidthMain=1;
	var BorderWidthSub=1;
 	var BorderBtwnMain=1;
	var BorderBtwnSub=1;
	var FontFamily="arial";
	var FontSize=9;
	var FontBold=0;
	var FontItalic=0;
	var MenuTextCentered="left";
	var MenuCentered="left";
	var MenuVerticalCentered="top";
	var ChildOverlap=0;
	var ChildVerticalOverlap=0;
	var StartTop=0;
	var StartLeft=0;
	var VerCorrect=0;
	var HorCorrect=3;
	var LeftPaddng=5;
	var TopPaddng=2;
	var FirstLineHorizontal=1;
	var MenuFramesVertical=0;
	var DissapearDelay=1000;
	var UnfoldDelay=0100;
	var TakeOverBgColor=1;
	var FirstLineFrame="";
	var SecLineFrame="";	
	var DocTargetFrame="";	
	var TargetLoc="MenuPos";
	var MenuWrap=1;		
	var RightToLeft=0;	
	var BottomUp=0;		
	var UnfoldsOnClick=0;	
	var BaseHref="";	

	var Arrws=[BaseHref+"", 0, 0,BaseHref+"", 0, 0,BaseHref+"", 0, 0,BaseHref+"", 0, 0];
	var MenuUsesFrames=0;		
	var RememberStatus=0;			
	var PartOfWindow=.7;		
	var BuildOnDemand=0;	
	var MenuSlide="progid:DXImageTransform.Microsoft.GradientWipe(duration=0.6,wipeStyle=0) progid:DXImageTransform.Microsoft.Alpha(opacity=100)"
	var MenuShadow="";
	var MenuOpacity="";
	function BeforeStart(){return}
	function AfterBuild(){return}
	function BeforeFirstOpen(){return}
	function AfterCloseAll(){return}

	Menu1=new Array("Orders", "", "", 2, 20, 90, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu1_1=new Array("Amend Order", "order_new.asp","", 0,20,175, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu2=new Array("Credits", "", "", 6, 20, 90, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_1=new Array("New Credit", "credit_new.asp","", 0,20,150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_2=new Array("Find Credit", "credit_find.asp", "", 0,20,150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_3=new Array("Find Invoice", "report_invoice.asp", "", 0,20,150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_4=new Array("Complaint Responsibility", "ComplaintResponsibility.asp", "", 0,20,150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_5=new Array("New Complaint", "NewComplaint.asp", "", 0,20,150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_6=new Array("Customer Statement", "CustomerStatement.asp", "", 0,20,150, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu3=new Array("Products", "", "", 2, 20, 90, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu3_1=new Array("New Product", "product_new.asp", "", 0,20,120, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu3_2=new Array("Find Product", "product_find.asp", "", 0,20,120, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu4=new Array("Customers", "", "", 7, 20, 90, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_1=new Array("New Customer", "customer_new.asp","", 0,20,170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_2=new Array("Find Customer", "customer_find.asp", "", 0,20,170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_3=new Array("Set Customer Profile", "customer_profile.asp", "", 0,20,170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_4=new Array("Customer Group", "CustomerGroup.asp", "", 0,20,170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_5=new Array("Customer Sub Group", "SubGroup.asp", "", 0,20,170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_6 = new Array("Find Customer Order", "FindCustomerOrder.asp", "", 0, 20, 170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_7 = new Array("Find and Replace", "FindandReplace.asp", "", 0, 20, 170, "", "", "", "", "", "", -1, -1, -1, "", "");
			
	Menu5=new Array("Suppliers", "", "", 3, 20, 90, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu5_1=new Array("New Supplier", "supplier_new.asp","", 0,20,215, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu5_2=new Array("Find/ Edit Supplier", "supplier_find.asp", "", 0,20,215, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu5_3=new Array("Add Ingredients supplied by supplier", "supplier_profile.asp", "", 0,20,215, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu6=new Array("Inventory", "", "", 2, 20, 90, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_1=new Array("Add New Dough", "inventory_doughs.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_2=new Array("Add Ingredients", "inventory_ingredients.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu7=new Array("Daily Processes", "", "", 2, 20, 100, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu7_1=new Array("Update Orders", "updateorders.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
       	Menu7_2=new Array("Process Report", "LoginInternalPage.asp","", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
	 	
		
	Menu8=new Array("Reports", "", "", 22, 20, 90, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_1=new Array("View Report", "daily_report.asp","", 0,20,162, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_2=new Array("Credits", "mon_report_credit.asp","", 0,20,162, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_3=new Array("Customer Brief", "mon_report_customer_brief.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_4=new Array("Customer Full", "mon_report_customer.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_5=new Array("Product Brief", "mon_report_product_brief.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		
		Menu8_6=new Array("Product Full", "mon_report_product.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_7=new Array("Sample", "mon_report_sample.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_8=new Array("Delivery Size", "mon_report_turnover.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_9=new Array("Facility", "mon_report_facilities.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_10=new Array("Sales Report", "", "", 7,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu8_10_1=new Array("Facility by Production Type", "sale_facility_by_productiontype.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu8_10_2=new Array("Customer by Production Type", "sale_customer_by_productiontype.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu8_10_3=new Array("Customer by Products", "sale_customer_by_product.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu8_10_4=new Array("Product by Customers", "sale_product_by_customer.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu8_10_5=new Array("Products by Delivery Time", "sale_product_by_delivery_time.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu8_10_6=new Array("Production Type", "sale_production_type.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu8_10_7=new Array("Delivery Time", "sale_delivery_time.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_11=new Array("Time Series Product Report", "TimeSeries_Product_List.asp", "", 0,20,162, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_12=new Array("Indirect Customer Brief", "mon_report_Indirect_customer_brief.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_13=new Array("Indirect Customer Full", "mon_report_Indirect_customer_full.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_14=new Array("Percentage Report", "PercentageReport.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		
		// Added Selva on 04 Aug 2009
		Menu8_15=new Array("Email Summary Report", "rep_email_sum.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_16=new Array("Log Activity Trace Report", "rep_log_trace.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_17=new Array("Amended Orders Report", "rep_ord_amd.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_18=new Array("7 Day Check Report", "rep_sales_history.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_19=new Array("Late Credit Note Report", "mon_report_late_crnotes.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_20=new Array("Late Invoice Report", "mon_report_late_invs.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_21=new Array("Early Credit Note Report", "mon_report_curr_month_crnotes.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_22=new Array("Sales/Credit Report", "report_sale_credit.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu9=new Array("MGM Reports", "", "", 5, 20, 90, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_1=new Array("Full Price List", "report_fullpricelist.asp","", 0,20,110, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_2=new Array("Special Price", "report_specialprice.asp", "", 0,20,110, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_3=new Array("Order Size", "report_ordersize.asp", "", 0,20,110, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_4=new Array("Delivery Count", "report_delivery.asp", "", 0,20,110, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_5=new Array("Transport Report", "report_transport.asp", "", 0, 20, 110, "", "", "", "", "", "", -1, -1, -1, "", "");
		
	Menu10=new Array("Logout", "logout.asp", "", 0, 20, 90, "", "", "", "", "", "", -1, -1, -1, "", "");
	