    
	var NoOffFirstLineMenus=5;
	var LowBgColor="#cccccc";
	var HighBgColor="#999999";
	var FontLowColor="black";
	var FontHighColor="White";
	var BorderColor="#666666";
	var BorderWidthMain=1;
	var BorderWidthSub=1;
 	var BorderBtwnMain=1;
	var BorderBtwnSub=1;
	var FontFamily="arial";
	var FontSize=9;
	var FontBold=0;
	var FontItalic=0;
	var MenuTextCentered="left";
	var MenuCentered="left";
	var MenuVerticalCentered="top";
	var ChildOverlap=0;
	var ChildVerticalOverlap=0;
	var StartTop=0;
	var StartLeft=0;
	var VerCorrect=0;
	var HorCorrect=0; //3
	var LeftPaddng=5;
	var TopPaddng=2;
	var FirstLineHorizontal=1;
	var MenuFramesVertical=0;
	var DissapearDelay=1000;
	var UnfoldDelay=0100;
	var TakeOverBgColor=1;
	var FirstLineFrame="";
	var SecLineFrame="";	
	var DocTargetFrame="";	
	var TargetLoc="MenuPos";
	var MenuWrap=1;		
	var RightToLeft=0;	
	var BottomUp=0;		
	var UnfoldsOnClick=0;	
	var BaseHref="";	

	var Arrws=[BaseHref+"", 0, 0,BaseHref+"", 0, 0,BaseHref+"", 0, 0,BaseHref+"", 0, 0];
	var MenuUsesFrames=0;		
	var RememberStatus=0;			
	var PartOfWindow=.7;		
	var BuildOnDemand=0;	
	var MenuSlide="progid:DXImageTransform.Microsoft.GradientWipe(duration=0.6,wipeStyle=0) progid:DXImageTransform.Microsoft.Alpha(opacity=100)"
	var MenuShadow="";
	var MenuOpacity="";
	function BeforeStart(){return}
	function AfterBuild(){return}
	function BeforeFirstOpen(){return}
	function AfterCloseAll(){return}

	Menu1=new Array("Credits", "", "", 2, 20, 150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu1_1=new Array("Find Credit", "credit_find.asp", "", 0,20,125, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu1_2=new Array("Find Invoice", "report_invoice.asp", "", 0,20,125, "", "", "", "", "", "", -1, -1, -1, "", "");

	Menu2=new Array("Products", "", "", 1, 20, 150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_1=new Array("Find Product", "product_find.asp?UType=N", "", 0, 20, 150, "", "", "", "", "", "", -1, -1, -1, "", "");
		
	Menu3=new Array("Customers", "", "", 2, 20, 150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu3_1=new Array("Find Customer", "customer_find.asp", "", 0, 20, 150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu3_2 = new Array("Find Customer Order", "FindCustomerOrder.asp", "", 0, 20, 170, "", "", "", "", "", "", -1, -1, -1, "", "");
		
	Menu4=new Array("Reports", "", "", 3, 20, 150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_1=new Array("View Report", "daily_report.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_2=new Array("Sales Report", "", "", 7,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu4_2_1=new Array("Facility by Production Type", "sale_facility_by_productiontype.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu4_2_2=new Array("Customer by Production Type", "sale_customer_by_productiontype.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu4_2_3=new Array("Customer by Products", "sale_customer_by_product.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu4_2_4=new Array("Product by Customers", "sale_product_by_customer.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu4_2_5=new Array("Products by Delivery Time", "sale_product_by_delivery_time.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu4_2_6=new Array("Production Type", "sale_production_type.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu4_2_7=new Array("Delivery Time", "sale_delivery_time.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_3=new Array("Percentage Report", "PercentageReport.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		
	Menu5=new Array("Logout", "logout.asp", "", 0, 20, 150, "", "", "", "", "", "", -1, -1, -1, "", "");
	