    
	var NoOffFirstLineMenus=11;
	var LowBgColor="#cccccc";
	var HighBgColor="#999999";
	var FontLowColor="black";
	var FontHighColor="White";
	var BorderColor="#666666";
	var BorderWidthMain=1;
	var BorderWidthSub=1;
 	var BorderBtwnMain=1;
	var BorderBtwnSub=1;
	var FontFamily="arial";
	var FontSize=9;
	var FontBold=0;
	var FontItalic=0;
	var MenuTextCentered="left";
	var MenuCentered="left";
	var MenuVerticalCentered="top";
	var ChildOverlap=0;
	var ChildVerticalOverlap=0;
	var StartTop=0;
	var StartLeft=0;
	var VerCorrect=0;
	var HorCorrect=3;
	var LeftPaddng=5;
	var TopPaddng=2;
	var FirstLineHorizontal=1;
	var MenuFramesVertical=0;
	var DissapearDelay=1000;
	var UnfoldDelay=0100;
	var TakeOverBgColor=1;
	var FirstLineFrame="";
	var SecLineFrame="";	
	var DocTargetFrame="";	
	var TargetLoc="MenuPos";
	var MenuWrap=1;		
	var RightToLeft=0;	
	var BottomUp=0;		
	var UnfoldsOnClick=0;	
	var BaseHref="";	

	var Arrws=[BaseHref+"", 0, 0,BaseHref+"", 0, 0,BaseHref+"", 0, 0,BaseHref+"", 0, 0];
	var MenuUsesFrames=0;		
	var RememberStatus=0;			
	var PartOfWindow=.7;		
	var BuildOnDemand=0;	
	var MenuSlide="progid:DXImageTransform.Microsoft.GradientWipe(duration=0.6,wipeStyle=0) progid:DXImageTransform.Microsoft.Alpha(opacity=100)"
	var MenuShadow="";
	var MenuOpacity="";
	function BeforeStart(){return}
	function AfterBuild(){return}
	function BeforeFirstOpen(){return}
	function AfterCloseAll(){return}

	Menu1=new Array("Orders", "", "", 3, 20, 70, "", "", "", "", "", "", -1, -1, -1, "", "");
	Menu1_1 = new Array("Amend Order", "order_new.asp", "", 0, 20, 175, "", "", "", "", "", "", -1, -1, -1, "", "");
	Menu1_2 = new Array("New/Sample Order", "order_sample.asp", "", 0, 20, 175, "", "", "", "", "", "", -1, -1, -1, "", "");
	Menu1_3 = new Array("New Order after Processing", "ordersampleprocessed.asp", "", 0, 20, 175, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu2=new Array("Credits", "", "", 6, 20, 75, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_1=new Array("New Credit", "credit_new.asp","", 0,20,150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_2=new Array("Find Credit", "credit_find.asp", "", 0,20,150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_3=new Array("Find Invoice", "report_invoice.asp", "", 0,20,150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_4=new Array("Complaint Responsibility", "ComplaintResponsibility.asp", "", 0,20,150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_5=new Array("New Complaint", "NewComplaint.asp", "", 0,20,150, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu2_6=new Array("Customer Statement", "CustomerStatement.asp", "", 0,20,150, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu3=new Array("Products", "", "", 2, 20, 90, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu3_1=new Array("New Product", "product_new.asp", "", 0,20,120, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu3_2=new Array("Find Product", "product_find.asp", "", 0,20,120, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu4=new Array("Customers", "", "", 9, 20, 95, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_1=new Array("New Customer", "customer_new.asp","", 0,20,170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_2=new Array("Find Customer", "customer_find.asp", "", 0,20,170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_3=new Array("Set Customer Profile", "customer_profile.asp", "", 0,20,170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_4=new Array("Customer Group", "CustomerGroup.asp", "", 0,20,170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_5=new Array("Customer Sub Group", "SubGroup.asp", "", 0,20,170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_6 = new Array("Management Note", "ManagementNote.asp", "", 0, 20, 170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_7 = new Array("Invoice Footer", "AddInvoiceFooter.asp", "", 0, 20, 170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_8 = new Array("Find Customer Order", "FindCustomerOrder.asp", "", 0, 20, 170, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu4_9 = new Array("Find and Replace", "FindandReplace.asp", "", 0, 20, 170, "", "", "", "", "", "", -1, -1, -1, "", "");
		
	Menu5=new Array("Suppliers", "", "", 3, 20, 95, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu5_1=new Array("New Supplier", "supplier_new.asp","", 0,20,215, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu5_2=new Array("Find/ Edit Supplier", "supplier_find.asp", "", 0,20,215, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu5_3=new Array("Add Ingredients supplied by supplier", "supplier_profile.asp", "", 0,20,215, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu6=new Array("Inventory", "", "", 5, 20, 95, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_1=new Array("Add New Dough", "inventory_doughs_new.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_2=new Array("List of Doughs", "inventory_ingredientsdoughtype_new.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_3=new Array("Add New Ingredient", "inventory_ingredients.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_4=new Array("List of Ingredients", "inventory_ingredientslist.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu6_5=new Array("Add Mother Ingredient", "AddMotherIngredient.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
	
	Menu7=new Array("Employees", "", "", 3, 20, 95, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu7_1=new Array("Add Employee", "employee_add.asp","", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu7_2=new Array("Find Employee", "employee_find.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu7_3=new Array("Change Password", "employee_changePassword.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		
	Menu8=new Array("Daily Processes", "", "", 1, 20, 100, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu8_1=new Array("Process Report", "LoginInternalPage.asp","", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		
		
	Menu9=new Array("Reports", "", "", 23, 20, 100, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_1=new Array("View Report", "daily_report.asp","", 0,20,162, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_2=new Array("Credits", "mon_report_credit.asp","", 0,20,162, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_3=new Array("Customer Brief", "mon_report_customer_brief.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_4=new Array("Customer Full", "mon_report_customer.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_5=new Array("Product Brief", "mon_report_product_brief.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		
		Menu9_6=new Array("Product Full", "mon_report_product.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_7=new Array("Sample", "mon_report_sample.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_8=new Array("Delivery Size", "mon_report_turnover.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_9=new Array("Facility", "mon_report_facilities.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_10=new Array("Sales Report", "", "", 7,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu9_10_1=new Array("Facility by Production Type", "sale_facility_by_productiontype.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu9_10_2=new Array("Customer by Production Type", "sale_customer_by_productiontype.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu9_10_3=new Array("Customer by Products", "sale_customer_by_product.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu9_10_4=new Array("Product by Customers", "sale_product_by_customer.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu9_10_5=new Array("Products by Delivery Time", "sale_product_by_delivery_time.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu9_10_6=new Array("Production Type", "sale_production_type.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu9_10_7=new Array("Delivery Time", "sale_delivery_time.asp", "", 0,20,145, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_11=new Array("Time Series Product Report", "TimeSeries_Product_List.asp", "", 0,20,162, "", "", "", "", "", "", -1, -1, -1, "", "");
		
		
		Menu9_12=new Array("Indirect Customer Brief", "mon_report_Indirect_customer_brief.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_13=new Array("Indirect Customer Full", "mon_report_Indirect_customer_full.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_14=new Array("Percentage Report", "PercentageReport.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_15=new Array("Email Summary Report", "rep_email_sum.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_16=new Array("Log Activity Trace Report", "rep_log_trace.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_17=new Array("Amended Orders Report", "rep_ord_amd.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_18=new Array("7 Day Check Report", "rep_sales_history.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_19=new Array("Late Credit Note Report", "mon_report_late_crnotes.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_20=new Array("Late Invoice Report", "mon_report_late_invs.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_21=new Array("Early Credit Note Report", "mon_report_curr_month_crnotes.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_22=new Array("Sales/Credit Report", "report_sale_credit.asp", "", 0,20,135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu9_23 = new Array("Customers/Orders Report", "Customer_Detail_report.asp", "", 0, 20, 135, "", "", "", "", "", "", -1, -1, -1, "", "");
		
		Menu10=new Array("MGM Reports", "", "", 19, 20, 100, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_1=new Array("Full Price List", "report_fullpricelist.asp","", 0,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_2=new Array("Product Cost", "report_productcost.asp", "", 0,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_3=new Array("Inventory Management", "", "", 2,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu10_3_1=new Array("Inventory Analysis", "report_inventoryanalysis.asp", "", 0,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");
			Menu10_3_2=new Array("Inventory History", "report_inventoryhistory.asp", "", 0,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");		
		Menu10_4=new Array("Sales", "report_sales.asp", "", 0,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_5=new Array("Order Size", "report_ordersize.asp", "", 0,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_6=new Array("Delivery Count", "report_delivery.asp", "", 0,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_7=new Array("Special Price", "report_specialprice.asp", "", 0,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_8=new Array("Invoice", "report_invoice.asp", "", 0,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_9=new Array("Address & Phone List", "report_address.asp", "", 0,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_10=new Array("Employee", "report_employee.asp", "", 0,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_11=new Array("Phone List", "report_phonelist.asp", "", 0,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");
		
		Menu10_12=new Array("Dough Reports", "", "", 6,20,280, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_12_1=new Array("Dough Mixing Report", "", "", 3,20,280, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_12_1_1=new Array("Direct Dough Mixing Report", "DoughMixingReport_serch.asp?dtype=1&facility=0", "", 0,20,240, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_12_1_2=new Array("48 Hours Dough Mixing Report", "DoughMixingReport_serch.asp?dtype=2&facility=0", "", 0,20,240, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_12_1_3=new Array("Stanmore English Dough Mixing Report", "DoughMixingReport_serch.asp?dtype=1&facility=11", "", 0,20,240, "", "", "", "", "", "", -1, -1, -1, "", "");
		
		Menu10_12_2=new Array("Dividing Report", "", "", 5,20,300, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_12_2_1=new Array("Direct dough report for dividing", "DividingReportConsolidation.asp?dtype=1&facility=0", "", 0,20,280, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_12_2_2=new Array("48 hours report for dividing", "DividingReportConsolidation.asp?dtype=2&facility=0", "", 0,20,280, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_12_2_3=new Array("Stanmore English report for dividing", "DividingReportConsolidation.asp?dtype=1&facility=11", "", 0,20,280, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_12_2_4=new Array("Cake Department Report for Dividing - Direct", "DividingReportConsolidationCakeDept.asp?dtype=N&facility=22", "", 0,20,280, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_12_2_5=new Array("Cake Department Report for Dividing - 48 Hours", "DividingReportConsolidationCakeDept.asp?dtype=Y&facility=22", "", 0,20,280, "", "", "", "", "", "", -1, -1, -1, "", "");		
		Menu10_12_3=new Array("Mother Calculation Report", "DoughMotherCalculationReport.asp", "", 0,20,280, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_12_4=new Array("Shaping Report", "ShapingRollsReportNew.asp", "", 0,20,210, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_12_5=new Array("Shaping Report for Cake Department - Direct", "ShapingRollsReportNewCakeDept.asp?dtype=N&facility=22", "", 0,20,280, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_12_6=new Array("Shaping Report for Cake Department - 48 Hours", "ShapingRollsReportNewCakeDept.asp?dtype=Y&facility=22", "", 0,20,280, "", "", "", "", "", "", -1, -1, -1, "", "");
		
		Menu10_13=new Array("Account Statement History Report", "AccountStatementHistoryReport.asp", "", 0,20,190, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_14 = new Array("Transport Report", "report_transport.asp", "", 0, 20, 135, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_15 = new Array("MultipleProducts Report", "report_multipleproduct_relationship.asp", "", 0, 20, 300, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_16 = new Array("Electronic Invoicing Email List", "report_EmailList.asp", "", 0, 20, 300, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_17 = new Array("Customer Document List", "report_CustomerDocList.asp", "", 0, 20, 300, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_18 = new Array("Product Document List", "report_ProductDocList.asp", "", 0, 20, 300, "", "", "", "", "", "", -1, -1, -1, "", "");
		Menu10_19 = new Array("Product Created Date Report", "report_ProductCreatedDate.asp", "", 0, 20, 300, "", "", "", "", "", "", -1, -1, -1, "", "");


	Menu11=new Array("Logout", "logout.asp", "", 0, 20, 73, "", "", "", "", "", "", -1, -1, -1, "", "");
	