<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
deldate= request.form("deldate")
if deldate= "" then response.redirect "LoginInternalPage.asp"
Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection) 
set detail = object.DisplayFacility()	
vFacarray =  detail("Facility") 
set detail = object.DisplayVec()	
vVecarray =  detail("Vec") 

set detail = Nothing
set object = Nothing

dim objBakery
dim recarray1, recarray2
dim ordno
dim retcol
Dim deldate
Dim intI
Dim intN
Dim intF
Dim intJ


dim tname, tno, curtno
Dim obj
Dim arCreditStatements
Dim arCreditStatementDetails
Dim vCreditDate

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; height:1px }
-->
</style>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
'Packing sheet report
set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
%>
<%
' 3rd Parties => 31-Sally Clarks, 32-Simply Bread, 30-Rinkoff   

			facility = "31,32,30"
			dtype="All"
			intN=0
			set col1= object.DailyPackingSheetReport(deldate,facility,dtype)
			vRecArray = col1("DailyPackingSheet")
			vtotcustarray = col1("TotalCustomers")
			if isarray(vtotcustarray) then
				totpages = ubound(vtotcustarray,2)+1
			else
				totpages = 0
			end if
			curpage=0
	    
			Facilityrec = col1("Facility")
			set col1= nothing
					mFontSize = 8
					mFooterSize =3
					mFooterFontSize = 3
					mExtraLine = 10
					mLine = 8
				if isarray(vrecarray) then
					mPage = 1
					currec=0
					totqty=0
					curcno = vrecarray(0,0)
					curvan = vrecarray(5,0)
					curpage=1	  
					for i = 0 to ubound(vRecArray,2)
						currec = clng(currec) + 1
						if clng(currec) = 1 then
							if mFontSize = 14 Then
								mLine = mline + 4
							else
								mLine = mline + 3
							end if	
							if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
								mPage = mPage + 1						
								mLine = 0
							End if
						end if
						if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then
							if mFontSize = 14 Then
								mLine = mLine + 2
							else
								mLine = mLine + 1
							end if	
						end if
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0
						End if          
						if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
							curvan = vrecarray(5,i)
							curcno = vrecarray(0,i)
							i=clng(i)-1
							if mFontSize = 14 Then
								mLine = mLine +4           
							else
								mLine = mLine +3           
							end if	
						
							if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then
								mPage = mPage + 1
								mLine = 0	
							else
								if curvan <> vrecarray(5,i)   Then										
									mPage = mPage + 1
									mLine = 0
								End if
							End if
							if i < ubound(vrecarray,2) then
							end if            
							curpage = curpage + 1
							currec = 0
							totqty=0
						end if
					next
	    
				else
					mPage = 1
				end if
				i = 0
if isarray(vrecarray) then
%>

<div align="center">
  <center>
<table border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
    <center><b><font size="3">Sally Clarks, Simply Bread and Rinkoff PACKING SHEET - <%=dtype%><br></font></b></center>
      <center><b><font size="3">(divided to Van and Customers)<br></font></b></center>
      <table border="0" width="100%" style="font-family: Verdana; font-size: <%=mFontSize%>pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><br>
            <br>
            <b>Date of report:</b> <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            <b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><BR>
           
            </td>
        </tr>
        <tr>
          <td>Number of pages for this report: <%=mPage%></td>
          <td align="right">Page 1 of <%=mPage%><br></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
</center>
</div><%
if mFontSize = 14 Then
	mLine = 16
Else
	mLine = 8
End if%>

<div align="center">
<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt" align="center"><%
	  if isarray(vrecarray) then
		mTotalPage = mPage
		mPage = 1		
	    currec=0
	    totqty=0
	    curcno = vrecarray(0,0)
	    curvan = vrecarray(5,0)
	    curpage=1	  
        for i = 0 to ubound(vRecArray,2)
          currec = clng(currec) + 1
          if clng(currec) = 1 then%>
            <tr>
            <td colspan="4">
              <b>Van : <%=vrecarray(5,i)%></b><br>
              <b>Customer : <%=vrecarray(0,i)%> - <%=vrecarray(1,i)%> (<%=vrecarray(7,i)%>)</b>
            </td>            
            </tr>
            <tr>            
            <td align="left" bgcolor="#CCCCCC" height="20" width="20%"><b>Facility</b></td> 
			<td align="left" bgcolor="#CCCCCC" height="20" width="65%"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Qty</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="5%"><b>SID</b></td>
            </tr><%
            if mFontSize = 14 Then
							mLine = mline + 4
						else
							mLine = mline + 3
						end if	
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td align="left" colspan="2"><b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%></td>
								<td colspan="2" align="right"><%
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
						End if
           end if
           if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then%>
          <tr>            
            <td align="left" width="20%"><%=vrecarray(8,i)%>&nbsp;</td>
			<td align="left" width="65%"><%=left(vrecarray(3,i),50)%>&nbsp;</td>
            <td align="center" width="10%"><%=vrecarray(4,i)%>&nbsp;</td>
			<%
            totqty = clng(totqty) + vrecarray(4,i)
			%>
            <td align="center" width="5%">&nbsp;</td>
          </tr><%
						if mFontSize = 14 Then
							mLine = mLine + 2
						else
							mLine = mLine + 1
						end if	
          end if
          if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
						mPage = mPage + 1						
						mLine = 0%>		
						</table>						
						<br class="page" />						
						<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td align="left" colspan="2"><b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%></td>
								<td colspan="2" align="right"><%	
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
          End if
          
		  if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
		    curvan = vrecarray(5,i)
		    curcno = vrecarray(0,i)
		    i=clng(i)-1%>
		    </table>
						<br>
            <br><%
            if mFontSize = 14 Then
							mLine = mLine +4           
						else
							mLine = mLine +3           
						end if	
         if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then 						
         
						mPage = mPage + 1
						mLine = 0%>								
						<br class="page" /><%						
		  else
			if curvan <> vrecarray(5,i)   Then										
				mPage = mPage + 1
				mLine = 0%>													
				<br class="page" /><%
			End if				
          End if
          
            if i < ubound(vrecarray,2) then%>
							<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
							if mLine = 0 Then %>
								<tr>
									<td align="left" colspan="2"><b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%></td>
								<td colspan="2" align="right"><%
									 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
								</tr><%						
							end if         
            end if   
            
		    curpage = curpage + 1
		    currec = 0
		    totqty=0
		  end if
	    next%>
	    <%else%>
        <tr>
        <td width="100%" colspan = "4" height="20">Sorry no items found</td>
        </tr><%
	  end if%>
      </table>
</div>
<%end if%>

<%
set col1= nothing
%>

<%
'Packing sheet report - 18-BMG 
		facility = "18"
		tno=3
		for curtno=1 to tno
			if curtno=1 then
				dtype="Morning"
			elseif curtno=2 then
				dtype="Noon"
			else
				dtype="Evening"
			end if
			intN=0
			set col1= object.DailyPackingSheetReport(deldate,facility,dtype)
			vRecArray = col1("DailyPackingSheet")
			vtotcustarray = col1("TotalCustomers")
			if isarray(vtotcustarray) then
				totpages = ubound(vtotcustarray,2)+1
			else
				totpages = 0
			end if
			curpage=0
	    	Facilityrec = col1("Facility")
			set col1= nothing

					mFontSize = 8
					mFooterSize =3
					mFooterFontSize = 3
					mExtraLine = 10
					mLine = 8
				if isarray(vrecarray) then
					mPage = 1
					currec=0
					totqty=0
					curcno = vrecarray(0,0)
					curvan = vrecarray(5,0)
					curpage=1	  
					for i = 0 to ubound(vRecArray,2)
						currec = clng(currec) + 1
						if clng(currec) = 1 then
							if mFontSize = 14 Then
								mLine = mline + 4
							else
								mLine = mline + 3
							end if	
							if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
								mPage = mPage + 1						
								mLine = 0
							End if
						end if
						if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then
							if mFontSize = 14 Then
								mLine = mLine + 2
							else
								mLine = mLine + 1
							end if	
						end if
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0
						End if          
						if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
							curvan = vrecarray(5,i)
							curcno = vrecarray(0,i)
							i=clng(i)-1
							if mFontSize = 14 Then
								mLine = mLine +4           
							else
								mLine = mLine +3           
							end if	
						
							if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then
								mPage = mPage + 1
								mLine = 0	
							else
								if curvan <> vrecarray(5,i)   Then										
									mPage = mPage + 1
									mLine = 0
								End if
							End if
							if i < ubound(vrecarray,2) then
							end if            
							curpage = curpage + 1
							currec = 0
							totqty=0
						end if
					next
	    
				else
					mPage = 1
				end if
				i = 0
if isarray(vrecarray) then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<div align="center">
  <center>
<table border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
    <center><b><font size="3">BMG PACKING SHEET - <%=dtype%><br></font></b></center>
      <center><b><font size="3">(divided to Van and Customers)<br></font></b></center>
      <table border="0" width="100%" style="font-family: Verdana; font-size: <%=mFontSize%>pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><br>
            <br>
            <b>Date of report:</b> <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            <b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><BR>
           
            </td>
        </tr>
        <tr>
          <td>Number of pages for this report: <%=mPage%></td>
          <td align="right">Page 1 of <%=mPage%><br></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
</center>
</div><%
if mFontSize = 14 Then
	mLine = 16
Else
	mLine = 8
End if%>

<div align="center">
<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt" align="center"><%
	  if isarray(vrecarray) then
		mTotalPage = mPage
		mPage = 1		
	    currec=0
	    totqty=0
	    curcno = vrecarray(0,0)
	    curvan = vrecarray(5,0)
	    curpage=1	  
        for i = 0 to ubound(vRecArray,2)
          currec = clng(currec) + 1
          if clng(currec) = 1 then%>
            <tr>
            <td colspan="4">
              <b>Van : <%=vrecarray(5,i)%></b><br>
              <b>Customer : <%=vrecarray(0,i)%> - <%=vrecarray(1,i)%> (<%=vrecarray(7,i)%>)</b>
            </td>            
            </tr>
            <tr>            
            <td align="left" bgcolor="#CCCCCC" height="20" width="20%"><b>Facility</b></td> 
			<td align="left" bgcolor="#CCCCCC" height="20" width="65%"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Qty</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="5%"><b>SID</b></td>
            </tr><%
            if mFontSize = 14 Then
							mLine = mline + 4
						else
							mLine = mline + 3
						end if	
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td colspan="4" align="right"><%
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
						End if
           end if
           if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then%>
          <tr>            
            <td align="left" width="20%"><%=vrecarray(8,i)%>&nbsp;</td>
			<td align="left" width="65%"><%=left(vrecarray(3,i),50)%>&nbsp;</td>
            <td align="center" width="10%"><%=vrecarray(4,i)%>&nbsp;</td>
			<%
            totqty = clng(totqty) + vrecarray(4,i)
			%>
            <td align="center" width="5%">&nbsp;</td>
          </tr><%
						if mFontSize = 14 Then
							mLine = mLine + 2
						else
							mLine = mLine + 1
						end if	
          end if
          if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
						mPage = mPage + 1						
						mLine = 0%>		
						</table>						
						<br class="page" />						
						<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td colspan="3" align="right"><%	
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
          End if
          
		  if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
		    curvan = vrecarray(5,i)
		    curcno = vrecarray(0,i)
		    i=clng(i)-1%>
		    </table>
						<br>
            <br><%
            if mFontSize = 14 Then
							mLine = mLine +4           
						else
							mLine = mLine +3           
						end if	
         if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then 						
         
						mPage = mPage + 1
						mLine = 0%>								
						<br class="page" /><%						
		  else
			if curvan <> vrecarray(5,i)   Then										
				mPage = mPage + 1
				mLine = 0%>													
				<br class="page" /><%
			End if				
          End if
          
            if i < ubound(vrecarray,2) then%>
							<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
							if mLine = 0 Then %>
								<tr>
									<td colspan="3" align="right"><%
									 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
								</tr><%						
							end if         
            end if   
            
		    curpage = curpage + 1
		    currec = 0
		    totqty=0
		  end if
	    next%>
	    <%else%>
        <tr>
        <td width="100%" colspan = "4" height="20">Sorry no items found</td>
        </tr><%
	  end if%>
      </table>
</div>
<%end if%>
<p>&nbsp;</p>
<%
next
set col1= nothing
%>


<%
' 16 - Flour Station - Added on 18th April 2008 by Selva
		facility = "16"
		tno=3
		for curtno=1 to tno
			if curtno=1 then
				dtype="Morning"
			elseif curtno=2 then
				dtype="Noon"
			else
				dtype="Evening"
			end if
			intN=0
			set col1= object.DailyPackingSheetReport(deldate,facility,dtype)
			vRecArray = col1("DailyPackingSheet")
			vtotcustarray = col1("TotalCustomers")
			if isarray(vtotcustarray) then
				totpages = ubound(vtotcustarray,2)+1
			else
				totpages = 0
			end if
			curpage=0
	    	Facilityrec = col1("Facility")
			set col1= nothing

					mFontSize = 8
					mFooterSize =3
					mFooterFontSize = 3
					mExtraLine = 10
					mLine = 8
				if isarray(vrecarray) then
					mPage = 1
					currec=0
					totqty=0
					curcno = vrecarray(0,0)
					curvan = vrecarray(5,0)
					curpage=1	  
					for i = 0 to ubound(vRecArray,2)
						currec = clng(currec) + 1
						if clng(currec) = 1 then
							if mFontSize = 14 Then
								mLine = mline + 4
							else
								mLine = mline + 3
							end if	
							if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
								mPage = mPage + 1						
								mLine = 0
							End if
						end if
						if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then
							if mFontSize = 14 Then
								mLine = mLine + 2
							else
								mLine = mLine + 1
							end if	
						end if
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0
						End if          
						if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
							curvan = vrecarray(5,i)
							curcno = vrecarray(0,i)
							i=clng(i)-1
							if mFontSize = 14 Then
								mLine = mLine +4           
							else
								mLine = mLine +3           
							end if	
						
							if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then
								mPage = mPage + 1
								mLine = 0	
							else
								if curvan <> vrecarray(5,i)   Then										
									mPage = mPage + 1
									mLine = 0
								End if
							End if
							if i < ubound(vrecarray,2) then
							end if            
							curpage = curpage + 1
							currec = 0
							totqty=0
						end if
					next
	    
				else
					mPage = 1
				end if
				i = 0
if isarray(vrecarray) then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<div align="center">
  <center>
<table border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
    <center><b><font size="3">Flour Station PACKING SHEET - <%=dtype%><br></font></b></center>
      <center><b><font size="3">(divided to Van and Customers)<br></font></b></center>
      <table border="0" width="100%" style="font-family: Verdana; font-size: <%=mFontSize%>pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><br>
            <br>
            <b>Date of report:</b> <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            <b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><BR>
           
            </td>
        </tr>
        <tr>
          <td>Number of pages for this report: <%=mPage%></td>
          <td align="right">Page 1 of <%=mPage%><br></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
</center>
</div><%
if mFontSize = 14 Then
	mLine = 16
Else
	mLine = 8
End if%>

<div align="center">
<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt" align="center"><%
	  if isarray(vrecarray) then
		mTotalPage = mPage
		mPage = 1		
	    currec=0
	    totqty=0
	    curcno = vrecarray(0,0)
	    curvan = vrecarray(5,0)
	    curpage=1	  
        for i = 0 to ubound(vRecArray,2)
          currec = clng(currec) + 1
          if clng(currec) = 1 then%>
            <tr>
            <td colspan="4">
              <b>Van : <%=vrecarray(5,i)%></b><br>
              <b>Customer : <%=vrecarray(0,i)%> - <%=vrecarray(1,i)%> (<%=vrecarray(7,i)%>)</b>
            </td>            
            </tr>
            <tr>            
            <td align="left" bgcolor="#CCCCCC" height="20" width="20%"><b>Facility</b></td> 
			<td align="left" bgcolor="#CCCCCC" height="20" width="65%"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Qty</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="5%"><b>SID</b></td>
            </tr><%
            if mFontSize = 14 Then
							mLine = mline + 4
						else
							mLine = mline + 3
						end if	
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td colspan="4" align="right"><%
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
						End if
           end if
           if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then%>
          <tr>            
            <td align="left" width="20%"><%=vrecarray(8,i)%>&nbsp;</td>
			<td align="left" width="65%"><%=left(vrecarray(3,i),50)%>&nbsp;</td>
            <td align="center" width="10%"><%=vrecarray(4,i)%>&nbsp;</td>
			<%
            totqty = clng(totqty) + vrecarray(4,i)
			%>
            <td align="center" width="5%">&nbsp;</td>
          </tr><%
						if mFontSize = 14 Then
							mLine = mLine + 2
						else
							mLine = mLine + 1
						end if	
          end if
          if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
						mPage = mPage + 1						
						mLine = 0%>		
						</table>						
						<br class="page" />						
						<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td colspan="3" align="right"><%	
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
          End if
          
		  if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
		    curvan = vrecarray(5,i)
		    curcno = vrecarray(0,i)
		    i=clng(i)-1%>
		    </table>
						<br>
            <br><%
            if mFontSize = 14 Then
							mLine = mLine +4           
						else
							mLine = mLine +3           
						end if	
         if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then 						
         
						mPage = mPage + 1
						mLine = 0%>								
						<br class="page" /><%						
		  else
			if curvan <> vrecarray(5,i)   Then										
				mPage = mPage + 1
				mLine = 0%>													
				<br class="page" /><%
			End if				
          End if
          
            if i < ubound(vrecarray,2) then%>
							<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
							if mLine = 0 Then %>
								<tr>
									<td colspan="3" align="right"><%
									 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
								</tr><%						
							end if         
            end if   
            
		    curpage = curpage + 1
		    currec = 0
		    totqty=0
		  end if
	    next%>
	    <%else%>
        <tr>
        <td width="100%" colspan = "4" height="20">Sorry no items found</td>
        </tr><%
	  end if%>
      </table>
</div>
<%end if%>
<p>&nbsp;</p>
<%
next
set col1= nothing
%>

<%
'Delivery Sheet
tno=3
for curtno=1 to tno
    if curtno=1 then
      dtype="Morning"
    elseif curtno=2 then
      dtype="Noon"
    else
      dtype="Evening"
    end if
    for k=0 to ubound(vvecarray,2)
      van = vvecarray(0,k)

      set col1= object.DailyDeliverySheet(deldate,van,dtype)
vRecArray = col1("DeliverySheet")
vtotcustarray = col1("TotalCustomers")
if isarray(vtotcustarray) then
  totpages=ubound(vtotcustarray,2)+1
else
  totpages=0
end if
curpage=0
    
drivername = col1("Driver")
vanname=col1("Van")
set col1= nothing

mLine = 10	
	if isarray(vrecarray) then
		mPage = 1
		currec=0
		totqty=0
		curcno = vrecarray(0,0)
		curfac=0
		curpage=1
    for i = 0 to ubound(vRecArray,2)
			currec = clng(currec) + 1
	    if clng(currec) = 1 then
				mLine = mline + 4
				if mLine >= PrintPgSize+4 and i < ubound(vrecarray,2) Then 							
					mPage = mPage + 1						
					mLine = 0
				End if
			End if
			
	    if clng(curcno) = vrecarray(0,i) then
				mLine = mLine + 1
	    end if          
	    
	    if mLine >= PrintPgSize+4 and i < ubound(vrecarray,2) Then 
				mPage = mPage + 1						
				mLine = 1
	    End if
	          
	    if clng(curcno) <> vrecarray(0,i) then
				curcno = vrecarray(0,i)
			  i=clng(i)-1
	      mLine = mLine +3
				if mLine >= PrintPgSize+4-4 and i < ubound(vrecarray,2) Then 
					mPage = mPage + 1
					mLine = 0
				End if				
	        curpage = clng(curpage) + 1
			    currec = 0
			    totqty=0
			 end if
		  next		  
	Else
		mPage = 1	  
	end if

if isarray(vrecarray) then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<div align="center">
<center>
<table border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <center><b><font size="3">Drivers DELIVERY SHEET Van&nbsp;<%=van%> - <%=dtype%><br></font></b></center><br>
      <center><b><font size="3">(divided to Van and Customers)<br></font></b></center>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><br>
            <br>
            <b>Date of report:</b>  <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            <b>To be produced and delivered on</b> <%=deldate%> (<%=dtype%>)<br>
            <b>To the attention of: </b><%=vanname%><br>
            <!--<b>Van <%=van%></b><br>-->
            </td>
        </tr>
        <tr>
          <td>Number of pages for this report: <%=mPage%><Br></td>
          <td align="right">Page 1 of <%=mPage%></td>
         
        </tr>
        </table>
    </td>
  </tr>
</table>
</center>
</div>
	<%mLine = 10%>
	<div align="center">
	<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt" align="center"><%    
		  if isarray(vrecarray) then
		  mTotalPage = mPage
				mPage = 1
		    currec=0
		    totqty=0
		    curcno = vrecarray(0,0)
		    curfac=0
		    curpage=1
	        for i = 0 to ubound(vRecArray,2)
	          currec = clng(currec) + 1
	          if clng(currec) = 1 then%>
	            <tr>
	            <td colspan="5">              
	              <b>Customer : <%=vrecarray(0,i)%> - <%=vrecarray(1,i)%></b>
	            </td>
	           
	            </tr>
	            <tr>
	            <td align="left" bgcolor="#CCCCCC" height="20" width="23%"><b>Facility</b></td>
	            <td align="center" bgcolor="#CCCCCC" height="20" width="17%"><b>Product code</b></td>
	            <td align="left" bgcolor="#CCCCCC" height="20" width="50%"><b>Product name</b></td>
	            <td align="center" bgcolor="#CCCCCC" height="20" width="2%"><b>Qty</b></td>
	            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Short in Delivery</b></td>
	            </tr><%
	            mLine = mline + 4
							if mLine >= PrintPgSize+4 and i < ubound(vrecarray,2) Then 							
								mPage = mPage + 1						
								mLine = 0%>		
								</table>						
								<br class="page" />						
								<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt" align="center">
								<tr>
									<td colspan="5" align="right"><%	
										Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
								</tr><%
							End if
	           end if
	           if clng(curcno) = vrecarray(0,i) then%>
	            <tr>
	            <td align="left" width="23%"><%=left(vrecarray(3,i),20)%><%
	              totqty = clng(totqty) + vrecarray(6,i)%>
	             &nbsp;</td>
	            <td align="center" width="17%"><%=vrecarray(4,i)%>&nbsp;</td>
	            <td align="left" width="50%"><%=vrecarray(5,i)%>&nbsp;</td>
	            <td align="center" width="2%"><%=vrecarray(6,i)%>&nbsp;</td>
	            <td align="center" width="10%">&nbsp;</td>
	            </tr><%
	            mLine = mLine + 1
	          end if          
	          if mLine >= PrintPgSize+4 and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt" align="center">
							<tr>
									<td colspan="5" align="right"><%	
										Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
								</tr><%
							mline = 1	
	          End if
	          
	          
			  if clng(curcno) <> vrecarray(0,i) then
			    curcno = vrecarray(0,i)
			    i=clng(i)-1%>
	            <tr>
	            <td align="center" height="20" colspan="3">
	              <p align="right"><b><font size="2" face="Verdana">Total&nbsp;&nbsp; </font></b>
	            </td>
	            <td align="center" height="20"  width="2%">
	              <p align="center"><font size="2" face="Verdana"  width="2%"><%=totqty%></font></p>
	            </td>
	            <td align="center" height="20"  width="10%">
	              <p align="center"><font size="2" face="Verdana">&nbsp;</font></p>
	            </td>
	            </tr>            
			    </table>
	            <br>
	            <br><%
	            mLine = mLine +3
							if mLine >= PrintPgSize+4-4 and i < ubound(vrecarray,2) Then 
								mPage = mPage + 1
								mLine = 0%>								
								<br class="page" /><%						
							End if
	            if i < ubound(vrecarray,2) then%>
								<table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt" align="center"><%
								if mLine = 0 then%>																
									<tr>
										<td colspan="5" align="right"><%	
											Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
									</tr><%
								End if															                
	            end if      
			    curpage = clng(curpage) + 1
			    currec = 0
			    totqty=0
			  end if
		    next
		    if clng(currec) > 0 then%>
		      <tr>
	            <td align="center" height="20" colspan="3"  width="80%">
	              <p align="right"><b><font size="2" face="Verdana">Total&nbsp;&nbsp; </font></b>
	            </td>
	            <td align="center" height="20"  width="10%">
	              <p align="center"><font size="2" face="Verdana"><%=totqty%></font></p>
	            </td>
	            <td align="center" height="20"  width="10%">
	              <p align="center"><font size="2" face="Verdana">&nbsp;</font></p>
	            </td>
	            </tr> <%
			  end if%>		    
			    <tr><%
	      else%>
	        <tr>
	        <td width="100%" colspan = "5" height="20">Sorry no items found</td>
	        </tr><%
		  end if%>
      </table>
</div>
<%
end if
%>
<p>&nbsp;</p>
<%
next
%>
<p>&nbsp;</p>
<%
next
%>
<%
set object = nothing
%>
</body>
</html>