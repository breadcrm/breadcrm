<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
	dim fromdtr,todtr
	
	fromdt=request("fromdt")
	todt=request("todt")
	
	dim arr
	arr = Split(fromdt,"/")
	fromdtr = arr(2) & "-" & arr(1) & "-" & arr(0)
	
	arr = Split(todt,"/")
	todtr = arr(2) & "-" & arr(1) & "-" &  arr(0)	
	
	set objBakery = server.CreateObject("Bakery.Reports")
	objBakery.SetEnvironment(strconnection)
	set retcol = objBakery.Display_EmailSummary(fromdtr,todtr)
	recarray = retcol("EmailSummary")
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=email_summary_rep.xls" 
	%>
      <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%"  colspan="6"><b>Email Summary Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; <% if strStatus<>"" then%> - <%=strStatusLabel%><%end if%></b></td>
        </tr>
        <tr>
          <td width="10%" bgcolor="#CCCCCC"><b>Date&nbsp;</b></td>
          <td width="10%" bgcolor="#CCCCCC"><b>Time&nbsp;</b></td>
          <td width="30%" bgcolor="#CCCCCC"><b>Email&nbsp;</b></td>        
          <td width="10%"  bgcolor="#CCCCCC"><b>Cus. Code&nbsp;</b></td>
          <td width="40%" bgcolor="#CCCCCC"><b>Customer Name&nbsp;</b></td>     
        </tr>
		
		<%if isarray(recarray) then%>
				<%for i=0 to UBound(recarray,2)%>
					<tr>
						<td><%=recarray(3,i)%></td>
						<td><%=recarray(4,i)%></td>
						<td><%=recarray(2,i)%></td> 
						<td><%=recarray(0,i)%></td>
						<td><%=recarray(1,i)%></td>
					</tr>
				<%next%>
          <%else%>
			<tr>
				<td colspan="5" width="100%" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td>
			</tr>
          <%end if%>
      </table>
</body>
</html>
