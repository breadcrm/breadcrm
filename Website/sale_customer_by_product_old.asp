<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title>Sales Report - By Customer By Product</title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--

    function GetSelected(selectTag) {
        var selIndexes = "";

        for (var i = 0; i < selectTag.options.length; i++) {
            var optionTag = selectTag.options[i];
            if (optionTag.selected) {
                if (selIndexes.length > 0)
                    selIndexes += ", ";
                selIndexes += optionTag.value;
            }
        }
        return selIndexes;
    }
            
function ExportToExcelFile(){
	fromdate=document.form.txtfrom.value
	todate=document.form.txtto.value
	//	customer=document.form.cus.value
	customer = GetSelected(document.form.customer)

	CustomerGroup = GetSelected(document.form.CustomerGroup)
	//alert(GetSelected(document.form.CustomerGroup))
	document.location="ExportToExcelFile_sale_customer_by_product.asp?customer=" + customer + "&CustomerGroup=" + CustomerGroup + "&fromdt=" + fromdate + "&todt=" +todate
}

function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function ValidateForm(){
	if (document.form.customer.value=="" && document.form.CustomerGroup.value=="")
	{
		alert ("Please select customer(s) or Customer Group.");
		document.form.customer.focus();
		return  false;
	}
	if (document.form.customer.value!="" && document.form.CustomerGroup.value!="")
	{
		alert ("Please select customer(s) or Customer Group only.");
		document.form.customer.focus();
		return  false;
	}
	return  true;
}
//-->
</SCRIPT>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;

<%
dim objBakery
dim recarray
dim retcol
dim fromdt, todt, i,Totalturnover,Credit,TotalNetturnover, strCustomer, arCustomerGroup,vCustomerGroup,avCustomerGroup,avCustomer,j
Dim arCustomer,CustomerCol,vCustomer,vNumber,strWeekVanName,strWeekEndVanName,strWeekVanNo,strWeekEndVanNo,flag

vCustomer = Request.Form("customer")
vCustomerGroup=Request.Form("CustomerGroup")
if vCustomerGroup<>"" then
	vCustomer=""
end if
if vCustomer<>"" then
	vCustomerGroup=""
end if

fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")


if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set CustomerCol = objBakery.Display_CustomerList()
arCustomer = CustomerCol("Customer")
set CustomerCol = nothing

set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set CustomerCol = objBakery.Display_CustomerGroupList()
arCustomerGroup = CustomerCol("Customer")
set CustomerCol = nothing

'response.write("fromdt-" + fromdt + "<br>todt-"+ todt + "<br>vCustomer-"+vCustomer + "<br>vCustomerGroup-"+vCustomerGroup)

if isdate(fromdt) and isdate(todt)  then
	set retcol = objBakery.Display_Customer_By_ProductNew(fromdt,todt,vCustomer,vCustomerGroup)
	recarray = retcol("CustByProduct")
	'strWeekVanName = retcol("WeekVanName")
	'strWeekVanNo = retcol("WeekVanNo")
	'strWeekEndVanName = retcol("WeekEndVanName")
	'strWeekEndVanNo = retcol("WeekEndVanNo")
	'strCustomer=retcol("CusName")
	set retcol = nothing
end if
set objBakery = nothing
%>
<div align="center">
<center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
 <tr>
    <td width="100%"><b><font size="3">Sales Report -  Customer By Products <br>
      &nbsp;</font></b>
      <form method="post" action="sale_customer_by_product.asp" name="form">
	  <input type="hidden" name="cus" value="<%=vCustomer%>">
	  <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
       
		<tr>
		  <%
		  if vCustomer<>"" then
				avCustomer=split(vCustomer,",")
		  end if
		  %>
		  <td width="75"><b>Customer</b></td>
         <td>
		  	
			<select size="5" name="customer" multiple="multiple" style="font-family: Verdana; font-size: 8pt">
            <option value="">Select</option>
            <%if IsArray(arCustomer) then%>
            <%for i = 0  to UBound(arCustomer,2)%>
				<%if isArray(avCustomer) then%>
				<%flag="true"%>
				<%for j=0 to ubound(avCustomer)%>					
					<%if trim(arCustomer(0,i))= Trim(avCustomer(j)) then%>
						<option value="<%=arCustomer(0,i)%>" Selected><%=arCustomer(1,i)%></option>
						<%flag="false"%>
					<%end if %>										
				<%next%>
				<%if flag="true" then%>
					<option value="<%=arCustomer(0,i)%>"><%=arCustomer(1,i)%></option>				
				<%end if%>
            	<%else%>
					<option value="<%=arCustomer(0,i)%>" <%if trim(arCustomer(0,i))= Trim(vCustomer) then response.write " Selected"%>><%=arCustomer(1,i)%></option>
				<%end if%>
			<%next%>
            <%end if%>
          </select>
		  </td>
		  <td colspan="3">
		 	<table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
			<tr>
			<td><b>OR</b></td>
			<%
				  if vCustomerGroup<>"" then
						avCustomerGroup=split(vCustomerGroup,",")
				  end if
			%>
			<td width="130" align="right"><b>Customer Group</b></td>
			<td>
				<select size="5" name="CustomerGroup" multiple="multiple" style="font-family: Verdana; font-size: 8pt">
				<option value="">Select</option>
				<%if IsArray(arCustomerGroup) then%>
				<%for i = 0  to UBound(arCustomerGroup,2)%>
					<%if isArray(avCustomerGroup) then%>
					<%flag="true"%>
					<%for j=0 to ubound(avCustomerGroup)%>					
						<%if trim(arCustomerGroup(0,i))= Trim(avCustomerGroup(j)) then%>
							<option value="<%=arCustomerGroup(0,i)%>" Selected><%=arCustomerGroup(1,i)%></option>
							<%flag="false"%>
						<%end if %>										
					<%next%>
					<%if flag="true" then%>
						<option value="<%=arCustomerGroup(0,i)%>"><%=arCustomerGroup(1,i)%></option>				
					<%end if%>
					<%else%>
						<option value="<%=arCustomerGroup(0,i)%>" <%if trim(arCustomerGroup(0,i))= Trim(vCustomerGroup) then response.write " Selected"%>><%=arCustomerGroup(1,i)%></option>
					<%end if%>
				<%next%>
				<%end if%>
			  </select></td>
			</tr>
			</table>
		  </td>
		
        </tr>
		 <tr>
		<td>&nbsp;</td>
		<td><font color="#993300">If you want to select more than one customer press "<b>Ctrl</b>" and select the customers.</font></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		</tr>
        <tr>

           <td width="50"><b>From:</b></td>
						<td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom size="12" onFocus="blur();" name="txtfrom" value="<%=replace(fromdt,"from","")%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					<td width="50"><b>To:</b></td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=replace(todt,"to","")%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td><td>
            <input type="submit" value="Search" onClick="return ValidateForm()" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
		
        <tr>
          <td colspan="5" align="right" height="30">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>
       </table>
	  </form>
	   <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
      <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%" colspan="6"><b>Sales Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
        <tr height="24">
          <td width="12%"  bgcolor="#CCCCCC"><b>Product Code</b></td>
		  <td width="40%"  bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td width="8%" bgcolor="#CCCCCC" align="right"><b>No of Products</b></td>
          <td width="15%" bgcolor="#CCCCCC" align="right"><b>Total TurnOver <br>(Exl Credits)</b></td>
          <td width="8%" bgcolor="#CCCCCC" align="right"><b>Credit</b></td>
		  <td width="15%" bgcolor="#CCCCCC" align="right"><b>Total Net TurnOver</b></td>
        </tr>
			
				<%if isarray(recarray) then%>
				<% 				
				Totalturnover = 0.00
				vNumber = 0
				Credit=0.00
				TotalNetturnover=0.00
				
				%>
				<%for i=0 to UBound(recarray,2)%>
		  <tr height="22">
		  <td width="12%"><b><%=recarray(5,i)%></b></td>
		  <td width="40%"><b><%if recarray(0,i)<> "" then Response.Write recarray(0,i) end if%></b>&nbsp;</td>
          <td width="8%" align="right"><b><%if recarray(1,i)<> "" then Response.Write formatnumber(recarray(1,i),0) else Response.Write "0" end if%></b></td> 
          <td width="15%" align="right"><b><%if recarray(2,i)<> "" then Response.Write formatnumber(recarray(2,i),2) else Response.Write "0.00" end if%></b></td>
          <td width="8%" align="right"><b><%if recarray(3,i)<> "" then Response.Write formatnumber(recarray(3,i),2) else Response.Write "0.00" end if%></b></td>
          <td width="15%" align="right"><b><%if recarray(4,i)<> "" then Response.Write formatnumber(recarray(4,i),2) else Response.Write "0.00" end if%></b></td>
                   
          </tr>
          <%
          if recarray(1,i)<> "" then 
				 		vNumber = vNumber + formatnumber(recarray(1,i),0)
		  End if

          if recarray(2,i)<> "" then 
				 		Totalturnover = Totalturnover + formatnumber(recarray(2,i),2)
		  End if
		  
		  if recarray(3,i)<> "" then 
				 		Credit = Credit + formatnumber(recarray(3,i),2)
		  End if
		  
		  if recarray(4,i)<> "" then 
				 		TotalNetturnover = TotalNetturnover + formatnumber(recarray(4,i),2)
		  End if

          %>
          <%next%>
          <tr height="22">
           <td align="right" colspan="2"><b>Grand Total</b>&nbsp;</td>
           <td align="right" ><b>&nbsp;<%=formatnumber(vNumber,0)%></b></td> 
		   <td align="right" ><b>&nbsp;<%=formatnumber(Totalturnover,2)%></b></td> 
		   <td align="right" ><b>&nbsp;<%=formatnumber(Credit,2)%></b></td> 
           <td align="right" ><b>&nbsp;<%=formatnumber(TotalNetturnover,2)%></b></td> 
                    
          </tr>
          <%
          else%>
         <td colspan="6" width="100%" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td><% 
          end if%>
        </tr>

      </table>
    </td>
  </tr>
</table>
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;<br><br><br><br><br><br>
</center>
</div>
</body>
<%if IsArray(recarray) then erase recarray%>
<%if IsArray(arCustomer) then erase arCustomer%>
</html>