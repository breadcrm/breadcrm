<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim obj
dim objBakery
Dim arCreditStatements
Dim arCreditStatementDetails
Dim intI
Dim intN
Dim intF
Dim vCreditDate
dim strAddress, strLogo
dim strResellerName, strResellerAddress,strResellerTown,strResellerPostcode,strResellerTelephone,strResellerFax,strCompanyLogo,strResellerInvoiceFooterMessage,strVATRegNo
dim intRID
dim object,DisplayResellerDetail,vRecArrayReseller
dim strLogo1, strTele
intN = 0

vCreditDate = Request.Form ("deldate")

Set obj = server.CreateObject("bakery.daily")
obj.SetEnvironment(strconnection)
Set objBakery = obj.Display_CreditStatements(vCreditDate)
arCreditStatements =  objBakery("CreditStatements")
arCreditStatementDetails =  objBakery("CreditStatementDetails")

Set objBakery = Nothing
Set obj = Nothing

If isArray(arCreditStatements) Then
	intF = ubound(arCreditStatements, 2) + 1
End If
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Credit Statements</title>
<style type="text/css">
<!--
br.page { page-break-before: always; height:1px }
-->
</style>
</head>
<body topmargin="0" leftmargin="0"><%

if IsArray(arCreditStatements) Then
	For intI = 0 To ubound(arCreditStatements, 2)
	
	     If isArray(arCreditStatements) Then
             strLogo1 = arCreditStatements(20,intI)
             strTele = arCreditStatements(21,intI)
	                	
	        if strTele = "" Then
	            strTele = "_"
	        End If
        End If
	
	
		intN = 0
		If arCreditStatements(11, intI) = "Bread Factory" Then
			strLogo = "<b><font face=""Monotype Corsiva"" size=""5"">THE BREAD FACTORY</font></b>"
			strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		    strAddress = strAddress & "<br>" & vbCrLf
		    strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
		    strAddress = strAddress & "Watford, Herts WD17 1LA<br>" & vbCrLf
		    strAddress = strAddress & "</p>" & vbCrLf
		    strAddress = strAddress & "<p>" & vbCrLf
		    if strLogo1 <>  "" then
		        strAddress = strAddress & "Telephone: &nbsp; " & strTele & vbCrLf
		    else
		        strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
				strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf		        
		    end if    
		    strAddress = strAddress & "</p></font>"
		ElseIf arCreditStatements(11, intI) = "Gail Force" Then
			strLogo = "<b><font face=""Kunstler Script"" size=""7"">Gail Force</font></b>"
			strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		    strAddress = strAddress & "<br>" & vbCrLf
		    strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
		    strAddress = strAddress & "Watford, Herts WD17 1LA<br>" & vbCrLf
		    strAddress = strAddress & "</p>" & vbCrLf
		    strAddress = strAddress & "<p>" & vbCrLf
		    if strLogo1 <>  "" then
		        strAddress = strAddress & "Telephone: &nbsp; " & strTele & vbCrLf
		    else
		        strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
				strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
		   end if
		    strAddress = strAddress & "</p></font>"
		End If
		intRID=arCreditStatements(18,intI)
		if intRID<>"" and intRID<>"0" then
			set object = Server.CreateObject("bakery.Reseller")
			object.SetEnvironment(strconnection)
			set DisplayResellerDetail= object.DisplayResellerDetail(intRID)
			vRecArrayReseller = DisplayResellerDetail("ResellerDetail")
			set DisplayResellerDetail= nothing
			set object = nothing
			strResellerName=""
			strResellerAddress=""
			strResellerTown=""
			strResellerPostcode=""
			strResellerTelephone=""
			strResellerFax=""
			strCompanyLogo=""
			strResellerInvoiceFooterMessage=""
			strVATRegNo=""
			strResellerAddress=""
			if isarray(vRecArrayReseller) then
				strResellerName=vRecArrayReseller(1,0)
				strResellerAddress1=vRecArrayReseller(2,0)
				strResellerTown=vRecArrayReseller(3,0)
				strResellerPostcode=vRecArrayReseller(4,0)
				strResellerTelephone=vRecArrayReseller(5,0)
				strResellerFax=vRecArrayReseller(6,0)
				strCompanyLogo="images/ResellerLogos/" & vRecArrayReseller(8,0)
				strResellerInvoiceFooterMessage=vRecArrayReseller(9,0)
				strVATRegNo=vRecArrayReseller(10,0)
				
				strResellerAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
				if strResellerName<>"" then
					strResellerAddress = strResellerAddress & "<b>" & strResellerName & "</b><br>" & vbCrLf
				end if
				if strResellerAddress1<>"" then
					strResellerAddress = strResellerAddress & strResellerAddress1 & "<br>"& vbCrLf
				end if
				if strResellerTown<>"" then
					strResellerAddress = strResellerAddress & strResellerTown & "<br>" & vbCrLf
				end if
				if strResellerPostcode<>"" then
					strResellerAddress = strResellerAddress & strResellerPostcode & "<br>" & vbCrLf
				end if
				strResellerAddress = strResellerAddress & "<BR>" & vbCrLf
				
				if strLogo1 = "" then
				    if strResellerTelephone<>"" then
					    strResellerAddress = strResellerAddress & "Telephone: " &  strResellerTelephone & "<br>" & vbCrLf
				    end if
				    if strResellerFax<>"" then
					    strResellerAddress = strResellerAddress & "Fax:" &  strResellerFax & "<br>" & vbCrLf
				    end if
				
				else
				
				    strResellerAddress = strResellerAddress & "Telephone: " &  strTele & "<br>" & vbCrLf   
				
				End if
					strResellerAddress = strResellerAddress & "</p></font>"
			end if
		end if
		%>
<div align="center">
<center>
<table border="0" width="90%" height="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%" valign="top">
      <center>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="33%" valign="top">
          <%if intRID<>"" and intRID<>"0" then%>
			<font face="Verdana" size="1">VAT Reg No. <%=strVATRegNo%></font>
			<%=strResellerAddress%>
			<%else%>
			<font face="Verdana" size="1">VAT Reg No. XXXXXX</font>
			<%=strAddress%>
			<%end if%>
          </td>
          <td width="34%" valign="top" align="center">
          <p align="center">
            <%if strLogo1 <> "" then%>
            <img src="images/CustomerGroupLogo/<%=strLogo1%>"><br /> 
		  	<%elseif intRID<>"" and intRID<>"0" then%>
			<img src="<%=strCompanyLogo%>"><br>
			<%else%>
			<img src="images/BakeryLogo.gif" width="225" height="77"><br>
			<%end if%>
		  </p>
          </td>
          <td width="33%" valign="top" align="right"><b>
			<font face="Verdana" size="4">
			THIS IS A CREDIT<br>
			STATEMENT<br>
			</font>
			<font face="Verdana" size="2">PLEASE KEEP SAFE AND PASS TO YOU ACCOUNTS DEPARTMENT</font>
           </b></td>
        </tr>
        <tr>
          <td width="33%" valign="top"><b><font face="Verdana" size="4">&nbsp;</font></b></td>
          <td width="34%" valign="top"></td>
          <td width="33%" valign="top"></td>
        </tr>
        <tr>
          <td width="33%"></td>
          <td width="34%">
            <p align="center"><font size="3" face="Verdana"><b>Credit<br>
            </b>NUMBER: <%=arCreditStatements(0, intI)%></font></td>
          <td width="33%"></td>
        </tr>
        <tr>
          <td width="33%"><font size="3" face="Verdana"><b>&nbsp;</b></font></td>
          <td width="34%"></td>
          <td width="33%"></td>
        </tr>
      </table>
      </center>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="top"><font face="Verdana" size="2">
		  Customer Code: <%=arCreditStatements(12, intI)%><br>
		  Van Number: <%=arCreditStatements(13, intI)%><br>
		  Customer Name: <%=arCreditStatements(2, intI)%><br>
          Address: <%=arCreditStatements(4, intI)%>,&nbsp;<%=arCreditStatements(5, intI)%>,&nbsp;<%=arCreditStatements(6, intI)%>,&nbsp;<%=arCreditStatements(7, intI)%><br><br>
          Date: <%=arCreditStatements(1, intI)%><br><br>
          </font></td>
        </tr>
        
        </table>
      <div align="center">
        <center>
        <table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
          <tr>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Quantity</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Product code</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Unit price</b></td>
            <td width="20%" align="center" bgcolor="#CCCCCC" height="20"><b>Total price</b></td>
          </tr><%
			for intN = 0 to ubound(arCreditStatementDetails, 2)
				if arCreditStatements(0, intI) = arCreditStatementDetails(0, intN) then
		%><tr>
            <td align="center"><%=arCreditStatementDetails(1, intN)%>&nbsp;</td>
            <td align="center"><%=arCreditStatementDetails(2, intN)%>&nbsp;</td>
            <td align="center"><%=arCreditStatementDetails(3, intN)%>&nbsp;</td>
            <td align="center"><%=FormatNumber(arCreditStatementDetails(4, intN), 2)%>&nbsp;</td>
            <td width="20%" align="center"><%=FormatNumber(arCreditStatementDetails(5, intN), 2)%>&nbsp;</td>
            </tr><%
		          end if
	            next
          %>
        </table>
        </center>
      </div>

      <div align="center">
        <table border="0" width="90%" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <p align="right"><b><font size="2" face="Verdana">Goods total&nbsp;&nbsp; </font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%">
                    <p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(arCreditStatements(8, intI), 2)%></b></font></p>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          <tr>
            <td>
              <p align="right"><b><font size="2" face="Verdana">VAT total&nbsp;&nbsp; </font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%">
                    <p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(arCreditStatements(9, intI), 2)%></b></font></p>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          <tr>
            <td>
              <p align="right"><b><font size="2" face="Verdana">Invoice total&nbsp;&nbsp; </font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%">
                    <p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(cdbl(FormatNumber(arCreditStatements(8, intI), 2))+cdbl(FormatNumber(arCreditStatements(9, intI), 2)), 2)%></b></font></p>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          </table>
      </div>
<br>
      </td>
  </tr>
</table>
</center>
</div>
<% if intI < ubound(arCreditStatements, 2) Then %>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%
	End if
	Next
Else
%>
<table border="0" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
  <tr>
    <td width="100%">
      <p align="center"><font size="3" face="Verdana"><b>No records found</b></font></p>
    </td>
  </tr>
</table><%
End if
%>
<%
dim recarray1,retcol,recarray2,TotalGoods,TotalVAT,TotalInvoice
set objBakery = server.CreateObject("Bakery.Reseller")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.DailyCreditResellerList(vCreditDate)
recarray1 = retcol("DailyCreditResellerList")
set retcol=nothing
if isarray(recarray1) then
For intI = 0 To ubound(recarray1, 2)
TotalVAT=0
TotalGoods=0
TotalInvoice=0
set retcol = objBakery.GetResellerCreditNoteDetails(recarray1(0, intI),vCreditDate)
recarray2 = retcol("InvoiceDetails")
if isarray(recarray2) then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<div align="center">
<center>
<table border="0" width="90%" height="100%" cellspacing="0" cellpadding="0" style="background-repeat:no-repeat">
<tr>
<td width="100%" valign="top">
<table border="0" width="90%" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%" valign="top" >
<font face="Verdana" size="1">VAT Reg No. XXXXXX</font>
<%
strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
strAddress = strAddress & "<br>" & vbCrLf
strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
strAddress = strAddress & "Watford, Herts WD17 1LA" & vbCrLf
strAddress = strAddress & "" & vbCrLf
strAddress = strAddress & "<BR>" & vbCrLf
strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
strAddress = strAddress & "</p></font>"
%>
<%=strAddress%>
</td>
<td width="46%" valign="top" align="center">
<img src="images/BakeryLogo.gif" width="225" height="77"><br>

</td>
<td width="28%" valign="top">
<p align="center">
<font face="Verdana" size="1">THIS IS AN INVOICE</font><br>
<font face="Verdana" size="1">PLEASE KEEP SAFE AND PASS TO YOUR ACCOUNTS DEPARTMENT</font></b><font size="1">
</font>
</td>
</tr>
<tr><td height="10"></td></tr>
</table>
<table border="0" width="90%" cellspacing="0" cellpadding="0"  align="center">
<tr>
<td width="33%"></td>
<td width="33%">
<p align="center"><font size="3" face="Verdana"><b>Reseller's Credit<br>
</b>NUMBER: <%=recarray1(1, intI)%></font></td>
<td width="34%"></td>
</tr>
<tr><td height="10"></td></tr>
</table>
<table border="0" width="90%" cellspacing="0" cellpadding="0"  align="center">
<tr>
<td valign="top"><font face="Verdana" size="2"><b>
<% if recarray1(0, intI)<>"" then%><%=recarray1(0, intI)%><br><%end if%>
<% if recarray1(1, intI)<>"" then%><%=recarray1(1, intI)%><br><%end if%>
<% if recarray1(2, intI)<>"" then%><b><%=recarray1(2, intI)%></b><br><%end if%>
<% if recarray1(3, intI)<>"" then%><%=recarray1(3, intI)%><br><%end if%>
<% if recarray1(4, intI)<>"" then%><%=recarray1(4, intI)%><br><%end if%>
<% if recarray1(5, intI)<>"" then%><%=recarray1(5, intI)%><br><%end if%>
</b></font></td>
<td width="33%" valign="top"><font face="Verdana" size="2"><b>
Date: <%= Day(recarray1(6, intI)) & "/" & Month(recarray1(6, intI)) & "/" & Year(recarray1(6, intI)) %>
</b></font></td>
</tr>
<tr><td height="10"></td></tr>
</table>

<table border="1" align="center" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
<tr>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Quantity</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product code</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product name</b></td>
<%if (recarray1(16, intI)<>"1") then%>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Unit price</b></td>
<td width="20%" align="center" bgcolor="#CCCCCC" height="20"><b>Total price</b></td>
<%end if%>
</tr><%
for intN=0 to ubound(recarray2, 2)
%>
<tr>
<td align="center"><%=recarray2(2, intN)%>&nbsp;</td>
<td align="center"><%=recarray2(0, intN)%>&nbsp;</td>
<td align="center"><%=recarray2(1, intN)%>&nbsp;</td>
<%if isnull(recarray2(3, intN)) then%>
<td align="center">0.00</td><%
else%>
<td align="center"><%=FormatNumber(recarray2(3, intN), 2)%>&nbsp;</td><%
end if
if isnull(recarray2(4, intN)) then%>
<td width="20%" align="center">0.00</td><%
else%>
<td width="20%" align="center"><%=FormatNumber(recarray2(4, intN), 2)%>&nbsp;</td><%
end if%>
</tr>
<%
if not isnull(recarray2(4, intN)) then
	TotalGoods=TotalGoods+cdbl(recarray2(4, intN))	
end if
if not isnull(recarray2(5, intN)) then
	TotalVAT=TotalVAT+cdbl(recarray2(5, intN))	
end if
TotalInvoice=TotalVAT+ TotalGoods

next
%>
</table>
<table border="0" width="90%" cellspacing="0" cellpadding="0" align="center">
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">Goods total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%"><p align="center"><font size="3" face="Verdana"><b><%=FormatNumber(TotalGoods, 2)%></b></font></p></td>
</tr>
</table>

</td>
</tr>
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">VAT total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%"><p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(TotalVAT, 2)%></b></font></p></td>
</tr>
</table>

</td>
</tr>
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">Invoice total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%"><p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(TotalInvoice, 2)%></b></font></p>
</td>
</tr>
</table>
</td>
</tr>
</table>

<table border="0" width="90%" cellspacing="0" cellpadding="0"  align="center">
<tr>
	<td width="100%" height="10"></td>
</tr>
<tr>
<td width="100%">
<font face="Verdana" size="1">
<b>IMPORTANT!!</b><br>
Shortages must be reported on day of delivery. To ensure next day delivery, ring before 14:00 Mon - Fri
</font>
</td>
</tr>
<tr><td height="10"></td></tr>
<tr>
<td width="100%" align="center"><font face="Verdana" size="1"><%=strglobalinvoicefootermessage%></font></td>
</tr>
<tr><td height="10"></td></tr>
</table>

</td>
</tr>
</table>
</center>
</div>
<% if (intI<ubound(recarray1, 2)) then%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%
end if
end if
set retcol = Nothing
Next
Response.Flush()

end if
%>
</body>
</html><%
If IsArray(arCreditStatements) Then erase arCreditStatements
If IsArray(arCreditStatementDetails) Then erase arCreditStatementDetails
%>