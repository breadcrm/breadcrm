<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%

Dim Obj,ObjCustomer
Dim arCustomer

Dim strCustomerName,strCustNo,strGroup
Dim vPageSize
Dim strDirection
Dim vpagecount
Dim vCurrentPage
Dim i,StrOrderType
Dim arGroup


vPageSize = 25
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vCurrentPage	= Request.Form("CurrentPage") 
strCustomerName = Replace(Request.Form("txtCustName"),"'","''")
strCustNo = Replace(Request.Form("txtCustNo"),"'","''")
strGroup = Replace(Request.Form("txtGroup"),"'","''")


if strCustNo <> "" Then
	if  not IsNumeric(strCustNo) Then
		strCustNo = 0
	End if
End if

If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if

Set Obj = CreateObject("Bakery.Customer")
Obj.SetEnvironment(strconnection)
Set ObjCustomer = obj.Display_CustomerList(vPageSize,vCurrentPage,strCustomerName,strCustNo,strGroup,StrOrderType,"")
arCustomer = ObjCustomer("Customer")
vpagecount  = ObjCustomer("Pagecount")

set objCustomer = obj.Display_Grouplist()
arGroup = objCustomer("GroupList")	


Set Obj = Nothing
Set ObjCustomer = Nothing

%>


<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">


<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Set customer profile<br>
      &nbsp;</font></b>
      <form method="POST"  id="frmCustomer" name="frmCustomer">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          <td><b>Customer Code:</b></td>
		  <td><b>Customer Name:</b></td>
          <td><b>Customer Group:</b></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
             <td><input type="text" name="txtCustNo"  value="<%=strCustNo%>"size="20" style="font-family: Verdana; font-size: 8pt"></td>
			 <td><input type="text" name="txtCustName" value="<%=strCustomerName%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
				
				    <td>
							<select size="1" name="txtGroup" style="font-family: Verdana; font-size: 8pt">
							<option value=" ">Select</option><%
								If IsArray(arGroup) then
									For i = 0 to UBound(arGroup,2)%>
										<option value="<%=arGroup(0,i)%>" <%if Trim(arGroup(0,i)) = Trim(strGroup) Then Response.Write "Selected"%>><%=arGroup(1,i)%></option><%
									Next 						
								End if%>
							</select>				    
				    </td>
          <td>
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
			</form>
			
			<%if vCurrentPage <> 1 then%>
		<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">      
			<tr>
				<td height="20"></td>
			</tr>
			
			<%				
			if IsArray(arCustomer) Then%>
				<tr>
				  <td align="right"><b>Page <%=vCurrentPage%> of <%=vpagecount%></b></td>          
				</tr><%
			End if%>	
		</table> 
		<%End if%>

      <table border="0" width="100%" bgcolor="#999999" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
				<Form name="frmpro">
					<tr>
					  <td width="23%" bgcolor="#CCCCCC"><b>Customer Code</b></td>
					  <td width="43%" bgcolor="#CCCCCC"><b>Customer Name</b></td>
					  <td width="34%" bgcolor="#CCCCCC" align="center"><b>Action</b></td>
					</tr><%
					dim strResellerID
					if IsArray(arCustomer) Then 
						for i = 0 to UBound(arCustomer,2)%>
							<tr bgcolor="#FFFFFF">					
								<td width="23%"><%=arCustomer(0,i)%></td>
								<td width="43%"><%=arCustomer(1,i)%></td>
								<td width="34%">
								  <table border="0" width="100%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
								    <tr>               
								      <td width="50%" align="center"><input onClick="NewWindow('customer_products.asp?cname=<%=Server.URLEncode(arCustomer(1,i))%>&CustNo=<%=arCustomer(0,i)%>','profile','750','300','yes','center');return false" onFocus="this.blur()" type="button" value="Add Products" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
								      <td width="50%" align="center">
									  	<input onClick="NewWindow('customer_specials.asp?cname=<%=Server.URLEncode(arCustomer(1,i))%>&CustNo=<%=arCustomer(0,i)%>','profile','700','300','yes','center');return false" onFocus="this.blur()" type="button" value="Set Special Prices" name="B1" style="font-family: Verdana; font-size: 8pt">
									  </td>
 									</tr> 				 
								  </table>            
								</td>
							</tr><%
						Next
					else%>
						<tr>
							<td colspan="3">Sorry no items found</td>
						</tr><%          	
					End if%>		
					
        

        <tr>
          <td width="23%"></td>
          <td width="43%"></td>
          <td width="34%"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>


	<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		<FORM Name="Navigate">
			<tr>					
				<td width="187"><%				
		
					if vCurrentPage <> 1 and vCurrentPage <>0 then%>
						<input type="button" value="FirstPage" Onclick="document.frmFirst.submit()" id=button1 name=button1 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) > 1 then%>
						<input type="button" value="PreviousPage" Onclick="document.frmPrevious.submit()" id=button3 name=button3 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%				
					if clng(vCurrentPage) < vPagecount then%>
						<input type="button" value="NextPage" Onclick="document.frmNext.submit()" id=button5 name=button5 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) <> vPagecount and vPagecount > 0  then%>
						<input type="button" value="LastPage" Onclick="document.frmLast.submit()" id=button7 name=button7 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>					
			</tr>
		</FORM>
	</table>

  </center>
</div>
<FORM METHOD="post" NAME="frmFirst">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="First">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">				
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<INPUT TYPE="hidden" NAME="txtGroup"			VALUE="<%=strGroup%>">
	</FORM>

	<FORM METHOD="post" NAME="frmPrevious">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Previous">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<INPUT TYPE="hidden" NAME="txtGroup"			VALUE="<%=strGroup%>">
	</FORM>

	<FORM METHOD="post" NAME="frmNext">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Next">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<INPUT TYPE="hidden" NAME="txtGroup"			VALUE="<%=strGroup%>">
	</FORM>

	<FORM METHOD="post" NAME="frmLast">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Last">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<INPUT TYPE="hidden" NAME="txtGroup"			VALUE="<%=strGroup%>">
	</FORM>


</body>

</html><%
	if IsArray(arCustomer) then erase arCustomer
	if IsArray(arGroup) Then erase arGroup
%>