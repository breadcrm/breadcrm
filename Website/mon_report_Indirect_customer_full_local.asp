<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--

function ExportToExcelFile(){
	fromdate=document.form.txtfrom.value
	todate=document.form.txtto.value
	ResellerID1=document.form.ResellerID.value
	document.location="ExportToExcelFile_mon_report_Indirect_customer_full.asp?fromdt=" + fromdate + "&todt=" +todate + "&ResellerID=" +ResellerID1
}

function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim recarray
dim retcol
dim fromdt, todt, i,TotalDeliveries,Totalturnover,Averageturnover,strResellerID


fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")
strResellerID=Request.form("ResellerID")

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

if isdate(fromdt) and isdate(todt) then
set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.Display_Indirect_customer(fromdt,todt,strResellerID)
recarray = retcol("Customer")
end if

dim vResellers,object,detail
Set object = Server.CreateObject("bakery.Reseller")
object.SetEnvironment(strconnection)
set detail = object.ListResellers()
vResellers =  detail("Resellers")
set detail = Nothing
set object = Nothing

%>
<div align="center">
<center>
<table border="0" width="100%" cellspacing="0" cellpadding="5" style="font-family: Verdana; font-size: 8pt">

 
 <tr>
    <td width="100%"><b><font size="3">Customer Full Report</font></b>
	  <form method="post" action="mon_report_Indirect_customer_full.asp" name="form">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td colspan="4" height="15">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
			  <tr>
				<td width="65">&nbsp;<strong>Reseller:</strong></td>
				<td>
					<select size="1" name="ResellerID" style="font-family: Verdana; font-size: 8pt">
					  <option value="0">- - - Select - - -</option>
					  <%
					  for i = 0 to ubound(vResellers,2)%>
						   <% if (vResellers(0,i)=cint(strResellerID)) then%>
								<option value="<%=vResellers(0,i)%>" selected="selected"><%=vResellers(1,i)%></option>
						   <%else%>
								<option value="<%=vResellers(0,i)%>"><%=vResellers(1,i)%></option>
						   <%end if%>
					   <%next%>
				   	</select>
				</td>
			  </tr>
			</table>
			</td>
      	</tr>
		<tr>
          <td></td>
          <td><b>From:</b></td>
          <td><b>To:</b></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
         <td height="26" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom onFocus="blur();" size="12"  name="txtfrom" value="<%=fromdt%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=todt%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td>  <td>
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
	  </form>
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
		  </td>
  </tr>
</table>
      <% if request("Search")<>"" then%>
	  <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
         <tr>
          <td colspan="35" align="left" height="30">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>
		<tr>
          <td width="100%" colspan="35"><b>Customer Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
        <tr>
          <td width="50"  bgcolor="#CCCCCC"><b>Customer Code&nbsp;</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Old Code</b></td>
          <td width="300" bgcolor="#CCCCCC"><b>Customer Name&nbsp;</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Nick Name</b></td>
		  <td width="100" bgcolor="#CCCCCC"><b>Weekly Van No&nbsp;</b></td>
		  <td width="100" bgcolor="#CCCCCC"><b>Weekend Van No&nbsp;</b></td>
		  <td width="300" bgcolor="#CCCCCC"><b>Address&nbsp;</b></td>
		  
		  <td width="90" bgcolor="#CCCCCC"><b>Town</b></td>
		  <td width="150" bgcolor="#CCCCCC"><b>Post Code&nbsp;</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Tel No</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Fax</b></td>
		  <td width="100" bgcolor="#CCCCCC"><b>Type&nbsp;</b></td>
		  <td width="75" bgcolor="#CCCCCC"><b>Year&nbsp;</b></td>
		  <td width="170" bgcolor="#CCCCCC"><b>Customer Of&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>No of Invoices&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Terms&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><nobr><b>Sales Person&nbsp;</b></nobr></td>
		  <td width="120" bgcolor="#CCCCCC"><nobr><b>Account Manager&nbsp;</b></nobr></td>
		  <td width="175" bgcolor="#CCCCCC"><b>Notes&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Standing Order&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Purchase Order&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Group Name&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Day-to-day Contact Name</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Day-to-day Contact Phone </b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Decision maker Name</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Decision maker Phone </b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Default Price</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Formula (if F)</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Priceless Invoice/Credit</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Minimum Order Amount</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Group Minimum Order</b></td>
		  <td width="150" bgcolor="#CCCCCC"><b>Delivery Charge Option</b></td>
          <td width="150" bgcolor="#CCCCCC" align="right"><b>Total Number of Deliveries&nbsp;</b></td>
          <td width="150" bgcolor="#CCCCCC" align="right"><b>Total Turnover&nbsp;</b></td>
          <td width="200" bgcolor="#CCCCCC" align="right"><b>Average turnover per delivery&nbsp;</b></td>
        </tr>
		
		<%if isarray(recarray) then%>
		<% 				
		TotalDeliveries = 0
		Totalturnover = 0.00
		Averageturnover = 0.00
		%>
		<%for i=0 to UBound(recarray,2)%>
         <tr>
		  <td><%=recarray(0,i)%><% 'Customer Code %>&nbsp;</td>
		  <td><%=recarray(22,i)%><% 'Customer Old code%>&nbsp;</td>
          <td><%=recarray(1,i)%><% 'Customer Name %>&nbsp;</td>
		  <td><%=recarray(21,i)%><% 'Nickname%>&nbsp;</td>
		  <td><%=recarray(5,i)%><% 'Weekly Van No%>&nbsp;</td>
		  <td><%=recarray(6,i)%><% 'Weekend Van No %>&nbsp;</td>
		  <td><%=recarray(7,i)%>&nbsp;<%=recarray(8,i)%><% 'Address1 & address2 %>&nbsp;</td>
		 
		  <td width="90"><%=recarray(9,i)%><% 'Town%>&nbsp;</td>
		  <td><%=recarray(10,i)%><% 'Post Code %>&nbsp;</td>
		  <td width="90"><%=recarray(11,i)%><% 'Tel No%>&nbsp;</td>
		   <td><%=recarray(23,i)%><% 'Fax%>&nbsp;</td>
		  <td><%=recarray(12,i)%>&nbsp;</td>
		  <td><%=recarray(13,i)%>&nbsp;</td>
		  <td><%=recarray(14,i)%>&nbsp;</td>
		  <td><%=recarray(24,i)%><% 'No. of invoices %>&nbsp;</td>
		  <td><%=recarray(15,i)%><% 'Terms %>&nbsp;</td>
		  <td><%=recarray(16,i)%><% 'Sales Person %>&nbsp;</td>
		  <td><%=recarray(32,i)%><% 'Account Manager %>&nbsp;</td>
		  <td width="175"><%=trim(recarray(17,i))%>&nbsp;</td>
		  <td><%=recarray(18,i)%>&nbsp;</td>
		  <td><%=recarray(19,i)%>&nbsp;</td>
		  <td><%=recarray(20,i)%><% 'Group Name%>&nbsp;</td>
		  <td><%=recarray(25,i)%><% 'Day-to-day contact � name%>&nbsp;</td>
		  <td><%=recarray(26,i)%><% 'Day-to-day contact � phone %>&nbsp;</td>
		  <td><%=recarray(27,i)%><% 'Decision maker � name %>&nbsp;</td>
		  <td><%=recarray(28,i)%><% 'Decision maker � phone%>&nbsp;</td>
		  <td><%=recarray(29,i)%><% 'Default price %>&nbsp;</td>
		  <%if recarray(29,i)="F" then%>
		  <td bgcolor="#E6E6E6"><%=recarray(30,i)%><% 'Formula (if F)%>&nbsp;</td>
		  <%else%>
		  <td><%=recarray(30,i)%><% 'Formula (if F)%>&nbsp;</td>
		  <%end if%>
          <td align="center">
		  <%
		  if recarray(31,i)="1" then
		  	response.write("Yes")
		  else
		  	response.write("No")
		  end if
		  %><% 'Priceless Invoice/Credit%>&nbsp;</td>
		  
		  <td align="right" ><%=recarray(33,i)%></td>
		  <td align="right" ><%=recarray(34,i)%></td>
		  <td align="right"  width="150" ><%=recarray(35,i)%></td>
		  
		  <td align="right" bgcolor="#CCCCCC"><%=recarray(2,i)%></td> 
          <td align="right"><%if recarray(3,i)<> "" then Response.Write formatnumber(recarray(3,i),2) else Response.Write "0.00" end if%></td>
          <td align="right" bgcolor="#CCCCCC">
          <%
          	if recarray(3,i)<> "" and recarray(3,i)<> "" then
           		if recarray(3,i)>recarray(2,i) then
		           Response.Write formatnumber(recarray(3,i)/recarray(2,i),2)
		        else
		         	Response.Write "0.00" 
		        end if
	        else
	         	Response.Write "0.00" 
           end if
           %>
           </td>
          </tr>
          <%
          if recarray(2,i) <> "" then
             TotalDeliveries = TotalDeliveries + recarray(2,i)
          end if
          if recarray(3,i)<> "" then 
						Totalturnover = Totalturnover + formatnumber(recarray(3,i),2)
				  End if
				  if recarray(4,i)<> "" then
						Averageturnover = Averageturnover +  formatnumber(recarray(4,i),2)
					End if
          %>
          <%next%>
          <tr>
           	<td colspan="32" align="right"><b>Grand Total&nbsp;</b></td>
          	<td align="right" bgcolor="#CCCCCC"><b><%=TotalDeliveries%></b>&nbsp;</td> 
          	<td align="right"><b><%=formatNumber(Totalturnover,2)%></b>&nbsp;</td>
          	<td align="right" bgcolor="#CCCCCC"><b><%'=formatNumber(Averageturnover,2)%>&nbsp;</b></td>
          </tr>
          <%
          else%>
          <tr><td colspan="31" width="100%" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td></tr>
		  <%end if%>
      </table>
	   <%end if%>
  
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;<br><br><br><br><br><br>
</center>
</div>
</body>
<%if IsArray(recarray) then erase recarray%>
</html>