<%@ Language=VBScript%>
<%
option Explicit
Response.Buffer = true
%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function ExportToExcelFile()
{
	fromdate=document.form.txtfrom.value
	todate=document.form.txtto.value	
	document.location="ExportToExcelFile_mon_report_credit.asp?fromdt=" + fromdate + "&todt=" +todate
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim recarray
dim retcol
dim fromdt, todt,i
Dim GrandTotal

fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if
if isdate(fromdt) and isdate(todt) then
	set objBakery = server.CreateObject("Bakery.reports")
	objBakery.SetEnvironment(strconnection)
	set retcol = objBakery.Display_credits(fromdt,todt)
	recarray = retcol("Credits")
end if
%>
<div align="center">
<center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">

 <tr>
    <td width="100%"><b><font size="3">Credit Report</font></b><br><br>
	  
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
       <form method="post" action="mon_report_credit.asp" name="form">
	    <tr>
          <td></td>
          <td><b>From:</b></td>
          <td><b>To:</b></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom onFocus="blur();" size="12"  name="txtfrom" value="<%=fromdt%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
				</td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=todt%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td><td>
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
       </form>
      
      </table>
	 
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2" align="center">
<tr>
   <td align="right" height="20">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
</tr>
</table>
<table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
  <td width="100%"  colspan="21" height="30"><b>Credit Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
</tr>
<tr>
  <td width="8%" bgcolor="#CCCCCC"><nobr><b>Credit No</b></nobr></td>
  <td width="8%" bgcolor="#CCCCCC"><b>Date of Credit</b></td>
  <td width="8%" bgcolor="#CCCCCC"><b>Invoice No</b></td>
  <td width="8%" bgcolor="#CCCCCC"><b>Date of Invoice</b></td>
  <td width="8%" bgcolor="#CCCCCC"><b>Customer Code</b></td>
  <td width="18%" bgcolor="#CCCCCC"><b>Customer Name</b></td>
  <td width="10%" bgcolor="#CCCCCC"><b>Name of person making Complaint</b></td>
  <td width="4%" bgcolor="#CCCCCC"><b>Mon Van No</b></td>
  <td width="4%" bgcolor="#CCCCCC"><b>Tue Van No</b></td>
  <td width="4%" bgcolor="#CCCCCC"><b>Wed Van No</b></td>
  <td width="4%" bgcolor="#CCCCCC"><b>Thu Van No</b></td>
  <td width="4%" bgcolor="#CCCCCC"><b>Fri Van No</b></td>
  <td width="4%" bgcolor="#CCCCCC"><b>Sat Van No</b></td>
  <td width="4%" bgcolor="#CCCCCC"><b>Sun Van No</b></td>    
  <td width="8%" bgcolor="#CCCCCC"><nobr><b>Product Code</b></nobr></td>
  <td width="18%" bgcolor="#CCCCCC"><b>Product Name</b></td>
  <td width="11%" bgcolor="#CCCCCC"><b>Reason for Credit</b></td>
  <td width="11%" bgcolor="#CCCCCC"><b>Responsibility for Complaint</b></td>
  <td width="5%" bgcolor="#CCCCCC"><b>Comments</b></td>
  <td width="6%" bgcolor="#CCCCCC"><b>Units</b></td>
  <td width="8%" bgcolor="#CCCCCC" align="right"><b>Value</b></td>
</tr>
<tr>
<%if isarray(recarray) then%>
<% GrandTotal = 0.00 %>
<%for i=0 to UBound(recarray,2)%>
<tr style="font-weight:bold">
<td>
<%
if (recarray(0,i)>0) then
	response.write(recarray(0,i))
end if
%>&nbsp</td>
<td><%=FormatDateInDDMMYYYY(recarray(20,i))%>&nbsp</td>
<td><%=recarray(21,i)%>&nbsp</td>
<td><%=FormatDateInDDMMYYYY(recarray(22,i))%>&nbsp</td>
<td><%=recarray(2,i)%>&nbsp</td>
<td><%=recarray(1,i)%>&nbsp</td>
<td><%=recarray(19,i)%>&nbsp;</td>
<td><%=recarray(10,i)%>&nbsp</td>
<td><%=recarray(11,i)%>&nbsp</td>
<td><%=recarray(12,i)%>&nbsp</td>
<td><%=recarray(13,i)%>&nbsp</td>
<td><%=recarray(14,i)%>&nbsp</td>
<td><%=recarray(15,i)%>&nbsp</td>
<td><%=recarray(16,i)%>&nbsp</td>
<td><nobr><%=recarray(7,i)%>&nbsp</nobr></td>
<td><%=recarray(3,i)%>&nbsp</td>
<td><%=recarray(5,i)%>&nbsp</td>
<td><%=recarray(18,i)%>&nbsp;</td>
<td><%=recarray(17,i)%>&nbsp;</td>
<td align="right"><%=recarray(4,i)%>&nbsp</td>
<td align="right"><b>&nbsp;<%if recarray(6,i)<> "" then Response.Write formatnumber(recarray(6,i),2) else Response.Write "0.00" end if%></b></td>

</tr>
<%
  if recarray(6,i) <> "" then
	 GrandTotal = GrandTotal + formatnumber(recarray(6,i),2)
  end if
%>
<%next%>
<tr>
  <td width="10%" colspan="20"  bgcolor="#CCCCCC" align="right"><b>Grand Total</b>&nbsp;&nbsp;</td>
  <td width="10%"  bgcolor="#CCCCCC" align="right"><b>&nbsp;<%=formatNumber(GrandTotal,2)%></b></td>
</tr>
<%else%>
  <td bgcolor="#FFFFFF" colspan="21" align="center" height="50"><b><font color="#FF0000">0 - No records matched...</font></b></td>
<%end if%>
</tr>
</table>
</td>
</tr>
</table>
</center>
</div>
</body>
<%if IsArray(recarray) then erase recarray %>
</html>