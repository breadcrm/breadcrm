<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%
Response.Buffer = true
Server.ScriptTimeout=6000
dim objBakery
dim recarray
dim retcol
dim fromdt, todt,i
Dim GrandTotal, strTimeSeriesProductList, recarrayHeaderName

cno = replace(Request("txtcno"),"'","''")
cname = replace(Request("txtcname"),"'","''")

fromdt = replace(Request("txtfrom"),"'","''")
todt = replace(Request("txtto"),"'","''")
strGroup = Replace(Request("txtGroup"),"'","''")
strordstatus = replace(Request("ordstatus"),"'","''")
strpcode=replace(Request("txtpcode"),"'","''")
strvno=Request("selvno")
strnewsearchoption=Request("newsearchoption")

if fromdt <> "" Then
	arSplit = Split(fromdt,"/")
	fromdtnew = arSplit(2) & "-" & arSplit(1) & "-" & arSplit(0)
End if

if todt <> "" Then
	arSplit = Split(todt,"/")
	todtnew = arSplit(2) & "-" & arSplit(1) & "-" & arSplit(0)
End if

if (isnumeric(cno)) then
	intcno=cno
else
	intcno=0
end if

if (isnumeric(strGroup)) then
	intgroupno=strGroup
else
	intgroupno=0
end if

if (strordstatus="") then
	strordstatus="Yes"
end if

if (strordstatus="All") then
	strordstatusnew=""
else
	strordstatusnew=strordstatus
end if

vPageSize = 15000

Set Obj = CreateObject("Bakery.Customer")
Obj.SetEnvironment(strconnection)
Set ObjCustomer = obj.CustomerOrders(fromdtnew,todtnew,intcno,intgroupno,cname,strordstatusnew,strpcode,strvno,strnewsearchoption)
arCustomer = ObjCustomer("CustomerOrders")
vRecordcount=UBound(arCustomer,2)+1
set objCustomer = obj.Display_Grouplist()
arGroup = objCustomer("GroupList")
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=FindCustomerOrder_Report.xls" 

%>

<table border="0" cellspacing="1" cellpadding="2">
        <tr>
          <td><nobr><b>Customer Code</b></nobr></td>
          <td><b>Customer Name</b></td>
		 <%
		  intday=DateDiff("d",fromdt,todt)
		  for i=0 to intday%>
		  <td width="90" bgcolor="#CCCCCC"><nobr><strong><%=displayBristishDate(DATEADD("d",i,fromdt))%>&nbsp;&nbsp;&nbsp;&nbsp;</strong></nobr></td>
		  <%next%>
          </tr>
		  <%
					if IsArray(arCustomer) Then 
						for i = 0 to UBound(arCustomer,2)
							if (i mod 2 =0) then
								strbg="#FFFFFF"
							else
								strbg="#F3F3F3"
							end if
						%>
							<tr bgcolor="<%=strbg%>">
							  <td><%=arCustomer(0,i)%></td>
							  <td><%=arCustomer(1,i)%></td>
							 <%
							  strallordstatus=arCustomer(2,i)
							  strIsStopStatus=arCustomer(4,i)							 
							  if strallordstatus <> "" Then							  	
								arallordstatus=Split(strallordstatus,"|")
								arIsStopStatus=Split(strIsStopStatus,"|")
								
								arNoofproducts=Split(arCustomer(15,i),"|")
								arValueofOrder=Split(arCustomer(16,i),"|")
																
								for j=0 to UBound(arallordstatus)-1
									dtdate=displayBristishDate(DATEADD("d",j,fromdt))
									%>
								 	<td>
								 	<%
								 	if (strnewsearchoption="2") then
										response.write(arNoofproducts(j))
									elseif (strnewsearchoption="3") then
										response.write(FormatNumber(arValueofOrder(j),2))
									else
								 		response.write(arallordstatus(j))
									end if	
								 	%>
								 </td>
								 </form>
							  	<%
								next
							  End if
							  %>
							</tr>
							<%
							response.Flush()
						Next
					else
					%>
					<tr>
						<td colspan="3" width="570">Sorry no items found</td>							
					</tr>
					<%
					End if
					%>      
</table>
<%
Set Obj = Nothing
Set ObjCustomer = Nothing
%>