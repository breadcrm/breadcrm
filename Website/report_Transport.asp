<%@  language="VBScript" %>
<%

Response.Buffer = true
%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"><!--#INCLUDE FILE="includes/head.inc" -->
    <title></title>
    <link media="all" href="calendar/calendar-system.css" type="text/css" rel="stylesheet">


</head>
<SCRIPT language=JavaScript type=text/javascript>


function ExportToExcelFile(){

	cNo=document.form.txtCustNo.value
	cName=document.form.txtCustName.value
	vNo=document.form.VNo_Def.value
	document.location="ExportToExcelFile_report_Transport.asp?cno=" + cNo + "&cname=" + cName + "&vno=" + vNo 
}


</script>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana;
     font-size: 8pt" onLoad="initValueCal()">
    <!--#INCLUDE FILE="nav.inc" -->
    &nbsp;&nbsp;&nbsp;<br>
    &nbsp;&nbsp;&nbsp;
    <%
dim objBakery, objBakery1
dim recarray,vVecarray
dim retcol,retcol2
Dim strCustomerName,strCustNo
Dim vanNo
Dim VanID
VanID = -1
count1 = 0


set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set retcol2 = objBakery.GetVehicalDetails()
vVecarray = retcol2("VehicalDetails")
'stop
strCustomerName = replace(Request.Form("txtCustName"),"'","''")
strCustNo = replace(Request.Form("txtCustNo"),"'","''")
vanNo = Request.Form("VNo_Def")
set objBakery1 = server.CreateObject("Bakery.MgmReports")
objBakery1.SetEnvironment(strconnection)
set retcol = objBakery1.Display_Transport_Report(strCustNo,strCustomerName,vanNo)
recarray = retcol("TransportReport")
set objBakery = nothing
set objBakery1 = nothing
'if isempty(vanNo) Then'
'    vanNo = -1
'End if
''stop
    %>
    <div align="center">
        <center>
            <table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana;
                font-size: 8pt">
                <tr>
                    <td width="100%">
                      <b><font size="3">Transport</font><font size="3"> Report<br>
                      &nbsp;</font></b>
                      <form method="post" action="report_Transport.asp" name="form">
							<table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
							  <tr>
								<td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
								<td><b>Customer Code:</b></td>
								<td><b>Customer Name:</b></td>
								<td><b>Van :</b></td>
								<td></td>
							  </tr>
							  <tr>
								<td></td>
								<td><input type="text" name="txtCustNo"  value="<%=strCustNo%>"size="20" style="font-family: Verdana; font-size: 8pt"></td>
								<td><input type="text" name="txtCustName" value="<%=strCustomerName%>" size="50" style="font-family: Verdana; font-size: 8pt"></td>
								<td><select size="1" name="VNo_Def" style="font-family: Verdana; font-size: 8pt">
									<option value="-1">Select</option>
										<%for i = 0 to ubound(vVecarray,2)%>
										
										 <%if CInt(vanNo) = vVecarray(0,i) then%>
												<option value="<%=vVecarray(0,i)%>" selected="selected"><%=vVecarray(0,i)%></option>
										 <%else%>
												<option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
										 <%end if%>
										<%next%>
									</select>
								</td>
								<td>
								  <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
							  </tr>
								<tr>
								  <td colspan="5" align="right" height="30">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
								</tr>
						</table>
                      </form>
							<table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
									<tr>
									<%
									if vanNo < 0 then%>
									  <td width="100%" colspan="11"><b>Transport  Report</b></td>
									<%else%>
									  <td width="100%" colspan="11"><b>Transport  Report - Vehile No : <%=vanNo%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
									<% end if%>
									</tr>
									<tr height="24">
									  <td bgcolor="#CCCCCC"><b>Customer No</b></td>
									  <td bgcolor="#CCCCCC"><b>Customer Name</b></td>
									  <td bgcolor="#CCCCCC"><b>Address</b></td>
									  <td bgcolor="#CCCCCC"><b>Delivery Frequency </b></td>
									  <td bgcolor="#CCCCCC"><nobr><strong>Week Day Vehicle No </strong></nobr></td>
									  <td bgcolor="#CCCCCC"><nobr><b>Weekend Vehicle No </b></nobr></td>
									  <td bgcolor="#CCCCCC" align="right"><nobr><b>Delivery Time </b></nobr></td>
									</tr>
									<%if isarray(recarray) then%>
		
									<%for i=0 to UBound(recarray,2)%>

									  <tr height="22">
									  <td><b><%=recarray(0,i)%></b></td>
									  <td><b><%if recarray(1,i)<> "" then Response.Write recarray(1,i) end if%></b>&nbsp;</td>
									  <td ><b><%if recarray(2,i)<> "" then Response.Write recarray(2,i) end if%></b>&nbsp;</td>
									  <td ><b><%if recarray(3,i)<> "" then Response.Write recarray(3,i) end if%></b>&nbsp;</td>
									  <td><b><%if recarray(4,i)<> "" then Response.Write recarray(4,i) end if%></b>&nbsp;</td>
									  <td><b><%if recarray(5,i)<> "" then Response.Write recarray(5,i) end if%></b>&nbsp;</td>
									  <td><b><%if recarray(6,i)<> "" then Response.Write recarray(6,i) end if%></b>&nbsp;</td>
									  

									  </tr>
							
									  <%next%>
									  <%Response.Flush()
									  response.End() %>
							
									  <%
									  else%>
									 <td colspan="11" width="100%" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td><% 
									  end if%>
									</tr>
      </table>
                  </td>
                </tr>
            </table>
        </center>
    </div>
</body>
<%if IsArray(recarray) then erase recarray %>
</html>
