<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function ExportToExcelFile(){
	document.location = "ExportToExcelFile_multipleproduct_relationship_report.asp"
}

function Hide()
{
	document.getElementById("HidePanel").style.display = "none";
}

function Onclick_Submit() {
    document.frmReport.submit();
    return true;
}

//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="Hide()">
<!--#INCLUDE FILE="nav.inc" -->
<br>
<DIV id="HidePanel">
	<P align="center"><b><font size="2" face="Verdana" color="#0099CC">Report generation in progress, Please wait...</font></b></P>
	<p align="center"><img name="animation" src="images/LoadingAnimation.gif" border="0" WIDTH="201" HEIGHT="33"></p>
</DIV>

<%
dim objBakery
dim recarray
dim retcol

strStatus=request("Status")

set objBakery = server.CreateObject("Bakery.Reports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.Display_ReportMultiProductRelationship(strStatus)
recarray = retcol("MultipleProductList")

%>
<div align="center">
<form method="POST" action="report_multipleproduct_relationship.asp" name="frmReport">
<center>
<table border="0" width="100%" cellspacing="0" cellpadding="5" style="font-family: Verdana; font-size: 8pt">
<tr>
    <td width="100%"><b><font size="3">Multiple Product Relationships Report<br>
      &nbsp;</font></b>
      </td>
      </tr>

<tr><td>  <select name="Status" >
		    <option value="A" <% if strStatus="A" then%> selected="selected" <%end if%>>Active</option>
			<option value="D" <% if strStatus="D" then%> selected="selected" <%end if%>>Deleted</option>
			<option value="0" <% if strStatus="0" then%> selected="selected" <%end if%>>All</option>
		  </select>
		  &nbsp;&nbsp;           
		   <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt" onClick="return Onclick_Submit();"></td>

		  
		  </td></tr>
 
 <tr>
    <td width="100%">
	  
      	<table border="1" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
         <tr>
          <td colspan="7" align="left" height="30">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>
		<tr>
          <td colspan="7"><b>Multiple Product Relationships Report</b></td>
        </tr>
        <tr>
          <td width="110" bgcolor="#CCCCCC"><b>Group</b></td>
          <td width="110" bgcolor="#CCCCCC"><b>Multiple PNo</b></td>
		  <td width="200" bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td width="110" bgcolor="#CCCCCC"><b>Sub Product No</b></td>
          <td width="200" bgcolor="#CCCCCC"><b>Sub Product Name</b></td>
		  <td width="110" bgcolor="#CCCCCC"><b>Qty</b></td>
        </tr>
       <%if isarray(recarray) then%>
		
		<%for i=0 to UBound(recarray,2)%>
		<tr>
          <td><%=recarray(0,i)%></td>
          <td><%=recarray(1,i)%></td>
		  <td><%=recarray(2,i)%></td>
          <td><%=recarray(3,i)%></td> 
		  <td><%=recarray(4,i)%></td>
		  <td align="right"><%=recarray(5,i)%></td>
         </tr>
         
          <%next
         
          else%>
          	<tr><td colspan="7"  bgcolor="#CCCCCC"><b>0 - No records matched...</b></td></tr>
		  <%end if%>
      </table>
    </td>
  </tr>
</table>
<br>
</center>
</form>
</div>
</body>
<%if IsArray(recarray) then erase recarray %>
</html>


