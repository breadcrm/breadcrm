<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
vCustNo= request.form("CustNo")
vinvoiceno= request.form("invoiceno")

if vCustNo = "" then
	vCustNo= Request.QueryString ("vCustNo")
	vinvoiceno= Request.QueryString ("vinvoiceno")
end if

if vCustNo= "" or vinvoiceno = "" or (not isnumeric(vinvoiceno)) then response.redirect "credit_new.asp"

set object = Server.CreateObject("bakery.credit")
object.SetEnvironment(strconnection)
CheckCredit = object.CheckCredit(vCustNo,vinvoiceno)

if CheckCredit <> "Ok" then response.redirect "credit_new.asp?error=CheckCredit"

set DisplayInvoiceDetail= object.DisplayInvoiceDetail(vCustNo,vinvoiceno)
vRecArray1 = DisplayInvoiceDetail("CustomerDetail")
vRecArray2 = DisplayInvoiceDetail("ProductDetail")
vRecArray3 = DisplayInvoiceDetail("OrderDateDetail")
vRecArray4 = DisplayInvoiceDetail("ComplaintResponsibility")

if (isarray(vRecArray3)) then
	strDelDate=vRecArray3(0,0)
	if (strDelDate="") then
		strDelDate=vRecArray3(1,0)
	end if
end if
set DisplayInvoiceDetail= nothing


if not (isarray(vRecArray1) and isarray(vRecArray2)) then  response.redirect "credit_new.asp"

%>

<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script Language="JavaScript">
<!--
function showsubproducts(vpno)
{
	vimgname="myImg" + vpno;
	vitr="tr" + vpno;
	if (document.getElementById("plusminus").value=="")
	{	
		document.getElementById(vimgname).src = "images/minus.png";
		document.getElementById("plusminus").value=1;
		document.getElementById(vitr).style.display = "";
	}
	else
	{
		document.getElementById(vimgname).src = "images/plus.png";
		document.getElementById("plusminus").value="";
		document.getElementById(vitr).style.display = "none";
	}
}

function ShowCreditAllItems()
{
	var vnumberofproducts=document.getElementById("numberofproducts").value;
	if(document.form1.CreditAllItems.checked == true)
    {
       for (i=0;i<=vnumberofproducts;i++)
	   {
	   	vQty="Qty" + i;
		vTotalQty="TotalQty" + i;
		//vReason="Reason" + i;
		//vComplaintResponsibility="ComplaintResponsibility" + i;
		
		document.getElementById(vQty).value=document.getElementById(vTotalQty).value
		//document.getElementById(vReason).value="Other";
		//document.getElementById(vComplaintResponsibility).value=72;
		
	   }
    }
	else
	{
		for (i=0;i<=vnumberofproducts;i++)
	   	{
			vQty="Qty" + i;
			vTotalQty="TotalQty" + i;
			//vReason="Reason" + i;
			//vComplaintResponsibility="ComplaintResponsibility" + i;
			document.getElementById(vQty).value="";
			//document.getElementById(vReason).selectedIndex=0;
			//document.getElementById(vComplaintResponsibility).value="";
	   	}
	}
}

function ShowCreditAllItemsReasonSearch()
{
	var vnumberofproducts=document.getElementById("numberofproducts").value;
	if(document.getElementById("ReasonSearch").value!="")
    {
	   for (i=0;i<=vnumberofproducts;i++)
	   {
	   		vReason="Reason" + i;		
			document.getElementById(vReason).value=document.getElementById("ReasonSearch").value;		
	   }
    }
	else
	{
		for (i=0;i<=vnumberofproducts;i++)
	   	{
			vReason="Reason" + i;
			document.getElementById(vReason).selectedIndex=document.getElementById(vReason).value=document.getElementById("ReasonSearch").value;;
	   	}
	}
}

function ShowCreditAllItemsComplaintResponsibilitySearch()
{
	var vnumberofproducts=document.getElementById("numberofproducts").value;
	if(document.getElementById("ComplaintResponsibilitySearch").value!="")
    {
	   for (i=0;i<=vnumberofproducts;i++)
	   {
	   		vComplaintResponsibility="ComplaintResponsibility" + i;		
			document.getElementById(vComplaintResponsibility).value=document.getElementById("ComplaintResponsibilitySearch").value;		
	   }
    }
	else
	{
		for (i=0;i<=vnumberofproducts;i++)
	   	{
			vComplaintResponsibility="ComplaintResponsibility" + i;
			document.getElementById(vComplaintResponsibility).selectedIndex=0;
	   	}
	}
}

function CheckMultipleProduct()
{
  if (document.form1.nameofpersonmakingcomplaint.value=="")
  {
    alert("Please enter a value for the \"Name of person making complaint\" field.");
    document.form1.nameofpersonmakingcomplaint.focus();
    return false;
  }
	
	var vnumberofproducts=document.getElementById("numberofproducts").value;
	vmainwarnning=true;
	for (i=0;i<=vnumberofproducts;i++)
	{
		vismultipleproduct="ismultipleproduct" + i;
		if (document.getElementById(vismultipleproduct).value==1)
		{
			vnumberofsubproducts="numberofsubproducts" + i;
			vQty="Qty" + i;
			vQtyOld="TotalQty" + i;
			
			vwarnning=true;
			vmainqty=document.getElementById(vQty).value;
			vmainqtyold=document.getElementById(vQtyOld).value;
			//alert(vmainqtyold)
			
			vnumberofsubproductsvalue=document.getElementById(vnumberofsubproducts).value;
			if (vnumberofsubproductsvalue>0)
			{
				for (j=0;j<=vnumberofsubproductsvalue-1;j++)
				{				
					vsubQty="Qty" + i + j;
					vsubQtyOld="TotalQty" + i + j;
					vsubqty=document.getElementById(vsubQty).value;
					vsubqtyold=document.getElementById(vsubQtyOld).value;
					if (vsubqty=="")
						vsubqty=0;
					if (vsubqtyold=="")
						vsubqtyold=0;	
						
					veneteredvalue=vsubqtyold/vmainqtyold;
					veneteredvaluenew=vsubqty/vmainqtyold;
					
					if (vsubqty<veneteredvalue)
						vwarnning=false
					
					//Math.floor(veneteredvaluenew)
					//alert(vsubqtyold);				
				}
				if (vwarnning==true)
				{
					if (confirm('Warning: Total value for subproduct is greater than the value for the Multiple Product. The credit will revert to a credit for the whole Multiple Product.'))
						return true
					else
						return false
				}
			}	
			
		}		
	}
	
}
//-->
</Script>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
<form method="POST" action="credit_finish.asp" name="form1"> 
  <tr>
   <input type="hidden" name="vCustNo" value="<%=vCustNo%>">
  <input type="hidden" name="vinvoiceno" value="<%=vinvoiceno%>">
  <input type="hidden" name="vDelDate" value="<%=strDelDate%>">
  <td width="100%"><b><font size="3">Create new credit<br></font></b>
<%
error = Request.QueryString ("error")

if error = "Qty" then
%>
<br><b><font size="2" color="#ff0000">Invalid Quantities entered in the Quantity Ordered fields.<br></font></b>
<%
elseif error = "reason" then
%>
<br><b><font size="2" color="#ff0000">You have not selected the reason.<br></font></b>
<%
end if
%>&nbsp;
      <table border="0" cellspacing="2" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="950">
        <tr>
          <td width="100">Code</td>
          <td width="850"><%=vCustNo%></td>
        </tr>
        <tr>
          <td>Invoice No</td>
          <td><%=vinvoiceno%></td>
        </tr>
        <tr>
          <td><b>Name</b></td>
          <td><%=vRecArray1(0,0)%></td>
        </tr>
        <tr>
          <td>Address</td>
          <td><%=vRecArray1(2,0)%></td>
        </tr>
        <tr>
          <td></td>
          <td><%=vRecArray1(3,0)%></td>
        </tr>
        <tr>
          <td>Town</td>
          <td><%=vRecArray1(4,0)%></td>
        </tr>
        <tr>
          <td>Postcode</td>
          <td><%=vRecArray1(5,0)%></td>
        </tr>
        <tr>
          <td>Telephone</td>
          <td><%=vRecArray1(6,0)%></td>
        </tr>
		<tr>
          <td colspan="2"><strong>Name of person making complaint:</strong> <input type="text" name="nameofpersonmakingcomplaint" id="nameofpersonmakingcomplaint" size="35" style="font-family: Verdana; font-size: 8pt" maxlength="30"> <font color="#FF0000">*</font></td>
        </tr>
		<tr>
          <td colspan="2">
		  <strong>
		  <div style="font-family: Verdana; font-size: 12px">Credit All Items</div>
		  </strong>
		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  Qty to be credited: <input name="CreditAllItems" onClick="ShowCreditAllItems()" id="CreditAllItems" type="checkbox" value="yes">
		  &nbsp;&nbsp;&nbsp;Reason for credit:
		  
		  <select size="1" name="ReasonSearch" id="ReasonSearch" onChange="ShowCreditAllItemsReasonSearch()" style="font-family: Verdana; font-size: 8pt">
              <option value="">Select</option>
              <option value="Quality">Quality</option>
              <option value="Shortage">Shortage</option>
			  <option value="Damaged">Damaged</option>
			  <option value="Late">Late</option>
			  <option value="Misdelivery">Misdelivery</option>
			  <option value="Price Discrepancy">Price Discrepancy</option>
			  <option value="Goodwill">Goodwill</option>
			  <option value="Cancelled">Cancelled</option>
              <option value="Other">Other</option>
            </select>
		  &nbsp;&nbsp;&nbsp;Responsibility for Complaint:
		  <select size="1" name="ComplaintResponsibilitySearch" id="ComplaintResponsibilitySearch" onChange="ShowCreditAllItemsComplaintResponsibilitySearch()" style="font-family: Verdana; font-size: 8pt; width:200px">
			<option value="">Select</option>
			<%
			if IsArray(vRecArray4) Then  	
				for j = 0 to ubound(vRecArray4,2)
			%>  				
			<option value="<%=vRecArray4(0,j)%>"><%=vRecArray4(1,j)%></option>
			 <%
				next                 
			End if
			%>
		</select>
		  </td>
        </tr>
		<tr>
			<td height="2"></td>
		</tr>
      </table>


      <table border="0" width="1000" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" bgcolor="#666666">
        <tr>
          <td bgcolor="#CCCCCC" width="10"></td>
		  <td bgcolor="#CCCCCC" width="100"><nobr><b>Product Code</b></nobr></td>
          <td bgcolor="#CCCCCC" width="350"><b>Product Name</b></td>
          <td bgcolor="#CCCCCC" width="120"><b>Qty Ordered</b></td>
          <td bgcolor="#CCCCCC" width="100"><b> Qty&nbsp; credited</b></td>
          <td bgcolor="#CCCCCC" width="100"><b> Qty to be credited</b></td>
          <td bgcolor="#CCCCCC" width="100"><b>Reason for credit</b></td>
		  <td bgcolor="#CCCCCC" width="100"><b>Responsibility for Complaint</b></td>
          <td bgcolor="#CCCCCC" width="120"><b>Comments</b></td>
        </tr>
        <input type="hidden" name="invoiceno" value="<%=vinvoiceno%>">
        <input type="hidden" name="total" value="<%=ubound(vRecArray2,2)%>">
		<input type="hidden" name="numberofproducts" id="numberofproducts" value="<%=ubound(vRecArray2,2)%>">
        <%
		j=0
		for i = 0 to ubound(vRecArray2,2)%>
        
        <%
        if not isnumeric(vRecArray2(3,i)) then
        	vQtyCredited = 0
		else
        	vQtyCredited = vRecArray2(3,i)
        end if
        %>
        <tr bgcolor="#FFFFFF">
		  <td width="10">
		  <%
		  strismultipleproduct=vRecArray2(4,i)
		  %>
		  <input type="hidden" name="ismultipleproduct<%=i%>" id="ismultipleproduct<%=i%>" value="<%=strismultipleproduct%>">
		  <%
		  if (strismultipleproduct=1) then
		  mutipno=vRecArray2(0,i)
		  set DisplayInvoiceDetail= object.ListofMultipleProductsForOrder(mutipno,vinvoiceno)
			vRecArrayMuti = DisplayInvoiceDetail("MultipleProductsForOrder")
		  %>
		  	<img src="images/plus.png" width="11" name="myImg<%=vRecArray2(0,i)%>" id="myImg<%=vRecArray2(0,i)%>" height="11" border="0" onClick="showsubproducts('<%=vRecArray2(0,i)%>')">
		  	<input type="hidden" name="plusminus" id="plusminus" value="">
		  <%
		  end if
		  %>
		  </td>
          <td width="100"><%=vRecArray2(0,i)%><input type="hidden" name="Pno<%=i%>" value="<%=vRecArray2(0,i)%>"></td>
          <td width="350"><%=vRecArray2(1,i)%></td>
          <td align="left" width="120"><%=vRecArray2(2,i)%><input type="hidden" name="TotalQty<%=i%>" id="TotalQty<%=i%>" value="<%=vRecArray2(2,i)%>"><input type="hidden" name="Qno<%=i%>" value="<%=vRecArray2(2,i)%>"></td>
          <td width="100"><%=vQtyCredited%><input type="hidden" name="QtyCredited<%=i%>" value="<%=vQtyCredited%>"></td>
          <td width="100">
          <input type="text" name="Qty<%=i%>" id="Qty<%=i%>" size="8" style="font-family: Verdana; font-size: 8pt"></td>
          <td width="100"><select size="1" name="Reason<%=i%>" id="Reason<%=i%>" style="font-family: Verdana; font-size: 8pt">
              <option value="">Select</option>
              <option value="Quality">Quality</option>
              <option value="Shortage">Shortage</option>
			  <option value="Damaged">Damaged</option>
			  <option value="Late">Late</option>
			  <option value="Misdelivery">Misdelivery</option>
			  <option value="Price Discrepancy">Price Discrepancy</option>
			  <option value="Goodwill">Goodwill</option>
			  <option value="Cancelled">Cancelled</option>
              <option value="Other">Other</option>
            </select>
			</td>
			<td width="120">
			
			<select size="1" name="ComplaintResponsibility<%=i%>" id="ComplaintResponsibility<%=i%>" style="font-family: Verdana; font-size: 8pt; width:100px">
			<option value="">Select</option>
			<%
			if IsArray(vRecArray4) Then  	
				for j = 0 to ubound(vRecArray4,2)
			%>  				
			<option value="<%=vRecArray4(0,j)%>"><%=vRecArray4(1,j)%></option>
			 <%
				next                 
			End if
			%>
		</select>
			</td>
          <td width="120"><textarea rows="2" name="Other<%=i%>" cols="20" style="font-family: Verdana; font-size: 8pt"></textarea></td>
        </tr>
        
        
        
        <%
		if (strismultipleproduct=1) then
		j=j+1
		if (isarray(vRecArrayMuti)) then
			multipleproductcountcount=ubound(vRecArrayMuti,2) + 1
		else
			multipleproductcountcount=0	
		end if		
		%>
		<tr id="tr<%=vRecArray2(0,i)%>" bgcolor="#FFFFFF" style="display:none;">
		<td colspan="9">
		<table border="0" width="1120" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="0" bgcolor="#666666">
		<input type="hidden" name="multipleproductcount<%=i%>" id="multipleproductcount<%=i%>" value="<%=multipleproductcountcount%>">
		<input type="hidden" name="numberofsubproducts<%=i%>" id="numberofsubproducts<%=i%>" value="<%=multipleproductcountcount%>">
		<%
		for k = 0 to ubound(vRecArrayMuti,2)		
			vQtyCredited = 0
		%>
		<tr bgcolor="#FFFFFF">
			<td width="10"></td>
			<td width="110"><%=vRecArrayMuti(0,k)%><input type="hidden" name="Pno<%=i%><%=k%>" value="<%=vRecArrayMuti(0,k)%>"></td>
			<td width="310"><%=vRecArrayMuti(1,k)%></td>
			<td width="120"><%=vRecArrayMuti(2,k)%><input type="hidden" id="TotalQty<%=i%><%=k%>" name="TotalQty<%=i%><%=k%>" value="<%=vRecArrayMuti(2,k)%>"><input type="hidden" name="Qno<%=i%><%=k%>" value="<%=vRecArrayMuti(2,k)%>"></td>
		 	<td width="100"><%=vQtyCredited%><input type="hidden" name="QtyCredited<%=i%><%=k%>" value="<%=vQtyCredited%>"></td>
			<td width="100"><input type="text" name="Qty<%=i%><%=k%>" id="Qty<%=i%><%=k%>" size="8" style="font-family: Verdana; font-size: 8pt"></td>
			<td width="100">
			<select size="1" name="Reason<%=i%><%=k%>" style="font-family: Verdana; font-size: 8pt">
			  <option>Select</option>
			  <option>Quality</option>
              <option>Shortage</option>
			  <option>Damaged</option>
			  <option>Late</option>
			  <option>Misdelivery</option>
			  <option>Price Discrepancy</option>
			  <option>Goodwill</option>
			   <option>Cancelled</option>
              <option>Other</option>
			</select>
			</td>
			<td width="120">
			
			<select size="1" name="ComplaintResponsibility<%=i%><%=k%>" style="font-family: Verdana; font-size: 8pt; width:100px">
			<option value="">Select</option>
			<%
			if IsArray(vRecArray4) Then  	
				for j = 0 to ubound(vRecArray4,2)
			%>  				
			<option value="<%=vRecArray4(0,j)%>"><%=vRecArray4(1,j)%></option>
			 <%
				next                 
			End if
			%>
			</select>
			</td>
			<td width="120"><textarea rows="2" name="Other<%=i%><%=k%>" cols="20" style="font-family: Verdana; font-size: 8pt"></textarea></td>
		</tr>
		<%
		next
		%>
		</table>
		</td>
		</tr>
		<%
		end if
		next
		%>		
		<input type="hidden" name="numberofmultiproducts" id="numberofmultiproducts" value="<%=j%>">
        <tr bgcolor="#CCCCCC">
          <td width="133%" colspan="9">
            <table border="0" width="100%">
              <tr>
                
                <td width="33%" align="center"></td>
                      

           
                <td width="33%" align="center"><input type="button" onClick="Javascript:window.location.href='credit_new.asp';" value="Go Back" name="Go Back" style="font-family: Verdana; font-size: 8pt"></td>
          
                
                <td width="34%" align="center"><input type="submit" value="Confirm Credit" onClick="return CheckMultipleProduct()" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
               
              </tr>
            </table>
            
          </td>
        </tr>
      </table>
    </td>
  </tr>
  </form>
</table>
  </center>
</div>
</body>
</html>
<img src="images/minus.png" width="1" height="1" border="0">
<%
set object = nothing
%>