<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
deldate= request.form("deldate")
'deldate= request.QueryString("deldate")
if deldate= "" then response.redirect "LoginInternalPage.asp"

dim objBakery
dim recarray1, recarray2
dim ordno
dim retcol
Dim deldate
Dim intI
Dim intN
Dim intF
Dim intJ


dim tname, tno, curtno
Dim obj
Dim arCreditStatements
Dim arCreditStatementDetails
Dim vCreditDate

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; height:1px }
-->
</style>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
'PrintPgSize=42

' Internal Productions => 11-Stanmore English, 12-Stanmore French, 99-Stanmore Goods, 15-Park Royal , 18-BMG, 22-Cake Department  
' 16 - Flour Station - Added on 18th April 2008 by Selva
  set object = Server.CreateObject("bakery.daily")
  object.SetEnvironment(strconnection)
%>
<%  
  'Slicing Sheets Report - 11-Stanmore English, 12-Stanmore French, 15-Park Royal 
  intN = 0
  facility = "11,12,15,16"
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheetWithSlicing(deldate,facility,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3">Slicing Sheets Reports (<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2">
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
				</td>
			</tr>
		 <tr>	
			<td>
			Number of pages for this report: <%= intF %>
			</td>
			<td align="right">
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
		<table align="center" border="2" width="95%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
		  <td width="17%" height="20"><b>Facility</b></td> 	
          <!--<td width="12%" height="20"><b>Product code</b></td>-->
          <td width="50%" height="20"><b>Product name</b></td>
          <td width="7%" height="20"><b>Size</b></td>
		  <td width="17%" height="20"><b>Slice Type</b></td>
          <td width="15%" height="20"><b>Quantity</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
		<%
		if intJ <> 1 then
			strBeforeType=left(vRecArray(7,intN-1),6)
			strType=left(vRecArray(7,intN),6)
			if	strBeforeType<>strType then

'intJ=0
'intF=1
'intI=1
'intN=0
			%>
			
			<tr>
		  	<td colspan="5" height="20" align="right"><hr>
			<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
			<table align="center" border="0" width="100%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
			  <tr>
				<td width="100%">
				  <p align="center"><b><font size="3">Slicing Sheets Reports (<%=tname%>)</font></b><br>
				  <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
					<tr>
					  <td colspan="2">
						Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
						To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
							</td>
						</tr>
					
					</table>
				</td>
			  </tr>
			</table>
			</td>
			</tr>
			<%	
			end if
		end if
		%>
        <tr>
          <td width="17%" height="20"><%=vRecArray(6,intN)%></td>
		 <!-- <td width="12%" height="20"><%=vRecArray(0,intN)%></td> -->
          <td width="50%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="7%" height="20"><%=vRecArray(2,intN)%></td>
		  <td width="17%" height="20"><%=vRecArray(7,intN)%></td>
          <td width="15%" height="20"><%=vRecArray(3,intN) %></td>
		 </tr>
		 <%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td height="20" colspan="4" align="right"><b>Total&nbsp; </b></td>
          <td height="20"><b><%=intTotal%>&nbsp;</b></td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table>
<%if intI <> intF Then%>
	<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<%end if%>
<p>&nbsp;</p>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%  
  if deldate <> "" then
	'deldate = NextDayinDDMMYY(deldate)
	deldate = deldate
  end if
  'Slicing Sheets Report - 15-Park Royal 48 hrs
  intN = 0
  facility = "15"
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet48HoursWithSlicing(deldate,facility,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<%
end if%>
<%
if isarray(vRecArray) then
%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3">Park Royal 48 Hrs - Slicing Sheets Reports (<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2">
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			</td>
		 </tr>
		 <tr>	
			<td>
			Number of pages for this report: <%= intF %>
			</td>
			<td align="right">
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table>
<% 
    curpage=1
	for intI = 1 to intF
%>
		<table align="center" border="2" width="95%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
		  <td width="17%" height="20"><b>Facility</b></td> 	
          <!--<td width="12%" height="20"><b>Product code</b></td> -->
          <td width="50%" height="20"><b>Product name</b></td>
          <td width="7%" height="20"><b>Size</b></td>
		  <td width="17%" height="20"><b>Slice Type</b></td>
          <td width="15%" height="20"><b>Quantity</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
		<%
		if intJ <> 1 then
			strBeforeType=left(vRecArray(5,intN-1),6)
			strType=left(vRecArray(5,intN),6)
			if	strBeforeType<>strType then
			%>
			
			<tr>
		  	<td colspan="5" height="20" align="right"><hr>
			<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
			</td>
			</tr>
			<%	
			end if
		end if
		%>
        <tr>
          <td width="17%" height="20"><%=vRecArray(2,intN)%> 48 Hrs</td>
		  <!--<td width="12%" height="20"><%=vRecArray(0,intN)%></td> -->
          <td width="50%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="7%" height="20"><%=vRecArray(3,intN)%></td>
		  <td width="17%" height="20"><%=vRecArray(5,intN)%></td>
          <td width="15%" height="20"><%=vRecArray(4,intN) %></td>
		 </tr>
		 <%
				intTotal = intTotal + vRecArray(4,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td height="20" colspan="4" align="right"><b>Total&nbsp; </b></td>
          <td height="20"><b><%=intTotal%>&nbsp;</b></td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<%
		End if
	next
end if
%>

<p>&nbsp;</p>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%  
  if deldate <> "" then
	'deldate = NextDayinDDMMYY(deldate)
	deldate = deldate
  end if
  ' 16 - Flour Station 48 hrs - Added on 18th April 2008 by Selva 
  intN = 0
  facility = "16"
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet48HoursWithSlicing(deldate,facility,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<%
end if%>
<%
if isarray(vRecArray) then
%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3">Flour Station 48 Hrs - Slicing Sheets Reports (<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2">
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			</td>
		 </tr>
		 <tr>	
			<td>
			Number of pages for this report: <%= intF %>
			</td>
			<td align="right">
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table>
<% 
    curpage=1
	for intI = 1 to intF
%>
		<table align="center" border="2" width="95%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
		  <td width="17%" height="20"><b>Facility</b></td> 	
          <!--<td width="12%" height="20"><b>Product code</b></td> -->
          <td width="50%" height="20"><b>Product name</b></td>
          <td width="7%" height="20"><b>Size</b></td>
		  <td width="17%" height="20"><b>Slice Type</b></td>
          <td width="15%" height="20"><b>Quantity</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
		<%
		if intJ <> 1 then
			strBeforeType=left(vRecArray(5,intN-1),6)
			strType=left(vRecArray(5,intN),6)
			if	strBeforeType<>strType then
			%>
			
			<tr>
		  	<td colspan="5" height="20" align="right"><hr>
			<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
			</td>
			</tr>
			<%	
			end if
		end if
		%>
        <tr>
          <td width="17%" height="20"><%=vRecArray(2,intN)%> 48 Hrs</td>
		  <!--<td width="12%" height="20"><%=vRecArray(0,intN)%></td> -->
          <td width="50%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="7%" height="20"><%=vRecArray(3,intN)%></td>
		  <td width="17%" height="20"><%=vRecArray(5,intN)%></td>
          <td width="15%" height="20"><%=vRecArray(4,intN) %></td>
		 </tr>
		 <%
				intTotal = intTotal + vRecArray(4,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td height="20" colspan="4" align="right"><b>Total&nbsp; </b></td>
          <td height="20"><b><%=intTotal%>&nbsp;</b></td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<%
		End if
	next
end if
%>
<%end if%>
<%end if%>
<p>&nbsp;</p>


<%  
  'GAILs central Kitchen 
  intN = 0
  facility = "34"
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayGAILsCentralKitchenProduction(deldate,facility,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3">GAILs central Kitchen (<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2">
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
				</td>
			</tr>
		 <tr>	
			<td>
			Number of pages for this report: <%= intF %>
			</td>
			<td align="right">
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
		<table align="center" border="2" width="95%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
		  <td width="17%" height="20"><b>Facility</b></td> 	
          <!--<td width="12%" height="20"><b>Product code</b></td>-->
          <td width="50%" height="20"><b>Product name</b></td>
          <td width="7%" height="20"><b>Size</b></td>
		  <td width="17%" height="20"><b>Slice Type</b></td>
          <td width="15%" height="20"><b>Quantity</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
		<%
		if intJ <> 1 then
			strBeforeType=left(vRecArray(7,intN-1),6)
			strType=left(vRecArray(7,intN),6)
			if	strBeforeType<>strType then

			%>
			
			<tr>
		  	<td colspan="5" height="20" align="right"><hr>
			<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
			<table align="center" border="0" width="100%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
			  <tr>
				<td width="100%">
				  <p align="center"><b><font size="3">GAILs central Kitchen (<%=tname%>)</font></b><br>
				  <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
					<tr>
					  <td colspan="2">
						Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
						To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
							</td>
						</tr>
					
					</table>
				</td>
			  </tr>
			</table>
			</td>
			</tr>
			<%	
			end if
		end if
		%>
        <tr>
          <td width="17%" height="20"><%=vRecArray(6,intN)%></td>
		 <!-- <td width="12%" height="20"><%=vRecArray(0,intN)%></td> -->
          <td width="50%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="7%" height="20"><%=vRecArray(2,intN)%></td>
		  <td width="17%" height="20"><%=vRecArray(7,intN)%></td>
          <td width="15%" height="20"><%=vRecArray(3,intN) %></td>
		 </tr>
		 <%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td height="20" colspan="4" align="right"><b>Total&nbsp; </b></td>
          <td height="20"><b><%=intTotal%>&nbsp;</b></td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table>
<%if intI <> intF Then%>
	<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
End IF
%>
</body>
</html>