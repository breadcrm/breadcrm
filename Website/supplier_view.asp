<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->

<%
if request.form("sno")<>"" then
	vSno= request.form("sno")
else
	vSno = request.querystring("sno")
end if
if vSno= "" then response.redirect "supplier_find.asp"

set object = Server.CreateObject("bakery.Supplier")
object.SetEnvironment(strconnection)
if request.form("delete")="yes" then
	vSno=request.form("sno")
	vIno=request.form("ino") 
	delete = object.DeleteSupplierIng(vSno,vIno)
end if
set DisplaySupplierDetail= object.DisplaySupplierDetail(vSno)
vRecArray = DisplaySupplierDetail("SupplierDetail")
vRecIngArray = DisplaySupplierDetail("SupplierIngDetail")
set DisplaySupplierDetail= nothing
set object = nothing

if not (isarray(vRecArray)) then response.redirect "supplier_find.asp"
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>

<script Language="JavaScript">
<!--
function validate(frm){
	if(confirm("Would you like to delete " + frm.iname.value + " ?")){
		frm.submit();		
	}
}
//-->
</Script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">View supplier - <%=vSno%><br>
      &nbsp;</font></b>
      
      
      <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td width="50%" valign="top"><table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="100%">
        <tr>
          <td width="25%"><b>Name</b></td>
          <td width="75%"><b><%=vrecarray(1,0)%></b></td>
        </tr>
        <tr>
          <td width="25%">Address</td>
          <td width="75%"><%=vrecarray(2,0)%></td>
        </tr>
        <tr>
          <td width="25%"></td>
          <td width="75%"><%=vrecarray(3,0)%></td>
        </tr>
        <tr>
          <td width="25%">Town</td>
          <td width="75%"><%=vrecarray(4,0)%></td>
        </tr>
        <tr>
          <td width="25%">Postcode</td>
          <td width="75%"><%=vrecarray(5,0)%></td>
        </tr>
        <tr>
          <td width="25%">Telephone</td>
          <td width="75%"><%=vrecarray(6,0)%></td>
        </tr>
        <tr>
          <td width="25%">Fax</td>
          <td width="75%"><%=vrecarray(7,0)%></td>
        </tr>
        <tr>
          <td width="25%">Email</td>
          <td width="75%"><%=vrecarray(8,0)%></td>
        </tr>
        <tr>
          <td width="25%">Website</td>
          <td width="75%"><%=vrecarray(9,0)%></td>
        </tr>
        <tr>
          <td width="25%">Payment term</td>
          <td width="75%"><%=vrecarray(10,0)%></td>
        </tr>
        <tr>
          <td colspan="2" width="25%"><b> Contacts</b></td>
        </tr>
        <tr>
          <td width="25%">Name</td>
          <td width="75%"><%=vrecarray(11,0)%></td>
        </tr>
        <tr>
          <td width="25%">Job title</td>
          <td width="75%"><%=vrecarray(12,0)%></td>
        </tr>
        <tr>
          <td width="25%">&nbsp;</td>
          <td width="75%"></td>
        </tr>
      </table></td>
        </tr>
      </table>

    </td>
  </tr>
</table>


  </center>
</div>


<br>


<div align="center">
  <center>

      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="6"><b><font size="2">Ingredients/Products</font></b></td>
        </tr>
        <tr>
          <td bgcolor="#CCCCCC"><b>Ing. Category</b></td>
          <td bgcolor="#CCCCCC"><b>Ing. Code</b></td>


          <td bgcolor="#CCCCCC"><b>Ing. Name</b></td>


          <td bgcolor="#CCCCCC"><b>Delivery Frequency</b></td>


          <td bgcolor="#CCCCCC"><b>Unit Qty</b></td>

          <td bgcolor="#CCCCCC"><b>Unit Measure</b></td>

          <td bgcolor="#CCCCCC"><b>Unit Price</b></td>


          <td bgcolor="#CCCCCC"><b>Facility</b></td>
		 <%if session("UserType") <> "N" Then%>
          <td align = "center" colspan = "2" bgcolor="#CCCCCC"><b>Action</b></td>
		  <%end if%>
        </tr>
<% 
	if IsArray(vRecIngArray) Then
		For i = 0 To ubound(vRecIngArray,2)
%>         
        
        
        <tr>
          <td><%=vRecIngArray(0,i)%></td>
          <td><%=vRecIngArray(1,i)%></td>
          <td><%=vRecIngArray(2,i)%></td>
          <td><%=vRecIngArray(7,i)%></td>
          <td><%=vRecIngArray(4,i)%></td>
          <td><%=vRecIngArray(8,i)%></td>
          <td>�<%=vRecIngArray(5,i)%><b></b></td>
          <td><%=vRecIngArray(6,i)%></td>
           <%if session("UserType") <> "N" Then%>
		  <form>
          <td  align = "center" >
          <input onClick="NewWindow('supplier_products.asp?sno=<%=Server.URLEncode(vrecarray(0,0))%>&name=<%=Server.URLEncode(vrecarray(1,0))%>&ino=<%=Server.URLEncode(vRecIngArray(1,i))%>&iname=<%=Server.URLEncode(vRecIngArray(2,i))%>&qty=<%=Server.URLEncode(vRecIngArray(4,i))%>&price=<%=Server.URLEncode(vRecIngArray(5,i))%>&often=<%=Server.URLEncode(vRecIngArray(7,i))%>&FaciName=<%=Server.URLEncode(vRecIngArray(6,i))%>','profile','700','300','yes','center');return false" onFocus="this.blur()" type="button" value="Edit" name="B1" style="font-family: Verdana; font-size: 8pt">
          </td>
          </form>
		  
          <form method="POST">
          
		  <td  align = "center" >
          <input type = "hidden" name="sno" value = "<%=vrecarray(0,0)%>">
          <input type = "hidden" name="name" value = "<%=vrecarray(1,0)%>">
          <input type = "hidden" name="ino" value = "<%=vRecIngArray(1,i)%>">
          <input type = "hidden" name="iname" value = "<%=vRecIngArray(2,i)%>">
          <input type = "hidden" name="delete" value = "yes">
          <input type="button" onClick="validate(this.form);" value="Delete" name="B1" style="font-family: Verdana; font-size: 8pt">
          </td>
          </form>
		  <%end if%>
        </tr>
<%
		Next
	Else
%>

        <tr>
          <td>Sorry no items found</td>
        </tr>
<%
	End if
%>        
        
        </table>
  </center>
</div>
<%if session("UserType") = "N" Then%>
	<p align="center"><input type="button" value=" Back " onClick="history.back()"></p>
<%end if%>
<br>

</body>
</html>