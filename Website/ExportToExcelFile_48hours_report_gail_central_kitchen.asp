<%@ Language=VBScript %>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->

<%
dim pCode
pCode = ""
dim deldate, dtype, facility
deldate = Request.Form("ex_deldate")
selecteddate = Request.Form("ex_selecteddate")
dtype = Request.Form("ex_dtype")
displaydeldate = request.form("ex_deldate")
'strdeldate = NextDayinDDMMYY(displaydeldate)
strdeldate = displaydeldate
'This is haed coded but need to replace the id for new facility.
facility = 34

dim intI
Dim intN
Dim intRow
Dim intF
Dim intTotal
intN = 0
intTotal = 0

if deldate= "" or facility = "" or dtype = "" then
	response.write("There is a problem occured. Please try it again.")
	response.End()
end if

set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
'set col1= object.DailySourDoughOrderSheetViewReport(deldate,facility,dtype)
set col1= object.DailySourDoughOrderSheetViewReportForGail(deldate,facility,dtype)
vRecArray = col1("SourDoughOrdersheet")

set col11 = object.DailySourDoughGailFullOrderSheetViewReport(deldate,facility,dtype)
vRecArrayNew = col11("SourDoughOrdersheet")
dim arrCName
dim arrPName
set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set objCName = objBakery.GetFilterData(vRecArrayNew,5,arrCName)
arrCName = objCName("FilterData")
set objPName = objBakery.GetFilterData(vRecArrayNew,1,arrPName)
arrPName = objPName("FilterData")
set objBakery = nothing


set maxColCount= object.DailySourDoughGailOrderSheetViewReportMaxColumn(deldate,facility,dtype)
vMaxColCountArray = maxColCount("MaxColumnCount")

MaxColumnCount = 0
if isarray(vMaxColCountArray) then
	MaxColumnCount = vMaxColCountArray(0,0) - 1
end if

Facilityrec = col1("Facility")

if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if

set col1= nothing
set object = nothing

intF = 1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round((ubound(vRecArray, 2)/PrintPgSize) + 0.49)
	End If
End If

dim strFileName, strPath
strFileName = "48hours_report_gails_central_kitchen.xls"
strPath = "Reports\Excel\" & strFileName

dim vDeliveryText
vDeliveryText = ""
if dtype="Morning" then
	vDeliveryText = "before 05:00 AM"
end if

%>
<html>
<head>
<title>48 Hours Report of GAIL's central Kitchen</title>
<SCRIPT language=JavaScript>
function clearPreloadPage() 
{ 
  if (document.getElementById){
    document.getElementById('prepage').style.visibility='hidden';
  }
  else{
  if (document.layers){ //NS
    document.prepage.visibility = 'hidden';
  }
  else { //IE
    document.all.prepage.style.visibility = 'hidden';
  }
  }
} //end function
</SCRIPT>
</head>
<body onLoad="clearPreloadPage()" topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">

<DIV id=prepage style="FONT-SIZE: 11px; FONT-FAMILY: arial; TOP: 10px; HEIGHT: 20px; TEXT-ALIGN: center">
<TABLE width="100%" align="center">
	<TR>
		<TD align="center" c>
			<!--Loading ....-->
			<img src="images/LoadingAnimation.gif" width="201" height="33"><br>
			<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><b>Please wait...</b></font>
		</TD>
	</TR>
</TABLE>
</DIV>

<%
DIM fso, act, a
set fso = createobject("scripting.filesystemobject")
Set act = fso.CreateTextFile(server.mappath(strPath), true)

act.WriteLine("<html><body topmargin=""0"" leftmargin=""0"" bgcolor=""#FFFFFF"" text=""#000000"" style=""font-family: Verdana; font-size: 8pt"">")
act.WriteLine("<p>&nbsp;</p>")

act.WriteLine("<table align=""center"" border=""0"" width=""90%"" cellspacing=""0"" cellpadding=""0"" style=""font-family: Verdana; font-size: 8pt"">")
act.WriteLine("<tr>")
act.WriteLine("<td width=""100%"">")
act.WriteLine("<p align=""center""><b><font size=""3"">48 Hours Report of GAIL's central Kitchen<br>&nbsp;</font></b>")
act.WriteLine("<table border=""0"" width=""85%"" style=""font-family: Verdana; font-size: 8pt"" cellspacing=""0"" cellpadding=""2"">")
act.WriteLine("<tr>")
act.WriteLine("<td>")
act.WriteLine("<b>Facility: </b>" & Facilityname & "<br>")
act.WriteLine("<b>To the Attention of: </b>" & attn & "<br>")
act.WriteLine("<b>By Fax Number: </b>" & fax & "<br><br>")
act.WriteLine("Date of report: " & day(now()) & "/" & month(now()) & "/" & year(now()) & "<br>")
'act.WriteLine("Date of report: " & selecteddate & "<br>")
act.WriteLine("To be produced and ready to be delivered on " & strdeldate & " " & vDeliveryText & "<br><br>")
act.WriteLine("Number of pages for this report: " & intF & "<br><br>")
act.WriteLine("</td>")
act.WriteLine("</tr>")
act.WriteLine("</table>")
act.WriteLine("</td>")
act.WriteLine("</tr>")
act.WriteLine("</table>")

set object1 = Server.CreateObject("bakery.daily")
object1.SetEnvironment(strconnection)
if isarray(vRecArray) then

act.WriteLine("<table align=""center"" border=""2"" width=""854"" cellspacing=""0"" cellpadding=""0"" style=""font-family: Verdana; font-size: 8pt; border-collapse:collapse"" bordercolor=""#000000"">")
act.WriteLine("<tr>")
act.WriteLine("<td width=""99"" height=""20""><b>Product code</b></td>")
act.WriteLine("<td width=""401"" height=""20""><b>Product</b></td>")
'act.WriteLine("<td width=""77"" height=""20""><b>Size</b></td>")
act.WriteLine("<td width=""70"" height=""20""><b>Total Production</b></td>")
for m=0 to UBound(arrCName)
	act.WriteLine("<td><b>" & arrCName(m) & "</b></td>")
next
act.WriteLine("</tr>")

For intJ = 1 To ubound(vRecArray, 2) + 1
	pCode = vRecArray(0,intN)
	set col1Customers= object1.DailySourDoughGailOrderSheetViewReport(pCode,deldate)
	vCustomersArray = col1Customers("SourDoughOrdersheet")
	
	act.WriteLine("<tr>")
	act.WriteLine("<td width=""99"" height=""20"">" & vRecArray(0,intN) & "</td>")
	act.WriteLine("<td width=""451"" height=""20"">" & vRecArray(1,intN) & "</td>")
	'act.WriteLine("<td width=""77"" height=""20"">" & vRecArray(2,intN) & "</td>")
	act.WriteLine("<td width=""70"" height=""20"">" & vRecArray(3,intN) & "</td>")
	
	'if isarray(vCustomersArray) then
	'	For intC = 0 to MaxColumnCount
	'		If intC <= ubound(vCustomersArray, 2) Then
	'			act.WriteLine("<td width=""90"" height=""20"">" & vCustomersArray(0,intC)& " (" & vCustomersArray(1,intC) & ")</td>")
	'		Else
	'			act.WriteLine("<td width=""90"" height=""20""></td>")
	'		End If
	'	Next
	'end if
	
	dim vCustomerTotal
	For intC = 0 to UBound(arrCName)
		vCustomerTotal = "0"
		For intC1 = 0 to ubound(vCustomersArray, 2)
			if arrCName(intC) = vCustomersArray(0,intC1) then
				vCustomerTotal = vCustomersArray(1,intC1)
			end if
		Next
		act.WriteLine("<td width=""90"" height=""20"">" & vCustomerTotal & "</td>")
	Next
	'set object1 = nothing
	act.WriteLine("</tr>")
	
	intTotal = intTotal + vRecArray(3,intN)
	If intN = ubound(vRecArray, 2) Then
		act.WriteLine("<tr>")
		act.WriteLine("<td width=""401"" colspan=""2"" height=""20"" align=""right""><b>Total&nbsp; </b></td>")
		act.WriteLine("<td width=""70"" height=""20"" align=""right""><b>" & intTotal & "&nbsp;</b></td>")
		act.WriteLine("</tr>")
	End If
	intN = intN + 1
Next

act.WriteLine("</table>")
End if

act.WriteLine("</body></html>")
act.Close

'if fso.FileExists(server.mappath(strPath)) Then
'	Response.Write "Excel report has been created."
'end if

set act = nothing
set fso = nothing
'OK, Excel report generated and now it is the time to send report via mail.
'set col1EmailResult= object1.EmailGail48HourReport(0)
'vEmailResult = col1EmailResult("EmailResult")


set col1EmailResult= object1.EmailGail48HourReport2
arrEmailResult = col1EmailResult("EmailResult")
if IsArray(arrEmailResult) Then
	IsEmailSent = arrEmailResult(0,0)
else
	IsEmailSent = "0"
end if

set object1 = nothing

%>

<br><br>
<h3 align="center">48 Hours Report of GAIL's central Kitchen</h3>
<br>
<%
If IsEmailSent = "1" then
%>
<h4 align="center"><font color="#006633">Successfully Sent.</font></h4> 
<%
Else
%>
<h4 align="center"><font color="#006633">Problem occured while sent the report. Please try again.</font></h4> 
<%
End If
%>
<p align="center"><input type="button" value="Close" onClick="window.close()"></p>
<SCRIPT>
    if (document.getElementById) {
          prepage.style.display="none";
    } 
</SCRIPT>

<SCRIPT>
    if (document.getElementById) {
          prepage.style.display="none";
    } 
</SCRIPT>
</body>
</html>