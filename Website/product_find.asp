<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script LANGUAGE="javascript" src="includes/script.js"></script>

<script Language="JavaScript">
<!--
function validate(frm){
	if(confirm("Would you like to delete " + frm.name.value + "?")){
		frm.submit();
	}
}

function validateActive(frm){
	if(confirm("Would you like to activate " + frm.name.value + "?")){
		frm.submit();
	}
}


//-->
</Script>

<SCRIPT LANGUAGE="Javascript">
<!--

function SearchValidation(){
		//if((CheckEmpty(document.frmForm.pname.value) == "true") && (CheckEmpty(document.frmForm.pcode.value) == "true")){ 
			//alert("Please enter a value");
		//	return false;
		//}
		return true;
}
//-->
</SCRIPT>
</head>

<%

Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayDoughType()	
vdoughtypearray =  detail("DoughType") 
set detail = Nothing
set object = Nothing

vPageSize = 20

vpname = trim(replace(Request.Form("pname"),"'","''"))
vpcode = trim(replace(Request.Form("pcode"),"'","''"))
strIsMultipleProduct = Request.Form("IsMultipleProduct")
vdno =  trim(Request.Form("dno"))
vpagecount = Request.Form("pagecount") 

select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(vpagecount) then	
			session("Currentpage")  = session("Currentpage")+1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage")-1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select
vSessionpage = session("Currentpage")
'vsessionpage = 0
if NOT ISNUMERIC(vdno) then
	vdno= 0
end if

Set obj = server.CreateObject("bakery.product")
obj.SetEnvironment(strconnection)
if request.form("delete")="yes" then
	vPno=request.form("pno")
	strDeleteVal = obj.DeleteProduct(vPno)
	strdel = obj.DeleteSubProductsForMutiMultiProduct(vPno)
%>
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%
	'Log
	LogAction "Product Deleted", "Code: " & vPno , ""
end if

if request.form("delete")="no" then
	vPno=request.form("pno")
	strDeleteVal = obj.ActivateProduct(vPno)
%>
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%
'Log
LogAction "Product Activated", "Code: " & vPno , ""
end if

strStatus=request("Status")
if strStatus="A" then
	strStatusLabel="Active Products"
elseif  strStatus="D" then
	strStatusLabel="Deleted Products"
else
	strStatusLabel=""
end if
if strStatus="" then
	strStatus="A"
end if
if (strIsMultipleProduct="") then
	strIsMultipleProduct=2
end if 
Set objUser = obj.DisplayProduct(vpname,vpcode, vdno, vsessionpage , vPageSize, strStatus,strIsMultipleProduct)

arProducts =  objUser("Products")
vpagecount =   objUser("Pagecount") 
vRecordCount = objUser("Recordcount")

Set objUser = Nothing
Set obj = Nothing
%>


<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
<% if request.form("delete")="yes" And strDeleteVal = "Ok" then %>
<b><font size="2" color="#FF0000">Product deleted successfully</font></b><br><br>
<% end if %>
    <b><font size="3">Find product<br>
      &nbsp;</font></b>
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" height="42">

		<form method="post" action="product_find.asp" name="frmForm"> 
		
        <tr>
          <td height="13"><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
           <td height="13"><b>Is Multiple Product:&nbsp;&nbsp;&nbsp;</b></td>
		  <td height="13"><b>Status:</b></td>
		  <td height="13"><b>Product Name:</b></td>
          <td height="13"><b>Product Code:</b></td>
          <td height="13"><b>Dough Name</b></td>
          <td height="13"></td>
        </tr>
        <tr>
          <td height="17"></td>
		   <td>
		  <select name="IsMultipleProduct" onChange="this.form.submit()">
		    <option value="2" <% if strIsMultipleProduct="2" then%> selected="selected" <%end if%>>All</option>
			<option value="1" <% if strIsMultipleProduct="1" then%> selected="selected" <%end if%>>Yes</option>			
			<option value="0" <% if strIsMultipleProduct="0" then%> selected="selected" <%end if%>>No</option>		   
		  </select>
		  </td>
          <td>
		  <select name="Status" onChange="this.form.submit()">
		    <option value="A" <% if strStatus="A" then%> selected="selected" <%end if%>>Active</option>
			<option value="D" <% if strStatus="D" then%> selected="selected" <%end if%>>Deleted</option>
			<option value="0" <% if strStatus="0" then%> selected="selected" <%end if%>>All</option>
		  </select>
		  </td>
		  <td height="17">
          <input type="text" name="pname" value="<%=vpname%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td height="17">
          <input type="text" name="pcode" value="<%=vpcode%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td height="17">
          <select size="1" name="dno" style="font-family: Verdana; font-size: 8pt">
          	<option value = "">ALL</option><%
          	if IsArray(vdoughtypearray) Then
							for i = 0 to ubound(vdoughtypearray,2)%>								
								<option value="<%=vdoughtypearray(0,i)%>" <%if trim(vdoughtypearray(0,i)) = vDno then%> selected<%end if%>><%=vdoughtypearray(1,i)%></option><%
              next 
						end if%>         
                  
            </select></td>
          <td height="17">
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt" onClick="return SearchValidation();"></td>
        </tr>
		</form>        
      </table>
	  <br>
      <table border="0" bgcolor="#999999" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">

   			<%if IsArray(arProducts) Then%>
				<tr bgcolor="#FFFFFF" height="25">
	 			  <td align = "left" colspan="3"><b>Total no of products : <%=vRecordcount%></b></td>
				  <td align="right" colspan="3"><b>Page <%=vSessionpage%> of <%=vpagecount%></b></td>          
				</tr>
			<%End if%>  
        <tr height="22">
          <td width="15%" bgcolor="#CCCCCC"><b>Product Code</b></td>
          <td width="30%" bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td width="20%" bgcolor="#CCCCCC"><b>Dough Name</b></td>
          <td colspan="3" bgcolor="#CCCCCC" align="center"><b>Action</b></td>
        </tr>
<% 	
	if IsArray(arProducts) Then
		For i = 0 To ubound(arProducts,2)
			if (i mod 2 =0) then
				strBgColour="#FFFFFF"
			else
				strBgColour="#F3F3F3"
			end if
%>
        <tr bgcolor="<%=strBgColour%>">
          <td width="15%"><%=arProducts(0,i)%></td>
		  <%
		  		dim pName
				pName = Server.HTMLEncode(arProducts(2,i))
		  %>
          <td width="30%"><%=pName%></td>

          <td width="20%"><%=arProducts(23,i)%></td>
          <td colspan="3" align="center">
          <table width="100%" cellpadding="2" border="0" cellspacing="0" align="center">
		  <tr>
          <form method="POST" action="product_view.asp">
          <input type = "hidden" name = "pno" value="<%=arProducts(0,i)%>">
		  <input type = "hidden" name = "Status" value="<%=strStatus%>">
          <td align="center"><input type="submit" value="View Product" name="B1" style="font-family: Verdana; font-size: 8pt; width:100px"></td>
          </form>

          <form method="POST" action="product_new.asp">
          <input type = "hidden" name = "pno" value="<%=arProducts(0,i)%>">
		   <input type = "hidden" name = "Status" value="<%=strStatus%>">
          
          <td align="center"><%
						if session("UserType") <> "U" and session("UserType") <> "N" and strconnection<>"vdbArchiveConn" Then%>
							<input type="submit" value=" Edit " name="B1" style="font-family: Verdana; font-size: 8pt; width:100px"><%
						End if%>	
					</td>
          </form>

          <form method="POST">
          <input type = "hidden" name = "pno" value="<%=arProducts(0,i)%>">
		  <input type = "hidden" name = "Status" value="<%=strStatus%>">
          <input type = "hidden" name = "name" value="<%=pName%>">
          <td align="center"><%
          if session("UserType") <> "U" and session("UserType") <> "N" and strconnection<>"vdbArchiveConn" Then
		  if (arProducts(17,i)="A") then
		  %>
				
				<input type = "hidden" name="delete" value = "yes"><input type="button"  onclick="validate(this.form);" value="To Delete" name="B1" style="font-family: Verdana; font-size: 8pt; width:100px">
		  
		  <%
		  else
		  %>
		  	
				<input type = "hidden" name="delete" value = "no"><input type="button"  onclick="validateActive(this.form);" value="To Activate" name="B1" style="font-family: Verdana; font-size: 8pt">
		  
		  <%
		  end if
		  End if
		  %>	
					</td>
          </form>          
          </tr>
		</table>
		</td>
        </tr>
<%
		Next
	Else
%>

        <tr>
          <td colspan="6" height="40" bgcolor="#FFFFFF" align="center"><font size="2" color="#FF0000"><strong>Sorry no products found.</strong></font></td>
        </tr>
<%
	End if
%>
        </table>
    </td>
  </tr>
</table>


  </center>
</div>

<table width="720" border="0" cellspacing="1" cellpadding="2" align="center">
  <tr> 
    <td width="180">
	<% if vSessionpage <> 1 and vsessionpage <> 0 then %>
		<form action="product_find.asp" method="post" name="frmFirst">
	        <input type="submit" name="Submit2" value="First Page">
			 <input type = "hidden" name = "Status" value="<%=strStatus%>">
			<input type="hidden" name="Direction" value="First">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">
			<input type="hidden" name="pname" value="<%=vpname%>">
			<input type="hidden" name="pcode" value="<%=vpcode%>">
			<input type="hidden" name="dno" value="<%=vdno%>">
			<input type="hidden" name="IsMultipleProduct" value="<%=strIsMultipleProduct%>">
		</form>
	<% End if %>
    </td>
    <td width="180">
	<% if clng(vSessionpage) > 1 then %>
		<form action="product_find.asp" method="post" name="frmPrevious">
	        <input type="submit" name="Submit5" value="Previous Page">
			<input type="hidden" name="Direction" value="Previous">
			 <input type = "hidden" name = "Status" value="<%=strStatus%>">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">			
			<input type="hidden" name="pname" value="<%=vpname%>">
			<input type="hidden" name="pcode" value="<%=vpcode%>">
			<input type="hidden" name="dno" value="<%=vdno%>">
			<input type="hidden" name="IsMultipleProduct" value="<%=strIsMultipleProduct%>">
		</form>
	<% End if %>
    </td>
    <td width="180">
	<% if clng(vSessionpage) < vPagecount then %>
		<form action="product_find.asp" method="post" name="frmNext">
	        <input type="submit" name="Submit4" value="Next Page">
			<input type="hidden" name="Direction" value="Next">
			 <input type = "hidden" name = "Status" value="<%=strStatus%>">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">			
			<input type="hidden" name="pname" value="<%=vpname%>">
			<input type="hidden" name="pcode" value="<%=vpcode%>">
			<input type="hidden" name="dno" value="<%=vdno%>">
			<input type="hidden" name="IsMultipleProduct" value="<%=strIsMultipleProduct%>">
		</form>
	<% End if %>
    </td>
    <td width="180">
	<% if clng(vSessionpage) <> vPagecount and vPagecount > 0  then %>
		<form action="product_find.asp" method="post" name="frmLast">
	        <input type="submit" name="Submit3" value="Last Page">
			<input type="hidden" name="Direction" value="Last">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">	
			 <input type = "hidden" name = "Status" value="<%=strStatus%>">		
			<input type="hidden" name="pname" value="<%=vpname%>">
			<input type="hidden" name="pcode" value="<%=vpcode%>">
			<input type="hidden" name="dno" value="<%=vdno%>">
			<input type="hidden" name="IsMultipleProduct" value="<%=strIsMultipleProduct%>">
		</form>
	<% End if %>
    </td>
  </tr>
</table>
<p><font face="Verdana, Arial, Helvetica, sans-serif" size="2"></font></p>
</body>
</html>
<%
If IsArray(arProducts) Then Erase arProducts
%>