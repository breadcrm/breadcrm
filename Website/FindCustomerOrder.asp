<%@ Language=VBScript%>
<%
Response.Buffer = true
server.ScriptTimeout=6000
session.lcid=2057
%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function validate(frm){
	if(confirm("Would you like to delete " + frm.name.value + " ?")){
		frm.submit();		
	}
}

function ExportToExcelFile(){
	vcno=document.form.txtcno.value
	vGroup=document.form.txtGroup.value
	vfrom=document.form.txtfrom.value
	vto=document.form.txtto.value
	vcname=encodeURIComponent(document.form.txtcname.value)
	vordstatus=document.form.ordstatus.value
	vpcode=document.form.txtpcode.value
	vselvno=document.form.selvno.value
	vnewsearchoption=document.form.newsearchoption.value
	document.location="ExportToExcelFileFindCustomerOrder.asp?txtcno=" + vcno + "&txtcname=" + vcname + "&ordstatus=" + vordstatus + "&txtGroup=" + vGroup + "&txtfrom=" + vfrom + "&txtto=" + vto + "&txtpcode=" + vpcode + "&selvno=" + vselvno  + "&newsearchoption=" + vnewsearchoption; 
}

function StopOrder(frm,dtdate){
	if(confirm("Are you sure you want to stop the order for this account for " + dtdate + "?")){
		frm.submit();		
	}
}

function ActivateOrder(frm,dtdate){
	if(confirm("Are you sure you want to activate the order for this account for " + dtdate + "?")){
		frm.submit();		
	}
}

//-->
</Script>
</head>
<%

dim strsessionusertype
strsessionusertype=session("UserType")

Dim Obj,ObjCustomer
Dim arCustomer
Dim arGroup

Dim cname,strCustNo,strGroup, strCustomerCode,intday, strordstatus
Dim strDirection
Dim vpagecount
Dim vCurrentPage
Dim i,StrOrderType, blnIsFindCustomerOrder, j
Dim vParaCustNo
dim vCno
dim delete
dim vRecordCount
Dim strProdCode

strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vCurrentPage	= Request.Form("CurrentPage") 


cno = replace(Request.Form("txtcno"),"'","''")
cname = replace(Request.Form("txtcname"),"'","''")
fromdt = replace(Request.Form("txtfrom"),"'","''")
todt = replace(Request.Form("txtto"),"'","''")
strGroup = Replace(Request.Form("txtGroup"),"'","''")
strordstatus = replace(Request.Form("ordstatus"),"'","''")
strpcode=replace(Request.Form("txtpcode"),"'","''")
strvno=Request.Form("selvno")

strnewsearchoption=Request.Form("newsearchoption")
if (strnewsearchoption="") then
	strnewsearchoption="1"
end if

if fromdt ="" then
  'fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  fromdt=Day(date()+1) & "/" & Month(date()+1) & "/" & Year(date()+1)
end if
'todt=Day(date()) & "/" & Month(date()) & "/" & Year(date()) 
if (todt="") then
	todt=displayBristishDate(DATEADD("d",6,date()))
end if

If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if

Set Obj = CreateObject("Bakery.Customer")
Obj.SetEnvironment(strconnection)

if request.form("stop")="yes" then
	vdtdate=request.form("dtdate")
	vCustNo=request.form("CustNo")
	if vdtdate <> "" Then
		arSplit = Split(vdtdate,"/")
		vdtdatenew = arSplit(2) & "-" & arSplit(1) & "-" & arSplit(0)
	End if
	strIsStopStatus="Y"
	delete = Obj.StopActiveCustomerOrder(strIsStopStatus,vCustNo,vdtdatenew)
	
	LogAction "Order Stopped", "Date of Order: " & vdtdate , vCustNo
elseif request.form("stop")="no" then
	vdtdate=request.form("dtdate")
	vCustNo=request.form("CustNo")
	if vdtdate <> "" Then
		arSplit = Split(vdtdate,"/")
		vdtdatenew = arSplit(2) & "-" & arSplit(1) & "-" & arSplit(0)
	End if
	strIsStopStatus="N"
	delete = Obj.StopActiveCustomerOrder(strIsStopStatus,vCustNo,vdtdatenew)
	
	LogAction "Order Activated", "Date of Order: " & vdtdate , vCustNo
end if

if fromdt <> "" Then
	arSplit = Split(fromdt,"/")
	fromdtnew = arSplit(2) & "-" & arSplit(1) & "-" & arSplit(0)
End if

if todt <> "" Then
	arSplit = Split(todt,"/")
	todtnew = arSplit(2) & "-" & arSplit(1) & "-" & arSplit(0)
End if
'response.write(fromdtnew & "-" & todtnew & "-" & cno & "-" & cname & "-" & strordstatus)
if (isnumeric(cno)) then
	intcno=cno
else
	intcno=0
end if

if (isnumeric(strGroup)) then
	intgroupno=strGroup
else
	intgroupno=0
end if

if (strordstatus="") then
	strordstatus="Yes"
end if

if (strordstatus="All") then
	strordstatusnew=""
else
	strordstatusnew=strordstatus
end if
if (request("searchfrm")<>"") then	
'response.write(fromdtnew & "-" & todtnew & "-" & intcno & "-" & intgroupno & "-" & cname & "-" & strordstatus)
Set ObjCustomer = obj.CustomerOrders(fromdtnew,todtnew,intcno,intgroupno,cname,strordstatusnew,strpcode,strvno,strnewsearchoption)
arCustomer = ObjCustomer("CustomerOrders")
vRecordcount=UBound(arCustomer,2)+1
end if
set objCustomer = obj.Display_Grouplist()
arGroup = objCustomer("GroupList")	


%>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>  
  
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Find Customer Order<br>
      &nbsp;</font></b>
     
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <form method="POST"  id="form" name="form">
	    <tr>
        	<td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
			<td><strong>Van No</strong></b></td>
			<td><strong>Status</strong></b></td>
          	<td><b>Customer Code:</b></td>
			<td><b>Customer Name:</b></td>
		  	<td><b>Customer Group:</b></td>
			<td><b>Product Code:</b></td>
          	<td></td>
        </tr>
        <tr>
          	 <td></td>
			 <td valign="top">
			<select  size="4" name="selvno" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
		   	  <option value="">Select</option>
			  <%
						for i = 0 to 100
							arvno=Split(strvno,",")
							if (IsArray(arvno)) then
								flag="true"
								for j =0 to ubound(arvno) 
									if i=cint(trim(arvno(j))) then%>
									%>
									<option value="<%=i%>" selected="selected"><%=i%></option>
									<%
									flag="false"
									end if
								next
								if flag="true" then
								%>
									<option value="<%=i%>"><%=i%></option>				
								<%
								end if
							else
						%>  				
							 <option value="<%=i%>" <%if strvno<>"" then%><%if cint(strvno)= i then response.write " Selected"%><%end if%>><%=i%></option>
					  <%
					  		end if
					   next                 

					%>   
         </select>	
			 </td>
			 <td valign="top">
			 <select size="1" name="ordstatus" style="font-family: Verdana; font-size: 8pt">
			 	<option value="All" <% if (strordstatus="All") then %> selected="selected" <% end if %>>All</option>
				<option value="Yes" <% if (strordstatus="Yes") then %> selected="selected" <% end if %>>Active Orders</option>
				<option value="SO" <% if (strordstatus="SO") then %> selected="selected" <% end if %>>Active Standing Order Only</option>
				<option value="AO" <% if (strordstatus="AO") then %> selected="selected" <% end if %>>Amended Order Only</option>
								
			 </select>		
			 </td>
             <td valign="top"><input type="text" name="txtcno"  value="<%=cno%>"size="20" style="font-family: Verdana; font-size: 8pt"></td>
			 <td valign="top"><input type="text" name="txtcname"  value="<%=cname%>"size="20" style="font-family: Verdana; font-size: 8pt"></td>
			 <td valign="top">
			 	<select size="1" name="txtGroup" style="font-family: Verdana; font-size: 8pt">
				<option value=" ">Select</option>
				<%
				If IsArray(arGroup) then
					For i = 0 to UBound(arGroup,2)%>
						<option value="<%=arGroup(0,i)%>" <%if Trim(arGroup(0,i)) = Trim(strGroup) Then Response.Write "Selected"%>><%=arGroup(1,i)%></option><%
					Next 						
				End if
				%>
				</select>				    
			</td>
			  <td valign="top"><input type="text" name="txtpcode"  value="<%=strpcode%>"size="12" maxlength="20" style="font-family: Verdana; font-size: 8pt"></td>
          	<td>&nbsp;</td>
        </tr>
        <tr>
          	 <td></td>
             <td colspan="5">
			 <table border="0" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0" height="38">
              <tr>
			  	<td colspan="2">&nbsp;</td>
                <td><b>From Date</b></td>
                <td><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                <td><b>To Date</b></td>
              </tr>
              <tr>
                 <td valign="top">
			 	<select size="1" name="newsearchoption" style="font-family: Verdana; font-size: 8pt">
			 		<option value="1" <% if (strnewsearchoption="1") then %> selected="selected" <% end if %>>Order Confirmation</option>
					<option value="2" <% if (strnewsearchoption="2") then %> selected="selected" <% end if %>>Number of Products</option>
					<option value="3" <% if (strnewsearchoption="3") then %> selected="selected" <% end if %>>Value of Order</option>								
			 	</select>		
			 	</td>
				<td width="10"></td>
				<td height="18" width="325">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						 <nobr>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom size="12" onFocus="blur();" name="txtfrom" value="<%=replace(fromdt,"from","")%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
						 </nobr>
					</td>
				<td height="25"></td>
                
                <td height="18" width="325">
                    <nobr>
					<select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=replace(todt,"to","")%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					 </nobr>
					</td>
					<td><input type="submit" value="Search" name="searchfrm" style="font-family: Verdana; font-size: 8pt"></td>
				  </tr>
            </table>
			 </td>
          	
        </tr>
		</form>
      </table>	
		<% if (request("searchfrm")<>"") then	%>
		<table border="0" align="center" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">      
		<tr>
        	<td colspan="9" align="right" height="10">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>
		<tr>
			<td colspan = "2"></td>
		</tr>			
		<%if IsArray(arCustomer) Then%>
		<tr>
	 		<td align = "left"><b>Total no of customers : <%=vRecordcount%></b></td>
		</tr>
		<%End if%>	
		</table> 
	  
    	<table border="0" bgcolor="#999999"  width="100%"style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
        <tr height="24">
          <td width="50" bgcolor="#CCCCCC"><nobr><b>Customer Code</b></nobr></td>
          <td width="200" bgcolor="#CCCCCC"><b>Customer Name</b></td>
		  <%
		  intday=DateDiff("d",fromdt,todt)
		  for i=0 to intday%>
		  <td width="90" bgcolor="#CCCCCC"><nobr><strong><%=displayBristishDate(DATEADD("d",i,fromdt))%>&nbsp;&nbsp;&nbsp;&nbsp;</strong></nobr></td>
		  <%next%>
          </tr>
		  <%
					if IsArray(arCustomer) Then 
						for i = 0 to UBound(arCustomer,2)
							if (i mod 2 =0) then
								strbg="#FFFFFF"
							else
								strbg="#F3F3F3"
							end if
						%>
							<tr bgcolor="<%=strbg%>" height="24">
							  <td><%=arCustomer(0,i)%></td>
							  <td><%=arCustomer(1,i)%></td>
							 <%
							  strallordstatus=arCustomer(2,i)
							  strIsStopStatus=arCustomer(4,i)							 
							  if strallordstatus <> "" Then							  	
								arallordstatus=Split(strallordstatus,"|")
								arIsStopStatus=Split(strIsStopStatus,"|")
								
								arNoofproducts=Split(arCustomer(15,i),"|")
								arValueofOrder=Split(arCustomer(16,i),"|")
																
								for j=0 to UBound(arallordstatus)-1
									dtdate=displayBristishDate(DATEADD("d",j,fromdt))
									%>
								 	<td>
								 	<%
									if (strnewsearchoption="2") then
										response.write(arNoofproducts(j))
									elseif (strnewsearchoption="3") then
										response.write(FormatNumber(arValueofOrder(j),2))
									else
								 		response.write(arallordstatus(j))
									end if	
								 	%>
								 </td>
								 </form>
							  	<%
								next
							  End if
							  %>
							</tr>
							<%
							response.Flush()
						Next
					else
					%>
					<tr>
						<td colspan="<%=intday+3%>" bgcolor="#FFFFFF" height="30" align="center"><font color="#FF0000">Sorry no items found</font></td>							
					</tr>
					<%
					End if
					%>      
      </table>
	  <%
	  End if
	  %>
    </td>
  </tr>
</table>
<br><br>
</body>
</html>
<SCRIPT type=text/javascript>
Calendar.setup({
	inputField     :    "txtfrom",
	ifFormat       :    "%d/%m/%Y",
	button         :    "f_trigger_frfrom",
	singleClick    :    true
});
</SCRIPT>
<SCRIPT type=text/javascript>
Calendar.setup({
	inputField     :    "txtto",
	ifFormat       :    "%d/%m/%Y",
	button         :    "f_trigger_frto",
	singleClick    :    true
});
</SCRIPT>
<%
Set Obj = Nothing
Set ObjCustomer = Nothing
if IsArray(arCustomer) Then Erase arCustomer
if IsArray(arGroup) Then Erase arGroup
%>