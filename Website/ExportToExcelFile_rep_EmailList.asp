<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
	
dim objBakery
dim recarray
dim retcol
dim recarray2
dim retcol2



set objBakery = server.CreateObject("Bakery.Reports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.Display_ReportEIGroupList()
recarray = retcol("DisplayReportEIGroupList")

set retcol2 = objBakery.Display_ReportEIIndividualList()
recarray2 = retcol2("DisplayReportEIIndividualList")
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=EmailList_report.xls" 
	%>
      <table border="0" width="100%" cellspacing="0" cellpadding="5" style="font-family: Verdana; font-size: 8pt">
<tr>
    <td width="100%"><b><font size="3">Electronic Invoicing Email List<br>
      &nbsp;</font></b>
      </td>
      </tr>

 
 <tr>
    <td width="100%">
	  
      	<table border="1" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
         <tr>
          <td colspan="7" align="left" height="30">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>
		<tr>
          <td colspan="7"><b>Electronic Invoicing Email List - Group</b></td>
        </tr>
        <tr>
          <td width="110" bgcolor="#CCCCCC"><b>Group No</b></td>
          <td width="110" bgcolor="#CCCCCC"><b>Group Name</b></td>
		  <td width="200" bgcolor="#CCCCCC"><b>Email</b></td>
          
        </tr>
       <%if isarray(recarray) then%>
		
		<%for i=0 to UBound(recarray,2)%>
		<tr>
          <td><%=recarray(0,i)%></td>
          <td><%=recarray(1,i)%></td>
		  <td><%=recarray(2,i)%></td>
         
         </tr>
         
          <%next
         
          else%>
          	<tr><td colspan="7"  bgcolor="#CCCCCC"><b>0 - No records matched...</b></td></tr>
		  <%end if%>
		  
		  
		  <tr>
          <td colspan="7"><b>Electronic Invoicing Email List - Individual</b></td>
        </tr>
        <tr>
          <td width="110" bgcolor="#CCCCCC"><b>Customer No</b></td>
          <td width="110" bgcolor="#CCCCCC"><b>Customer Name</b></td>
		  <td width="200" bgcolor="#CCCCCC"><b>Email</b></td>
          
        </tr>
       <%if isarray(recarray2) then%>
		
		<%for i=0 to UBound(recarray2,2)%>
		<tr>
          <td><%=recarray2(0,i)%></td>
          <td><%=recarray2(1,i)%></td>
		  <td><%=recarray2(2,i)%></td>
         
         </tr>
         
          <%next
         
          else%>
          	<tr><td colspan="7"  bgcolor="#CCCCCC"><b>0 - No records matched...</b></td></tr>
		  <%end if%>
      </table>
    </td>
  </tr>
</table>
</body>
</html>






