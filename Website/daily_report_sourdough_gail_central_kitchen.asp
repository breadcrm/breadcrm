<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<% LCID = 2057 %>
<%
Response.Buffer
Dim intI
Dim intJ
Dim intN
Dim intRow
Dim intF
Dim intTotal
intN = 0
intTotal = 0
Dim isBreak 
isBreak = 0

PrintPgSize = 25
Dim rowCountHeader
Dim rowCountShopHeader 
rowCountHeader = 11
rowCountShopHeader = 6

pCode = ""
deldate = request.form("deldate")
selecteddate = request.form("deldate")
'increase the date by one to get the correct data for the report.
deldate = NextDayinDDMMYY(deldate)
displaydeldate = request.form("deldate")
strdeldate = NextDayinDDMMYY(displaydeldate)
'This is haed coded but need to replace the id for new facility.
facility= 34
dtype=Request.Form("deltype")
if deldate= "" or facility = "" or dtype = "" then response.redirect "daily_report.asp"
stop
set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
'set col1= object.DailySourDoughOrderSheetViewReport(deldate,facility,dtype)
set col1= object.DailySourDoughOrderSheetViewReportForGail(deldate,facility,dtype)
vRecArray = col1("SourDoughOrdersheet")

set col11 = object.DailySourDoughGailFullOrderSheetViewReport(deldate,facility,"Y",dtype)
vRecArrayNew = col11("SourDoughOrdersheet")

set col2= object.DailySourDoughOrderSheetViewReportForGailExcluding48(deldate,facility,dtype)
vRecArray2 = col2("SourDoughOrdersheet")

set col22 = object.DailySourDoughGailFullOrderSheetViewReport(deldate,facility,"N",dtype)
vRecArray2New = col22("SourDoughOrdersheet")



dim arrCName
dim arrCName2
dim arrPName
dim arrPName2

set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set objCName = objBakery.GetFilterData(vRecArrayNew,5,arrCName)
arrCName = objCName("FilterData")
set objPName = objBakery.GetFilterData(vRecArrayNew,1,arrPName)
arrPName = objPName("FilterData")
set objBakery = nothing

set objBakery2 = server.CreateObject("Bakery.reports")
objBakery2.SetEnvironment(strconnection)
set objCName2 = objBakery2.GetFilterData(vRecArray2New,5,arrCName2)
arrCName2 = objCName2("FilterData")
set objPName2 = objBakery2.GetFilterData(vRecArray2New,1,arrPName2)
arrPName2 = objPName2("FilterData")
set objBakery2 = nothing

'set maxColCount= object.DailySourDoughGailOrderSheetViewReportMaxColumn(deldate,facility,dtype)
'vMaxColCountArray = maxColCount("MaxColumnCount")

'MaxColumnCount = 0
'if isarray(vMaxColCountArray) then
	'MaxColumnCount = vMaxColCountArray(0,0) - 1
'end if

if isarray(vrecarray2) then
  totrecs = ubound(vrecarray2,2)+1
else
  totrecs = 0
end if
curpage=0
totpages=0
if totrecs <> 0 then
  if clng(totrecs) mod 20  = 0 then
    totpages = clng(totrecs)/20
    curpage=1
  else
    totpages = fix(clng(totrecs)/20) +  1
    curpage=1
  end if
end if

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
set object = nothing
intF = 1
If isArray(vRecArray2) Then
	If ubound(vRecArray2, 2) = 0 Then
		intF = 1
	Else
		intF = Round((ubound(vRecArray2, 2)/PrintPgSize) + 0.49)
		
		intF = intF + Round((((intF * rowCountShopHeader) + rowCountHeader) /PrintPgSize) + 0.49)
	End If
End If

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>GAIL's central Kitchen</title>
<style type="text/css">

<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px} input#btnEmail{display: none;}}
-->
</style>
<style type="text/css" media="print">
@page
{
	size: landscape;
}
</style>

<style type="text/css" media="print">
    .page
    {
     -webkit-transform: rotate(-90deg); -moz-transform:rotate(-90deg);
     filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }
</style>

</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">

<!--Start of GAIL's central Kitchen (excluding 48 hours product) -->
<p>&nbsp;</p>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3">GAIL's central Kitchen (Excluding 48 hours product)<br>
      &nbsp;</font></b>
      <table border="0" width="85%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>Facility: </b><%=Facilityname %><br>
			<b>To the Attention of: </b><%=attn%><br>
			<b>By Fax Number: </b><%=fax%><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			<!--Date of report: <%=selecteddate%><br>-->
			To be produced and ready to be delivered on <%=strdeldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
            Number of pages for this report: <%=intF%><br><br>
          </td>
        </tr>
      </table>
    </td>
  </tr>
 
</table>

<%

if isarray(vRecArray2) then 
	for intI = 1 to intF
	
	if intI <> 1 Then
	rowCountHeader = 0
	End if 
%>
<table align="center" border="2" width="950" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
<%
		if intI = 1 or isBreak = 1 then
		%>
              <tr>
              <td width="150" height="20"><b>Product code</b></td>
              <td width="451" height="20"><b>Product</b></td>
              <!--<td width="77" height="20"><b>Size</b></td>-->
              <td width="50" height="20"><b>Total Production</b></td>
			  <%for m=0 to UBound(arrCName2)
			  %>
	        	<td><b><%=arrCName2(m)%></b></td>
	    	  <%next%>
              </tr>
			  <%
		isBreak = 0
		rowCountShopHeader = 4
		else
		rowCountShopHeader = 0
		end if
		%>
		<%
        For intJ = 1 To (PrintPgSize - rowCountHeader - rowCountShopHeader) 'PrintPgSize - changed by selva 21 Nov 2006
			
			if intN <= ubound(vRecArray2, 2) Then
			pCode = vRecArray2(0,intN)
			set object1 = Server.CreateObject("bakery.daily")
			object1.SetEnvironment(strconnection)
			set col1Customers2= object1.DailySourDoughGailOrderSheetViewReport(pCode,deldate)
			vCustomersArray2 = col1Customers2("SourDoughOrdersheet")
		%>
            <tr>
            <td width="150" height="20"><%=vRecArray2(0,intN)%></td>
            <td width="451" height="20"><%=vRecArray2(1,intN)%></td>
            <!--<td width="77" height="20"><%=vRecArray2(2,intN)%></td>-->
            <td width="50" height="20"><%=vRecArray2(3,intN)%></td>
			<%
			dim vCustomerTotal2
			For intC = 0 to UBound(arrCName2)
				vCustomerTotal2 = "0"
				For intC1 = 0 to ubound(vCustomersArray2, 2)
					if arrCName2(intC) = vCustomersArray2(0,intC1) then
						vCustomerTotal2 = vCustomersArray2(1,intC1)
					end if
				Next
			%>
				<td width="90" height="20"><%=vCustomerTotal2%></td>
			<%
			Next
			set object1 = nothing
			%>
			
            </tr>
			<%
				intTotal = intTotal + vRecArray2(3,intN)
			End if
			if intN = ubound(vRecArray2, 2) Then
		%>
		
           <tr>
            <td width="451" colspan="2" height="20" align="right"><b>Total&nbsp; </b></td>
            <td width="70" height="20"><b><%=intTotal%>&nbsp;</b></td>
            </tr>
			<%
			End if
			intN = intN + 1
		Next
%>
</table>
<%
	if intI <> intF Then
	isBreak = 1
	Response.Flush()
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>


<%
	End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2">Sorry no items found</td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>

<!--End of GAIL's central Kitchen (excluding 48 hours product) -->

<DIV CLASS="PAGEBREAK">&nbsp;</DIV>

<!--Start of GAIL's central Kitchen (48 hours product) -->
<%

intN = 0
intTotal = 0
rowCountHeader = 11
rowCountShopHeader = 4

if isarray(vrecarray) then
  totrecs = ubound(vrecarray,2)+1
else
  totrecs = 0
end if
curpage=0
totpages=0
if totrecs <> 0 then
  if clng(totrecs) mod 20  = 0 then
    totpages = clng(totrecs)/20
    curpage=1
  else
    totpages = fix(clng(totrecs)/20) +  1
    curpage=1
  end if
end if

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
set object = nothing
intF = 1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round((ubound(vRecArray, 2)/PrintPgSize) + 0.49)
		intF = intF + Round((((intF * rowCountShopHeader) + rowCountHeader) /PrintPgSize) + 0.49)
	End If
End If

 %>
<p>&nbsp;</p>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3">48 Hours Report of GAIL's central Kitchen<br>
      &nbsp;</font></b>
      <table border="0" width="85%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>Facility: </b><%=Facilityname %><br>
			<b>To the Attention of: </b><%=attn%><br>
			<b>By Fax Number: </b><%=fax%><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			<!--Date of report: <%=selecteddate%><br>-->
			To be produced and ready to be delivered on <%=strdeldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
            Number of pages for this report: <%=intF%><br><br>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  
</table>
<%

if isarray(vRecArray) then 

	for intI = 1 to intF
	
	if intI <> 1 Then
	rowCountHeader = 0
	End if 
%>
<table align="center" border="2" width="900" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
<%
		if intI = 1 or isBreak = 1 then
		%>
              <tr>
              <td width="99" height="20"><b>Product code</b></td>
              <td width="401" height="20"><b>Product</b></td>
              <!--<td width="77" height="20"><b>Size</b></td>-->
              <td width="70" height="20"><b>Total Production</b></td>
			  <%for m=0 to UBound(arrCName)
			  %>
	        	<td><b><%=arrCName(m)%></b></td>
	    	  <%next%>
              </tr>
			  <%
			  isBreak = 0
			  rowCountShopHeader = 4
			  else
			  rowCountShopHeader = 0
		end if
		%>
		<%
        For intJ = 1 To (PrintPgSize - rowCountHeader - rowCountShopHeader) 'PrintPgSize - changed by selva 21 Nov 2006
			if intN <= ubound(vRecArray, 2) Then
			pCode = vRecArray(0,intN)
			set object1 = Server.CreateObject("bakery.daily")
			object1.SetEnvironment(strconnection)
			set col1Customers= object1.DailySourDoughGailOrderSheetViewReport(pCode,deldate)
			vCustomersArray = col1Customers("SourDoughOrdersheet")
		%>
            <tr>
            <td width="99" height="20"><%=vRecArray(0,intN)%></td>
            <td width="451" height="20"><%=vRecArray(1,intN)%></td>
            <!--<td width="77" height="20"><%=vRecArray(2,intN)%></td>-->
            <td width="70" height="20"><%=vRecArray(3,intN)%></td>
			<%
			dim vCustomerTotal
			For intC = 0 to UBound(arrCName)
				vCustomerTotal = "0"
				For intC1 = 0 to ubound(vCustomersArray, 2)
					if arrCName(intC) = vCustomersArray(0,intC1) then
						vCustomerTotal = vCustomersArray(1,intC1)
					end if
				Next
			%>
				<td width="90" height="20"><%=vCustomerTotal%></td>
			<%
			Next
			set object1 = nothing
			%>
			
            </tr>
			<%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
		
           <tr>
            <td width="401" colspan="2" height="20" align="right"><b>Total&nbsp; </b></td>
            <td width="70" height="20"><b><%=intTotal%>&nbsp;</b></td>
            </tr>
			<%
			End if
			intN = intN + 1
		Next
%>
</table>
<%
	if intI <> intF Then
	isBreak = 1
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>



<%
	End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2">Sorry no items found</td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>

<!--End of GAIL's central Kitchen (48 hours product) -->

</body>
</html>