<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/top.inc" -->
<%
deldate= request.form("deldate")
if deldate= "" then response.redirect "LoginInternalPage.asp"

Set object = Server.CreateObject("bakery.general")
set detail = object.DisplayFacility()	
vFacarray =  detail("Facility") 
set detail = object.DisplayVec()	
vVecarray =  detail("Vec") 

set detail = Nothing
set object = Nothing


dim objBakery
dim recarray1, recarray2
dim ordno
dim retcol
Dim deldate
Dim intI
Dim intN
Dim intF
Dim intJ
intN = 0

dim tname, tno, curtno
Dim obj
Dim arCreditStatements
Dim arCreditStatementDetails
Dim vCreditDate
tno=3
%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; }
-->
</style>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
'daily_report_order.asp
'----------------------
curtno=1
'facility= request.form("facility")
dim totpages, totrecs, curpage, currec
if isarray(vFacarray) then
  set object = Server.CreateObject("bakery.daily")
  
  for k=0 to ubound(vFacarray,2)
    facilityNo = vFacarray(0,k)
    if clng(facilityNo) = 12 then
    'or clng(facilityNo) = 15
      tno=2
    else
      tno=1
    end if
    for curtno=1 to tno
      intN=0
	  if curtno=1 then
	    if clng(tno)=1 then
	      tname = "Morning','Noon','Evening"
	    else
	      tname="Morning"
	      
	    end if
	  elseif curtno=2 then
	    tname="Noon','Evening"
	   
	  end if
		set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
		vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
		Facility = DisplayDailyFacilityOrderSheet("Facility")
		set DisplayDailyFacilityOrderSheet= nothing
		intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><%
      if clng(tno) = 1 then 
        tname = ""
      elseif clng(curtno) = 2 then
        tname = "(Noon + Evening)"
      end if%>
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%> <% if tname <> "" then Response.Write (" - " & tname)%><br>
      &nbsp;</font></b>
      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<!--<b>Delivery Type: <%=tname%></b><br>-->
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="65%" height="20"><b>Product name</b></td>
          <td width="11%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
        </tr><%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="4" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr><%
		intJ=intJ+1
		end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="65%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="11%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN)%></td>
        </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="90%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p><%
      end if
    next
  next
  set object = nothing
end if
tno=3%>
<p>&nbsp;</p>
<!--
<%
'daily-report_invoice.asp
'------------------------

intN = 0
ordno = 0
curtno=1
set objBakery = server.CreateObject("Bakery.MgmReports")

for curtno=1 to tno
  if curtno=1 then
    tname="Morning"
  elseif curtno=2 then
    tname="Noon"
  else
    tname="Evening"
  end if
  set retcol = objBakery.GetInvoiceTotal(ordno, deldate, tname)
  recarray1 = retcol("InvoiceTotal")
  set retcol = Nothing

  If isArray(recarray1) Then
	intF = ubound(recarray1, 2) + 1
  End If

  if IsArray(recarray1) Then%>
    <div align="center">
      <b><font size="3">Invoices</font></b>
      </div><%
    For intI = 0 To ubound(recarray1, 2)
      intN = 0%>
      <DIV CLASS="PAGEBREAK"></DIV>
      <div align="center">
      <center>
      <table border="0" width="90%" height="100%" cellspacing="0" cellpadding="0">
      <tr>
      <td width="100%" valign="top">
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
      <td width="33%" valign="top"><b><font face="Verdana" size="2">VAT Reg No. 539 3383 22</font></b></td>
      <td width="33%" valign="top">
      <p align="center"><b><font face="Verdana" size="4"><%=recarray1(3, intI)%></font></b></td>
      </center>
      <td width="34%" valign="top">
      <p align="right">
      <b><font face="Verdana" size="4">THIS IS AN INVOICE</font><br>
      <font face="Verdana" size="2">PLEASE KEEP SAFE AND PASS TO YOU ACCOUNTS DEPARTMENT</font></b>
      </td>
      </tr>
      <tr>
      <td width="33%" valign="top"><b><font face="Verdana" size="4">&nbsp;</font></b></td>
      <td width="33%" valign="top"></td>
      <td width="34%" valign="top"></td>
      </tr>
      </table>
      <center>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
      <td width="33%"></td>
      <td width="33%">
      <p align="center"><font size="3" face="Verdana"><b>INVOICE - <%=tname%><br>
      </b>NUMBER: <%=recarray1(0, intI)%></font></td>
      <td width="34%"></td>
      </tr>
      <tr>
      <td width="33%"><font size="3" face="Verdana"><b>&nbsp;</b></font></td>
      <td width="33%"></td>
      <td width="34%"></td>
      </tr>
      </table>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
      <td valign="top"><font face="Verdana" size="2"><b>
      <%=recarray1(2, intI)%><br>
      <%=recarray1(5, intI)%><br>
      <%=recarray1(6, intI)%><br>
      <%=recarray1(7, intI)%><br>
      <%=recarray1(8, intI)%>
      </b></font></td>
      <td width="33%" valign="top"><font face="Verdana" size="2"><b>
      Purchase order No.: <%=recarray1(9, intI)%><br><br>
      Delivered by van number: <%=recarray1(10, intI)%><br><br>
      Date: <%= Day(recarray1(1, intI)) & "/" & Month(recarray1(1, intI)) & "/" & Year(recarray1(1, intI)) %>
      </b></font></td>
      </tr>
      <tr>
      <td valign="top"><font face="Verdana" size="2"><b>&nbsp;</b></font></td>
      <td width="33%" valign="top"><font face="Verdana" size="2"><b>&nbsp;</b></font></td>
      </tr>
      <tr>
      <td valign="top"><font face="Verdana" size="2"><b>&nbsp;</b></font></td>
      <td width="33%" valign="top"><font face="Verdana" size="2"><b>&nbsp;</b></font></td>
      </tr>
      </table>
      <div align="center">
        <center>
        <table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
          <tr>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Quantity</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Product code</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Unit price</b></td>
            <td width="20%" align="center" bgcolor="#CCCCCC" height="20"><b>Total price</b></td>
          </tr><%
			set retcol = objBakery.GetInvoiceDetails(recarray1(0, intI))
			recarray2 = retcol("InvoiceDetails")
			if isarray(recarray2) then
				for intN=0 to ubound(recarray2, 2)
				  if recarray1(0, intI) = recarray2(0, intN) then%>
                    <tr>
                    <td align="center"><%=recarray2(1, intN)%>&nbsp;</td>
                    <td align="center"><%=recarray2(2, intN)%>&nbsp;</td>
                    <td align="center"><%=recarray2(3, intN)%>&nbsp;</td><%
                    if isnull(recarray2(4, intN)) then%>
                      <td align="center">0.00</td><%
                    else%>
                      <td align="center"><%=FormatNumber(recarray2(4, intN), 2)%>&nbsp;</td><%
                    end if
                    if isnull(recarray2(5, intN)) then%>
                       <td width="20%" align="center">0.00</td><%
                    else%>
                       <td width="20%" align="center"><%=FormatNumber(recarray2(5, intN), 2)%>&nbsp;</td><%
                    end if%>
                    </tr><%
				  end if
				next
				erase recarray2
			end if
			set retcol = Nothing %>
            </table>
            </center>
            </div>
            <div align="center">
            <table border="0" width="90%" cellspacing="0" cellpadding="0">
            <tr>
            <td>
              <p align="right"><b><font size="2" face="Verdana">Goods total&nbsp;&nbsp; </font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%"><%
                    if isnull(recarray1(11,intI)) then%>
                    <p align="center"><font size="3" face="Verdana"><b>&nbsp;0.00</b></font></p><%
                    else%>
                    <p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(recarray1(11,intI), 2)%></b></font></p><%
                    end if%>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          <tr>
            <td>
              <p align="right"><b><font size="2" face="Verdana">VAT total&nbsp;&nbsp; </font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%"><%
                    if isnull(recarray1(12,intI)) then%>
                    <p align="center"><font size="3" face="Verdana"><b>&nbsp;0.00</b></font></p><%
                    else%>
                    <p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(recarray1(12,intI), 2)%></b></font></p><%
                    end if%>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          <tr>
            <td>
              <p align="right"><b><font size="2" face="Verdana">Invoice total&nbsp;&nbsp; </font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%"><%
                    if isnull(recarray1(13,intI)) then%>
                    <p align="center"><font size="3" face="Verdana"><b>&nbsp;0.00</b></font></p><%
                    else%>
                    <p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(recarray1(13,intI), 2)%></b></font></p><%
                    end if%>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          </table>
      </div>
      <br>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%">&nbsp;</td>
        </tr>
        <tr>
          <td width="100%">
            <span style="font-size:12pt"><b>IMPORTANT!! </b><br>
                Shortages must be <br>
                reported on day of delivery.<br> 
                To ensure next day 
                delivery,<br>ring before 14:00 Mon. - Fri </span>
            &nbsp;</td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
          <td width="100%" align="center"><font size="1">
          Proprietor: Bread Limited incorporated in England and Wales � company No. 3237576 Registered office: Harben House<br>
           Harben Parade Finchley Road London NW3 6LH</font>
          </td>
        </tr>
                <tr><td>&nbsp;</td></tr>
        <tr>
          <td width="100%" align="center">
                    <b>Comments:</b><br>
          For some customers you have to fill in the purchase order number in order to produce<br>
          the invoice. The system should not produce the invoice unless this cell was filled in. 

          </td>
        </tr>
      </table>
      <br><br>
    </td>
  </tr>
</table>
</center>
</div>
<% 
	Next
End if 
next
set objBakery = Nothing
%>
<p>&nbsp;</p>

<%
'daily_report_van.asp
'--------------------

van= request.form("van")
curtno=1

if isarray(vVecarray) then
  set object = Server.CreateObject("bakery.daily")
  for curtno=1 to tno
    if curtno=1 then
      tname="Morning"
    elseif curtno=2 then
      tname="Noon"
    else
      tname="Evening"
    end if
    for k=0 to ubound(vvecarray,2)
      intN=0
      van = vvecarray(0,k)
      set DisplayDailyVanDriverSheet= object.DisplayDailyVanDriverSheet(deldate,van,tname)
vRecArray = DisplayDailyVanDriverSheet("Vansheet")
set DisplayDailyFacilityOrderSheet= nothing
intF=1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round((ubound(vRecArray, 2)/PrintPgSize) + 0.49)
	End If
End If
if isarray(vRecArray) then
intTotal=0
%>
<DIV CLASS="PAGEBREAK"></DIV>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3">Van Driver Sheet - <%=tname%><br>
      &nbsp;</font></b>
      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            To be packed on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%>
            <p>Number of pages for this report: <%= intF %></p>
            <p>&nbsp;</p>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><p><b>VAN <%=van%></b></p></td>
	</tr>
</table><%
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="11%" height="20"><b>Product code</b></td>
          <td width="56%" height="20"><b>Product name</b></td>
          <td width="8%" height="20"><b>Quantity</b></td>
        </tr><%
		end if
        For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="11%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="56%" height="20"><%=vRecArray(3,intN)%></td>
          <td width="8%" height="20"><%=vRecArray(5,intN)%></td>
        </tr><%
				intTotal = intTotal + vRecArray(5,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
%>
        <tr>
          <td width="67%" height="20" colspan="2" align="right"><b>Total&nbsp; </b></td>
          <td width="8%" height="20"><b><%=intTotal%>&nbsp;</b></td>
        </tr><%
			End if
			intN = intN + 1
		Next
%>
</table><%
		if intI <> intF Then
%>
<br class="page" /><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p><%
end if
next
next
end if%>

<div align="center">
<b><font size="3">Credit Report</font></b>
</div><br>
-->

<%
'daily_report_credit.asp
'----------------------=

intN = 0

vCreditDate = Request.Form ("deldate")

Set obj = server.CreateObject("bakery.daily")
Set objBakery = obj.Display_CreditStatements(vCreditDate)
arCreditStatements =  objBakery("CreditStatements")
arCreditStatementDetails =  objBakery("CreditStatementDetails")

Set objBakery = Nothing
Set obj = Nothing

If isArray(arCreditStatements) Then
	intF = ubound(arCreditStatements, 2) + 1
End If

if IsArray(arCreditStatements) Then
    For intI = 0 To ubound(arCreditStatements, 2)
		intN = 0
%>
<DIV CLASS="PAGEBREAK"></DIV>
<div align="center">
<center>
<table border="0" width="90%" height="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%" valign="top">
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="33%" valign="top"><b><font face="Verdana" size="2">VAT Reg No. 539 3383 22</font></b></td>
          <td width="33%" valign="top">
            <p align="center"><b><font face="Verdana" size="4"><%=arCreditStatements(11, intI)%></font></b></td>
        </center>
          <td width="34%" valign="top" align="right"><b>
			<font face="Verdana" size="4">
			THIS IS A CREDIT<br>
			STATEMENT<br>
			</font>
			<font face="Verdana" size="2">PLEASE KEEP SAFE AND PASS TO YOU ACCOUNTS DEPARTMENT</font>
           </b></td>
        </tr>
        <tr>
          <td width="33%" valign="top"><b><font face="Verdana" size="4">&nbsp;</font></b></td>
          <td width="33%" valign="top"></td>
          <td width="34%" valign="top"></td>
        </tr>
      </table><center>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="33%"></td>
          <td width="33%">
            <p align="center"><font size="3" face="Verdana"><b>Credit<br>
            </b>NUMBER: <%=arCreditStatements(0, intI)%></font></td>
          <td width="34%"></td>
        </tr>
        <tr>
          <td width="33%"><font size="3" face="Verdana"><b>&nbsp;</b></font></td>
          <td width="33%"></td>
          <td width="34%"></td>
        </tr>
      </table>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="top"><font face="Verdana" size="2">
          Customer Name: <%=arCreditStatements(2, intI)%><br>
          Address: <%=arCreditStatements(4, intI)%>,&nbsp;<%=arCreditStatements(5, intI)%>,&nbsp;<%=arCreditStatements(6, intI)%>,&nbsp;<%=arCreditStatements(7, intI)%><br><br>
          Date: <%=arCreditStatements(1, intI)%>
          </font></td>
        </tr>
        <tr>
          <td valign="top"><font face="Verdana" size="2"><b>&nbsp;</b></font><p><font face="Verdana" size="2"><b>&nbsp;</b></font></td>
        </tr>
        </table>
      <div align="center">
        <center>
        <table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
          <tr>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Quantity</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Product code</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Unit price</b></td>
            <td width="20%" align="center" bgcolor="#CCCCCC" height="20"><b>Total price</b></td>
          </tr><%
			for intN = 0 to ubound(arCreditStatementDetails, 2)
				if arCreditStatements(0, intI) = arCreditStatementDetails(0, intN) then
		%><tr>
            <td align="center"><%=arCreditStatementDetails(1, intN)%>&nbsp;</td>
            <td align="center"><%=arCreditStatementDetails(2, intN)%>&nbsp;</td>
            <td align="center"><%=arCreditStatementDetails(3, intN)%>&nbsp;</td>
            <td align="center"><%=FormatNumber(arCreditStatementDetails(4, intN), 2)%>&nbsp;</td>
            <td width="20%" align="center"><%=FormatNumber(arCreditStatementDetails(5, intN), 2)%>&nbsp;</td>
            </tr><%
		          end if
	            next
          %>
        </table>
        </center>
      </div>

      <div align="center">
        <table border="0" width="90%" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <p align="right"><b><font size="2" face="Verdana">Goods total&nbsp;&nbsp; </font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%">
                    <p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(arCreditStatements(8, intI), 2)%></b></font></p>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          <tr>
            <td>
              <p align="right"><b><font size="2" face="Verdana">VAT total&nbsp;&nbsp; </font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%">
                    <p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(arCreditStatements(9, intI), 2)%></b></font></p>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          <tr>
            <td>
              <p align="right"><b><font size="2" face="Verdana">Invoice total&nbsp;&nbsp; </font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%">
                    <p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(arCreditStatements(10, intI), 2)%></b></font></p>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          </table>
      </div>

      </td>
  </tr>
</table>
</center>
</div>
<% if intI >= intF Then %>
<hr>
<br class="page" />
<%
	End if
	Next
End if %>
<p></p>

<!--
<%
'daily_report_pack.asp
'--------------------=
if isarray(vvecarray) then
  set object = Server.CreateObject("bakery.daily")
  for curtno=1 to tno
    if curtno=1 then
      tname="Morning"
    elseif curtno=2 then
      tname="Noon"
    else
      tname="Evening"
    end if
    for k=0 to ubound(vvecarray,2)
      van = vvecarray(0,k)
      set DisplayDailyPackingSheet= object.DisplayDailyPackingSheet(deldate,van,tname)
vRecArray = DisplayDailyPackingSheet("Packingsheet")
set DisplayDailyPackingSheet= nothing


mLine = 7
i=0
if isarray(vRecArray) then 
	mPage = 1
	
	cno = vRecArray(2,i)
	for i = 0 to ubound(vRecArray,2)
	newcno = vRecArray(2,i)      
	if cno <> newcno or i = 0 then
		cno = newcno               
	
		if i <>0 then	 
			mLine = mline  + 3			
			if mLine >= PrintPgSize and i < ubound(vRecArray,2) Then 
				mPage = mPage + 1
				mLine = 0
			End if
		end if  	   
		mLine = mLine + 2
	end if
	mLine = mLine + 1	
	if mLine >= PrintPgSize and i < ubound(vRecArray,2) Then 
		mPage = mPage + 1
		mLine = 0		
	End if	
	next
Else
	mPage = 1		
end if
i=0
if isarray(vRecArray) then %>
<DIV CLASS="PAGEBREAK"></DIV>
<div align="center">     
<table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
<td>
<p align="center"><b><font size="3">Packing Sheet - <%=tname%></font><b></p>
</td>
</tr>
<tr>
<td>
<br>
<br>
Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
To be packed on: <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br>
VAN: <%=van%><br>
</td>
</tr>

<tr>
<td>
Number of pages for this report: <%=mPage%><BR>&nbsp; 
</td>
</tr>
</table>
</div>
<%mLine = 7%>

<%
if isarray(vRecArray) then 
	mPage = 1
	cno = vRecArray(2,i)
	for i = 0 to ubound(vRecArray,2)
	newcno = vRecArray(2,i)      
	if cno <> newcno or i = 0 then
	cno = newcno            
	%>        
	<%if i <>0 then%>
	<tr>
	<td width="15%" height="20"></td>
	<td width="45%" height="20"><p align="right"><b>TOTAL:&nbsp;&nbsp; </b></p></td>
	<td width="15%" bgcolor="#CCCCCC" height="20"><%=total%>&nbsp;</td>
	<td width="25%" bgcolor="#CCCCCC" height="20">&nbsp;</td>
	</tr>
	<tr>
	<td width="15%" height="20">
	<td width="45%" height="20">
	<td width="15%" height="20"><b>TOTAL PIECES IN PACKAGE:&nbsp;&nbsp; </b></td>
	<td width="25%" bgcolor="#CCCCCC" height="20"></td>
	</tr>		
	</table>
</div></br>	<% 
	mLine = mline  + 3
	total = 0
	if mLine >= PrintPgSize and i < ubound(vRecArray,2) Then 
		mPage = mPage + 1
		mLine = 0%>		
		<br class="page" /><%
	End if%>
	<%end if%>  	   
<div align="center">      
<table border="2" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
	<tr>
	<td width="15%" height="20"><b>Customer Code</b></td>
	<td width="45%" height="20"><b><%=vRecArray(2,i)%></b></td>
	<td width="15%" height="20"><b>Customer Name</b></td>
	<td width="25%" height="20"><b><%=vRecArray(3,i)%></b></td>
	</tr>
	<tr>
	<td width="15%" height="20"><b>Product code</b></td>
	<td width="45%" height="20"><b>Product name</b></td>
	<td width="15%" height="20"><b>Quantity</b></td>
	<td width="25%" height="20"><b>Short in delivery</b></td>
	</tr>
	<%
	mLine = mLine + 3
	end if
	%>
	<tr>
	<td width="15%" height="20"><%=vRecArray(4,i)%></td>
	<td width="45%" height="20"><%=vRecArray(5,i)%></td>
	<td width="15%" height="20"><%=vRecArray(7,i)%></td>
	<td width="25%" height="20"></td>
	</tr><%
	mLine = mLine + 1
	total = total + vRecArray(7,i) 	
	if mLine >= PrintPgSize and i < ubound(vRecArray,2) Then 
		mPage = mPage + 1
		mLine = 0%>		
		</table>
		</div>
		<br class="page" />
		<div align="center">      
		<table border="2" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%		
	End if
	
	next
	%>
	<%
	if total>0 then
	%>
	<tr>
	<td width="15%" height="20"></td>
	<td width="45%" height="20"><p align="right"><b>TOTAL:&nbsp;&nbsp; </b></p></td>
	<td width="15%" bgcolor="#CCCCCC" height="20"><%=total%>&nbsp;</td>
	<td width="25%" bgcolor="#CCCCCC" height="20">&nbsp;</td>
	</tr>
	<tr>
	<td width="15%" height="20">
	<td width="45%" height="20">
	<td width="15%" height="20"><b>TOTAL PIECES IN PACKAGE:&nbsp;&nbsp; </b></td>
	<td width="25%" bgcolor="#CCCCCC" height="20"></td>
	</tr>
</table>
</div>
<% if i < ubound(vRecArray,2) Then %>
<br class="page" />
<% End if %>
	<%
	end if
	else
	%>
<div align="center">      
<table border="2" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
	<tr>
	<td align = "center" width="100%" height="20">Sorry no items found</td>
	</tr>
</table>
</div>
<%
end if
end if
next
next
end if
%>
-->
<%
'sour dough report
'------------------
set object=nothing
'Response.Write "deldate = " & deldate & "<BR>"
if deldate <> "" then
'a1=split(deldate,"/")
'deldate = a1(1) & "/" & a1(0) & "/" & a1(2)
deldate = NextDayinDDMMYY(deldate)
'deldate = day(deldate) & "/" & month(deldate) & "/" & year(deldate)
end if
'Response.Write "deldate = " & deldate

if isarray(vFacarray) then
  set object = Server.CreateObject("bakery.daily")
  for curtno=1 to tno
    if curtno=1 then
      dtype="Morning"
    elseif curtno=2 then
      dtype="Noon"
    else
      dtype="Evening"
    end if
    dtype ="Morning','Noon','Evening"
    curtno=3
    'Response.Write "curtno = " & curtno
    for k=0 to ubound(vFacarray,2)
      intN=0
      facility = vFacarray(0,k)
      if clng(facility) = 15 then
      set col1= object.DailySourDoughOrderSheet(deldate,facility,dtype)
vRecArray = col1("SourDoughOrdersheet")
if isarray(vrecarray) then
  totrecs = ubound(vrecarray,2)+1
else
  totrecs = 0
end if
curpage=0
totpages=0
if totrecs <> 0 then
  if clng(totrecs) mod 20  = 0 then
    totpages = clng(totrecs)/20
    curpage=1
  else
    totpages = fix(clng(totrecs)/20) +  1
    curpage=1
  end if
end if

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
intF=1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
	End If
End If
if isarray(vRecArray) then 
intTotal=0
%>
<DIV CLASS="PAGEBREAK"></DIV>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3">48 Hours Sour Dough Report<br>
      &nbsp;</font></b>
      <table border="0" width="85%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>Facility: </b><%=Facilityname %><br>
			<b>To the Attention of: </b><%=attn%><br>
			<b>By Fax Number: </b><%=fax%><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
            Number of pages for this report: <%=intF%>
            <%Response.Write(replace(space(60)," ","&nbsp;"))%>
			Page 1 of <%=intF%>
            <br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
  curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="654" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
<%
		if intI = 1 then
		%>
              <tr>
              <td width="99" height="20"><b>Product code</b></td>
              <td width="401" height="20"><b>Product name</b></td>
              <td width="77" height="20"><b>Size</b></td>
              <td width="70" height="20"><b>Quantity</b></td>
              </tr><%
		end if
        For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
		if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="4" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr><%
		intJ=intJ+1
		end if%>
            <tr>
            <td width="99" height="20"><%=vRecArray(0,intN)%></td>
            <td width="451" height="20"><%=vRecArray(1,intN)%></td>
            <td width="77" height="20"><%=vRecArray(2,intN)%></td>
            <td width="70" height="20"><%=vRecArray(3,intN)%></td>
            </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
            <tr>
            <td width="629" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
            <td width="70" height="20"><b><%=intTotal%>&nbsp;</b></td>
            </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2">Sorry no items found</td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p><%
end if
end if
next
next
end if
set col1= nothing
set object = nothing%>

<%
'ordering sheet by customer
'--------------------------

deldate= request.form("deldate")
if isarray(vFacarray) then
  set object = Server.CreateObject("bakery.daily")
  'for curtno=1 to tno
    for k=0 to ubound(vFacarray,2)
      facilityNo=vfacarray(0,k)
      intN = 0
      intJ = 0
      intQ = 0
      intP = 1
      intTotal = 0
      intF = 0
      intD = 0
	  intRow = 1
      if clng(facilityNo) = 21  or clng(facilityNo) = 22  or clng(facilityNo) = 23 then 
      if curtno=1 then
        deltype=1
      elseif curtno=2 then
        deltype=2
      else
        deltype=3
      end if
      delType = "1,2,3"
     set DisplayReport= object.Display_OrderingSheetByCustomer(facilityNo, delType, delDate)
     arOrdSheetCustomerDetails = DisplayReport("OrdSheetCustomerDetails")
     arCustomerList = DisplayReport("CustomerList")
     arOrdSheetCustomerList = DisplayReport("OrdSheetCustomerList")
     set DisplayReport= nothing

    'If delType = "1" Then
	'	delType = "Morning"
	'ElseIf delType = "2" Then
	'	delType = "Noon"
	'ElseIf delType = "3" Then
	'	delType = "Evening"
	'End If

if IsArray(arCustomerList) And IsArray(arOrdSheetCustomerList) Then
	intP = 1
	intRow = 1
	For intI = 0 To ubound(arCustomerList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		if intI = 0 Or intRow = 1 Then
			intRow = intRow + 1
		End if

		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetCustomerList, 2)
			if (arCustomerList(0, intI) = arOrdSheetCustomerList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
						intRow = intRow + 1
					End if
				End if
				if intN = 0 Then
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetCustomerList, 2) Then
					intTotal = 0
					intRow = intRow + 1
				End if
				if intI <= ubound(arCustomerList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arCustomerList, 2) Then
						intP = intP + 1
					End if
				End if
				intQ = 0				
			End if
		Next
		intD = intD + 1
	Next
Else
	intP = 1
End if

intN = 1
intJ = 0
intQ = 0
intF = 0
intD = 0
intTotal=0
if IsArray(arOrdSheetCustomerDetails) and IsArray(arCustomerList) And IsArray(arOrdSheetCustomerList) Then
%>
<DIV CLASS="PAGEBREAK"></DIV>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
<td>
<p align="center"><font size="3"><b>ORDERING SHEET BY CUSTOMER</b></font></p>
</td>
</tr>
<tr>
<td>
<br><br>
<b><%= arOrdSheetCustomerDetails(0, 0) %><br>
To the attention of: <%= arOrdSheetCustomerDetails(1, 0) %><br>
By fax number: <%= arOrdSheetCustomerDetails(2, 0) %></b><br><br>
Date of report: <%= Day(Now()) & "/" & Month(Now()) & "/" & Year(Now()) %><br>
To be produced and ready to be delivered on <%= Day(FormatDateTime(delDate, 2)) & "/" & Month(FormatDateTime(delDate, 2)) & "/" & Year(FormatDateTime(delDate, 2)) %> <%if deltype="Morning" then%> before 05:00 AM<%end if%><br><br>
Number of pages for this report: <%= intP %>
<br><br>
</td>
</tr>
</table><%
if IsArray(arCustomerList) And IsArray(arOrdSheetCustomerList) Then
	currentPage = 1
	intRow = 1
	For intI = 0 To ubound(arCustomerList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		if intI = 0 Or intRow = 1 Then%>
			<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
				<tr>
				<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
				</tr>
			</table><%
			intRow = intRow + 1
			currentPage = currentPage + 1
		End if
%>
<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
	<tr>
	<td height="20"><b>Customer Code: <%= arCustomerList(0, intI) %></b></td>
	<td height="20"><b>Customer Name: <%= arCustomerList(1, intI) %></b></td>
	<td height="20"><b>Van No.: <%= arCustomerList(2, intI) %></b></td>
	</tr>
</table><%
		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetCustomerList, 2)
			if (arCustomerList(0, intI) = arOrdSheetCustomerList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
					%>
						<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
							<tr>
							<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
							</tr>
						</table><%
						intRow = intRow + 1
						currentPage = currentPage + 1
					End if
%>
<table align="center" border="1" width="90%" cellspacing="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000" cellpadding="0"><%
				End if
				if intN = 0 Then
%>
	<tr>
	<td width="11%" height="20"><b>Product code</b></td>
	<td width="56%" height="20"><b>Product name</b></td>
	<td width="8%" height="20"><b>Quantity</b></td>
	</tr><%
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
	%>
	<tr>
	<td width="11%" height="20"><%= arOrdSheetCustomerList(4, intJ) %></td>
	<td width="56%" height="20"><%= arOrdSheetCustomerList(5, intJ) %></td>
	<td width="8%" height="20"><%= arOrdSheetCustomerList(6, intJ) %></td>
	</tr><%
				intTotal = intTotal + arOrdSheetCustomerList(6, intJ)
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetCustomerList, 2) Then
%>
	<tr>
	<td width="67%" height="20" colspan="2" align="right"><b>Total&nbsp; </b></td>
	<td width="8%" height="20"><b><%= intTotal %>&nbsp;</b></td>
	</tr><%
					intTotal = 0
					intRow = intRow + 1
				End if
%>
</table><%
				if intI <= ubound(arCustomerList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arCustomerList, 2) Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
					End if
				End if
				intQ = 0
			End if
		Next
		intD = intD + 1
	Next
	'curpage=clng(curpage)+1
End if
%>
<p>&nbsp;</p><%
End if
end if
next
'next
end if
If IsArray(arOrdSheetCustomerDetails) Then erase arOrdSheetCustomerDetails
If IsArray(arCustomerList) Then erase arCustomerList
If IsArray(arOrdSheetCustomerList) Then erase arOrdSheetCustomerList
set DisplayReport= nothing
set object = nothing
%>

<%

'ordering sheet by van
'--------------------

if isarray(vFacarray) then
  set object = Server.CreateObject("bakery.daily")
  'for curtno=1 to tno
    for k=0 to ubound(vFacarray,2)
      'if curtno=1 then
      '  deltype=1
      'elseif curtno=2 then
      '  deltype=2
      'else
      '  deltype=3
      'end if
      deltype="1,2,3"
    
      intN = 0
      intJ = 0
      intQ = 0
      intP = 1
      intTotal=0
      intD = 0
   	  intRow = 1
      facilityNo = vFacarray(0,k)
      if clng(facilityNo) = 30 or clng(facilityNo) = 31 or clng(facilityNo) = 32  or clng(facilityNo) = 33 then
      set DisplayReport= object.Display_OrderingSheetByVans(facilityNo, delType, delDate)
arOrdSheetVansDetails = DisplayReport("OrdSheetVansDetails")
arVansList = DisplayReport("VansList")
arOrdSheetVansList = DisplayReport("OrdSheetVansList")
set DisplayReport= nothing

	'If delType = "1" Then
	'	delType = "Morning"
	'ElseIf delType = "2" Then
	'	delType = "Noon"
	'ElseIf delType = "3" Then
	'	delType = "Evening"
	'End If

if IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
	intP = 1
	intRow = 1
	For intI = 0 To ubound(arVansList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		if intI = 0 Or intRow = 1 Then
			intRow = intRow + 1
		End if

		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetVansList, 2)
			if (arVansList(0, intI) = arOrdSheetVansList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
						intRow = intRow + 1
					End if
				End if
				if intN = 0 Then
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
			End if

			if intJ = ubound(arOrdSheetVansList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetVansList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetVansList, 2) Then
					intTotal = 0
					intRow = intRow + 1
				End if
				if intI <= ubound(arVansList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arVansList, 2) Then
						intP = intP + 1
					End if
				End if
				intQ = 0				
			End if
		Next
		intD = intD + 1
	Next
Else
	intP = 1
End if

intN = 1
intJ = 0
intQ = 0
intF = 0
intD = 0
intTotal=0
if IsArray(arOrdSheetVansDetails) and IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
%>
<DIV CLASS="PAGEBREAK"></DIV>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
<td>
<p align="center"><font size="3"><b>ORDERING SHEET BY VANS</b></font></p>
</td>
</tr>
<tr>
<td>
<br><br>
<b><%= arOrdSheetVansDetails(0, 0) %><br>
To the attention of: <%= arOrdSheetVansDetails(1, 0) %><br>
By fax number: <%= arOrdSheetVansDetails(2, 0) %></b><br><br>
Date of report: <%= Day(Now()) & "/" & Month(Now()) & "/" & Year(Now()) %><br>
To be produced and  delivered on <%= Day(FormatDateTime(delDate, 2)) & "/" & Month(FormatDateTime(delDate, 2)) & "/" & Year(FormatDateTime(delDate, 2)) %> <%if deltype="Morning" then%> before 05:00 AM<%end if%><br><br>
Number of pages for this report: <%= intP %>
<br><br>
</td>
</tr>
</table><%
if IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
	currentPage = 1
	intRow = 1
	For intI = 0 To ubound(arVansList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		if intI = 0 Or intRow = 1 Then%>
	<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
		<tr>
		<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
		</tr>
	</table><%
			intRow = intRow + 1
			currentPage = currentPage + 1
		End if
%>
<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
	<tr>
	<td width="75%" height="20"><b>Order for Van <%= arVansList(0, intI) %></b></td>
	</tr>
</table><%
		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetVansList, 2)
			if (arVansList(0, intI) = arOrdSheetVansList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
					%>
						<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
							<tr>
							<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
							</tr>
						</table><%
						intRow = intRow + 1
						currentPage = currentPage + 1
					End if
%>
<table align="center" border="1" width="90%" cellspacing="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000" cellpadding="0"><%
				End if
				if intN = 0 Then
%>
	<tr>
	<td width="12%" height="20"><b>Product code</b></td>
	<td width="54%" height="20"><b>Product name</b></td>
	<td width="9%" height="20"><b>Quantity</b></td>
	</tr><%
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
	%>
	<tr>
	<td width="12%" height="20"><%= arOrdSheetVansList(2, intJ) %></td>
	<td width="54%" height="20"><%= arOrdSheetVansList(3, intJ) %></td>
	<td width="9%" height="20"><%= arOrdSheetVansList(4, intJ) %></td>
	</tr><%
				intTotal = intTotal + arOrdSheetVansList(4, intJ)
			End if

			if intJ = ubound(arOrdSheetVansList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetVansList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetVansList, 2) Then
%>
	<tr>
	<td width="66%" height="20" colspan="2" align="right"><b>Total&nbsp; </b></td>
	<td width="9%" height="20"><b><%= intTotal %>&nbsp;</b></td>
	</tr><%
					intTotal = 0
					intRow = intRow + 1
				End if
%>
</table><%
				if intI <= ubound(arVansList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arVansList, 2) Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
					End if
				End if
				intQ = 0
			End if
		Next
		intD = intD + 1
	Next
End if
%>
<p>&nbsp;</p><%
End if
end if
next
'next
end if
If IsArray(arOrdSheetVansDetails) Then erase arOrdSheetVansDetails
If IsArray(arVansList) Then erase arVansList
If IsArray(arOrdSheetVansList) Then erase arOrdSheetVansList
%>

<%
set DisplayReport= nothing
set object = nothing
%>

<%
'Packing sheet report
'-------------------

if isarray(vFacarray) then
	set object = Server.CreateObject("bakery.daily")
	for k=0 to ubound(vFacarray,2)
		facility = vFacarray(0,k)
		'Response.Write "(" & facility & ")"
		for curtno=1 to tno
			if curtno=1 then
				dtype="Morning"
			elseif curtno=2 then
				dtype="Noon"
			else
				dtype="Evening"
			end if
			intN=0
			'Response.Write "{" & curtno & "}"
			if clng(facility) = 11 or clng(facility) = 18 or clng(facility) = 15 or facility = "11,12,90,99" then
				if facility = 11 or facility = "11,12,90,99" then 
					facility = "11,12,90,99"
					if curtno = 1 then
						dtype = "Morning"
					elseif clng(curtno) = 2 then
						dtype = "Noon"
					else
					  dtype = "Evening"
					end if
				elseif facility = 18 or facility = 15 then
					if curtno = 1 then
						dtype = "Morning"
					elseif clng(curtno) = 2 then
						dtype = "Noon"
					else
					  dtype = "Evening"
					end if
				end if
				'Response.Write "[" & curtno & "," & facility & "," & dtype & "]"
				
				set col1= object.DailyPackingSheetReport(deldate,facility,dtype)
				vRecArray = col1("DailyPackingSheet")
				vtotcustarray = col1("TotalCustomers")
				if isarray(vtotcustarray) then
					totpages = ubound(vtotcustarray,2)+1
				else
					totpages = 0
				end if
				curpage=0
	    
				Facilityrec = col1("Facility")
				if isarray(facilityrec) then
					if facility = "11,12,90,99" then
						facilityname = "Stanmore"
					else
						facilityname = facilityrec(0,0)
					end if
					attn = facilityrec(1,0)
					fax = facilityrec(2,0)
				else
					facilityname = "Unknown"
					attn = "Unknown"
					fax = "Unknown"
				end if
				set col1= nothing

				'Page Count
				'if facility = "11,12,90,99" Then
				'	mFontSize = 14
				'	mFooterSize =5
				'	mFooterFontSize = 5
				'	mExtraLine = 12
				'Else
					mFontSize = 8
					mFooterSize =3
					mFooterFontSize = 3
					mExtraLine = 10
				'End if


				'Page Count

				'mLine = 18
				'if mFontSize = 14 Then
				'	mLine = 16
				'Else
					mLine = 8
				'End if
				if isarray(vrecarray) then
					mPage = 1
					currec=0
					totqty=0
					curcno = vrecarray(0,0)
					curvan = vrecarray(5,0)
					curpage=1	  
					for i = 0 to ubound(vRecArray,2)
						currec = clng(currec) + 1
						if clng(currec) = 1 then
							if mFontSize = 14 Then
								mLine = mline + 4
							else
								mLine = mline + 3
							end if	
							if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
								mPage = mPage + 1						
								mLine = 0
							End if
						end if
						if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then
							if mFontSize = 14 Then
								mLine = mLine + 2
							else
								mLine = mLine + 1
							end if	
						end if
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0
						End if          
						if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
							curvan = vrecarray(5,i)
							curcno = vrecarray(0,i)
							i=clng(i)-1
							if mFontSize = 14 Then
								mLine = mLine +4           
							else
								mLine = mLine +3           
							end if	
						
							if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then
								mPage = mPage + 1
								mLine = 0	
							else
								if curvan <> vrecarray(5,i)   Then										
									mPage = mPage + 1
									mLine = 0
								End if
							End if
							if i < ubound(vrecarray,2) then
							end if            
							curpage = curpage + 1
							currec = 0
							totqty=0
						end if
					next
	    
				else
					mPage = 1
				end if
				i = 0
if isarray(vrecarray) then
%>
<DIV CLASS="PAGEBREAK"></DIV>
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: <%=mFontSize%>pt">
  <tr>
    <td width="100%">
    <center><b><font size="3">PACKING SHEET - <%=dtype%><br></font></b></center>
      <center><b><font size="3">(divided to Van and Customers)<br></font></b></center>
      <table border="0" width="90%" style="font-family: Verdana; font-size: <%=mFontSize%>pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><br>
            <br>
            <b>Date of report:</b> <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            <b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><BR>
            <b>Facility:</b> <%=Facilityname %><BR>
            <b>To the Attention of:</b> <%=attn%><BR>
            <b>By Fax Number:</b> <%=fax%><br>
            </td>
        </tr>
        <tr>
          <td>Number of pages for this report: <%=mPage%></td>
          <td align="right">Page 1 of <%=mPage%><br></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
</center>
</div><%
if mFontSize = 14 Then
	mLine = 16
Else
	mLine = 8
End if%>

<div align="center">
<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
	  if isarray(vrecarray) then
		mTotalPage = mPage
		mPage = 1		
	    currec=0
	    totqty=0
	    curcno = vrecarray(0,0)
	    curvan = vrecarray(5,0)
	    curpage=1	  
        for i = 0 to ubound(vRecArray,2)
          currec = clng(currec) + 1
          if clng(currec) = 1 then%>
            <tr>
            <td colspan="3">
              <b>Van : <%=vrecarray(5,i)%></b><br>
              <b>Customer : <%=vrecarray(0,i)%> - <%=vrecarray(1,i)%></b>
            </td>            
            </tr>
            <tr>            
            <td align="left" bgcolor="#CCCCCC" height="20" width="85%"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Qty</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="5%"><b>SID</b></td>
            </tr><%
            if mFontSize = 14 Then
							mLine = mline + 4
						else
							mLine = mline + 3
						end if	
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td colspan="3" align="right"><%
								 Response.Write "Page "  & mPage & " of " & mTotalPage%>
								</td>
							</tr><%
						End if
           end if
           if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then%>
          <tr>            
            <td align="left" width="85%"><%=left(vrecarray(3,i),50)%>&nbsp;</td>
            <td align="center" width="10%"><%=vrecarray(4,i)%>&nbsp;</td><%
            totqty = clng(totqty) + vrecarray(4,i)%>
            <td align="center" width="5%">&nbsp;</td>
          </tr><%
						if mFontSize = 14 Then
							mLine = mLine + 2
						else
							mLine = mLine + 1
						end if	
          end if
          if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
						mPage = mPage + 1						
						mLine = 0%>		
						</table>						
						<br class="page" />						
						<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td colspan="3" align="right"><%	
								 Response.Write "Page "  & mPage & " of " & mTotalPage%>
								</td>
							</tr><%
          End if
          
		  if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
		    curvan = vrecarray(5,i)
		    curcno = vrecarray(0,i)
		    i=clng(i)-1%>
		    <!--<tr>
		      <td width="85%">
		        <p align="right"><b><font size="<%=mFooterFontSize%>" face="Verdana">Total&nbsp;&nbsp; </font></b>
              </td>
              <td width="10%" align="center" >
                <p align="center"><font size="<%=mFooterFontSize%>" face="Verdana"><%=totqty%></font></p>
              </td>
              <td width="5%">&nbsp;</td>
            </tr>-->
		    </table>
						<br>
            <br><%
            if mFontSize = 14 Then
							mLine = mLine +4           
						else
							mLine = mLine +3           
						end if	
         if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then 						
         
						mPage = mPage + 1
						mLine = 0%>								
						<br class="page" /><%						
		  else
			if curvan <> vrecarray(5,i)   Then										
				mPage = mPage + 1
				mLine = 0%>													
				<br class="page" /><%
			End if				
          End if
          
            if i < ubound(vrecarray,2) then%>
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
							if mLine = 0 Then %>
								<tr>
									<td colspan="3" align="right"><%
									 Response.Write "Page "  & mPage & " of " & mTotalPage%>
									</td>
								</tr><%						
							end if         
            end if   
            
		    curpage = curpage + 1
		    currec = 0
		    totqty=0
		  end if
	    next%>
	    <!--<tr>
		  <td  width="85%">
		    <p align="right"><b><font size="<%=mFooterFontSize%>" face="Verdana">Total&nbsp;&nbsp; </font></b>
          </td>
          <td width="10%" align="center">
            <p align="center"><font size="<%=mFooterFontSize%>" face="Verdana"><%=totqty%></font></p>
          </td>
          <td width="5%">&nbsp;</td>
        </tr>--><%
      else%>
        <tr>
        <td width="100%" colspan = "4" height="20">Sorry no items found</td>
        </tr><%
	  end if%>
      </table>
</div><%
end if
end if
next
next
end if%>

<%
set col1= nothing
set object = nothing
%>

<%
'Delivery Sheet
'--------------

if isarray(vvecarray) then
  set object = Server.CreateObject("bakery.daily")
  for curtno=1 to tno
    if curtno=1 then
      dtype="Morning"
    elseif curtno=2 then
      dtype="Noon"
    else
      dtype="Evening"
    end if
    for k=0 to ubound(vvecarray,2)
      van = vvecarray(0,k)

      set col1= object.DailyDeliverySheet(deldate,van,dtype)
vRecArray = col1("DeliverySheet")
vtotcustarray = col1("TotalCustomers")
if isarray(vtotcustarray) then
  totpages=ubound(vtotcustarray,2)+1
else
  totpages=0
end if
curpage=0
    
drivername = col1("Driver")
vanname=col1("Van")
set col1= nothing

mLine = 10	
	if isarray(vrecarray) then
		mPage = 1
		currec=0
		totqty=0
		curcno = vrecarray(0,0)
		curfac=0
		curpage=1
    for i = 0 to ubound(vRecArray,2)
			currec = clng(currec) + 1
	    if clng(currec) = 1 then
				mLine = mline + 4
				if mLine >= PrintPgSize+4 and i < ubound(vrecarray,2) Then 							
					mPage = mPage + 1						
					mLine = 0
				End if
			End if
			
	    if clng(curcno) = vrecarray(0,i) then
				mLine = mLine + 1
	    end if          
	    
	    if mLine >= PrintPgSize+4 and i < ubound(vrecarray,2) Then 
				mPage = mPage + 1						
				mLine = 1
	    End if
	          
	    if clng(curcno) <> vrecarray(0,i) then
				curcno = vrecarray(0,i)
			  i=clng(i)-1
	      mLine = mLine +3
				if mLine >= PrintPgSize+4-4 and i < ubound(vrecarray,2) Then 
					mPage = mPage + 1
					mLine = 0
				End if				
	        curpage = clng(curpage) + 1
			    currec = 0
			    totqty=0
			 end if
		  next		  
	Else
		mPage = 1	  
	end if

if isarray(vrecarray) then
%>
<DIV CLASS="PAGEBREAK"></DIV>
<div align="center">
<center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 10pt">
  <tr>
    <td width="100%">
      <center><b><font size="3">DELIVERY SHEET - <%=dtype%><br></font></b></center><br>
      <center><b><font size="3">(divided to Van and Customers)<br></font></b></center>
      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><br>
            <br>
            <b>Date of report:</b>  <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            <b>To be produced and delivered on</b> <%=deldate%> (<%=dtype%>)<br>
            <b>To the attention of: </b><%=vanname%><br>
            <b>Van <%=van%></b><br>
            </td>
        </tr>
        <tr>
          <td>Number of pages for this report: <%=mPage%><Br></td>
          <td align="right">Page 1 of <%=mPage%></td>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table>
</center>
</div>
	<%mLine = 10%>
	<div align="center">
	<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 10pt" align="center"><%    
		  if isarray(vrecarray) then
		  mTotalPage = mPage
				mPage = 1
		    currec=0
		    totqty=0
		    curcno = vrecarray(0,0)
		    curfac=0
		    curpage=1
	        for i = 0 to ubound(vRecArray,2)
	          currec = clng(currec) + 1
	          if clng(currec) = 1 then%>
	            <tr>
	            <td colspan="3" width="80%">              
	              <b>Customer : <%=vrecarray(0,i)%> - <%=vrecarray(1,i)%></b>
	            </td>
	            <td align="right" colspan="2" width="20%"></td>
	            </tr>
	            <tr>
	            <td align="left" bgcolor="#CCCCCC" height="20" width="25%"><b>Facility</b></td>
	            <td align="center" bgcolor="#CCCCCC" height="20" width="15%"><b>Product code</b></td>
	            <td align="left" bgcolor="#CCCCCC" height="20" width="40%"><b>Product name</b></td>
	            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Quantity</b></td>
	            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Short in Delivery</b></td>
	            </tr><%
	            mLine = mline + 4
							if mLine >= PrintPgSize+4 and i < ubound(vrecarray,2) Then 							
								mPage = mPage + 1						
								mLine = 0%>		
								</table>						
								<br class="page" />						
								<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 10pt" align="center">
								<tr>
									<td colspan="5" align="right"><%	
										Response.Write "Page "  & mPage & " of " & mTotalPage%>
									</td>
								</tr><%
							End if
	           end if
	           if clng(curcno) = vrecarray(0,i) then%>
	            <tr>
	            <td align="left" width="25%"><%=left(vrecarray(3,i),20)%><%
	              totqty = clng(totqty) + vrecarray(6,i)%>
	             &nbsp;</td>
	            <td align="center" width="15%"><%=vrecarray(4,i)%>&nbsp;</td>
	            <td align="left" width="40%"><%=vrecarray(5,i)%>&nbsp;</td>
	            <td align="center" width="10%"><%=vrecarray(6,i)%>&nbsp;</td>
	            <td align="center" width="10%">&nbsp;</td>
	            </tr><%
	            mLine = mLine + 1
	          end if          
	          if mLine >= PrintPgSize+4 and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 10pt" align="center">
							<tr>
									<td colspan="5" align="right"><%	
										Response.Write "Page "  & mPage & " of " & mTotalPage%>
									</td>
								</tr><%
							mline = 1	
	          End if
	          
	          
			  if clng(curcno) <> vrecarray(0,i) then
			    curcno = vrecarray(0,i)
			    i=clng(i)-1%>
	            <tr>
	            <td align="center" height="20" colspan="3"  width="80%">
	              <p align="right"><b><font size="2" face="Verdana">Total&nbsp;&nbsp; </font></b>
	            </td>
	            <td align="center" height="20"  width="10%">
	              <p align="center"><font size="2" face="Verdana"  width="10%"><%=totqty%></font></p>
	            </td>
	            <td align="center" height="20"  width="10%">
	              <p align="center"><font size="2" face="Verdana"  width="10%">&nbsp;</font></p>
	            </td>
	            </tr>            
			    </table>
	            <br>
	            <br><%
	            mLine = mLine +3
							if mLine >= PrintPgSize+4-4 and i < ubound(vrecarray,2) Then 
								mPage = mPage + 1
								mLine = 0%>								
								<br class="page" /><%						
							End if
	            if i < ubound(vrecarray,2) then%>
								<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 10pt" align="center"><%
								if mLine = 0 then%>																
									<tr>
										<td colspan="5" align="right"><%	
											Response.Write "Page "  & mPage & " of " & mTotalPage%>
										</td>
									</tr><%
								End if															                
	            end if      
			    curpage = clng(curpage) + 1
			    currec = 0
			    totqty=0
			  end if
		    next
		    if clng(currec) > 0 then%>
		      <tr>
	            <td align="center" height="20" colspan="3"  width="80%">
	              <p align="right"><b><font size="2" face="Verdana">Total&nbsp;&nbsp; </font></b>
	            </td>
	            <td align="center" height="20"  width="10%">
	              <p align="center"><font size="2" face="Verdana"><%=totqty%></font></p>
	            </td>
	            <td align="center" height="20"  width="10%">
	              <p align="center"><font size="2" face="Verdana">&nbsp;</font></p>
	            </td>
	            </tr> <%
			  end if%>		    
			    <tr><%
	      else%>
	        <tr>
	        <td width="100%" colspan = "4" height="20">Sorry no items found</td>
	        </tr><%
		  end if%>
      </table>
</div>
<%
end if
next
next
end if
set object=nothing%>
</body>
</html>