<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim intI
Dim intJ
Dim intN
Dim intRow
Dim intF
Dim intTotal
intN = 0
intTotal = 0
Response.Buffer
strdate=request("sdate")
strmonth=request("smonth")
stryear=request("syear")

if (strdate<10) then
	strdate="0" & strdate
end if
if (strmonth<10) then
	strmonth="0" & strmonth
end if

deldate=strdate & "/" & strmonth & "/" & stryear
csvfile=strdate & "_" & strmonth & "_" & stryear

'28/7/2015'
strfacility=21
dtype="All"
Dim objFSO,oFiles  
Dim sFileName,i  
 
sFileName = "exportorderfiles/OrderingSheetCocomaya_" & csvfile & ".csv"  
'*** Create Object ***'  
Set objFSO = CreateObject("Scripting.FileSystemObject")  
set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
'*** Create Text Files ***'  
Set oFiles = objFSO.CreateTextFile(Server.MapPath(sFileName), 2)  

set DisplayDailyFacilityOrderSheet= object.DisplayReportViewDailyReportFacilityOrderSheet(deldate,strfacility,dtype)
vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
set DisplayDailyFacilityOrderSheet= nothing
set object = nothing
If isArray(vRecArray) Then
	oFiles.WriteLine("Product Code,Product Name,Order Qty, Qty Done")
	for i = 0 to ubound(vRecArray, 2)
	strline=vRecArray(0,i) & "," & vRecArray(1,i) & "," & vRecArray(2,i) & "," 
	oFiles.WriteLine(strline)
	next
end if
%>