<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim obj,ObjInventory
Dim arDough,arDoughInt,arTwoStageMixingDoughInt
Dim vDno
Dim xM,i,vQtyIndex 
Dim mTag
Dim vNoIngredient
Dim arUpdate
Dim arSecondMixUpdate
Dim varCount
Dim varSecondMixCount
Dim objResult
Dim strDoughName
Dim vqty
Dim vsqty

vNoIngredient = 15

if Trim(Request.QueryString("Dno")) <> "" Then
	vDno = Request.QueryString("Dno") 
Else
	vDno = Request.Form("Dno")
End if	

Set obj = Server.CreateObject("Bakery.Inventory")
obj.SetEnvironment(strconnection)
if Trim(Request.Form("Update")) <> "" Then
	stop
	strDoughName = Request.Form("doughname")
	strDTNo = Request.Form("DoughType")
	'if Request.Form("SecondMixCategory") <> "" then
		strIsSecondMixCategory = Request.Form("SecondMixCategory")
	'else
	'	strIsSecondMixCategory = Request.Form("hdnSecondMixCategory")
	'end if
	strMainDoughNo = Request.Form("MainDough")
	strMainDoughWeight = Request.Form("SecondMixCategoryWeight")
	strMarginOfError = Request.Form("marginoferror")
	if strMarginOfError = "" then
		strMarginOfError = "0"
	end if
	vSecondMixCategory = Request.Form("SecondMixCategory")
	if vSecondMixCategory = "0" then
		vMixingMethod = "1"
	elseif vSecondMixCategory = "1" then
		vMixingMethod = "2"
	elseif vSecondMixCategory = "2" then
		vMixingMethod = "3"
	end if
	
	'Ingredient array update
	varCount = 0
	Redim arUpdate(1,varCount)
	For i = 1 to vNoIngredient
		if Trim(Request.Form("I" & i)) <> ""  Then
			vqty = Trim(Request.Form("qty" & i))
			if Trim(vqty) <> "" then
				if Not IsNumeric(vqty) Then
					vqty = 0
				Elseif vqty < 0 Then
					vqty = 0
				End if
			Else
				vqty = 0 
			End if
					
			Redim preserve arUpdate(1,varCount)
			arUpdate(0,varCount) = Trim(Request.Form("I" & i))
			arUpdate(1,varCount) = vqty
			varCount = varCount + 1					
		End if
	Next
	
	if vMixingMethod = "3" then
		varSecondMixCount = 0
		Redim arSecondMixUpdate(1,varSecondMixCount)
		For i = 1 to vNoIngredient
			if Trim(Request.Form("I" & i)) <> ""  Then
				vsqty = Trim(Request.Form("sqty" & i))
				if Trim(vsqty) <> "" then
					if Not IsNumeric(vsqty) Then
						vsqty = 0
					Elseif vsqty < 0 Then
						vsqty = 0
					End if
				Else
					vsqty = 0 
				End if
						
				Redim preserve arSecondMixUpdate(1,varSecondMixCount)
				arSecondMixUpdate(0,varSecondMixCount) = Trim(Request.Form("I" & i))
				arSecondMixUpdate(1,varSecondMixCount) = vsqty
				varSecondMixCount = varSecondMixCount + 1					
			End if
		Next
	end if
	
	if (strIsSecondMixCategory="0") then
		strMainDoughNo="0"
		strMainDoughWeight="0"
	end if
	
	'objResult = obj.Update_Dough(vDno,strDoughName,arUpdate,strDTNo,strIsSecondMixCategory,strMainDoughNo,strMainDoughWeight)
	if vMixingMethod = "3" then
		objResult = obj.Update_TwoStageMixing_Dough(vDno,strDoughName,arUpdate,arSecondMixUpdate,strDTNo,"0","0","0",vMixingMethod,strMarginOfError)
	else
		objResult = obj.Update_Dough_New(vDno,strDoughName,arUpdate,strDTNo,strIsSecondMixCategory,strMainDoughNo,strMainDoughWeight,vMixingMethod,strMarginOfError)
	end if
End if


set ObjInventory = obj.Display_DoughtDetails(vDno)	
arDough =  ObjInventory("Dough") 
strSecondMixCategory=arDough(2,0)
vDoughMixingMethod = arDough(5,0)
strDoughType=arDough(3,0)
strMainDoughWeight=arDough(4,0)

if (strSecondMixCategory) then
	strSecondMixCategory="1"
else
	strSecondMixCategory="0"
end if
arDoughInt =  ObjInventory("Ingredients")

if vDoughMixingMethod = 3 then
	set ObjInventory = obj.GetTwoStageDoughIngredient(vDno)
	arTwoStageMixingDoughInt = ObjInventory("Ingredients")
end if

Set obj = Nothing
Set ObjInventory = Nothing

Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.ListDoughType()
vDoughTypearray =  detail("DoughType") 

set detail = object.DisplayDoughType()	
vDougharray =  detail("DoughType")

set detail = Nothing
set object = Nothing
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{
	var vNoIngredient = <%=vNoIngredient%>
	
	for(i=1;i<=vNoIngredient;i++){
		for(xm=i;xm<vNoIngredient;xm++){
			var dupIndex = Number(xm)+1;
			if(eval("theForm.I" + i + ".selectedIndex") >0){	
				if(eval("theForm.I" + i + ".selectedIndex") == eval("theForm.I" + dupIndex + ".selectedIndex")){
					alert("Duplicate Ingredients " + dupIndex);
					eval("theForm.I" + dupIndex + ".focus()");
					return (false);}
			}			
		}		
	}

  if (theForm.doughname.value == "")
  {
    alert("Please enter a value for the \"Dough Name\" field.");
    theForm.doughname.focus();
    return (false);
  }
  if (theForm.DoughType.value == "")
  {
    alert("Please enter a value for the \"Dough Type\" field.");
    theForm.DoughType.focus();
    return (false);
  }
  if (theForm.SecondMixCategory[1].checked)
  {
  		  if (theForm.MainDough.value == "")
		  {
			alert("Please enter a value for the \"Main Dough\" field.");
			theForm.MainDough.focus();
			return (false);
		  }
		  
		  if (theForm.SecondMixCategoryWeight.value == "")
		  {
			alert("Please enter a value for the \"Amount of main dough to create 1 kilo of second mix\" field.");
			theForm.SecondMixCategoryWeight.focus();
			return (false);
		  }
  
  }
  
  if (theForm.I1.selectedIndex < 0)
  {
    alert("Please select one of the \"I1\" options.");
    theForm.I1.focus();
    return (false);
  }

  if (theForm.qty1.value == "")
  {
    alert("Please enter a value for the \"qty1\" field.");
    theForm.qty1.focus();
    return (false);
  }
  
  dTotalValue=0;
  dTotalValue = document.getElementById("TotalValue").innerHTML;
  
  if (dTotalValue > 1)
  {
  	alert("Please check entered value for quantities as total exceeds 1 kg.");
	return (false);
  }
  
  if(confirm("Are you sure you want to update this dough")){
	  return (true);
  }
  else
  {
      return (false);
  }  

}

function ShowSecondMix(chk)
{
	if (chk.value=="1")
	{
		document.getElementById("trMainDough").style.display = "";
		document.getElementById("trSecondMixCategoryWeight").style.display = "";
	}
	else
	{
		document.getElementById("trMainDough").style.display = "none";
		document.getElementById("trSecondMixCategoryWeight").style.display = "none";
	}
}

function TotalValueLoad()
{
	document.getElementById("TotalValueKg").innerHTML="kg";
	//Lets show second mix total
	//if (document.getElementById("STotalValueKg") != null)
	//	document.getElementById("STotalValueKg").innerHTML="kg";
	
	dTotalValue=0;
	//total of second mix ingredient
	dSTotalValue=0;
	
	boolSecondMixCategory=document.FrontPage_Form1.SecondMixCategory[1].checked;
	
	for (i=1;i<=15;i++)
	{
		vqty="qty"+i
		x=document.getElementById(vqty).value;
		if (x!="")
		{
			dTotalValue=parseFloat(x)+parseFloat(dTotalValue)
			dTotalValue=Math.round(dTotalValue*Math.pow(10,4))/Math.pow(10,4);
		}
		
		//lets calculate second mix ingredient total
		if (document.getElementById("STotalValueKg") != null)
		{
			vsqty="sqty"+i
			sx=document.getElementById(vsqty).value;
			if (sx!="")
			{
				dSTotalValue=parseFloat(sx)+parseFloat(dSTotalValue)
				dSTotalValue=Math.round(dSTotalValue*Math.pow(10,4))/Math.pow(10,4);
			}
		}
	}
	
	if (boolSecondMixCategory)
	{
		if (document.getElementById("SecondMixCategoryWeight").value!="")
			dTotalValue+=parseFloat(document.getElementById("SecondMixCategoryWeight").value);
	}
	
	document.getElementById("TotalValue").innerHTML= dTotalValue + dSTotalValue;
	
	//show total value of ingredients for second mix
	//if (document.getElementById("STotalValueKg") != null)
	//{
	//	document.getElementById("STotalValue").innerHTML=dSTotalValue;
	//}
}

//-->
</script>
</head>

<body topmargin="0" leftmargin="0" <%if objResult="" then%> <%end if%> bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->

<form method="post" name="FrontPage_Form1" action="Editinventory_doughs_new.asp" onSubmit="return FrontPage_Form1_Validator(this)">

<table border="0" align="center" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Edit Dough<br>
      &nbsp;</font></b>
      
		    <%
			if objResult = "Fail" then
				Response.Write "<br><font size=""3"" color=""#FF0000""><b>Dough name already exists</b></font><br><br>"
			elseif objResult = "OK" then
				Response.Write "<br><font size=""3"" color=""#16A23D""><b>Dough updated successfully</b></font><br><br>"		
			end if
			%>
			
		<%if objResult ="" then%>		
	
        <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0">
        <tr>
          <td width="230"><b>Dough Name</b></td>
          <td>
          			<%
          			if IsArray(arDough) Then%>          
						<input type="text" name="doughname" value="<%=arDough(0,0)%>" size="35" style="font-family: Verdana; font-size: 8pt" maxlength = "50"><%
					Else%>
						<input type="text" name="doughname" size="35" style="font-family: Verdana; font-size: 8pt" maxlength = "50"><%
					End if%>        </td>
		</tr>
        <tr height="25">          
          <td><b>Dough Type&nbsp;</b></td>
          <td><select size="1" name="DoughType" style="font-family: Verdana; font-size: 8pt">
            <option value = "">Select</option>
            <%
			if IsArray(vDoughTypearray) Then
				for i = 0 to ubound(vDoughTypearray,2)%>
            <option value="<%=vDoughTypearray(0,i)%>" <%if arDough(1,0)<>"" then%><%if arDough(1,0)= vDoughTypearray(0,i) then response.write " Selected"%><%end if%>><%=vDoughTypearray(1,i)%></option>
            <%
			   next                 
			End if
			%>
          </select></td>
        </tr>
		<tr height="25">
			<td><b>Margin of Error&nbsp;</b></td>
			<td>
				<input type="text" name="marginoferror" value="<%=arDough(6,0)%>" size="10" style="font-family: Verdana; font-size: 8pt" maxlength = "5">
			</td>
		</tr>
		<tr height="25">
		<td><b>Dough Mixing Method&nbsp;</b></td>
		<td>
		<input type="radio" name="SecondMixCategory" id="SecondMixCategory" value="0" class="radioinput" onClick="ShowSecondMix(this);TotalValueLoad()" <%if vDoughMixingMethod = 1 then%> checked="checked" <%end if%>> Main Dough
		<input type="radio" name="SecondMixCategory" id="SecondMixCategory" value="1" class="radioinput"  onClick="ShowSecondMix(this);TotalValueLoad()" <%if vDoughMixingMethod = 2 then%> checked="checked" <%end if%>> Second Mix Category
		<input type="radio" name="SecondMixCategory" id="SecondMixCategory" value="2" class="radioinput"  onClick="ShowSecondMix(this);TotalValueLoad()" <%if vDoughMixingMethod = 3 then%> checked="checked" <%end if%>> Two Stage Mixing		</td>
		</tr>
		<tr height="25" id="trMainDough">
			<td><b>Main Dough&nbsp;</b></td>
			<td>
			<select size="1" name="MainDough" style="font-family: Verdana; font-size: 8pt">
				<option value = "">Select</option>
				<%
				if IsArray(vDougharray) Then  	
					for i = 0 to ubound(vDougharray,2)%>  				
				 <option value="<%=vDougharray(0,i)%>" <%if strDoughType<>"" then%><%if strDoughType= vDougharray(0,i) then response.write " Selected"%><%end if%>><%=vDougharray(1,i)%></option>
				  <%
				   next                 
				End if
				%>   
		    </select>			</td>
		</tr>
		<tr height="25" id="trSecondMixCategoryWeight">
		<td><strong>Amount of main dough to create<br>1 kilo of second mix&nbsp;</strong></td>
		<td><input type="text" value="<%=strMainDoughWeight%>" onChange="TotalValueLoad()" name="SecondMixCategoryWeight" id="SecondMixCategoryWeight" size="10" style="font-family: Verdana; font-size: 8pt" maxlength = "10"></td>
		</tr>
		</table>
		<br>
		<script Language="JavaScript">
		<!--
		varSecondMixCategory=<%=strSecondMixCategory%>
		if (varSecondMixCategory=="1")
		{
			document.getElementById("trMainDough").style.display = "";
			document.getElementById("trSecondMixCategoryWeight").style.display = "";
		}
		else
		{
			document.getElementById("trMainDough").style.display = "none";
			document.getElementById("trSecondMixCategoryWeight").style.display = "none";
		}
		//-->
		</script>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0">

        <tr height="22">
          <td width="28%"><b>Ingredient</b></td>
		  <%if vDoughMixingMethod = 3 then%>
          <td width="36%"><b>Quantities required to produce 1 kg of first mix</b></td>
		  <td width="36%"><b>Quantities required to produce 1 kg of second mix</b></td>
		  <%else%>
		  <td width="72%"><b>Quantities required to produce 1 kg of dough</b></td>
		  <%end if%>
        </tr><%
		if vDoughMixingMethod <> 3 then
			if IsArray(arDoughInt) Then
						dbTotal=0
						For xM = 1 to vNoIngredient
							vQtyIndex = ""
							mTag = True%>
							<tr>
								<td width="28%">
									<select size="1" name="I<%=xM%>" style="font-family: Verdana; font-size: 8pt">
										<option value="">Select</option><%
										for i = 0 to ubound(arDoughInt,2)
											if Trim(arDoughInt(2,i)) = "Y"	 and mTag = True Then		
												mTag = False
												vQtyIndex = i
												arDoughInt(2,i) = "N"%>
												<option value="<%=arDoughInt(0,i)%>" <%Response.Write "Selected"%>><%=arDoughInt(1,i)%></option><%
											Else%>
												<option value="<%=arDoughInt(0,i)%>"><%=arDoughInt(1,i)%></option><%
											End if	
						
										next%> 
									</select>
								</td>
								<td width="72%"><%
								if vQtyIndex <> ""  Then%>
									<input type="text" name="qty<%=xM%>" id="qty<%=xM%>" onChange="javacript:TotalValueLoad()"  size="20" VALUE="<%=arDoughInt(3,vQtyIndex)%>" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp; 
								Kg<%
								Else%>
									<input type="text" name="qty<%=xM%>" id="qty<%=xM%>" onChange="javacript:TotalValueLoad()"  size="20"  style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;
								Kg<%
								End if%>	
							</td>
							</tr><%	
							 dbTotal=dbTotal+formatnumber(arDoughInt(3,vQtyIndex),4)				
						Next		
			End if
			if (strSecondMixCategory="1") then
				dbTotal=dbTotal+strMainDoughWeight
			end if
		else
			if IsArray(arTwoStageMixingDoughInt) Then
						dbTotal=0
						dbSTotal=0
						For xM = 1 to vNoIngredient
							vQtyIndex = ""
							mTag = True%>
							<tr>
								<td width="28%">
									<select size="1" name="I<%=xM%>" style="font-family: Verdana; font-size: 8pt">
										<option value="">Select</option><%
										for i = 0 to ubound(arTwoStageMixingDoughInt,2)
											if Trim(arTwoStageMixingDoughInt(2,i)) = "Y"	 and mTag = True Then
												mTag = False
												vQtyIndex = i
												arTwoStageMixingDoughInt(2,i) = "N"%>
												<option value="<%=arTwoStageMixingDoughInt(0,i)%>" <%Response.Write "Selected"%>><%=arTwoStageMixingDoughInt(1,i)%></option><%
											Else%>
												<option value="<%=arTwoStageMixingDoughInt(0,i)%>"><%=arTwoStageMixingDoughInt(1,i)%></option><%
											End if	
						
										next%> 
									</select>
								</td>
								<td width="36%"><%
								if vQtyIndex <> ""  Then%>
									<input type="text" name="qty<%=xM%>" id="qty<%=xM%>" onChange="javacript:TotalValueLoad()"  size="20" VALUE="<%=arTwoStageMixingDoughInt(3,vQtyIndex)%>" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp; 
								Kg<%
								Else%>
									<input type="text" name="qty<%=xM%>" id="qty<%=xM%>" onChange="javacript:TotalValueLoad()"  size="20"  style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;
								Kg<%
								End if%>	
								</td>
								<td width="36%"><%
								if vQtyIndex <> ""  Then%>
									<input type="text" name="sqty<%=xM%>" id="sqty<%=xM%>" onChange="javacript:TotalValueLoad()"  size="20" VALUE="<%=arTwoStageMixingDoughInt(4,vQtyIndex)%>" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp; 
								Kg<%
								Else%>
									<input type="text" name="sqty<%=xM%>" id="sqty<%=xM%>" onChange="javacript:TotalValueLoad()"  size="20"  style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;
								Kg<%
								End if%>	
								</td>
							</tr><%	
							 'dbTotal=dbTotal+formatnumber(arTwoStageMixingDoughInt(3,vQtyIndex),3)
							 'dbSTotal=dbSTotal+formatnumber(arTwoStageMixingDoughInt(4,vQtyIndex),3)
							 dbTotal=dbTotal+formatnumber(arTwoStageMixingDoughInt(3,vQtyIndex),3)+formatnumber(arTwoStageMixingDoughInt(4,vQtyIndex),3)
						Next
			End if
		end if'End for [vDoughMixingMethod = 3] check
		%>
         <tr>
          <td width="28%" height="30"><strong>Total</strong></td>
		  <%if vDoughMixingMethod = 3 then%>
          <td width="36%" height="30"><strong><span id="TotalValue"><%=dbTotal%></span>&nbsp;<span id="TotalValueKg">Kg</span></strong></td>
		  <td width="36%" height="30"><strong><span id="STotalValue"><%'=dbSTotal%></span>&nbsp;<span id="STotalValueKg"></span></strong></td>
		  <%else%>
		  <td width="72%" height="30"><strong><span id="TotalValue"><%=dbTotal%></span>&nbsp;<span id="TotalValueKg">Kg</span></strong></td>
		  <%end if%>
        </tr>
         <tr>
          <td width="28%">&nbsp;</td>
          <td width="72%" colspan="<%if vDoughMixingMethod = 3 then response.Write("2") else response.Write("1") end if%>">&nbsp;
            <p><input type="submit" value="Update Dough" name="B1" style="font-family: Verdana; font-size: 8pt"></p>
          </td>
        </tr>
      </table> 
	  <%end if%>
    </td>
  </tr>
</table>

<input type="hidden" value="<%=vDno%>" name="Dno">
<input type="hidden" value="<%=strSecondMixCategory%>" name="hdnSecondMixCategory">
<input type="hidden" value="True" name="Update">

</form>


</body>

</html><%
if IsArray(arDough) Then erase arDough 
if IsArray(arDoughInt) Then erase arDoughInt 
if IsArray(arTwoStageMixingDoughInt) Then erase arTwoStageMixingDoughInt 
%>