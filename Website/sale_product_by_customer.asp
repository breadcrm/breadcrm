<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title>Sales Report - By Products By Customers</title>
<script LANGUAGE="Javascript">

function validproduct() {

if(document.frmform.product.selectedIndex == 0) {
	alert("Please select a product !"); 
	document.frmform.product.focus();
return;}

document.frmform.submit()
}

</script>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function ExportToExcelFile(){
	fromdate=document.form.txtfrom.value
	todate=document.form.txtto.value
	product=document.form.ProductID.value
	document.location="ExportToExcelFile_sale_product_by_customer.asp?product=" + product + "&fromdt=" + fromdate + "&todt=" +todate
}
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function CheckProduct(){
	if (document.form.ProductID.value=="")
	{
		alert("Please select Product");
		document.form.ProductID.focus();
		return false;
	}
	return true;
}

//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
<br>
<%
dim objBakery
dim recarray
dim retcol
dim fromdt, todt, i,Totalturnover
Dim vproduct,vNumber,vpcode,vdno


fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")
vproduct = Request.Form("product")
vpcode = replace(Request.Form("pcode"),"'","''")
vdno =  Request.Form("dno") 
vProductID = Request.Form("ProductID")
'fromdt = date()
'todt = date()
vPageSize = 20
vpagecount = Request.Form("pagecount") 

select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(vpagecount) then	
			session("Currentpage")  = session("Currentpage")+1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage")-1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select
vSessionpage = session("Currentpage")

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if
dim object,detail,vdoughtypearray
Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayDoughType()	
vdoughtypearray =  detail("DoughType") 
set detail = Nothing
set object = Nothing

set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
if isdate(fromdt) and isdate(todt) then


	set retcol = objBakery.DisplayProductsByOrderDate(fromdt,todt,vproduct,vpcode,vdno,vsessionpage , vPageSize)
	recarray = retcol("ProductsByOrderDate")
	vpagecount =   retcol("Pagecount") 
	vRecordCount = retcol("Recordcount")
	set retcol = nothing
end if

set objBakery = nothing

%>
<div align="center">
<center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
 <tr>
    <td width="100%"><b><font size="3">Sales Report - Product By Customers</font></b><br>
        <br><br>
	  <form method="post" action="sale_product_by_customer.asp" name="form">
      	<table border="0" width="100%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
         	<td width="162"><b>Product Code:</b></td>
			<td width="165"><b>Product Name:</b></td>
         	<td width="300"><b>Dough Name</b></td>
			<td>&nbsp;</td>
       	</tr>
		<tr>
       	  <td><input type="text" name="pcode" value="<%=vpcode%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
		  <td><input type="text" name="product" value="<%=vproduct%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
		  <td>
		  	<select size="1" name="dno" style="font-family: Verdana; font-size: 8pt">
          	<option value = "">ALL</option><%
          	if IsArray(vdoughtypearray) Then
				for i = 0 to ubound(vdoughtypearray,2)%>								
				<option value="<%=vdoughtypearray(0,i)%>" <%if trim(vdoughtypearray(0,i)) = vDno then%> selected<%end if%>><%=vdoughtypearray(1,i)%></option><%
              	next 
			end if
			%>         
            </select>
		  </td>
		 </tr>
	    <tr>
	   		<td colspan="4" height="5"></td>
	    </tr>
		</table>
		<table border="0" width="100%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
		<tr>
         	<td width="330"><b>From Date</b></td>
		  	<td width="330"><b>To Date</b></td>
			<td width="50">&nbsp;</td>
			<td>&nbsp;</td>
        </tr>
		<tr>
          <td>
		   <%
				dim arrayMonth
				arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
			%>
				<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
				  <%for i=1 to 31%>
					<option value="<%=i%>"><%=i%></option>
				  <%next%>
				</select>
				<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
				   <%for i=0 to 11%>
					<option value="<%=i%>"><%=arrayMonth(i)%></option>
				  <%next%>
				</select>
				
				<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
				  <%for i=2000 to Year(Date)+1%>
					<option value="<%=i%>"><%=i%></option>
				  <%next%>
				</select>
				<INPUT class=Datetxt id=txtfrom size="12" onFocus="blur();" name="txtfrom" value="<%=replace(fromdt,"from","")%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
				<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
  		 </td>
		 <td>
		 <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
		  <%for i=1 to 31%>
			<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
		   <%for i=0 to 11%>
			<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		
		<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
		  <%for i=2000 to Year(Date)+1%>
			<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=replace(todt,"to","")%>" onChange="setCalCombo1(this.form,document.form.txtto)">
		<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	 	</td>
		<td><input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
		<td>&nbsp;</td>
       </tr>
	     </table>
	   </form>
	 
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
			</SCRIPT>
			<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
			</SCRIPT>
      <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="5"><b>List of Products (From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt;)</b></td>
        </tr>
		<%if IsArray(recarray) Then%>
			<tr>
			  <td align="left" colspan="3"><b>Total no of products : <%=vRecordcount%></b></td>
			  <td align="right"><b>Page <%=vSessionpage%> of <%=vpagecount%></b></td>          
			</tr>
		<%End if%>  
        <tr>
		  <td width="15%"  bgcolor="#CCCCCC"><b>Product Code</b></td>
		  <td width="40%" bgcolor="#CCCCCC"><b>Product Name</b></td>
		  <td width="30%" bgcolor="#CCCCCC"><b>Dough Name</b></td>
          <td width="15%" bgcolor="#CCCCCC" align="center"><b>Action</b></td>
          
        </tr>
				<%
				if isarray(recarray) then%>
				<%for i=0 to UBound(recarray,2)%>
				<tr>
				<td><%=recarray(0,i)%>&nbsp;</td>
				<td><%=recarray(1,i)%>&nbsp;</td>
				<td><%=recarray(2,i)%>&nbsp;</td>
				  <form method="POST" action="sale_product_by_customer_view.asp">
				  <input type = "hidden" name = "pcode" value="<%=vpcode%>">
				  <input type = "hidden" name = "product" value="<%=recarray(1,i)%>">
				  <input type = "hidden" name = "dno" value="<%=vdno%>">
				   <input type = "hidden" name = "ProductID" value="<%=recarray(0,i)%>">
				  <input type = "hidden" name = "txtfrom" value="<%=fromdt%>">
				  <input type = "hidden" name = "txtto" value="<%=todt%>">
				   <input type = "hidden" name = "ismultipleproduct" value="<%=recarray(3,i)%>">
				  <td><input type="submit" value="View Sales Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
				  </form>
						
			  </tr>
			  <%
			  next
			  %>
          <%else%>
          <td colspan="6" bgcolor="#CCCCCC"><b>No records matched...</b></td> <% 
          end if%>
        </tr>

      </table>
    </td>
  </tr>
</table>
<table width="90%" border="0" cellspacing="1" cellpadding="2" align="center">
  <tr height="35"> 
    <td width="22%">
	<% if vSessionpage <> 1 and vsessionpage <> 0 then %>
		<form action="sale_product_by_customer.asp" method="post" name="frmFirst">
	        <input type="submit" name="Submit2" value="First Page">
			<input type="hidden" name="Direction" value="First">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">
			<input type="hidden" name="product" value="<%=vproduct%>">
			<input type="hidden" name="pcode" value="<%=vpcode%>">
			<input type="hidden" name="dno" value="<%=vdno%>">
			<input type="hidden" name="txtfrom" value="<%=fromdt%>">
			<input type="hidden" name="txtto" value="<%=todt%>">
		</form>
	<% End if %>
    </td>
    <td width="22%">
	<% if clng(vSessionpage) > 1 then %>
		<form action="sale_product_by_customer.asp" method="post" name="frmPrevious">
	        <input type="submit" name="Submit5" value="Previous Page">
			<input type="hidden" name="Direction" value="Previous">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">			
			<input type="hidden" name="product" value="<%=vproduct%>">
			<input type="hidden" name="pcode" value="<%=vpcode%>">
			<input type="hidden" name="dno" value="<%=vdno%>">
			<input type="hidden" name="txtfrom" value="<%=fromdt%>">
			<input type="hidden" name="txtto" value="<%=todt%>">
		</form>
	<% End if %>
    </td>
    <td width="22%">
	<% if clng(vSessionpage) < vPagecount then %>
		<form action="sale_product_by_customer.asp" method="post" name="frmNext">
	        <input type="submit" name="Submit4" value="Next Page">
			<input type="hidden" name="Direction" value="Next">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">			
			<input type="hidden" name="product" value="<%=vproduct%>">
			<input type="hidden" name="pcode" value="<%=vpcode%>">
			<input type="hidden" name="dno" value="<%=vdno%>">
			<input type="hidden" name="txtfrom" value="<%=fromdt%>">
			<input type="hidden" name="txtto" value="<%=todt%>">
		</form>
	<% End if %>
    </td>
    <td width="22%">
	<% if clng(vSessionpage) <> vPagecount and vPagecount > 0  then %>
		<form action="sale_product_by_customer.asp" method="post" name="frmLast">
	        <input type="submit" name="Submit3" value="Last Page">
			<input type="hidden" name="Direction" value="Last">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">			
			<input type="hidden" name="product" value="<%=vproduct%>">
			<input type="hidden" name="pcode" value="<%=vpcode%>">
			<input type="hidden" name="dno" value="<%=vdno%>">
			<input type="hidden" name="txtfrom" value="<%=fromdt%>">
			<input type="hidden" name="txtto" value="<%=todt%>">
		</form>
	<% End if %>
    </td>
  </tr>
</table>
</center>
</div>
</body>
<%if IsArray(recarray) then erase recarray%>
</html>