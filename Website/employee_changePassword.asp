<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%
Dim CurrentPassword
Dim object
Dim objResult
Dim strPassword
Dim vEmpNo

if Trim(Request.Form("Update")) <> "" Then
	Set object = Server.CreateObject("bakery.employee")
	object.SetEnvironment(strconnection)
	vEmpNo = session("UserNo")
	strPassword = Request.Form("txtNewPassword") 
	objResult = object.Update_Password(vEmpNo,strPassword)
	
	If objResult = "OK" Then
		LogAction "Password Changed", "" , ""
		session("Password") = strPassword 
	Else
		LogAction "Password Change Failed", "" , ""
	End if	
	
End if 

CurrentPassword = Trim(session("Password"))

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script LANGUAGE="javascript" SRC="includes/Script.js"></script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr><script Language="JavaScript"><!--
  
function Validator(theForm){  
var vCuPassword = '<%=CurrentPassword%>'

  if (theForm.txtCurrentPassword.value == "")
  {
    alert("Please enter a value for the \"Current Password\" field.");
    theForm.txtCurrentPassword.focus();
    return;
  }
  
  if (theForm.txtCurrentPassword.value != vCuPassword ){
		alert("Invalid  Current Password.");
    theForm.txtCurrentPassword.focus();
    return;
  
  }
  
  
  if (theForm.txtNewPassword.value == "")
  {
    alert("Please enter a value for the \"New Password\" field.");
    theForm.txtNewPassword.focus();
    return;
  }
  
   if (theForm.txtRePassword.value == "")
  {
    alert("Please enter a value for the \"Re Enter Password\" field.");
    theForm.txtRePassword.focus();
    return;
  }
  
  if (theForm.txtNewPassword.value != theForm.txtRePassword.value){
		alert("New password dose not match with re entered password");
    theForm.txtRePassword.focus();
    return;  
  }
  
  
  

    theForm.submit();
}
//--></script>
<form method="POST" action="employee_changepassword.asp"  name="frmpass">        
    <td width="100%"><b><font size="3">Change Password<br>
      &nbsp;</font></b>
      
      <table border="0" width="100%" cellspacing="0" cellpadding="2"><%
				if Trim(objResult) <> "" Then
					if objResult  = "OK" Then%>	
						<tr>
						  <td valign="top"><font size="3" color="#FF0000"><B>Password updated successfully</B></font><Td>
						<tr><%
					Else%>
						<tr>
						  <td valign="top"><font size="3" color="#FF0000"><B>Invalid Entry</B></font><Td>
						<tr><%	
					End		if	
				End if%>
        <tr>
          <td valign="top">
						<table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="65%">
							<tr>
								<td><b>Current Password&nbsp;</b></td>
								<td>         
									<input type="password" name="txtCurrentPassword" value="<%=strPassword%>"  maxlength = "10" size="25" style="font-family: Verdana; font-size: 8pt">
								</td>
							</tr>
							
							<tr>
								<td><b>New Password&nbsp;</b></td>
								<td>         
									<input type="password" name="txtNewPassword" value="<%=strPassword%>" maxlength = "10" size="25" style="font-family: Verdana; font-size: 8pt">
								</td>
							</tr>
							
							<tr>
								<td><b>Re Enter Password&nbsp;</b></td>
								<td>         
									<input type="password" name="txtRePassword" value="<%=strPassword%>" maxlength = "10" size="25" style="font-family: Verdana; font-size: 8pt">
								</td>
							</tr>														
							
							<tr>
								<td></td>
								<td></br>         
										<input type="button" value="Change Password" onClick="Validator(this.form);" name="Edit" style="font-family: Verdana; font-size: 8pt"> 			 											
								</td>
							</tr>														
						</table>
					</td>
        </tr>
      </table>
      
      
    </td>
  </tr> 
  
		<input type="hidden" value="True" name="Update"> 			 						
  </form>
</table>


  </center>
</div>


</body>

</html>