<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->

<%
Dim inNumber

'intNumber = Request.QueryString("txtInNo")


if Request.Form("intNumber")= "" then
	intNumber = Request.QueryString("txtInNo")
else
	intNumber = Request.Form("intNumber")
end if


if trim(intNumber) <> "" then
	Set objBakery = Server.CreateObject("Bakery.inventory")
	objBakery.SetEnvironment(strconnection)
	set retcol = objBakery.Display_IntMovementRecord(intNumber)
	recarray = retcol("IntMoveRec")

	set retcol = nothing
	set objBakery = nothing
	
	vday = Day(recarray(1,0))
	vmonth = Month(recarray(1,0))
	vyear = Year(recarray(1,0))
	lstdt = vday & "/" & vmonth & "/" & vyear
	
else
	vday = day(Date())
	vMonth = month(Date())
	vYear = year(Date())
	lstdt = vday & "/" & vMonth & "/" & vYear
end if




Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplaySupplier()	
vSuparray =  detail("Supplier") 
set detail = object.DisplayIng()	
vingarray =  detail("Ingredients") 
set detail = object.DisplayFacility()	
vFacarray =  detail("Facility") 
set detail = Nothing
set object = Nothing
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT LANGUAGE=javascript>
<!--
function initValueCal(theForm,theForm2){
	y=theForm.indate.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm2.value=t.getDate() +'/'+eval((t.getMonth())+1) +'/'+ t.getFullYear();
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
		
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
//-->
</SCRIPT>
<script Language="JavaScript">
<!--
function closewindow() {
window.opener.location.href = window.opener.location.href;
window.close();
}
//-->
</script>
<script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{

  if (theForm.indate.value == "")
  {
    alert("Please enter a value for the \"Date\" field.");
    theForm.indate.focus();
    return (false);
  }

  if (theForm.fromfac.selectedIndex < 0)
  {
    alert("Please select one of the \"From Facility\" options.");
    theForm.fromfac.focus();
    return (false);
  }

  if (theForm.fromfac.selectedIndex == 0)
  {
    alert("The first \"From Facility\" option is not a valid selection.  Please choose one of the other options.");
    theForm.fromfac.focus();
    return (false);
  }

  if (theForm.tofac.selectedIndex < 0)
  {
    alert("Please select one of the \"To facility\" options.");
    theForm.tofac.focus();
    return (false);
  }

  if (theForm.tofac.selectedIndex == 0)
  {
    alert("The first \"To facility\" option is not a valid selection.  Please choose one of the other options.");
    theForm.tofac.focus();
    return (false);
  }

  if (theForm.ing.selectedIndex < 0)
  {
    alert("Please select one of the \"Ingredient\" options.");
    theForm.ing.focus();
    return (false);
  }

  if (theForm.ing.selectedIndex == 0)
  {
    alert("The first \"Ingredient\" option is not a valid selection.  Please choose one of the other options.");
    theForm.ing.focus();
    return (false);
  }

  if (theForm.qty.value == "")
  {
    alert("Please enter a value for the \"Quantity\" field.");
    theForm.qty.focus();
    return (false);
  }
  return (true);
}
//--></script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal(document.form1,document.form1.indate)">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;

<form method="POST" action="save_stockmovement.asp" name = "form1" onSubmit="return FrontPage_Form1_Validator(this)">

<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Inventory Movement<br>
      &nbsp;</font></b>
<%
if Request.QueryString ("status") = "error" then
	Response.Write "<br><font size=""3"" color=""#FF0000""><b>Error occurred while making Inventory Movement entry</b></font><br><br>"
elseif Request.QueryString ("status") = "ok" then
	Response.Write "<br><font size=""3"" color=""#FF0000""><b>Inventory Movement entry added successfully</b></font><br><br>"
elseif Request.QueryString ("status") = "okay" then	
	
	Response.Write "<br><font size=""3"" color=""#FF0000""><b>Inventory In entry Updated successfully</b></font><br><br>"
	%>
 <table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
  
    <td height="21" align="center"><input type="button" value="Close" name="B2" onClick="closewindow()" style="font-family: Verdana; font-size: 8pt"></td>
</tr>
 </table>
<%
elseif Request.QueryString ("status") = "errors" then	
Response.Write "<br><font size=""3"" color=""#FF0000""><b>Error occurred while Updating Inventory In entry</b></font><br><br>"
end if
%>
<%if Request.QueryString ("status") <> "okay" then%>
      <table border="0" width="70%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        
        <tr>
          <td height="15">Date</td>
           <TD width="375">
	    <%
		dim arrayMonth
		arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
	    %>
		<select name="day" onChange="setCal(this.form,document.form1.indate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form1.indate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form1.indate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt id=deldate  name="indate" value="<%=lstdt%>" onFocus="blur();" onChange="setCalCombo(this.form,document.form1.indate)">
      <IMG id=f_trigger_fr onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  </TD>
		  </tr>
        <tr>
          <td>From Facility</td>
          <td>
          <select size="1" name="fromfac" style="font-family: Verdana; font-size: 8pt">
          			<option value="">Select</option>
								<%for i = 0 to ubound(vFacarray,2)%> 
									<%if IsArray(recarray) then%> 
										<%if recarray(3,0) = vFacarray(0,i)  then%>
											<option selected value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
										<%else%>	
											<%if vfacarray(0,i) = "15" or vfacarray(0,i) = "18" or vfacarray(0,i) = "90" then%>			
												<option value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
											<%end if%>
										<%end if%>
									<%else%>
										<%if vfacarray(0,i) = "15" or vfacarray(0,i) = "18" or vfacarray(0,i) = "90" then%>
											<option value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
										<%end if%>
                  <%end if%>
                <%next%>              
                  </select></td>
        </tr>
        <tr>
          <td>To Facility</td>
          <td>
          <select size="1" name="tofac" style="font-family: Verdana; font-size: 8pt">
          <option value="">Select</option>
					<%for i = 0 to ubound(vFacarray,2)%> 
						<%if IsArray(recarray) then%>  
							<%if recarray(4,0) = vFacarray(0,i)  then%>		
								<option selected value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
							<%else%>
								<%if vfacarray(0,i) = "15" or vfacarray(0,i) = "18" or vfacarray(0,i) = "90" then%>		
									<option value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
								<%end if%>
							<%end if%>
						<%else%>
							<%if vfacarray(0,i) = "15" or vfacarray(0,i) = "18" or vfacarray(0,i) = "90" then%>		
                  <option value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
              <%end if%>
            <%End if%>
          <%next%>                        
          </select></td>
        </tr>
        <tr>
          <td>Ingredient</td>
          <td>
          <select size="1" name="ing" style="font-family: Verdana; font-size: 8pt">
          <option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%> 
								<%if IsArray(recarray) then%> 
							<%if trim(recarray(2,0)) = trim(vingarray(0,i))  then%>
						 		<option selected value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%else%>	
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>	
						<%else%>			
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
            <%end if%>
           <%next%>  
           </select></td>
        </tr>
        <tr>
          <td>Qty</td>
          <td >
          <input type="text" name="qty" <%If IsArray(recarray) then%>value="<%=recarray(5,0)%>" <%end if%> size="20" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <%If IsArray(recarray) then%>
          <td><input type="submit" value="Update Movement" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
          <input type="hidden" value="Update" name="Update">
           <input type="hidden" value="<%=intNumber%>" name="intNumber">
          <%else%>
          <td><input type="submit" value="Add Movement" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
          <%end if%>
        </tr>
      </table>

<%end if%>
  </center>
    </td>
  </tr>
</table>

</div>

</form>
<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "indate",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr",
				singleClick    :    true
			});
	</SCRIPT>

</body>
<%if IsArray(recarray) then erase recarray %>
<%if IsArray(vingarray) then erase vingarray %>
</html>