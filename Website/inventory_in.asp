<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->

<%

Dim inNumber

txtfrom = Request.Form("txtfrom")
txtto = Request.Form("txtto")

if Request.Form("intNumber")= "" then
	intNumber = Request.QueryString("txtInNo")
else
	intNumber = Request.Form("intNumber")
end if




if Request.Form("indate")<>"" then

	lstdt = Request.Form("indate")
	
else
	lstdt = Request.Querystring("indate")
	
	if IsDate(lstdt) then
	vday = day(lstdt)
	vMonth = month(lstdt)
	vYear = year(lstdt)
	lstdt = vday & "/" & vMonth & "/" & vYear
	else
		vday = day(Date())
	vMonth = month(Date())
	vYear = year(Date())
	lstdt = vday & "/" & vMonth & "/" & vYear
	
	End if
End if	


if Request.form("sup")<>"" then
	vsup = Request.form("sup")
else
	vsup = Request.QueryString("sup")
end if
if Request.form("facility")<>"" then
	vfacility = Request.form("facility")
else
	vfacility = Request.QueryString("facility")
end if

if Request.form("unit")<>"" then
	vunit = Request.form("unit")
else
	vunit = Request.QueryString("unit")
end if

if Request.form("qty")<>"" then
	vqty = Request.form("qty")
else
	vqty = Request.QueryString("qty")
end if


if Request.form("price")<>"" then
	vprice = Request.form("price")
else
	vprice = Request.QueryString("price")
end if


if Request.form("ing")<>"" then
	ving = Request.form("ing")
else
	ving = Request.QueryString("ing")
end if

Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplaySupplier()	
vSuparray =  detail("Supplier") 
set detail = object.DisplayFacility()	
vFacarray =  detail("Facility") 
set detail = object.DisplaySubFacIng(vsup,vfacility)	
vingarray =  detail("Ingredients")

set detail = Nothing
set object = Nothing
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>

<script Language="JavaScript">
<!--
function closewindow() {
window.opener.location.href = window.opener.location.href;
window.close();
}
//-->
</script>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT LANGUAGE=javascript>
<!--
function initValueCal(theForm,theForm2){
	y=theForm.indate.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm2.value=t.getDate() +'/'+eval((t.getMonth())+1) +'/'+ t.getFullYear();
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
		
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
//-->
</SCRIPT>
<script Language="JavaScript"><!--

function validateFacility(theForm){
	if(theForm.indate.value == ""){
    alert("Please enter a value for the \"Date\" field.");
    theForm.selFacCode.selectedIndex = 0;
    theForm.indate.focus();
    return;
  }  
	theForm.submit();
}


function validator(theForm){
	if(theForm.indate.value == ""){
    alert("Please enter a value for the \"Date\" field.");
    theForm.indate.focus();
    return;}  

  if(theForm.selFacCode.selectedIndex == 0){
		alert("The first \"Facility\" option is not a valid selection.  Please choose one of the other options.");
    theForm.selFacCode.focus();
    return;}	
    theForm.Update.value="True";	
		theForm.submit();
}

//-->
</script>

<script Language="JavaScript">
<!--
function GetIngredients() {
document.form1.action = 'inventory_in.asp';
document.form1.submit();
}
//-->
</script>
<script Language="JavaScript"><!--
function FrontPage_Form1_Validator()
{

  if (document.form1.indate.value == "")
  {
    alert("Please enter a value for the \"Date\" field.");
    document.form1.indate.focus();
    return;
  }
  if (document.form1.sup.selectedIndex < 0)
  {
    alert("Please select one of the \"Supplier\" options.");
    document.form1.sup.focus();
    return;
  }

  if (document.form1.sup.selectedIndex == 0)
  {
    alert("The first \"Supplier\" option is not a valid selection.  Please choose one of the other options.");
    document.form1.sup.focus();
    return ;
  }

  if (document.form1.facility.selectedIndex < 0)
  {
    alert("Please select one of the \"Facility\" options.");
    document.form1.facility.focus();
    return;
  }

  if (document.form1.facility.selectedIndex == 0)
  {
    alert("The first \"Facility\" option is not a valid selection.  Please choose one of the other options.");
    document.form1.facility.focus();
    return;
  }

  if (document.form1.ing.selectedIndex < 0)
  {
    alert("Please select one of the \"Ingredient\" options.");
    document.form1.ing.focus();
    return;
  }

  if (document.form1.ing.selectedIndex == 0)
  {
    alert("The first \"Ingredient\" option is not a valid selection.  Please choose one of the other options.");
    document.form1.ing.focus();
    return;
  }

  if (document.form1.unit.value == "")
  {
    alert("Please enter a value for the \"Unit\" field.");
    document.form1.unit.focus();
    return;
  }

  if (document.form1.qty.value == "")
  {
    alert("Please enter a value for the \"Quantity\" field.");
    document.form1.qty.focus();
    return;
  }

  if (document.form1.price.value == "")
  {
    alert("Please enter a value for the \"Price\" field.");
    document.form1.price.focus();
    return ;
  }

  document.form1.action = 'save_stockin.asp'
  document.form1.submit();
}
//--></script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal(document.form1,document.form1.indate)">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;

<form method="POST" name = "form1"> 

<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Inventory In<br>
      &nbsp;</font></b>
<%
if Request.QueryString ("status") = "error" then
	Response.Write "<br><font size=""3"" color=""#FF0000""><b>Error occurred while making Inventory In entry</b></font><br><br>"
elseif Request.QueryString ("status") = "ok" then
	Response.Write "<br><font size=""3"" color=""#FF0000""><b>Inventory In entry added successfully</b></font><br><br>"
elseif Request.QueryString ("status") = "okay" then		
	Response.Write "<br><font size=""3"" color=""#FF0000""><b>Inventory In entry Updated successfully</b></font><br><br>"	
%>
 <table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
  
    <td height="21" align="center"><input type="button" value="Close" name="B2" onClick="closewindow()" style="font-family: Verdana; font-size: 8pt"></td>
</tr>
 </table>
<%	
elseif Request.QueryString ("status") = "errors" then	
	Response.Write "<br><font size=""3"" color=""#FF0000""><b>Error occurred while Updating Inventory In entry</b></font><br><br>"
end if
%>

<%if Request.QueryString ("status") <> "okay" then%>
     <table border="0" width="70%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2" height="199">
        <tr>
          <td height="15">Date</td>
          <td height="15">
		  <%
		dim arrayMonth
		arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
	    %>
		<select name="day" onChange="setCal(this.form,document.form1.indate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form1.indate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form1.indate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt id=indate  name="indate" value="<%=lstdt%>" onFocus="blur();" onChange="setCalCombo(this.form,document.form1.indate)">
      <IMG id=f_trigger_fr onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	   </td>
        </tr>
        <tr>
       
          <td height="19">Supplier</td>
          <td height="19">
          <select size="1" name="sup" style="font-family: Verdana; font-size: 8pt" onChange="GetIngredients();">
          		<option value="">Select</option>
          		<%if IsArray(vSuparray) then%>	
							<%for i = 0 to ubound(vSuparray ,2)%> 
								
									<%if trim(vsup) = vSuparray(0,i) then%>
											<option selected value="<%=vSuparray(0,i)%>"><%=vSuparray(1,i)%></option>
										<%else%>
								 		<option value="<%=vSuparray(0,i)%>"<% if trim(vsup) =  trim(vSuparray(0,i)) then Response.Write "selected" end if%>><%=vSuparray(1,i)%></option>
                  <%end if%>
              <%next%>   
                <%end if%>
                         
            </select></td>
        </tr>
        <tr>
          <td height="19">Production Facility</td>
          <td height="19">
          <select size="1" name="facility" style="font-family: Verdana; font-size: 8pt" onChange="GetIngredients();">
          			<option value="">Select</option>
          <%if isarray(vFacarray) then%>
					<%for i = 0 to ubound(vFacarray,2)%>
								<%if trim(vfacility) = vfacarray(0,i)  then%>
								<option selected value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>

							<%else%>
								<%if vfacarray(0,i) = "15" or vfacarray(0,i) = "18" or vfacarray(0,i) = "90" then%>			
										<option value="<%=vFacarray(0,i)%>" <% if trim(vfacility) =  trim(vFacarray(0,i)) then Response.Write "selected"  end if%>><%=vFacarray(1,i)%></option>
                <%end if%>
            <%End if%>
          <%next%>
          <%End if%> 
          </select></td>
        </tr>
        <tr>
          <td height="19">Ingredient</td>
          <td height="19">
          <select size="1" name="ing" style="font-family: Verdana; font-size: 8pt">
          			<option value="">Select</option>
          			<%if isarray(vingarray) then%>
									<%for i = 0 to ubound(vingarray,2)%>  	
										<%if trim(ving) = vingarray(0,i)  then%>		
										<option selected value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
										<%else%>
										<option value="<%=vingarray(0,i)%>" <% if trim(vIng) =  trim(vingarray(0,i)) then Response.Write "selected"  end if%>><%=vingarray(2,i)%></option>
										<%end if%>
  
									<%next%>       
               <%end if%>       
            </select></td>
        </tr>
        <tr>
          <td height="19">Unit</td>
          <td height="19">
          <input type="text" name="unit"  value="<%=vunit%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Qty</td>
          <td height="19" >
          <input type="text" name="qty" value="<%=vqty%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Price per Unit</td>
          <td height="19" >
          <input type="text" name="price" value="<%=vprice%>"  size="20" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="13"></td>
          <td height="13"></td>
        </tr>
        <tr>
          <td height="21"></td>
          <%If intNumber <> "" then%>
          <td height="21"><input type="button" value="Update Inventory" name="B1" onClick="FrontPage_Form1_Validator(this)" style="font-family: Verdana; font-size: 8pt"></td>
           <input type="hidden" value="Update" name="Update">
            <input type="hidden" value="<%=intNumber%>" name="intNumber">
          
           
          <%else%>
          <td height="21"><input type="button" value="Add Inventory" name="B2" onClick="FrontPage_Form1_Validator(this)" style="font-family: Verdana; font-size: 8pt"></td>
          <%end if%>
        </tr>
      </table>

<%end if%>

  </center>
    </td>
  </tr>
</table>


</div>
</form>
<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "indate",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr",
				singleClick    :    true
			});
</SCRIPT>
</body>
<%if IsArray(recarray) then erase recarray %>
<%if IsArray(vSuparray) then erase vSuparray %>
<%if IsArray(vFacarray) then erase vFacarray %>
<%if IsArray(vingarray) then erase vingarray %>
</html>