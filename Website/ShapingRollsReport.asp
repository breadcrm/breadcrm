<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function CheckSearch(frm)
{
	if (frm.DoughType.value=="")
	{
		alert("Please select a Dough Name");
		frm.DoughType.focus();
		return  false;
	}
	if (frm.Shape.value=="")
	{
		alert("Please select a Shape");
		frm.Shape.focus();
		return  false;
	}
	if (frm.DeliveryType.value=="")
	{
		alert("Please select a Delivery Type");
		frm.DeliveryType.focus();
		return  false;
	}
	return  true;
}

//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim recarray
dim retcol
dim fromdt, todt , i
Dim TotQuantity,TotTurnover
dim arrProductCategory
dim arrIName
dim recsecondmixarray
dim retsecondmixcol
dim arrSecondMixProductCategory
dim arrSecondMixIName

fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")
strDoughType = Request.Form("DoughType")
if (strDoughType="" or not isnumeric(strDoughType)) then
	strDoughType="0"
end if
strShape = Request.Form("Shape")
if (strShape="" or not isnumeric(strShape)) then
	strShape="0"
end if
strDeliveryType = Request.Form("DeliveryType") 
if (strDeliveryType="" or not isnumeric(strDeliveryType)) then
	strDeliveryType="0"
end if

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

if isdate(fromdt) and isdate(todt) then   
    set objBakery = server.CreateObject("Bakery.reports")
	objBakery.SetEnvironment(strconnection)
	set retsecondmixcol = objBakery.DoughShapingReport(cint(strDoughType),cint(strShape),cint(strDeliveryType),fromdt,todt)    
    recsecondmixarray=retsecondmixcol("DoughShapingReport")
end if

Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayDoughType()	
vDoughTypearray =  detail("DoughType")

set detail = object.DisplayTypeDes()	
vShapeArray =  detail("Type2") 

set detail = object.ListDeliveryTypes()	
vDeliveryTypearray =  detail("DeliveryTypes")

set detail = Nothing
set object = Nothing

stop

%>
<table border="0" width="90%" align="center" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
<tr>
    <td width="100%"><b><font size="3">Shaping Report</font></b><br><br><br>
	  <form method="post" action="ShapingRollsReport.asp" name="form" onSubmit="return CheckSearch(this)">
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
	  <tr>
	  <td>
		  <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
			<tr height="25">          
			  <td><b>Dough Name:&nbsp;</b></td>
			  <td>
				<select size="1" name="DoughType" style="font-family: Verdana; font-size: 8pt">
					<option value = "">Select</option>
					<%
					if IsArray(vDoughTypearray) Then  	
						for i = 0 to ubound(vDoughTypearray,2)%>  				
							 <option value="<%=vDoughTypearray(0,i)%>" <%if strDoughType<>"" then%><%if cint(strDoughType)= vDoughTypearray(0,i) then response.write " Selected"%><%end if%>><%=vDoughTypearray(1,i)%></option>
					  <%
					   next                 
					End if
					%>   
			  	</select>
			  </td>
			</tr>
			<tr height="25">          
			  <td><b>Shape:&nbsp;</b></td>
			  <td>
				<select size="1" name="Shape" style="font-family: Verdana; font-size: 8pt">
					<option value = "">Select</option>
					<%
					if IsArray(vShapearray) Then  	
						for i = 0 to ubound(vShapearray,2)
							if (vShapearray(0,i)="3" or vShapearray(0,i)="8") then
						%>  				
								 <option value="<%=vShapearray(0,i)%>" <%if strShape<>"" then%><%if cint(strShape)= vShapearray(0,i) then response.write " Selected"%><%end if%>><%=vShapearray(1,i)%></option>
						<%
							end if
					   next                 
					End if
					%>   
			  	</select>
			  </td>
			</tr>
			<tr height="25">          
			  <td><b>Delivery Type:&nbsp;</b></td>
			  <td>
				<select size="1" name="DeliveryType" style="font-family: Verdana; font-size: 8pt">
					<option value="0">All</option>
					<%
					if IsArray(vDeliveryTypearray) Then  	
						for i = 0 to ubound(vDeliveryTypearray,2)%>  				
							 <option value="<%=vDeliveryTypearray(0,i)%>" <%if strDeliveryType<>"" then%><%if cint(strDeliveryType)= vDeliveryTypearray(0,i) then response.write " Selected"%><%end if%>><%=vDeliveryTypearray(1,i)%></option>
					  <%
					   next                 
					End if
					%>   
			  	</select>
			  </td>
			</tr>
			</table>
		</td>
		</tr>
		<tr>
		<td>
		<table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">	
        <tr>
          <td><b>From:</b></td>
          <td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom onFocus="blur();" size="12"  name="txtfrom" value="<%=fromdt%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					<td><b>To:</b></td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=todt%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td> <td>
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
      </table>
	  </td>
	  </tr>
	  </table>
	  </form>
  <SCRIPT type=text/javascript>
		Calendar.setup({
			inputField     :    "txtfrom",
			ifFormat       :    "%d/%m/%Y",
			button         :    "f_trigger_frfrom",
			singleClick    :    true
		});
	</SCRIPT>
	<SCRIPT type=text/javascript>
		Calendar.setup({
			inputField     :    "txtto",
			ifFormat       :    "%d/%m/%Y",
			button         :    "f_trigger_frto",
			singleClick    :    true
		});
	</SCRIPT>
	<% if (Request("Search")<>"") then%>
    <table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
	<tr>
          <td width="100%"  colspan="5" height="40" bgcolor="#FFFFFF"><b>Shaping Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
    </tr>
     	<tr style="font-weight:bold" bgcolor="#CCCCCC">               
            <td>Product Name</td>    
            <td>Pre-bake size</td>	  
            <td>Post bake size</td>    
            <td>Number of units</td>    
        </tr>
		<%if isarray(recsecondmixarray) then%>
		   <%
				for m=0 to UBound(recsecondmixarray,2)
					if (m mod 2 =0) then
						strBgColour="#FFFFFF"
					else
						strBgColour="#F3F3F3"
					end if
			%>
				<tr bgcolor="<%=strBgColour%>" height="22">
			                    	    
					<td><%=recsecondmixarray(0,m)%></td>
					<td><%=recsecondmixarray(1,m)%></td>
					<td><%=recsecondmixarray(2,m)%></td>	
					<td><%=recsecondmixarray(3,m)%></td>
			
				</tr>    	    	            
			<%
				next
			%>
			<%else%>
			<tr><td bgcolor="#FFFFFF" colspan="5" height="40"><p align="center"><font color="#FF0000"><strong>There are no records found.</strong></font></p></td></tr>	
            <%end if%>    
        </table> 
		<%end if%>       
    </td>
</tr>
</table>
<br><br>
</body>
<%if IsArray(recarray) then erase recarray  %>
</html>
