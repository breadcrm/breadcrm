<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
	fromdt=request("fromdt")
	todt=request("todt")
	vCustomer = Request("customer")
	
	vCustomerGroup=Request("CustomerGroup")
	if vCustomerGroup<>"" then
		vCustomer="-99"
	end if
	if vCustomer="" then
		vCustomer="-99"
	end if
	
	if fromdt ="" then
    	fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
	  	todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
	end if
	
	set objBakery = server.CreateObject("Bakery.reports")
	objBakery.SetEnvironment(strconnection)
	'set CustomerCol = objBakery.Display_CustomerList()
	'arCustomer = CustomerCol("Customer")
	
	set CustomerCol = nothing
	if isdate(fromdt) and isdate(todt)  then
		set retcol = objBakery.Display_Sales_Credit(fromdt,todt,vCustomer,vCustomerGroup)
		recarray = retcol("CustSaleCredit")
		'strWeekVanName = retcol("WeekVanName")
		'strWeekVanNo = retcol("WeekVanNo")
		'strWeekEndVanName = retcol("WeekEndVanName")
		'strWeekEndVanNo = retcol("WeekEndVanNo")
		'strCusName = retcol("CusName")
		set retcol = nothing
	end if
	set objBakery = nothing
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=report_sale_credit.xls" 
	%>
	<style>
		.text{
  			mso-number-format:"\@";/*force text*/
		}
	</style>
	 <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%" colspan="11"><b>Sales/Credit Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
        <tr height="24">
          <td width="5%"  bgcolor="#CCCCCC"><b>Customer No</b></td>
		  <td width="20%"  bgcolor="#CCCCCC"><b>Customer Name</b></td>
          <td width="8%"  bgcolor="#CCCCCC"><b>Invoice/Credit Date</b></td>
		  <td width="8%"  bgcolor="#CCCCCC"><b>Invoice/Credit No</b></td>
		  <td width="8%"  bgcolor="#CCCCCC"><b>Product No</b></td>
		  <td width="20%"  bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td width="6%" bgcolor="#CCCCCC" align="right"><b>Ordered Qty</b></td>
          <td width="6%" bgcolor="#CCCCCC" align="right"><b>Credit Qty</b></td>
		  <td width="6%" bgcolor="#CCCCCC" align="right"><b>Unit Price</b></td>
		  <td width="6%" bgcolor="#CCCCCC" align="right"><b>Total Price</b></td>
		  <td width="6%" bgcolor="#CCCCCC" align="right"><b>Line Total </b></td>
        </tr>
			
				<%if isarray(recarray) then%>
				<% 				
				Totalturnover = 0.00

				
				%>
				<%for i=0 to UBound(recarray,2)%>
		  <tr height="22">
		  <td><b><%=recarray(0,i)%></b></td>
		  <td><b><%if recarray(1,i)<> "" then Response.Write recarray(1,i) end if%></b>&nbsp;</td>
		  <td ><b><%if recarray(2,i)<> "" then Response.Write recarray(2,i) end if%></b>&nbsp;</td>
		  <td ><b><%if recarray(3,i)<> "" then Response.Write recarray(3,i) end if%></b>&nbsp;</td>
		  <td class="text"><b><%if recarray(4,i)<> "" then Response.Write recarray(4,i) end if%></td>
		  <td><b><%if recarray(5,i)<> "" then Response.Write recarray(5,i) end if%></b>&nbsp;</td>
		  
          <td align="right"><b><%if recarray(6,i)<> "" then Response.Write formatnumber(recarray(6,i),0) else Response.Write "0" end if%></b></td> 
          <td align="right"><b><%if recarray(7,i)<> "" then Response.Write formatnumber(recarray(7,i),0) else Response.Write "0" end if%></b></td>
          <td align="right"><b><%if recarray(8,i)<> "" then Response.Write formatnumber(recarray(8,i),2) else Response.Write "0.00" end if%></b></td>
          <td align="right"><b><%if recarray(9,i)<> "" then Response.Write formatnumber(recarray(9,i),2) else Response.Write "0.00" end if%></b></td>
		  <%
          if recarray(9,i)<> "" then 
				Totalturnover = Totalturnover + formatnumber(recarray(9,i),2)
		  End if
          %>
          <td align="right" ><b>&nbsp;<%=formatnumber(Totalturnover,2)%></b></td> 
          </tr>

          <%next%>
     	  <%Response.Flush()%>

          <%
          else%>
         <td colspan="11" width="100%" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td><% 
          end if%>
        </tr>
      </table>
    </td>
  </tr>
</table>