<%@ Language=VBScript%>
<%
'option Explicit
Response.Buffer = true
%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim recarray
dim retcol
dim fromdt, todt,i
Dim GrandTotal, strTimeSeriesProductList, recarrayHeaderName

fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")
strUnitOption=Request.Form("UnitOption")

strTimeSeriesProductList=trim(Request.Form("TimeSeriesProductList"))
strTimeSeriesProductList=left(strTimeSeriesProductList,len(strTimeSeriesProductList)-1)
'strTimeSeriesProductList = replace(strTimeSeriesProductList,",","','")
if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
   todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if
if isdate(fromdt) and isdate(todt) then
	set objBakery = server.CreateObject("Bakery.reports")
	objBakery.SetEnvironment(strconnection)
	set retcol = objBakery.Display_TimeSeries_Products(strUnitOption,fromdt,todt,strTimeSeriesProductList)
	recarray = retcol("Product")
	strvalue=retcol("HeaderName")	
	recarrayHeaderName=split(strvalue,",")
end if
if isarray(recarray) then
	dim  arrayGrandTotal(25)
	for j=1 to UBound(recarray,1)
		arrayGrandTotal(j-1)=0	
	next
	ColNo=UBound(recarray,1)+1
end if
%>
<table border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt" align="center">
 <%if isarray(recarray) then%>
 <form action="ExportToExcelFile_TimeSeries_Product_Report.asp" method="post">
 <input type="hidden" name="TimeSeriesProductList" value="<%=strTimeSeriesProductList%>">
 <INPUT type="hidden" name="txtfrom" value="<%=fromdt%>">
 <INPUT type="hidden" name="txtto" value="<%=todt%>">
 <INPUT type="hidden" name="UnitOption" value="<%=strUnitOption%>">
 <tr>
 <td align="left" height="40"><input type="submit" value="Export to Excel - Time Series Product Report" style="font-family: Verdana; font-size: 8pt; width:280px"></td>
 </tr>
 </form>
 <%end if%> 
 <tr>
 <td><b><font size="2"><%=strUnitOption%> Time Series Product Report &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt;<br><br></font></b>
	<table border="1" width="<%=ColNo*120%>" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<%if isarray(recarray) then%>
	<tr style="font-weight:bold">
	<%for j=0 to UBound(recarrayHeaderName)%>
		
		<td bgcolor="#CCCCCC" width="120"><b><%=recarrayHeaderName(j)%></b></td>
		
	<%next%>
	</tr>
	<%for i=0 to UBound(recarray,2)%>
	<tr>
		<td align="left" width="75"><%=recarray(0,i)%></td>
	<%for j=1 to UBound(recarray,1)%>
		<td align="center"><%=recarray(j,i)%></td>
		<%
		arrayGrandTotal(j)=arrayGrandTotal(j)+recarray(j,i)
	next
	%>
	</tr>
	<%next%>
	<tr>
	  <td bgcolor="#CCCCCC" align="right"><b>&nbsp;Grand Total</b>&nbsp;</td>
	  <%for j=1 to UBound(recarray,1)%>
	  <td align="center"  bgcolor="#CCCCCC"><b>&nbsp;<%=arrayGrandTotal(j)%></b></td>
	  <%next%>
	</tr>
	<%else%>
	  <tr><td colspan="7" width="500" bgcolor="#CCCCCC" align="center"><b>0 - No records matched...</b></td></tr>
	<%end if%>
	</table>
</td>
</tr>
</table>
<p align="center"><input type="button" value=" Back " name="back" onClick="history.back()"></p>
</body>
<%if IsArray(recarray) then erase recarray %>
</html>