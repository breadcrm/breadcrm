<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function ExportToExcelFile()
{
	fromdate=document.form.txtfrom.value
	todate=document.form.txtto.value
	cno=document.form.txtcno.value
	cname=document.form.txtcname.value
	ordno=document.form.txtordno.value
	crno=document.form.txtcrno.value
	pono=document.form.txtpono.value
	document.location="ExportToExcelFile_CustomerStatement.asp?fromdt=" + fromdate + "&todt=" + todate + "&cno=" + cno + "&cname=" + cname + "&ordno=" + ordno + "&crno=" + crno + "&pono=" + pono
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim recarray
dim retcol
dim totpages
dim vSessionpage
dim i

dim cno
dim cname
dim ordno
dim fromdt
dim todt

vPageSize = 50
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vSessionpage	= Request.Form("CurrentPage") 

cno = replace(Request.Form("txtcno"),"'","''")
cname = replace(Request.Form("txtcname"),"'","''")
ordno = replace(Request.Form("txtordno"),"'","''")
fromdt = replace(Request.Form("txtfrom"),"'","''")
todt = replace(Request.Form("txtto"),"'","''")

crno = replace(Request.Form("txtcrno"),"'","''")
pono = replace(Request.Form("txtpono"),"'","''")

if fromdt ="" then
  fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

if not isnumeric(cno) then
	cno = 0
end if
if not isnumeric(ordno) then
	ordno = 0
end if

if not isnumeric(crno) then
	crno = 0
end if

totpages = Request.Form("pagecount")

If strDirection = "First" or strDirection="" Then
	vSessionpage = 1
Elseif strDirection = "Previous" Then
	vSessionpage = vSessionpage - 1
Elseif strDirection = "Next" Then
	vSessionpage = vSessionpage +1	
Elseif strDirection = "Last" Then
	vSessionpage = vpagecount
Else
	vSessionpage = 1
End if

set objBakery = server.CreateObject("Bakery.credit")
objBakery.SetEnvironment(strconnection)

set retcol = objBakery.ListCustomerStatement(cno,cname,ordno,crno,pono,replace(fromdt,"from",""),replace(todt,"to",""),vSessionpage,vPageSize)
totpages = retcol("pagecount")
recarray = retcol("Invoices")
vRecordCount = retcol("Recordcount")

if cno=0 then
	cno = ""
end if

if ordno=0 then
	ordno = ""
end if

if crno=0 then
	crno = ""
end if
%>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt" align="center">
  
  <tr>
    <td><b><font size="3">Customer Statement<br><br></font></b>
      <table border="0" width="850" style="font-family: Verdana; font-size: 8pt">
        <form method="post" action="CustomerStatement.asp" name="form">
		<tr height="25">
          <td width="96"><b>Customer Code</b></td>
          <td><input type="text" value="<%=cno%>" name="txtcno" size="30" maxlength="12" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr height="25">
          <td><b>Customer Name</b></td>
          <td><input type="text" value="<%=cname%>" name="txtcname" size="30" maxlength="200" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr height="25">
          <td><b>Invoice No</b></td>
          <td><input type="text" value="<%=ordno%>" name="txtordno" size="30" maxlength="12" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
		<tr height="25">
          <td><b>Credit No</b></td>
          <td><input type="text" value="<%=crno%>" name="txtcrno" size="30" maxlength="12" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
		<tr height="25">
          <td><nobr><b>Purchase Order No</b>&nbsp;</nobr></td>
          <td><input type="text" value="<%=pono%>" name="txtpono" size="30" maxlength="50" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td></td>
          <td>
            <table border="0" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0" height="38">
              <tr>
                <td height="13"><b>Invoice From Date</b></td>
                <td height="13"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                <td height="13"><b>Invoice To Date</b></td>
              </tr>
              <tr>
                <td height="18" width="325">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom size="12" onFocus="blur();" name="txtfrom" value="<%=replace(fromdt,"from","")%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
				<td height="25"></td>
                
                <td height="18" width="325">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=replace(todt,"to","")%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
				  </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            
          </td>
        </tr>
        <tr>
          <td></td>
          <td><input type="submit" value="Submit" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
		</form>
      </table>
	 
      
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
			</SCRIPT>
			<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
			</SCRIPT>
   <p align="left">
   
  
    
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr height="25">
		<td> <b><font size="2">Customer Statement between &lt;<%=fromdt%>&gt; and &lt;<%=todt%>&gt;</font></b></td>
		<%
		if isarray(recarray) then
		%>
		<td align="right" class="headtext"><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></td>
		<%
		end if
		%>
	  </tr>
	</table>
	
	<%
    if isarray(recarray) then
	%>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr class="headtext" height="22">
		<td>Total no of records: <%=vRecordcount%></td>
		<td align="right">Page <%=vSessionpage%> of <%=totpages%></td>
	  </tr>
	</table>
	<%
		end if
	%>
		<table border="0" width="100%" class="tableborder" cellspacing="1" cellpadding="2" align="center">
        <tr class="tablehead">
          <td align="left"><b>Customer Code</b></td>
		  <td align="left"><b>Customer Name</b></td>
	   	  <td align="left"><b>Invoice Date</b></td>
		  <td align="left"><b>Invoice No</b></td>
          <td align="left"><b>Purchase Order No</b></td>         
          <td align="right"><b>Invoice Amount</b></td>		  
		  <td align="left"><b>Credit Date</b></td>
		  <td align="left"><b>Credit No</b></td>
		  <td align="right"><b>Credit Amount</b></td>
		  <td align="right"><b>Charge</b></td>       
        </tr>
		<%
        if isarray(recarray) then
          for i=0 to ubound(recarray,2)
		  	if (recarray(8,i)<>"") then
				dbcharge=recarray(7,i)-recarray(8,i)
			else
				dbcharge=recarray(7,i)	
			end if
			if (i mod 2=0) then
				strbg="tablerow"
			else
				strbg="tablealtrow"
			end if		
		%>
          <tr class="<%=strbg%>">
			  <td><%=recarray(0,i)%></td>
			  <td><%=recarray(1,i)%></td>
			  <td align="left"><%=displayBristishDate(recarray(2,i))%></td>
			  <td align="left"><%=recarray(3,i)%></td>
			  <td align="left"><%=recarray(4,i)%></td>
			  <td align="right">&pound;<%=FormatNumber(recarray(7,i),2)%></td>
			  <td align="left"><%=displayBristishDate(recarray(5,i))%></td>
			  <td align="left"><%=recarray(6,i)%></td>
			  <td align="left"><% if (recarray(8,i)<>"") then %>&pound; <% end if%><%=FormatNumber(recarray(8,i),2)%></td>
			 <td align="right">&pound;<%=FormatNumber(dbcharge,2)%></td>
          </tr>
		  <%
            next
          else
		  %>
          <tr>
            <td colspan="10" bgcolor="#FFFFFF" class="errortext" align="center" height="40" ><b>There are no records found...</b></td>
          </tr>
		  <%
          end if
		  %>
      </table>
    </center>
<br>

  <table width="720" border="0" cellspacing="1" cellpadding="2" align="center">
  <tr> 
  <% if clng(vSessionpage) <> 1 then %>
  <td align="center">
    <form name="frmFirstPage" action="CustomerStatement.asp" method="post">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
	<input type="hidden" name="CurrentPage" value="<%=vSessionpage%>">
    <input type="hidden" name="Direction" value="First">
    <input type="submit" name="submit" value="First Page">
    <input type="hidden" name="txtfrom" value="<%=fromdt%>">
    <input type="hidden" name="txtto" value="<%=todt%>">
    <input type="hidden" value="<%=cno%>" name="txtcno">
    <input type="hidden" value="<%=cname%>" name="txtcname">
    <input type="hidden" value="<%=ordno%>" name="txtordno">
	
	<input type="hidden" value="<%=crno%>" name="txtcrno">
	<input type="hidden" value="<%=pono%>" name="txtpono">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) < clng(totpages) then %>
  <td align="center">
    <form name="frmNextPage" action="CustomerStatement.asp" method="post">
    <input type="hidden" name="Direction" value="Next">
	<input type="hidden" name="CurrentPage" value="<%=vSessionpage%>">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Next Page">
    <input type="hidden" name="txtfrom" value="<%=fromdt%>">
    <input type="hidden" name="txtto" value="<%=todt%>">
    <input type="hidden" value="<%=cno%>" name="txtcno">
    <input type="hidden" value="<%=cname%>" name="txtcname">
    <input type="hidden" value="<%=ordno%>" name="txtordno">
	
	<input type="hidden" value="<%=crno%>" name="txtcrno">
	<input type="hidden" value="<%=pono%>" name="txtpono">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) > 1 then %>
  <td align="center">
    <form name="frmPreviousPage" action="CustomerStatement.asp" method="post">
    <input type="hidden" name="Direction" value="Previous">
	<input type="hidden" name="CurrentPage" value="<%=vSessionpage%>">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Previous Page">
    <input type="hidden" name="txtfrom" value="<%=fromdt%>">
    <input type="hidden" name="txtto" value="<%=todt%>">
    <input type="hidden" value="<%=cno%>" name="txtcno">
    <input type="hidden" value="<%=cname%>" name="txtcname">
    <input type="hidden" value="<%=ordno%>" name="txtordno">
	
	<input type="hidden" value="<%=crno%>" name="txtcrno">
	<input type="hidden" value="<%=pono%>" name="txtpono">
    </form>
  </td>
  <%end if%>
  <%if clng(vSessionPage) <> clng(totpages) and clng(totpages) > 0 then %>
  <td align="center">
    <form name="frmLastPage" action="CustomerStatement.asp" method="post">
    <input type="hidden" name="Direction" value="Last">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
	<input type="hidden" name="CurrentPage" value="<%=vSessionpage%>">
    <input type="submit" name="submit" value="Last Page">
    <input type="hidden" name="txtfrom" value="<%=fromdt%>">
    <input type="hidden" name="txtto" value="<%=todt%>">
    <input type="hidden" value="<%=cno%>" name="txtcno">
    <input type="hidden" value="<%=cname%>" name="txtcname">
    <input type="hidden" value="<%=ordno%>" name="txtordno">
	
	<input type="hidden" value="<%=crno%>" name="txtcrno">
	<input type="hidden" value="<%=pono%>" name="txtpono">
    </form>
  </td>
  <% end if%>
  </tr>
  </table>
  <br>
   </td>
  </tr>
</table>
</div>
</body>
</html>
