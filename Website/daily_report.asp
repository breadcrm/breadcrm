<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayFacility()
vFacarray =  detail("Facility")

set detail = object.DisplayTypeDes()	
vType1Array =  detail("Type3ForPacking")
vType2Array =  detail("Type4ForPacking") 

set detail = object.DisplayVec()
vVecarray =  detail("Vec")

set detail = Nothing
set object = Nothing


strDate = Day(date()) & "/" & Month(date()) & "/" & Year(date())
arSplit = Split(strDate,"/")
strDate = Right( "0" & arSplit(0),2) & "/" & Right( "0" & arSplit(1),2) & "/" & arSplit(2)

%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(theForm,theForm2){
	t=new Date();
	theForm2.value=t.getDate() +'/'+eval((t.getMonth())+1) +'/'+ t.getFullYear();
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()

}
function ChangeType4ForMultipleProductByCustomer(type3)
{
	if (type3==9)
	{
		document.getElementById("Type5").style.display = "";
		var oForm = document.forms["form14"];
		oForm.elements["Type2"].style.display = "none"; // hide
		oForm.elements["Type2"].value="";
	}
	else
	{
		document.getElementById("Type5").style.display = "none"; // hide
		var oForm = document.forms["form14"];
		oForm.elements["Type2"].style.display = "";
		document.getElementById("Type5").value=""
	}
}

function ChangeType4ForMultipleProductByProduct(type3)
{
	if (type3==9)
	{
		document.getElementById("Type6").style.display = "";
		var oForm = document.forms["form15"];
		oForm.elements["Type4"].style.display = "none"; // hide
		oForm.elements["Type4"].value="";
	}
	else
	{
		document.getElementById("Type6").style.display = "none"; // hide
		var oForm = document.forms["form15"];
		oForm.elements["Type4"].style.display = "";
		document.getElementById("Type4").value=""
	}
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30)
	{
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}

function NewWindowOpen(deldate)
{
	window.open('ExportToCSV_File_Daily_Electronic_Invoices.asp?deldate='+ deldate,'_blank','left=300 top=200 height=215 width=475')
}

//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal(document.form1,document.form1.deldate);initValueCal(document.form2,document.form2.deldate);initValueCal(document.form4,document.form4.deldate);initValueCal(document.form6,document.form6.deldate);initValueCal(document.form7,document.form7.deldate);initValueCal(document.form8,document.form8.deldate);initValueCal(document.form9,document.form9.deldate);initValueCal(document.form10,document.form10.deldate);initValueCal(document.form11,document.form11.deldate);initValueCal(document.form12,document.form12.deldate);initValueCal(document.form13,document.form13.deldate);initValueCal(document.form14,document.form14.deldate);initValueCal(document.form15,document.form15.deldate);initValueCal(document.form16,document.form16.deldate);initValueCal(document.form17,document.form17.deldate);initValueCal(document.form18,document.form18.deldate);initValueCal(document.form19,document.form19.deldate);ChangeType4ForMultipleProductByCustomer(6);ChangeType4ForMultipleProductByProduct(6)">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">View Reports<br>
      &nbsp;</font></b>

      <table border="0" width="900" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" height="155" bgcolor="#999999" >
        <tr height="30" bgcolor="#ffffff">
          <td width="380"><b>Type of Report</b></td>
          <td width="76" align="center"><b>Delivery Type</b></td>
          <td width="318" align="center"><b>Delivery Date</b></td>
          <td width="105" align="center"><b>Action</b></td>
        </tr>
        <form method = "post" action="daily_report_order.asp" name="form1" target = "_blank">
        <tr>
          <td bgcolor="#E1E1E1" width="380">
		  <table border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		  <tr>
			<td bgcolor="#E1E1E1" valign="middle">Ordering sheet to&nbsp;&nbsp;</td>
			<td bgcolor="#E1E1E1" valign="top">
		     <select size="3" multiple="multiple" name="facility" style="font-family: Verdana; font-size: 8pt">
			 <%
			 strFacarray=""
			 for i = 0 to ubound(vFacarray,2)
		     	if vFacarray(0,i)<> "33" and vFacarray(0,i)<> "90" then
					strFacarray=strFacarray & vFacarray(0,i) & ", "
				end if
			 next
			 %>
			 <option value="<%=strFacarray%>" selected>All</option>
		     <%
			  for i = 0 to ubound(vFacarray,2)%>
		     	<%if vFacarray(0,i)<> "33" and vFacarray(0,i)<> "90" then%>
                <option value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
                <%
				end if
				%>
             <%
			  next
			 %>
			  <option value="0">Stanmore All</option>


             </select>
          	</td>
			</tr>
			</table>
			</td>
		  <td  bgcolor="#E1E1E1" width="76">
          <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          <td bgcolor="#E1E1E1" width="318"><nobr>
           <%
		dim arrayMonth,i
		arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
	    %>
		<select name="day" onChange="setCal(this.form,document.form1.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form1.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form1.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt type="text" size="10" name="deldate" onFocus="blur();" onChange="setCalCombo(document.form1,document.form1.deldate)">
	  <IMG id=f_trigger_fr onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate1" onChange="document.form1.deldate.value=document.form1.deldate1.value">
	  </nobr></TD>

		  <td bgcolor="#E1E1E1" height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>

		</form>
        <form method = "post" action="daily_report_invoice.asp" name="form2" target = "_blank">
        <tr bgcolor="#ffffff">
          <td width="380">Invoices to Customers</td>
          <td width="76">
           <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
         <td width="318">
		 <nobr>
     	<select name="day" onChange="setCal(this.form,document.form2.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form2.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form2.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt type="text" size="10" name="deldate" onFocus="blur();" onChange="setCalCombo(document.form2,document.form2.deldate)">
	  <IMG id=f_trigger_fr2 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate2" onChange="document.form2.deldate.value=document.form2.deldate2.value;setCalCombo(document.form2,document.form2.deldate)">
	 </nobr>
	 </TD>
	  <td height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
        <!--
        <form method = "post" action="daily_report_van.asp" name="form3" target = "_blank">
        <tr>
          <td width="401" bgcolor="#E1E1E1">Reports to Drivers
            <select size="1" name="van" style="font-family: Verdana; font-size: 8pt">
			<%for i = 0 to ubound(vVecarray,2)%>
               <option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
            <%next%>
            </select>
          </td>
          <td  bgcolor="#E1E1E1" width="70">
            <Select size="1" name="deltype" style="font-family: Verdana; font-size: 8pt">
              <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
            </select>
          </td>
          <td bgcolor="#E1E1E1" width="340">
          <input type="text" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=strDate%>" onfocus="blur();"><a href="javascript:onClick=calpopup('calendar.asp?Page=BODY&amp;Form=form3&amp;Element=deldate')"><img border="0" src="images/cal.gif" WIDTH="21" HEIGHT="19"></a></td>
          <td bgcolor="#E1E1E1" height="21" width="106">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
		-->
        <form method = "post" action="daily_report_credit.asp" name="form4" target = "_blank">
        <tr bgcolor="#E1E1E1" height="30">
          <td width="380" bgcolor="#E1E1E1">Credit to Customers</td>
          <td width="76" bgcolor="#E1E1E1">&nbsp;</td>
          <td bgcolor="#E1E1E1" width="318">
		<nobr>
		<select name="day" onChange="setCal(this.form,document.form4.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form4.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form4.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(this.form,document.form4.deldate)">
	  <IMG id=f_trigger_fr4 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate4" onChange="document.form4.deldate.value=document.form4.deldate4.value;setCalCombo(document.form4,document.form4.deldate)">
	  </nobr>
	  </TD><td height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
        <!--
        <form method = "post" action="daily_report_pack.asp" name="form5" target = "_blank">
        <tr>
          <td width="401" bgcolor="#E1E1E1">Packing Sheet Report
            <select size="1" name="van" style="font-family: Verdana; font-size: 8pt">
			<%for i = 0 to ubound(vVecarray,2)%>
               <option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
            <%next%>
            </select>
          </td>
          <td  bgcolor="#E1E1E1" width="70">
            <Select size="1" name="deltype" style="font-family: Verdana; font-size: 8pt">
              <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
            </select>
          </td>
          <td bgcolor="#E1E1E1" width="340">
          <input type="text" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=strDate%>" onfocus="blur();"><a href="javascript:onClick=calpopup('calendar.asp?Page=BODY&amp;Form=form5&amp;Element=deldate')"><img border="0" src="images/cal.gif" WIDTH="21" HEIGHT="19"></a></td>
          <td bgcolor="#E1E1E1" height="21" width="106">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
        -->
        <form method = "post" action="daily_report_sourdough.asp" name="form6" target = "_blank">
        <tr bgcolor="#ffffff">
          <td width="380">48 Hours Report
          <select size="1" name="facility" style="font-family: Verdana; font-size: 8pt">
			<%
			for i = 0 to ubound(vFacarray,2)
			%>  
               <option value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
			<%
            next
			%>
            </select>
          </td>
          <td width="76">
          <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          <td width="318">
		<nobr>
		<select name="day" onChange="setCal(this.form,document.form6.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form6.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form6.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(this.form,document.form6.deldate)">
	  <IMG id=f_trigger_fr6 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate6" onChange="document.form6.deldate.value=document.form6.deldate6.value;setCalCombo(document.form6,document.form6.deldate)">
	 </nobr>
	 </TD>
	  <td height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
		
		<!--26 Jan 2010 START-->
		
		
		<!--26 Jan 2010 END-->
        <form method = "post" action="daily_report_orderbycust.asp" name="form7" target = "_blank">
        <tr>
          <td width="380" bgcolor="#E1E1E1">
		   <table border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		    <tr>
			<td bgcolor="#E1E1E1" valign="middle" >Ordering Sheet by Customer&nbsp;&nbsp;</td>
			<td bgcolor="#E1E1E1" valign="top">
		     <select size="3" name="facility" multiple="multiple" style="font-family: Verdana; font-size: 8pt">
			  <%
			 strFacarray=""
			 for i = 0 to ubound(vFacarray,2)
		     	strFacarray=strFacarray & vFacarray(0,i) & ", "
			 next
			 %>
			 <option value="<%=strFacarray%>" selected>All</option>
			<%for i = 0 to ubound(vFacarray,2)%>
               <option value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
			<%next%>
            </select>
          	</td>
			</tr>
			</table>
		</td>
          <td  bgcolor="#E1E1E1" width="76">
            <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
         <td bgcolor="#E1E1E1" width="318">
		<nobr>
		<select name="day" onChange="setCal(this.form,document.form7.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form7.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>

		<select name="year" onChange="setCal(this.form,document.form7.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(this.form,document.form7.deldate)">
	  <IMG id=f_trigger_fr7 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate7" onChange="document.form7.deldate.value=document.form7.deldate7.value;setCalCombo(document.form7,document.form7.deldate)">
	 </nobr>
	 </TD>
	  <td bgcolor="#E1E1E1" height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
        <form method = "post" action="daily_report_orderbyvan.asp" name="form8" target = "_blank">
        <tr bgcolor="#ffffff">
          <td width="380">Ordering Sheet by Van
			<select size="1" name="facility" style="font-family: Verdana; font-size: 8pt">
			<%for i = 0 to ubound(vFacarray,2)
			  'if vFacarray(0,i) = "30" or  vFacarray(0,i) = "31" or vFacarray(0,i) = "32"  then%>
               <option value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option><%
             ' end if
             next%>
            </select>
          </td>
          <td width="76">
           <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          <td width="318">
		<nobr>
		<select name="day" onChange="setCal(this.form,document.form8.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form8.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>

		<select name="year" onChange="setCal(this.form,document.form8.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(this.form,document.form8.deldate)">
	  <IMG id=f_trigger_fr8 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate8" onChange="document.form8.deldate.value=document.form8.deldate8.value;setCalCombo(document.form8,document.form8.deldate)">
	 </nobr>
	  </TD> <td height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
        <form method = "post" action="daily_report_packsheet.asp" name="form9" target = "_blank">
        <tr>
          <td width="380" bgcolor="#E1E1E1" valign="top">
		  <table border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		  <tr>
			<td bgcolor="#E1E1E1" valign="middle" >Packing Sheet Report&nbsp;&nbsp;</td>
			<td bgcolor="#E1E1E1" valign="top">
			<Select size="3" name="facility" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
		      <%
			  strFacarray=""
			  for i = 0 to ubound(vFacarray,2)
		     	strAllFacarray=strAllFacarray & vFacarray(0,i) & ","
              next
			  %>
			 <option value="<%=strAllFacarray%>" selected="selected">All</option>
			 <%for i = 0 to ubound(vFacarray,2)%>		     	
                <option value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
             <%next%>
             </select>
			</td>
		  </tr>
		</table>


          </td>
          <td  bgcolor="#E1E1E1" width="76">
           <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="Morning, Noon, Evening" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          <td bgcolor="#E1E1E1" width="318">
		<nobr>
		<select name="day" onChange="setCal(this.form,document.form9.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form9.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>

		<select name="year" onChange="setCal(this.form,document.form9.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(this.form,document.form9.deldate)">
	  <IMG id=f_trigger_fr9 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate9" onChange="document.form9.deldate.value=document.form9.deldate9.value;setCalCombo(document.form9,document.form9.deldate)">
	  </nobr>
	  </TD> <td bgcolor="#E1E1E1" height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
        <form method = "post" action="daily_report_delivery.asp" name="form10" target = "_blank">
        <tr bgcolor="#ffffff">
          <td width="380">
		  <table border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		  <tr >
			<td valign="middle" >Delivery Sheet&nbsp;&nbsp;</td>
			<td valign="top">

            <Select size="3" name="van" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
			<%
			strAllVecarray
			for i = 0 to ubound(vVecarray,2)
				strAllVecarray=strAllVecarray & vVecarray(0,i) & ","
			next
			%>
			<option value="<%=strAllVecarray%>" selected="selected">All</option>
			<%for i = 0 to ubound(vVecarray,2)%>
               <option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
            <%next%>

            </select>
			
          </td>
		  </tr>
		  </table>
		  </td>
          <td width="76">
           <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          <td width="318">
		  <nobr>
        <select name="day" onChange="setCal(this.form,document.form10.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form10.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form10.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(this.form,document.form10.deldate)">
	  <IMG id=f_trigger_fr10 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate10" onChange="document.form10.deldate.value=document.form10.deldate10.value;setCalCombo(document.form10,document.form10.deldate)">
	  </nobr>
	  </TD><td height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
        <form method = "post" action="daily_report_vancheck.asp" name="form11" target = "_blank">
        <tr>
          <td width="380" bgcolor="#E1E1E1">Van Check List
            <select size="3" name="van" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
			<%for i = 0 to ubound(vVecarray,2)%>
               <option value="<%=vVecarray(0,i)%>" <%if vVecarray(0,i) = "1" then%>Selected <%End If %> ><%=vVecarray(0,i)%></option>
            <%next%>
            </select>
            
             <select size="3" multiple="multiple" name="facility" style="font-family: Verdana; font-size: 8pt">
			 <%
			 strFacarray=""
			 for i = 0 to ubound(vFacarray,2)
		     	if vFacarray(0,i)<> "33" and vFacarray(0,i)<> "90" then
					strFacarray=strFacarray & vFacarray(0,i) & ", "
				end if
			 next
			 %>
			 <option value="<%=strFacarray%>" selected>All</option>
		     <%
			  for i = 0 to ubound(vFacarray,2)%>
		     	<%if vFacarray(0,i)<> "33" and vFacarray(0,i)<> "90" then%>
                <option value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
                <%
				end if
				%>
             <%
			  next
			 %>
			  <option value="0">Stanmore All</option>


             </select>
          </td>
          <td  bgcolor="#E1E1E1" width="76">
            <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          <td bgcolor="#E1E1E1" width="318">
		 <nobr>

		<select name="day" onChange="setCal(this.form,document.form11.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form11.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form11.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(this.form,document.form11.deldate)">
	  <IMG id=f_trigger_fr11 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate11" onChange="document.form11.deldate.value=document.form11.deldate11.value;setCalCombo(document.form11,document.form11.deldate)">
	  </nobr>
	  </TD><td bgcolor="#E1E1E1" height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
        <form method = "post" action="daily_report_temporders.asp" name="form12" target = "_blank">
        <tr bgcolor="#ffffff" height="30">
          <td width="380">Temporary Orders</td>
            <td width="76">
         	 <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          <td width="318">

		<nobr>
		<select name="day" onChange="setCal(this.form,document.form12.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form12.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form12.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(document.form12,document.form12.deldate12)">
	  <IMG id=f_trigger_fr12 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate12" onChange="document.form12.deldate.value=document.form12.deldate12.value;setCalCombo(document.form12,document.form12.deldate)">
	  </nobr>
	  </TD><td height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>

		<form method = "post" action="daily_report_sourdough_gail_central_kitchen.asp" name="form13" target = "_blank">
        <tr bgcolor="#E1E1E1" style="display:none;">
          <td width="380">GAIL's central Kitchen
          </td>
          <td width="76">
          <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          <td width="318">
	<nobr>
		<select name="day" onChange="setCal(this.form,document.form13.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form13.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form13.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(this.form,document.form13.deldate)">
	  <IMG id=f_trigger_fr13 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate13" onChange="document.form13.deldate.value=document.form13.deldate13.value;setCalCombo(document.form13,document.form13.deldate)">
	</nobr>
	 </TD>
	  <td height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>





<!--New Report for Checking purpose starting here 


        <form method = "post" action="daily_report_new_temporders.asp" name="form13" target = "_blank">
        <tr bgcolor="#E1E1E1" height="30">
          <td width="380"><font color="#000066">Temporary Orders (New)</font></td>
            <td  bgcolor="#E1E1E1" width="76">
          <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          <td width="318">


		<select name="day" onChange="setCal(this.form,document.form13.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form13.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form13.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(document.form13,document.form13.deldate13)">
	  <IMG id=f_trigger_fr13 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate13" onChange="document.form13.deldate.value=document.form13.deldate13.value;setCalCombo(document.form13,document.form13.deldate)">
	  </TD><td height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>

New Report for Checking purpose ending here
-->

 <form method = "post" action="daily_report_packing_sheet_filter_types.asp" name="form14" id="form14" target = "_blank">
        <tr bgcolor="#ffffff" height="50" style="display:none;">
          <td width="380" height="50">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		  <tr>
			<td height="25">
			Products by Customer Packing Sheet  </td>
		  </tr>
		  <tr >
			<td height="25">
			    Type3: <select size="1" name="Type1" style="font-family: Verdana; font-size: 8pt" onChange="ChangeType4ForMultipleProductByCustomer(this.value)">
					<option value="">Select</option>
					<%for i = 0 to ubound(vType1Array,2)%>  				
                              <option value="<%=vType1Array(0,i)%>"><%=vType1Array(1,i)%></option>
                  	<%next%> 
           </select>
		    Type4: 
		    <select size="1" name="Type2" id="Type2" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vType2Array,2)%>  				
                              <option value="<%=vType2Array(0,i)%>"><%=vType2Array(1,i)%></option>
                  	<%next%> 
           </select>
		   <select size="1" name="Type5" id="Type5" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					  				
                                <option value="22">BMG</option>
                  	  				
                              <option value="21">Bread</option>
                  	  				
                              
                  	  				
                              <option value="23">Cake</option>
                  	  				
                             
                  	  				
                              <option value="24">CK</option>
                  	  				
                             
                              <option value="25">Gails Supply</option>
                  	  				
                             
                  	 
           </select>

			</td>
		  </tr>
		</table>

		   </td>
            <td width="76">
         	<Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          <td width="318">
			<nobr>

		<select name="day" onChange="setCal(this.form,document.form14.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form14.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form14.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(document.form14,document.form14.deldate14)">
	  <IMG id=f_trigger_fr14 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate14" onChange="document.form14.deldate.value=document.form14.deldate14.value;setCalCombo(document.form14,document.form14.deldate)">
	  </nobr>
	  </TD><td height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
  
  
  <!--Burger Bun report -->
  
  <form method = "post" action="daily_report_Burger_Bun.asp" name="form15" target = "_blank">
        <tr bgcolor="#E1E1E1" height="50" style="display:none;">
          <td width="380" height="50">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		  <tr>
			<td height="25">
			Customer by Products Packing Sheet</td>
		  </tr>
		  <tr >
			<td height="25">
			Type3: 
		      <select size="3" name="Type3" id="Type3" multiple="multiple" style="font-family: Verdana; font-size: 8pt" onChange="ChangeType4ForMultipleProductByProduct(this.value)">
					<option value="">Select</option>
					<%for i = 0 to ubound(vType1Array,2)%>  				
                              <option value="<%=vType1Array(0,i)%>"><%=vType1Array(1,i)%></option>
                  	<%next%> 
           </select>
		    Type4: 
		    <select size="3" name="Type4" id="Type4" multiple="multiple" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vType2Array,2)%>  				
                              <option value="<%=vType2Array(0,i)%>"><%=vType2Array(1,i)%></option>
                  	<%next%> 
           </select>
		   
		    <select name="Type6" id="Type6" multiple="multiple" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					  				
                                <option value="22">BMG</option>
                  	  				
                              <option value="21">Bread</option>
                  	  				
                              
                  	  				
                              <option value="23">Cake</option>
                  	  				
                             
                  	  				
                              <option value="24">CK</option>
                  	  				
                             
                              <option value="25">Gails Supply</option>
                  	  				
                             
                  	 
           </select>
			</td>
		  </tr>
		</table>

		   </td>
            <td width="76">
         	<Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          <td width="318">
			<nobr>

		<select name="day" onChange="setCal(this.form,document.form15.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form15.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form15.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(document.form15,document.form15.deldate15)">
	  <IMG id=f_trigger_fr15 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate15" onChange="document.form15.deldate.value=document.form15.deldate15.value;setCalCombo(document.form15,document.form15.deldate)">
	  </nobr>
	  </TD><td height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
  
  		<form method = "post" action="daily_report_sourdough_wsgail_central_kitchen.asp" name="form16" target = "_blank">
        <tr bgcolor="#ffffff" style="display:none;">
          <td width="380">Whole Sale GAIL's central Kitchen
          </td>
          <td width="76">
          <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          <td width="318">
			<nobr>
		<select name="day" onChange="setCal(this.form,document.form16.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form16.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form16.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(this.form,document.form16.deldate)">
	  <IMG id=f_trigger_fr16 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate16" onChange="document.form16.deldate.value=document.form16.deldate16.value;setCalCombo(document.form16,document.form16.deldate)">
	 </nobr>
	 </TD>
	  <td height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
		
  		<!-- Generate Electronic Invoices Start - 17 -->
  		<form name="form17" >
        <tr bgcolor="#E1E1E1" height="30" style="display:none;">
        	<td width="380" bgcolor="#E1E1E1">Generate Electronic Invoices</td>
          	<td width="76" bgcolor="#E1E1E1">&nbsp;</td>
          	<td bgcolor="#E1E1E1" width="318">
			<nobr>
			<select name="day" onChange="setCal(this.form,document.form17.deldate)">
			  <%for i=1 to 31%>
				<option value="<%=i%>"><%=i%></option>
			  <%next%>
			</select>
			<select name="month" onChange="setCal(this.form,document.form17.deldate)">
			   <%for i=0 to 11%>
				<option value="<%=i%>"><%=arrayMonth(i)%></option>
			  <%next%>
			</select>
			<select name="year" onChange="setCal(this.form,document.form17.deldate)">
			  <%for i=2000 to Year(Date)+1%>
				<option value="<%=i%>"><%=i%></option>
			  <%next%>
			</select>
	  		<INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(this.form,document.form17.deldate)">
	  		<IMG id=f_trigger_fr17 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  		<INPUT type="hidden" name="deldate17" onChange="document.form17.deldate.value=document.form17.deldate17.value;setCalCombo(document.form17,document.form17.deldate)">
	  		</nobr>
			</td>
			<td height="21" width="105"><input type="button" onClick="NewWindowOpen(document.form17.deldate.value)" value="Generate" name="B1" style="font-family: Verdana; font-size: 8pt; width:102px"></td>
        </tr>
        </form>
		<!-- Generate Electronic Invoices End -->

		<!-- Slicing Report Begin - 18 -->
		
		<form method = "post" action="daily_reports_slicing.asp" name="form18" target = "_blank">
        <tr bgcolor="#ffffff" style="display:none;">
          <td width="380">Slicing Sheets Report
		   <Select name="tempornormalorder" style="font-family: Verdana; font-size: 8pt">
              <option value="Normal">Normal</option>
              <option value="Temporary">Temporary</option>
			</select>
		  
          </td>
          <td width="76">
          <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          <td width="318">
			<nobr>
		<select name="day" onChange="setCal(this.form,document.form18.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form18.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form18.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt size="10" id=deldate  name="deldate" onFocus="blur();" onChange="setCalCombo(this.form,document.form18.deldate)">
	  <IMG id=f_trigger_fr18 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate18" onChange="document.form18.deldate.value=document.form18.deldate18.value;setCalCombo(document.form18,document.form18.deldate)">
	 </nobr>
	 </TD>
	  <td height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
		<!-- Slicing Report End -->
		
		<!-- VC Product Report Begin -->
		<form method = "post" action="daily_report_VCProduct.asp" name="form19" target = "_blank">
        <tr style="display:none;">
        <td bgcolor="#E1E1E1" width="380">
		  <table border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		  <tr>
			<td bgcolor="#E1E1E1" valign="middle">VC Product&nbsp;&nbsp;</td>
			<td bgcolor="#E1E1E1" valign="top">
		     <select size="3" multiple="multiple" name="facility" style="font-family: Verdana; font-size: 8pt">
			 <%
			 stop
			 strFacarray=""
			 for i = 0 to ubound(vFacarray,2)
		     	if vFacarray(0,i)<> "33" and vFacarray(0,i)<> "90" then
					
					if strFacarray <> "" Then
					    strFacarray=strFacarray & ", " & vFacarray(0,i) 
					    
					else
					    strFacarray= vFacarray(0,i) 
					End If
					
				end if
			 next
			 %>
			 <option value="<%=strFacarray%>" selected>All</option>
		     <%
			  for i = 0 to ubound(vFacarray,2)%>
		     	<%if vFacarray(0,i)<> "33" and vFacarray(0,i)<> "90" then%>
                <option value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
                <%
				end if
				%>
             <%
			  next
			 %>
			 <option value="0">Stanmore All</option>


             </select>
          	</td>
			</tr>
			</table>
			</td>
			
			 <td  bgcolor="#E1E1E1" width="76">
          <Select size="3" name="deltype" style="font-family: Verdana; font-size: 8pt" multiple="multiple">
              <option value="All" selected="selected">All</option>
			  <option value="Morning">Morning</option>
              <option value="Noon">Noon</option>
              <option value="Evening">Evening</option>
			</select>
          </td>
          
          <td bgcolor="#E1E1E1" width="318"><nobr>
           <%
		dim arrayMonth2
		arrayMonth2=Array("January","February","March","April","May","June","July","August","September","October","November","December")
	    %>
		<select name="day" onChange="setCal(this.form,document.form19.deldate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.form19.deldate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth2(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.form19.deldate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt type="text" size="10" name="deldate" onFocus="blur();" onChange="setCalCombo(document.form19,document.form19.deldate)">
	  <IMG id=f_trigger_fr19 onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <INPUT type="hidden" name="deldate19" onChange="document.form19.deldate.value=document.form19.deldate19.value;setCalCombo(document.form19,document.form19.deldate)">
	  </nobr></TD>
		
		 <td bgcolor="#E1E1E1" height="21" width="105">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
			
        </tr>
        </form>
		
		<!-- VC Product Report End -->
		
		<br>
		 <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate2",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr2",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate4",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr4",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate6",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr6",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate7",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr7",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate8",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr8",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate9",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr9",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate10",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr10",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate11",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr11",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate12",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr12",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate13",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr13",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate14",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr14",
				singleClick    :    true
			});
		</SCRIPT>
		
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate15",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr15",
				singleClick    :    true
			});
		</SCRIPT>
						
		<SCRIPT type=text/javascript>
		    Calendar.setup({
		        inputField: "deldate16",
		        ifFormat: "%d/%m/%Y",
		        button: "f_trigger_fr16",
		        singleClick: true
		    });
		</SCRIPT>
		
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate17",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr17",
				singleClick    :    true
			});
		</SCRIPT>
		
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "deldate18",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr18",
				singleClick    :    true
			});
		</SCRIPT>
		
		<SCRIPT type=text/javascript>
		    Calendar.setup({
		        inputField    : "deldate19",
		        ifFormat      : "%d/%m/%Y",
		        button        : "f_trigger_fr19",
		        singleClick   : true
		    });
		</SCRIPT>
		
		
      </table>
    </td>
  </tr>
</table>
</center>
</div>
<br><br>
</body>
</html>