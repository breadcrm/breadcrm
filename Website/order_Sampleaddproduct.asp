<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim Obj,ObjProduct,ObjResult
Dim arProduct
Dim strProdName,strProdCode
Dim vPageSize
Dim strDirection
Dim vpagecount
Dim vCurrentPage
Dim i
Dim vOrderNo
Dim vQty
Dim strAddProdCode
Dim vNewSampleOrdTag
Dim strDate
Dim vTripNo
Dim vCustNo
Dim strSample
Dim strProcTag
Dim strOrderType 
dim strBgColour

session("txtDate") = Request.Form("txtDate")

vPageSize = 999999
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vCurrentPage	= Request.Form("CurrentPage") 
strProdName = Replace(Request.Form("txtProdName"),"'","''")
strProdCode = Replace(Request.Form("txtProdCode"),"'","''")
vOrderNo = Request.Form("OrderNo") 


if Trim(vOrderNo) = "" Then
	vOrderNo = 0 
End if


'New order details
vNewSampleOrdTag = Trim(Request.Form("NewSampleOrdTag"))  
vCustNo = Request.Form("CustNo") 
strDate = Request.Form("txtDate") 
vTripNo = Request.Form("selTrip") 	
strSample = Request.Form("radSample") 
strOrderType = Request.Form("OrderType")			

stop

Set Obj = CreateObject("Bakery.Orders")
Obj.SetEnvironment(strconnection)
if Trim(Request.Form("SampleUpdate")) <> "" Then
	if Trim(Request.Form("txtqty")) <> "" And IsNumeric(Request.Form("txtqty")) Then
		vQty = Request.Form("txtqty")
		
		if vNewSampleOrdTag = "True" Then 'Add New Order
			strProcTag =  session("processed")	
			'ObjResult = obj.Display_OrderDateCheck(strDate,strProcTag)
			'if ObjResult <> "OK" Then
			'	Response.Redirect "order_sample1.asp?ErrTag=Yes&CustNo="&vCustNo&"&txtDate="&strDate&"&OrderType="&Server.URLEncode(strOrderType)&"&radSample="&strSample
			'End if
		
			ObjResult = obj.OrderExistCheck(vCustNo,strDate,vTripNo,strSample)	
			if ObjResult <> "OK" Then
				Response.Redirect "order_sample1.asp?ErrTag=Yes&CustNo="&vCustNo&"&txtDate="&strDate&"&ExistOrdTag=Yes&radSample="&strSample&"&OrderType="&Server.URLEncode(strOrderType)
			End if	
			
			'Response.Write vCustNo & "," & strDate & "," & vTripNo & "," & strSample
			'Response.End 
			
			Set ObjProduct = obj.Add_Order(vCustNo,strDate,vTripNo,strSample)
			vOrderNo = ObjProduct("OrderNo")
		End if		
		
		strAddProdCode = Request.Form("txtProdCode") 				
		ObjResult = obj.Update_AdditionalTypicalOrderItem(vOrderNo,strAddProdCode,vQty) 
		
		if ObjResult = "OK" Then
			Response.Redirect "order_sample1.asp?OrderNo="&vOrderNo
		end if
	End if 
End if  


If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if

'Response.Write vPageSize & "," & vCurrentPage & "," & strProdCode & "," & strProdName & "," & vOrderNo


set ObjProduct = obj.Display_AdditionalTypicalOrderList(vPageSize,vCurrentPage,strProdCode,strProdName,vOrderNo) 
arProduct = ObjProduct("Product")
vpagecount  = ObjProduct("Pagecount")

Set Obj = Nothing
Set ObjProduct = Nothing
%>



<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Add Product<br>
      &nbsp;</font></b>
      <form name="frmAddSearch" method="post">
				<table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
				  <tr>
				    <td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
				    <td><b>Product code:</b></td>
				    <td><b>Product Name:</b></td>
				    <td></td>
				  </tr>
				  <tr>
				    <td></td>
				    <td><input type="text" name="txtProdCode" value="<%=strProdCode%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
				    <td><input type="text" name="txtProdName" value="<%=strProdName%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
				    <td><input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
				  </tr>
				  <tr>
				    <td><b>&nbsp;</b></td>
				    <td></td>
				    <td></td>
				    <td></td>
				  </tr>
				</table>
					<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
					<input type="hidden"  name="CustNo"  value="<%=vCustNo%>">
					<input type="hidden"  name="txtDate"  value="<%=strDate%>">
					<input type="hidden"  name="selTrip"  value="<%=vTripNo%>">
					<input type="hidden"  name="radSample"  value="<%=strSample%>">							
					<input type="hidden"  name="NewSampleOrdTag"  value="<%=vNewSampleOrdTag%>">				
					<input type="hidden"  name="OrderType"  value="<%=strOrderType%>">				
      </Form>
      
      	<!--
        <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">      
				<tr>
					<td height="20"></td>
				</tr><%				
				if IsArray(arProduct) Then%>
					<tr>
					  <td align="right"><b>Page <%=vCurrentPage%> of <%=vpagecount%></b></td>          
					</tr><%
				End if%>	
			</table>
		-->
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="25%" bgcolor="#CCCCCC"><b>Product Code</b></td>
          <td width="25%" bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td width="25%" bgcolor="#CCCCCC"><b>Qty</b></td>
          <td width="25%" bgcolor="#CCCCCC"><b>Action</b></td>
        </tr><%
        If (not (strProdCode= "" and strProdName="")) and IsArray(arProduct) Then
					For I = 0 to UBound(arProduct,2)
						if (i mod 2 =0) then
							strBgColour="#FFFFFF"
						else
							strBgColour="#F3F3F3"
						end if					
					%>      
					  
						<FORM name="frmAddProd<%=i%>" method="post">
							<tr  bgcolor="<%=strBgColour%>">
							  <td><%=arProduct(0,i)%></td>
							  <td><%=arProduct(1,i)%></td>
							  <td><input type="text" name="txtQty" size="7" style="font-family: Verdana; font-size: 8pt" maxlength="6"></td>
							  <td><input type="submit" value="Add Product" name="B1" style="font-family: Verdana; font-size: 8pt"></td>							  
							</tr>
							<input type="hidden"  name="txtProdCode"  value="<%=arProduct(0,i)%>">
							<input type="hidden"  name="SampleUpdate"  value="True">
							<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
														
							<input type="hidden"  name="CustNo"  value="<%=vCustNo%>">
							<input type="hidden"  name="txtDate"  value="<%=strDate%>">
							<input type="hidden"  name="selTrip"  value="<%=vTripNo%>">
							<input type="hidden"  name="radSample"  value="<%=strSample%>">							
							<input type="hidden"  name="NewSampleOrdTag"  value="<%=vNewSampleOrdTag%>">
							<input type="hidden"  name="OrderType"  value="<%=strOrderType%>">				
						</Form><%						
					Next
				else%>
					<tr>
					  <td Colspan="4">Sorry no items found</td>	
					</tr><%
				End if%>	 
        
        <tr>
          <td></td>
          <td><input onClick="Javascript:document.back.submit();" type="button" value="Go Back" name="Go Back" style="font-family: Verdana; font-size: 8pt"></td>
          <form method="POST" action="order_sample1.asp" name="back">
          <input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
          <input type="hidden"  name="CustNo"   value="<%=vCustNo%>">           
          <td></td>
          <td></td>
          </form>
        </tr>
      </table>
    </td>
  </tr>
</table>

	<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		<FORM Name="Navigate">
			<tr>					
				<td width="187"><%				
		
					if vCurrentPage <> 1 then%>
						<input type="button" value="FirstPage" Onclick="document.frmFirst.submit()" id=button1 name=button1 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) > 1 then%>
						<input type="button" value="PreviousPage" Onclick="document.frmPrevious.submit()" id=button3 name=button3 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%				
					if clng(vCurrentPage) < vPagecount then%>
						<input type="button" value="NextPage" Onclick="document.frmNext.submit()" id=button5 name=button5 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) <> vPagecount and vPagecount > 0  then%>
						<input type="button" value="LastPage" Onclick="document.frmLast.submit()" id=button7 name=button7 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>					
			</tr>
		</FORM>
	</table>
  </center>
</div>


	<FORM METHOD="post" NAME="frmFirst">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="First">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">				
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"			VALUE="<%=strProdCode%>">
		<input type="hidden" name="OrderNo"  value="<%=vOrderNo%>">
		
		<input type="hidden"  name="CustNo"  value="<%=vCustNo%>">
		<input type="hidden"  name="txtDate"  value="<%=strDate%>">
		<input type="hidden"  name="selTrip"  value="<%=vTripNo%>">
		<input type="hidden"  name="radSample"  value="<%=strSample%>">							
		<input type="hidden"  name="NewSampleOrdTag"  value="<%=vNewSampleOrdTag%>">
		<input type="hidden"  name="OrderType"  value="<%=strOrderType%>">				
	</FORM>

	<FORM METHOD="post" NAME="frmPrevious">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Previous">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"			VALUE="<%=strProdCode%>">
		<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
		
		<input type="hidden"  name="CustNo"  value="<%=vCustNo%>">
		<input type="hidden"  name="txtDate"  value="<%=strDate%>">
		<input type="hidden"  name="selTrip"  value="<%=vTripNo%>">
		<input type="hidden"  name="radSample"  value="<%=strSample%>">							
		<input type="hidden"  name="NewSampleOrdTag"  value="<%=vNewSampleOrdTag%>">
		<input type="hidden"  name="OrderType"  value="<%=strOrderType%>">				
	</FORM>

	<FORM METHOD="post" NAME="frmNext">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Next">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"			VALUE="<%=strProdCode%>">
		<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
		
		<input type="hidden"  name="CustNo"  value="<%=vCustNo%>">
		<input type="hidden"  name="txtDate"  value="<%=strDate%>">
		<input type="hidden"  name="selTrip"  value="<%=vTripNo%>">
		<input type="hidden"  name="radSample"  value="<%=strSample%>">							
		<input type="hidden"  name="NewSampleOrdTag"  value="<%=vNewSampleOrdTag%>">
		<input type="hidden"  name="OrderType"  value="<%=strOrderType%>">				
	</FORM>

	<FORM METHOD="post" NAME="frmLast">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Last">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"			VALUE="<%=strProdCode%>">
		<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
		
		<input type="hidden"  name="CustNo"  value="<%=vCustNo%>">
		<input type="hidden"  name="txtDate"  value="<%=strDate%>">
		<input type="hidden"  name="selTrip"  value="<%=vTripNo%>">
		<input type="hidden"  name="radSample"  value="<%=strSample%>">							
		<input type="hidden"  name="NewSampleOrdTag"  value="<%=vNewSampleOrdTag%>">
		<input type="hidden"  name="OrderType"  value="<%=strOrderType%>">				
	</FORM>
  </center>
</div>


</body>

</html><%
If IsArray(arProduct) Then Erase arProduct
%>