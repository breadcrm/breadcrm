<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%
Response.Buffer
Dim Obj,ObjCustomer
Dim arCustomer

Dim strCustomerName,strCustNo,strGroup,FindProduct_ismultipleproduct,ReplacewithProduct_ismultipleproduct
Dim vPageSize
Dim strDirection
Dim vpagecount
Dim vCurrentPage
Dim i,StrOrderType
Dim arGroup


vPageSize = 100000
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vCurrentPage	= Request.Form("CurrentPage") 

strSearch=trim(Request("Search")) 

strCustomerName = Replace(Request.Form("txtCustName"),"'","''")
strCustNo = Replace(Request.Form("txtCustNo"),"'","''")
strGroup = Replace(Request.Form("txtGroup"),"'","''")
strFindProductCode=trim(Request("txtFindProductCode")) 

if strCustNo <> "" Then
	if  not IsNumeric(strCustNo) Then
		strCustNo = 0
	End if
End if

If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if

Set Obj = CreateObject("Bakery.Customer")
Obj.SetEnvironment(strconnection)
if ((strSearch<>"") or (strApply="Apply")) then
	Set ObjCustomer = obj.FindAndReplaceCustomerList(vPageSize,vCurrentPage,strCustomerName,strCustNo,strGroup,StrOrderType,strFindProductCode)
	arCustomer = ObjCustomer("Customer")
end if
set objCustomer = obj.Display_Grouplist()
arGroup = objCustomer("GroupList")	

%>


<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<SCRIPT LANGUAGE="Javascript">
<!--
function checkAll(bx) {
  var cbs = document.getElementsByTagName('input');
  for(var i=0; i < cbs.length; i++) {
    if(cbs[i].type == 'checkbox') {
      cbs[i].checked = bx.checked;
    }
  }
}
function clickcheck()
{
	var blnAll=1
	var cbs1 = document.getElementsByTagName('input');
	for (var i=0;i<cbs1.length;i++)
    {
		if (cbs1[i].checked==false)
		{			
			if(cbs1[i].type=='checkbox')
			{
				if (cbs1[i].name!='allbox')
				{
					blnAll=0;
				}
			}
    	}    	
    }
    if(blnAll==0)
    {
		document.frmpro.allbox.checked=false;
	}
	else
	{
		document.frmpro.allbox.checked=true;
	}
}
//-->
</SCRIPT>
<title></title>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;<b><font size="3">Find and Replace</font></b><br><br>
<div align="center">
  <center>
<table border="0" width="900" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
	  <%
	  	
		strFindProductCodeQty=trim(Request("txtFindProductCodeQty"))
		strReplacewithProductCode=trim(Request("txtReplacewithProductCode")) 
		strReplacewithProductCodeQty=trim(Request("txtReplacewithProductCodeQty"))
	  	strApply=trim(Request("Apply")) 
		if (strApply="Apply") then
		%>
		<p align="center" style="font-family: Verdana; font-size: 12px; color:#FF0000; font-weight:bold">
		<%
			
			if Not isnumeric(strFindProductCodeQty)  then
				strFindProductCodeQty=0
			end if
			
			if Not isnumeric(strReplacewithProductCodeQty)  then
				strReplacewithProductCodeQty=0
			end if			
					
			if (strFindProductCode="") then
			%>
			Please enter the value for "Find Product Code".
			<%				
			elseif (strReplacewithProductCode="") then
			%>
				Please enter the value for "Replace with Product Code".
			<%				
			elseif (strReplacewithProductCode=strFindProductCode) then
			%>
				Please enter the different product code value for value for "Find Product Code" and "Replace with Product Code".
			<%				
			elseif (strFindProductCodeQty=0) then
			%>
				Please enter the value for "Find Product Code" Qty.
			<%
			elseif (strReplacewithProductCodeQty=0) then
			%>
				Please enter the value for "Replace with Product Code" Qty.
			<%
			elseif (obj.IsProductCheck(strFindProductCode)="2") then
			%>
				Product Code "<%=strFindProductCode%>" does not exist in the bread system.
			<%
			elseif (obj.IsProductCheck(strReplacewithProductCode)="2") then
			%>
				Product Code "<%=strReplacewithProductCode%>" does not exist in the bread system.
			<%
			else				
				if IsArray(arCustomer) Then 
					strcus=""
					
					FindProductismultipleproduct=obj.IsProductCheck(strFindProductCode)					
					ReplacewithProductismultipleproduct=obj.IsProductCheck(strReplacewithProductCode)
							
					for i = 0 to UBound(arCustomer,2)
						str="che" & i
						if Request.Form(str)="on" then
							strCustomerNo=arCustomer(0,i)
							
							set result = obj.UpdateStandingOrderProduct(strFindProductCode,strReplacewithProductCode,strCustomerNo,strFindProductCodeQty,strReplacewithProductCodeQty)
							strAction=strFindProductCode & " (qty: " &  strFindProductCodeQty & ") replaced with " & strReplacewithProductCode & " (qty: " &  strReplacewithProductCodeQty & ")"
							LogAction "Std Order - Find and Replace", strAction , strCustomerNo
													
							set result = obj.UpdateTemp_Orderdetail_Product(strFindProductCode,strReplacewithProductCode,strCustomerNo,strFindProductCodeQty,strReplacewithProductCodeQty,FindProductismultipleproduct,ReplacewithProductismultipleproduct)
							strAction=strFindProductCode & " (qty: " &  strFindProductCodeQty & ") replaced with " & strReplacewithProductCode & " (qty: " &  strReplacewithProductCodeQty & ")"
							LogAction "Order - Find and Replace", strAction , strCustomerNo
							
						end if
					next
					if (strCustomerNo="") then
					%>
					Please select customer(s) from below list.	
					<%
					else
					%>
					<p align="center" style="font-family: Verdana; font-size: 12px; color:#009900; font-weight:bold">Successfully replaced product code "<%=strFindProductCode%>" with  "<%=strReplacewithProductCode%>" for customer(s) selected below.</p>
					<%
					end if
				end if
			end if					
	  	%>		
		</p>
		<%
		end if
		%>
      <form method="POST"  id="frmpro" name="frmpro">
	  <INPUT TYPE="hidden" NAME="Search"	VALUE="Search">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
         <tr>
          <td colspan="4">
          	<table border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		  	<tr height="30">
				<td><strong>Find Product Code:&nbsp;</strong></td>
				<td><input type="text" name="txtFindProductCode"  value="<%=strFindProductCode%>"size="20" maxlength="18" style="font-family: Verdana; font-size: 8pt"></td>
				<td width="10"></td>
				<td><strong>Qty:&nbsp;</strong></td>
				<td><input type="text" name="txtFindProductCodeQty"  value="<%=strFindProductCodeQty%>"size="5" maxlength="3" style="font-family: Verdana; font-size: 8pt"></td>
		  	</tr>
			<tr height="30">
				<td><nobr><strong>Replace with Product Code:&nbsp;</strong></nobr></td>
				<td><input type="text" name="txtReplacewithProductCode"  value="<%=strReplacewithProductCode%>"size="20" maxlength="18" style="font-family: Verdana; font-size: 8pt"></td>
				<td width="10"></td>
				<td><strong>Qty:&nbsp;</strong></td>
				<td><input type="text" name="txtReplacewithProductCodeQty"  value="<%=strReplacewithProductCodeQty%>"size="5" maxlength="3" style="font-family: Verdana; font-size: 8pt"></td>
		  	</tr>
			<tr>
				<td height="5"></td>
			</tr>
			</table>
		  </td>
        </tr>
		<tr>
          <td><b>Customer Code:</b></td>
		  <td><b>Customer Name:</b></td>
          <td><b>Customer Group:</b></td>
          <td></td>
        </tr>
        <tr>
        	<td><input type="text" name="txtCustNo"  value="<%=strCustNo%>"size="20" style="font-family: Verdana; font-size: 8pt"></td>
			<td><input type="text" name="txtCustName" value="<%=strCustomerName%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>				
			<td>
				<select size="1" name="txtGroup" style="font-family: Verdana; font-size: 8pt">
				<option value=" ">Select</option><%
					If IsArray(arGroup) then
						For i = 0 to UBound(arGroup,2)%>
							<option value="<%=arGroup(0,i)%>" <%if Trim(arGroup(0,i)) = Trim(strGroup) Then Response.Write "Selected"%>><%=arGroup(1,i)%></option><%
						Next 						
					End if%>
				</select>				    
			</td>
			<td><input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>       
        </tr>
        <%
		if (strSearch<>"") then
		%>
		<tr height="50">
          <td><input type="submit" value="Apply" name="Apply" style="width:70px; font-family: Verdana; font-size: 12px; font-weight:bold; padding:5px"></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
		<%
		end if
		%>
      </table>			
	<%
	if (strSearch<>"") then
	%>
    <table border="0" width="900" bgcolor="#999999" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
	<%if IsArray(arCustomer) Then%>
	<tr bgcolor="#FFFFFF" height="25">
	  <td align = "left" colspan="4"><b>Total no of customers <% if (strFindProductCode<>"") then %>(Product Code: <%=strFindProductCode%> on the Amend or Standing Order)<% end if %>: <%=UBound(arCustomer,2)+1%></b></td>
	</tr>	
	<tr>
	  <td width="20" bgcolor="#CCCCCC"><input type="checkbox" name="allbox" id="allbox" onClick="checkAll(this)"></td>
	  <td width="110" bgcolor="#CCCCCC"><b>Customer Code</b></td>
	  <td width="650" bgcolor="#CCCCCC"><b>Customer Name</b></td>
	  <td width="120" bgcolor="#CCCCCC" align="center"><b>Action</b></td>
	</tr>
	<%End if%>  
	<%
		if IsArray(arCustomer) Then 
			for i = 0 to UBound(arCustomer,2)
				str="che" & i
				if (i mod 2 =0) then
					strBgColour="#FFFFFF"
				else
					strBgColour="#F3F3F3"
				end if
				if (i Mod 300=0) then
					Response.Flush()
				end if
				%>
					<tr bgcolor="<%=strBgColour%>" height="30">								
						<td><input type="checkbox" name="<%=str%>" onClick="JavaScript:clickcheck()"></td>			
						<td><%=arCustomer(0,i)%></td>
						<td><%=arCustomer(1,i)%></td>
						<td align="center"><input onClick="NewWindow('customer_view_popupwindow.asp?CustNo=<%=arCustomer(0,i)%>','profile','700','300','yes','center');return false" onFocus="this.blur()" type="button" value="View" name="B1" style="width:60px; font-family: Verdana; font-size: 8pt"></td>
					</tr>
				<%
			Next
		else
		%>
			<tr>
				<td colspan="5" bgcolor="#FFFFFF" align="center" height="40"><font color="#FF0000"><strong>There are no customer(s) found.</strong></font></td>
			</tr>
		<%
		End if
		%>
      </table>
	  </Form>
    </td>
  </tr>
</table>
<%
	end if
%>
</center>
</div>
</body>
</html>
<%
	if IsArray(arCustomer) then erase arCustomer
	if IsArray(arGroup) Then erase arGroup
	Set Obj = Nothing
	Set ObjCustomer = Nothing
%>