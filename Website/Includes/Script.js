function checkNum(rawVal, per){
	var intI;
	var numList;
	var checkNumFlag = true;
	rawVal = rawVal.toLowerCase();

	if(per == 1){
		numList = "0123456789.";
	}else if(per == -1){
		numList = "0123456789";
	}	
	for(intI=0; intI<rawVal.length; intI++){
		if(numList.indexOf(rawVal.charAt(intI),0) == -1){
			checkNumFlag = false;
		}
	}
	return checkNumFlag;
}
function checkPercentage(varPer){
	if(varPer.indexOf('.',0) != -1){
		if((varPer.substring(varPer.indexOf('.',0)+1, varPer.length)).length > 2){
			return true;
		}
		if(varPer > 100.00 || checkNum(varPer, 1) == false){
			return true;
		}
	}else if(varPer.indexOf('.',0) == -1){
		if(varPer > 100.00 || varPer.length > 3 || checkNum(varPer, -1) == false){
			return true;
		}
	}
	return false;
}
function checkRealNum(varNum){
	if(isNaN(varNum) == true || varNum.indexOf('-', 0) == 0){ 
		return true;
	}else if(varNum.indexOf(".", 0) != -1)  {
		if(varNum.length - varNum.indexOf(".", 0)-1 > 4){
			return true;
		}
	}
}
function checkIntRealNum(varNum){
	if(isNaN(varNum) == true || varNum.indexOf('-', 0) == 0){ 
		return true;
	}else if(varNum.indexOf(".", 0) != -1)  {
		if(varNum.length - varNum.indexOf(".", 0)-1 > 2){
			return true;
		}
	}
}
function CheckEmpty(tString){
	var Counter;
	var tempFlag;
	tempFlag = true;
			    
	for (Counter=0;Counter<tString.length;Counter++){
	if (tString.charAt(Counter) != " "){
		tempFlag = false;
		break;
	}
	}
	 if (tempFlag==true){
	return "true"; 
	}
	if (tempFlag==false){
	    return "false";
	}
}

	
function CheckEmail(tString){
	var Counter;
	var tempFlag1;
	var tempFlag2;
	
	tempFlag1 = true;
	tempFlag2 = true;
			    
	for (Counter=0;Counter<tString.length;Counter++){
		if (tString.charAt(Counter)=="@"){
				tempFlag1 = false;
				break;
			}
	 }
		 
	for (Counter=0;Counter<tString.length;Counter++){
		if (tString.charAt(Counter)=="."){
				tempFlag2 = false;
				break;
		}
	}
		 
	if (tempFlag1==false && tempFlag2==false){
			return "false"; 
	    }
	else{
	     return "true";
	    }    
}


/*function NumericCheck(strString){
	
var checkOK = "0123456789 -+-() \t\r\n\f";
var checkStr = strString;
var allValid = true;
  for (i = 0;  i < checkStr.length;  i++)
  {
    ch = checkStr.charAt(i);
    for (j = 0;  j < checkOK.length;  j++)
      if (ch == checkOK.charAt(j))
        break;
    if (j == checkOK.length)
    {
      allValid = false;
      break;
    }
  }
}
*/

function CheckTelephone(strString){	
var checkOK = "0123456789 ";
var checkStr = strString;
var allValid = true;
  for (i = 0;  i < checkStr.length;  i++)
  {		
    var ch = checkStr.charAt(i);
    for (j = 0;  j < checkOK.length;  j++)
      if (ch == checkOK.charAt(j))
        break;
    if (j == checkOK.length)
    {
      allValid = false;
      break;
    }
  }
  if(checkStr.length < 8 || checkStr.length > 14){
      allValid = false;
  }
  return allValid
}

function isZero(strString){
var checkOK = "0";
var checkStr = strString;
var allValid = true;
  for (i = 0;  i < checkStr.length;  i++)
  {
    ch = checkStr.charAt(i);
    for (j = 0;  j < checkOK.length;  j++)
      if (ch == checkOK.charAt(j))
        break;
    if (j == checkOK.length)
    {
      allValid = false;
      break;
    }
  }
return allValid;   	
}

function isInt(strString){

var checkOK = "0123456789";
var checkStr = strString;
var allValid = true;
  for (i = 0;  i < checkStr.length;  i++)
  {
    ch = checkStr.charAt(i);
    for (j = 0;  j < checkOK.length;  j++)
      if (ch == checkOK.charAt(j))
        break;
    if (j == checkOK.length)
    {
      allValid = false;
      break;
    }
  }
	
  if(allValid && (eval(strString) == 0)){
	allValid = false;	

  }

return allValid;
}

function chkDate(intDay,intMonth,intYear) {
//Date Validation
//The month will be sent as a Number !
//The Date will be sent as a Number !
//The Year will be sent as a Text !

if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intDay > 30))
	{
		return false;
	}

if (intMonth == 2) 
	{
	if (LeapYear(intYear) == true) 
	{
		if (intDay > 29) 
		{
			return false;
		}
		else
			{return true;}
	}
	else 
	{
		if (intDay > 28) 
			{
			return false;
			}
		else
			{return true;};
	}
 }
 else
     {return true;}
}



function LeapYear(intYear) 
//Leap year validation
{
	if (intYear % 100 == 0) 
		{
		if (intYear % 400 == 0) 
			{ 
			return true; 
			}
		else 
			{
			if ((intYear % 4) == 0) 
				{ 
				return true; 
				}
			}
	}	
		else{
			if ((intYear % 4) == 0) 
				return true; 
			else	
				return false;
		}
	}


function chkDateWithWeekend(intDay,intMonth,intYear) {
//Date with weekend Validation
//The month will be sent as a Number !
//The Date will be sent as a Number !
//The Year will be sent as a Text !


if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intDay > 30))
	{
		return false;
	}

if (intMonth == 2) 
	{
	if (LeapYear(intYear) == true) 
	{
		if (intDay > 29) 
		{
			return false;
		}
		else
			{return true;}
	}
	else 
	{
		if (intDay > 28) 
			{
			return false;
			}
		else
			{return true;};
	}
 }
 else
     //{return true;}
   {  
		if(CheckWeekend(intDay,intMonth,intYear)==true)
		 return true
		else
		 return false
   }
}

function DateRangCheck(f_day,f_month,f_year,t_day,t_month,t_year){
var ResultTag = true;
	
	if(t_year>f_year)		
		return ResultTag;
	else if(t_year==f_year){
		if(t_month>f_month)
			return ResultTag;
		else if(t_month==f_month)
			if(t_day>f_day)
			return ResultTag;
	}		
	ResultTag = false;
	return ResultTag;
}
function CheckWeekend(intDay,intMonth,intYear) {

	var arrMonth = new 	Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")						
	var strDate = arrMonth[intMonth-1] + "," + intDay + "," + intYear
	var ADay = new Date(strDate)
	var DayNo = ADay.getDay()			

	if(DayNo==6 || DayNo==0)
		return false
	else	
		return true
}

function CheckPercentageAmount(strString,DecimalLen){	
var checkOK = "0123456789.";
var checkStr = strString;
var allValid = true;
var Decimal

  for (i = 0;  i < checkStr.length;  i++)
  {		
    var ch = checkStr.charAt(i);
    for (j = 0;  j < checkOK.length;  j++)
      if (ch == checkOK.charAt(j))
        break;
    if (j == checkOK.length)
    {
      allValid = false;
      break;
    }
  }
  if(strString > 100){
		allValid = false; }	
	else{
		Decimal = strString.substring(strString.lastIndexOf('.')+1,strString.length);
		if(DecimalLen < Decimal.length){
			allValid = false;}
	}	
		
  return allValid
}

function CheckNumber(strString,DecimalLen,PercentageTag){	
var checkOK = "0123456789.";
var checkStr = strString;
var allValid = true;
var Decimal
var DecimalCount = 0;

  for (i = 0;  i < checkStr.length;  i++)
  {		
    var ch = checkStr.charAt(i);
    for (j = 0;  j < checkOK.length;  j++)
      if (ch == checkOK.charAt(j)){
					if(checkOK.charAt(j)=='.'){
						DecimalCount ++
					}       
        break;}
    if (j == checkOK.length)
    {
      allValid = false;
      break;
    }
  }
  if(PercentageTag=="y"){   
		if(strString > 100){			
			allValid = false; }
		}	
	
	Decimal = strString.substring(strString.lastIndexOf('.')+1,strString.length);	
	if(DecimalLen > 0){
		if(DecimalLen < Decimal.length){		
			allValid = false;}
	}
	
	if(DecimalCount > 1){		
		allValid = false;}
					
  return allValid
}

function NewWindow(mypage, myname, w, h, scroll) {
	var winl = (screen.width - w) / 2;
	var wint = (screen.height - h) / 2;
	winprops = 'height='+h+',width='+w+',top='+wint+',left='+winl+',scrollbars='+scroll+',resizable'
	win = window.open(mypage, myname, winprops)
	if (parseInt(navigator.appVersion) >= 4) { win.window.focus(); }
}


function FindChar(StringIn){	
	var CharInArray = new Array("<",">","/","'","=","?","!","#"	);	
	for (var i=0;i<StringIn.length;i++){
		for (var j=0;j<CharInArray.length;j++){
			if (StringIn.charAt(i)==CharInArray[j]){
				return "1";
			}
		}
	}
	return "0";
}

