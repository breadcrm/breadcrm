<%Response.Expires = -1%>
<%Response.Buffer = True%>
<%Session.Timeout = 1200%>
<%'Do not remove scripttimeout line %>
<%server.ScriptTimeout=3600%>

<%On error Resume Next%>
<%
dim strglobalinvoicefootermessage
dim PrintPgSize
dim pagesize

strglobalinvoicefootermessage="Proprietor: Light Work , Gresham House, 53 Clarendon Road, Watford, Herts WD17 1LA  � company No. XXXX<br><br>Payments may be made by Credit/ Debit card by calling  020 7041 6898<br>"
PrintPgSize = 40
pagesize = 999999
if session("UserNo") = "" or session("UserType") = "" or Session("LoginIP") <> Request.ServerVariables("REMOTE_ADDR") then
	response.redirect "full.asp"
end if
function displayBristishDate(dt)
  displayBristishDate=dt
  if dt <> "" then
    dt=cdate(dt)
    displayBristishDate=formatdatetime(dt,vbShortDate)
    if isdate(dt) then
      displayBristishDate = day(dt) & "/" & month(dt) & "/" & year(dt)
    end if
  end if
end function

function chkDate(intDay,intMonth,intYear) 
if ((intMonth = 4 or intMonth = 6 or intMonth = 9 or intMonth = 11) and (intDay > 30)) then
	chkDate = false
end if

if (intMonth = 2) then
	if (LeapYear(intYear) = true) then
		if (intDay > 29) then
			chkDate = false
		else
			chkDate = true
		end if
	else 
		if (intDay > 28) then
			chkDate = false
		else
			chkDate = true
		end if
	end if
 else
   chkDate = true
 end if
end function

function LeapYear(intYear) 
//Leap year validation
  if (intYear mod 100 = 0) then
  		if (intYear mod 400 = 0) then
			LeapYear = true
		else 
			if ((intYear mod 4) = 0) then
				LeapYear = true
			end if
		end if
  else
  	if ((intYear mod 4) = 0) then
		LeapYear = true
	else	
		LeapYear = false
	end if
  end if
end function	

function NextDayinDDMMYY(aDateinDMY)
  a1=split(aDateinDMY,"/")
  intMonth = a1(1)
  intDay = a1(0)
  intYear = a1(2)
  if chkDate(intDay,intMonth,intYear) then
    if (intMonth = 4 or intMonth = 6 or intMonth = 9 or intMonth = 11) then
      if (intDay < 30) then
	    NextDayinDDMMYY = (cint(intDay) + 1) & "/" & intMonth & "/" & intYear
	  else
	    NextDayinDDMMYY = "1/" & (cint(intMonth) + 1) & "/" & intYear
      end if
    elseif (intMonth = 2) then
	  if (LeapYear(intYear) = true) then
		if (cint(intDay) < 29) then
			NextDayinDDMMYY = (cint(intDay) + 1) & "/" & intMonth & "/" & intYear
		else
			NextDayinDDMMYY = "1/" & (cint(intMonth) + 1) & "/" & intYear
		end if
	  else 
		if (intDay < 28) then
			NextDayinDDMMYY = (cint(intDay) + 1) & "/" & intMonth & "/" & intYear
		else
			NextDayinDDMMYY = "1/" & (cint(intMonth) + 1) & "/" & intYear
		end if
	  end if
    else
      if (intDay < 31) then
          NextDayinDDMMYY = (cint(intDay) + 1) & "/" & intMonth & "/" & intYear
      else
        if (intMonth = 12) then
          NextDayinDDMMYY = "1/1" & "/" & (cint(intYear) + 1)
        else
          NextDayinDDMMYY = "1/" & (cint(intMonth) + 1) & "/" & intYear
        end if
      end if
    end if
  else
    NextDayinDDMMYY = aDate
  end if
end function

function PreviousDayinDDMMYY(aDateinDMY)
  a1=split(aDateinDMY,"/")
  intMonth = a1(1)
  intDay = a1(0)
  intYear = a1(2)
  if chkDate(intDay,intMonth,intYear) then
    
      if (intDay <> 1) then
	    PreviousDayinDDMMYY = (cint(intDay) - 1) & "/" & intMonth & "/" & intYear
	  elseif (intMonth=2 or intMonth=4 or intMonth=6 or intMonth=9 or intMonth=11) then
	    PreviousDayinDDMMYY = "31/" & (cint(intMonth) - 1) & "/" & intYear
      elseif (intMonth=1) then
	  	 PreviousDayinDDMMYY = "31/12" & "/" & (cint(intYear) - 1) 
	  elseif (intMonth=5 or intMonth=7 or intMonth=8 or intMonth=10 or intMonth=12) then
	 	  PreviousDayinDDMMYY = "30/" & (cint(intMonth) - 1) & "/" & intYear	
	  elseif (intMonth=3) then
	  	  if ((intYear mod 4=0) and ((intYear mod 400=0) or (intYear mod 100<>0))) then
		 	 PreviousDayinDDMMYY = "29/" & (cint(intMonth) - 1) & "/" & intYear			 		
		  else
		     PreviousDayinDDMMYY = "28/" & (cint(intMonth) - 1) & "/" & intYear	
		  end if
	  end if
	  
  else
    PreviousDayinDDMMYY = aDate
  end if
end function

function FormatDateInDDMMYYYY(dte)
	Dim iDay
	Dim iMonth
	Dim iYear
	iDay = Day(FormatDateTime(dte, 2))
	iMonth = Month(FormatDateTime(dte, 2))
	iYear = Year(FormatDateTime(dte, 2))
	If(iDay < 10) Then
		iDay = "0" & CStr(iDay)  
	End If
	If(iMonth < 10) Then
		iMonth = "0" & CStr(iMonth)  
	End If
	If(iYear < 10) Then
			iYear = "0" & CStr(iYear)  
	End If				
	FormatDateInDDMMYYYY = iDay & "/" & iMonth & "/" & iYear		
end function

%>
