var xmlHttp
function clearamendorder(OrderNo,CusCode,DeliveryDate)
{
	xmlHttp=GetXmlHttpObject();
	if (xmlHttp==null)
  	{
  		alert ("Your browser does not support AJAX!");
  		return;
  	} 
	
	var url="Includes/clearamendorder.asp?OrderNo=" + OrderNo + "&CusCode="+CusCode+ "&DeliveryDate="+DeliveryDate;
	xmlHttp.open("POST",url,true);
	xmlHttp.send();
}

function GetXmlHttpObject()
{
var xmlHttp=null;
try
  {
  // Firefox, Opera 8.0+, Safari
  xmlHttp=new XMLHttpRequest();
  }
catch (e)
  {
  // Internet Explorer
  try
    {
    xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
  catch (e)
    {
    xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  }
return xmlHttp;
}