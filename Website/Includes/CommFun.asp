<%
Function LogAction(sActionType,sActionSummery,sCustomer)
	Dim obj_DB
	Dim str_Sql
	Dim strUserID
	Dim strIP
	
	If (session("UserNo") = "") Then
		strUserID = "NULL"
	Else
		strUserID = session("UserNo")
	End If
	
	strIP = Request.ServerVariables("REMOTE_ADDR")
	
	Dim objLog
	
	Set objLog = Server.CreateObject("bakery.Log")
	objLog.LogAction strUserID, sActionType , sActionSummery , sCustomer, strIP
	
	Set objLog = Nothing
	
End Function


Sub WriteErrorLog(vOrderNo,Error,errMethod)
stop
     Const ForAppending = 8
     Dim path,fileName
     path = strAmendOrderLogPath 
     dim filesys, filetxt
     Set filesys = CreateObject("Scripting.FileSystemObject")
     fileName = "AmendOrder_" & Day(Now()) & "_" & Month(Now())& "_" & Year(Now()) & ".txt"
     Set filetxt = filesys.OpenTextFile(strAmendOrderLogPath & fileName, ForAppending, True) 
     filetxt.WriteLine "-------------------------------------------------"
     filetxt.WriteLine "Order No - " & vOrderNo 
     filetxt.WriteLine "Error Descrption - " & Error 
     filetxt.WriteLine "Error Method - " & errMethod 
     filetxt.WriteLine "Process Date - " & now()
     filetxt.Close 	

End Sub

function seourl(strIN)
   str = replace(strIN," ","-")
   str = replace(str,"'","")
   str = replace(str,",","-")
   str = replace(str,"#","-")
   str = replace(str,"%","-")
   str = replace(str,"&","-")
   str = replace(str,"*","-")
   str = replace(str,"{","-")
   str = replace(str,"}","-")
   str = replace(str,"\","-")
   str = replace(str,":","-")
   str = replace(str,"<","-")
   str = replace(str,">","-")
   str = replace(str,"?","-")
   str = replace(str,"/","-")
   str = replace(str,"--","-")
   str = replace(str,"--","-")
   str = replace(str,"--","-")
   'str = replace(server.urlencode(str),"+","-")
   seourl = lcase(str)
end function


function alphanumericvalue(stralphanumeric)
	Dim regEx
	Set regEx = New RegExp
	regEx.Pattern = "[^\w]"
	regEx.Global = True
	stralphanumeric = regEx.Replace(stralphanumeric,"-")
    alphanumericvalue = lcase(stralphanumeric)
end function

Function in_array(element, arr)
    For i=0 To Ubound(arr) 
        If Trim(arr(i)) = Trim(element) Then 
            in_array = True
            Exit Function
        Else 
            in_array = False
        End If  
    Next 
End Function

Function RemoveSpecialCharacters(stralphanumeric)
 	Dim regEx
	Set regEx = New RegExp
	'Only alow a to z, A to Z, 0 - 9, .
 	regEx.Pattern = "((?![a-zA-Z0-9.]).)+"
 	regEx.Global = True
	RemoveSpecialCharacters = regEx.Replace(stralphanumeric,"-")
    'RemoveSpecialCharacters = lcase(stralphanumeric)
End Function

%>
