<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/filepath.asp" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%

Dim Obj,ObjOrder,objResult
Dim arTypicalOrder
Dim arTypicalOrderDetails, arTempOrderMasterDeliveryCharge
Dim strDirection
Dim vOrderNo
Dim i
Dim strProductStream
Dim arSplit
Dim arUpdate
Dim strPo
Dim strPoTag
Dim strErrTag, strAction, strProductsAmended
Dim Total

if Trim(Request.Form("OrderNo")) <> "" Then
	vOrderNo = Request.Form("OrderNo")
Else
	vOrderNo = Request.QueryString("OrderNo")
End if	
strnotapplydeliverycharge = Request.Form("notapplydeliverycharge")
Set Obj = CreateObject("Bakery.Orders")
Obj.SetEnvironment(strconnection)
if Trim(Request.Form("Update")) <> "" and request("confirm")="Yes" Then
	strProductStream = Request.Form("ProductStream")	
	arSplit = Split(strProductStream,",")	
	strPo = Request.Form("txtPo")
	
	
	strErrTag = false
		
	if IsArray(arSplit) Then
		Redim arUpdate(1,UBound(arSplit)) 
		For i=0 To UBound(arSplit)
			
			if (Request.Form("qty" & arSplit(i))<>Request.Form("qtyoldval" & arSplit(i))) then
				if (IsNumeric(Request.Form("qty" & arSplit(i))) and IsNumeric(Request.Form("qtyoldval" & arSplit(i)))) then
					if (Request.Form("qty" & arSplit(i))>0) then
						if (strProductsAmended="") then
							strProductsAmended=strProductsAmended & arSplit(i) & " (qty: " & Request.Form("qtyoldval" & arSplit(i)) & " to " &  Request.Form("qty" & arSplit(i)) & ")"
						else
							strProductsAmended=strProductsAmended & ", " & arSplit(i) & " (qty: " & Request.Form("qtyoldval" & arSplit(i)) & " to " &  Request.Form("qty" & arSplit(i)) & ")"
						end if
					elseif (Request.Form("qty" & arSplit(i))=0 and Request.Form("qtyoldval" & arSplit(i))>0) then
						if (strProductsAmended="") then
							strProductsAmended=strProductsAmended & arSplit(i) & " (qty: " & Request.Form("qtyoldval" & arSplit(i)) & " to " &  Request.Form("qty" & arSplit(i)) & ")"
						else
							strProductsAmended=strProductsAmended & ", " & arSplit(i) & " (qty: " & Request.Form("qtyoldval" & arSplit(i)) & " to " &  Request.Form("qty" & arSplit(i)) & ")"
						end if
					end if
				end if		
			end if
			
			If IsNumeric(Request.Form("qty" & arSplit(i))) Then
				arUpdate(0,i) = arSplit(i)
				arUpdate(1,i) = Request.Form("qty" & arSplit(i))								
			Else
				arUpdate(0,i) = arSplit(i)
				arUpdate(1,i) = 0 ' If numeric in not entred
				strErrTag = true
			End if			
		Next
		
		if strErrTag <> true Then
			Dim cusNo,Del_Date
			
			if (strnotapplydeliverycharge<>1) then
				strnotapplydeliverycharge=0
			end if
			
			Del_Date = Request.Form("HF_DelDate")
			cusNo = obj.GetCustomerIdByOrderId(vOrderNo)
			objResult = obj.Update_TypicalOrderDetails(vOrderNo,arUpdate,strPo,strnotapplydeliverycharge)	
			objResult2 = obj.Log_OrderAmend (vOrderNo,session("UserNo"), Del_Date)
			
			
			
			objResult3=obj.SaveTempOrderMasterDeliveryCharge(vOrderNo,strnotapplydeliverycharge)
			
			if (objResult="OK") then
				if (session("sessionAddProdCode")<>"") then
					strProductsAdded="Product(s) Added: " & session("sessionAddProdCode")
					session("sessionAddProdCode")=""
				end if
				
				if (strProductsAmended="" and strProductsAdded="") then
					strProductsAmended="; Amend order confirmed with no changes."
				elseif (strProductsAmended<>"") then
					strProductsAmended=", Product(s) Amended: " & strProductsAmended
				end if
				
				if (strProductsAdded<>"") then
					strProductsAmended=strProductsAmended & ", " & strProductsAdded
				end if
				
				strAction="Delivery Date: " & Request("hidDeliveryDate") & strProductsAmended
				LogAction "Order Confirmed",  strAction, cusNo
			else
			    if (session("sessionAddProdCode")<>"") then
					strProductsAdded="Product(s) Added: " & session("sessionAddProdCode")
					session("sessionAddProdCode")=""
				end if
				
				if (strProductsAmended="" and strProductsAdded="") then
					strProductsAmended="; Amend order confirmed with no changes."
				elseif (strProductsAmended<>"") then
					strProductsAmended=", Product(s) Amended: " & strProductsAmended
				end if
				
				if (strProductsAdded<>"") then
					strProductsAmended=strProductsAmended & ", " & strProductsAdded
				end if
				
				strAction="Delivery Date: " & Request("hidDeliveryDate") & strProductsAmended
				strAction=strAction & "<br>" & objResult
				LogAction "Order Confirmed - Log Failed",  strAction, cusNo
			end if	
		End if
		

		if objResult = "OK" and objResult2 = "OK" Then
			Set ObjOrder = obj.Display_TypicalOrderDetails(vOrderNo)
			arTypicalOrder = ObjOrder("TypicalOrder")
			arTypicalOrderDetails = ObjOrder("TypicalOrderDetails")
			Set Obj = Nothing
			Set ObjOrder = Nothing
		
			if Trim(objResult) = "OK" Then
				if Request.Form("SaveAddProduct") = "Yes" Then
				Response.Redirect "order_addproduct.asp?OrderNo="&vOrderNo & "&vDateDiff=" & strDateDiff  & "&hidDeliveryDate=" & Request("hidDeliveryDate")
				else 
				Response.Redirect "order_finish.asp?date="&arTypicalOrder(1,0)&"&name="&Server.URLEncode(arTypicalOrder(5,0))&"&day="&arTypicalOrder(2,0)&"&type="&Server.URLEncode(arTypicalOrder(3,0))
			    End if
			End if 
		Else
		
		    if objResult2 <> "OK" Then
		        Call WriteErrorLog(vOrderNo,objResult2,"Log_OrderAmend")	
		    End if	
		
		    if objResult <> "OK" Then
		        Call WriteErrorLog(vOrderNo,objResult,"Update_TypicalOrderDetails")	
		    End if	    
		    pageError = true	
			
		End if	
		
	End if
End if 
Set ObjOrder = obj.Display_TypicalOrderDetails(vOrderNo)
arTypicalOrder = ObjOrder("TypicalOrder")
arTypicalOrderDetails = ObjOrder("TypicalOrderDetails")
arTempOrderMasterDeliveryCharge = ObjOrder("TempOrderMasterDeliveryCharge")

Set Obj = Nothing
Set ObjOrder = Nothing

if IsArray(arTypicalOrder) Then
	strPoTag = arTypicalOrder(18,0) 
	strPurchaseOrderNumber = arTypicalOrder(17,0)
	strCusGNo = arTypicalOrder(22,0)
End if

if IsArray(arTempOrderMasterDeliveryCharge) and strnotapplydeliverycharge="" Then
	strnotapplydeliverycharge = arTempOrderMasterDeliveryCharge(0,0) 
	
	if (strnotapplydeliverycharge) then
		strTempOrderMasterDeliveryChargeChecked="Checked"
	else
		strTempOrderMasterDeliveryChargeChecked=""
	end if
	
End if

strProductStream = ""
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script src="Includes/clearamendorder.js"></script>
<SCRIPT LANGUAGE="Javascript">
<!--
var strPoTag = '<%=strPoTag%>'
var strPurchaseOrderNumber = "<%=strPurchaseOrderNumber%>"
var strCusGNo = '<%=strCusGNo%>'

function chkNumeric(strString)
{
	var strValidChars = "0123456789";
	var strChar;
	var blnResult = true;

	if (strString.length == 0) return false;
	for (i = 0; i < strString.length && blnResult == true; i++)
  	{
  		strChar = strString.charAt(i);
  		if (strValidChars.indexOf(strChar) == -1)
	 	{
	 		blnResult = false;
	 	}
  	}
	return blnResult;
}


function validation(){

	if(strPoTag=='Y'){
		if(document.frmOrder.txtPo.value == ''){
			alert("Please enter a Purchase order number");
			document.frmOrder.txtPo.focus();	
			return;
		}		
	}	
	
	if (strCusGNo=='147' || strCusGNo=='152')
	{
		if (document.frmOrder.txtPo.value!='')
		{
			if(document.frmOrder.txtPo.value.length>20)
			{
				alert("Please enter maximum 20 characters");	
				document.frmOrder.txtPo.focus();
				return;
			}
			/*if(document.frmOrder.txtPo.value.length>6)
			{
				alert("Please enter maximum 6 numeric numbers");	
				document.frmOrder.txtPo.focus();
				return;
			}
			if (chkNumeric(document.frmOrder.txtPo.value) == false)
			{
				alert("Please enter only numeric numbers");	
				document.frmOrder.txtPo.focus();
				return;
			}*/
			
		}			
	}
	
	//if(!(confirm("Are you sure the quantities entered are numeric values?"))){	
	//return;	
	//}
	
	if (document.frmOrder.confirm.value == 'Yes') 
	{
	if(confirm("Would you like to confirm this order?")){ 
		document.frmOrder.submit();} 
	}
	else
	{
		document.frmOrder.submit();
    }

    
}

function ConfirmIs48Product(LateProd, DateDiff, objNewValue, objoldValue) {
   // alert(document.frmOrder.LateProd21.value);
	if (LateProd=="Y" && DateDiff==1)
	{
		if (confirm("This is a 48 hrs product & requires 2 day notice - are you sure you want to proceed?"))
		{
			return true;
		}
		else
		{
			document.getElementById(objNewValue).value=objoldValue;
			return false;
		}	
	}
	if (LateProd=="Y" && DateDiff<=1)
	{
		alert("This product is a 48 hours product so you can't amend for this delivery date.")
		document.getElementById(objNewValue).value=objoldValue;
		return false;
	}
	return true	
}

function clearorders()
{
	if(confirm("Are you sure you want to clear all New Qty?\nThis is not reversible."))
	{	
		for(i=0; i<frmOrder.length; i++)
		{
			field_type = frmOrder[i].type.toLowerCase();
			if (field_type=="text")
			{
				if (frmOrder[i].name.substring(0,3)=="qty")
					frmOrder[i].value = "0";
			}
			if (field_type=="hidden")
			{
				if (frmOrder[i].name=="hidDeliveryDate")
					vDeliveryDate=frmOrder[i].value;
				else if (frmOrder[i].name=="hidCustomerCode")
					CusCode=frmOrder[i].value;
				else if (frmOrder[i].name=="hidOrderNo")
					OrderNo=frmOrder[i].value;		
			}
		}
		clearamendorder(OrderNo,CusCode,vDeliveryDate);
	}
}

function CheckPurchaseOrderNumber()
{
	//dummy method.
}
//-->
</SCRIPT>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
 <form method="POST" action="order_new1.asp" name="frmOrder">
<%
	if strErrTag = true Then%>
		<tr>    
		  <td width="100%">
				<font color="#FF0000" size=3><b>Invalid quantities, Please check</b></font>  
			</td>	
		</tr><%	
	End if%>
	
	<%if pageError = true Then%>
		<tr>    
		  <td width="100%">
				<font color="#FF0000" size=3><b>Error - <%=objResult1%><br /><%=objResult2 %></b></font>  
			</td>	
		</tr><%	
	End if%>
  <tr>
   
    <td width="100%"><b><font size="3">Amend order<br>
      &nbsp;</font></b><%
      
		if IsArray(arTypicalOrder) Then%>
		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
		<td>
		<table border="0" cellspacing="1" cellpadding="3" style="font-family: Verdana; font-size: 8pt" width="400">
        <tr>
          <td><b>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b></td>
          <td><%=arTypicalOrder(5,0)%></td>
        </tr>
        <tr>
          <td><strong>Customer Code</strong></td>
          <td><%=arTypicalOrder(6,0)%></td>
        </tr>
        <tr>
          <td><b>Delivery Date&nbsp;</b></td>
          <td><%
						if IsDate(arTypicalOrder(1,0)) Then
							Response.Write Day(arTypicalOrder(1,0)) & "/" & Month(arTypicalOrder(1,0)) & "/" & Year(arTypicalOrder(1,0))	
						    strDeliveryDate=Day(arTypicalOrder(1,0)) & "/" & Month(arTypicalOrder(1,0)) & "/" & Year(arTypicalOrder(1,0))							
						else
							strDeliveryDate=displayBristishDate(arTypicalOrder(1,0))
						End if						
						%>
						<INPUT type="hidden" id="hidDeliveryDate" name="hidDeliveryDate" value="<%=strDeliveryDate%>">       
          
          </td>
        </tr>
		 <%
		  strDateDiff=DateDiff("d",Date,arTypicalOrder(1,0)) 
		  %>
		   <INPUT type="hidden" id="DeliveryDate" name="DeliveryDate" value="<%=Month(arTypicalOrder(1,0)) & "/" & Day(arTypicalOrder(1,0)) & "/" & Year(arTypicalOrder(1,0))%>">
           <INPUT type="hidden" id="vDateDiff" name="vDateDiff" value="<%=strDateDiff%>">
		   <INPUT type="hidden" id="HF_DelDate" name="HF_DelDate" value="<%=Day(arTypicalOrder(1,0)) & "/" & Month(arTypicalOrder(1,0)) & "/" & Year(arTypicalOrder(1,0))%>">
        <tr>
          <td><b>Day&nbsp;</b></td>
          <td><%=arTypicalOrder(2,0)%></td>
        </tr>
        <tr>
          <td>Delivery time</td>
          <td><%=arTypicalOrder(3,0)%></td>
        </tr>
        <tr>
          <td>Sample order</td>
          <td><%=arTypicalOrder(4,0)%></td>
        </tr>        
        <tr>
          <td valign="top">Address</td>
          <td valign="top">
		  <%=arTypicalOrder(7,0)%>
		  <%
		  if (arTypicalOrder(8,0)<>"") then
		  %>
		  <%=arTypicalOrder(8,0)%>
		  <%
		  end if
		  %>
		  </td>
        </tr>
        <tr>
          <td>Town</td>
          <td><%=arTypicalOrder(9,0)%></td>
        </tr>
        <tr>
          <td>Postcode</td>
          <td><%=arTypicalOrder(10,0)%></td>
        </tr>
        <tr>
          <td>Telephone</td>
          <td><%=arTypicalOrder(11,0)%></td>
        </tr>
        <tr>
          <td>Weekday Van</td>
          <td><%=arTypicalOrder(19,0)%></td>
        </tr>		
		<tr>
          <td>Weekend Van</td>
          <td><%=arTypicalOrder(20,0)%></td>
        </tr>
		<tr>
          <td><nobr>Account Minimum Order</nobr></td>
          <td>
		  <%
		  if (arTypicalOrder(24,0)<>"") then
		  %>
		  &pound;<%=FormatNumber(arTypicalOrder(24,0))%>
		  <%
		  end if
		  %>
		  </td>
        </tr>
		<tr>
          <td><nobr>Group Minimum Order</nobr></td>
          <td>		  
		   <%
		  if (arTypicalOrder(25,0)<>"") then
		  %>
		  &pound;<%=FormatNumber(arTypicalOrder(25,0))%>
		  <%
		  end if
		  %>
		  </td>
        </tr>
		
		<tr>
          <td><nobr>Delivery Charge Option</nobr></td>
          <td><%=arTypicalOrder(26,0)%></td>
        </tr>
        <tr>
          <td>Contact Name</td>
          <td><%=arTypicalOrder(12,0)%></td>
        </tr>
        <tr>
          <td>Contact Phone</td>
          <td><%=arTypicalOrder(13,0)%></td>
        </tr>
        <tr>
          <td>Name</td>
          <td><%=arTypicalOrder(14,0)%></td>
        </tr>
        <tr>
          <td>Phone</td>
          <td><%=arTypicalOrder(15,0)%></td>
        </tr>
		<tr>
          <td>Sales Person</td>
          <td><%=arTypicalOrder(21,0)%></td>
        </tr> 
		<tr>
          <td>Account Manager</td>
          <td><%=arTypicalOrder(27,0)%></td>
        </tr> 
		  
       <tr>
          <td>Order Amendment Date</td>
          <td>
		  	<%
			if IsDate(arTypicalOrder(16,0)) Then
				Response.Write Day(arTypicalOrder(16,0)) & "/" & Month(arTypicalOrder(16,0)) & "/" & Year(arTypicalOrder(16,0))	
				strOrderAmendmentDate=Day(arTypicalOrder(16,0)) & "/" & Month(arTypicalOrder(16,0)) & "/" & Year(arTypicalOrder(16,0))					 							 	
			End if
			%>		
			<INPUT type="hidden" id="hidCustomerCode" name="hidCustomerCode" value="<%=arTypicalOrder(6,0)%>">
			<INPUT type="hidden" id="hidOrderNo" name="hidOrderNo" value="<%=vOrderNo%>">       
          </td>
        </tr>
        
        <tr>
          <td>Purchase Order Number</td>
          <td>
		  <% 
		  dim strOption,strPurchaseOrder
		  if request("confirm")="No" then
		  strOption="Confirm Order"
		  strPurchaseOrder=request("txtPo")
		  %>
		  <%=strPurchaseOrder%>
          <INPUT type="hidden" id="txtPo" name="txtPo" value="<%=strPurchaseOrder%>">
		  <INPUT type="hidden" id="confirm" name="confirm" value="Yes">
		  <%else
		  strOption="View Order"
		  %>
		  	<INPUT type="text" id="txtPo" onClick="CheckPurchaseOrderNumber()" name="txtPo" value="<%=arTypicalOrder(17,0)%>" maxlength="50" size="20">
          	<INPUT type="hidden" id="confirm" name="confirm" value="No">
		  <%end if%>
		  </td>
        </tr>
		
		 <tr>
          <td>Do not apply a delivery charge to this order</td>
          <td>
		  <% if request("confirm")="No" then%>
		  <INPUT type="hidden" id="notapplydeliverycharge" name="notapplydeliverycharge" value="<%=strnotapplydeliverycharge%>">
		  <%
		  if (strnotapplydeliverycharge="1") then
		  	response.Write("Yes")
		  else
		  	response.Write("No")
		  end if
		  %>
		  <%else%>
		  <input type="checkbox" name="notapplydeliverycharge" id="notapplydeliverycharge" style="font-family: Verdana; font-size: 8pt" value="1" <%=strTempOrderMasterDeliveryChargeChecked%>>
		  <% end if%>
		  </td>
        </tr>
		</table>
        <%if request("confirm")="No" then%>
		<br>
		<table border="0" cellspacing="1" cellpadding="3" style="font-family: Verdana; font-size: 8pt" width="400">
       	<tr>
      		<td bgcolor="#FB4A6F">Product value above &pound;30</td>
		</tr>
		<tr>
			<td bgcolor="#FFE07F">Product not ordered in the last 3 months</td>
		</tr>
		</table>
		<br>
		<%end if%>
    
		</td>
		        <td valign="top" style="width:570px;">
		        <iframe frameborder="0" scrolling="no" src="AmendOrderMessages.asp?CNo=<%=arTypicalOrder(6,0)%>" width="570px" height="450px;"> iframe</iframe>
		       </td>
		    </tr>
		</table>
		      
      
      
      <%End if%>   
	
	 	
        <%if request("confirm")<>"No" then%>
		  <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
		 <tr bgcolor="#FFFFFF" height="26">
          <td></td>
          <td align="right"><input type="button" onClick="clearorders()" value="Clear Order" name="clearorder" id="clearorder" style="font-family: Verdana; font-size: 8pt; width:90px"></td>
		</tr>
		</table>
		 <% end if%>
		 <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" bgcolor="#999999">
		 <tr height="22">
          <td bgcolor="#AAAAAA" width="90"><b>Product Code</b></td>
          <td bgcolor="#AAAAAA"><b>Product Name</b></td>
		  
          <td width="110" bgcolor="#AAAAAA"><b>Typical Qty</b></td>
		  <td width="110" bgcolor="#AAAAAA"><b>New Qty</b></td>
          <td width="110" bgcolor="#AAAAAA" align="right"><b>Unit Price&nbsp;</b></td>
		  <td width="80" bgcolor="#AAAAAA" align="right"><b>Pack Size</b></td>
		  <td width="110" bgcolor="#AAAAAA" align="right"><b>Total Price&nbsp;</b></td>
		  
		 </tr>
		<%
        dim x,qty1
        dim j,k
        j=0
		k=0
        if IsArray(arTypicalOrderDetails) Then
					Total=0
					TotalOrderQty=0
					intCount=1
					dblTotalPrice=0
					for i = 0 to UBound(arTypicalOrderDetails,2)
					
					%>						
						<%if not (arTypicalOrderDetails(4,i)="N" and arTypicalOrderDetails(2,i) = "") then
						
						if (j mod 2 =0) then
							strBgColour="#F3F3F3"
						else
							strBgColour="#CCCCCC"
						end if					
						j=j+1
						%>
						<Input type="hidden" name="LateProd<%=intCount%>" value="<%=arTypicalOrderDetails(6,i)%>">
						<input type="hidden" name="Oldqty<%=intCount%>" value="<%=arTypicalOrderDetails(2,i)%>">
						<Input type="hidden" name="LateProd2<%=intCount%>" value="<%=arTypicalOrderDetails(11,i)%>">
						<%
						if request("confirm")="No" then
						x1="qty" & arTypicalOrderDetails(0,i)
						qty1new=request(x1)
						if (not Isnumeric(qty1new)) then
							qty1new=0
						end if
						TotalOrderQty=TotalOrderQty+qty1new
						if (qty1new>0) then
							if (k mod 2 =0) then
								strBgColourNew="#F3F3F3"
							else
								strBgColourNew="#CCCCCC"
							end if						
							k=k+1
							
							if (arTypicalOrderDetails(7,i)=0) then
								strBgColourNew="#FFE07F"
							end if
							
							if ((cint(qty1new)*CDbl(arTypicalOrderDetails(5,i)))>30.0) then
								strBgColourNew="#FB4A6F"
							end if
						%>				
						<tr bgcolor="<%=strBgColourNew%>" height="22">
							<td><%=arTypicalOrderDetails(0,i)%></td>
							<td><%=arTypicalOrderDetails(1,i)%></td>
							
							<td><%=arTypicalOrderDetails(2,i)%></td>
							
							<td>
							 <%
							 	x="qty" & arTypicalOrderDetails(0,i)
								qty1=request(x)
								Total=Total+(cdbl(arTypicalOrderDetails(5,i))*qty1)
								
								y="qtyoldval" & arTypicalOrderDetails(0,i)
								qtyoldval1=request(y)
								
								%>
							 	<%=qty1%>
								<input type="hidden" name="qty<%=arTypicalOrderDetails(0,i)%>" value="<%=qty1%>">
								<input type="hidden" name="qtyoldval<%=arTypicalOrderDetails(0,i)%>" value="<%=qtyoldval1%>">
							
						</td>
						<td align="right"><%=FormatNumber(arTypicalOrderDetails(5,i),2)%>&nbsp;</td>
						<td align="right">
						<%
						if (arTypicalOrderDetails(9,i)="1") then
							response.write(arTypicalOrderDetails(10,i))
						else
							response.write(arTypicalOrderDetails(8,i))
						end if
						%>
						</td>
						<td align="right" width="100"><%=FormatNumber(cdbl(qty1)*cdbl(arTypicalOrderDetails(5,i)),2)%>&nbsp;</td>
						</tr>
						<%
						dblTotalPrice=dblTotalPrice+ cdbl(qty1)*cdbl(arTypicalOrderDetails(5,i))
						else
							x="qty" & arTypicalOrderDetails(0,i)
							qty1=request(x)
							Total=Total+(cdbl(arTypicalOrderDetails(5,i))*qty1)
							
							y="qtyoldval" & arTypicalOrderDetails(0,i)
							qtyoldval1=request(y)
							%>
							<input type="hidden" name="qty<%=arTypicalOrderDetails(0,i)%>" value="0">
							<input type="hidden" name="qtyoldval<%=arTypicalOrderDetails(0,i)%>" value="<%=qtyoldval1%>">
						<%
						end if
						else
						'if ((cint(arTypicalOrderDetails(3,i))*CDbl(arTypicalOrderDetails(5,i)))>30.0) then
'							strBgColour="#FB4A6F"
'						end if
'						if (arTypicalOrderDetails(7,i)=0) then
'							strBgColour="#FFE07F"
'						end if
						%>
						<tr bgcolor="<%=strBgColour%>" height="22">
							<td><%=arTypicalOrderDetails(0,i)%><b></b></td>
							<td><%=arTypicalOrderDetails(1,i)%></td>
							
							<td><%=arTypicalOrderDetails(2,i)%><b></b></td>
							
							<td>
							 <%if request("confirm")="No" then
							 
							 	x="qty" & arTypicalOrderDetails(0,i)
								qty1=request(x)
								Total=Total+(cdbl(arTypicalOrderDetails(5,i))*qty1)
								
								y="qtyoldval" & arTypicalOrderDetails(0,i)
								qtyoldval1=request(y)
								 
								%>
							 	<%=qty1%>
								<input type="hidden" name="qty<%=arTypicalOrderDetails(0,i)%>" value="<%=qty1%>">
								<input type="hidden" name="qtyoldval<%=arTypicalOrderDetails(0,i)%>" value="<%=qtyoldval1%>">
							 <%else
							 Total=Total+(cdbl(arTypicalOrderDetails(5,i))*arTypicalOrderDetails(3,i))
							 TotalOrderQty=TotalOrderQty+arTypicalOrderDetails(3,i)
							 %>	
								<input type="text" onChange="ConfirmIs48Product(document.frmOrder.LateProd2<%=intCount%>.value,document.frmOrder.vDateDiff.value,'qty<%=arTypicalOrderDetails(0,i)%>',document.frmOrder.Oldqty<%=intCount%>.value)" name="qty<%=arTypicalOrderDetails(0,i)%>" size="8" value="<%=arTypicalOrderDetails(3,i)%>" maxlength="6" style="font-family: Verdana; font-size: 8pt">
								<input type="hidden" name="qtyoldval<%=arTypicalOrderDetails(0,i)%>" value="<%=arTypicalOrderDetails(3,i)%>">
							<%end if%>
						</td>
						<td width="100" align="right"><%=FormatNumber(arTypicalOrderDetails(5,i),2)%>&nbsp;</td>
						<td align="right">
						<%
						if (arTypicalOrderDetails(9,i)="1") then
							response.write(arTypicalOrderDetails(10,i))
						else
							response.write(arTypicalOrderDetails(8,i))
						end if
						%>
						</td>
						<td width="100" align="right"><%=FormatNumber(arTypicalOrderDetails(3,i)*cdbl(arTypicalOrderDetails(5,i)),2)%>&nbsp;</td>
						</tr>
						<% end if %>
						<%
						intCount=intCount+1
						strProductStream = strProductStream & arTypicalOrderDetails(0,i) & "," 
						end if%>
						<%
						
					Next         
					
					If Trim(strProductStream) <> "" Then
						strProductStream = Left(strProductStream,len(strProductStream)-1)
					End if										
        End if%>
        
        <tr bgcolor="#AAAAAA">
		<td colspan="3" align="right"><strong>Total Order Qty:&nbsp;</strong></td>
		<td><strong><%=TotalOrderQty%></strong></td>
		<td align="right" height="25" colspan="2" ><b>Total Order Value:</b></td><td align="right" height="25" ><b><%=FormatNumber(Total,2)%>&nbsp;</b></td>
		
		</tr>
        <tr bgcolor="#FFFFFF">
          <td colspan="<%if request("confirm")="No" then%>7<%else%>7<%end if%>">
            <table border="0" width="100%">
              <tr>
           
                <td width="33%" align="center">&nbsp;<% if request("confirm")="" then%><input type="button" onClick="Javascript:document.product.submit();" value="Add Product" name="B2" style="font-family: Verdana; font-size: 8pt"><%end if%></td>
                
                 <td width="33%" align="center">&nbsp;<% if request("confirm")="" then%><input type="button" onClick="document.frmOrder.confirm.value = 'Yes';document.frmOrder.SaveAddProduct.value = 'Yes'; validation();" value="Save & Add Product" name="B2" style="font-family: Verdana; font-size: 8pt"><%end if%></td>
          
          
                <td width="33%" align="center">&nbsp;<input type="button" onClick="history.back();" value="Go Back" name="Go Back" style="font-family: Verdana; font-size: 8pt"></td>
       
                 <% if request("UType")<>"N" then%>
                <td width="34%" align="center"><input type="button" onClick="validation()" value="<%=strOption%>" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
             	<%end if%>
				
              </tr>
            </table>
          </td>
		
        </tr>
      </table>
    </td>
  </tr>   
		
		<Input type="hidden" name="ProductStream" value="<%=strProductStream%>">
		<Input type="hidden" name="OrderNo" value="<%=vOrderNo%>">		
		<Input type="hidden" name="Update" value="True">
		<Input type="hidden" name="SaveAddProduct" value="">
   </form>
</table>
<br>

  </center>
</div>
     <form method="POST" action="order_addproduct.asp" name="product">
       <Input type="hidden" name="OrderNo" value="<%=vOrderNo%>">
	   <Input type="hidden" name="vDateDiff" value="<%=strDateDiff%>">
	   <INPUT type="hidden" id="hidDeliveryDate" name="hidDeliveryDate" value="<%=strDeliveryDate%>">
     </form>
                <form method="POST" action="order_new.asp" name="back">
             
                </form>
<Input type="hidden" name="showclearorders" id="showclearorders">       
</body>

</html><%
if IsArray(arTypicalOrder) Then Erase arTypicalOrder  
if IsArray(arTypicalOrderDetails) Then Erase arTypicalOrderDetails 
%>