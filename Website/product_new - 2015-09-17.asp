<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
dim strismultipleproductvalue
dim strProductGroupList
strismultipleproduct=request.form("ismultipleproduct")
if (not isnumeric(strismultipleproduct)) then
	strismultipleproduct=0
end if	
vpno= request.form("pno")

strProductGroupList = Request.Form("hdSelectedGroup")
if vpno <> "" then 
	set object = Server.CreateObject("bakery.product")
	object.SetEnvironment(strconnection)
	
	set DisplayProductDetail= object.DisplayProductDetail(vpno)
	vRecArray1 = DisplayProductDetail("ProductDetail")
	vRecArray2 = DisplayProductDetail("ProductAdditiveDetail")
	vRecArray3 = DisplayProductDetail("ProductPriceDetail")
	set DisplayProductDetail= nothing
	
	set vListofMultipleProducts= object.ListofMultipleProducts(vpno)
	vRecArray = vListofMultipleProducts("MultipleProduct")
	set vListofMultipleProducts= nothing
	
	set DisplayProductGroupDetail= object.DisplayOnlineProductGroupByPNo(vpno)
	vRecArray5 = DisplayProductGroupDetail("ProductGroupList")
	
	For i = 0 to UBound(vRecArray5,2)

        
	  If strProductGroupList = "" OR strProductGroupList = Empty  Then
	    strProductGroupList = vRecArray5(0,i) & "|" & vRecArray5(1,i)
	  Else
	    strProductGroupList = strProductGroupList & "," & vRecArray5(0,i) & "|" & vRecArray5(1,i)
	  End IF
 
	Next
	
	set DisplayProductGroupDetail= nothing
	
	set object = nothing
end if

Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayDoughType()	
vdoughtypearray =  detail("DoughType") 
set detail = object.DisplayIng()	
vingarray =  detail("Ingredients") 
set detail = object.DisplayFacility()	
vFacarray =  detail("Facility")

set detail = object.DisplayTypeDes()	
vType1Array =  detail("Type1")
vType2Array =  detail("Type2") 
vType3ForPackingArray =  detail("Type3ForPacking")
vType4ForPackingArray =  detail("Type4ForPacking")

set BODetails = object.GetBakingOrder()
vBakingOrder = BODetails("BakingOrder")

set detail = object.ListProductionCategory()	
vProductionCategoryarray =  detail("ProductionCategory") 

set detail = object.ListConsolidationType()	
vConsolidationTypearray =  detail("ConsolidationType") 


Set obj = CreateObject("Bakery.Customer")
obj.SetEnvironment(strconnection)
set objCustomer = obj.Display_Grouplist()
arGroup = objCustomer("GroupList")	
 
set detail = Nothing
set object = Nothing
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<SCRIPT src="Includes/Script.js" type=text/javascript></SCRIPT>
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script Language="JavaScript">
<!--
function goto(url)
{
	document.location = url;
}

function FrontPage_Form1_Validator(theForm)
{
  if (theForm.oldcode.value == "")
  {
    alert("Please enter a value for the \"Old Code\" field.");
    theForm.oldcode.focus();
    return;
  }

  if (theForm.oldcode.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Old Code\" field.");
    theForm.oldcode.focus();
    return;
  }

  if (theForm.pname.value == "")
  {
    alert("Please enter a value for the \"Name\" field.");
    theForm.pname.focus();
    return;
  }

  if (theForm.pname.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Name\" field.");
    theForm.pname.focus();
    return;
  }

  if (theForm.doughtype.selectedIndex < 0)
  {
    if (document.FrontPage_Form1.ismultipleproduct.selectedIndex==0)
	{
		alert("Please select one of the \"Dough Name\" options.");
		theForm.doughtype.focus();
		return;
	}
  }
  
  if (theForm.ProductionCategory.selectedIndex <= 0)
  {
    alert("Please select one of the \"Production Category\" options.");
    theForm.ProductionCategory.focus();
    return;
  }

	//validate additives name
	var j = 1;
	for (j=1; j<= 5 ; j++)
	{
		if(document.getElementById('Qty' + j).value != "")
		{
			
			if(document.getElementById('Ing' + j).selectedIndex == "")
			{
			   alert('Please select a additives name for ' + j + ' Item');
				document.getElementById('Ing' + j).focus();
				return;
			}
		}
	
	}
	
	//validate QTY
	var j = 1;
	for (j=1; j<= 5 ; j++)
	{
		if(document.getElementById('Ing' + j).selectedIndex != "")
		{
			if(document.getElementById('Qty' + j).value == "")
			{
				alert('Please select a Qty for ' + j + ' Item');
				document.getElementById('Qty' + j).focus();
				return;
			}
		}	
	}
	
  if (theForm.doughtype.selectedIndex == 0 )
  {
   	if (document.FrontPage_Form1.ismultipleproduct.selectedIndex==0)
	{
		alert("The first \"Dough Name\" option is not a valid selection.  Please choose one of the other options.");
		theForm.doughtype.focus();
		return;
	}
  }  
  if (theForm.PreSize.value == "")
  {
    alert("Please enter a value for the \"Pre Baking\" field.");
    theForm.PreSize.focus();
    return;
  }

  if (theForm.PreSize.value.length > 10)
  {
    alert("Please enter at most 10 characters in the \"Pre Baking\" field.");
    theForm.PreSize.focus();
    return;
  }

  if (theForm.PostSize.value == "")
  {
    alert("Please enter a value for the \"Post Baking\" field.");
    theForm.PostSize.focus();
    return;
  }

  if (theForm.PostSize.value.length > 10)
  {
    alert("Please enter at most 10 characters in the \"Post Baking\" field.");
    theForm.PostSize.focus();
    return;
  }

  if (theForm.Shape.value=="" && theForm.HidShape.value=="")
  {
    alert("Please enter a value for the \"Shape\" field.");
    theForm.Shape.focus();
    return;
  }

  if (theForm.Shape.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Shape\" field.");
    theForm.Shape.focus();
    return;
  }
  
  if (theForm.Slice.selectedIndex < 0 && theForm.HidSlice.value=="")
  {
    alert("Please select one of the \"Slice\" options.");
    theForm.Slice.focus();
    return;
  }

  if (theForm.Slice.selectedIndex == 0 && theForm.HidSlice.value=="")
  {
    alert("The first \"Slice\" option is not a valid selection.  Please choose one of the other options.");
    theForm.Slice.focus();
    return;
  }  

  if (theForm.fno.selectedIndex < 0 && theForm.Hidfno.value=="")
  {
    alert("Please select one of the \"Facility\" options.");
    theForm.fno.focus();
    return;
  }
  
  if (theForm.fno.selectedIndex == 0 && theForm.Hidfno.value=="")
  {
    alert("The first \"Facility\" option is not a valid selection.  Please choose one of the other options.");
    theForm.fno.focus();
    return;
  }

  if (theForm.vat.selectedIndex < 0 && theForm.Hidvat.value=="")
  {
    alert("Please select one of the \"VAT\" options.");
    theForm.vat.focus();
    return;
  }

  if (theForm.vat.selectedIndex == 0 && theForm.Hidvat.value=="")
  {
    alert("The first \"VAT\" option is not a valid selection.  Please choose one of the other options.");
    theForm.vat.focus();
    return;
  }

  if (theForm.selLastProd.selectedIndex == 0 && theForm.HidLastProd.value==""){
    alert("Please select a 48 hrs products");
    theForm.selLastProd.focus();
    return;
  }

  if (theForm.MulProd.value == "")
  {
    alert("Please enter a value for the \"No of Products\" field.");
    theForm.MulProd.focus();
    return;
  }

  if (theForm.Type1.selectedIndex <= 0 && theForm.HidType1.value=="")
  {
    alert("Please select one of the \"Type1\" options.");
    theForm.Type1.focus();
    return;
  }
  
  if (theForm.Type2.selectedIndex <= 0 && theForm.HidType2.value=="")
  {
    alert("Please select one of the \"Type2\" options.");
    theForm.Type2.focus();
    return;
  }

//  if (theForm.ProductVisible.selectedIndex <= 0)
//  {
//    alert("Please select one of the \"ProductVisible\" options.");
//    theForm.ProductVisible.focus();
//    return;
  //  }

  var s = document.getElementById("pvSelectedGroup");
  if (s.options.length == 0) {
      alert("Please Select customer group for online product availability.");
      theForm.pvSelectedGroup.focus();
      return;
  }
  
  if (theForm.price1.value == "")
  {
    alert("Please enter a value for the \"Price A\" field.");
    theForm.price1.focus();
    return;
  }
  
  if (theForm.priceA1.value == "")
  {
    alert("Please enter a value for the \"Price A1\" field.");
    theForm.priceA1.focus();
    return;
  }
  
  if (theForm.priceB.value == "")
  {
    alert("Please enter a value for the \"Price B\" field.");
    theForm.priceB.focus();
    return;
  }  
  
  if (theForm.priceC.value == "")
  {
    alert("Please enter a value for the \"Price C\" field.");
    theForm.priceC.focus();
    return;
  }  
  
  if (theForm.priceD.value == "")
  {
    alert("Please enter a value for the \"Price D\" field.");
    theForm.priceD.focus();
    return;
  }
  
  if (theForm.priceB1.value == "")
  {
    alert("Please enter a value for the \"Price B1\" field.");
    theForm.priceB1.focus();
    return;
  }
  
  if (theForm.priceB1.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B1\" field.");
    theForm.priceB1.focus();
    return;
  }
  
  intPrice1=theForm.priceB1.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B1');
	theForm.priceB1.focus()
	return false;
  }
  
  if (theForm.priceB2.value == "")
  {
    alert("Please enter a value for the \"Price B2\" field.");
    theForm.priceB2.focus();
    return;
  }
  if (theForm.priceB2.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B2\" field.");
    theForm.priceB2.focus();
    return;
  }
  intPrice1=theForm.priceB2.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B2');
	theForm.priceB2.focus()
	return false;
  }
  
  if (theForm.priceB3.value == "")
  {
    alert("Please enter a value for the \"Price B3\" field.");
    theForm.priceB3.focus();
    return;
  }
  if (theForm.priceB3.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B3\" field.");
    theForm.priceB3.focus();
    return;
  }
  intPrice1=theForm.priceB3.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B3');
	theForm.priceB3.focus()
	return false;
  }
  
  if (theForm.priceB4.value == "")
  {
    alert("Please enter a value for the \"Price B4\" field.");
    theForm.priceB4.focus();
    return;
  }
  if (theForm.priceB4.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B4\" field.");
    theForm.priceB4.focus();
    return;
  }
  intPrice1=theForm.priceB4.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B4');
	theForm.priceB4.focus()
	return false;
  }
  
  if (theForm.priceB5.value == "")
  {
    alert("Please enter a value for the \"Price B5\" field.");
    theForm.priceB5.focus();
    return;
  }
  if (theForm.priceB5.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B5\" field.");
    theForm.priceB5.focus();
    return;
  }
  intPrice1=theForm.priceB5.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B5');
	theForm.priceB5.focus()
	return false;
  }
  
  
 if (theForm.priceB6.value == "")
  {
    alert("Please enter a value for the \"Price B6\" field.");
    theForm.priceB6.focus();
    return;
  }
  if (theForm.priceB6.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B6\" field.");
    theForm.priceB6.focus();
    return;
  }
  intPrice1=theForm.priceB6.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B6');
	theForm.priceB6.focus()
	return false;
  }
  
  
  
  if (theForm.priceB7.value == "")
  {
    alert("Please enter a value for the \"Price B7\" field.");
    theForm.priceB7.focus();
    return;
  }
  if (theForm.priceB7.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B7\" field.");
    theForm.priceB7.focus();
    return;
  }
  intPrice1=theForm.priceB7.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B7');
	theForm.priceB7.focus()
	return false;
  }
  
  
   if (theForm.priceB8.value == "")
  {
    alert("Please enter a value for the \"Price B8\" field.");
    theForm.priceB8.focus();
    return;
  }
  if (theForm.priceB8.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B8\" field.");
    theForm.priceB8.focus();
    return;
  }
  intPrice1=theForm.priceB8.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B8');
	theForm.priceB8.focus()
	return false;
  }
  
  
    if (theForm.priceB9.value == "")
  {
    alert("Please enter a value for the \"Price B9\" field.");
    theForm.priceB9.focus();
    return;
  }
  if (theForm.priceB9.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B9\" field.");
    theForm.priceB9.focus();
    return;
  }
  intPrice1=theForm.priceB9.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B9');
	theForm.priceB9.focus()
	return false;
  }
  
  if (theForm.priceB10.value == "")
  {
    alert("Please enter a value for the \"Price B10\" field.");
    theForm.priceB10.focus();
    return;
  }
  if (theForm.priceB10.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B10\" field.");
    theForm.priceB10.focus();
    return;
  }
  intPrice1=theForm.priceB10.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B10');
	theForm.priceB10.focus()
	return false;
  }

  if (theForm.priceB11.value == "")
  {
    alert("Please enter a value for the \"Price B11\" field.");
    theForm.priceB11.focus();
    return;
  }
  if (theForm.priceB11.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B11\" field.");
    theForm.priceB11.focus();
    return;
  }
  intPrice1=theForm.priceB11.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B11');
	theForm.priceB11.focus()
	return false;
  }
  
  if (theForm.priceB12.value == "")
  {
    alert("Please enter a value for the \"Price B12\" field.");
    theForm.priceB12.focus();
    return;
  }
  if (theForm.priceB12.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B12\" field.");
    theForm.priceB12.focus();
    return;
  }
  intPrice1=theForm.priceB12.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B12');
	theForm.priceB12.focus()
	return false;
  }
 
  if (theForm.priceB13.value == "")
  {
    alert("Please enter a value for the \"Price B13\" field.");
    theForm.priceB13.focus();
    return;
  }
  if (theForm.priceB13.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B13\" field.");
    theForm.priceB13.focus();
    return;
  }
  intPrice1=theForm.priceB13.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B13');
	theForm.priceB13.focus()
	return false;
  }

  if (theForm.priceB14.value == "")
  {
    alert("Please enter a value for the \"Price B14\" field.");
    theForm.priceB14.focus();
    return;
  }
  if (theForm.priceB14.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B14\" field.");
    theForm.priceB14.focus();
    return;
  }
  intPrice1=theForm.priceB14.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B14');
	theForm.priceB14.focus()
	return false;
  } 
 
  if (theForm.priceB15.value == "")
  {
    alert("Please enter a value for the \"Price B15\" field.");
    theForm.priceB15.focus();
    return;
  }
  if (theForm.priceB15.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B15\" field.");
    theForm.priceB15.focus();
    return;
  }
  intPrice1=theForm.priceB15.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B15');
	theForm.priceB15.focus()
	return false;
  } 
 
   
   if (theForm.price1.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price A\" field.");
    theForm.price1.focus();
    return;
  }
  
   if (theForm.priceA1.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price A1\" field.");
    theForm.priceA1.focus();
    return;
  }
  
  intPrice1=theForm.price1.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Factor');
	theForm.price1.focus()
	return false;
  }
  intPriceA1=theForm.priceA1.value
  if (((intPriceA1 / intPriceA1) != 1) && (intPriceA1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Factor');
	theForm.priceA1.focus()
	return false;
  }
  
  if (theForm.priceD1.value == "")
  {
    alert("Please enter a value for the \"Price D1\" field.");
    theForm.priceD1.focus();
    return;
  }
  if (theForm.priceD1.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price D1\" field.");
    theForm.priceD1.focus();
    return;
  }

  if (theForm.priceD2.value == "")
  {
    alert("Please enter a value for the \"Price D2\" field.");
    theForm.priceD2.focus();
    return;
  }
  if (theForm.priceD2.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price D2\" field.");
    theForm.priceD2.focus();
    return;
  }

  if (theForm.priceD3.value == "")
  {
    alert("Please enter a value for the \"Price D3\" field.");
    theForm.priceD3.focus();
    return;
  }
  if (theForm.priceD3.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price D3\" field.");
    theForm.priceD3.focus();
    return;
  }

  if (theForm.priceD4.value == "")
  {
    alert("Please enter a value for the \"Price D4\" field.");
    theForm.priceD4.focus();
    return;
  }
  if (theForm.priceD4.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price D4\" field.");
    theForm.priceD4.focus();
    return;
  }

  if (theForm.priceD5.value == "")
  {
    alert("Please enter a value for the \"Price D5\" field.");
    theForm.priceD5.focus();
    return;
  }
  if (theForm.priceD5.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price D5\" field.");
    theForm.priceD5.focus();
    return;
  }
 
  //This validation added by Thilina 17-05-2010
  if (document.FrontPage_Form1.ismultipleproduct.selectedIndex == 0) {
      var elm = document.getElementById('fno');

      //If the facility is selected as �GAILs central Kitchen�, then this G price band is mandatory
      if (elm.options[elm.selectedIndex].value == 34) {
          if (theForm.priceG1.value == "") {
              alert("Please enter a value for the \"Price G\" field.");
              theForm.priceG1.focus();
              return;
          }


      }
  }
  
    if (theForm.priceG1.value.length > 6)
	  {
	      alert("Please enter at most 6 characters in the \"Price G\" field.");
	      theForm.priceG1.focus();
	      return;
	   }
	    intPrice1=theForm.priceG1.value
	    if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
	    {
		    alert('Please enter only numbers or decimal points in Price G');
		    theForm.priceG1.focus()
		    return false;
	    }
  
  
  
  //----------- added Ajantha 2006 Feb 28 Start--------------------
  
  //Validate for Pre Baking
  if (theForm.PreSize.value == "")
  {
    alert("Please enter a value for the \"Pre Baking\" field.");
    theForm.PreSize.focus();
    return;
  }
  
  if (!checkNum(theForm.PreSize.value, -1))
  {
    alert("Please enter Integer value in the \"Pre Baking \" field.");
    theForm.PreSize.focus();
    return;
  }
  
    if (isZero(theForm.PreSize.value) && isZero(theForm.HidPreSize.value))
  {
    alert("Please enter Non Zero value in the \"Pre Baking \" field.");
    theForm.PreSize.focus();
    return;
  }
  
  //validate for Post Baking
  if (theForm.PostSize.value == "")
  {
    alert("Please enter a value for the \"Post Baking\" field.");
    theForm.PostSize.focus();
    return;
  }
  
  if (!checkNum(theForm.PostSize.value, -1))
  {
    alert("Please enter Integer value in the \"Post Baking \" field.");
    theForm.PostSize.focus();
    return;
  }
  
  
   if (isZero(theForm.PostSize.value) && isZero(theForm.HidPostSize.value))
  {
    alert("Please enter Non Zero value in the \"Post Baking \" field.");
    theForm.PostSize.focus();
    return;
  }
  
  if (!checkNum(theForm.TrayQty.value, -1))
  {
    alert("Please enter Integer value in the \"Tray Qty \" field.");
    theForm.TrayQty.focus();
    return;
  }
  
  if (theForm.Ing2.selectedIndex > 0)
  {
    if (theForm.Ing2.selectedIndex==theForm.Ing1.selectedIndex)
	{
		alert("Please select a diffrent additives name.");
    	theForm.Ing2.focus();
    	return;
	}	
  }
  
  if (theForm.Ing3.selectedIndex > 0)
  {
    if ((theForm.Ing3.selectedIndex==theForm.Ing1.selectedIndex) || (theForm.Ing3.selectedIndex==theForm.Ing2.selectedIndex))
	{
		alert("Please select a diffrent additives name.");
    	theForm.Ing3.focus();
    	return;
	}
  }
  
  if (theForm.Ing4.selectedIndex > 0)
  {
    if ((theForm.Ing4.selectedIndex==theForm.Ing1.selectedIndex) || (theForm.Ing4.selectedIndex==theForm.Ing2.selectedIndex) || (theForm.Ing4.selectedIndex==theForm.Ing3.selectedIndex))
	{
		alert("Please select a diffrent additives name.");
    	theForm.Ing4.focus();
    	return;
	}
  }
  if (theForm.Ing5.selectedIndex > 0)
  {
    if ((theForm.Ing5.selectedIndex==theForm.Ing1.selectedIndex) || (theForm.Ing5.selectedIndex==theForm.Ing2.selectedIndex) || (theForm.Ing5.selectedIndex==theForm.Ing3.selectedIndex) || (theForm.Ing5.selectedIndex==theForm.Ing4.selectedIndex))
	{
		alert("Please select a diffrent additives name.");
    	theForm.Ing5.focus();
    	return;
	}
  }
  	
  if(!(confirm("Are you sure the quantity and the price entered are numeric values?"))){	
		return;	
	}
	
  theForm.submit();
}

/* ------------------------------------------------------------------------------------------------------- */
//Multiple Product
function FrontPage_Form1_ValidatorMultiple(theForm)
{
   
  document.getElementById("hidAddProductForMultiple").value="Add Product For Multiple";

  if (theForm.oldcode.value == "")
  {
    alert("Please enter a value for the \"Old Code\" field.");
    theForm.oldcode.focus();
    return;
  }

  if (theForm.oldcode.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Old Code\" field.");
    theForm.oldcode.focus();
    return;
  }

  if (theForm.pname.value == "")
  {
    alert("Please enter a value for the \"Name\" field.");
    theForm.pname.focus();
    return;
  }

  if (theForm.pname.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Name\" field.");
    theForm.pname.focus();
    return;
  }
  
  if (theForm.doughtype.selectedIndex < 0)
  {
    if (document.FrontPage_Form1.ismultipleproduct.selectedIndex==0)
	{
		alert("Please select one of the \"Dough Name\" options.");
		theForm.doughtype.focus();
		return;
	}
  }
 
  // multi product
 /* if (theForm.ProductionCategory.selectedIndex <= 0)
  {
    alert("Please select one of the \"Production Category\" options.");
    theForm.ProductionCategory.focus();
    return;
  }*/

	//validate additives name
	var j = 1;
	for (j=1; j<= 5 ; j++)
	{
		if(document.getElementById('Qty' + j).value != "")
		{
			
			if(document.getElementById('Ing' + j).selectedIndex == "")
			{
			   alert('Please select a additives name for ' + j + ' Item');
				document.getElementById('Ing' + j).focus();
				return;
			}
		}
	
	}
	
	//validate QTY
	var j = 1;
	for (j=1; j<= 5 ; j++)
	{
		if(document.getElementById('Ing' + j).selectedIndex != "")
		{
			if(document.getElementById('Qty' + j).value == "")
			{
				alert('Please select a Qty for ' + j + ' Item');
				document.getElementById('Qty' + j).focus();
				return;
			}
		}	
	}


  if (theForm.doughtype.selectedIndex == 0)
  {
      if (theForm.ismultipleproduct.selectedIndex == 0)
	{
		alert("The first \"Dough Name\" option is not a valid selection.  Please choose one of the other options.");
		theForm.doughtype.focus();
		return;
	}
  }
  if ((theForm.PreSize.value=="") && (theForm.HidPreSize.value!=1))
  {
    alert("Please enter a value for the \"Pre Baking\" field.");
    theForm.PreSize.focus();
    return;
  }
  if (theForm.PreSize.value.length > 10)
  {
    alert("Please enter at most 10 characters in the \"Pre Baking\" field.");
    theForm.PreSize.focus();
    return;
  }

  if (theForm.PostSize.value == "" && theForm.HidPostSize.value != "1")
  {
    alert("Please enter a value for the \"Post Baking\" field.");
    theForm.PostSize.focus();
    return;
  }

  if (theForm.PostSize.value.length > 10)
  {
    alert("Please enter at most 10 characters in the \"Post Baking\" field.");
    theForm.PostSize.focus();
    return;
  }
//  if (theForm.ProductVisible.selectedIndex <= 0)
//  {
//    alert("Please select one of the \"ProductVisible\" options.");
//    theForm.ProductVisible.focus();
//    return;
//  }

  var s = document.getElementById("pvSelectedGroup");
  if (s.options.length == 0) {
      alert("Please Select customer group for online product availability.");
      theForm.pvSelectedGroup.focus();
      return;
  }

  if (theForm.price1.value == "")
  {
    alert("Please enter a value for the \"Price A\" field.");
    theForm.price1.focus();
    return;
  }
  if (theForm.priceA1.value == "")
  {
    alert("Please enter a value for the \"Price A1\" field.");
    theForm.priceA1.focus();
    return;
  }
  if (theForm.priceB.value == "")
  {
    alert("Please enter a value for the \"Price B\" field.");
    theForm.priceB.focus();
    return;
  }  
  if (theForm.priceC.value == "")
  {
    alert("Please enter a value for the \"Price C\" field.");
    theForm.priceC.focus();
    return;
  }  
  
  if (theForm.priceD.value == "")
  {
    alert("Please enter a value for the \"Price D\" field.");
    theForm.priceD.focus();
    return;
  }  

  
  if (theForm.priceB1.value == "")
  {
    alert("Please enter a value for the \"Price B1\" field.");
    theForm.priceB1.focus();
    return;
  }
  if (theForm.priceB1.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B1\" field.");
    theForm.priceB1.focus();
    return;
  }
  intPrice1=theForm.priceB1.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B1');
	theForm.priceB1.focus()
	return false;
  }
  
  if (theForm.priceB2.value == "")
  {
    alert("Please enter a value for the \"Price B2\" field.");
    theForm.priceB2.focus();
    return;
  }
  if (theForm.priceB2.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B2\" field.");
    theForm.priceB2.focus();
    return;
  }
  intPrice1=theForm.priceB2.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B2');
	theForm.priceB2.focus()
	return false;
  }
  
  if (theForm.priceB3.value == "")
  {
    alert("Please enter a value for the \"Price B3\" field.");
    theForm.priceB3.focus();
    return;
  }
  if (theForm.priceB3.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B3\" field.");
    theForm.priceB3.focus();
    return;
  }
  intPrice1=theForm.priceB3.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B3');
	theForm.priceB3.focus()
	return false;
  }
  
  if (theForm.priceB4.value == "")
  {
    alert("Please enter a value for the \"Price B4\" field.");
    theForm.priceB4.focus();
    return;
  }
  if (theForm.priceB4.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B4\" field.");
    theForm.priceB4.focus();
    return;
  }
  intPrice1=theForm.priceB4.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B4');
	theForm.priceB4.focus()
	return false;
  }
  
  if (theForm.priceB5.value == "")
  {
    alert("Please enter a value for the \"Price B5\" field.");
    theForm.priceB5.focus();
    return;
  }
  if (theForm.priceB5.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B5\" field.");
    theForm.priceB5.focus();
    return;
  }
  intPrice1=theForm.priceB5.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B5');
	theForm.priceB5.focus()
	return false;
  }
  
  
 if (theForm.priceB6.value == "")
  {
    alert("Please enter a value for the \"Price B6\" field.");
    theForm.priceB6.focus();
    return;
  }
  if (theForm.priceB6.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B6\" field.");
    theForm.priceB6.focus();
    return;
  }
  intPrice1=theForm.priceB6.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B6');
	theForm.priceB6.focus()
	return false;
  }
  
  
  
  if (theForm.priceB7.value == "")
  {
    alert("Please enter a value for the \"Price B7\" field.");
    theForm.priceB7.focus();
    return;
  }
  if (theForm.priceB7.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B7\" field.");
    theForm.priceB7.focus();
    return;
  }
  intPrice1=theForm.priceB7.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B7');
	theForm.priceB7.focus()
	return false;
  }
  
  
   if (theForm.priceB8.value == "")
  {
    alert("Please enter a value for the \"Price B8\" field.");
    theForm.priceB8.focus();
    return;
  }
  if (theForm.priceB8.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B8\" field.");
    theForm.priceB8.focus();
    return;
  }
  intPrice1=theForm.priceB8.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B8');
	theForm.priceB8.focus()
	return false;
  }
  
  
    if (theForm.priceB9.value == "")
  {
    alert("Please enter a value for the \"Price B9\" field.");
    theForm.priceB9.focus();
    return;
  }
  if (theForm.priceB9.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B9\" field.");
    theForm.priceB9.focus();
    return;
  }
  intPrice1=theForm.priceB9.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B9');
	theForm.priceB9.focus()
	return false;
  }
  
  if (theForm.priceB10.value == "")
  {
    alert("Please enter a value for the \"Price B10\" field.");
    theForm.priceB10.focus();
    return;
  }
  if (theForm.priceB10.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B10\" field.");
    theForm.priceB10.focus();
    return;
  }
  intPrice1=theForm.priceB10.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B10');
	theForm.priceB10.focus()
	return false;
  }

  if (theForm.priceB11.value == "")
  {
    alert("Please enter a value for the \"Price B11\" field.");
    theForm.priceB11.focus();
    return;
  }
  if (theForm.priceB11.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B11\" field.");
    theForm.priceB11.focus();
    return;
  }
  intPrice1=theForm.priceB11.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B11');
	theForm.priceB11.focus()
	return false;
  }
  
  if (theForm.priceB12.value == "")
  {
    alert("Please enter a value for the \"Price B12\" field.");
    theForm.priceB12.focus();
    return;
  }
  if (theForm.priceB12.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B12\" field.");
    theForm.priceB12.focus();
    return;
  }
  intPrice1=theForm.priceB12.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B12');
	theForm.priceB12.focus()
	return false;
  }
 
  if (theForm.priceB13.value == "")
  {
    alert("Please enter a value for the \"Price B13\" field.");
    theForm.priceB13.focus();
    return;
  }
  if (theForm.priceB13.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B13\" field.");
    theForm.priceB13.focus();
    return;
  }
  intPrice1=theForm.priceB13.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B13');
	theForm.priceB13.focus()
	return false;
  }

  if (theForm.priceB14.value == "")
  {
    alert("Please enter a value for the \"Price B14\" field.");
    theForm.priceB14.focus();
    return;
  }
  if (theForm.priceB14.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B14\" field.");
    theForm.priceB14.focus();
    return;
  }
  intPrice1=theForm.priceB14.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B14');
	theForm.priceB14.focus()
	return false;
  } 
 
  if (theForm.priceB15.value == "")
  {
    alert("Please enter a value for the \"Price B15\" field.");
    theForm.priceB15.focus();
    return;
  }
  if (theForm.priceB15.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price B15\" field.");
    theForm.priceB15.focus();
    return;
  }
  intPrice1=theForm.priceB15.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Price B15');
	theForm.priceB15.focus()
	return false;
  } 
 
   
   if (theForm.price1.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price A\" field.");
    theForm.price1.focus();
    return;
  }
  
   if (theForm.priceA1.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price A1\" field.");
    theForm.priceA1.focus();
    return;
  }
  
  intPrice1=theForm.price1.value
  if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Factor');
	theForm.price1.focus()
	return false;
  }
  intPriceA1=theForm.priceA1.value
  if (((intPriceA1 / intPriceA1) != 1) && (intPriceA1 != 0)) 
  {
	alert('Please enter only numbers or decimal points in Factor');
	theForm.priceA1.focus()
	return false;
  }
  
  if (theForm.priceD1.value == "")
  {
    alert("Please enter a value for the \"Price D1\" field.");
    theForm.priceD1.focus();
    return;
  }
  if (theForm.priceD1.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price D1\" field.");
    theForm.priceD1.focus();
    return;
  }

  if (theForm.priceD2.value == "")
  {
    alert("Please enter a value for the \"Price D2\" field.");
    theForm.priceD2.focus();
    return;
  }
  if (theForm.priceD2.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price D2\" field.");
    theForm.priceD2.focus();
    return;
  }

  if (theForm.priceD3.value == "")
  {
    alert("Please enter a value for the \"Price D3\" field.");
    theForm.priceD3.focus();
    return;
  }
  if (theForm.priceD3.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price D3\" field.");
    theForm.priceD3.focus();
    return;
  }

  if (theForm.priceD4.value == "")
  {
    alert("Please enter a value for the \"Price D4\" field.");
    theForm.priceD4.focus();
    return;
  }
  if (theForm.priceD4.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price D4\" field.");
    theForm.priceD4.focus();
    return;
  }

  if (theForm.priceD5.value == "")
  {
    alert("Please enter a value for the \"Price D5\" field.");
    theForm.priceD5.focus();
    return;
  }
  if (theForm.priceD5.value.length > 6)
  {
    alert("Please enter at most 6 characters in the \"Price D5\" field.");
    theForm.priceD5.focus();
    return;
  }
 
  //This validation added by Thilina 17-05-2010
  /*
  var elm = document.getElementById('fno');

//If the facility is selected as �GAILs central Kitchen�, then this G price band is mandatory
  if (elm.options[elm.selectedIndex].value == 34)
  {
      if (theForm.priceG1.value == "")
      {
          alert("Please enter a value for the \"Price G\" field.");
	      theForm.priceG1.focus();
	      return;
	  }
	  
	 
  }
  */
  
    if (theForm.priceG1.value.length > 6)
	  {
	      alert("Please enter at most 6 characters in the \"Price G\" field.");
	      theForm.priceG1.focus();
	      return;
	   }
	    intPrice1=theForm.priceG1.value
	    if (((intPrice1 / intPrice1) != 1) && (intPrice1 != 0)) 
	    {
		    alert('Please enter only numbers or decimal points in Price G');
		    theForm.priceG1.focus()
		    return false;
	    }
  
  
  
  //----------- added Ajantha 2006 Feb 28 Start--------------------
  
  //Validate for Pre Baking
  if (!checkNum(theForm.PreSize.value, -1) && !checkNum(theForm.HidPreSize.value, -1))
  {
    alert("Please enter Integer value in the \"Pre Baking \" field.");
    theForm.PreSize.focus();
    return;
  }
  
  /*
   if (isZero(theForm.PreSize.value) && !isZero(theForm.HidPreSize.value, -1))
  {
    alert("Please enter Non Zero value in the \"Pre Baking \" field.");
    theForm.PreSize.focus();
    return;
  }*/
  
  //validate for Post Baking
  if (!checkNum(theForm.PostSize.value, -1) && !checkNum(theForm.HidPostSize.value, -1))
  {
    alert("Please enter Integer value in the \"Post Baking \" field.");
    theForm.PostSize.focus();
    return;
  }
  
  
  /* if (isZero(theForm.PostSize.value) && !isZero(theForm.HidPostSize.value, -1))
  {
    alert("Please enter Non Zero value in the \"Post Baking \" field.");
    theForm.PostSize.focus();
    return;
  }*/
  
  if (!checkNum(theForm.TrayQty.value, -1))
  {
    alert("Please enter Integer value in the \"Tray Qty \" field.");
    theForm.TrayQty.focus();
    return;
  }
  
  
  if (theForm.Ing2.selectedIndex > 0)
  {
    if (theForm.Ing2.selectedIndex==theForm.Ing1.selectedIndex)
	{
		alert("Please select a diffrent additives name.");
    	theForm.Ing2.focus();
    	return;
	}	
  }
  
  if (theForm.Ing3.selectedIndex > 0)
  {
    if ((theForm.Ing3.selectedIndex==theForm.Ing1.selectedIndex) || (theForm.Ing3.selectedIndex==theForm.Ing2.selectedIndex))
	{
		alert("Please select a diffrent additives name.");
    	theForm.Ing3.focus();
    	return;
	}
  }
  
  if (theForm.Ing4.selectedIndex > 0)
  {
    if ((theForm.Ing4.selectedIndex==theForm.Ing1.selectedIndex) || (theForm.Ing4.selectedIndex==theForm.Ing2.selectedIndex) || (theForm.Ing4.selectedIndex==theForm.Ing3.selectedIndex))
	{
		alert("Please select a diffrent additives name.");
    	theForm.Ing4.focus();
    	return;
	}
  }
  if (theForm.Ing5.selectedIndex > 0)
  {
    if ((theForm.Ing5.selectedIndex==theForm.Ing1.selectedIndex) || (theForm.Ing5.selectedIndex==theForm.Ing2.selectedIndex) || (theForm.Ing5.selectedIndex==theForm.Ing3.selectedIndex) || (theForm.Ing5.selectedIndex==theForm.Ing4.selectedIndex))
	{
		alert("Please select a diffrent additives name.");
    	theForm.Ing5.focus();
    	return;
	}
  }
  	
  if(!(confirm("Are you sure the quantity and the price entered are numeric values?"))){	
		return;	
	}
	
  theForm.submit();
}

function validate(frm,type){
	if(confirm("Would you like to "+ type +" this product?")){

		if (type=='delete'){
			frm.type.value='Delete';		
			frm.submit();}
		else{	
			FrontPage_Form1_Validator(frm)
		}	
	}	
}
function PriceChanges(theForm1)
{
 	document.FrontPage_Form1.priceB.value=(parseFloat(document.FrontPage_Form1.price1.value) * 0.98).toFixed(2)
  	document.FrontPage_Form1.priceC.value=(parseFloat(document.FrontPage_Form1.price1.value) * 0.93).toFixed(2)
  	document.FrontPage_Form1.priceD.value=(parseFloat(document.FrontPage_Form1.price1.value) * 0.95).toFixed(2)
	
   	document.FrontPage_Form1.priceD1.value=document.FrontPage_Form1.price1.value
   	document.FrontPage_Form1.priceD2.value=document.FrontPage_Form1.price1.value
   	document.FrontPage_Form1.priceD3.value=document.FrontPage_Form1.price1.value
   	document.FrontPage_Form1.priceD4.value=document.FrontPage_Form1.price1.value
   	document.FrontPage_Form1.priceD5.value=document.FrontPage_Form1.price1.value
  	document.FrontPage_Form1.priceB1.value=document.FrontPage_Form1.price1.value
	document.FrontPage_Form1.priceB2.value=document.FrontPage_Form1.price1.value
	document.FrontPage_Form1.priceB3.value=document.FrontPage_Form1.price1.value
	document.FrontPage_Form1.priceB4.value=document.FrontPage_Form1.price1.value
	document.FrontPage_Form1.priceB5.value=document.FrontPage_Form1.price1.value
	document.FrontPage_Form1.priceB6.value=document.FrontPage_Form1.price1.value
	document.FrontPage_Form1.priceB7.value=document.FrontPage_Form1.price1.value
	document.FrontPage_Form1.priceB8.value=document.FrontPage_Form1.price1.value
	document.FrontPage_Form1.priceB9.value=document.FrontPage_Form1.price1.value
	document.FrontPage_Form1.priceB10.value=document.FrontPage_Form1.price1.value
	document.FrontPage_Form1.priceB11.value=document.FrontPage_Form1.price1.value
	document.FrontPage_Form1.priceB12.value=document.FrontPage_Form1.price1.value

	document.FrontPage_Form1.priceB13.value=(parseFloat(document.FrontPage_Form1.price1.value) * 0.96).toFixed(2)
	document.FrontPage_Form1.priceB14.value=document.FrontPage_Form1.price1.value
	document.FrontPage_Form1.priceB15.value=(parseFloat(document.FrontPage_Form1.price1.value) * 1.10).toFixed(2)

	document.FrontPage_Form1.priceG1.value=document.FrontPage_Form1.price1.value
	
	document.FrontPage_Form1.priceA1.value=(parseFloat(document.FrontPage_Form1.price1.value) * 0.9).toFixed(2)
	
	document.FrontPage_Form1.priceH.value=(parseFloat(document.FrontPage_Form1.price1.value) * 0.93).toFixed(2)
	
	document.FrontPage_Form1.priceI.value=(parseFloat(document.FrontPage_Form1.price1.value)).toFixed(2)
	
}  

function changeismultipleproduct()
{
	// if ismultipleproduct is No
	if (document.FrontPage_Form1.ismultipleproduct.selectedIndex==0)
	{
		document.getElementById("trismultipleproduct1").style.display = "";
		document.getElementById("trismultipleproduct2").style.display = "";
		document.getElementById("trismultipleproduct3").style.display = "";
		document.getElementById("trismultipleproduct4").style.display = "";
		document.getElementById("trismultipleproduct5").style.display = "";
		document.getElementById("trismultipleproduct6").style.display = "";
		document.getElementById("trismultipleproduct7").style.display = "";
		document.getElementById("trismultipleproduct8").style.display = "";
		document.getElementById("trismultipleproduct9").style.display = "";	
		//Add Product For Multiple - Hide
		document.getElementById("trismultipleproduct10").style.display = "none";
		
		document.getElementById("trismultipleproduct11").style.display = "";
		
		document.getElementById("trismultipleproduct12").style.display = "";
		document.getElementById("trismultipleproduct13").style.display = "";
		document.getElementById("trismultipleproduct14").style.display = "";
		document.getElementById("trismultipleproduct15").style.display = "";
		document.getElementById("trismultipleproduct16").style.display = "";
		
		//Add New Product Button
		document.getElementById("Search").style.display = "";	
		//Edit Product Button
		//document.getElementById("Edit").style.display = "";	
		
		document.getElementById("doughtype").style.display = "";
		document.getElementById("doughtypemulti").style.display = "none";
		
		document.getElementById("Type3ForPacking").style.display = "";
		document.getElementById("Type3ForPackingMulti").style.display = "none";
	}
	else 
	{
		// if ismultipleproduct is Yes
		document.getElementById("trismultipleproduct1").style.display = "none";
		document.getElementById("trismultipleproduct2").style.display = "none";
		document.getElementById("trismultipleproduct3").style.display = "none";
		document.getElementById("trismultipleproduct4").style.display = "none";
		document.getElementById("trismultipleproduct5").style.display = "none";
		document.getElementById("trismultipleproduct6").style.display = "none";
		document.getElementById("trismultipleproduct7").style.display = "none";
		document.getElementById("trismultipleproduct8").style.display = "none";
		document.getElementById("trismultipleproduct9").style.display = "none";		
		//Add Product For Multiple - Show
		document.getElementById("trismultipleproduct10").style.display = "";	
		
		document.getElementById("trismultipleproduct11").style.display = "none";
		
		document.getElementById("trismultipleproduct12").style.display = "none";
		document.getElementById("trismultipleproduct13").style.display = "none";
		document.getElementById("trismultipleproduct14").style.display = "none";
		document.getElementById("trismultipleproduct15").style.display = "none";
		document.getElementById("trismultipleproduct16").style.display = "none";	
		//Add New Product Button not Multiple
		document.getElementById("Search").style.display = "none";
		//Edit Product Button
		//document.getElementById("Edit").style.display = "none";
		
		document.getElementById("HidShape").value=1
		document.getElementById("HidSlice").value=1
		document.getElementById("Hidfno").value=1
		document.getElementById("Hidvat").value=1
		document.getElementById("HidLastProd").value=1
		document.getElementById("HidType1").value=1
		document.getElementById("HidType2").value=1
		
		document.getElementById("HidProductionCategory").value=1
		
		document.getElementById("HidPreSize").value=1
		document.getElementById("HidPostSize").value=1
		document.getElementById("HidBakingOrder").value=1
		
		document.getElementById("doughtype").style.display = "none";
		document.getElementById("doughtypemulti").style.display = "";
		
		document.getElementById("Type3ForPacking").style.display = "none";
		document.getElementById("Type3ForPackingMulti").style.display = "";
		
	}
	if (document.getElementById("pno").value!="")
	{
		document.getElementById('ismultipleproduct').disabled=true;
	}

}

function onClick_AddGroup() {
    debugger;
    var e = document.getElementById("pvGroup");
    var s = document.getElementById("pvSelectedGroup");
    

    for (var i = 0; i < e.options.length; i++) {
        if (e.options[i].selected) {

            var sValue = e.options[i].value;
            var sText = e.options[i].text;
            createOption(s, sText, sValue)
        }
    }

  //  alert(document.getElementById("hdSelectedGroup").value);
   }

function createOption(ddl, text, value) {
    var opt = document.createElement('option');
    opt.value = value;
    opt.text = text;
    ddl.options.add(opt);

    if (document.getElementById("hdSelectedGroup").value == "") {

        document.getElementById("hdSelectedGroup").value = text + '|' + value;
    }
    else {

        document.getElementById("hdSelectedGroup").value = document.getElementById("hdSelectedGroup").value + ',' + text + '|' + value;
    }
}

function onClick_DeleteGroup() {
    debugger;
    if ((event.keyCode == 8 || event.keyCode == 46)) {

       // var e = document.getElementById("pvGroup");
        var s = document.getElementById("pvSelectedGroup");
        document.getElementById("hdSelectedGroup").value = "";
        var len = s.options.length;

        for (var i = 0; i < len ; i++) {
            if (s.options[i].selected) {

                // var sValue = s.options[i].value;
                //  var sText = s.options[i].text;
                s.options[i] = null;
                len--;
                i--;
            }
            else {
                var sValue = s.options[i].value;
                var sText = s.options[i].text;

                if (document.getElementById("hdSelectedGroup").value == "") {

                    document.getElementById("hdSelectedGroup").value = sText + '|' + sValue;
                }
                else {

                    document.getElementById("hdSelectedGroup").value = document.getElementById("hdSelectedGroup").value + ',' + sText + '|' + sValue;
                }
            }
        }
    }
  //  alert(document.getElementById("hdSelectedGroup").value);
}



//-->
</Script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="changeismultipleproduct()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<form method="POST" action="product_finish.asp" onSubmit="return FrontPage_Form1_Validator(this)" name="FrontPage_Form1">
<input type="hidden" name ="pno" value="<%=vpno%>" id="pno">

<input type="hidden" name ="HidShape" id="HidShape" value="">
<input type="hidden" name ="HidSlice" id="HidSlice" value="">
<input type="hidden" name ="Hidfno" id="Hidfno" value="">
<input type="hidden" name ="Hidvat" id="Hidvat" value="">
<input type="hidden" name ="HidLastProd" id="HidLastProd" value="">
<input type="hidden" name ="HidType1" id="HidType1" value="">
<input type="hidden" name ="HidType2" id="HidType2" value="">
<input type="hidden" name ="HidProductionCategory" id="HidProductionCategory" value="">
<input type="hidden" name ="HidPreSize" id="HidPreSize" value="">
<input type="hidden" name ="HidPostSize" id="HidPostSize" value="">
<input type="hidden" name ="HidBakingOrder" id="HidBakingOrder" value="">
<input type="hidden" name ="hdSelectedGroup" id="hdSelectedGroup" value="<%=strProductGroupList%>">
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
   
   
   <td width="100%"><b><font size="3"><%if vpno= "" then%>Create new<%else%>Edit<%end if%> Product<br>
      &nbsp;</font></b>
      <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td width="50%" valign="top">
            <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0">
			  <tr height="24">
                <td width="50%"><font color="#FF0000">*</font>Is Multiple Product </td>
                <td width="50%">
				<%
				if isarray(vRecArray1) then 
					strismultipleproduct=vRecArray1(32,0)
				end if
				
				%>
				
				<select name="ismultipleproduct" id="ismultipleproduct" onChange="changeismultipleproduct()">
				<option value="0" <% if (strismultipleproduct=0) then %> selected="selected" <% end if %>>No</option>
				<option value="1" <% if (strismultipleproduct=1) then %> selected="selected" <% end if %>>Yes</option>									
				</select>
				<% if (vpno<>"") then %> 
				<input type="hidden" name ="hidismultipleproduct" value="<%=strismultipleproduct%>">
				<% end if %>
				</td>
              </tr>
              
			  <%if vpno <> "" then%>
              <tr height="24">
                <td width="50%">&nbsp;&nbsp;Code</td>
                <td width="50%"><%=vrecarray1(17,0)%>  
                </td>
              </tr>
              <%end if%>
              <tr height="24">
                <td width="50%"><font color="#FF0000">*</font>Old Code</td>
                <td width="50%">
  				<input type="text" name="oldcode" size="20" style="font-family: Verdana; font-size: 8pt" maxlength="50" value="<%if isarray(vRecArray1) then response.write vRecArray1(0,0)%>" >
                </td>
              </tr>
              <tr height="24">
                <td width="50%"><font color="#FF0000">*</font>Name</td>
                <td width="50%">
  <input type="text" name="pname" size="50" style="font-family: Verdana; font-size: 8pt" maxlength="50" value="<%if isarray(vRecArray1) then response.write server.HTMLEncode(vRecArray1(1,0))%>" >
                </td>
              </tr>
              <tr height="24">
                <td width="50%"><font color="#FF0000">*</font>Dough Name</td>
                <td width="50%">
                <select size="1" name="doughtype" id="doughtype" style="font-family: Verdana; font-size: 8pt">
	          	<option value = "">Select</option><%
							if IsArray(vdoughtypearray) Then  	
								for i = 0 to ubound(vdoughtypearray,2)
								if (vdoughtypearray(0,i)<>stemultipleproductdough) then
								%>  				
                  <option value="<%=vdoughtypearray(0,i)%>" <%if isarray(vRecArray1) then%><%if vRecArray1(2,0) = vdoughtypearray(1,i) then response.write " Selected"%><%end if%>><%=vdoughtypearray(1,i)%></option>
			                  <%
							  end if
				               next                 
							End if%>   
                  </select>
				  <select size="1" name="doughtypemulti" id="doughtypemulti" style="font-family: Verdana; font-size: 8pt">
	          		<option value="<%=stemultipleproductdough%>" >Multiple Product Dough</option>
                  </select>
				  </td>
              </tr>
			   <tr height="24" id="trismultipleproduct11">
                <td width="50%"><font color="#FF0000">*</font>Production category</td>
                <td width="50%">
                <select size="1" name="ProductionCategory" style="font-family: Verdana; font-size: 8pt">
	          	<option value = "">Select</option><%
							if IsArray(vProductionCategoryarray) Then  	
								for i = 0 to ubound(vProductionCategoryarray,2)%>  				
                  <option value="<%=vProductionCategoryarray(0,i)%>" <%if isarray(vRecArray1) then%><%if vRecArray1(24,0) = vProductionCategoryarray(1,i) then response.write " Selected"%><%end if%>><%=vProductionCategoryarray(1,i)%></option>
			                  <%
				               next                 
							End if%>   
                  </select></td>
              </tr>
              <tr bgcolor="#999999" height="22">
                <td width="50%">&nbsp;<b>Additives</b></td>
                <td width="50%" >&nbsp;</td>
              </tr>
              <tr>
                <td width="100%" colspan="2" bgcolor="#E1E1E1">
                  <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0">
                    <tr height="20">
                      <td width="50%">&nbsp;<b>Name</b></td>
                      <td width="50%">&nbsp;<b>Qty (grams)</b></td>
                    </tr>
                    <tr>
                      <td width="50%">
  <select size="1" name="Ing1" id="Ing1" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option><%
					if IsArray(vingarray) Then
						for i = 0 to ubound(vingarray,2)%>  				
							<option value="<%=vingarray(0,i)%>" <%if isarray(vRecArray2) then%><%if vRecArray2(1,0) = vingarray(2,i) then response.write " Selected"%><%end if%>><%=vingarray(2,i)%></option><%
            			next   
          			End if%>  
                  </select> <font color="#FF0000"></font></td>
                      <td width="50%">
  <input type="text" name="Qty1" id="Qty1" size="20" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray2) then%><%=vRecArray2(2,0)%><%end if%>">
  </td>
                    </tr>
                    <tr>
                      <td width="50%">
  <select size="1" name="Ing2" id="Ing2" style="font-family: Verdana; font-size: 8pt">
  					<option value="">Select</option>
  					<%if IsArray(vingarray) Then%>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>" <%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >0 then%><%if vRecArray2(1,1) = vingarray(2,i) then response.write " Selected"%><%end if%><%end if%>><%=vingarray(2,i)%></option>
                  <%next%>
                  <%End if%> 
                  </select>
                      </td>
                      <td width="50%">
  <input type="text" name="Qty2" id="Qty2" size="20" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >0 then%><%=vRecArray2(2,1)%><%end if%><%end if%>">
                      </td>
                    </tr>
                    <tr>
                      <td width="50%">
  <select size="1" name="Ing3" id="Ing3" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%if IsArray(vingarray) Then%>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>" <%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >1 then%><%if vRecArray2(1,2) = vingarray(2,i) then response.write " Selected"%><%end if%><%end if%>><%=vingarray(2,i)%></option>
                  <%next%>   
                  <%End if%>
                  </select>
                      </td>
                      <td width="50%">
  <input type="text" name="Qty3" id="Qty3" size="20" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >1 then%><%=vRecArray2(2,2)%><%end if%><%end if%>">
                      </td>
                    </tr>
                    <tr>
                      <td width="50%">
  <select size="1" name="Ing4" id="Ing4" style="font-family: Verdana; font-size: 8pt">
  					<option value="">Select</option>
  					<%if IsArray(vingarray) Then%>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>" <%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >2 then%><%if vRecArray2(1,3) = vingarray(2,i) then response.write " Selected"%><%end if%><%end if%>><%=vingarray(2,i)%></option>
                  <%next%> 
                  <%End if%>
                  </select>
                      </td>
                      <td width="50%">
  <input type="text" name="Qty4" size="20" id="Qty4" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >2 then%><%=vRecArray2(2,3)%><%end if%><%end if%>">
                      </td>
                    </tr>
                    <tr>
                      <td width="50%">
  <select size="1" name="Ing5" id="Ing5" style="font-family: Verdana; font-size: 8pt">
  					<option value="">Select</option>
  					<%if IsArray(vingarray) Then%>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>" <%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >3 then%><%if vRecArray2(1,4) = vingarray(2,i) then response.write " Selected"%><%end if%><%end if%>><%=vingarray(2,i)%></option>
                  <%next%> 
                  <%End if%>
                  </select>
                      </td>
                      <td width="50%">
  <input type="text" name="Qty5" size="20" id="Qty5" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >3 then%><%=vRecArray2(2,4)%><%end if%><%end if%>">
                      </td>
                    </tr>
                    <tr>
                      <td width="50%">&nbsp;
  
                      </td>
                      <td width="50%">&nbsp;
  
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td width="50%">&nbsp;</td>
                <td width="50%"></td>
              </tr>
              <tr id="trismultipleproduct12">
                <td width="50%" bgcolor="#E1E1E1"><b>Size</b></td>
                <td width="50%" bgcolor="#E1E1E1">&nbsp;</td>
              </tr>
              <tr id="trismultipleproduct13">
                <td width="50%" bgcolor="#E1E1E1"><font color="#FF0000">*</font>Pre Baking (g)<br>
                  <input type="text" name="PreSize" size="20" style="font-family: Verdana; font-size: 8pt" maxlength="10" value="<%if isarray(vRecArray1) then response.write Replace(vRecArray1(3,0),"g","")%>" >
                </td>
                <td width="50%" bgcolor="#E1E1E1"><font color="#FF0000">*</font>Post Baking (g)<br>
                  <input type="text" name="PostSize" size="20" style="font-family: Verdana; font-size: 8pt" maxlength="10" value="<%if isarray(vRecArray1) then response.write Replace(vRecArray1(4,0),"g","")%>" >
                </td>
              </tr>
              <tr id="trismultipleproduct14">
                <td width="50%" bgcolor="#E1E1E1">&nbsp;</td>
                <td width="50%" bgcolor="#E1E1E1">&nbsp;</td>
              </tr>
              <tr>
                <td width="50%">&nbsp;</td>
                <td width="50%"></td>
              </tr>
			  <tr id="trismultipleproduct1">
                <td width="50%">Consolidation type for dividing</td>
                <td width="50%">
                <select size="1" name="ConsolidationType" style="font-family: Verdana; font-size: 8pt">
	          	<option value = "">None</option><%
							if IsArray(vConsolidationTypearray) Then  	
								for i = 0 to ubound(vConsolidationTypearray,2)%>  				
                  <option value="<%=vConsolidationTypearray(0,i)%>" <%if isarray(vRecArray1) then%><%if vRecArray1(25,0) = vConsolidationTypearray(1,i) then response.write " Selected"%><%end if%>><%=vConsolidationTypearray(1,i)%></option>
			                  <%
				               next                 
							End if%>   
                  </select></td>
              </tr>
              <tr id="trismultipleproduct2">
                <td width="50%"><font color="#FF0000">*</font>Shape</td>
                <td width="50%">
                <input type="text" name="Shape" size="20" style="font-family: Verdana; font-size: 8pt" maxlength="50" value="<%if isarray(vRecArray1) then response.write vRecArray1(5,0)%>" >
                </td>
              </tr>
              <tr id="trismultipleproduct3">
                <td width="50%"><font color="#FF0000">*</font>Is it sliced</td>
                <td width="50%">
                <select size="1" name="Slice" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>                
                    <option value="No" <%if isarray(vRecArray1) then%><%if vRecArray1(6,0) = "No" then response.write " Selected"%><%end if%>>No</option>
                    <option value="Thick Regular" <%if isarray(vRecArray1) then%><%if vRecArray1(6,0) = "Thick Regular" then response.write " Selected"%><%end if%>>Thick Regular</option>
                    <option value="Medium Regular" <%if isarray(vRecArray1) then%><%if vRecArray1(6,0) = "Medium Regular" then response.write " Selected"%><%end if%>>Medium Regular</option>
                    <option value="Thick Lateral" <%if isarray(vRecArray1) then%><%if vRecArray1(6,0) = "Thick Lateral" then response.write " Selected"%><%end if%>>Thick Lateral</option>
					<option value="Medium Lateral" <%if isarray(vRecArray1) then%><%if vRecArray1(6,0) = "Medium Lateral" then response.write " Selected"%><%end if%>>Medium Lateral</option>
				<option value="Extra Thick" <%if isarray(vRecArray1) then%><%if vRecArray1(6,0) = "Extra Thick" then response.write " Selected"%><%end if%>>Extra Thick</option>				<option value="Super Thick" <%if isarray(vRecArray1) then%><%if vRecArray1(6,0) = "Super Thick" then response.write " Selected"%><%end if%>>Super Thick</option>                </select>
				</td>
              </tr>
              <tr id="trismultipleproduct4">
                <td width="50%"><font color="#FF0000">*</font>Where is it produced</td>
                <td width="50%">
                <select size="1" name="fno" style="font-family: Verdana; font-size: 8pt" id="fno">
					<option value="">Select</option>
					<%for i = 0 to ubound(vfacarray,2)%>  				
                  <%if vfacarray(0,i)<>"90" then%>
                  <option value="<%=vfacarray(0,i)%>" <%if isarray(vRecArray1) then%><%if vRecArray1(7,0) = vfacarray(1,i) then response.write " Selected"%><%end if%>><%=vfacarray(1,i)%></option>
                  <%end if%>
                  <%next%> 
                  </select></td>
              </tr>
              <tr id="trismultipleproduct5">
                <td width="50%"><font color="#FF0000">*</font>Vat</td>
                <td width="50%">
                <select size="1" name="vat" style="font-family: Verdana; font-size: 8pt">
									<option value="">Select</option>                
                    <option value="0" <%if isarray(vRecArray1) then%><%if vRecArray1(18,0) = "0" then response.write " Selected"%><%end if%>>0</option>
                    <option value="1" <%if isarray(vRecArray1) then%><%if vRecArray1(18,0) = "1" then response.write " Selected"%><%end if%>>1</option>
                    <option value="2" <%if isarray(vRecArray1) then%><%if vRecArray1(18,0) = "2" then response.write " Selected"%><%end if%>>2</option>
                  </select></td>
              </tr>
              
              
              <tr id="trismultipleproduct6">
                <td width="50%"><font color="#FF0000">*</font>48 hrs Product</td>
                <td width="50%">
                <select size="1" name="selLastProd" style="font-family: Verdana; font-size: 8pt">
									<option value="">Select</option>                
                    <option value="Y" <%if isarray(vRecArray1) then%><%if vRecArray1(20,0) = "Y" then response.write " Selected"%><%end if%>>Yes</option>
                    <option value="N" <%if isarray(vRecArray1) then%><%if vRecArray1(20,0) = "N" then response.write " Selected"%><%end if%>>No</option>                    
                  </select></td>
              </tr>
              
				  <%
				  if isarray(vRecArray1) then
					  MulProd = vRecArray1(21,0)
                  else
	                  MulProd = 1
                  end if
                  %>
              
              <tr id="trismultipleproduct7">
                <td width="50%"><font color="#FF0000">*</font>No of Products</td>
                <td width="50%">                
                  <input type="text" name="MulProd" size="20" style="font-family: Verdana; font-size: 8pt" maxlength="10" value="<%=MulProd%>"></td>
              </tr>              
              
             
               <tr id="trismultipleproduct8">
                <td width="50%"><font color="#FF0000">*</font>Type1</td>
                <td width="50%">
				   <select size="1" name="Type1" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vType1Array,2)%>  				
                              <option value="<%=vType1Array(0,i)%>" <%if isarray(vrecarray1) then%><%if vrecarray1(22,0) = vType1Array(1,i) then response.write " Selected"%><%end if%>><%=vType1Array(1,i)%></option>
                  	<%next%> 
                  </select>
			  </td>
              </tr>
				<tr id="trismultipleproduct9">
                <td width="50%"><font color="#FF0000">*</font>Type2</td>
                <td width="50%">
                   <select size="1" name="Type2" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vType2Array,2)%>  				
                              <option value="<%=vType2Array(0,i)%>" <%if isarray(vrecarray1) then%><%if vrecarray1(23,0) = vType2Array(1,i) then response.write " Selected"%><%end if%>><%=vType2Array(1,i)%></option>
                  	<%next%> 
                  </select>
				  </td>
              </tr>
			 
			  <tr>
                <td width="50%">Type3 For Packing</td>
                <td width="50%">
                   <select size="1" name="Type3ForPacking" id="Type3ForPacking" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vType3ForPackingArray,2)
						if (vType3ForPackingArray(0,i)<>9) then
						%>  				
                              <option value="<%=vType3ForPackingArray(0,i)%>" <%if isarray(vrecarray1) then%><%if vrecarray1(27,0) = vType3ForPackingArray(1,i) then response.write " Selected"%><%end if%>><%=vType3ForPackingArray(1,i)%></option>
                  	<%
						end if
					next
					%> 
                  </select>
				  
				  <select size="1" name="Type3ForPackingMulti" id="Type3ForPackingMulti" style="font-family: Verdana; font-size: 8pt">
	          		<option value="9" >Multiple Product</option>
                  </select>
				  
				  </td>
              </tr>
			  <%
			  if (strismultipleproduct="1") then			  
			  %>
			  <input type="hidden" name="trismultipleproduct16" id="trismultipleproduct16">
			  <input type="hidden" name ="Type4ForPacking" value="<%=vrecarray1(34,0)%>">
			  <tr height="25">
                <td width="50%">Type4 For Packing</td>
                <td width="50%"><%=vrecarray1(28,0)%></td>
              </tr>
			  <% else %>
			  <tr id="trismultipleproduct16" height="22">
                <td width="50%">Type4 For Packing</td>
                <td width="50%">
                   <select size="1" name="Type4ForPacking" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vType4ForPackingArray,2)
						if (vType4ForPackingArray(0,i)<>21 and vType4ForPackingArray(0,i)<>22 and vType4ForPackingArray(0,i)<>23 and vType4ForPackingArray(0,i)<>24 and vType4ForPackingArray(0,i)<>25) then
					%>  				
                              <option value="<%=vType4ForPackingArray(0,i)%>" <%if isarray(vrecarray1) then%><%if vrecarray1(28,0) = vType4ForPackingArray(1,i) then response.write " Selected"%><%end if%>><%=vType4ForPackingArray(1,i)%></option>
                  	<%
						end if
					next
					%> 
                  </select>
				  </td>
              </tr>			 
			  <% end if %>
			<!-- <tr>
                <td width="50%"><font color="#FF0000">*</font>Is Product visible to non Gails customers ?</td>
                <td width="50%">
                   <select size="1" name="ProductVisible" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<option value="1" <%if isarray(vrecarray1) then%><%if vrecarray1(26,0) = "1" then response.write " Selected"%><%end if%>>Yes</option>
					<option value="0" <%if isarray(vrecarray1) then%><%if vrecarray1(26,0) = "0" then response.write " Selected"%><%end if%>>No</option>
                  </select>
				  </td>
              </tr>-->
               <tr>
                <td width="50%" valign="top"><font color="#FF0000">*</font>Select customer group for online product availability ?</td>
                <td width="50%" valign="top">              
                
                   <select size="10"  name="pvGroup" id="pvGroup" style="font-family: Verdana; font-size: 8pt; width:110px;" multiple="multiple">
						<option value=" ">Select</option><%
						'If IsArray(arGroupWithEmailAutomateInfo) then
							For i = 0 to UBound(arGroup,2)%>
								<option value="<%=arGroup(0,i)%>" ><%=arGroup(1,i)%></option><%
							Next 						
						'End if%>
                    </select>
                          
                   <input type="button" value="Add" name="B1" onclick="onClick_AddGroup();" style="font-family: Verdana; font-size: 9pt; font-weight:bold;">
				  
				    <select size="10"  name="pvSelectedGroup" id="pvSelectedGroup" style="font-family: Verdana; font-size: 8pt; width:110px;" onkeydown="onClick_DeleteGroup();" >
				   <% For i = 0 to UBound(vRecArray5,2)%>
								<option value="<%=vRecArray5(1,i)%>" ><%=vRecArray5(0,i)%></option>
								
				  <%
				 
				  Next %>
				    </select>
				  </td>
              </tr>
			  <tr>
			  	<td colspan="2">
				<%
				if isarray(vRecArray) then
				%>
				<br>
				<table border="0" bgcolor="#999999" width="500" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
				<tr bgcolor="#CCCCCC">
				<td width="100"><b>Product Code</b></td>
				<td><b>Product Name</b></td>
				<td width="50"><b>Qty</b></td>
				</tr>
				<%
				For i = 0 to UBound(vRecArray,2)
					if (i mod 2 =0) then
						strBgColour="#FFFFFF"
					else
						strBgColour="#F3F3F3"
					end if
				%>
				<tr bgcolor="<%=strBgColour%>">
				<td><%=vRecArray(0,i)%></b></td>
				<td><%=vRecArray(1,i)%></td>
				<td>
				<input type="text" name="qty<%=vRecArray(0,i)%>" size="4" value="<%=vRecArray(2,i)%>" maxlength="3" style="font-family: Verdana; font-size: 8pt">
								
				</td>
				</tr>
				<%
				next
				%>
				</table>
				<%
				end if
				%>
				</td>
			  </tr>
			  <input type="hidden" name ="hidAddProductForMultiple" id="hidAddProductForMultiple" value="">
			  <tr id="trismultipleproduct10">
			  
                <td colspan="2" height="50"><input type="button" value="Add Product For Multiple" onClick="FrontPage_Form1_ValidatorMultiple(this.form);" name="AddProductForMultiple" id="AddProductForMultiple" style="font-family: Verdana; font-size: 8pt; width:170px"></td>
              </tr>
            </table>
          </td>
          <td width="50%" valign="top">
<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0">
  <tr>
    <td width="67%" bgcolor="#E1E1E1" height="14"><b>&nbsp;Price (&pound;)</b></td>
    <td width="33%" bgcolor="#E1E1E1">&nbsp;
  
    </td>
  </tr>
  <tr>
    <td width="100%" colspan="2" bgcolor="#E1E1E1">
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0" height="45">
        <tr height="18">
          <td width="40">&nbsp;</td>
		  <td width="80" align="center"><b>A</b><font color="#FF0000">*</font></td>
		  <td width="80" align="center"><b>A1</b><font color="#FF0000">*</font></td>

		  <td width="80" align="center"><b>B</b><font color="#FF0000">*</font></td>
		  <td width="80" align="center"><b>C</b><font color="#FF0000">*</font></td>
		  <td width="80" align="center"><b>D</b><font color="#FF0000">*</font></td>

		  <td></td>
		</tr>
		
        <tr height="18">
          <td width="40">&nbsp;</td>
		  <td align="center" valign="top"><input type="text" <%if not isarray(vRecArray3) then%>onChange="PriceChanges()"<%end if%> name="price1" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;"  size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=formatnumber(vRecArray3 (2,0),2)%><%end if%>"></td>
		  
		  <td align="center" valign="top"><input type="text"  name="priceA1" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=formatnumber(vRecArray3 (2,4),2)%><%end if%>"></td>
		  
  <td align="center" valign="top"><input type="text" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" name="priceB" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=formatnumber(vRecArray3 (2,1),2)%><%end if%>"></td>

 <td align="center" valign="top"><input type="text" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" name="priceC" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=formatnumber(vRecArray3 (2,2),2)%><%end if%>"></td>
 
 <td align="center" valign="top"><input type="text" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" name="priceD" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=formatnumber(vRecArray3 (2,3),2)%><%end if%>"></td>
		
		   <td></td>
		</tr>
		<tr height="18"> 
			<td width="40">&nbsp;</td>
			  <td width="80" align="center"><b>B1</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>B2</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>B3</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>B4</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>B5</b><font color="#FF0000">*</font></td>
			  <td></td>
		</tr>
		<tr>  
			<td width="40">&nbsp;</td>
			<td align="center" valign="top"><input type="text" name="priceB1" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=formatnumber(vRecArray3 (2,5),2)%><%end if%>"></td>
		  	<td align="center" valign="top"><input type="text" name="priceB2" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=formatnumber(vRecArray3 (2,6),2)%><%end if%>"></td>
		  	<td align="center" valign="top"><input type="text" name="priceB3" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=formatnumber(vRecArray3 (2,7),2)%><%end if%>"></td>
		  	<td align="center" valign="top"><input type="text" name="priceB4" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=formatnumber(vRecArray3 (2,8),2)%><%end if%>"></td>
		  	<td align="center" valign="top"><input type="text" name="priceB5" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=formatnumber(vRecArray3 (2,9),2)%><%end if%>"></td>
			<td></td>
		</tr>
		<tr height="18"> 
			<td width="40">&nbsp;</td>
			  
			  <td width="80" align="center"><b>B6</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>B7</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>B8</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>B9</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>B10</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>B11</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>B12</b><font color="#FF0000">*</font></td>
			  <td></td>
		</tr>
		<tr>
		<td width="40">&nbsp;</td>
		
		<td align="center" valign="top"><input type="text" name="priceB6" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,10)%><%end if%>"></td>
		
		
		<td align="center" valign="top"><input type="text" name="priceB7" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,11)%><%end if%>"></td>
		
		<td align="center" valign="top"><input type="text" name="priceB8" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,12)%><%end if%>"></td>
		
		<td align="center" valign="top"><input type="text" name="priceB9" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,13)%><%end if%>"></td>

		<td align="center" valign="top"><input type="text" name="priceB10" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,15)%><%end if%>"></td>

		<td align="center" valign="top"><input type="text" name="priceB11" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,16)%><%end if%>"></td>

		<td align="center" valign="top"><input type="text" name="priceB12" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,17)%><%end if%>"></td>
		

		
		</tr>
		
		<tr height="18"> 
			<td width="40">&nbsp;</td>
			  
			  <td width="80" align="center"><b>B13</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>B14</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>B15</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>G</b></td>
			  <td></td>
		</tr>
		<tr>
		<td width="40">&nbsp;</td>
		

		<td align="center" valign="top"><input type="text" name="priceB13" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,18)%><%end if%>"></td>

		<td align="center" valign="top"><input type="text" name="priceB14" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,19)%><%end if%>"></td>

		<td align="center" valign="top"><input type="text" name="priceB15" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,20)%><%end if%>"></td>
		
		<td align="center" valign="top"><input type="text" name="priceG1" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,14)%><%end if%>"></td>
		<td colspan="4"></td>
		
		</tr>


		<tr height="18"> 
			<td width="40">&nbsp;</td>
			  <td width="80" align="center"><b>D1</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>D2</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>D3</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>D4</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>D5</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>H</b><font color="#FF0000">*</font></td>
			  <td width="80" align="center"><b>I</b><font color="#FF0000">*</font></td>
			  <td></td>
		</tr>
		<tr>
		<td width="40">&nbsp;</td>
		

		<td align="center" valign="top"><input type="text" name="priceD1" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,21)%><%end if%>"></td>

		<td align="center" valign="top"><input type="text" name="priceD2" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,22)%><%end if%>"></td>

		<td align="center" valign="top"><input type="text" name="priceD3" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,23)%><%end if%>"></td>
		
		<td align="center" valign="top"><input type="text" name="priceD4" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,24)%><%end if%>"></td>


		<td align="center" valign="top"><input type="text" name="priceD5" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=vRecArray3 (2,25)%><%end if%>"></td>
		<td align="center" valign="top"><input type="text" name="priceH" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=formatnumber(vRecArray3 (2,26),2)%><%end if%>"></td>
		<td align="center" valign="top"><input type="text" name="priceI" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;" size="3" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray3) then%><%=formatnumber(vRecArray3 (2,27),2)%><%end if%>"></td>
		<td></td>		
		</tr>
		
		
      </table>
    </td>
  </tr>
  <tr>
    <td width="67%">&nbsp;</td>
    <td width="33%">
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1"><b>Product Cost</b></td>
    <td width="33%" bgcolor="#E1E1E1">&nbsp;
  
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">Ingredient factor (%)</td>
    <td width="33%" bgcolor="#E1E1E1">
  <input type="text" name="IFactor" size="5" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray1) then response.write vRecArray1(9,0)%>" >
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">Labour cost</td>
    <td width="33%" bgcolor="#E1E1E1">
  <input type="text" name="LCost" size="5" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray1) then response.write vRecArray1(11,0)%>">
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">Packaging</td>
    <td width="33%" bgcolor="#E1E1E1">
  <input type="text" name="PCost" size="5" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray1) then response.write vRecArray1(12,0)%>">
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">Hygiene</td>
    <td width="33%" bgcolor="#E1E1E1">
  <input type="text" name="HCost" size="5" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray1) then response.write vRecArray1(13,0)%>">
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">Utilities</td>
    <td width="33%" bgcolor="#E1E1E1">
  <input type="text" name="UCost" size="5" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray1) then response.write vRecArray1(14,0)%>">
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">Rent, rates &amp; other</td>
    <td width="33%" bgcolor="#E1E1E1">
  <input type="text" name="R_OCost" size="5" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray1) then response.write vRecArray1(15,0)%>">
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">&nbsp;</td>
    <td width="33%" bgcolor="#E1E1E1">&nbsp;
  
    </td>
  </tr>
   <tr>
    <td width="67%">&nbsp;</td>
    <td width="33%">
    </td>
  </tr>
   <tr id="trismultipleproduct15">
    <td width="67%">Baking Order</td>
    <td width="33%">
    
     <select size="1" name="BakingOrder" style="font-family: Verdana; font-size: 8pt">
	<option value="0">Select</option>
	<%for i = 0 to ubound(vBakingOrder,2)%>  				
              <option value="<%=vBakingOrder(0,i)%>" <%if isarray(vrecarray1) then%><%if vrecarray1(29,0) = vBakingOrder(0,i) then response.write " Selected"%><%end if%>><%=vBakingOrder(1,i)%></option>
  	<%next%> 
  </select></td>
  </tr>
  
  <tr>
    <td width="67%">Tray Quantity</td>
    <td width="33%">
	<input type="text" name="TrayQty" size="5" style="font-family: Verdana; font-size: 8pt" maxlength="6" value="<%if isarray(vRecArray1) then response.write vRecArray1(31,0)%>">
    </td>
  </tr>

   <tr>
    <td width="67%"></td>
    <td width="33%">&nbsp;
    
     </td>
  </tr>

  <tr>
    <td width="67%"><font color="#FF0000">* Mandatory fields</font></td>
    <td width="33%">
	
    </td>
  </tr>
  <tr>
    <td width="33%">
    </td>
  </tr>
   <tr>
    <td width="67%"></td>
    <td width="33%">&nbsp;
    
     </td>
  </tr>

  <tr>
   			<input type="hidden" name="type" value="">
	        <%if vPno = "" then%>
  			<td width="50%">
  			<input type="button" value="Add product" id="Search" name="Search" onClick="FrontPage_Form1_Validator(this.form);" style="font-family: Verdana; font-size: 8pt">
		    <input type="hidden" id="Edit" value="">
			</td>   
		    <td width="50%"></td>
  			<%else%>
			<input type = "hidden" id="Search" value="">
  			<td width="50%" align = "right"><input type="button" value="Edit product" onClick="validate(this.form,'edit');" id="Edit" name="Edit" style="font-family: Verdana; font-size: 8pt">&nbsp;&nbsp;</td>
  			<td width="50%"><input type="button" value="Delete product" onClick="validate(this.form,'delete');" name="Delete" style="font-family: Verdana; font-size: 8pt"></td>
  			<%end if%>   

  </tr>
  
</table>
</td>
</tr>
</table>
 <div align="center"><input type="button" value=" Back " onClick="javascript:goto('product_find.asp')"><br></div>
</td>
</tr>
</table>
</form>
</center>
</div>
</body>
</html>