<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
dim cname,cno,vno,objBakery1,retcol,recarray
Server.ScriptTimeout=6000
vCustNo=request("CustNo")
Set Obj = CreateObject("Bakery.Customer")
Obj.SetEnvironment(strconnection)
Set ObjCustomer = obj.Display_CustomerDetail(vCustNo)
arSpecialProductPrice = ObjCustomer("SpecialPrice")
arCustomer = ObjCustomer("Customer")
arStandingOrder = ObjCustomer("StandingOrder")
set Obj = nothing
set ObjCustomer = nothing
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=StandingOrders.xls" 
	%>
	
	<table border="1" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
       <tr height="25">
          <td colspan="9"><b><font size="2">Standing Orders (Customer Code: <%=arCustomer(0,0)%>, Customer Name: <%=arCustomer(1,0)%>)</font></b></td>
        </tr>
        <tr height="20">
          <td><b>Product Code</b></td>
          <td width="350"><b>Product Name</b></td>
          <td align="center" width="50"><b>Mon</b></td>
          <td align="center" width="50"><b>Tue</b></td>
          <td align="center" width="50"><b>Wed</b></td>
          <td align="center" width="50"><b>Thu</b></td>
          <td align="center" width="50"><b>Fri</b></td>
          <td align="center" width="50"><b>Sat</b></td>
          <td align="center" width="50"><b>Sun</b></td>
        </tr>
		<%       
        if IsArray(arStandingOrder) Then
			i = 0					
			Do While i<=UBound(arStandingOrder,2)			
			%>
		<TR height="20">
			<td><%=arStandingOrder(0,i)%><b></b></td>
			<td><nobr><%=arStandingOrder(6,i)%><nobr></td>
			<%						
			prno = arStandingOrder(0,i) 
			
			Do While i<=UBound(arStandingOrder,2) and prno = arStandingOrder(0,i) 
				totalqty=0
				vWNo = arStandingOrder(1,i)							
				Do While i <= UBound(arStandingOrder,2)	and prno = arStandingOrder(0,i) and vWNo = arStandingOrder(1,i)					
					if Trim(arStandingOrder(5,i)) <> "" Then
						totalqty=totalqty+arStandingOrder(5,i)												
					End if 												
					i = i+1
					if i > UBound(arStandingOrder,2) then exit do							
				Loop
				%>
				<td align="center"><%=totalqty%></td>
				<%			
				if i > UBound(arStandingOrder,2) then exit do															
			Loop
			%> 						
		</tr>	
		<%
		if i > UBound(arStandingOrder,2) then exit do																			
	loop
End if%>   
</table>