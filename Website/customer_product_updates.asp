<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim Obj,ObjRecord,arWeek,arType,i,vResult,strday,strtype,vCustNo

Set Obj = CreateObject("bakery.customer")
Obj.SetEnvironment(strconnection)
Set ObjRecord = obj.Display_Week_types() 
arWeek = ObjRecord("Week")
arType = ObjRecord("Type")
Set ObjRecord = Nothing

if Request.Form("CustNo")<>"" then
	vCustNo = Request.Form("CustNo")
else
	vCustNo = Request.QueryString("CustNo")
end if
strday = Request.Form("day")
strtype = Request.Form("type")

if vCustNo<>"" and  strtype<>"" and strday <>"" then

'Response.Write vCustNo & "," & strday & "," &  strtype 

	vResult = obj.Delete_TempOrders(vCustNo,strday,strtype)
	%>
	<script>
	window.opener.location.href = 'customer_view.asp?custno=<%=vCustNo%>';
	window.close();
	</script>
	<%	

end if 
Set Obj = Nothing
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<SCRIPT LANGUAGE="Javascript">
<!--
function validate(){
	if(document.frmProUpdate.day.value==''){
	alert("Please select the day");
	document.frmProUpdate.day.focus();
	return;
	}
	if(document.frmProUpdate.type.value==''){
	alert("Please select the delivery type");
	document.frmProUpdate.type.focus();
	return;
	}
	if (confirm("Are you sure to proceed with the deletion ?"))
	document.frmProUpdate.submit();	
	else
	return false;	
}
	
//-->
</SCRIPT>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>

<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
<FORM  Method="Post" action="customer_product_updates.asp" name="frmProUpdate">	
  <tr>
    <td width="100%"><b><font size="3">Delete daily quantity for specific delivery<br>
      &nbsp;</font></b>
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><b> Days</b></td>
          <td><select size="1" name="day" style="font-family: Verdana; font-size: 8pt">
							<option value="">Select</option>
							<option value="1,2,3,4,5,6,7">Whole Week</option>
							<%          
              If IsArray(arWeek) then
								For i = 0 to UBound(arWeek,2)%>
									<option value="<%=arWeek(0,i)%>" <%if trim(Request.Form("day")) = trim(arWeek(0,i)) then Response.Write "Selected"%>><%=arWeek(1,i)%></option><%
								Next 						
							End if%>
            </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
            <td><b>Delivery Type</b></td>
          <td><select size="1" name="type" style="font-family: Verdana; font-size: 8pt">
          		<option value="">Select</option><%          
              If IsArray(arType) then
								For i = 0 to UBound(arType,2)%>
									<option value="<%=arType(0,i)%>" <%if trim(Request.Form("type")) = trim(arType(0,i)) then Response.Write "Selected"%>><%=arType(1,i)%></option><%
								Next 						
							End if%>
            </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
    		<tr>
				<td><input type="button" value="Delete" name="Update" style="font-family: Verdana; font-size: 8pt" onClick="validate();"></td>
				<td><input type="button" value="Close" name="close" style="font-family: Verdana; font-size: 8pt" onClick="window.close();"></td>
				
				<INPUT TYPE="hidden" NAME="CustNo"	VALUE="<%=vCustNo%>">				
				</tr>
				      </table>
		    </td>
  </tr>  

  
</FORM>  

	<tr>
		<td colspan="3" height="30"></td>						  
	</tr>	
</table>

	
</div>
</body>
</html><%
if IsArray(arWeek) Then Erase arWeek 
if IsArray(arType) Then Erase arType
%>