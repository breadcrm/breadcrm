<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayFacility()	
vFacarray =  detail("Facility") 
set detail = object.DisplayVec()	
vVecarray =  detail("Vec") 

set detail = Nothing
set object = Nothing


strDate = Day(date()) & "/" & Month(date()) & "/" & Year(date())
arSplit = Split(strDate,"/")
strDate = Right( "0" & arSplit(0),2) & "/" & Right( "0" & arSplit(1),2) & "/" & arSplit(2)	

%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3"> View daily reports<br>
      &nbsp;</font></b>
     
      <table border="0" width="656" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2" height="155">
        <tr>
          <td width="300"><b>Type of Report</b></td>
          <td width="200"><b>Delivery Date</b></td>
          <td height="13"><b>Action</b></td>
        </tr>
        <form method = "post" action="daily_report_order.asp" name="form1">
        <tr>
          <td bgcolor="#E1E1E1" width="300">Reports to
                    <select size="1" name="facility" style="font-family: Verdana; font-size: 8pt">
					<%for i = 0 to ubound(vFacarray,2)%>  				
                  <option value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
                  <%next%>              
                  </select></td>
          <td bgcolor="#E1E1E1" width="200">
          <input type="text" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=strDate%>" onFocus="blur();"><a href="javascript:onClick=calpopup('calendar.asp?Page=BODY&amp;Form=form1&amp;Element=deldate')"><img border="0" src="images/cal.gif" WIDTH="21" HEIGHT="19"></a></td>
          <td bgcolor="#E1E1E1" height="21">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
        <form method = "post" action="daily_report_invoice.asp" name="form2" target = "_blank">        
        <tr>
          <td width="300">Invoices to Customers</td>
          <td width="200">
          <input type="text" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=strDate%>" onFocus="blur();"><a href="javascript:onClick=calpopup('calendar.asp?Page=BODY&amp;Form=form2&amp;Element=deldate')"><img border="0" src="images/cal.gif" WIDTH="21" HEIGHT="19"></a></td>
          <td height="21">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>        
        <form method = "post" action="daily_report_van.asp" name="form3">        
        <tr>
          <td width="300" bgcolor="#E1E1E1">Reports to Drivers<select size="1" name="van" style="font-family: Verdana; font-size: 8pt">
					<%for i = 0 to ubound(vVecarray,2)%>  				
                  <option value="<%=vVecarray(0,i)%>"><%=vVecarray(1,i)%></option>
                  <%next%>              
                  </select></td>
          <td bgcolor="#E1E1E1" width="200">
          <input type="text" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=strDate%>" onFocus="blur();"><a href="javascript:onClick=calpopup('calendar.asp?Page=BODY&amp;Form=form3&amp;Element=deldate')"><img border="0" src="images/cal.gif" WIDTH="21" HEIGHT="19"></a></td>
          <td bgcolor="#E1E1E1" height="21">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>

        <form method = "post" action="daily_report_credit.asp" name="form4" target = "_blank">        
        <tr>
          <td width="300">Credit to Customers</td>
          <td width="200">
          <input type="text" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=strDate%>" onFocus="blur();"><a href="javascript:onClick=calpopup('calendar.asp?Page=BODY&amp;Form=form4&amp;Element=deldate')"><img border="0" src="images/cal.gif" WIDTH="21" HEIGHT="19"></a></td>
          <td height="21">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>    
        <form method = "post" action="daily_report_pack.asp" name="form5">        
        <tr>
          <td width="300" bgcolor="#E1E1E1">Packing Sheet Report
                    <select size="1" name="van" style="font-family: Verdana; font-size: 8pt">
					<%for i = 0 to ubound(vVecarray,2)%>  				
                  <option value="<%=vVecarray(0,i)%>"><%=vVecarray(1,i)%></option>
                  <%next%>              
                  </select></td>
          <td bgcolor="#E1E1E1" width="200">
          <input type="text" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=strDate%>" onFocus="blur();"><a href="javascript:onClick=calpopup('calendar.asp?Page=BODY&amp;Form=form5&amp;Element=deldate')"><img border="0" src="images/cal.gif" WIDTH="21" HEIGHT="19"></a></td>
          <td bgcolor="#E1E1E1" height="21">
          <input type="submit" value="View Report" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        </form>
        <tr>
         <td colspan="3">&nbsp;</td>
        </tr>
      </table>
      <table border="0" width="656" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2" height="155">
        <form name="form6" action="daily_all_reports.asp" method="post">
        <tr>
          <td bgcolor="#E1E1E1"> To
          <select size="1" name="facility" style="font-family: Verdana; font-size: 8pt">
			<%for i = 0 to ubound(vFacarray,2)%>  				
               <option value="<%=vFacarray(0,i)%>"><%=vFacarray(1,i)%></option>
            <%next%>              
          </select>
          </td>
          <td bgcolor="#E1E1E1">Drivers
          <select size="1" name="van" style="font-family: Verdana; font-size: 8pt">
			<%for i = 0 to ubound(vVecarray,2)%>  				
               <option value="<%=vVecarray(0,i)%>"><%=vVecarray(1,i)%></option>
            <%next%>              
          </select>
          </td>
          <!--<td bgcolor="#E1E1E1">Packing
            <select size="1" name="van" style="font-family: Verdana; font-size: 8pt">
			<%for i = 0 to ubound(vVecarray,2)%>  				
               <option value="<%=vVecarray(0,i)%>"><%=vVecarray(1,i)%></option>
            <%next%>              
            </select>
          </td>-->
          <td bgcolor="#E1E1E1" width="200">
            <input type="text" name="deldate" size="20" style="font-family: Verdana; font-size: 8pt" value="<%=strDate%>" onFocus="blur();">
              <a href="javascript:onClick=calpopup('calendar.asp?Page=BODY&amp;Form=form6&amp;Element=deldate')"><img border="0" src="images/cal.gif" WIDTH="21" HEIGHT="19"></a>
          </td>
          <td bgcolor="#E1E1E1" height="21">
            <input type="submit" value="View All Reports" name="B1" style="font-family: Verdana; font-size: 8pt">
          </td>
        </tr>
        </form>
      </table>
    </td>
  </tr>
</table>
</center>
</div>


</body>

</html>