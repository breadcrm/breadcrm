<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
Dim intI
Dim intJ
Dim intF
Dim intN
Dim intQ
Dim intP
Dim intD
Dim intRow
Dim delType
Dim delDate
Dim facilityNo
Dim arOrdSheetVansDetails
Dim arVansList
Dim arOrdSheetVansList
Dim intTotal
intN = 0
intJ = 0
intQ = 0
intP = 1
intTotal = 0
intF = 0
intD = 0

delDate= request.form("deldate")
facilityNo= request.form("facility")
delType=Request.Form("deltype")

if delDate= "" or facilityNo = "" or delType="" then response.redirect "daily_report.asp"

set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
set DisplayReport= object.Display_OrderingSheetByVans(facilityNo, delType, delDate)
arOrdSheetVansDetails = DisplayReport("OrdSheetVansDetails")
arVansList = DisplayReport("VansList")
arOrdSheetVansList = DisplayReport("OrdSheetVansList")
set DisplayReport= nothing
set object = nothing

If delType = "1" Then
	delType = "Morning"
ElseIf delType = "2" Then
	delType = "Noon"
ElseIf delType = "3" Then
	delType = "Evening"
End If

if IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
	intP = 1
	For intI = 0 To ubound(arVansList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetVansList, 2)
			if (arVansList(0, intI) = arOrdSheetVansList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
				End if
				if intN = 0 Then
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
			End if

			if intJ = ubound(arOrdSheetVansList, 2) And intRow + 5 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetVansList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetVansList, 2) Then
					intTotal = 0
					intRow = intRow + 1
				End if
				if intI <= ubound(arVansList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arVansList, 2) Then
						intP = intP + 1
					End if
				End if
				intQ = 0				
			End if
		Next
		intD = intD + 1
	Next
Else
	intP = 1
End if

intN = 1
intJ = 0
intQ = 0
intF = 0
intD = 0
intTotal = 0
%>
<html>
<head>
<title>ORDERING SHEET BY VANS</title>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<p>&nbsp;</p><%
if IsArray(arOrdSheetVansDetails) Then
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
<td>
<p align="center"><font size="3"><b>ORDERING SHEET BY VANS - <%if deltype="All" then%>(Morning, Noon, Evening)<%else%>(<%=deltype%>)<%end if%></b></font></p>
</td>
</tr>
<tr>
<td>
<br><br>
<b><%= arOrdSheetVansDetails(0, 0) %><br>
To the attention of: <%= arOrdSheetVansDetails(1, 0) %><br>
By fax number: <%= arOrdSheetVansDetails(2, 0) %></b><br><br>
Date of report: <%= Day(Now()) & "/" & Month(Now()) & "/" & Year(Now()) %><br>
To be produced and  delivered on <%= Day(FormatDateTime(delDate, 2)) & "/" & Month(FormatDateTime(delDate, 2)) & "/" & Year(FormatDateTime(delDate, 2)) %> <%if deltype="Morning" then%> before 05:00 AM<%end if%><br><br>
Number of pages for this report: <%= intP %><br><br>
</td>
</tr>
</table><%
End if
if IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
	For intI = 0 To ubound(arVansList, 2)
		if intI = 0 Then
			intRow = 9
		End if
%>
<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
	<tr>
	<td width="75%" height="20"><b>Order for Van <%= arVansList(0, intI) %></b></td>
	</tr>
</table><%
		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetVansList, 2)
			if (arVansList(0, intI) = arOrdSheetVansList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
%>
<table align="center" border="1" width="90%" cellspacing="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000" cellpadding="0"><%
				End if
				if intN = 0 Then
%>
	<tr>
	<td width="12%" height="20"><b>Product code</b></td>
	<td width="54%" height="20"><b>Product name</b></td>
	<td width="9%" height="20"><b>Quantity</b></td>
	</tr><%
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
	%>
	<tr>
	<td width="12%" height="20"><%= arOrdSheetVansList(2, intJ) %></td>
	<td width="54%" height="20"><%= arOrdSheetVansList(3, intJ) %></td>
	<td width="9%" height="20"><%= arOrdSheetVansList(4, intJ) %></td>
	</tr><%
				intTotal = intTotal + arOrdSheetVansList(4, intJ)
			End if

			if intJ = ubound(arOrdSheetVansList, 2) And intRow + 5 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetVansList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetVansList, 2) Then
%>
	<tr>
	<td width="66%" height="20" colspan="2" align="right"><b>Total&nbsp; </b></td>
	<td width="9%" height="20"><b><%= intTotal %>&nbsp;</b></td>
	</tr><%
					intTotal = 0
					intRow = intRow + 1
				End if
%>
</table><%
				if intI <= ubound(arVansList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arVansList, 2) Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
					End if
				End if
				intQ = 0
			End if
		Next
		intD = intD + 1
	Next
Else
%>
<table align="center" border="1" width="90%" cellspacing="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000" cellpadding="0">
	<tr>
	<td width="100%" height="20" colspan="3"><b>Sorry no records found</b></td>
	</tr>
</table><%
End if
%>
<p>&nbsp;</p>
</body>
</html><%
If IsArray(arOrdSheetVansDetails) Then erase arOrdSheetVansDetails
If IsArray(arVansList) Then erase arVansList
If IsArray(arOrdSheetVansList) Then erase arOrdSheetVansList
%>