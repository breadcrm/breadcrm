<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/top.inc" -->
<%
deldate= request.form("deldate")
if deldate= "" then response.redirect "LoginInternalPage.asp"

dim objBakery
dim recarray1, recarray2
dim ordno
dim retcol
Dim deldate
Dim intI
Dim intN
Dim intF
Dim intJ


dim tname, tno, curtno
Dim obj
Dim arCreditStatements
Dim arCreditStatementDetails
Dim vCreditDate

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; }
-->
</style>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
' Internal Productions => 11-Stanmore English, 12-Stanmore French, 99-Stanmore Goods, 15-Park Royal , 18-BMG, 22-Cake Department  
  set object = Server.CreateObject("bakery.daily")
%>
<%  
  'Stanmore English - 11 - All
  intN = 0
  facilityNo=11
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%" >
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="15%" height="20"><b>Product code</b></td>
          <td width="49%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="16%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="15%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="49%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="16%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="74%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="16%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>
<DIV CLASS="PAGEBREAK"></DIV>
<%  
  '12-Stanmore French - Morning
  intN = 0
  facilityNo=12
  tname = "Morning"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="15%" height="20"><b>Product code</b></td>
          <td width="49%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="16%"><b>Quantity Done</b></td>
        </tr><%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="15%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="49%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="16%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="74%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="16%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>
<DIV CLASS="PAGEBREAK"></DIV>
<%  
  '12-Stanmore French - Noon,Evening
  intN = 0
  facilityNo=12
  tname = "Noon','Evening"
  tname1 = "Noon, Evening"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname1%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="15%" height="20"><b>Product code</b></td>
          <td width="49%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="16%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="15%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="49%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="16%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="16%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>

<DIV CLASS="PAGEBREAK"></DIV>
<%  
  '12-Stanmore French - All
  intN = 0
  facilityNo=12
  tname = "Evening"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="15%" height="20"><b>Product code</b></td>
          <td width="49%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="16%"><b>Quantity Done</b></td>
        </tr><%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="15%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="49%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="11%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="16%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="16%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>

<DIV CLASS="PAGEBREAK"></DIV>
<%  
  ' 99-Stanmore Goods -All
  intN = 0
  facilityNo=12
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="15%" height="20"><b>Product code</b></td>
          <td width="49%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="16%"><b>Quantity Done</b></td>
        </tr><%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="15%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="49%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN)%></td>
		  <td width="16%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="16%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>
<DIV CLASS="PAGEBREAK"></DIV>
<%  
  '99-Stanmore Goods - All
  intN = 0
  facilityNo=99
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
         <tr>
          <td width="15%" height="20"><b>Product code</b></td>
          <td width="49%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="16%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="49%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="16%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="16%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>
<DIV CLASS="PAGEBREAK"></DIV>
<%  
  '15-Park Royal - All
  intN = 0
  facilityNo=15
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;<%=tname%>&nbsp;Direct Dough</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="15%" height="20"><b>Product code</b></td>
          <td width="49%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="16%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="15%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="49%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="16%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="16%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>
<DIV CLASS="PAGEBREAK"></DIV>

<%

'48 Hours Sour Dough Report Park Royal -15
if deldate <> "" then
	deldateNextDay = NextDayinDDMMYY(deldate)
end if
  
    dtype ="All"
      intN=0
      facility = 15
          set col1= object.DailySourDoughOrderSheet(deldateNextDay,facility,dtype)
		vRecArray = col1("SourDoughOrdersheet")
		if isarray(vrecarray) then
		  totrecs = ubound(vrecarray,2)+1
		else
		  totrecs = 0
		end if
		curpage=0
		totpages=0
		if totrecs <> 0 then
		  if clng(totrecs) mod 20  = 0 then
			totpages = clng(totrecs)/20
			curpage=1
		  else
			totpages = fix(clng(totrecs)/20) +  1
			curpage=1
		  end if
		end if

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
intF=1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
	End If
End If
if isarray(vRecArray) then 
intTotal=0
%>
<DIV CLASS="PAGEBREAK"></DIV>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"><%=Facilityname %> &nbsp;48 Hours Sour Dough Report - <%=dtype%><br></font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>Facility: </b><%=Facilityname %><br>
			<b>To the Attention of: </b><%=attn%><br>
			<b>By Fax Number: </b><%=fax%><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldateNextDay%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
            Number of pages for this report: <%=intF%>
            <%Response.Write(replace(space(60)," ","&nbsp;"))%>
			Page 1 of <%=intF%>
            <br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
  curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
<%
		if intI = 1 then
		%>
         <tr>
          <td width="15%" height="20"><b>Product code</b></td>
          <td width="49%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="16%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
        For intJ = 1 To  41 'PrintPgSize - changed by selva 12 Oct 2006
			if intN <= ubound(vRecArray, 2) Then
		if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr><%
		intJ=intJ+1
		end if%>
            <tr>
            <td width="15%" height="20"><%=vRecArray(0,intN)%></td>
            <td width="49%" height="20"><%=vRecArray(1,intN)%></td>
            <td width="10%" height="20"><%=vRecArray(2,intN)%></td>
            <td width="10%" height="20"><%=vRecArray(3,intN)%></td>
			<td width="15%">&nbsp;</td>
            </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
            <tr>
            <td height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
            <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
			<td width="15%">&nbsp;</td>
            </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2">Sorry no items found</td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%
end if
set col1= nothing
%>
<DIV CLASS="PAGEBREAK"></DIV>
<%  
  '18-BMG - All
  intN = 0
  facilityNo=18
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="15%" height="20"><b>Product code</b></td>
          <td width="49%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="16%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="15%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="49%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="16%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="16%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>
<DIV CLASS="PAGEBREAK"></DIV>
<%  
  '22-Cake Department - All
  intN = 0
  facilityNo=22
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
         <tr>
          <td width="15%" height="20"><b>Product code</b></td>
          <td width="49%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="16%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="15%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="49%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="16%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="16%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>
<DIV CLASS="PAGEBREAK"></DIV>

<%

'48 Hours Sour Dough Report 22-Cake Department - All
if deldate <> "" then
	deldateNextDay = NextDayinDDMMYY(deldate)
end if
  
    dtype ="All"
      intN=0
      facility = 22
          set col1= object.DailySourDoughOrderSheet(deldateNextDay,facility,dtype)
		vRecArray = col1("SourDoughOrdersheet")
		if isarray(vrecarray) then
		  totrecs = ubound(vrecarray,2)+1
		else
		  totrecs = 0
		end if
		curpage=0
		totpages=0
		if totrecs <> 0 then
		  if clng(totrecs) mod 20  = 0 then
			totpages = clng(totrecs)/20
			curpage=1
		  else
			totpages = fix(clng(totrecs)/20) +  1
			curpage=1
		  end if
		end if

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
intF=1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
	End If
End If
if isarray(vRecArray) then 
intTotal=0
%>
<DIV CLASS="PAGEBREAK"></DIV>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"><%=Facilityname %> &nbsp;48 Hours Product Report - <%=dtype%><br></font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>Facility: </b><%=Facilityname %><br>
			<b>To the Attention of: </b><%=attn%><br>
			<b>By Fax Number: </b><%=fax%><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldateNextDay%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
            Number of pages for this report: <%=intF%>
            <%Response.Write(replace(space(60)," ","&nbsp;"))%>
			Page 1 of <%=intF%>
            <br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
  curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
<%
		if intI = 1 then
		%>
        <tr>
          <td width="15%" height="20"><b>Product code</b></td>
          <td width="49%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="16%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
        For intJ = 1 To  41 'PrintPgSize - changed by selva 12 Oct 2006
			if intN <= ubound(vRecArray, 2) Then
		if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr><%
		intJ=intJ+1
		end if%>
            <tr>
            <td width="15%" height="20"><%=vRecArray(0,intN)%></td>
            <td width="49%" height="20"><%=vRecArray(1,intN)%></td>
            <td width="10%" height="20"><%=vRecArray(2,intN)%></td>
            <td width="10%" height="20"><%=vRecArray(3,intN)%></td>
			<td width="15%">&nbsp;</td>
            </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
            <tr>
            <td height="75%" colspan="3" align="right"><b>Total&nbsp; </b></td>
            <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
			<td width="15%">&nbsp;</td>
            </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK"></DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2">Sorry no items found</td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%
end if
set col1= nothing
%>
<%
set object = nothing
%>
<p>&nbsp;</p>
</body>
</html>