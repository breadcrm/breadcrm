<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
	fromdt=request("fromdt")
	todt=request("todt")
	vCustomer = Request("customer")
	vCustomerGroup=Request("CustomerGroup")
	vCustomerCode=Request("CustomerCode")
	
	if vCustomerCode<>"" then
        vCustomerGroup=""
        vCustomer=""
    Else
         vCustomerCode = ""   
    End if
	
	if vCustomerGroup<>"" then
		vCustomer=""
	end if
	if vCustomer<>"" then
		vCustomerGroup=""
	end if
	
	if fromdt ="" then
    	fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
	  	todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
	end if
	
	set objBakery = server.CreateObject("Bakery.reports")
	objBakery.SetEnvironment(strconnection)
	'set CustomerCol = objBakery.Display_CustomerList()
	'arCustomer = CustomerCol("Customer")

	set CustomerCol = nothing
	if isdate(fromdt) and isdate(todt)  then
		set retcol = objBakery.Display_Customer_By_ProductNew(fromdt,todt,vCustomer,vCustomerGroup,vCustomerCode)
		recarray = retcol("CustByProduct")
		'strWeekVanName = retcol("WeekVanName")
		'strWeekVanNo = retcol("WeekVanNo")
		'strWeekEndVanName = retcol("WeekEndVanName")
		'strWeekEndVanNo = retcol("WeekEndVanNo")
		'strCusName = retcol("CusName")
		set retcol = nothing
	end if
	set objBakery = nothing
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=sale_customer_by_product.xls" 
	%>
	
	<style>
		.text{
  			mso-number-format:"\@";/*force text*/
		}
	</style>

	 <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%" colspan="6"><b>Sales Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
       <tr height="24">
          <td width="12%"  bgcolor="#CCCCCC"><b>Product Code</b></td>
		  <td width="40%"  bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td width="8%" bgcolor="#CCCCCC" align="right"><b>No of Products</b></td>
          <td width="15%" bgcolor="#CCCCCC" align="right"><b>Total TurnOver <br>(Exl Credits)</b></td>
          <td width="8%" bgcolor="#CCCCCC" align="right"><b>Credit</b></td>
		  <td width="15%" bgcolor="#CCCCCC" align="right"><b>Total Net TurnOver</b></td>
        </tr>
			
				<%if isarray(recarray) then%>
				<% 				
				Totalturnover = 0.00
				vNumber = 0
				Credit=0.00
				TotalNetturnover=0.00
				
				%>
				<%for i=0 to UBound(recarray,2)%>
		  <tr height="22">		
		  <td width="12%" class="text"><b><%=recarray(5,i)%></b></td>
		  <td width="40%"><b><%if recarray(0,i)<> "" then Response.Write recarray(0,i) end if%></b>&nbsp;</td>
          <td width="8%" align="right"><b><%if recarray(1,i)<> "" then Response.Write formatnumber(recarray(1,i),0) else Response.Write "0" end if%></b></td> 
          <td width="15%" align="right"><b><%if recarray(2,i)<> "" then Response.Write formatnumber(recarray(2,i),2) else Response.Write "0.00" end if%></b></td>
          <td width="8%" align="right"><b><%if recarray(3,i)<> "" then Response.Write formatnumber(recarray(3,i),2) else Response.Write "0.00" end if%></b></td>
          <td width="15%" align="right"><b><%if recarray(4,i)<> "" then Response.Write formatnumber(recarray(4,i),2) else Response.Write "0.00" end if%></b></td>
                   
          </tr>
          <%
          if recarray(1,i)<> "" then 
				 		vNumber = vNumber + formatnumber(recarray(1,i),0)
		  End if

          if recarray(2,i)<> "" then 
				 		Totalturnover = Totalturnover + formatnumber(recarray(2,i),2)
		  End if
		  
		  if recarray(3,i)<> "" then 
				 		Credit = Credit + formatnumber(recarray(3,i),2)
		  End if
		  
		  if recarray(4,i)<> "" then 
				 		TotalNetturnover = TotalNetturnover + formatnumber(recarray(4,i),2)
		  End if

          %>
          <%next%>
          <tr height="22">
           <td align="right" colspan="2"><b>Grand Total</b>&nbsp;</td>
           <td align="right" ><b><%=formatnumber(vNumber,0)%></b></td> 
		   <td align="right" ><b><%=formatnumber(Totalturnover,2)%></b></td> 
		   <td align="right" ><b><%=formatnumber(Credit,2)%></b></td> 
           <td align="right" ><b><%=formatnumber(TotalNetturnover,2)%></b></td> 
                    
          </tr>
          <%
          else%>
         <td colspan="6" width="100%" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td><% 
          end if%>
        </tr>

      </table>
    </td>
  </tr>
</table>