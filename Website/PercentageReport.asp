<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title>Sales Report - By Customer By Product</title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--

function ExportToExcelFile(){
	fromdate=document.form.txtfrom.value
	todate=document.form.txtto.value
	customer=document.form.cus.value
	CustomerGroup=document.form.CustomerGroup.value
	document.location="ExportToExcelFile_PercentageReport.asp?customer=" + customer + "&CustomerGroup=" + CustomerGroup + "&fromdt=" + fromdate + "&todt=" +todate
}

function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function ValidateForm(){
	if (document.form.customer.value=="" && document.form.CustomerGroup.value=="")
	{
		alert ("Please select customer(s) or Customer Group.");
		document.form.customer.focus();
		return  false;
	}
	if (document.form.customer.value!="" && document.form.CustomerGroup.value!="")
	{
		alert ("Please select customer(s) or Customer Group only.");
		document.form.customer.focus();
		return  false;
	}
	return  true;
}
//-->
</SCRIPT>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;

<%
vSubGroup=Request.Form("SubGroup")
if vSubGroup="" or not isnumeric(vSubGroup) then
	vSubGroup=0
end if
set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set CustomerCol = objBakery.SubGroupList(strSearchSubCustomerGroup)
arCustomerSubGroup = CustomerCol("SubGroup")
set CustomerCol = nothing
set objBakery = nothing

fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

if isdate(fromdt) and isdate(todt)  then
	set objBakery = server.CreateObject("Bakery.reports")
	objBakery.SetEnvironment(strconnection)
	set retcol = objBakery.PercentageReport(vSubGroup,fromdt,todt)
	recarray = retcol("Product")
end if
set objBakery = nothing
%>

<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
 <tr>
    <td width="100%"><b><font size="3">Sales Report - Percentage Report<br>&nbsp;</font></b>
      <form method="post" action="PercentageReport.asp" name="form">
	  <input type="hidden" name="cus" value="<%=vCustomer%>">
	  <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
       
		<tr>
		  <td colspan="5">
		 	<table border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
			<tr>
				<td align="left"><b>Customer Sub Group:</b>&nbsp;</td>
			  	<td>
				<select size="1" name="SubGroup" style="font-family: Verdana; font-size: 9pt">
				<option value="">Select</option>
				<%if IsArray(arCustomerSubGroup) then%>
				<%for i = 0  to UBound(arCustomerSubGroup,2)%>
				<option value="<%=arCustomerSubGroup(0,i)%>" <%if arCustomerSubGroup(0,i)= cint(vSubGroup) then response.write " Selected"%>><%=arCustomerSubGroup(1,i)%></option>
				<%next%>
				<%end if%>
				</select>
			  </td>
			</tr>
			<tr>
			<td colspan="2" height="5"></td>
			</tr>
			</table>
		  </td>
		</tr>
		
        <tr>

           <td width="40"><b>From:</b></td>
						<td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom size="12" onFocus="blur();" name="txtfrom" value="<%=replace(fromdt,"from","")%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					<td width="50" align="right"><b>To:</b>&nbsp;</td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=replace(todt,"to","")%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td><td>
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
	   </table>
	  </form>
	   <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" bgcolor="#999999">
        <tr>
          <td width="100%" colspan="12" bgcolor="#FFFFFF" height="30"><b>Sales Percentage Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
        <tr height="24">
          <td width="12%"  bgcolor="#CACACA"><b>Product Code</b></td>
		  <td width="30%"  bgcolor="#CACACA"><b>Product Name</b></td>
          <td width="7%" bgcolor="#CACACA" align="right"><b>Bread</b></td>
          <td width="7%" bgcolor="#CACACA" align="right"><b>Sandwiches</b></td>
          <td width="7%" bgcolor="#CACACA" align="right"><b>Morning Goods</b></td>
		  <td width="7%" bgcolor="#CACACA" align="right"><b>Muffins</b></td>
		  <td width="7%" bgcolor="#CACACA" align="right"><b>Sweets</b></td>
		  <td width="7%" bgcolor="#CACACA" align="right"><b>Cakes</b></td>
		  <td width="7%" bgcolor="#CACACA" align="right"><b>Other Savory</b></td>
		  <td width="7%" bgcolor="#CACACA" align="right"><b>Others</b></td>
		  <td width="7%" bgcolor="#CACACA" align="right"><b>Total</b></td>
        </tr>
		<%if isarray(recarray) then%>
		<%
		BreadTot=0
		SandwichesTot=0
		MorningGoodsTot=0
		MuffinsTot=0
		SweetsTot=0
		CakesTot=0
		OtherSavoryTot=0
		OthersTot=0
		TotalTot=0

		for i=0 to UBound(recarray,2)
			if (i mod 2 =0) then
				strBgColour="#FFFFFF"
			else
				strBgColour="#E6E6E6"
			end if
			BreadTot=BreadTot+formatnumber(recarray(2,i),2)
			SandwichesTot=SandwichesTot+formatnumber(recarray(3,i),2)
			MorningGoodsTot=MorningGoodsTot+formatnumber(recarray(4,i),2)
			MuffinsTot=MuffinsTot+formatnumber(recarray(5,i),2)
			SweetsTot=SweetsTot+formatnumber(recarray(6,i),2)
			CakesTot=CakesTot+formatnumber(recarray(7,i),2)
			OtherSavoryTot=OtherSavoryTot+formatnumber(recarray(8,i),2)
			OthersTot=OthersTot+formatnumber(recarray(9,i),2)
			TotalTot=TotalTot+formatnumber(recarray(10,i),2)
		%>
		  <tr height="22" bgcolor="<%=strBgColour%>">
		  <td><%=recarray(0,i)%></td>
		  <td><%=recarray(1,i)%></td>
          <td align="right"><%=formatnumber(recarray(2,i),2)%></td> 
          <td align="right"><%=formatnumber(recarray(3,i),2)%></td>
		  <td align="right"><%=formatnumber(recarray(4,i),2)%></td> 
		  <td align="right"><%=formatnumber(recarray(5,i),2)%></td> 
		  <td align="right"><%=formatnumber(recarray(6,i),2)%></td> 
		  <td align="right"><%=formatnumber(recarray(7,i),2)%></td> 
		  <td align="right"><%=formatnumber(recarray(8,i),2)%></td>
		  <td align="right"><%=formatnumber(recarray(9,i),2)%></td> 
		  <td align="right"><%=formatnumber(recarray(10,i),2)%></td> 
		  </tr>
          <%next%>
		  <tr height="22" bgcolor="#CACACA">
		  <td colspan="2" align="right"><strong>Total</strong>&nbsp;</td>
		  <td align="right"><%=formatnumber(BreadTot,2)%></td> 
          <td align="right"><%=formatnumber(SandwichesTot,2)%></td>
		  <td align="right"><%=formatnumber(MorningGoodsTot,2)%></td> 
		  <td align="right"><%=formatnumber(MuffinsTot,2)%></td> 
		  <td align="right"><%=formatnumber(SweetsTot,2)%></td> 
		  <td align="right"><%=formatnumber(CakesTot,2)%></td> 
		  <td align="right"><%=formatnumber(OtherSavoryTot,2)%></td>
		  <td align="right"><%=formatnumber(OthersTot,2)%></td> 
		  <td align="right"><%=formatnumber(TotalTot,2)%></td> 
		  </tr>
          <%
          else%>
         <td colspan="12" width="100%" bgcolor="#FFFFFF" align="center" height="24"><b><font color="#FF0000">There are no records...</font></b></td><% 
          end if%>
        </tr>

      </table>
    </td>
  </tr>
</table>
<br><br><br>
</body>
<%if IsArray(recarray) then erase recarray%>
</html>