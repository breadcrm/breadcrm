<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Response.Buffer
deldate= request.form("deldate")
van= request.form("van")
arrayVan=split(van,",")
dtype=Request.Form("deltype")
if deldate= "" or van = "" or dtype = "" then response.redirect "daily_report.asp"


%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; height:1px }
-->
</style>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
<center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 10pt">
  <tr>
    <td width="100%">
      <center><b><font size="3">DELIVERY SHEET - <%if dtype="All" then%>(Morning, Noon, Evening)<%else%>(<%=dtype%>)<%end if%><br></font></b></center><br>
      <center><b><font size="3">(divided to Van and Customers)<br></font></b></center>
    
    </td>
  </tr>
</table>
<%
if isarray(arrayVan) then 
for hh = 0 to ubound(arrayVan)
set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
'Response.Write deldate & "," & van & "," & dtype
set col1= object.DailyDeliverySheet(deldate,trim(arrayVan(hh)),dtype)
vRecArray = col1("DeliverySheet")
vtotcustarray = col1("TotalCustomers")
if isarray(vtotcustarray) then
  totpages=ubound(vtotcustarray,2)+1
else
  totpages=0
end if
curpage=0
    
drivername = col1("Driver")
vanname=col1("Van")
set col1= nothing
set object = nothing
dim curvan, prevvan
dim curcno, prevcno

Dim mPage,mLine


mLine = 9	
	if isarray(vrecarray) then
		mPage = 1
		currec=0
		totqty=0
		curcno = vrecarray(0,0)
		curfac=0
		curpage=1
    for i = 0 to ubound(vRecArray,2)
			currec = clng(currec) + 1
	    if clng(currec) = 1 then
				mLine = mline + 3
				if mLine >= PrintPgSize+4 and i < ubound(vrecarray,2) Then 							
					mPage = mPage + 1						
					mLine = 0
				End if
			End if
			
	    if clng(curcno) = vrecarray(0,i) then
				mLine = mLine + 1
	    end if          
	    
	    if mLine >= PrintPgSize+4 and i < ubound(vrecarray,2) Then 
				mPage = mPage + 1						
				mLine = 0
	    End if
	          
	    if clng(curcno) <> vrecarray(0,i) then
				curcno = vrecarray(0,i)
			  i=clng(i)-1
	      mLine = mLine +3
				if mLine >= PrintPgSize+4-3 and i < ubound(vrecarray,2) Then 
					mPage = mPage + 1
					mLine = 0
				End if				
	        curpage = clng(curpage) + 1
			    currec = 0
			    totqty=0
			 end if
		  next		  
	Else
		mPage = 1	  
	end if
%>
  <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><br>
            <br>
            <b>Date of report:</b>  <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            <b>To be produced and delivered on</b> <%=deldate%> (<%=dtype%>)<br>
            <b>To the attention of: </b><%=vanname%><br>
            <b>Van <%=arrayVan(hh)%></b><br>
            </td>
        </tr>
        <tr>
          <td colspan="2"><br>
            Number of pages for this report: <%=mPage%>
          </td>
        </tr>
        </table>
</center>

</div>
	<%mLine = 9%>
	<div align="center">
	<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 10pt" align="center"><%    
		  if isarray(vrecarray) then
				mPage = 1
		    currec=0
		    totqty=0
		    curcno = vrecarray(0,0)
		    curfac=0
		    curpage=1
	        for i = 0 to ubound(vRecArray,2)
	          currec = clng(currec) + 1
	          if clng(currec) = 1 then%>
	            <tr>
	            <td colspan="5" width="100%">              
	              <b>Customer : <%=vrecarray(0,i)%> - <%=vrecarray(1,i)%></b>
	            </td>
	           
	            </tr>
	            <tr>
	            <td align="left" bgcolor="#CCCCCC" height="20" width="25%"><b>Facility</b></td>
	            <td align="center" bgcolor="#CCCCCC" height="20" width="15%"><b>Product code</b></td>
	            <td align="left" bgcolor="#CCCCCC" height="20" width="40%"><b>Product name</b></td>
	            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Quantity</b></td>
	            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Short in Delivery</b></td>
	            </tr><%
	            mLine = mline + 3
							if mLine >= PrintPgSize+4 and i < ubound(vrecarray,2) Then 							
								mPage = mPage + 1						
								mLine = 0%>		
								</table>						
								<br class="page" />						
								<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 10pt" align="center"><%
							End if
	           end if
	           if clng(curcno) = vrecarray(0,i) then%>
	            <tr>
	            <td align="left" width="25%"><%=left(vrecarray(3,i),20)%><%
	              totqty = clng(totqty) + vrecarray(6,i)%>
	             &nbsp;</td>
	            <td align="center" width="15%"><%=vrecarray(4,i)%>&nbsp;</td>
	            <td align="left" width="40%"><%=vrecarray(5,i)%>&nbsp;</td>
	            <td align="center" width="10%"><%=vrecarray(6,i)%>&nbsp;</td>
	            <td align="center" width="10%">&nbsp;</td>
	            </tr><%
	            mLine = mLine + 1
	          end if          
	          if mLine >= PrintPgSize+4 and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 10pt" align="center"><%
	          End if
	          
	          
			  if clng(curcno) <> vrecarray(0,i) then
			    curcno = vrecarray(0,i)
			    i=clng(i)-1%>
	            <tr>
	            <td align="center" height="20" colspan="3"  width="80%">
	              <p align="right"><b><font size="2" face="Verdana">Total&nbsp;&nbsp; </font></b>
	            </td>
	            <td align="center" height="20"  width="10%">
	              <p align="center"><font size="2" face="Verdana"  width="10%"><%=totqty%></font></p>
	            </td>
	            <td align="center" height="20"  width="10%">
	              <p align="center"><font size="2" face="Verdana"  width="10%">&nbsp;</font></p>
	            </td>
	            </tr>            
			    </table>
	            <br>
	            <br><%
	            mLine = mLine +3
							if mLine >= PrintPgSize+4-3 and i < ubound(vrecarray,2) Then 
								mPage = mPage + 1
								mLine = 0%>								
								<br class="page" /><%						
							End if
	            if i < ubound(vrecarray,2) then%>
								<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 10pt" align="center"><%														                
	            end if      
			    curpage = clng(curpage) + 1
			    currec = 0
			    totqty=0
			  end if
			  if (i mod 100=0) then
					Response.Flush()
			  end if
		    next
		    if clng(currec) > 0 then%>
		      <tr>
	            <td align="center" height="20" colspan="3"  width="80%">
	              <p align="right"><b><font size="2" face="Verdana">Total&nbsp;&nbsp; </font></b>
	            </td>
	            <td align="center" height="20"  width="10%">
	              <p align="center"><font size="2" face="Verdana"><%=totqty%></font></p>
	            </td>
	            <td align="center" height="20"  width="10%">
	              <p align="center"><font size="2" face="Verdana">&nbsp;</font></p>
	            </td>
	            </tr> <%
			  end if%>		    
			    <tr><%
	      else%>
	        <tr>
	        <td width="100%" colspan = "4" height="20">Sorry no items found</td>
	        </tr><%
		  end if%>
      </table>
<p><hr></p>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%
if (i mod 100=0) then
	Response.Flush()
end if
next
end if
%>	  
</div>
</body>
</html>