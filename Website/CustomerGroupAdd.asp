<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<!--#include file="Includes/Loader.asp"-->
<%
' load object
   Dim load
      Set load = new Loader
   ' calling initialize method
   load.initialize

' File binary data
   Dim fileData
      fileData = load.getFileData("file")
   ' File name
   Dim fileName
      fileName = LCase(load.getFileName("file"))
   ' File path
   Dim filePath
      filePath = load.getFilePath("file")
   ' File path complete
   Dim filePathComplete
      filePathComplete = load.getFilePathComplete("file")
   ' File size
   Dim fileSize
      fileSize = load.getFileSize("file")
   ' File size translated
   Dim fileSizeTranslated
      fileSizeTranslated = load.getFileSizeTranslated("file")
   ' Content Type
   Dim contentType
      contentType = load.getContentType("file")
stop
Dim sAction, nEmailCount
sAction = load.getValue("Action")
nEmailCount = load.getValue("EmailCount")
sEmailValues = load.getValue("EmailValues")


if sAction = "" Then
    nEmailCount = 1
End If

strEmail = sEmailValues
    if  (strEmail <> "" ) Then
    arrEmail = Split(strEmail,",")
    End if

strGname = replace(load.getValue("gname"),"'","''")
strEmail = replace(load.getValue("email1"),"'","''")
strElectronicInvoice = replace(load.getValue("ElectronicInvoice"),"'","''")
strDailyAutomatedGroupInvoice = replace(load.getValue("DailyAutomatedGroupInvoice"),"'","''")
strDailyAutomatedGroupCredit = replace(load.getValue("DailyAutomatedGroupCredit"),"'","''")

strNotes = replace(load.getValue("Notes"),"'","''")
straddress1 = replace(load.getValue("address1"),"'","''")
straddress2 = replace(load.getValue("address2"),"'","''")
straddress3 = replace(load.getValue("address3"),"'","''")
strtown = replace(load.getValue("town"),"'","''")
strpostcode = replace(load.getValue("postcode"),"'","''")
vPackingSheetType = load.getValue("selPackingSheetType")
strTelephone = replace(load.getValue("Telephone"),"'","''")
strLogo=fileName
strGNO=load.getValue("GNO")

strMinimumGroupOrder = load.getValue("txtMinimumGroupOrder")
if (strMinimumGroupOrder="") then
	'strMinimumGroupOrder="null"
	strMinimumGroupOrder=""
end if


	
strPricelessInvoice = replace(load.getValue("PricelessInvoice"),"'","''")
strPricelessInvoiceCreditOld = replace(load.getValue("PricelessInvoiceCreditOld"),"'","''")

if (strPricelessInvoice="" or (not isnumeric(strPricelessInvoice))) then
	strPricelessInvoice="0"
end if
set object = Server.CreateObject("bakery.customer")
object.SetEnvironment(strconnection)




Dim pathToFile,pathToFileOld
strLogo1=replace(trim(load.getValue("file1")),"'","''")
pathToFile = Server.mapPath("images/CustomerGroupLogo/") & "\" & fileName
pathToFileOld = Server.mapPath("images/CustomerGroupLogo/") & "\" & strLogo1

if strLogo = "" and strLogo1 <> ""  Then
    strLogo = strLogo1
End If


strgroupcustomercategoryid = replace(load.getValue("groupcustomercategoryid"),"'","''")
if (not isnumeric(strgroupcustomercategoryid)) then
	strgroupcustomercategoryid=0
end if

strgroupcustomercategoryidold = replace(load.getValue("groupcustomercategoryidold"),"'","''")
if (not isnumeric(strgroupcustomercategoryidold)) then
	strgroupcustomercategoryidold=0
end if


strCustomerGroupStatus=""


if (strGNO="") then
	strGNO="0"
end if 

if sAction = "Save" Then



if strGname<>"" then

if (not (IsNumeric(strMinimumGroupOrder))) then
	strMinimumGroupOrder="null"
	'strMinimumGroupOrder=""
end if
	set CustomerGroupSave= object.CustomerGroupSave(cint(strGNO),strGname,strElectronicInvoice,strNotes,strEmail,strDailyAutomatedGroupInvoice,strDailyAutomatedGroupCredit,straddress1,straddress2,straddress3,strtown,strpostcode, vPackingSheetType, strTelephone,strLogo,strPricelessInvoice,strgroupcustomercategoryid,strMinimumGroupOrder)
	if CustomerGroupSave("Sucess") <> "OK" then
		set CustomerGroupSave= nothing
		strCustomerGroupStatus="Error"
		'response.redirect "CustomerGroupAdd.asp?status=error"
		
		if (not (IsNumeric(strMinimumGroupOrder))) then
	       ' strMinimumGroupOrder="null"
	        strMinimumGroupOrder=""
        end if
		
	else
		if strGNO = "0" then
			LogAction "Customer Group Added", "Name: " & strGname , ""
		else
			LogAction "Customer Group Edited", "Name: " & strGname , ""
			if (strPricelessInvoiceCreditOld="Yes") then
				strPricelessInvoiceCreditOldValue="1"
			else
				strPricelessInvoiceCreditOldValue="0"
			end if 
			
			if ((strPricelessInvoice<>strPricelessInvoiceCreditOldValue) and isnumeric(strGNO)) then
				set vUpdateCustomerPricelessInvoiceCredit= object.UpdateCustomerPricelessInvoiceCredit(strGNO,strPricelessInvoice)
				set vUpdateCustomerPricelessInvoiceCredit= nothing	
			end if
			
			if ((strgroupcustomercategoryid<>strgroupcustomercategoryidold) and isnumeric(strGNO) and strgroupcustomercategoryid<>"0") then
				set vUpdateCustomercustomercategoryid= object.UpdateCustomerCategoryid(strGNO,strgroupcustomercategoryid)
				set vUpdateCustomercustomercategoryid= nothing	
			end if
			
		end if
		
		
		strCustomerGroupStatus="OK"
		
		'Save email group list
		If strCustomerGroupStatus="OK" Then
		    stop
		    Dim arrEmail2
		    Dim nGNO
		    nGNo = CustomerGroupSave("GNO")
		   ' set CustomerEmailGroupSave

            if  (sEmailValues <> "" ) Then
            arrEmail2 = Split(sEmailValues,",")
            End if

            If UBound(arrEmail2) > 0 Then
                set CustomerEmailGroupDelete= object.DeleteMultipleEmailByGNO(cint(nGNo),1)
            End If

            For i = 0 To UBound(arrEmail2)
		
			'for a group insert the group number and ser the IsGroup option 1
			set CustomerEmailGroupSave= object.CustomerGroupEmailList(cint(nGNo),0,arrEmail2(i),1)
			
			Next
            set CustomerEmailGroupSave= nothing
		
		End If
		
		
		if strLogo<>"" and strLogo1<>"" then
		Dim fso	
		Set fso = Server.CreateObject("Scripting.FileSystemObject")		
		On Error Resume Next		
			'Call fso.DeleteFile(pathToFileOld, True)	
		Set fso = Nothing
	    end if
    	
	    if strLogo="" then
		    strLogo=strLogo1
	    end if
	    
	    fileUploaded = load.saveToFile ("file", pathToFile)

		
	end if
end if
set object = nothing
End if

sgno=load.getValue("sgno")
set CustomerGroupSave= nothing


'List Customer Categories
set objCustomer = object.Display_CustomerCategoryList()
arCustomerCategory = objCustomer("CustomerCategoryList")
	
if sgno<>"" and strGname="" then
	set DisplayCustomerGroupDetail= object.CustomerGroupList("","",sgno)
	vCustomerGroupDetailArray = DisplayCustomerGroupDetail("CustomerGroupList")
	set DisplayCustomerGroupDetail= nothing
	set object = nothing
	strGNO = vCustomerGroupDetailArray(0,0)
	strGname = vCustomerGroupDetailArray(1,0)
	strEmail = vCustomerGroupDetailArray(2,0)
	sEmailValues = vCustomerGroupDetailArray(2,0)
	strElectronicInvoice = vCustomerGroupDetailArray(3,0)
	if strElectronicInvoice = "Yes" Then
	    strElectronicInvoice = 1
	End If 
	strDailyAutomatedGroupInvoice = vCustomerGroupDetailArray(4,0)
	if strDailyAutomatedGroupInvoice = "Yes" Then
	    strDailyAutomatedGroupInvoice = 1
	End If 
	strDailyAutomatedGroupCredit = vCustomerGroupDetailArray(5,0)
	if strDailyAutomatedGroupCredit = "Yes" Then
	    strDailyAutomatedGroupCredit = 1
	End If 
	strNotes = vCustomerGroupDetailArray(6,0)
	straddress1 = vCustomerGroupDetailArray(7,0)
	straddress2 = vCustomerGroupDetailArray(8,0)
	straddress3 = vCustomerGroupDetailArray(9,0)
	strtown = vCustomerGroupDetailArray(10,0)
	strpostcode = vCustomerGroupDetailArray(11,0)
	vPackingSheetType = vCustomerGroupDetailArray(12,0)
	strTelephone = vCustomerGroupDetailArray(13,0)
	strLogo1 =  vCustomerGroupDetailArray(14,0)
	strPricelessInvoice =  vCustomerGroupDetailArray(15,0)
	if strPricelessInvoice = "Yes" Then
	    strPricelessInvoice = 1
	End If 
	strgroupcustomercategoryid =  vCustomerGroupDetailArray(16,0)
	strMinimumGroupOrder =  vCustomerGroupDetailArray(18,0)
	set objBakery1 = server.CreateObject("Bakery.Customer")
	objBakery1.SetEnvironment(strconnection)
	set CustomerGroupCol = objBakery1.CustomersFromCustomerGroup(cint(strGNO))
	strCustomerGroup = CustomerGroupCol("Customers")
	
	

	CustomerGroupCol.close()
	set CustomerGroupCol=nothing
	objBakery1.close()
	set objBakery1=nothing
end if
Set load = Nothing

stop
 
 if sAction <> "AddEmail" Then
     if strGNO <> "" and strGNO <> "0" then
                
        sEmailValues = DisplayEmailEditMode (strGNO)
        sAction = "Edit"

     End If


    strEmail = sEmailValues
    if  (strEmail <> "" ) Then
    arrEmail = Split(strEmail,",")
    End if

    if strGNO <> "" then
    nEmailCount = UBound(arrEmail) + 1
    End If

End If



%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>

<script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{

    if (document.FrontPage_Form1.gname.value == "") {
        alert("Please enter a value for the \"Group Name\" field.");
        document.FrontPage_Form1.gname.focus();
        return (false);
    }
    if (document.FrontPage_Form1.gname.value == "") {
        alert("Please enter a value for the \"Group Name\" field.");
        document.FrontPage_Form1.gname.focus();
        return (false);
    }
    if ((document.FrontPage_Form1.DailyAutomatedGroupInvoice.checked == true) || (document.FrontPage_Form1.DailyAutomatedGroupCredit.checked == true)) {
        if (document.FrontPage_Form1.email1.value == "") {
            alert("Please enter a value for the \"Email\" field.");
            document.FrontPage_Form1.email1.focus();
            return (false);
        }
    }
    return (true);
}

function goto(url)
{
	document.location = url;
}



function onClick_AddEmail() {
    debugger;
   // alert('test');
    document.FrontPage_Form1.Action.value = "AddEmail";
   // alert(document.FrontPage_Form1.Action.value);
    document.FrontPage_Form1.EmailCount.value = parseInt(document.FrontPage_Form1.EmailCount.value) + 1;
    var count = document.FrontPage_Form1.EmailCount.value;

    var emailText;
    emailText = '';

    for (i = 1; i <= (count - 1); i++) {

        if (emailText != '') {
            emailText = emailText + ',' + document.getElementById("email" + i.toString()).value;

        }
        else {
            emailText = document.getElementById("email" + i.toString()).value;

        }

    }

    document.FrontPage_Form1.EmailValues.value = emailText;
    document.FrontPage_Form1.submit();
}

function onClick_Save() {
    debugger;
    if (FrontPage_Form1_Validator()) {
        document.FrontPage_Form1.Action.value = "Save";

        var count = document.FrontPage_Form1.EmailCount.value;

        var emailText;
        emailText = '';

        for (i = 1; i <= count ; i++) {

            if (emailText != '') {
                emailText = emailText + ',' + document.getElementById("email" + i.toString()).value;

            }
            else {
                emailText = document.getElementById("email" + i.toString()).value;

            }

        }

        document.FrontPage_Form1.EmailValues.value = emailText;
        
        
        document.FrontPage_Form1.submit();
    }
}


//--></script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<form method="POST" action="CustomerGroupAdd.asp" name="FrontPage_Form1"  enctype="multipart/form-data" >
<input type="hidden" value="<%=strGNO%>" name="GNO">
<input type="hidden" name="PricelessInvoiceCreditOld" value="<%=strPricelessInvoice%>">
<input type="hidden" name="groupcustomercategoryidold" value="<%=strgroupcustomercategoryid%>">
<input type="hidden" name="Action" value="<%=sAction%>">
<input type="hidden" name="EmailCount" value="<%=nEmailCount%>">
<input type="hidden" name="EmailValues" value="<%=sEmailValues%>">
<table border="0" align="center" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
		<td height="25" valign="bottom"><a href="CustomerGroup.asp"><strong><font color="#000099">List of Customer Groups</font></strong></a></td>
  </tr>
  <tr>
    <td width="100%" align="center">
	<%
	if strCustomerGroupStatus = "Error" then
		Response.Write "<font size=""2"" color=""#FF0000""><b>Customer Group Name already exists or a deleted group.</b></font><br><br>"
	elseif strCustomerGroupStatus = "OK" then
		Response.Redirect("CustomerGroup.asp")
	end if
	%>
	<%if strCustomerGroupStatus = "" or  strCustomerGroupStatus = "Error" then%>
      <table bgcolor="#CCCCCC" align="center" border="0" width="700" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="4">
         <tr height="30">
          <td colspan="2" align="center"><strong>Customer Group Details</strong></td>
        </tr>
		<%
		if (strGNO<>"" and strGNO<>"0")   then%>
		<tr bgcolor="#FFFFFF" height="25">
          <td align="right">Group No:</td>
          <td><%=strGNO%></td>
        </tr>
		<%
		end if
		%>
		<tr height="25" bgcolor="#FFFFFF">
          <td align="right" width="200">Group Name:</td>
          <td><input type="text" name="gname" value="<%=strGname%>" size="35" style="font-family: Verdana; font-size: 8pt" maxlength="50"></td>
        </tr>
		<tr height="25" bgcolor="#FFFFFF">
          <td align="right" valign="top">Email:</td>
          <td>
          <table id="tbEmail" cellpadding=0 cellspacing=0 >
          <tr>
          
          <%
          
          'strEmail = load.getValue("EmailValues")
          
         
          
           %>
          <td> <input type="text" name="email1" id="email1" size="35" value="<%If UBound(arrEmail) >= 0 Then %><%=arrEmail(0)%><%End IF %>" style="font-family: Verdana; font-size: 8pt" maxlength="100">&nbsp;</td>
          <td><input type="submit" value=" + " name="B1" onClick="onClick_AddEmail();" style="font-family: Verdana; font-size: 9pt; font-weight:bold;">
          
          </td>
          </tr>
          <%Call CreateMoreEmail() %>
          
          <%
          
       '   if strGNO <> "" then
          '  stop
         '  DisplayEmailEditMode(strGNO)
            
        '  End If
          
           %>
          </table>
          
         
          
          
          </td>
        </tr>
		
		<tr height="25" bgcolor="#FFFFFF">
          <td align="right">Address 1:</td>
          <td><input type="text" name="address1" size="35" value="<%=straddress1%>" style="font-family: Verdana; font-size: 8pt" maxlength="100"></td>
        </tr>
		
		<tr height="25" bgcolor="#FFFFFF">
          <td align="right">Address 2:</td>
          <td><input type="text" name="address2" size="35" value="<%=straddress2%>" style="font-family: Verdana; font-size: 8pt" maxlength="100"></td>
        </tr>
		
		<tr height="25" bgcolor="#FFFFFF">
          <td align="right">Address 3:</td>
          <td><input type="text" name="address3" size="35" value="<%=straddress3%>" style="font-family: Verdana; font-size: 8pt" maxlength="100"></td>
        </tr>
		
		<tr height="25" bgcolor="#FFFFFF">
          <td align="right">Town:</td>
          <td><input type="text" name="town" size="35" value="<%=strtown%>" style="font-family: Verdana; font-size: 8pt" maxlength="100"></td>
        </tr>
		
		<tr height="25" bgcolor="#FFFFFF">
          <td align="right">Postcode:</td>
          <td><input type="text" name="postcode" size="35" value="<%=strpostcode%>" style="font-family: Verdana; font-size: 8pt" maxlength="100"></td>
        </tr>
        
        <tr height="25" bgcolor="#FFFFFF">
          <td align="right">Telephone:</td>
          <td><input type="text" name="Telephone" size="35" value="<%=strTelephone%>" style="font-family: Verdana; font-size: 8pt" maxlength="100"></td>
        </tr>
		
		<tr height="25" bgcolor="#FFFFFF">
          <td align="right">CSV File Group Invoice:</td>
          <td><input type="checkbox" value="1" name="ElectronicInvoice" <%if (strElectronicInvoice="1") then%> checked="checked"<%end if%>></td>
        </tr>
		<tr height="25" bgcolor="#FFFFFF">
          <td align="right"><nobr>Daily Automated Group Invoice:</nobr></td>
          <td><input type="checkbox" value="1" name="DailyAutomatedGroupInvoice" <%if (strDailyAutomatedGroupInvoice="1") then%> checked="checked"<%end if%>></td>
        </tr>
		<tr height="25" bgcolor="#FFFFFF">
          <td align="right">Daily Automated Group Credit:</td>
          <td><input type="checkbox" value="1" name="DailyAutomatedGroupCredit" <%if (strDailyAutomatedGroupCredit="1") then%> checked="checked"<%end if%>></td>
        </tr>
		 
		<tr height="25" bgcolor="#FFFFFF">
          <td align="right">Priceless Invoice/Credit:</td>
          <td><input type="checkbox" value="1" name="PricelessInvoice" <%if (strPricelessInvoice="1") then%> checked="checked"<%end if%>></td>
        </tr>
		
        <tr height="25" bgcolor="#FFFFFF">
          <td align="right">Packing Sheet Type:</td>
          <td>
          <select size="1" name="selPackingSheetType" style="font-family: Verdana; font-size: 8pt">
  		    				
		    <option value="1" <%if Trim(vPackingSheetType) = "1" Then Response.Write "Selected"%>>Type 1</option>
		    <option value="2" <%if Trim(vPackingSheetType) = "2" Then Response.Write "Selected"%>>Type 2</option>					    
		  </select> 
          
          </td>
        </tr>
		 <tr height="25" bgcolor="#FFFFFF">
          <td align="right">Minimum Group Order Amount:</td>
          <td>&pound; <input type="text" name="txtMinimumGroupOrder" value="<%=strMinimumGroupOrder%>" Maxlength="5" size="7" onKeyPress="if ( (event.keyCode > 32 && event.keyCode < 48 ) || (event.keyCode > 57 && event.keyCode < 255)) event.returnValue = false;" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
		 <tr height="25" bgcolor="#FFFFFF">
          <td align="right">Customer Category:</td>
          <td>
          <select size="1" name="groupcustomercategoryid" style="font-family: Verdana; font-size: 8pt">
				<option value="0">---Select---</option>
				<%
				If IsArray(arCustomerCategory) then
					For i = 0 to UBound(arCustomerCategory,2)%>
						<option value="<%=arCustomerCategory(0,i)%>" <%if arCustomerCategory(0,i)=CInt(strgroupcustomercategoryid) Then Response.Write "Selected"%>><%=arCustomerCategory(1,i)%></option><%
					Next 						
				End if
				%>
          </select>          
          </td>
        </tr>
		<tr height="25" bgcolor="#FFFFFF">
          <td align="right" valign="top">Message:</td>
          <td><textarea name="Notes" cols="50" rows="5"style="font-family: Verdana; font-size: 8pt"><%=strNotes%></textarea></td>
        </tr>
		 <% if (strCustomerGroup<>"") then%>
		 <tr bgcolor="#FFFFFF" height="25" valign="top">
          <td align="right">Customer Code with Name:</td>
          <td><%=strCustomerGroup%></td>
		 </tr> 
		 <%end if%>
		 
		 <tr bgcolor="#FFFFFF" height="25" valign="top">
          <td valign="top" align="right">Company Logo&nbsp;</td>
          <td>
		 <%
		  if strLogo1<>"" then
		    %>
		    
		    <img src="images/CustomerGroupLogo/<%=strLogo1%>"><br>
		    <%
		    end if
		 %>
		 
		  <input type="file" name="file" size="26">
		  <input type="hidden" name="file1"  value="<%=strLogo1%>">
		  <br />
		  <font color="#FF0000">(This image size should be, width 225 pixels and height 75 pixels.<br>Image type must be GIF, JPG or JPEG)</font>
		  </td>
        </tr>
		 
		 
        <tr>
          <td></td>
          <td>
          <input type="submit" value=" Save " name="B2" style="font-family: Verdana; font-size: 8pt" onClick="onClick_Save();">&nbsp;
		  <input type="button" value=" Back " name="B3" onClick="history.back()" style="font-family: Verdana; font-size: 9pt">
		  </td>
        </tr>
      </table>
<%end if%>

    </td>
  </tr>
</table>
</form>
</body>
</html>
<%
obj.close()
set obj=nothing

Sub CreateMoreEmail()

Dim rowCount, strEmail, arrEmail

stop
rowCount = nEmailCount


strEmail = sEmailValues


if  (strEmail <> "" ) Then
arrEmail = Split(strEmail,",")
End if


For i = 2 To rowCount
 
 
If sAction = "Edit" Then
%>

<tr>
    <td colspan="2"> <input type="text" name="email<%=i%>" id="email<%=i%>" size="35"  <%if UBound(arrEmail) >= 1 Then %>    value="<%=arrEmail(i-1)%>"  <%else %>   value=""  <%End If %>   style="font-family: Verdana; font-size: 8pt" maxlength="100">&nbsp;</td>
         
 </tr>

<%

Else 
%>

<tr>
    <td colspan="2"> <input type="text" name="email<%=i%>" id="email<%=i%>" size="35" <%if i <> CInt(rowCount) Then %>     <%if UBound(arrEmail) >= 1 Then %>    value="<%=arrEmail(i-1)%>"  <%else %>   value=""  <%End If %>  <%else %> value=""  <%End If %>  style="font-family: Verdana; font-size: 8pt" maxlength="100">&nbsp;</td>
         
 </tr>

<%
End If

Next
End sub


Function DisplayEmailEditMode(gno)
stop
Dim strEmailList

strEmailList = ""
set object = Server.CreateObject("bakery.customer")
object.SetEnvironment(strconnection)
set listCustomerGroupEmailList = object.ListCustomerGroupEmailList(gno, 1)
arrlistCustomerGroupEmailList = listCustomerGroupEmailList("CustomerGroupEmailList")
stop
If IsArray(arrlistCustomerGroupEmailList) then
	For j = 0 to UBound(arrlistCustomerGroupEmailList,2)
		
		if (strEmailList = "") Then        
		    strEmailList =  arrlistCustomerGroupEmailList(3,j)
		Else     			
			strEmailList = strEmailList + "," + arrlistCustomerGroupEmailList(3,j)	
			
	    End If				
	Next 	
End If

DisplayEmailEditMode = strEmailList
End Function
%>