<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title>Sales Report - By Customer By Product</title>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function ValidateForm(){
	if (document.form.customer.value=="" && document.form.CustomerGroup.value=="")
	{
		alert ("Please select customer(s) or Customer Group.");
		document.form.customer.focus();
		return  false;
	}
	if (document.form.customer.value!="" && document.form.CustomerGroup.value!="")
	{
		alert ("Please select customer(s) or Customer Group only.");
		document.form.customer.focus();
		return  false;
	}
	return  true;
}

function CheckPercentage(x,z){
	
	var Total=0;
	var TotalwithOthers=0;
	vBread="Bread" + x
	vSandwiches="Sandwiches" + x
	vMorningGoods="MorningGoods" + x
	vMuffins="Muffins" + x
	vSweets="Sweets" + x
	vCakes="Cakes" + x
	vOtherSavory="OtherSavory" + x
	vOthers="Others" + x
	vOthers1="Others" + x
	
	vBread=document.getElementById(vBread).value
	vSandwiches=document.getElementById(vSandwiches).value
	vMorningGoods=document.getElementById(vMorningGoods).value
	vMuffins=document.getElementById(vMuffins).value
	vSweets=document.getElementById(vSweets).value
	vCakes=document.getElementById(vCakes).value
	vOtherSavory=document.getElementById(vOtherSavory).value
	vOthers=document.getElementById(vOthers).value
	
	if (IsNumeric(vBread))
		Total+=parseInt(vBread,10);
	if (IsNumeric(vSandwiches))
		Total+=parseInt(vSandwiches,10);
	if (IsNumeric(vMorningGoods))
		Total+=parseInt(vMorningGoods,10);
	if (IsNumeric(vMuffins))
		Total+=parseInt(vMuffins,10);
	if (IsNumeric(vSweets))
		Total+=parseInt(vSweets,10);
	if (IsNumeric(vCakes))
	{
		Total+=parseInt(vCakes,10);
	}
	if (IsNumeric(vOtherSavory))
		Total+=parseInt(vOtherSavory,10);
	if (IsNumeric(vOthers))
		TotalwithOthers=Total+ parseInt(vOthers,10);
	
	if (Total>100)
	{
		alert ("The total percentage value can't exceed over 100");
		z.style.backgroundColor="#FF0000";
		z.value=0;
		z.focus();
	}
	else
	{
		z.style.backgroundColor="#FFFFFF";
		document.getElementById(vOthers1).value=100-Total;
	}
	
	return  true;
}

function IsNumeric(strString)
   //  check for valid numeric strings	
   {
   var strValidChars = "0123456789.-";
   var strChar;
   var blnResult = true;

   if (strString.length == 0) return false;

   //  test strString consists of valid characters listed above
   for (i = 0; i < strString.length && blnResult == true; i++)
      {
      strChar = strString.charAt(i);
      if (strValidChars.indexOf(strChar) == -1)
         {
         blnResult = false;
         }
      }
   return blnResult;
   }
//-->
</SCRIPT>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
 <tr>
    <td width="100%">
	<b><font size="3">Percentage<br>&nbsp;</font></b>
	</td>
</tr>
</table>	
<%
dim objBakery
dim recarray
dim retcol

vSubGroup=Request.Form("SubGroup")
if vSubGroup="" or not isnumeric(vSubGroup) then
	vSubGroup=0
end if
set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set CustomerCol = objBakery.SubGroupList(strSearchSubCustomerGroup)
arCustomerSubGroup = CustomerCol("SubGroup")
set CustomerCol = nothing
set objBakery = nothing

set objBakery1 = server.CreateObject("Bakery.Customer")
objBakery1.SetEnvironment(strconnection)
set ProductsCol = objBakery1.Products_Percentage(vSubGroup)
arProducts = ProductsCol("Products")
set ProductsCol = nothing
if request("Save")<>"" then
 i=0
 for i=0 to UBound(arProducts,2)
	intID=0
	sID1="sID" & i
	intID=request(sID1)
	if (intID="") then
		intID=0
	end if	
	
	PNO="PNO" & i
	sPNO=request(PNO)
	
	Bread="Bread" & i
	sBread=request(Bread)
	if (sBread="" or not isnumeric(sBread)) then
		sBread=0
	end if
	
	Sandwiches="Sandwiches" & i
	sSandwiches=request(Sandwiches)
	if (sSandwiches="" or not isnumeric(sSandwiches)) then
		sSandwiches=0
	end if
	
	MorningGoods="MorningGoods" & i
	sMorningGoods=request(MorningGoods)
	if (sMorningGoods="" or not isnumeric(sMorningGoods)) then
		sMorningGoods=0
	end if
	
	Muffins="Muffins" & i
	sMuffins=request(Muffins)
	if (sMuffins="" or not isnumeric(sMuffins)) then
		sMuffins=0
	end if
	
	Sweets="Sweets" & i
	sSweets=request(Sweets)
	if (sSweets="" or not isnumeric(sSweets)) then
		sSweets=0
	end if
	
	Cakes="Cakes" & i
	sCakes=request(Cakes)
	if (sCakes="" or not isnumeric(sCakes)) then
		sCakes=0
	end if
	
	OtherSavory="OtherSavory" & i
	sOtherSavory=request(OtherSavory)
	if (sOtherSavory="" or not isnumeric(sOtherSavory)) then
		sOtherSavory=0
	end if
	OthersTotal=0
	OthersTotal=cdbl(sBread) + cdbl(sSandwiches) + cdbl(sMorningGoods) + cdbl(sMuffins) + cdbl(sSweets) + cdbl(sCakes) + cdbl(sOtherSavory) 
	if (OthersTotal<100) then
		OthersTotal=100.00-cdbl(OthersTotal)
	else
		OthersTotal=0
	end if
	Others="Others" & i
	sOthers=request(Others)
	if (sOthers="" or not isnumeric(sOthers)) then
		sOthers=0
	end if
	sOthers=OthersTotal
	total=cdbl(sBread)+cdbl(sSandwiches)+cdbl(sMorningGoods)+cdbl(sMuffins)+cdbl(sSweets)+cdbl(sCakes)+cdbl(sOtherSavory)+cdbl(sOthers)
	
	if (total<=100) then
		set SaveGroup=objBakery1.SavePercentage(intID,cint(vSubGroup),sPNO,cdbl(sBread),cdbl(sSandwiches),cdbl(sMorningGoods),cdbl(sMuffins),cdbl(sSweets),cdbl(sCakes),cdbl(sOtherSavory),cdbl(sOthers))
 	end if
 next
%>
<div align="center"><font color="#009900" size="2"><b>Successfully updated.</b></font></div><br>
<% 
end if
%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
 <tr>
    <td width="100%">
      <table align="center" border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
       <form method="post" action="PercentageEntry.asp" name="form">
	  <tr>
		  <td><b>Customer Sub Group</b></td>
		  <td>
			<select size="1" name="SubGroup" style="font-family: Verdana; font-size: 9pt" onChange="this.form.submit()">
            <option value="">Select</option>
            <%if IsArray(arCustomerSubGroup) then%>
            <%for i = 0  to UBound(arCustomerSubGroup,2)%>
            <option value="<%=arCustomerSubGroup(0,i)%>" <%if arCustomerSubGroup(0,i)= cint(vSubGroup) then response.write " Selected"%>><%=arCustomerSubGroup(1,i)%></option>
            <%next%>
            <%end if%>
          	</select>
		  </td>
	  </tr>
	  </form>
	  </table>
	  <br><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" bgcolor="#999999">
      <tr height="24">
          <td width="10%"  bgcolor="#CCCCCC"><b>Product Code</b></td>
		  <td width="30%"  bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td width="6%" bgcolor="#CCCCCC" align="right"><b>% of Bread</b></td>
          <td width="8%" bgcolor="#CCCCCC" align="right"><b>% of Sandwiches</b></td>
          <td width="11%" bgcolor="#CCCCCC" align="right"><b>% of<br>Morning Goods</b></td>
		  <td width="6%" bgcolor="#CCCCCC" align="right"><b>% of Muffins</b></td>
		  <td width="6%" bgcolor="#CCCCCC" align="right"><b>% of Sweets</b></td>
		  <td width="6%" bgcolor="#CCCCCC" align="right"><b>% of Cakes</b></td>
		  <td width="10%" bgcolor="#CCCCCC" align="right"><b>% of<br>Other Savory</b></td>
		  <td width="7%" bgcolor="#CCCCCC" align="right"><b>% of Others</b></td>
      </tr>
	  <%if not isarray(arProducts) then%>
	  <tr>
	  		<td colspan="10" bgcolor="#FFFFFF" align="center" height="30"><strong><font color="#FF0000">There are no records.</font></strong></td>
	  </tr>	
	  <%else%>			
	  <form method="post" action="PercentageEntry.asp" name="form">
	  <input type="hidden" name="SubGroup" value="<%=vSubGroup%>">
	  
	  <%
		  for i=0 to UBound(arProducts,2)
		  	set PercentageCol = objBakery1.PercentageView(vSubGroup,arProducts(0,i))
			arPercentage = PercentageCol("Percentage")
			set PercentageCol = nothing
		  	if isarray(arPercentage) then
				strID=arPercentage(0,0)
				strBread=arPercentage(3,0)
				strSandwiches=arPercentage(4,0)
				strMorningGoods=arPercentage(5,0)
				strMuffins=arPercentage(6,0)
				strSweets=arPercentage(7,0)
				strCakes=arPercentage(8,0)
				strOtherSavory=arPercentage(9,0)
				strOthers=arPercentage(10,0)
			else
				strID=0
			end if
		  	
			if (i mod 2 =0) then
				strBgColour="#FFFFFF"
			else
				strBgColour="#E6E6E6"
			end if
	  %>
	  <input type="hidden" name="PNO<%=i%>" value="<%=arProducts(0,i)%>">
	  <input type="hidden" name="sID<%=i%>" value="<%=strID%>">
	  <tr height="22" bgcolor="<%=strBgColour%>">
		  	<td><%=arProducts(0,i)%></td>
			<td><%=arProducts(1,i)%></td>
			<td align="center"><input type="text" name="Bread<%=i%>" onBlur="CheckPercentage(<%=i%>,this)" value="<%=strBread%>" size="3" maxlength="3" onKeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue = false;"></td>
			<td align="center"><input type="text" name="Sandwiches<%=i%>" onBlur="CheckPercentage(<%=i%>,this)" value="<%=strSandwiches%>" size="3" maxlength="3" onKeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue = false;"></td>
			<td align="center"><input type="text" name="MorningGoods<%=i%>" onBlur="CheckPercentage(<%=i%>,this)" value="<%=strMorningGoods%>" size="3" maxlength="3" onKeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue = false;"></td>
			<td align="center"><input type="text" name="Muffins<%=i%>" onBlur="CheckPercentage(<%=i%>,this)" value="<%=strMuffins%>" size="3" maxlength="3" onKeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue = false;"></td>
			<td align="center"><input type="text" name="Sweets<%=i%>" onBlur="CheckPercentage(<%=i%>,this)" value="<%=strSweets%>" size="3" maxlength="3" onKeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue = false;"></td>
			<td align="center"><input type="text" name="Cakes<%=i%>"  onBlur="CheckPercentage(<%=i%>,this)" value="<%=strCakes%>" size="3" maxlength="3" onKeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue = false;"></td>
			<td align="center"><input type="text" name="OtherSavory<%=i%>"  onBlur="CheckPercentage(<%=i%>,this)" value="<%=strOtherSavory%>" size="3" maxlength="3" onKeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue = false;"></td>
			<td align="center"><input type="text" name="Others<%=i%>" onBlur="CheckPercentage(<%=i%>,this)" value="<%=strOthers%>" size="3" maxlength="3" onKeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue = false;"></td>
	  </tr>
      <%next%>
	  <tr>
		 	<td colspan="10" bgcolor="#FFFFFF" align="center" height="40"><input type="submit" style="width:80px" value="Save" name="Save"></td>
	  </tr>
	  </form>
      <%end if%>
	  </table>
	  <br><br>
  
</td>
</tr>
</table>	  
</body>
<%
set objBakery1 = nothing
if IsArray(recarray) then erase recarray%>
<%if IsArray(arCustomer) then erase arCustomer%>
</html>