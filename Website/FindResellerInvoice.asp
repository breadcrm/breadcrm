<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
<center>
<%
dim objBakery
dim recarray
dim retcol
dim totpages
dim vSessionpage
dim i

dim cno
dim cname
dim ordno
dim fromdt
dim todt

cno = replace(Request.Form("txtcno"),"'","''")
cname = replace(Request.Form("txtcname"),"'","''")
ordno = replace(Request.Form("txtordno"),"'","''")
fromdt = replace(Request.Form("txtfrom"),"'","''")
todt = replace(Request.Form("txtto"),"'","''")

if fromdt ="" then
  fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

'Response.Write "chkweekvno = " & Request.Form("chkweekvno") & "<br>"
'Response.Write "chkcno = " & Request.Form("chkcno") & "<br>"
'Response.Write "chkgno = " & Request.Form("chkgno") & "<br>"
'Response.end

if not isnumeric(cno) then
 cno = 0
end if
if not isnumeric(ordno) then
 ordno = 0
end if

totpages = Request.Form("pagecount")
select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(totpages) then	
			session("Currentpage")  = session("Currentpage") + 1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage") - 1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select
vSessionpage = session("Currentpage")
set objBakery = server.CreateObject("Bakery.Reseller")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.FindResellerInvoice(cno,cname,ordno,replace(fromdt,"from",""),replace(todt,"to",""),vSessionpage,50)
totpages = retcol("pagecount")
recarray = retcol("Invoices")
if cno=0 then
  cno = ""
end if
if ordno=0 then
  ordno = ""
end if

%>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <form method="post" action="FindResellerInvoice.asp" name="form">
  <tr>
    <td ><b><font size="3">Find Reseller Invoice<br>
      &nbsp;</font></b>
      <table border="0" width="850" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td width="120"><b>Reseller ID</b></td>
          <td width="670"><input type="text" value="<%=cno%>" name="txtcno" size="20" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td><b>Reseller Name</b></td>
          <td><input type="text" value="<%=cname%>" name="txtcname" size="20" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td><b>Reseller Invoice Number</b></td>
          <td><input type="text" value="<%=ordno%>" name="txtordno" size="20" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
		
        <tr>

          <td colspan="2">
            <table border="0" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0" height="38">
              <tr>
                <td height="13"><b>From Date</b></td>
                <td height="13"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                <td height="13"><b>To Date</b></td>
              </tr>
              <tr>
                <td height="18" width="325">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom size="12" onFocus="blur();" name="txtfrom" value="<%=replace(fromdt,"from","")%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
				<td height="25"></td>
                
                <td height="18" width="325">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=replace(todt,"to","")%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
				  </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            
          </td>
        </tr>
        <tr>
          
          <td><input type="submit" value="Submit" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
      </table>
      </form>
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
			</SCRIPT>
			<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
			</SCRIPT>
   <p align="left">
   <br>
   <b><font size="2">Invoices between
    &lt;<%=fromdt%>&gt; and &lt;<%=todt%>&gt;</font></b>
    <div align="right"><%
      if isarray(recarray) then%>
        <font size="2"><b>Page <%=vsessionpage%> of <%=totpages%></b></font></td><%
      end if%>
    </div>
  <center>
      <table border="0" width="90%" align="center" style="font-family: Verdana; font-size: 8pt" bgcolor="#999999" cellspacing="1" cellpadding="2">
        <tr height="22">
          <td bgcolor="#CCCCCC" width="200"><b>Reseller Invoice Number</b></td>
          <td bgcolor="#CCCCCC" width="150" align="left"><b>Invoice Date</b></td>
          <td bgcolor="#CCCCCC" width="150" align="left"><b>Reseller ID</b></td>
          <td bgcolor="#CCCCCC" align="left"><b>Reseller Name</b></td>
          
          <td bgcolor="#CCCCCC" align="center" width="150"><b>Action</b></td>
        </tr><%
		dim strBg
        if isarray(recarray) then
          for i=0 to ubound(recarray,2)
		  if i mod 2=0 then
		  	strBg="#FFFFFF"
		  else
		  	strBg="#F0F0F0"
		  end if
		  %>
          <tr bgcolor="<%=strBg%>">
          <td><%=recarray(0,i)%></td>
          <td align="left"><%=displayBristishDate(recarray(1,i))%></td>
          <td align="left"><%=recarray(2,i)%></td>
          <td align="left"><%if trim(recarray(3,i)) = "" then Response.Write("no name") else Response.Write(recarray(3,i))%></td>
          
          <td align="center">
            <input type="button" onClick="NewWindow('ResellerInvoiceView.asp?RInvoiceNo=<%=recarray(0,i)%>&ResellerID=<%=recarray(2,i)%>&OrdDate=<%=displayBristishDate(recarray(1,i))%>','profile','700','300','yes','center');return false" onFocus="this.blur()" value="View Invoice" name="B1" style="font-family: Verdana; font-size: 8pt">
          </td>
          </tr><%
            next
          else%>
          <tr>
            <td height="25" colspan="6" bgcolor="#FFFFFF" align="center">
            <b>No Records Found...</b>
            </td>
          </tr><%
          end if%>
      </table>
    </center>
    </td>
  </tr>
</table>
</center>
</div>
<br>
<div align="center">
  <table width="720" border="0" cellspacing="1" cellpadding="2" align="center">
  <tr> 
  <% if clng(vSessionpage) <> 1 then %>
  <td>
    <form name="frmFirstPage" action="FindResellerInvoice.asp" method="post">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="hidden" name="Direction" value="First">
    <input type="submit" name="submit" value="First Page">
    <input type="hidden" name="txtfrom" value="<%=fromdt%>">
    <input type="hidden" name="txtto" value="<%=todt%>">
    <input type="hidden" value="<%=cno%>" name="txtcno">
    <input type="hidden" value="<%=cname%>" name="txtcname">
    <input type="hidden" value="<%=ordno%>" name="txtordno">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) < clng(totpages) then %>
  <td>
    <form name="frmNextPage" action="FindResellerInvoice.asp" method="post">
    <input type="hidden" name="Direction" value="Next">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Next Page">
    <input type="hidden" name="txtfrom" value="<%=fromdt%>">
    <input type="hidden" name="txtto" value="<%=todt%>">
    <input type="hidden" value="<%=cno%>" name="txtcno">
    <input type="hidden" value="<%=cname%>" name="txtcname">
    <input type="hidden" value="<%=ordno%>" name="txtordno">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) > 1 then %>
  <td>
    <form name="frmPreviousPage" action="FindResellerInvoice.asp" method="post">
    <input type="hidden" name="Direction" value="Previous">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Previous Page">
    <input type="hidden" name="txtfrom" value="<%=fromdt%>">
    <input type="hidden" name="txtto" value="<%=todt%>">
    <input type="hidden" value="<%=cno%>" name="txtcno">
    <input type="hidden" value="<%=cname%>" name="txtcname">
    <input type="hidden" value="<%=ordno%>" name="txtordno">
    </form>
  </td>
  <%end if%>
  <%if clng(vSessionPage) <> clng(totpages) and clng(totpages) > 0 then %>
  <td>
    <form name="frmLastPage" action="FindResellerInvoice.asp" method="post">
    <input type="hidden" name="Direction" value="Last">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Last Page">
    <input type="hidden" name="txtfrom" value="<%=fromdt%>">
    <input type="hidden" name="txtto" value="<%=todt%>">
    <input type="hidden" value="<%=cno%>" name="txtcno">
    <input type="hidden" value="<%=cname%>" name="txtcname">
    <input type="hidden" value="<%=ordno%>" name="txtordno">
    </form>
  </td>
  <% end if%>
  </tr>
  </table>
</div>
</body>
</html>
