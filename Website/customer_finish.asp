<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim vCustNo
Dim strRecordExistTag
Dim strErrTag, strResellerID, strDuplicateFileNameTag,strFileName

vCustNo = Request.QueryString("CustNo") 
strRecordExistTag = Request.QueryString("RecordExistTag") 
strErrTag = Request.QueryString("ErrTag") 
strDuplicateFileNameTag = cstr(Request.QueryString("DuplicateFileNameTag"))
strResellerID = Request.QueryString("ResellerID") 
strFileName = Request.QueryString("FileName")

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td><b><font size="3">&nbsp;</font></b>
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="675">
        <tr>
          <td ><b>Customer Name&nbsp;<%=Request.QueryString("Name")%></b></td>
          <td ><b></b></td>
        </tr>
        <tr>
          <td width="142">&nbsp;</td>
          <td width="127"></td>
        </tr>
        <tr>		<%
					if strRecordExistTag = "True" And strDuplicateFileNameTag = "" then%>  
						<td colspan="2"><b>Has been updated successfully</b></td>
					<%
					Else if strErrTag <> "True" And strDuplicateFileNameTag = "" Then
					%>
							<td colspan="2"><b>Has been created successfully</b></td>
						<%
						Else If strDuplicateFileNameTag = "" Then
							%>
								<td colspan="2"><b>This customer already exist, Please use the back button and modify the customer again</b></td>
							<%
							Else
							%>
								<td colspan="2"><b>Name Given for 'Electronic Invoice' already exist, Please use the back button and modify the 'Electronic Invoice' again</b></td>
							<%
							End If
							
						End if
						
					End if
					%>	
        </tr>
        <tr>
          <td colspan="2" width="273">&nbsp;</td>
        </tr>
        <% 
          ' if strErrTag <> "True" Then%>
        <tr style="display:none;">
          <td colspan="2" width="273"><b>CREATE CUSTOMER PROFILES</b></td>
        </tr>
        <tr style="display:none;">
          <td colspan="2" width="273"><table border="0" width="100%" cellspacing="0" cellpadding="0">
           <form>  
           
           <tr>
               <td width="25%" align="center">
               <input onClick="NewWindow('customer_products.asp?CustNo=<%=vCustNo%>','profile','850','300','yes','center');return false" onFocus="this.blur()" type="button" value="Edit Typical Order" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
             
                
                <td width="25%" align="center"></td>
             	
               	<td width="25%" align="center"><input onClick="NewWindow('customer_specials.asp?CustNo=<%=vCustNo%>','profile','700','300','yes','center');return false" onFocus="this.blur()" type="button" value="Special Prices" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
                
				 </tr>
				 <%
            'end if %>    
			</form>
            </table>
</td>
        </tr>
      </table>

    </td>
  </tr>
</table>


  </center>
</div>


</body>

</html>