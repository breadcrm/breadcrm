<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
deldate= request.form("deldate")
facility= request.form("facility")
dtype=Request.Form("deltype")
if deldate= "" then response.redirect "daily_report.asp"
'Response.Write deldate & "," & facility & "," & dtype
set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
set col1= object.DisplayDailyTempOrders(deldate,facility,dtype)
vRecArray = col1("OrderSheet")
monRec=0
if isarray(vrecarray) then
  totrecs = ubound(vrecarray,2)+1
else
  totrecs = 0
end if
if totrecs <> 0 then
  if clng(totrecs) mod printpgsize  = 0 then
    totpages = clng(totrecs)/printpgsize
    curpage=1
  else
    totpages = fix(clng(totrecs)/printpgsize) +  1
    curpage=1
  end if
end if
curpage=0

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
set object = nothing
dim curfno

Dim mPage,mLine

'Page Count
	if isarray(vrecarray) then
		mLine = 3
	  	mPage = 1
	    currec=0
	    totqty=0
	    curfno = vrecarray(0,0)
	    curpage=1
        for i = 0 to ubound(vRecArray,2)
          currec = clng(currec) + 1
          if clng(currec) = 1 then
            mLine = mline + 3
			if mLine >= PrintPgSize+3 and i < ubound(vrecarray,2) Then
				mPage = mPage + 1
				mLine = 0
			End if
          end if
          if curfno = vrecarray(0,i) then
		  	mLine = mLine + 1
          end if
          if mLine >= PrintPgSize+3 and i < ubound(vrecarray,2) Then
			mPage = mPage + 1
			mLine = 0
          End if
		  if curfno <> vrecarray(0,i) then
		    curfno = vrecarray(0,i)
		    i=clng(i)-1
            mLine = mLine +4
         	if mLine >= PrintPgSize+3-4 and i < ubound(vrecarray,2) Then
				mPage = mPage + 1
				mLine = 0
	        End if
            if i < ubound(vrecarray,2) then
            end if
		    curpage = curpage + 1
		    currec = 0
		    totqty=0
		  end if
	    next
      else
		mPage = 1
	  end if
		i = 0
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; }
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center><%
if isarray(vrecarray) then%>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <center><b><font size="3">Daily  Orders<br></font></b></center>
      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><br>
            <br>
            </td>
        </tr>
        <tr>
          <td colspan="2"><br>
            Number of pages for this report: <%=mPage%>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table>
</center>
</div>
<%mLine = 3%>
<div align="center">
<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt" align="center">
            <tr>
            <td align="left" bgcolor="#CCCCCC" height="20" width="25%"><b>Product code</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="25%"><b>Product Name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="25%"><b>Qty</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="25%"><b>Price</b></td>
            </tr>
			<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt" align="center">
<%
	  		mPage = 1
	    currec=0
	    totqty=0
	    totprice=0
	    curfno = vrecarray(0,0)
	    curpage=1
        for i = 0 to ubound(vRecArray,2)
          currec = clng(currec) + 1
		  monRec =  monRec + 1
		%>
			<tr>
            <td align="left" width="15%"><%=monRec%>&nbsp;</td>
            <td align="left" width="15%"><%=vrecarray(2,i)%>&nbsp;</td>
            <td width="45%"><%=vrecarray(3,i)%>&nbsp;</td><%
            totqty = clng(totqty) + vrecarray(4,i)
            totprice = clng(totprice) + vrecarray(5,i)%>
            <td align="center" width="25%"><%=vrecarray(4,i)%>&nbsp;</td>
            <td align="center" width="25%"><%=vrecarray(5,i)%>&nbsp;</td>
          </tr>
			<%
	    next%>
		    <tr>
		      <td colspan="2" width="60%">
		        <p align="right"><b><font size="2" face="Verdana">Total&nbsp;&nbsp; </font></b>
              </td>
              <td width="25%" align="center" >
                <p align="center"><font size="2" face="Verdana"><%=totqty%></font></p>
              </td>
              <td width="25%" align="center" >
                <p align="center" ><font size="2" face="Verdana"><%=totprice%></font></p>
              </td>

            </tr>
	    <tr>
		  <td colspan="2" width="60%">
		    <p align="right"><b><font size="2" face="Verdana">Total&nbsp;&nbsp; </font></b>
          </td>
          <td width="25%" align="center">
            <p align="center"><font size="2" face="Verdana"><%=totqty%></font></p>
          </td>
          <td width="25%" align="center">
            <p align="center"><font size="2" face="Verdana"><%=totprice%></font></p>
          </td>

          <td width="25%" align="center">Total Rec
            <p align="center"><font size="2" face="Verdana"><%=monRec%></font></p>
          </td>


        </tr>
		</table>
		<%
      else%>
        <tr>
        <td width="100%" colspan = "4" height="20">Sorry no items found</td>
        </tr><%
	  end if%>
      </table>
</div>
</body>
</html>