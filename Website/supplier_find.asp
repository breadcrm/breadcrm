<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->

<%

vPageSize = 999999

vSname = replace(Request.Form("sname"),"'","''")
vScode = replace(Request.Form("scode"),"'","''")
viname =  replace(Request.Form("iname"),"'","''")
 
vpagecount = Request.Form("pagecount") 

select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(vpagecount) then	
			session("Currentpage")  = session("Currentpage")+1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage")-1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select
vSessionpage = session("Currentpage")
vsessionpage = 0
if NOT ISNUMERIC(vScode) then
	vScode = 0
end if

Set obj = server.CreateObject("bakery.Supplier")
obj.SetEnvironment(strconnection)
if request.form("delete")="yes" then
	vSno=request.form("sno")
	delete = obj.DeleteSupplier(vSno)
end if
Set objUser = obj.DisplaySupplier(vSname,vScode, viname, vsessionpage, vPageSize)

arSuppliers =  objUser("Suppliers")
vpagecount =   objUser("Pagecount") 
vRecordCount = objUser("Recordcount")

Set objUser = Nothing
Set obj = Nothing
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script LANGUAGE="javascript" src="includes/script.js"></script>
<script Language="JavaScript">
<!--
function validate(frm){
	if(confirm("Would you like to delete " + frm.name.value + " ?")){
		frm.submit();		
	}
}
//-->
</Script>
<SCRIPT LANGUAGE="Javascript">
<!--
function SearchValidation(){
		if((CheckEmpty(document.frmForm.sname.value) == "true") && (CheckEmpty(document.frmForm.scode.value) == "true") && (CheckEmpty(document.frmForm.iname.value) == "true")){ 
			alert("Please enter a value");
			return false;
		}
		return true;
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Find / Edit Supplier<br>
      &nbsp;</font></b>
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" width="100%" bordercolor="#111111">
		<form method="post" action="supplier_find.asp" name="frmForm">      
        <tr>
          <td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          <td><b>Supplier name:</b></td>
          <td><b>Supplier code:</b></td>
          <td><b>Ingredient name:</b></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>
          <input type="text" name="sname" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td>
          <input type="text" name="scode" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td>
          <input type="text" name="iname" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td>
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt" onClick="return SearchValidation();"></td>
        </tr>
		</form>
      </table>

       <table border="0" bgcolor="#999999" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">

	<%if vSessionpage <> 0 then%>
  <tr bgcolor="#FFFFFF">
    <td align="right" colspan="5" width="664">		
		<%
		If isArray(arSuppliers) Then
			If (session("Currentpage") * vPageSize) > vRecordCount Then
				vMiddleVal  = vRecordCount
			Else
				vMiddleVal  = (session("Currentpage") * vPageSize)
			End if

			vFirstVal = (session("Currentpage") * vPageSize) - (vPageSize - 1)
			Response.Write "Suppliers " & vFirstVal & " To " & vMiddleVal & " of " & vRecordCount
		End if
		%>
    </td>
  </tr>
	  <%else%>
				<tr bgcolor="#FFFFFF">
					<td colspan="5" align = "right"><b>Total no of suppliers : <%=vRecordcount%></b></td>
				</tr>	
	<%end if%>    
      
        <tr bgcolor="#CCCCCC">
          <td width="101" bgcolor="#CCCCCC"><b>Supplier Code</b></td>
          <td width="516" bgcolor="#CCCCCC"><b>Supplier Name</b></td>
          <td colspan = "3" bgcolor="#CCCCCC" align="center"><b>Action</b></td>
        </tr>
<% 
	if IsArray(arSuppliers) Then
		For i = 0 To ubound(arSuppliers,2)
		if (i mod 2 =0) then
			strBgColour="#FFFFFF"
		else
			strBgColour="#F3F3F3"
		end if
%>         
        <tr bgcolor="<%=strBgColour%>">
          <td width="120"><%=arSuppliers(0,i)%></td>
          <td width="600"><%=arSuppliers(1,i)%></td>
		  <td align="center">
		  <table width="100%" cellpadding="2" border="0" cellspacing="0" align="center">
		  <tr>
          <form method="POST" action="supplier_view.asp" STYLE="margin: 0px; padding: 0px;">
         	<td align="center">
			<input type = "hidden" name="sno" value = "<%=arSuppliers(0,i)%>"><input type="submit" value="View Details" name="B1" style="font-family: Verdana; font-size: 8pt; width:100px">
          	</td>
		  </form>
          <%if session("UserType") <> "N" and strconnection<>"vdbArchiveConn" Then%>
		  <form method="POST" action="supplier_new.asp" STYLE="margin: 0px; padding: 0px;">
          <td align="center">
		  <input type = "hidden" name="sno" value = "<%=arSuppliers(0,i)%>"><input type="submit" value="Edit" name="B1" style="font-family: Verdana; font-size: 8pt; width:100px">
          </td>
		  </form>
          <form method="POST" STYLE="margin: 0px; padding: 0px;">
		  <%if trconnection<>"vdbArchiveConn" Then%>
		  <td align="center">
		  <input type = "hidden" name="name" value = "<%=arSuppliers(1,i)%>">
          <input type = "hidden" name="delete" value = "yes"><input type = "hidden" name="sno" value = "<%=arSuppliers(0,i)%>"><input type="button" onClick="validate(this.form);" value="Delete" name="B1" style="font-family: Verdana; font-size: 8pt; width:100px">
          </td>
		  <%
		  end if
		  %>
		  </form>
		  </tr>
							  </table>
		  </td>
			<%end if%>
        </tr>
<%
		Next
	Else
%>

        <tr>
          <td colspan="5" width="101">Sorry no items found</td>
        </tr>
<%
	End if
%>
      </table>
    </td>
  </tr>
</table>


  </center>
</div>

<table width="720" border="0" cellspacing="1" cellpadding="2" align="center">
  <tr> 
    <td width="180">
	<% if vSessionpage <> 1 and vsessionpage <>0 then %>
		<form action="Supplier_find.asp" method="post" name="frmFirst">
	        <input type="submit" name="Submit2" value="First Page">
			<input type="hidden" name="Direction" value="First">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">
			<input type="hidden" name="sname" value="<%=vsname%>">
			<input type="hidden" name="scode" value="<%=vscode%>">
			<input type="hidden" name="iname" value="<%=viname%>">
		</form>
	<% End if %>
    </td>
    <td width="180">
	<% if clng(vSessionpage) > 1 then %>
		<form action="Supplier_find.asp" method="post" name="frmPrevious">
	        <input type="submit" name="Submit5" value="Previous Page">
			<input type="hidden" name="Direction" value="Previous">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">
			<input type="hidden" name="sname" value="<%=vsname%>">
			<input type="hidden" name="scode" value="<%=vscode%>">
			<input type="hidden" name="iname" value="<%=viname%>">
		</form>
	<% End if %>
    </td>
    <td width="180">
	<% if clng(vSessionpage) < vPagecount then %>
		<form action="Supplier_find.asp" method="post" name="frmNext">
	        <input type="submit" name="Submit4" value="Next Page">
			<input type="hidden" name="Direction" value="Next">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">			
			<input type="hidden" name="sname" value="<%=vsname%>">
			<input type="hidden" name="scode" value="<%=vscode%>">
			<input type="hidden" name="iname" value="<%=viname%>">
		</form>
	<% End if %>
    </td>
    <td width="180">
	<% if clng(vSessionpage) <> vPagecount and vPagecount > 0  then %>
		<form action="Supplier_find.asp" method="post" name="frmLast">
	        <input type="submit" name="Submit3" value="Last Page">
			<input type="hidden" name="Direction" value="Last">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">			
			<input type="hidden" name="sname" value="<%=vsname%>">
			<input type="hidden" name="scode" value="<%=vscode%>">
			<input type="hidden" name="iname" value="<%=viname%>">
		</form>
	<% End if %>
    </td>
  </tr>
</table>
<p><font face="Verdana, Arial, Helvetica, sans-serif" size="2"></font></p>
</body>
</html>
<%
If IsArray(arSuppliers) Then Erase arSuppliers
%>