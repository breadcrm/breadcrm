<%@  language="VBScript" %>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
Response.Buffer = true
%>
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <!--#INCLUDE FILE="includes/head.inc" -->
    <title></title>
   

</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana;
    font-size: 8pt" >
    <!--#INCLUDE FILE="nav.inc" -->
    &nbsp;&nbsp;&nbsp;<br>
    &nbsp;&nbsp;&nbsp;
    <%
dim objBakery
dim recarray,vVecarray,sFileName
dim retcol,retcol2
dim fromdt, todt,i
Dim GrandTotal
Dim vanNo
Dim VanID
Dim count1, subTotal
VanID = -1
count1 = 0
subTotal = 0
GrandTotal = 0
fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")


set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set retcol2 = objBakery.GetVehicalDetails()
vVecarray = retcol2("VehicalDetails")
stop
if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
   todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if
if isdate(fromdt) and isdate(todt) then
	vanNo = Request.Form("VNo_Def")
	set retcol = objBakery.Display_VanRevenueReportByVan(cInt(vanNo),fromdt,todt)
	recarray = retcol("VanRevenueReportByVan")
end if

if isempty(vanNo) Then
    vanNo = -1
End if


stop
sFileName = "Van_Revenue_report.xls" 

Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=" & sFileName 

    %>
    <div align="center">
        <center>
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="font-family: Verdana;
                font-size: 8pt">
                <tr>
                    <td width="100%">
                        <b><font size="3">Revenue Report by Van<br>
                            &nbsp;</font></b>


                        <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0"
                            cellpadding="2">
                          
                            <tr>
                                <td width="100%" colspan="5">
                                    <b>Revenue Report by Van - From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" colspan="5">
                                    
                                    <%
                                    stop
                                    if isarray(recarray) then%> 
                                       <%for i=0 to UBound(recarray,2)
                                       
                                       if i <>  UBound(recarray,2) Then
                                       VanID = recarray(0,i+1)
                                       else
                                       VanID = -1
                                       End if
                                       %>
                                    
                                        
                                    
                                    <%If count1 = 0 Then %>
                                    <br /><br />
                                    <b>Van <%=recarray(0,i)%></b>
                                    <br /><br />
                                    <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0"
                                        cellpadding="2">
                                      
                                        <tr style="height:50px;">
                                            <td width="10%" bgcolor="#CCCCCC">
                                                <b>Customer No</b>
                                            </td>
                                            <td width="30%" bgcolor="#CCCCCC">
                                                <b>Customer Name</b>
                                            </td>
                                            <td width="20%" bgcolor="#CCCCCC">
                                                <b>Order Value</b>
                                            </td>
                                            <td width="20%" bgcolor="#CCCCCC">
                                                <b>Credit Value</b>
                                            </td>
                                            <td width="20%" bgcolor="#CCCCCC">
                                                <b>Net Revenue</b>
                                            </td>
                                        </tr>
                                     <%
                                     count1 = 1
                                     End if%>   
                                           
                                            <tr style="font-weight: bold; height:30px;">
                                                <td>
                                                    <%=recarray(1,i)%>
                                                </td>
                                                <td>
                                                    <%=recarray(2,i)%>
                                                </td>
                                                <td align="right">
                                                    � <%=formatNumber(recarray(3,i),2)%>
                                                </td>
                                                <td align="right">
                                                    � <%=formatNumber(recarray(4,i),2)%>
                                                </td>
                                                <td align="right">
                                                    � <%=formatNumber(recarray(5,i),2)%>
                                                </td>
                                            </tr>
                                            <%subTotal = subTotal + recarray(5,i)
                                              GrandTotal = GrandTotal + recarray(5,i)
                                             %>
                                            
                                            <%If recarray(0,i) <> VanID Then %>
                                            <tr style="height:30px;">
                                                <td width="10%" bgcolor="#CCCCCC" align="right" colspan="4">
                                                    <b>Total</b>&nbsp;&nbsp;
                                                </td>
                                                <td width="10%" bgcolor="#CCCCCC" align="right">
                                                    <b>&nbsp;� <%=formatNumber(subTotal,2)%></b>
                                                </td>
                                            </tr>
                                            
                                            <%
                                            if i =  UBound(recarray,2) Then%>
                                            <tr style="height:30px;">
                                                <td width="10%" bgcolor="#CCCCCC" align="right" colspan="4">
                                                    <b>Grand Total</b>&nbsp;&nbsp;
                                                </td>
                                                <td width="10%" bgcolor="#CCCCCC" align="right">
                                                    <b>&nbsp;� <%=formatNumber(GrandTotal,2)%></b>
                                                </td>
                                            </tr>
                                            
                                           <% End If
                                             %>
                                            
                                             </table>
                                             
                                             <%
                                             count1 = 0
                                             subTotal = 0
                                             End If%>
                                             
                                             
                                      <%
                                      
                                      next%>     
                                   
                                    <%else%>
                                     <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0"
                                        cellpadding="2">
                                    <tr>
                                    <td bgcolor="#CCCCCC" colspan="5">
                                        <b>0 - No records matched...</b>
                                    </td>
                                    </tr>
                                    </table>
                                    <%end if%>  
                                </td>
                            </tr>
                        </table>
                       
                        
                    </td>
                </tr>
            </table>
        </center>
    </div>
</body>
<%if IsArray(recarray) then erase recarray %>
</html>
