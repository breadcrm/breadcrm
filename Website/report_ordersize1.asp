<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim recarray
dim retcol
dim totpages
dim vSessionpage
dim i

dim weekvno
dim price
dim cno
dim gno
dim fromdt
dim todt

weekvno = replace(Request.Form("chkweekvno"),"'","''")
price = replace(Request.Form("txtprice"),"'","''")
cno = replace(Request.Form("chkcno"),"'","''")
gno = replace(Request.Form("chkgno"),"'","''")
fromdt = replace(Request.Form("txtfrom"),"'","''")
todt = replace(Request.Form("txtto"),"'","''")

if gno = "" then gno = "-1"
if cno = "" then cno = "-1"

if fromdt ="" then
  fromdt="from"
  todt="to"
end if

if weekvno = "-1" or weekvno = "" then
  weekvno = "0,1,2,3,5,8,9"
end if
'Response.Write "chkweekvno = " & Request.Form("chkweekvno") & "<br>"
'Response.Write "chkcno = " & Request.Form("chkcno") & "<br>"
'Response.Write "price = " & price & "<br>"
'Response.Write "chkgno = " & Request.Form("chkgno") & "<br>"
'Response.Write "fromdt = " & fromdt & "<br>"
'Response.Write "todt = " & todt & "<br>"
'Response.end

if not isnumeric(price) then
 price = 0
end if

totpages = Request.Form("pagecount")
select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(totpages) then	
			session("Currentpage")  = session("Currentpage") + 1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage") - 1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select

vSessionpage = session("Currentpage")
'Response.Write "vSessionpage = " & vSessionpage & "<br>"

if trim(weekvno) <> "" and isnumeric(price) and trim(cno) <> "" and trim(gno) <> "" and isdate(fromdt) and isdate(todt) then
set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
'Response.Write weekvno & "," & price & "," & cno & "," & gno & "," & fromdt & "," & todt & "," & vSessionpage & ",0"
set retcol = objBakery.OrderSizeReport(weekvno,price,cno,gno,fromdt,todt,vSessionpage,0)
'totpages = retcol("pagecount")
recarray = retcol("OrderSize")

'Response.Write "totpages = " & totpages & "<br>"

end if
%>
<div align="center">
  <center>
<table border="0" width="80%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="75%"><b><font size="3">Report - Order size</font></b>
    </td>
    <td width="25%" align="right"><b><font size="2">Page 1 of 1</font></b></td>
  </tr>
</table>
  </center>
</div>
<br>
<div align="center">
  <center>
      <table border="0" width="80%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td bgcolor="#CCCCCC"  width="50%"></td>
          <td colspan="3" bgcolor="#CCCCCC"  width="50%" align="right"><b>Date &lt;<%=fromdt%>&gt; - &lt;<%=todt%>&gt;</b>&nbsp;</td>
        </tr>
        <tr>
          <td bgcolor="#CCCCCC" width="50%"><b>Customer</b>&nbsp;</td>
          <td bgcolor="#CCCCCC" width="20%"><b>No of Deliveries</b></td>
          <td bgcolor="#CCCCCC" width="20%" align="right"><b>Order Value</b>&nbsp;</td>
          <td bgcolor="#CCCCCC" width="10%"></td>
        </tr><%
        if isarray(recarray) then
          for i=0 to ubound(recarray,2)%>
          <tr>
          <td width="50%"><%if trim(recarray(0,i))="" then Response.Write("no name") else Response.Write(recarray(0,i))%></td>
          <td align="left" width="20%"><%=recarray(2,i)%>&nbsp;</td>
          <td align="right" width="20%">� <%=formatnumber(recarray(1,i),2)%></td>
          <td width="10%"></td>
          </tr><%
          next
        else%>
          <tr>
          <td colspan="4">
          <b>No Records Found...</b>
          </td>
          </tr><%
        end if%>
      </table>
  </center>
</div>
<br>
<div align="center">
  <table width="720" border="0" cellspacing="1" cellpadding="2" align="center">
  <tr> 
  <% if clng(vSessionpage) <> 1 then %>
  <td>
    <form name="frmFirstPage" action="report_ordersize1.asp" method="post">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="hidden" name="Direction" value="First">
    <input type="hidden" name="chkweekvno" value="<%=weekvno%>">
    <input type="hidden" name="txtprice" value="<%=price%>">
    <input type="hidden" name="chkcno" value="<%=cno%>">
    <input type="hidden" name="chkgno" value="<%=gno%>">
    <input type="hidden" name="txtfrom" value="<%=fromdt%>">
    <input type="hidden" name="txtto" value="<%=todt%>">
    <input type="submit" name="submit" value="First Page">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) < clng(totpages) then %>
  <td>
    <form name="frmNextPage" action="report_ordersize1.asp" method="post">
    <input type="hidden" name="Direction" value="Next">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="hidden" name="chkweekvno" value="<%=weekvno%>">
    <input type="hidden" name="txtprice" value="<%=price%>">
    <input type="hidden" name="chkcno" value="<%=cno%>">
    <input type="hidden" name="chkgno" value="<%=gno%>">
    <input type="hidden" name="txtfrom" value="<%=fromdt%>">
    <input type="hidden" name="txtto" value="<%=todt%>">
    <input type="submit" name="submit" value="Next Page">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) > 1 then %>
  <td>
    <form name="frmPreviousPage" action="report_ordersize1.asp" method="post">
    <input type="hidden" name="Direction" value="Previous">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="hidden" name="chkweekvno" value="<%=weekvno%>">
    <input type="hidden" name="txtprice" value="<%=price%>">
    <input type="hidden" name="chkcno" value="<%=cno%>">
    <input type="hidden" name="chkgno" value="<%=gno%>">
    <input type="hidden" name="txtfrom" value="<%=fromdt%>">
    <input type="hidden" name="txtto" value="<%=todt%>">
    <input type="submit" name="submit" value="Previous Page">
    </form>
  </td>
  <%end if%>
  <%if clng(vSessionPage) <> clng(totpages) and clng(totpages) > 0 then %>
  <td>
    <form name="frmLastPage" action="report_ordersize1.asp" method="post">
    <input type="hidden" name="Direction" value="Last">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="hidden" name="chkweekvno" value="<%=weekvno%>">
    <input type="hidden" name="txtprice" value="<%=price%>">
    <input type="hidden" name="chkcno" value="<%=cno%>">
    <input type="hidden" name="chkgno" value="<%=gno%>">
    <input type="hidden" name="txtfrom" value="<%=fromdt%>">
    <input type="hidden" name="txtto" value="<%=todt%>">
    <input type="submit" name="submit" value="Last Page">
    </form>
  </td>
  <% end if%>
  </tr>
  </table>
</div>
</body>
</html>