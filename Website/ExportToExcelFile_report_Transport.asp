<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
Response.Buffer = true
Server.ScriptTimeout=6000
dim cname,cno,vno,objBakery1,retcol,recarray
cname = Request("cname")
cno = Request("cno")
vno = Request("vno")

set objBakery1 = server.CreateObject("Bakery.MgmReports")
objBakery1.SetEnvironment(strconnection)
'response.Write("cname-"+cname+"cno-"+cno+"vno-"+vno)
'response.End()
set retcol = objBakery1.Display_Transport_Report(cno,cname,vno)
recarray = retcol("TransportReport")
'set objBakery = nothing
set objBakery1 = nothing
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=report_transport.xls" 
%>

<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana;
	font-size: 8pt">
	<tr>
		<td width="100%">
		  <b><font size="3">Transport</font><font size="3"> Report<br>
		  &nbsp;</font></b>
				<table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">

						<tr height="24">
						  <td bgcolor="#CCCCCC"><b>Customer No</b></td>
						  <td bgcolor="#CCCCCC"><b>Customer Name</b></td>
						  <td bgcolor="#CCCCCC"><b>Address</b></td>
						  <td bgcolor="#CCCCCC"><b>Delivery Frequency </b></td>
						  <td bgcolor="#CCCCCC"><nobr><strong>Week Day Vehicle No </strong></nobr></td>
						  <td bgcolor="#CCCCCC"><nobr><b>Weekend Vehicle No </b></nobr></td>
						  <td bgcolor="#CCCCCC" align="right"><nobr><b>Delivery Time </b></nobr></td>
						</tr>
						<%if isarray(recarray) then%>

						<%for i=0 to UBound(recarray,2)%>
							  <tr height="22">
								  <td><b><%=recarray(0,i)%></b></td>
								  <td><b><%if recarray(1,i)<> "" then Response.Write recarray(1,i) end if%></b>&nbsp;</td>
								  <td ><b><%if recarray(2,i)<> "" then Response.Write recarray(2,i) end if%></b>&nbsp;</td>
								  <td ><b><%if recarray(3,i)<> "" then Response.Write recarray(3,i) end if%></b>&nbsp;</td>
								  <td><b><%if recarray(4,i)<> "" then Response.Write recarray(4,i) end if%></b>&nbsp;</td>
								  <td><b><%if recarray(5,i)<> "" then Response.Write recarray(5,i) end if%></b>&nbsp;</td>
								  <td><b><%if recarray(6,i)<> "" then Response.Write recarray(6,i) end if%></b>&nbsp;</td>
							  </tr>
							  
						  <%
						  next
          					response.Flush()
							response.End()
						  %>
				
						  <%
						  else%>
							 <td colspan="11" width="100%" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td><% 
						  end if%>
						</tr>
			</table>
	  </td>
	</tr>
</table>

