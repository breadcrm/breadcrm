<%@  language="VBScript" %>
<%
option Explicit
Response.Buffer = true
%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <!--#INCLUDE FILE="includes/head.inc" -->
    <title></title>
    <link media="all" href="calendar/calendar-system.css" type="text/css" rel="stylesheet">

    <script src="calendar/calendar.js" type="text/javascript"></script>

    <script src="calendar/calendar-en.js" type="text/javascript"></script>

    <script src="calendar/calendar-setup.js" type="text/javascript"></script>

    <script language="JavaScript" type="text/javascript">
<!--
        function initValueCal() {
            y = document.form.txtfrom.value
            x = y.split("/");
            d1 = x[1] + "/" + x[0] + "/" + x[2]
            t = new Date(d1);

            y = document.form.txtto.value
            x = y.split("/");
            d2 = x[1] + "/" + x[0] + "/" + x[2]
            t1 = new Date(d2);
            document.form.txtfrom.value = t.getDate() + '/' + eval((t.getMonth()) + 1) + '/' + t.getFullYear();
            document.form.txtto.value = t1.getDate() + '/' + eval((t1.getMonth()) + 1) + '/' + t1.getFullYear();
            document.form.day.value = t.getDate()
            document.form.month.value = t.getMonth()
            document.form.year.value = t.getFullYear()

            document.form.day1.value = t1.getDate()
            document.form.month1.value = t1.getMonth()
            document.form.year1.value = t1.getFullYear()
        }
        function setCal(theForm, del) {
            y = eval(theForm.year.options[theForm.year.selectedIndex].value)
            m = eval(theForm.month.options[theForm.month.selectedIndex].value)
            d = eval(theForm.day.options[theForm.day.selectedIndex].value)
            ret_val = 0
            if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
                ret_val = 1;
            if (m == 1 && ret_val == 0 && d >= 29) {
                theForm.day.value = 28
                d = 28
                del.value = d + '/' + (eval(m) + 1) + '/' + y;
            }
            if (m == 1 && ret_val == 1 && d >= 30) {
                theForm.day.value = 29
                d = 29
                del.value = d + '/' + (eval(m) + 1) + '/' + y;
            }
            if ((m == 3 || m == 5 || m == 8 || m == 10) && d > 30) {
                theForm.day.value = 30
                d = 30
                del.value = d + '/' + (eval(m) + 1) + '/' + y;
            }
            del.value = d + '/' + (eval(m) + 1) + '/' + y;
        }

        function setCal1(theForm, del) {
            y = eval(theForm.year1.options[theForm.year1.selectedIndex].value)
            m = eval(theForm.month1.options[theForm.month1.selectedIndex].value)
            d = eval(theForm.day1.options[theForm.day1.selectedIndex].value)
            ret_val = 0
            if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
                ret_val = 1;
            if (m == 1 && ret_val == 0 && d >= 29) {
                theForm.day1.value = 28
                d = 28
                del.value = d + '/' + (eval(m) + 1) + '/' + y;
            }
            if (m == 1 && ret_val == 1 && d >= 30) {
                theForm.day1.value = 29
                d = 29
                del.value = d + '/' + (eval(m) + 1) + '/' + y;
            }
            if ((m == 3 || m == 5 || m == 8 || m == 10) && d > 30) {
                theForm.day1.value = 30
                d = 30
                del.value = d + '/' + (eval(m) + 1) + '/' + y;
            }
            del.value = d + '/' + (eval(m) + 1) + '/' + y;
        }

        function setCalCombo(theForm, del) {
            y = del.value
            x = y.split("/");
            d1 = x[1] + "/" + x[0] + "/" + x[2]
            t = new Date(d1);
            theForm.day.value = t.getDate()
            theForm.month.value = t.getMonth()
            theForm.year.value = t.getFullYear()
        }
        function setCalCombo1(theForm, del) {
            y = del.value
            x = y.split("/");
            d1 = x[1] + "/" + x[0] + "/" + x[2]
            t = new Date(d1);
            theForm.day1.value = t.getDate()
            theForm.month1.value = t.getMonth()
            theForm.year1.value = t.getFullYear()
        }
//-->
    </script>
    
     <script language="JavaScript" type="text/javascript">
         function OnClick_Excel() {
            // document.form.method = 'post';
             document.form.action = 'Export_To_Excel_Van_Revenue_report.asp';
             document.form.submit();


         }

         function OnClick_Submit() {
             // document.form.method = 'post';
             document.form.action = 'Van_Revenue_report.asp';
             document.form.submit();


         }
    </script>

</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana;
    font-size: 8pt" onLoad="initValueCal()">
    <!--#INCLUDE FILE="nav.inc" -->
    &nbsp;&nbsp;&nbsp;<br>
    &nbsp;&nbsp;&nbsp;
    <%
dim objBakery
dim recarray,vVecarray
dim retcol,retcol2
dim fromdt, todt,i
Dim GrandTotal
Dim vanNo
Dim VanID
Dim count1, subTotal
VanID = -1
count1 = 0
subTotal = 0
GrandTotal = 0
fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")

set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set retcol2 = objBakery.GetVehicalDetails()
vVecarray = retcol2("VehicalDetails")
stop
if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
   todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if
if isdate(fromdt) and isdate(todt) then
	vanNo = Request.Form("VNo_Def")
	
	if isempty(vanNo) Then
        vanNo = -1
    End if
	
	set retcol = objBakery.Display_VanRevenueReportByVan(cInt(vanNo),fromdt,todt)
	recarray = retcol("VanRevenueReportByVan")
end if




    %>
    <div align="center">
        <center>
            <table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana;
                font-size: 8pt">
                <tr>
                    <td width="100%">
                        <b><font size="3">Revenue Report by Van<br>
                            &nbsp;</font></b>
                        <form method="post" action="Van_Revenue_report.asp" name="form">
                        <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <b>Van :</b>
                                    <select size="1" name="VNo_Def" style="font-family: Verdana; font-size: 8pt">
                                        <option value="-1">Select</option>
                                        <%for i = 0 to ubound(vVecarray,2)%>
                                        
                                         <%if CInt(vanNo) = vVecarray(0,i) then%>
						                        <option value="<%=vVecarray(0,i)%>" selected="selected"><%=vVecarray(0,i)%></option>
				                         <%else%>
						                        <option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
				                         <%end if%>
                                        
                                       
                                        <%next%>
                                    </select>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <b>From:</b>
                                </td>
                                <td>
                                    <b>To:</b>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td height="18" width="350">
                                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
                                    %>
                                    <select name="day" onChange="setCal(this.form,document.form.txtfrom)">
                                        <%for i=1 to 31%>
                                        <option value="<%=i%>">
                                            <%=i%></option>
                                        <%next%>
                                    </select>
                                    <select name="month" onChange="setCal(this.form,document.form.txtfrom)">
                                        <%for i=0 to 11%>
                                        <option value="<%=i%>">
                                            <%=arrayMonth(i)%></option>
                                        <%next%>
                                    </select>
                                    <select name="year" onChange="setCal(this.form,document.form.txtfrom)">
                                        <%for i=2000 to Year(Date)+1%>
                                        <option value="<%=i%>">
                                            <%=i%></option>
                                        <%next%>
                                    </select>
                                    <input class="Datetxt" id="txtfrom" onFocus="blur();" size="12" name="txtfrom" value="<%=fromdt%>"
                                        onchange="setCalCombo(this.form,document.form.txtfrom)">
                                    <img id="f_trigger_frfrom" onMouseOver="this.style.background='red';" title="Date selector"
                                        style="cursor: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
                                </td>
                                <td height="18" width="350">
                                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
                                        <%for i=1 to 31%>
                                        <option value="<%=i%>">
                                            <%=i%></option>
                                        <%next%>
                                    </select>
                                    <select name="month1" onChange="setCal1(this.form,document.form.txtto)">
                                        <%for i=0 to 11%>
                                        <option value="<%=i%>">
                                            <%=arrayMonth(i)%></option>
                                        <%next%>
                                    </select>
                                    <select name="year1" onChange="setCal1(this.form,document.form.txtto)">
                                        <%for i=2000 to Year(Date)+1%>
                                        <option value="<%=i%>">
                                            <%=i%></option>
                                        <%next%>
                                    </select>
                                    <input class="Datetxt" id="txtto" onFocus="blur();" name="txtto" size="12" value="<%=todt%>"
                                        onchange="setCalCombo1(this.form,document.form.txtto)">
                                    <img id="f_trigger_frto" onMouseOver="this.style.background='red';" title="Date selector"
                                        style="cursor: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
                                </td>
                                <td>
                                    <input type="button" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt" onClick="OnClick_Submit();">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        </form>

                        <script type="text/javascript">
                            Calendar.setup({
                                inputField: "txtfrom",
                                ifFormat: "%d/%m/%Y",
                                button: "f_trigger_frfrom",
                                singleClick: true
                            });
                        </script>

                        <script type="text/javascript">
                            Calendar.setup({
                                inputField: "txtto",
                                ifFormat: "%d/%m/%Y",
                                button: "f_trigger_frto",
                                singleClick: true
                            });
                        </script>

                        <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0"
                            cellpadding="2">
                           
                            <tr>
                                <td width="100%" colspan="5">
                                    <b>Revenue Report by Van - From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" colspan="5">
                                    
                                    <%
                                    stop
                                    if isarray(recarray) then%> 
                                       
                                       <div style="float:right;"><input name="btnExcel" type="button" onClick="OnClick_Excel();" value="Export To Excel" style="font-family: Verdana; font-size: 8pt"></div>
                                       <%for i=0 to UBound(recarray,2)
                                       
                                       if i <>  UBound(recarray,2) Then
                                       VanID = recarray(0,i+1)
                                       else
                                       VanID = -1
                                       End if
                                       %>
                                    
                                        
                                     
                                    <%If count1 = 0 Then %>
                                    
                                    
                                    <br /><br />
                                    <b>Van <%=recarray(0,i)%></b>
                                    <br /><br />
                                    
                           
                                    
                                    <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0"
                                        cellpadding="2">
                                      
                                        <tr>
                                            <td width="10%" bgcolor="#CCCCCC">
                                                <b>Customer No</b>
                                            </td>
                                            <td width="30%" bgcolor="#CCCCCC">
                                                <b>Customer Name</b>
                                            </td>
                                            <td width="20%" bgcolor="#CCCCCC">
                                                <b>Order Value</b>
                                            </td>
                                            <td width="20%" bgcolor="#CCCCCC">
                                                <b>Credit Value</b>
                                            </td>
                                            <td width="20%" bgcolor="#CCCCCC">
                                                <b>Net Revenue</b>
                                            </td>
                                        </tr>
                                     <%
                                     count1 = 1
                                     End if%>   
                                           
                                            <tr style="font-weight: bold">
                                                <td>
                                                    <%=recarray(1,i)%>
                                                </td>
                                                <td>
                                                    <%=recarray(2,i)%>
                                                </td>
                                                <td align="right">
                                                    � <%=formatNumber(recarray(3,i),2)%>
                                                </td>
                                                <td align="right">
                                                    � <%=formatNumber(recarray(4,i),2)%>
                                                </td>
                                                <td align="right">
                                                    � <%=formatNumber(recarray(5,i),2)%>
                                                </td>
                                            </tr>
                                            <%subTotal = subTotal + recarray(5,i)
                                              GrandTotal = GrandTotal + recarray(5,i)
                                             %>
                                            
                                            <%If recarray(0,i) <> VanID Then %>
                                            <tr>
                                                <td width="10%" bgcolor="#CCCCCC" align="right" colspan="4">
                                                    <b>Total</b>&nbsp;&nbsp;
                                                </td>
                                                <td width="10%" bgcolor="#CCCCCC" align="right">
                                                    <b>&nbsp;� <%=formatNumber(subTotal,2)%></b>
                                                </td>
                                            </tr>
                                            
                                            <%
                                            if i =  UBound(recarray,2) Then%>
                                            <tr>
                                                <td width="10%" bgcolor="#CCCCCC" align="right" colspan="4">
                                                    <b>Grand Total</b>&nbsp;&nbsp;
                                                </td>
                                                <td width="10%" bgcolor="#CCCCCC" align="right">
                                                    <b>&nbsp;� <%=formatNumber(GrandTotal,2)%></b>
                                                </td>
                                            </tr>
                                            
                                           <% End If
                                             %>
                                            
                                             </table>
                                             
                                             <%
                                             count1 = 0
                                             subTotal = 0
                                             End If%>
                                             
                                             
                                      <%
                                      
                                      next%>     
                                   
                                    <%else%>
                                     <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0"
                                        cellpadding="2">
                                    <tr>
                                    <td bgcolor="#CCCCCC" colspan="5">
                                        <b>0 - No records matched...</b>
                                    </td>
                                    </tr>
                                    </table>
                                    <%end if%>  
                                </td>
                            </tr>
                        </table>
                       
                        
                    </td>
                </tr>
            </table>
        </center>
    </div>
</body>
<%if IsArray(recarray) then erase recarray %>
</html>
