<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function DoughTypeChanged(frm)
{
	var selIdx = frm.selectedIndex;
	var vDoughType = frm[selIdx].text;		
	if(vDoughType != 'Select')
	{
		if (document.getElementById("lblDoughType")!=null)
			document.getElementById('lblDoughType').innerHTML = vDoughType;
	}
	else
		if (document.getElementById("lblDoughType")!=null)
			lblDoughType.value = "";
}

function CheckSearch(frm)
{
	if (frm.DoughType.value=="")
	{
		alert("Please select a Dough Name");
		frm.DoughType.focus();
		return  false;
	}
	return  true;
}
//-->
</SCRIPT>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal();DoughTypeChanged(document.form.DoughType)">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%

dim objBakery
dim recarray
dim retcol
dim fromdt, todt , i
Dim TotQuantity,TotTurnover
dim arrIName
dim arrProductCategory
dim recsecondmixarray
dim retsecondmixcol
dim arrSecondMixProductCategory
dim arrSecondMixIName

fromdt = Request.form("txtfrom")
strlblDoughType=request("lblDoughType")
todt = Request.Form("txtto")
strDoughType = Request.Form("DoughType")

if (strDoughType="") then
	strDoughType="0"
end if

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
   todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

if isdate(fromdt) and isdate(todt) then
    set objBakery = server.CreateObject("Bakery.reports")
	objBakery.SetEnvironment(strconnection)
    set retcol = objBakery.DoughMixingReport(cint(strDoughType),fromdt,todt)
    recarray = retcol("DoughMixingReport")
    
    set retsecondmixcol = objBakery.DoughSecondMixReport(cint(strDoughType),fromdt,todt)    
    recsecondmixarray=retsecondmixcol("DoughSecondMixReport")
end if

Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)

set detail = object.DisplayDoughType()	
vDoughTypearray =  detail("DoughType")

if (Request("Search")<>"") then
	'first mix
	if isarray(recarray) then
		'Production Category
		set objIName = objBakery.GetFilterData(recarray,3,arrIName)
		arrIName = objIName("FilterData")
		
		'IName -Ingredient Name
		set objProductCategory = objBakery.GetFilterData(recarray,2,arrProductCategory)
		arrProductCategory = objProductCategory("FilterData")
	end if    
	
	'second mix
	if isarray(recsecondmixarray) then
		'DName
		set objSecondmixDName = objBakery.GetFilterData(recsecondmixarray,1,arrSecondMixDName)
		arrSecondMixDName = objSecondmixDName("FilterData")
					
		'Production Category
		set objSecondmixProductCategory = objBakery.GetFilterData(recsecondmixarray,2,arrSecondMixProductCategory)
		arrSecondMixProductCategory = objSecondmixProductCategory("FilterData")
		
		'IName -Ingredient Name
		set objSecondmixIName = objBakery.GetFilterData(recsecondmixarray,3,arrSecondMixIName)
		arrSecondMixIName = objSecondmixIName("FilterData")
	end if
end if	    

set detail = Nothing
set object = Nothing
%>

<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
 <tr>
    <td width="100%"><b><font size="3">Dough Mixing Report<br>
      &nbsp;</font></b>
	  <form method="post" action="DoughMixingReport.asp" name="form" onSubmit="return CheckSearch(this)">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr height="35">
          
          <td colspan="4"><b>Dough Name:&nbsp; 
		      <select size="1" name="DoughType" style="font-family: Verdana; font-size: 8pt" onChange="DoughTypeChanged(this)">
			<option value = "">Select</option>
			<%
			if IsArray(vDoughTypearray) Then  	
				for i = 0 to ubound(vDoughTypearray,2)%>  				
			 <option value="<%=vDoughTypearray(0,i)%>" <%if strDoughType<>"" then%><%if cint(strDoughType)= vDoughTypearray(0,i) then response.write " Selected"%><%end if%>><%=vDoughTypearray(1,i)%></option>
			  <%
			   next                 
			End if
			%>   
		  </select>
          </b></td>
          <td></td>
        </tr>
		
		
        <tr>
          <td><b>From:</b></td>
          <td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom onFocus="blur();" size="12"  name="txtfrom" value="<%=fromdt%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					<td><b>To:</b></td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=todt%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td> <td>
            <input type="submit" value="Search" name="Search"  style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
	  </form>
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
<% if (Request("Search")<>"") then%>
<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
 <form method="post"name="frmReport">
<tr>
  <td width="100%"  colspan='<%=UBound(arrProductCategory) %>' height="40"><b>Dough Mixing Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
</tr>
</form>
<% 
    Set objGeneral = Server.CreateObject("bakery.general")
	objGeneral.SetEnvironment(strconnection)
    dim objIndex
    dim n, m, p
    dim weight, arrProductCategoryTotal (50)
    
    if not isempty(arrIName) then
        objGeneral.BubbleSort(arrIName)
    end if
        
    if not isempty(arrProductCategory) then        
        objGeneral.BubbleSort(arrProductCategory)
    end if
%>

<tr>
<td colspan='<%=UBound(arrProductCategory) %>'>
	<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
	<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
    <%if isarray(arrIName) then%>
		<td width="170" bgcolor="#CCCCCC">&nbsp;</td>
	    <%for m=0 to UBound(arrIName)
			arrProductCategoryTotal(m)=0
		%>
	        <td align="right" bgcolor="#CCCCCC"><%=arrIName(m)%></td>	
	    <%next%>
			<td align="right" bgcolor="#CCCCCC" width="120">Total Weight (Kg)</td>	
    <%else%>
		<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
	<%end if%>
	</tr>
	
<%if isarray(arrProductCategory) then%>
	<%
	strGrandTotalWeight=0
	for n=0 to UBound(arrProductCategory)
		if (n mod 2 =0) then
			strBgColour="#FFFFFF"
		else
			strBgColour="#F3F3F3"
		end if
	%>
	    <tr bgcolor="<%=strBgColour%>">
	        <td><%=arrProductCategory(n)%></td>
    	    
	        <%
			strTotalWeight=0
			
	        for p=0 to UBound(arrIName)
	            weight=0
    	        
	            for c=0 to UBound(recarray,2)
	                if(recarray(3,c)=arrIName(p) and recarray(2,c) = arrProductCategory(n)) then
	                   weight = recarray(4,c)
	                end if	            	
				next	           
    	            
	        %>
	            <td align="right"><%=formatnumber(weight,4)%></td>
	        <%			
			strTotalWeight=cdbl(strTotalWeight)+ cdbl(weight) 
			arrProductCategoryTotal(p)=cdbl(arrProductCategoryTotal(p))+cdbl(weight)
	        next
			
	        %>	    
    	   	<td align="right" style="font-weight:bold"><%=formatnumber(strTotalWeight,4)%></td> 
	    </tr>
		
	<%
	
	strGrandTotalWeight=strGrandTotalWeight+strTotalWeight
	next
	%>
	<tr bgcolor="#CCCCCC" style="font-weight:bold">
		<td>Total (Kg)</td>
		<%for m=0 to UBound(arrIName)%>
			<td align="right"><%=formatnumber(arrProductCategoryTotal(m),4)%></td>
		<%next%>
		<td align="right"><%=formatnumber(strGrandTotalWeight,4)%></td>
	</tr>
<%end if%>
</table>
</td>
</tr>
<tr>
    <td>
        <!--#include file = "Includes/DoughSecondMix.asp"-->
    </td>
</tr>
</table>
<%end if%>
</td>
</tr>
</table>
<br><br>
</body>
<%if IsArray(recarray) then erase recarray  %>
</html>