<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Dough Mixing Report</title>
<LINK media="all" href="calendar/calendar-system.css" type="text/css" rel="stylesheet" />

<style type="text/css">
<!--
body {
	font-family:Verdana, Geneva, sans-serif;
	font-size: 12px;
	color:#000000;
	background-color:#FFFFFF;
	/*vertical-align:text-top;*/
	margin:0px;
	padding:0px;
}

#contentstart { width:2000px; /*vertical-align:text-top;*/}

.pagehead {
	border:0px;
	width:100%;
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	display:block;
}

br.page{page-break-before: always; height:1px}

table{
font-family:Verdana, Arial;
font-size: 12px;
}
-->
</style>

<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>

<SCRIPT language=JavaScript type=text/javascript>
<!--
function PrintThisPage() 
{ 
   var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
       sOption+="scrollbars=yes,width=750,height=550,left=100,top=25,resizable=1"; 

	document.getElementById('tbName').style.display = 'block';
	

   var sWinHTML = document.getElementById('contentstart').innerHTML; 
   
   var winprint=window.open("","",sOption); 
       winprint.document.open(); 
       winprint.document.write('<html>');
       winprint.document.write('<head>');
       winprint.document.write('<title>Bakery System</title>');
       //winprint.document.write('<LINK href=images/styles.css rel=Stylesheet>');
	   winprint.document.write('<style type="text/css">');
		winprint.document.write('<!--');
		winprint.document.write('br.page{page-break-before: always; height:1px}');
		winprint.document.write('table{');
		winprint.document.write('font-family:Verdana, Arial;');
		winprint.document.write('font-size: 10px;');
		winprint.document.write('}');
		winprint.document.write('-->');
		winprint.document.write('</style>');
       winprint.document.write('</head>');
       winprint.document.write('<body onload="window.print()">'); 
	  // winprint.document.write('Direct Dough Mixing Report');
 	
       winprint.document.write(sWinHTML);          
       winprint.document.write('</body></html>'); 
       winprint.document.close(); 
       winprint.focus(); 
}

function initValueCal(){


	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
	
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function DoughTypeChanged(frm)
{

	var selIdx = frm.selectedIndex;
	var vDoughType = frm[selIdx].text;
	if(vDoughType != 'Select')
	{
		if (vDoughType != 'All')
		{
			if (document.getElementById("lblDoughType")!=null)
				document.getElementById('lblDoughType').innerHTML = vDoughType;
		}
	}
	else
		if (document.getElementById("lblDoughType")!=null)
			lblDoughType.value = "";
			
	
}

function CheckSearch(frm)
{
	if (frm.DoughType.value=="")
	{
		alert("Please select a Dough Name");
		frm.DoughType.focus();
		return  false;
	}
	
	
	return  true;
}

function OnClick_Excel()
{
	document.frmReport.method = 'post';
	document.frmReport.action = 'ExportToExcelFile_DoughMixingReport.asp';
	document.getElementById("hdnAction").value = 'ExcelExport';
	document.frmReport.submit();
	
	
}

//-->
</SCRIPT>
</head>
<body>

<!--onLoad="initValueCal();DoughTypeChanged(document.form.DoughType)"-->

<%

dim objBakery
dim retcol
dim fromdt, todt , i , fromdtCom , todtCom
Dim TotQuantity,TotTurnover
dim arrProductCategory
dim arrSecondMixProductCategory
dim vDoughType
dim nRecCount 
dim sAction
dim colWidth
dim TotcolWidth
dim NoOfColumnsPerLine
dim isTableBreak
dim nPrevoiusType

'from Scenario-2
dim retsecondmixcol

'from Scenario-3
dim recsecondmixarrayNew
dim retsecondmixcolNew

colWidth = "110px"
TotcolWidth = "140px"
NoOfColumnsPerLine = 7
nPrevoiusType = -1


sAction = Request.form("hdnAction")

if sAction = "ExcelExport" Then

Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=DoughMixingReport.xls" 
End IF


if Request.QueryString("pageFrom") = "" Then

	fromdt = Request.form("txtfrom")
	'strlblDoughType=request("lblDoughType")
	todt = Request.Form("txtto")
	strDoughType = Request.Form("DoughType")
	vDoughType = Request.QueryString("dtype")
	vFacility = Request.QueryString("facility")
	
	fromdtCom = Request.Form("year") & "-" & CInt(Request.Form("month")) + 1 & "-" & Request.Form("day")
	todtCom = Request.Form("year1") & "-" & CInt(Request.Form("month1")) + 1 & "-" & Request.Form("day1")
	
Elseif  Request.QueryString("pageFrom") = "Process" Then	
	
	fromdt = Request.form("deldate")
	'strlblDoughType=request("lblDoughType")
	todt = Request.Form("deldate")
	strDoughType = Request.Form("DoughType")
	vDoughType = Request.Form("dtype")
	vFacility = Request.Form("facility")
	
	fromdtCom = Request.Form("year") & "-" & CInt(Request.Form("month")) + 1 & "-" & Request.Form("day")
	todtCom = Request.Form("year") & "-" & CInt(Request.Form("month")) + 1 & "-" & Request.Form("day")
	
	
End if

strlblDoughType=request("lblDoughType")

strDeliveryType = Request.Form("DeliveryType") 
if (strDeliveryType="" or not isnumeric(strDeliveryType)) then
	strDeliveryType="0"
end if

if vReportCaregoty = "" then
	vReportCaregoty = "0"
end if

If vDoughType = "" Then
	vDoughType = 1
End If

if (strDoughType = "") then
	strDoughType = "-1"
end if

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
   todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
   
   fromdtCom = Year(date()) & "-" & CInt(Month(date())) + 1 & "-" & Day(date())
	todtCom = Year(date()) & "-" & CInt(Month(date())) + 1 & "-" & Day(date())
end if

Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayDoughNameDoughMixingReport(vDoughType)
vDoughTypearray =  detail("DoughName")

set detail = Nothing


%>
<form method="post" name="frmReport" id="frmReport" >
<table align="center" border="0"  cellspacing="0" cellpadding="0"  style="font-family: Verdana; font-size: 12px; vertical-align:top; "  >
 <tr valign="top">
    <td valign="top">
	<div align="left" id="contentstart" style="padding-left:20px;">
		
			<table id="tbName" cellspacing="0" cellpadding="0" style="vertical-align:top;">
				<tr>
					<td><b><font size="3"><%if vDoughType = 1 then if vFacility = "0" then response.Write("Direct") else response.Write("Stanmore English") end if else response.Write("48 Hours") end if%> Dough Mixing Report<br>
					  &nbsp;</font></b></td>
				</tr>
				<tr>
					<td>From : <%=fromdt%> - To : <%=todt%></td>
				</tr>
			</table>
		
			<!--<form method="" name="frmReport">-->
			<table align="right" style="vertical-align:top;">
				<tr>
					<td align="right">
						<!--<input name="ExportToExcel" value="Export To Excel" type="submit"  onClick="OnClick_Excel();">-->
					</td>
				</tr>
			</table>
			
			<table width="50%" style="vertical-align:top;">
            	<tr>
					<td align="right"><input name="btnExcel" type="button" onClick="OnClick_Excel();" value="Export To Excel" style="font-family: Verdana; font-size: 8pt"></td>
                </tr>    
			</table>
			<!--</form>-->
			<%
    
            set objBakery = server.CreateObject("Bakery.reports")
            objBakery.SetEnvironment(strconnection)
            dim NoOfLoops
            nRecCount = 5 'this is for page header
            If strDoughType = "-1" Then
                NoOfLoops = UBound(vDoughTypearray,2)
            Else
                NoOfLoops = 0
            End If
            
            dim IsDataAvailbale, IsSecondMixDataAvailbale
            IsDataAvailbale = 0 'default value is 0
            IsSecondMixDataAvailbale = 0 'default value is 0
            
            
            for d=0 to NoOfLoops
            
			isTableBreak = false
            dim arrIName
            dim recarray
            dim vReportType
            'from Scenario-2
            dim arrSecondMixIName
            dim recsecondmixarray
            
			'if(cint(vDoughTypearray(0,d)) = 69) then
			'stop
			'end if
			
            Set objGeneral = Server.CreateObject("bakery.general")
			objGeneral.SetEnvironment(strconnection)
            If strDoughType = "-1" Then
                set retdoughtypecol = objGeneral.DoughMixingReportType(cint(vDoughTypearray(0,d)))
                recarrayreportype = retdoughtypecol("ReportType")
            Else
                set retdoughtypecol = objGeneral.DoughMixingReportType(cint(strDoughType))
                recarrayreportype = retdoughtypecol("ReportType")
				pecol("ReportType")
            End If
            
            if isarray(recarrayreportype) then
                vReportType = recarrayreportype(0,0)
            end if
            
            if vReportType = "" then
                vReportType = "1"
            end if
            
            If isdate(fromdt) and isdate(todt) Then
                If vReportType = "1" Then'FIRST CONDITION
                    If strDoughType = "-1" Then
                        set retcol = objBakery.DoughMixingReport(cint(vDoughTypearray(0,d)), fromdtCom, todtCom, cint(vFacility),cint(strDeliveryType),0)
                        recarray = retcol("DoughMixingReport")
                    Else
                        set retcol = objBakery.DoughMixingReport(cint(strDoughType), fromdtCom, todtCom, cint(vFacility),cint(strDeliveryType),0)
                        recarray = retcol("DoughMixingReport")
                    End If
                Elseif vReportType = "0" Then'SECOND CONDITION
                
                    If strDoughType = "-1" Then
                        set retcol = objBakery.DoughMixSummaryReport(cint(vDoughTypearray(0,d)), fromdtCom, todtCom, cint(vFacility),cint(strDeliveryType),0)
                        recarray = retcol("DoughMixSummaryReport")
                        
                        set retsecondmixcol = objBakery.DoughSecondMixReport(cint(vDoughTypearray(0,d)),fromdtCom,todtCom)
                        recsecondmixarray=retsecondmixcol("DoughSecondMixReport")
                        
                        set retsecondmixcolNew = objBakery.DoughSecondMixReportNew(cint(vDoughTypearray(0,d)), fromdtCom, todtCom, cint(vFacility), cint(strDeliveryType),0)
                        recsecondmixarrayNew=retsecondmixcolNew("DoughSecondMixReport")
                    Else
                        set retcol = objBakery.DoughMixSummaryReport(cint(strDoughType), fromdtCom, todtCom, cint(vFacility),cint(strDeliveryType),0)
                        recarray = retcol("DoughMixSummaryReport")
                        
                        set retsecondmixcol = objBakery.DoughSecondMixReport(cint(strDoughType),fromdtCom,todtCom)
                        recsecondmixarray=retsecondmixcol("DoughSecondMixReport")
                        
                        set retsecondmixcolNew = objBakery.DoughSecondMixReportNew(cint(strDoughType), fromdtCom, todtCom, cint(vFacility), cint(strDeliveryType),0)
                        recsecondmixarrayNew=retsecondmixcolNew("DoughSecondMixReport")
                    End If
                Elseif vReportType = "3" Then'THIRD CONDITION
                    'get the data for the first mix
                    If strDoughType = "-1" Then
                        set retcol = objBakery.DoughMixingReportTwoStageMixing(cint(vDoughTypearray(0,d)), fromdtCom, todtCom, 0, cint(vFacility), cint(strDeliveryType),0)
                        recarray = retcol("DoughMixingReport")
                    Else
                        set retcol = objBakery.DoughMixingReportTwoStageMixing(cint(strDoughType), fromdtCom, todtCom, 0, cint(vFacility), cint(strDeliveryType),0)
                        recarray = retcol("DoughMixingReport")
                    End If
                    
                    'get the data for the secod mix
                    If strDoughType = "-1" Then
                        set retsecondmixcol = objBakery.DoughMixingReportTwoStageMixing(cint(vDoughTypearray(0,d)), fromdtCom, todtCom, 1, cint(vFacility), cint(strDeliveryType),0)
                        recsecondmixarray = retsecondmixcol("DoughMixingReport")
                    Else
                        set retsecondmixcol = objBakery.DoughMixingReportTwoStageMixing(cint(strDoughType), fromdtCom, todtCom, 1, cint(vFacility), cint(strDeliveryType),0)
                        recsecondmixarray = retsecondmixcol("DoughMixingReport")
                    End If
                End If
            End If
            
           ' if (Request("Search")<>"") then
            
                If vReportType = "1" Then'FIRST CONDITION
                    if isarray(recarray) then
                        'IName -Ingredient Name
                        set objIName = objBakery.GetFilterData(recarray,3,arrIName)
                        arrIName = objIName("FilterData")
                        
                        'Production Category
                        set objProductCategory = objBakery.GetFilterData(recarray,2,arrProductCategory)
                        arrProductCategory = objProductCategory("FilterData")
                        
                        'OK, it says that there are data availble to show
                        IsDataAvailbale = 1
                        nRecCount = nRecCount + 5 ' Name + 2 headers + one line break below the table
                    Else
                        arrIName = "Fail"
                        arrProductCategory = "Fail"
                    end if
                ElseIf vReportType = "0" Then'SECOND CONDITION
                
                    'first mix
                    if isarray(recarray) then
                        'Production Category
                        set objProductCategory = objBakery.GetFilterData(recarray,2,arrProductCategory)
                        arrProductCategory = objProductCategory("FilterData")
                        
                        
                        'IName -Ingredient Name
                        set objIName = objBakery.GetFilterData(recarray,3,arrIName)
                        arrIName = objIName("FilterData")
                        
                        'OK, it says that there are data availble to show
                        IsDataAvailbale = 1
                        nRecCount = nRecCount + 5 ' Name + 2 headers + one line break below the table
						
                    else
                        arrProductCategory = "Fail"
                        arrIName = "Fail"
                    end if
                    
                    'second mix
                    if isarray(recsecondmixarray) then
                        'DName
                        set objSecondmixDName = objBakery.GetFilterData(recsecondmixarray,1,arrSecondMixDName)
                        arrSecondMixDName = objSecondmixDName("FilterData")
                                    
                        'Production Category
                        set objSecondmixProductCategory = objBakery.GetFilterData(recsecondmixarray,2,arrSecondMixProductCategory)
                        arrSecondMixProductCategory = objSecondmixProductCategory("FilterData")
                        
                        'IName -Ingredient Name
                        set objSecondmixIName = objBakery.GetFilterData(recsecondmixarray,3,arrSecondMixIName)
                        arrSecondMixIName = objSecondmixIName("FilterData")
                        nRecCount = nRecCount + 4 ' Name + 2 headers + one line break below the table
						
						 
                    else
                        arrSecondMixDName = "Fail"
                        arrSecondMixProductCategory = "Fail"
                        arrSecondMixIName = "Fail"
                    end if
                    
                    'second mix - NEW
                    If isarray(recsecondmixarrayNew) Then
                        'DName
                        
                        set objSecondmixDNameNew = objBakery.GetFilterData(recsecondmixarrayNew,1,arrSecondMixDNameNew)
                        arrSecondMixDNameNew = objSecondmixDNameNew("FilterData")
                        
                        'IName -Ingredient Name
                        
                        set objSecondmixINameNew = objBakery.GetFilterData(recsecondmixarrayNew,2,arrSecondMixINameNew)
                        arrSecondMixINameNew = objSecondmixINameNew("FilterData")
                        nRecCount = nRecCount + 4 ' Name + 2 headers + one line break below the table
						
						'nRecCount = nRecCount + UBOUND(recsecondmixarrayNew) *4
                    Else
                        arrSecondMixDNameNew = "Fail"
                        arrSecondMixINameNew = "Fail"
                    End If
                ElseIf vReportType = "3" Then'THIRD CONDITION
            
                    'first mix
                    if isarray(recarray) then
                        'IName -Ingredient Name
                        set objIName = objBakery.GetFilterData(recarray,3,arrIName)
                        arrIName = objIName("FilterData")
                        
                        'Production Category
                        set objProductCategory = objBakery.GetFilterData(recarray,2,arrProductCategory)
                        arrProductCategory = objProductCategory("FilterData")
                        
                        'OK, it says that there are data availble to show
                        IsDataAvailbale = 1
                        nRecCount = nRecCount + 6 ' Name + 3 headers + one line break below the table
						
						'nRecCount = nRecCount + UBOUND(recarray) * 5
                    Else
                        arrIName = "Fail"
                        arrProductCategory = "Fail"
                    end if
                    
                    'second mix
                    if isarray(recsecondmixarray) then
                        'IName - Ingredent Name
                        set objSecondMixIName = objBakery.GetFilterData(recsecondmixarray,3,arrSecondMixIName)
                        arrSecondMixIName = objSecondMixIName("FilterData")
                        
                        'Production Category
                        set objSecondMixProductCategory = objBakery.GetFilterData(recsecondmixarray,2,arrSecondMixProductCategory)
                        arrSecondMixProductCategory = objSecondMixProductCategory("FilterData")
                        
                        'OK, it says that there are data availble to show
                        IsSecondMixDataAvailbale = 1
                        nRecCount = nRecCount + 6 ' Name + 3 headers + one line break below the table
						
						'nRecCount = nRecCount + UBOUND(recsecondmixarray) * 5
                    else
                        arrSecondMixIName = "Fail"
                        arrSecondMixProductCategory = "Fail"
                    end if
                End If
          '  end if
            
            set object = Nothing
            
            %>
            <%If vReportType = "1" Then%>
            <!-- START OF MAIN DOUGH WITHOUT SECOND MIX CATEGORY -->
            <%' if (Request("Search")<>"") then%>
            	<%if isarray(recarray) then
            
						if IslineBreak(vReportType,UBound(arrIName),0) Then%>
						<!--<hr color="#CC3366">-->
						
						<br class="page" />
						<!--<p style="page-break-before: always;">-->
						<%End If %>
            <div >
			<table border="0" style="font-family: Verdana; font-size: 12px;vertical-align:top;" cellspacing="0" cellpadding="2"  >
			<%
			
                if strDoughType = "-1" then
                    If isarray(arrProductCategory) then
            %>

 			<!--<form method="post"name="frmReport">-->
 				<tr>
  					<td width="100%" colspan='<%=UBound(arrProductCategory) %>' height="40" style="font-size:14px;"><b>Dough Name - <label id="lblDoughType"><%=vDoughTypearray(1,d)%></label></b></td>
				</tr>
				<!--report type 1 -->
			<!--</form>-->

			<%
                    End If
                Else
            %>
 			<!--<form method="post"name="frmReport">-->
				<tr>
				<% 
				strlblDoughType =  GetSelectedDoughType(strDoughType)
				
				%>
  					<td width="100%"  colspan='<%=UBound(arrProductCategory) %>' height="40" style="font-size:14px;"><b>Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
				</tr>
			<!--</form>-->

			<%
                End If ' End for [strDoughType = "-1"] check
                
                'Set objGeneral = Server.CreateObject("bakery.general")
                dim objIndex
                dim n, m, p
                dim weight, arrProductCategoryTotal (50), arrProductCategoryTotal1 (50)
                
                if not isempty(arrIName) then
                    objGeneral.BubbleSort(arrIName)
                end if
                    
                if not isempty(arrProductCategory) then        
                    objGeneral.BubbleSort(arrProductCategory)
                end if
                
                set objGeneral = Nothing
            %>
				
				<tr>
					<td colspan='<%=UBound(arrProductCategory) %>'>
						<table border="1"  bordercolorlight="#CCCCCC" bordercolordark="#FFFFFF"  style="font-family: Verdana; font-size: 12px" cellspacing="0" cellpadding="0">  
							<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
							<%if isarray(arrIName) then%>
								<td width="<%=colWidth%>" bgcolor="#CCCCCC">&nbsp;</td>
								<%for m=0 to UBound(arrIName)
                                    arrProductCategoryTotal(m)=0
                                    arrProductCategoryTotal1(m)=0
                                %>
								
								<%If m < NoOfColumnsPerLine then %>
								<td align="center" bgcolor="#CCCCCC" width="<%=colWidth%>"><%=arrIName(m)%></td>	
								
								<%End If %>
								
								<%next%>
								
								<%If m < NoOfColumnsPerLine then %>
								
								<td align="center" bgcolor="#CCCCCC" width="<%=TotcolWidth%>">Total Weight </td>	
								<%End If %>
								
								<%
                                else
                                    If strDoughType <> "-1" Then
                                %>
								<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
								<%
								End If
							end if
							%>
							</tr>

							<%
                            
                            If isarray(arrProductCategory) Then
                                strGrandTotalWeight1 = 0
                                for n1=0 to UBound(arrProductCategory)
                                    strTotalWeight1 = 0
                                    
                                    for p1=0 to UBound(arrIName)
                                        weight1 = 0
                                        for c1=0 to UBound(recarray,2)
                                            if(recarray(3,c1)=arrIName(p1) and recarray(2,c1) = arrProductCategory(n1)) then
                                               weight1 = recarray(4,c1)
                                            end if
                                        next
                                        strTotalWeight1 = cdbl(strTotalWeight1)+ cdbl(weight1) 
                                        arrProductCategoryTotal1(p1) = cdbl(arrProductCategoryTotal1(p1))+cdbl(weight1)
                                    next
                                strGrandTotalWeight1 = strGrandTotalWeight1 + strTotalWeight1
                                next
                            End If
                            %>
                            
                            <%if isarray(arrProductCategory) then%>
							<tr bgcolor="#CCCCCC" style="font-weight:bold">
								<td width="<%=TotcolWidth%>">Total</td>
								<%for m1=0 to UBound(arrIName)%>
								<%If m1 < NoOfColumnsPerLine then %>
								<td align="right" width="<%=colWidth%>"><%=formatnumber(arrProductCategoryTotal1(m1),3)%> Kg</td>
								<%End If %>
								<%next%>
								
								<%If m1 < NoOfColumnsPerLine then %>
								<td align="right" width="<%=TotcolWidth%>" style="font-size:14px;"><%=formatnumber(strGrandTotalWeight1,3)%> Kg</td>						<%End If 
								
								nRecCount = nRecCount + 1
								%>
							</tr>
							<%
							
                            strGrandTotalWeight=0
                            for n=0 to UBound(arrProductCategory)
                                if (n mod 2 =0) then
                                    strBgColour="#FFFFFF"
                                else
                                    strBgColour="#F3F3F3"
                                end if
                            %>
	    					<tr bgcolor="<%=strBgColour%>">
	        					<td width="<%=colWidth%>"><%=arrProductCategory(n)%> </td>
								<%
                                strTotalWeight=0
                                
                                for p=0 to UBound(arrIName)
                                    weight=0
                                    
                                    for c=0 to UBound(recarray,2)
                                        if(recarray(3,c)=arrIName(p) and recarray(2,c) = arrProductCategory(n)) then
                                           weight = recarray(4,c)
                                        end if	            	
                                    next	           
                                        
                                %>
								<%If p < NoOfColumnsPerLine then%>
	            				<td align="right" width="<%=colWidth%>"><%=formatnumber(weight,3)%> Kg</td>
								<% 
									if p = NoOfColumnsPerLine - 1 Then
										isTableBreak = true
									end if
								
								else
										isTableBreak = true					
								End if 
								
								
								
											
                                strTotalWeight=cdbl(strTotalWeight)+ cdbl(weight) 
                                arrProductCategoryTotal(p)=cdbl(arrProductCategoryTotal(p))+cdbl(weight)
                                next
                                
                                %>	    
    	   						<%If isTableBreak = false then %>
								<td align="right"  style="font-weight:bold" width="<%=TotcolWidth%>"><%=formatnumber(strTotalWeight,3)%> Kg</td> 							<%End if %>
							</tr>
							<%
                            
                            strGrandTotalWeight = strGrandTotalWeight + strTotalWeight
                            nRecCount = nRecCount + 1
                            'session("RecCount") = session("RecCount") + 1
                            next
                            %>
                            <!--
                            'Total at the botton were commented by Ramanan on 4th Feb 2010
                            <tr bgcolor="#CCCCCC" style="font-weight:bold">
                                <td>Total (Kg)</td>
                                <%for m=0 to UBound(arrIName)%>
                                    <td align="right"><%=formatnumber(arrProductCategoryTotal(m),4)%></td>
                                <%next%>
                                <td align="right"><%=formatnumber(strGrandTotalWeight,4)%></td>
                            </tr>
                            -->
							
						<%end if%>
							
			
							
                    </table>
                </td>
            </tr>
			
			<% 'Drow second line for type =1 
			If isTableBreak = true then
			nRecCount = nRecCount + 5
			%>
			
			
			<tr><td height="22px;"  ></td></tr> <!--Blank line -->							
			
				<tr>
					<td colspan='<%=UBound(arrProductCategory) %>'>
						<table border="1"  bordercolorlight="#CCCCCC" bordercolordark="#FFFFFF"  style="font-family: Verdana; font-size: 12px;vertical-align:top;" cellspacing="0" cellpadding="0" >  
							<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
							<%if isarray(arrIName) then%>
								<!--<td width="<%'=colWidth%>" bgcolor="#CCCCCC">&nbsp;</td>-->
								<%for m=NoOfColumnsPerLine to UBound(arrIName)
                                    arrProductCategoryTotal(m)=0
                                    arrProductCategoryTotal1(m)=0
                                %>
								
								<%If m >= NoOfColumnsPerLine then %>
								<td align="center" bgcolor="#CCCCCC" width="<%=colWidth%>"><%=arrIName(m)%></td>	
								
								<%End If %>
								
								<%next%>
								
								<%If m >= NoOfColumnsPerLine then %>
								
								<td align="center" bgcolor="#CCCCCC" width="<%=TotcolWidth%>">Total Weight </td>	
								<%End If %>
								
								<%
                                else
                                    If strDoughType <> "-1" Then
                                %>
								<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
								<%
								End If
							end if
							%>
							</tr>

							<%
                            
                            If isarray(arrProductCategory) Then
                                strGrandTotalWeight1 = 0
                                for n1=0 to UBound(arrProductCategory)
                                    strTotalWeight1 = 0
                                    
                                    for p1=0 to UBound(arrIName)
                                        weight1 = 0
                                        for c1=0 to UBound(recarray,2)
                                            if(recarray(3,c1)=arrIName(p1) and recarray(2,c1) = arrProductCategory(n1)) then
                                               weight1 = recarray(4,c1)
                                            end if
                                        next
                                        strTotalWeight1 = cdbl(strTotalWeight1)+ cdbl(weight1) 
                                        arrProductCategoryTotal1(p1) = cdbl(arrProductCategoryTotal1(p1))+cdbl(weight1)
                                    next
                                strGrandTotalWeight1 = strGrandTotalWeight1 + strTotalWeight1
                                next
                            End If
                            %>
                            
                            <%if isarray(arrProductCategory) then%>
							<tr bgcolor="#CCCCCC" style="font-weight:bold">
								<!--<td width="<%'=TotcolWidth%>">Total</td>-->
								<%for m1=NoOfColumnsPerLine to UBound(arrIName)%>
								<%If m1 >= NoOfColumnsPerLine then %>
								<td align="right" width="<%=colWidth%>"><%=formatnumber(arrProductCategoryTotal1(m1),3)%> Kg</td>
								<%End If %>
								<%next%>
								
								<%If m1 >= NoOfColumnsPerLine then %>
								<td align="right" width="<%=TotcolWidth%>" style="font-size:14px;"><%=formatnumber(strGrandTotalWeight1,3)%> Kg</td>						<%End If 
								
								nRecCount = nRecCount + 1
								%>
							</tr>
							<%
							
                            strGrandTotalWeight=0
                            for n=0 to UBound(arrProductCategory)
                                if (n mod 2 =0) then
                                    strBgColour="#FFFFFF"
                                else
                                    strBgColour="#F3F3F3"
                                end if
                            %>
	    					<tr bgcolor="<%=strBgColour%>">
	        					<!--<td width="<%'=colWidth%>"><%'=arrProductCategory(n)%> </td>-->
								<%
                                strTotalWeight=0
                                
                                for p=0 to UBound(arrIName)
                                    weight=0
                                    
                                    for c=0 to UBound(recarray,2)
                                        if(recarray(3,c)=arrIName(p) and recarray(2,c) = arrProductCategory(n)) then
                                           weight = recarray(4,c)
                                        end if	            	
                                    next	           
                                        
                                %>
								<%If p >= NoOfColumnsPerLine then%>
	            				<td align="right" width="<%=colWidth%>"><%=formatnumber(weight,3)%> Kg</td>
								<% 
									
									if UBound(arrIName) = p Then
										isTableBreak = false
									end if
									
									'if p = NoOfColumnsPerLine + 1 Then
										'isTableBreak = true
									'end if
								
								else
										
										
										if UBound(arrIName) = p Then
											isTableBreak = false
										else
											isTableBreak = true	
										end if				
								End if 
								
								
								
											
                               strTotalWeight=cdbl(strTotalWeight)+ cdbl(weight) 
                               arrProductCategoryTotal(p)=cdbl(arrProductCategoryTotal(p))+cdbl(weight)
                                next
                                
                                %>	    
    	   						<%If isTableBreak = false then %>
								<td align="right" style="font-weight:bold" width="<%=TotcolWidth%>"><%=formatnumber(strTotalWeight,3)%> Kg</td> 							<%End if %>
							</tr>
							<%
                            
                          '  strGrandTotalWeight = strGrandTotalWeight + strTotalWeight
                             nRecCount = nRecCount + 1
                            
                            next
                            %>
                            <!--
                            'Total at the botton were commented by Ramanan on 4th Feb 2010
                            <tr bgcolor="#CCCCCC" style="font-weight:bold">
                                <td>Total (Kg)</td>
                                <%for m=0 to UBound(arrIName)%>
                                    <td align="right"><%=formatnumber(arrProductCategoryTotal(m),4)%></td>
                                <%next%>
                                <td align="right"><%=formatnumber(strGrandTotalWeight,4)%></td>
                            </tr>
                            -->
							
						<%end if%>
							
			
							
                    </table>
                </td>
            </tr>
			
			
										
			<%End IF
			
			'end of second line
			%> 
			
			
			
			
			<tr><td height="22px"></td>
			</tr>
        </table>
		</div>
		<%Else
			If strDoughType <> "-1" Then
			
			strlblDoughType =  GetSelectedDoughType(strDoughType)
			
		%>
		<table border="0"  style="font-family: Verdana; font-size: 12px;vertical-align:top;" cellspacing="0" cellpadding="0" >
			<!--<form method="post"name="frmReport">-->
            <tr>
                <td width="100%"  colspan='' height="40"><b>Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
            </tr>
            <!--</form>-->
            <tr>
                <td colspan=''>
                    <table border="0" width="100%" style="font-family: Verdana; font-size: 12px" cellspacing="1" cellpadding="3">  
                        <tr bgcolor="#FFFFFF" style="font-weight:bold" height="22">
                            <td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

		<%
            End IF
        %>
        <%'end if%>
        <%end if%>
        <%
        if IsArray(recarray) then erase recarray
        if IsArray(arrIName) then erase arrIName
        %>
        <!-- END OF MAIN DOUGH REPORT WITHOUT SECOND MIX CATEGORY -->
        
        <%
        ElseIf vReportType = "3" Then
        %>
        <!-- START OF TWO STAGE MIXING CATEGORY -->
        
        <%' if (Request("Search")<>"") then%>
        <%if isarray(recarray) then
        
			if IslineBreak(vReportType,UBound(arrIName),0) Then%>
			<!--<hr color="#CC3333" >-->
			
			<br class="page" />
			<!--<p style="page-break-before: always;">-->
			<%End If %>
        
        <table border="0"  style="font-family: Verdana; font-size: 12px;vertical-align:top;" cellspacing="0" cellpadding="2" >
        <%
            if strDoughType = "-1" then
                If isarray(arrProductCategory) then
        %>

        	<!--<form method="post"name="frmReport">-->
            <tr>
                <td width="100%"  colspan='<%=UBound(arrProductCategory) %>' height="40" style="font-size:14px"><b>Dough Name - <label id="lblDoughType"><%=vDoughTypearray(1,d)%></label></b></td>
            </tr>
       		<!--report type 3-->
        	<!--</form>-->

		<%
                End If
            Else
        %>
			<!--<form method="post"name="frmReport">-->
			<tr>
			<% 
				strlblDoughType =  GetSelectedDoughType(strDoughType)
				
			%>
			
 				<td width="100%"  colspan='<%=UBound(arrProductCategory) %>' height="40" style="font-size:14px"><b>Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
			</tr>
			<!--</form>-->

			<%
				End If ' End for [strDoughType = "-1"] check
				
				'Set objGeneral = Server.CreateObject("bakery.general")
				'dim objIndex
				'dim n, m, p
				'dim weight, arrProductCategoryTotal (50), arrProductCategoryTotal1 (50)
				
				if not isempty(arrIName) then
					objGeneral.BubbleSort(arrIName)
				end if
					
				if not isempty(arrProductCategory) then        
					objGeneral.BubbleSort(arrProductCategory)
				end if
			%>

				<tr>
					<td colspan='<%=UBound(arrProductCategory)%>'>
						<table border="1"  bordercolorlight="#CCCCCC" bordercolordark="#FFFFFF" style="font-family: Verdana; font-size: 12px" cellspacing="0" cellpadding="0" align="left" >
							<tr bgcolor="#CCCCCC" style="font-weight:bold" height="22">
								<td colspan="<%=ubound(arrIName) + 3%>">First mix</td>
							</tr>
							<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
							<%if isarray(arrIName) then%>
								<td width="<%=colWidth%>" bgcolor="#CCCCCC">&nbsp;</td>
								<%for m=0 to UBound(arrIName)
									arrProductCategoryTotal(m)=0
									arrProductCategoryTotal1(m)=0
								%>
	       						
								<%If m < NoOfColumnsPerLine then %>
								<td align="center" bgcolor="#CCCCCC" width="<%=colWidth%>"><%=arrIName(m)%></td>	
								<%End If %>
								
								<%next%>
								
								<%If m < NoOfColumnsPerLine then %>
								<td align="center" bgcolor="#CCCCCC" width="<%=TotcolWidth%>">Total Weight </td>	
    							<%End If %>
								
								<%
								else
									If strDoughType <> "-1" Then
								%>
								<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
								<%
									End If
								end if
								%>
							</tr>

							<%
							If isarray(arrProductCategory) Then
								strGrandTotalWeight1 = 0
								for n1=0 to UBound(arrProductCategory)
									strTotalWeight1 = 0
									
									for p1=0 to UBound(arrIName)
										weight1 = 0
										for c1=0 to UBound(recarray,2)
											if(recarray(3,c1)=arrIName(p1) and recarray(2,c1) = arrProductCategory(n1)) then
											   weight1 = recarray(4,c1)
											end if
										next
										strTotalWeight1 = cdbl(strTotalWeight1)+ cdbl(weight1) 
										arrProductCategoryTotal1(p1) = cdbl(arrProductCategoryTotal1(p1))+cdbl(weight1)
									next
								strGrandTotalWeight1 = strGrandTotalWeight1 + strTotalWeight1
								next
							End If
							%>

							<%if isarray(arrProductCategory) then%>

							<tr bgcolor="#CCCCCC" style="font-weight:bold">
								<td width="<%=TotcolWidth%>">Total</td>
								<%for m1=0 to UBound(arrIName)%>
								<%If m1 < NoOfColumnsPerLine then %>
									<td align="right" width="<%=colWidth%>"><%=formatnumber(arrProductCategoryTotal1(m1),3)%> Kg</td>
								<%End If %>
								<%next%>
								
								<%If m1 < NoOfColumnsPerLine then %>
								<td align="right" width="<%=TotcolWidth%>" style="font-size:14px"><%=formatnumber(strGrandTotalWeight1,3)%> Kg</td>							<%End If 
								
								nRecCount = nRecCount + 1
								
								%>
								
							</tr>

							<%
							strGrandTotalWeight=0
							for n=0 to UBound(arrProductCategory)
								if (n mod 2 =0) then
									strBgColour="#FFFFFF"
								else
									strBgColour="#F3F3F3"
								end if
							%>
	    					<tr bgcolor="<%=strBgColour%>">
								<td width="<%=colWidth%>"><%=arrProductCategory(n)%> </td>
    	    
							<%
							strTotalWeight=0
							
							for p=0 to UBound(arrIName)
								weight=0
								
								for c=0 to UBound(recarray,2)
									if(recarray(3,c)=arrIName(p) and recarray(2,c) = arrProductCategory(n)) then
									   weight = recarray(4,c)
									end if	            	
								next	           
									
							%>
							<%If p < NoOfColumnsPerLine then%>
								<td align="right" width="<%=colWidth%>"><%=formatnumber(weight,3)%> Kg</td>
							<%	
							
									if p = NoOfColumnsPerLine - 1 Then
										isTableBreak = true
									end if
								
							 else
										isTableBreak = true					
							End if 
									
							strTotalWeight=cdbl(strTotalWeight)+ cdbl(weight) 
							arrProductCategoryTotal(p)=cdbl(arrProductCategoryTotal(p))+cdbl(weight)
							next
							
							%>	
							<%If isTableBreak = false then %>    
    	   						<td align="right" style="font-weight:bold" width="<%=TotcolWidth%>"><%=formatnumber(strTotalWeight,3)%> Kg</td>
								<%End if %> 
	   						</tr>
		
							<%
							
							strGrandTotalWeight = strGrandTotalWeight + strTotalWeight
							nRecCount = nRecCount + 1
							
							next
							%>
							<%end if%>

					</table>
				</td>
			</tr>
			
			
			<% 'Drow second line for type =3 
			If isTableBreak = true then%>
			
			<tr><td height="22px"></td>	</tr>
			
			<tr>
					<td colspan='<%=UBound(arrProductCategory)%>'>
						<table border="1"  bordercolorlight="#CCCCCC" bordercolordark="#FFFFFF" style="font-family: Verdana; font-size: 12px;vertical-align:top;" cellspacing="0" cellpadding="0" align="left" >
							<tr bgcolor="#CCCCCC" style="font-weight:bold" height="22">
								<td colspan="<%=ubound(arrIName) + 3%>">First mix</td>
							</tr>
							<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
							<%if isarray(arrIName) then%>
								<td width="<%=colWidth%>" bgcolor="#CCCCCC">&nbsp;</td>
								<%for m=0 to UBound(arrIName)
									arrProductCategoryTotal(m)=0
									arrProductCategoryTotal1(m)=0
								%>
	       						
								<%If m >= NoOfColumnsPerLine then %>
								<td align="center" bgcolor="#CCCCCC" width="<%=colWidth%>"><%=arrIName(m)%></td>	
								<%End If %>
								
								<%next%>
								
								<%If m >= NoOfColumnsPerLine then %>
								<td align="center" bgcolor="#CCCCCC" width="<%=TotcolWidth%>">Total Weight </td>	
    							<%End If %>
								
								<%
								else
									If strDoughType <> "-1" Then
								%>
								<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
								<%
									End If
								end if
								%>
							</tr>

							<%
							If isarray(arrProductCategory) Then
								strGrandTotalWeight1 = 0
								for n1=0 to UBound(arrProductCategory)
									strTotalWeight1 = 0
									
									for p1=0 to UBound(arrIName)
										weight1 = 0
										for c1=0 to UBound(recarray,2)
											if(recarray(3,c1)=arrIName(p1) and recarray(2,c1) = arrProductCategory(n1)) then
											   weight1 = recarray(4,c1)
											end if
										next
										strTotalWeight1 = cdbl(strTotalWeight1)+ cdbl(weight1) 
										arrProductCategoryTotal1(p1) = cdbl(arrProductCategoryTotal1(p1))+cdbl(weight1)
									next
								strGrandTotalWeight1 = strGrandTotalWeight1 + strTotalWeight1
								next
							End If
							%>

							<%if isarray(arrProductCategory) then%>

							<tr bgcolor="#CCCCCC" style="font-weight:bold">
								<!--<td width="<%'=TotcolWidth%>">Total</td>-->
								<%for m1=NoOfColumnsPerLine to UBound(arrIName)%>
								<%If m1 >= NoOfColumnsPerLine then %>
									<td align="right" width="<%=colWidth%>"><%=formatnumber(arrProductCategoryTotal1(m1),3)%> Kg</td>
								<%End If %>
								<%next%>
								
								<%If m1 >= NoOfColumnsPerLine then %>
								<td align="right" width="<%=TotcolWidth%>" style="font-size:14px"><%=formatnumber(strGrandTotalWeight1,3)%> Kg</td>							<%End If 
								
								nRecCount = nRecCount + 1
								%>
							</tr>

							<%
							strGrandTotalWeight=0
							for n=0 to UBound(arrProductCategory)
								if (n mod 2 =0) then
									strBgColour="#FFFFFF"
								else
									strBgColour="#F3F3F3"
								end if
							%>
	    					<tr bgcolor="<%=strBgColour%>">
								<!--<td width="<%'=colWidth%>"><%'=arrProductCategory(n)%> </td>-->
    	    
							<%
							strTotalWeight=0
							
							for p=0 to UBound(arrIName)
								weight=0
								
								for c=0 to UBound(recarray,2)
									if(recarray(3,c)=arrIName(p) and recarray(2,c) = arrProductCategory(n)) then
									   weight = recarray(4,c)
									end if	            	
								next	           
									
							%>
							<%If p >= NoOfColumnsPerLine then%>
								<td align="right" width="<%=colWidth%>"><%=formatnumber(weight,3)%> Kg</td>
							<%	
							
									if UBound(arrIName) = p Then
										isTableBreak = false
									end if
								
							 else
										if UBound(arrIName) = p Then
											isTableBreak = false
										else
											isTableBreak = true	
										end if				
							End if 
									
							strTotalWeight=cdbl(strTotalWeight)+ cdbl(weight) 
							arrProductCategoryTotal(p)=cdbl(arrProductCategoryTotal(p))+cdbl(weight)
							next
							
							%>	
							<%If isTableBreak = false then %>    
    	   						<td align="right" style="font-weight:bold" width="<%=TotcolWidth%>"><%=formatnumber(strTotalWeight,3)%> Kg</td>
								<%End if %> 
	   						</tr>
		
							<%
							
							strGrandTotalWeight = strGrandTotalWeight + strTotalWeight
							nRecCount = nRecCount + 1
							
							next
							%>
							<%end if%>

					</table>
				</td>
			</tr>
			
			
			
			
			<%End If %> <!--End of type 3 second row -->
			
			<tr><td height="22px"></td>	</tr>
		</table>
		<%Else
			If strDoughType <> "-1" Then
			strlblDoughType =  GetSelectedDoughType(strDoughType)
		%>
		<table border="0"  style="font-family: Verdana; font-size: 12px; vertical-align:top;" cellspacing="0" cellpadding="2" >
		<!--<form method="post"name="frmReport">-->
			<tr>
  				<td width="100%"  colspan='' height="40" style="font-size:14px"><b>Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
			</tr>
		<!--</form>-->
			<tr>
				<td colspan=''>
					<table  border="0" width="100%" style="font-family: Verdana; font-size: 12px; vertical-align:top;" cellspacing="1" cellpadding="3" >  
						<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22">
							<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found for first mix.</font></p></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<%
			End IF
		%>
		<%end if%>
		
		<%
			'This is for second mix
			if isarray(recsecondmixarray) then
		%>
		
		<table border="0"  style="font-family: Verdana; font-size: 12px; vertical-align:top;" cellspacing="0" cellpadding="0" >
		<%
			Set objSecondMixGeneral = Server.CreateObject("bakery.general")
			objSecondMixGeneral.SetEnvironment(strconnection)
			'dim objIndex
			'dim n, m, p
			dim secondmixweight, arrSecondMixProductCategoryTotal (50), arrSecondMixProductCategoryTotal1 (50)
			
			if not isempty(arrSecondMixIName) then
				objSecondMixGeneral.BubbleSort(arrSecondMixIName)
			end if
				
			if not isempty(arrSecondMixProductCategory) then        
				objSecondMixGeneral.BubbleSort(arrSecondMixProductCategory)
			end if
			
			set objSecondMixGeneral = Nothing
		%>

			<tr>
				<td colspan='<%=UBound(arrSecondMixProductCategory) %>'>
					<table border="1"  bordercolorlight="#CCCCCC" bordercolordark="#FFFFFF"  style="font-family: Verdana; font-size: 12px;vertical-align:top;" cellspacing="0" cellpadding="0" align="left" >  
						<tr bgcolor="#CCCCCC" style="font-weight:bold" height="22"> 
							<td colspan="<%=ubound(arrSecondMixIName) + 3%>" align="left">Second mix</td>
						</tr>
						<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
						<%if isarray(arrSecondMixIName) then%>
							<td width="<%=colWidth%>" bgcolor="#CCCCCC">&nbsp;</td>
							<%for m=0 to UBound(arrSecondMixIName)
								arrSecondMixProductCategoryTotal(m)=0
								arrSecondMixProductCategoryTotal1(m)=0
							%>
	       					 <td align="center" bgcolor="#CCCCCC" width="<%=colWidth%>"><%=arrSecondMixIName(m)%></td>	
						<%next%>
							 <td align="center" bgcolor="#CCCCCC" width="<%=TotcolWidth%>">Total Weight </td>	
						<%
						else
							If strDoughType <> "-1" Then
						%>
							<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
						<%
							End If
						end if
						%>
						</tr>

						<%
						If isarray(arrSecondMixProductCategory) Then
							strSecondMixGrandTotalWeight1 = 0
							for n1=0 to UBound(arrSecondMixProductCategory)
								strSecondMixTotalWeight1 = 0
								
								for p1=0 to UBound(arrSecondMixIName)
									secondmixweight1 = 0
									for c1=0 to UBound(recsecondmixarray,2)
										if(recsecondmixarray(3,c1)=arrSecondMixIName(p1) and recsecondmixarray(2,c1) = arrSecondMixProductCategory(n1)) then
										   secondmixweight1 = recsecondmixarray(4,c1)
										end if
									next
									strSecondMixTotalWeight1 = cdbl(strSecondMixTotalWeight1)+ cdbl(secondmixweight1) 
									arrSecondMixProductCategoryTotal1(p1) = cdbl(arrSecondMixProductCategoryTotal1(p1))+cdbl(secondmixweight1)
								next
							strSecondMixGrandTotalWeight1 = strSecondMixGrandTotalWeight1 + strSecondMixTotalWeight1
							next
						End If
						%>
						
						<%if isarray(arrSecondMixProductCategory) then%>

						<tr bgcolor="#CCCCCC" style="font-weight:bold">
								<td width="<%=TotcolWidth%>">Total</td>
								<%for m1=0 to UBound(arrSecondMixIName)%>
								<td align="right" width="<%=colWidth%>"><%=formatnumber(arrSecondMixProductCategoryTotal1(m1),3) %> Kg</td>
								<%next%>
								<td align="right" width="<%=TotcolWidth%>" style="font-size:14px"><%=formatnumber(strSecondMixGrandTotalWeight1,3)%> Kg</td>
						</tr>

						<%
						strSecondMixGrandTotalWeight=0
						for n=0 to UBound(arrSecondMixProductCategory)
							if (n mod 2 =0) then
								strBgColour="#FFFFFF"
							else
								strBgColour="#F3F3F3"
							end if
						%>
	    				<tr bgcolor="<%=strBgColour%>">
	       					 <td><%=arrSecondMixProductCategory(n)%> </td>
    	    
							<%
							strSecondMixTotalWeight=0
							
							for p=0 to UBound(arrSecondMixIName)
								secondmixweight=0
								
								for c=0 to UBound(recsecondmixarray,2)
									if(recsecondmixarray(3,c)=arrSecondMixIName(p) and recsecondmixarray(2,c) = arrSecondMixProductCategory(n)) then
									   secondmixweight = recsecondmixarray(4,c)
									end if	            	
								next	           
									
							%>
	            			<td align="right"><%=formatnumber(secondmixweight,3)%> Kg</td>
							<%			
							strSecondMixTotalWeight=cdbl(strSecondMixTotalWeight)+ cdbl(secondmixweight) 
							arrSecondMixProductCategoryTotal(p)=cdbl(arrSecondMixProductCategoryTotal(p))+cdbl(secondmixweight)
							next
							
							%>	    
    	   					<td align="right" style="font-weight:bold"><%=formatnumber(strSecondMixTotalWeight,3)%> Kg</td> 
   					  </tr>
		
						<%
						
						strSecondMixGrandTotalWeight = strSecondMixGrandTotalWeight + strSecondMixTotalWeight
						nRecCount = nRecCount + 1
						'session("RecCount") = session("RecCount") + 1
						next
						%>
						<%end if%>		
					</table>
			  </td>
			</tr>
			<tr><td height="22px"></td>
			</tr>
		  </table>
			<%else
				if strDoughType <> "-1" then
			%>
			<table border="0"  style="font-family: Verdana; font-size: 12px; vertical-align:top;" cellspacing="0" cellpadding="2" >
				<tr>
					<td colspan=''>
						<table  border="0" width="100%" style="font-family: Verdana; font-size: 12px;vertical-align:top;" cellspacing="1" cellpadding="3">  
							<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22">
								<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found for second mix.</font></p></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td></td></tr>
			</table>
			<%
				end if
			%>
			<%end if' This is the end for second mix report%>
			
			<%'end if ' This is the end for Request("Search")%>
			<%
			if IsArray(recarray) then erase recarray
			if IsArray(arrIName) then erase arrIName
			if IsArray(recsecondmixarray) then erase recsecondmixarray
			if IsArray(arrSecondMixIName) then erase arrSecondMixIName
			%>
			
			<!-- END OF TWO STAGE MIXING CATEGORY -->
			
			<%
			ElseIf vReportType = "0" Then
			
			%>
			
			<!-- START OF MAIN DOUGH WITH SECOND MIX CATEGORY -->
			
			<%
			'if (Request("Search")<>"") then
			%>
			<%if isarray(recarray) then
			
			dim arrSMixSize
			
			if isarray(arrSecondMixIName) then
				arrSMixSize = UBound(arrSecondMixIName)
			else
			   	arrSMixSize = 0
			end if
			
			if IslineBreak(vReportType,UBound(arrIName),arrSMixSize) Then%>
			<!--<hr color="#CC3366">-->
			
			<br class="page" />
			<!--<p style="page-break-before: always;">-->
			<%End If%>
		<div style="width:100%">
			
	<table align="left" border="0" bgcolor="#FFFFFF"   style="font-family: Verdana; font-size: 12px;vertical-align:top;" cellspacing="0" cellpadding="0" >
				<%
				if strDoughType = "-1" then
				%>
				<tr >
  					<td width="100%" bgcolor="#FFFFFF" colspan='<%=UBound(arrProductCategory) +3%>' height="40" style="font-size:14px"><b>Dough Name - <label id="lblDoughType"><%=vDoughTypearray(1,d)%></label></b></td>
				</tr>
				<%
				else
				%>
				<tr>
 					 <td width="100%" bgcolor="#FFFFFF" colspan='<%=UBound(arrProductCategory) +3%>' height="40" style="font-size:14px"><b>Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
				</tr>
				<%
				end if
				%>
				<%
				'Set objGeneral = Server.CreateObject("bakery.general")
				'dim objIndex
				'dim n, m, p
				'dim weight, weight1, arrProductCategoryTotal (50), arrProductCategoryTotal1 (50)
				
				if not isempty(arrIName) then
					objGeneral.BubbleSort(arrIName)
				end if
					
				if not isempty(arrProductCategory) then        
					objGeneral.BubbleSort(arrProductCategory)
				end if
				
				set objGeneral = nothing
				%>

				<tr>
					<td colspan='<%=UBound(arrProductCategory) %>'>
						<table border="1"  bordercolorlight="#CCCCCC" bordercolordark="#FFFFFF"  style="font-family: Verdana; font-size: 12px;vertical-align:top;" cellspacing="0" cellpadding="0">  
				<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
   				 <%if isarray(arrIName) then%>
					<td width="<%=colWidth%>" bgcolor="#CCCCCC">&nbsp;</td>
				<%for m=0 to UBound(arrIName)
					arrProductCategoryTotal(m)=0
				%>
	      			  <%If m < NoOfColumnsPerLine then %>
					  <td align="center" bgcolor="#CCCCCC" width="<%=colWidth%>"><%=arrIName(m)%></td>	
					  <%End If %>
	    		<%next%>
				
				<%If m < NoOfColumnsPerLine then %>
					  <td align="center" bgcolor="#CCCCCC" width="<%=TotcolWidth%>">Total Weight</td>	
    			<%End If %>	
				
				<%else%>
					  <td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
				<%end if%>
				</tr>
	
				<%
				
				Call ClearArray(arrProductCategoryTotal1)
				
					If isarray(arrProductCategory) Then
						strGrandTotalWeight1 = 0
						for n1=0 to UBound(arrProductCategory)
							strTotalWeight1 = 0
							
							for p1=0 to UBound(arrIName)
								weight1 = 0
								for c1=0 to UBound(recarray,2)
									if(recarray(3,c1)=arrIName(p1) and recarray(2,c1) = arrProductCategory(n1)) then
									   weight1 = cdbl(weight1) + cdbl(recarray(4,c1))
									end if
								next
								strTotalWeight1 = cdbl(strTotalWeight1)+ cdbl(weight1) 
								arrProductCategoryTotal1(p1) = cdbl(arrProductCategoryTotal1(p1))+cdbl(weight1)
							next
						strGrandTotalWeight1 = strGrandTotalWeight1 + strTotalWeight1
						next
					End If
				%>
	
    			<%if isarray(arrProductCategory) then%>
	
				<tr bgcolor="#CCCCCC" style="font-weight:bold">
					<td width="<%=TotcolWidth%>">Total</td>
					<%for m1=0 to UBound(arrIName)%>
					<%If m1 < NoOfColumnsPerLine then %>
						<td align="right" width="<%=colWidth%>"><%=formatnumber(arrProductCategoryTotal1(m1),3)%> Kg</td>
					<%End If %>
					<%next%>
					
					<%If m1 < NoOfColumnsPerLine then %>
					<td align="right" width="<%=TotcolWidth%>" style="font-size:14px"><%=formatnumber(strGrandTotalWeight1,3)%> Kg</td>
					<%End If 
					nRecCount = nRecCount + 1
					%>
				</tr>
	
				<%
				
				strGrandTotalWeight=0
				for n=0 to UBound(arrProductCategory)
					if (n mod 2 =0) then
						strBgColour="#FFFFFF"
					else
						strBgColour="#F3F3F3"
					end if
				%>
				<tr bgcolor="<%=strBgColour%>">
					<td width="<%=colWidth%>"><%=arrProductCategory(n)%> </td>
						
						<%
						strTotalWeight=0
						
						for p=0 to UBound(arrIName)
							weight=0
							
							for c=0 to UBound(recarray,2)
								if(recarray(3,c)=arrIName(p) and recarray(2,c) = arrProductCategory(n)) then
								   weight = cdbl(weight) + cdbl(recarray(4,c))
								end if	            	
							next	           
								
						%>
	           	<%If p < NoOfColumnsPerLine then%>
				 <td align="right" width="<%=colWidth%>"><%=formatnumber(weight,3)%> Kg</td>
					<%			
					
								if p = NoOfColumnsPerLine - 1 Then
										isTableBreak = true
								end if
								
					else
										isTableBreak = true					
				    End if 
					
					strTotalWeight=cdbl(strTotalWeight)+ cdbl(weight) 
					arrProductCategoryTotal(p)=cdbl(arrProductCategoryTotal(p))+cdbl(weight)
					next
			

	        		%>	    
    	   		<%If isTableBreak = false then %>
				<td align="right" style="font-weight:bold" width="<%=TotcolWidth%>"><%=formatnumber(strTotalWeight,3)%> Kg</td> 
	    	
				<%End if %>
			</tr>
		
			<%
			
			strGrandTotalWeight=strGrandTotalWeight+strTotalWeight
			nRecCount = nRecCount + 1
			'session("RecCount") = session("RecCount") + 1
			next
			%>
			<%end if%>
			
			</table>
        </td>
        </tr>
		
		<% 'Drow second line for type =1 
			If isTableBreak = true then %>
		
		<tr>
		  <td height="22px"></td>
		</tr>	
		
		
						<tr>
					<td colspan='<%=UBound(arrProductCategory) %>'>
						<table border="1"  bordercolorlight="#CCCCCC" bordercolordark="#FFFFFF"  style="font-family: Verdana; font-size: 12px;vertical-align:top;" cellspacing="0" cellpadding="0">  
				<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
   				 <%if isarray(arrIName) then%>
					<!--<td width="<%'=colWidth%>" bgcolor="#CCCCCC">&nbsp;</td>-->
				<%for m=0 to UBound(arrIName)
					arrProductCategoryTotal(m)=0
				%>
	      			  <%If m >= NoOfColumnsPerLine then %>
					  <td align="center" bgcolor="#CCCCCC" width="<%=colWidth%>"><%=arrIName(m)%></td>	
					  <%End If %>
	    		<%next%>
				
				<%If m >= NoOfColumnsPerLine then %>
					  <td align="center" bgcolor="#CCCCCC" width="<%=TotcolWidth%>">Total Weight</td>	
    			<%End If %>	
				
				<%else%>
					  <td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
				<%end if%>
				</tr>
	
				<%
				
				Call ClearArray(arrProductCategoryTotal1)
				
					If isarray(arrProductCategory) Then
						strGrandTotalWeight1 = 0
						for n1=0 to UBound(arrProductCategory)
							strTotalWeight1 = 0
							
							for p1=0 to UBound(arrIName)
								weight1 = 0
								for c1=0 to UBound(recarray,2)
									if(recarray(3,c1)=arrIName(p1) and recarray(2,c1) = arrProductCategory(n1)) then
									   weight1 = cdbl(weight1) + cdbl(recarray(4,c1))
									end if
								next
								strTotalWeight1 = cdbl(strTotalWeight1)+ cdbl(weight1) 
								arrProductCategoryTotal1(p1) = cdbl(arrProductCategoryTotal1(p1))+cdbl(weight1)
							next
						strGrandTotalWeight1 = strGrandTotalWeight1 + strTotalWeight1
						next
					End If
				%>
	
    			<%if isarray(arrProductCategory) then%>
	
				<tr bgcolor="#CCCCCC" style="font-weight:bold">
					<!--<td width="<%'=TotcolWidth%>">Total</td>-->
					<%for m1=NoOfColumnsPerLine to UBound(arrIName)%>
					<%If m1 >= NoOfColumnsPerLine then %>
						<td align="right" width="<%=colWidth%>"><%=formatnumber(arrProductCategoryTotal1(m1),3)%> Kg</td>
					<%End If %>
					<%next%>
					
					<%If m1 >= NoOfColumnsPerLine then %>
					<td align="right" width="<%=TotcolWidth%>" style="font-size:14px"><%=formatnumber(strGrandTotalWeight1,3)%> Kg</td>
					<%End If 
					nRecCount = nRecCount + 1
					
					%>
				</tr>
	
				<%
				
				strGrandTotalWeight=0
				for n=0 to UBound(arrProductCategory)
					if (n mod 2 =0) then
						strBgColour="#FFFFFF"
					else
						strBgColour="#F3F3F3"
					end if
				%>
				<tr bgcolor="<%=strBgColour%>">
					<!--<td width="<%'=colWidth%>"><%'=arrProductCategory(n)%> </td>-->
						
						<%
						strTotalWeight=0
						
						for p=0 to UBound(arrIName)
							weight=0
							
							for c=0 to UBound(recarray,2)
								if(recarray(3,c)=arrIName(p) and recarray(2,c) = arrProductCategory(n)) then
								   weight = cdbl(weight) + cdbl(recarray(4,c))
								end if	            	
							next	           
								
						%>
	           		<%If p >= NoOfColumnsPerLine then%>
				 	<td align="right" width="<%=colWidth%>"><%=formatnumber(weight,3)%> Kg</td>
					<%			
					
						if UBound(arrIName) = p Then
								isTableBreak = false
						end if
								
					else
						if UBound(arrIName) = p Then
							isTableBreak = false
						else
							isTableBreak = true	
						end if			
				    End if 
					
					strTotalWeight=cdbl(strTotalWeight)+ cdbl(weight) 
					arrProductCategoryTotal(p)=cdbl(arrProductCategoryTotal(p))+cdbl(weight)
					next
			

	        		%>	    
    	   		<%If isTableBreak = false then %>
				<td align="right" style="font-weight:bold" width="<%=TotcolWidth%>"><%=formatnumber(strTotalWeight,3)%> Kg</td> 
	    	
				<%End if %>
			</tr>
		
			<%
			
			strGrandTotalWeight=strGrandTotalWeight+strTotalWeight
			nRecCount = nRecCount + 1
			'session("RecCount") = session("RecCount") + 1
			next
			%>
			<%end if%>
			
			</table>
        </td>
        </tr>
		
		 <%End If%>
		<tr>
		  <td height="22px"></td>
		</tr>
		
        <tr>
			<td >
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="vertical-align:top;">
					<tr>
					<% 
					
					if ubound(arrSecondMixDNameNew) > 0 Then
					
					nRecCount = nRecCount + (ubound(arrSecondMixDNameNew) * 4)
					
					end if
					 %>
					<td colspan='<%=UBound(arrProductCategory) %>'><!--#include file = "Includes/DoughSecondMixNew.asp"--></td>
					</tr>
				</table>
			</td>
           
        </tr>
	</table>
	
</div>
	<!--report type 0-->
	<%
	else
		If strDoughType <> "-1" Then
		strlblDoughType =  GetSelectedDoughType(strDoughType)
	%>
 <div>
	<table border="0" width="100px"  style="font-family: Verdana; font-size: 12px;vertical-align:top;" cellspacing="0" cellpadding="2" >
	  <tr>
  			<td width="100%"  colspan='' height="40"><b>Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
		</tr>
		<tr>
			<td colspan=''>
				<table  border="0" width="100%" style="font-family: Verdana; font-size: 12px;vertical-align:top;" cellspacing="1" cellpadding="3">  
					<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22">
						<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	<%
		end if
	%>
	<%end if ' This is end for isarray(recarray) check%>
	<%'end if ' This is end for (Request("Search")<>"") check%>
	<%
	if IsArray(recarray) then erase recarray
	%>
	
	<!-- END OF MAIN DOUGH WITH SECOND MIX CATEGORY -->
	
	<%
	End If 'This is the end of report for Report type checking
	
	next 'This is the end of the loop for different dough types
	
	
%>
<!--empty text-->
	</div>
	</td>
	</tr>
	
</table>

<table align="center" width="100%" border="0"  cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 12px; vertical-align:text-top;vertical-align:top;">

<tr><td>
	<%
	'If strDoughType = "-1" and Request("Search") <> "" Then
	If strDoughType = "-1" Then
		'If  IsDataAvailbale = 0 Then
		If  (IsDataAvailbale = 0) and (IsSecondMixDataAvailbale = 0) Then
	%>
	<div>
	<table border="0" width="100%"  style="font-family: Verdana; font-size: 12px;vertical-align:top;" cellspacing="0" cellpadding="2">
		<!--<form method="post"name="frmReport">-->
		<tr>
			<td width="100%"  height="40" style="padding-left:20px"><b>Dough Name - <label id="lblDoughType">All</label></b></td>
		</tr>
		<!--</form>-->
		<tr>
			<td style="padding-left:20px">
				<table  border="0" width="100%" style="font-family: Verdana; font-size: 12px; vertical-align:top;" cellspacing="1" cellpadding="3">  
					<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22">
						<td bgcolor="#FFFFFF" ><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	<%
		End If
	End If
	%>
	
	
	</td></tr>


</table>

<br /><br />
    <input name="hdnAction" type="hidden" value="<%=sAction%>">
    <input name="txtfrom" type="hidden" value="<%=fromdt%>">
    <input name="txtto" type="hidden" value="<%=todt%>">
    <input name="DoughType" type="hidden" value="<%=strDoughType%>">
    <input name="year" type="hidden" value="<%= Request.Form("year")%>">
    <input name="month" type="hidden" value="<%=Request.Form("month")%>">
    <input name="day" type="hidden" value="<%=Request.Form("day")%>">
    <input name="year1" type="hidden" value="<%=Request.Form("year1")%>">
    <input name="month1" type="hidden" value="<%=Request.Form("month1")%>">
    <input name="day1" type="hidden" value="<%=Request.Form("day1")%>">
	<input name="dtype" type="hidden" value="<%=vDoughType%>">
    <input name="facility" type="hidden" value="<%=vFacility%>">
	<input name="pageFrom" type="hidden" value="<%=Request.QueryString("pageFrom")%>">
	<input name="deldate" type="hidden" value="<%=Request.form("deldate")%>">
	<input name="DeliveryType" type="hidden" value="<%=strDeliveryType%>">
</form>
</body>
</html>
<%
'Breaking page 
Function IslineBreak(vReportType,arrSize,arrSecondMixSize)

	Dim nLinePerPage,nRemaningLine,lineBreak
	nLinePerPage = 32  '50
	nRemaningLine = 0
	lineBreak = false

	nRemaningLine = nLinePerPage - nRecCount

	if nRecCount >= nLinePerPage Then
	
		lineBreak = true
		 
	Else
		'type with second mix
		if nPrevoiusType <> "-1" and nPrevoiusType = "0" then
			lineBreak = true
		else
			if vReportType = "3" Then
				
				If nRemaningLine >= 20 Then
					
					if arrSize > NoOfColumnsPerLine Then
						lineBreak = true
					else 
						lineBreak = false
					end if
					
				else
					lineBreak = true
					
				End If	
			
			Elseif vReportType = "1" Then
				
				If nRemaningLine >= 8 Then
					
					if arrSize > NoOfColumnsPerLine Then
						
						if nRemaningLine >= 15 then
							lineBreak =  false
						else
							lineBreak = true
						end if	
					else 
						lineBreak = false
					end if	
						
				else
					lineBreak = true
					
				End If	
				
			Elseif vReportType = "0" Then
				'if second mix is available break the line
				
				if arrSecondMixSize > 0  Then
					lineBreak = true
				else
					lineBreak = false	
				
				end if
			End If
			
		End IF		
	
	End IF

nPrevoiusType = vReportType
if (lineBreak) Then
	nRecCount = 0
	
End If	

IslineBreak = lineBreak

End Function 


Sub ClearArray(arr)

	for p1=0 to UBound(arr)
	
	arr(p1) = cdbl(0)
	
	next
End Sub



Function FormatDate(strDate)
    Dim strYYYY
    Dim strMM
    Dim strDD

        strYYYY = CStr(DatePart("yyyy", strDate))

        strMM = CStr(DatePart("m", strDate))
        If Len(strMM) = 1 Then strMM = "0" & strMM

        strDD = CStr(DatePart("d", strDate))
        If Len(strDD) = 1 Then strDD = "0" & strDD

		 FormatDate = strYYYY & "-" & strMM & "-" & strDD
       ' FormatDate = strMM & "-" & strDD & "-" & strYYYY

End Function 


Function GetSelectedDoughType(selDoughID)

	for p=0 to UBound(vDoughTypearray,2)
	
		if(cint(vDoughTypearray(0,p)) = cint(selDoughID)) Then
		
			GetSelectedDoughType = vDoughTypearray(1,p)
			exit function
		else
			GetSelectedDoughType = ""
		end if
	
	next


End Function


%>