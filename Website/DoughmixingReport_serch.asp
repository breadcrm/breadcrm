<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<style type="text/css">
<!--
br.page{page-break-before: always; height:1px}
table{
font-family:Verdana, Arial;
font-size: 10px;
}
-->
</style>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--

function PrintThisPage() 
{ 
   var sOption="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
       sOption+="scrollbars=yes,width=750,height=550,left=100,top=25,resizable=1"; 

	document.getElementById('tbName').style.display = 'block';
	

   var sWinHTML = document.getElementById('contentstart').innerHTML; 
   
   var winprint=window.open("","",sOption); 
       winprint.document.open(); 
       winprint.document.write('<html>');
       winprint.document.write('<head>');
       winprint.document.write('<title>Bakery System</title>');
       //winprint.document.write('<LINK href=images/styles.css rel=Stylesheet>');
	   winprint.document.write('<style type="text/css">');
		winprint.document.write('<!--');
		winprint.document.write('br.page{page-break-before: always; height:1px}');
		winprint.document.write('table{');
		winprint.document.write('font-family:Verdana, Arial;');
		winprint.document.write('font-size: 10px;');
		winprint.document.write('}');
		winprint.document.write('-->');
		winprint.document.write('</style>');
       winprint.document.write('</head>');
       winprint.document.write('<body onload="window.print()">'); 
	  // winprint.document.write('Direct Dough Mixing Report');
 	
       winprint.document.write(sWinHTML);          
       winprint.document.write('</body></html>'); 
       winprint.document.close(); 
       winprint.focus(); 
}

function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}

	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function DoughTypeChanged(frm)
{
	var selIdx = frm.selectedIndex;
	var vDoughType = frm[selIdx].text;
	if(vDoughType != 'Select')
	{
		if (vDoughType != 'All')
		{
			if (document.getElementById("lblDoughType")!=null)
				document.getElementById('lblDoughType').innerHTML = vDoughType;
		}
	}
	else
		if (document.getElementById("lblDoughType")!=null)
			lblDoughType.value = "";
}

function CheckSearch(frm)
{
	if (frm.DoughType.value=="")
	{
		alert("Please select a Dough Name");
		frm.DoughType.focus();
		return  false;
	}
	
	
	return  true;
}




//-->
</SCRIPT>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal();DoughTypeChanged(document.form.DoughType)">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%

dim objBakery
dim retcol
dim fromdt, todt , i
Dim TotQuantity,TotTurnover
dim arrProductCategory
dim arrSecondMixProductCategory
dim vDoughType
dim nRecCount 

'from Scenario-2
dim retsecondmixcol

'from Scenario-3
dim recsecondmixarrayNew
dim retsecondmixcolNew

fromdt = Request.form("txtfrom")
strlblDoughType=request("lblDoughType")
todt = Request.Form("txtto")
strDoughType = Request.Form("DoughType")
vDoughType = Request.QueryString("dtype")
vFacility = Request.QueryString("facility")

if vReportCaregoty = "" then
	vReportCaregoty = "0"
end if

If vDoughType = "" Then
	vDoughType = 1
End If

if (strDoughType = "") then
	strDoughType = "-1"
end if

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
   todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayDoughNameDoughMixingReport(vDoughType)
vDoughTypearray =  detail("DoughName")

set detail = Nothing
%>

<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
 <tr>
    <td width="100%"><b><font size="3"><%if vDoughType = 1 then if vFacility = "0" then response.Write("Direct") else response.Write("Stanmore English") end if else response.Write("48 Hours") end if%> Dough Mixing Report<br>
      &nbsp;</font></b>
	  <form method="post" action="DoughMixingReportNew1.asp?dtype=<%=vDoughType%>&facility=<%=vFacility%>" name="form" onSubmit="return CheckSearch(this)" target="_blank">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="100%">
        <tr  >
		 <td width="130"><b>Dough Name:&nbsp;</b></td>
          <td colspan="3">
		  	
			<select size="1" name="DoughType" style="font-family: Verdana; font-size: 8pt" <%If strDoughType <> "-1" Then response.Write("onChange=""DoughTypeChanged(this)""")%>>
			<option value = "">Select</option>
			<option value = "-1" <%if strDoughType = "-1" then response.write "Selected" %> >All</option>
			<%
			if IsArray(vDoughTypearray) Then  	
				for i = 0 to ubound(vDoughTypearray,2)%>  				
			 <option value="<%=vDoughTypearray(0,i)%>" <%if strDoughType<>"" then%><%if cint(strDoughType)= vDoughTypearray(0,i) then response.write " Selected"%><%end if%>><%=vDoughTypearray(1,i)%></option>
			  <%
			   next                 
			End if
			%>   
		  	</select>		  </td>
		  <td></td>
         
        </tr>
		<tr height="25">          
			  <td><b>Delivery Type:&nbsp;</b></td>
			  <td>
				<select size="1" name="DeliveryType" style="font-family: Verdana; font-size: 8pt">
					<option value="0">All</option>
					<option value="1" <%if strDeliveryType<>"" then%><%if cint(strDeliveryType)= 1 then response.write " Selected"%><%end if%>>Morning + Noon</option>
					<option value="3" <%if strDeliveryType<>"" then%><%if cint(strDeliveryType)= 3 then response.write " Selected"%><%end if%>>Evening</option>
			  	</select>			  </td>
			   <td></td> <td></td> <td></td>
		</tr>
		<tr>
          <td><b>From:</b></td>
          <td height="18" width="350">
			<%
				dim arrayMonth
				arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
			%>
			<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
			  <%for i=1 to 31%>
				<option value="<%=i%>"><%=i%></option>
			  <%next%>
			</select>
			<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
			   <%for i=0 to 11%>
				<option value="<%=i%>"><%=arrayMonth(i)%></option>
			  <%next%>
			</select>
			
			<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
			  <%for i=2000 to Year(Date)+1%>
				<option value="<%=i%>"><%=i%></option>
			  <%next%>
			</select>
			<INPUT class=Datetxt id=txtfrom onFocus="blur();" size="12"  name="txtfrom" value="<%=fromdt%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
			<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">		  </td>
			<td><b>To:</b></td>
			<td height="18" width="350">
				<select name="day1" onChange="setCal1(this.form,document.form.txtto)">
				<%for i=1 to 31%>
				<option value="<%=i%>"><%=i%></option>
				<%next%>
				</select>
				<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
				<%for i=0 to 11%>
				<option value="<%=i%>"><%=arrayMonth(i)%></option>
				<%next%>
				</select>
				
				<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
				<%for i=2000 to Year(Date)+1%>
				<option value="<%=i%>"><%=i%></option>
				<%next%>
				</select>
				<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=todt%>" onChange="setCalCombo1(this.form,document.form.txtto)">
				<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">			</td>
			<td>
				<input type="submit" value="View Report" name="Search"  style="font-family: Verdana; font-size: 8pt"  ></td>
		</tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
	  </form>
	  
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>



<br><br>
</body>
</html>


