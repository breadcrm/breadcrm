﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
//using System.Data.Odbc;
using System.Configuration;
using System.Data.SqlClient;

namespace CustomerAccountStatementPDF
{
    class ProcessTransaction
    {
       
        string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
       

         SqlConnection conn;
        private string strSql = string.Empty;
        string message = string.Empty;

        /// <summary>
        /// This method is used to List all customers
        /// </summary>
        public DataTable ListAllCustomerDetails()
        {
            conn = new SqlConnection(strConnectionString);
            DataTable dtCustomer = new DataTable();

            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                strSql = "exec spGetCustomersForAccountStatement";

                SqlDataAdapter daCustomer = new SqlDataAdapter(strSql, conn);
                dtCustomer = new DataTable();
                daCustomer.Fill(dtCustomer);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                conn.Close();
            }


            return dtCustomer;
        }

        /// <summary>
        /// This method is used to get the customer statement report
        /// </summary>
        /// <param name="nCustNo"></param>
        /// <returns></returns>
        public DataTable GetCustomerStatement(int nCustNo, string startDate, string endDate)
        {
            conn = new SqlConnection(strConnectionString);
            DataTable dtstatement = null;

            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                strSql = "exec spGetCustomerStatement " + nCustNo + ",'" + startDate + "','" + endDate + "'";

                SqlDataAdapter dastatement = new SqlDataAdapter(strSql, conn);
                dtstatement = new DataTable();
                dastatement.Fill(dtstatement);
            }
            catch (Exception ex)
            {
                message = ex.Message;
              
            }
            finally
            {
                conn.Close();
            }


            return dtstatement;
        }

        /// <summary>
        /// This method is used to insert record to Account Statement History table - for a group insert only one record
        /// </summary>
        /// <param name="nCustNo"></param>
        /// <param name="nGroupNo"></param>
        /// <param name="totAmount"></param>
        /// <param name="isGroup">group=1, individual = 0</param>
        /// <param name="sFileName"></param>
        public string InsertAccountStatementHistory(int nCustNo, int nGroupNo, decimal totAmount, int isGroup, string sFileName)
        {
           // conn = new OdbcConnection(strConnectionString);
            string msg = string.Empty;
            conn = new SqlConnection(strConnectionString);
            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                SqlCommand cmd = new SqlCommand("exec spInsertAccountStatementHistory " + nCustNo + "," + nGroupNo + ",'" + DateTime.Now.ToString("yyyy-MM-dd") + "'," + totAmount + "," + isGroup + ",'" + sFileName + "'", conn);
                cmd.ExecuteNonQuery();
                msg = "Successfull DB Insert";
            }
            catch (Exception ex)
            {
                message = ex.Message;
                msg = message;
            }
            finally
            {
                conn.Close();
            }

            return msg;
        }


        /// <summary>
        /// This method is used to Get the current record count of AccountStatementHistory Table
        /// </summary>
        /// <returns></returns>
        public DataTable GetAccountStatementHistoryRecCount()
        {
           // conn = new OdbcConnection(strConnectionString);
            conn = new SqlConnection(strConnectionString);
            DataTable dtReport = null;

            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                strSql = "Select * From dbo.AccountStatementHistory";

                SqlDataAdapter daReportt = new SqlDataAdapter(strSql, conn);
                dtReport = new DataTable();
                daReportt.Fill(dtReport);

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                conn.Close();
            }


            return dtReport;
        }

        /// <summary>
        /// This method is used to Get last current record number from  AccountStatementHistory Table
        /// </summary>
        /// <returns></returns>
        public DataTable GetReportNumber()
        {
            //conn = new OdbcConnection(strConnectionString);
            conn = new SqlConnection(strConnectionString);
            DataTable dtReport = null;

            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                strSql = "SELECT IDENT_CURRENT ('AccountStatementHistory')";

                SqlDataAdapter daReportt = new SqlDataAdapter(strSql, conn);
                dtReport = new DataTable();
                daReportt.Fill(dtReport);

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                conn.Close();
            }


            return dtReport;
        }

        /// <summary>
        /// This method is used to get the Group email address by using GNo
        /// </summary>
        /// <param name="nGNo"></param>
        /// <returns></returns>
        public DataTable GetGroupMasterEmailbyGNo(int nGNo)
        {
            conn = new SqlConnection(strConnectionString);
            DataTable dtGroup = null;

            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                strSql = "exec spGetGroupEmailAddressByGNo " + nGNo;

                SqlDataAdapter daGroup = new SqlDataAdapter(strSql, conn);
                dtGroup = new DataTable();
                daGroup.Fill(dtGroup);
            }
            catch (Exception ex)
            {
                message = ex.Message;

            }
            finally
            {
                conn.Close();
            }


            return dtGroup;
        }



    }
}
