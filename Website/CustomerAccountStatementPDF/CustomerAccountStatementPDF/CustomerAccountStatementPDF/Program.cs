﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace CustomerAccountStatementPDF
{
    class Program
    {
        static void Main(string[] args)
        {
            SendCustomerAccountStatement();
          // testMethod();

        }


        static void SendCustomerAccountStatement()
        {
            int nGroupNo = 0;
            int isReportCreated = 0;
            ProcessTransaction oTrans = new ProcessTransaction();
            try
            {
                
                DataTable dtAllCustomerList = oTrans.ListAllCustomerDetails();
                if (dtAllCustomerList.Rows.Count > 0)
                {
                    PDFReportGenerator oReport = new PDFReportGenerator();
                    foreach (DataRow dtCust in dtAllCustomerList.Rows)
                    {
                        if (dtCust["GNo"].ToString() == "0") //Individual customer report
                        {
                            isReportCreated = oReport.GeneratePDFReport(false, int.Parse(dtCust["CNo"].ToString()), 0, dtCust["Startdate"].ToString(), dtCust["EndDate"].ToString());

                        }
                        else if (dtCust["GNo"].ToString() != "0") //customer Group report
                        {
                            if (nGroupNo != int.Parse(dtCust["GNo"].ToString()) || nGroupNo == 0)
                            {
                                nGroupNo = int.Parse(dtCust["GNo"].ToString());
                                isReportCreated = oReport.GeneratePDFReport(true, 0, int.Parse(dtCust["GNo"].ToString()), dtCust["Startdate"].ToString(), dtCust["EndDate"].ToString());

                            }
                            else if (nGroupNo == int.Parse(dtCust["GNo"].ToString()) && isReportCreated != 0)
                            {
                                isReportCreated = 2;
                            }
                        }

                        //DataTable dtReportNo = oTrans.GetReportNumber();
                        if (isReportCreated == 1)
                        {
                            Console.WriteLine("Customer Statement created - Report No :" + oReport.m_nReportNo);

                        }
                        else if (isReportCreated == 0)
                        {
                            Console.WriteLine("No Record for Generate report");
                        }
                        else if (isReportCreated == 2)
                        {
                            Console.WriteLine("This Report has already created");
                        }
                    }
                }
                
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("1" + ex.Message);
            }
        }

        //test method
        static void testMethod()
        {

            PDFReportGenerator oReport = new PDFReportGenerator();
            ProcessTransaction oTrans = new ProcessTransaction();
          //if (oReport.GeneratePDFReport(true, 1670, 66, "2010-02-10", "2010-03-10") == 1)
            if (oReport.GeneratePDFReport(true, 1086, 279, "2012-04-09", "2012-04-16") == 1)
           // if (oReport.GeneratePDFReport(false, 900, 0, "2010-03-01", "2010-03-18") == 1)
            {
                Console.WriteLine("Customer Statement created - Report No :" + oReport.m_nReportNo);
            }
            else
            {
                Console.WriteLine("ERROR IN REPORT GENERATION");

            }
        }
    }
}
