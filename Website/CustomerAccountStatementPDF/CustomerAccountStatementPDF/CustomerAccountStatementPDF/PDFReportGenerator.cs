﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Net.Mail;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Threading;

namespace CustomerAccountStatementPDF
{
    class PDFReportGenerator
    {
        public int m_nReportNo = 0;
        public string message = string.Empty;
        public static string m_sFileName = string.Empty;
        public int m_nMargin = 20;
        public string fontname = "Monotype Corsiva";
        public float fsize = 10;

        public int GeneratePDFReport(bool isCustomerGroup, int nCustomerNo, int nCustGroupNo, string sStartDate, string sEndDate)
        {
            string sPath = string.Empty;
            int isReportcreated = 0;
            int isGroup;
            string sFilePath = string.Empty;

            try
            {
                Document pdfdocument = new Document(iTextSharp.text.PageSize.A4, m_nMargin, m_nMargin, m_nMargin, m_nMargin);
                iTextSharp.text.pdf.PdfWriter writer;
                iTextSharp.text.pdf.PdfPTable pdfptable;
                iTextSharp.text.pdf.PdfPTable pdfptableHeader;
                iTextSharp.text.pdf.PdfPTable pdfptableCustomerAddress;
                iTextSharp.text.pdf.PdfPTable pdfptableFooter;
                iTextSharp.text.pdf.PdfPTable pdfptablePageFooter;


                pdfptableHeader = new iTextSharp.text.pdf.PdfPTable(3);
                pdfptable = new iTextSharp.text.pdf.PdfPTable(3);
                pdfptableFooter = new iTextSharp.text.pdf.PdfPTable(1);
                pdfptableCustomerAddress = new iTextSharp.text.pdf.PdfPTable(1);
                pdfptablePageFooter = new iTextSharp.text.pdf.PdfPTable(1);

                sPath = ConfigurationSettings.AppSettings["ReportPath"];

                m_nReportNo = GenerateReportNumber();

                isGroup = isCustomerGroup ? 1 : 0;
                //File Name = Acc_stmt_ReportNumber_1/0 (if group then 1 else 0)
                m_sFileName = "Acc_stmt_" + m_nReportNo + "_" + isGroup + ".pdf";
                sFilePath = sPath + m_sFileName;

                writer = PdfWriter.GetInstance(pdfdocument, new FileStream(sFilePath, FileMode.Create));

                CreatepageFooter(pdfptablePageFooter, pdfdocument);

                pdfdocument.Open();
                pdfdocument.NewPage();

                

                if (DisplayAccountStatementDetails(isCustomerGroup, nCustomerNo, nCustGroupNo, sStartDate, sEndDate, pdfdocument, pdfptable, pdfptableHeader, pdfptableFooter, pdfptableCustomerAddress, pdfptablePageFooter))
                {
                    //Console.WriteLine("report created Successfully");
                    // After Generating the file send the statement to customer
                    SendCustomerAccountStatement(isCustomerGroup, nCustomerNo, nCustGroupNo, m_sFileName, m_nReportNo);
                   
                    isReportcreated = 1;
                }
                else
                    isReportcreated = 0;

            }
            catch (Exception ex)
            {
                message = ex.Message;
                Console.WriteLine(message);
            }

            return isReportcreated;
        }


         /// <summary>
        /// This method is used to Drow the customer Account Staement
        /// </summary>
        /// <param name="isCutGroup"></param>
        /// <param name="custNo"></param>
        /// <param name="workSheet"></param>
        /// <param name="range"></param>
        private bool DisplayAccountStatementDetails(bool isCutGroup, int custNo, int custGroupNo, string sStartDate, string sEndDate, Document pdfdocument, PdfPTable pdfptable, PdfPTable pdfptableHeader, PdfPTable pdfptableFooter, PdfPTable pdfptableCustAdd, PdfPTable pdfptablePageFooter)
        {
            float totAmount = 0.0f;
            float GrandTotal = 0.0f;
            ProcessTransaction oTrans = new ProcessTransaction();
            DataTable dtCustomerStatement = null;
            int nCount = 0;
            string sAmount = string.Empty;
            string sMsg = string.Empty;
            string strImgPath = string.Empty;
            try
            {

                DataRow[] groupCustomer = null;
                DataRow[] individualCustomer = null;
                DataTable dtCustomerList = oTrans.ListAllCustomerDetails();

                //Display customer Group report
                if (isCutGroup)
                {
                    if (custGroupNo != 0)
                    {
                        groupCustomer = dtCustomerList.Select("GNo = " + custGroupNo);

                        if (groupCustomer[0]["ResellerID"].ToString() == "0")
                            strImgPath = ConfigurationSettings.AppSettings["BreadFactoryLogo"].ToString();
                        else if (groupCustomer[0]["ResellerID"].ToString() == "1")
                            strImgPath = ConfigurationSettings.AppSettings["FlourStationLogo"].ToString();

                        //This is used to display the report title
                        GenerateReportTitle(pdfptableHeader, ConfigurationSettings.AppSettings["ReportTitle"].ToString());

                        
                        //This is used to diaplay the logo
                        GenerateLogoRow(pdfptableHeader, strImgPath, pdfptableCustAdd, groupCustomer[0]["Address1"].ToString(), groupCustomer[0]["Address2"].ToString(), groupCustomer[0]["Address3"].ToString(), groupCustomer[0]["Town"].ToString(), groupCustomer[0]["PostCode"].ToString());

                      //  CustomerAddressRow(pdfptableCustAdd, groupCustomer[0]["Address1"].ToString(), groupCustomer[0]["Address2"].ToString(), groupCustomer[0]["Address3"].ToString(), groupCustomer[0]["Town"].ToString(), groupCustomer[0]["PostCode"].ToString());    

                        GenerateReportHeader(pdfdocument, pdfptable, isCutGroup, m_nReportNo, string.Empty, string.Empty, groupCustomer[0]["GName"].ToString(), DateTime.Parse(groupCustomer[0]["StartDate"].ToString()).ToString("dd/MM/yyyy"), DateTime.Parse(groupCustomer[0]["EndDate"].ToString()).ToString("dd/MM/yyyy"));

                       // GenerateTitleRow(pdfptable, pdfdocument);

                        
                        foreach (DataRow dtRowGroup in groupCustomer)
                        {
                            try
                            {
                                dtCustomerStatement = oTrans.GetCustomerStatement(int.Parse(dtRowGroup["CNo"].ToString()), DateTime.Parse(dtRowGroup["StartDate"].ToString()).ToString("MM/dd/yyyy"), DateTime.Parse(dtRowGroup["EndDate"].ToString()).ToString("MM/dd/yyyy"));

                                if (dtCustomerStatement.Rows.Count > 0)
                                {

                                    //Display customer name and code.
                                    GenerateCustomerdetailsRow(pdfdocument, pdfptable, dtRowGroup["CName"].ToString(), dtRowGroup["CNo"].ToString());                                //nStartRow++;
                                    GenerateTitleRow(pdfptable, pdfdocument);

                                    foreach (DataRow dtRow in dtCustomerStatement.Rows)
                                    {

                                        if (dtRow["Amount"].ToString().StartsWith("-"))
                                        {
                                            sAmount = "(" + float.Parse(dtRow["Amount"].ToString()).ToString("0.00") + ")";
                                            totAmount = totAmount + float.Parse(dtRow["Amount"].ToString());
                                        }
                                        else
                                        {
                                            sAmount = float.Parse(dtRow["Amount"].ToString()).ToString("0.00");
                                            totAmount = totAmount + float.Parse(dtRow["Amount"].ToString());
                                        }

                                        GenerateDataRow(pdfdocument, pdfptable, dtRow["OrdNo"].ToString(), dtRow["OrderDate"].ToString(), sAmount);
                                    }

                                    GenerateTotalColumn(pdfdocument, pdfptable, totAmount, ConfigurationSettings.AppSettings["TotalText"].ToString());

                                    GrandTotal = GrandTotal + totAmount;
                                    totAmount = 0.0f;
                                    nCount++;
                                }
                                
                            }
                            catch (Exception ex)
                            {
                                message = ex.Message;
                                Console.WriteLine(message);
                            }
                        }

                        if (nCount > 0)
                        {
                            //This is used to display the grand total column
                            GenerateTotalColumn(pdfdocument, pdfptable,GrandTotal, ConfigurationSettings.AppSettings["GrandTotalText"].ToString());

                            //Generate report footer to display the address
                            GenerateReportFooter(pdfptableFooter, groupCustomer[0]["ResellerID"].ToString());
                           // CreatepageFooter(pdfptablePageFooter, pdfdocument);
                            try
                            {
                                //Insert Record to history table
                                sMsg = InsertHistoryTableRecord(0, custGroupNo, decimal.Parse(GrandTotal.ToString()), 1, m_sFileName);
                               // Console.WriteLine(sMsg);
                            
                            }
                            catch (Exception ex)
                            {
                                message = ex.Message;
                                Console.WriteLine(message);
                            }
                        }
                    }
                }
                else //Display single customer report
                {
                    //This is used to display the report title
                    GenerateReportTitle(pdfptableHeader, ConfigurationSettings.AppSettings["ReportTitle"].ToString());
                    
                   
                    individualCustomer = dtCustomerList.Select("CNo = " + custNo);

                   if(individualCustomer[0]["ResellerID"].ToString() == "0")
                       strImgPath = ConfigurationSettings.AppSettings["BreadFactoryLogo"].ToString();
                   else if(individualCustomer[0]["ResellerID"].ToString() == "1")
                       strImgPath = ConfigurationSettings.AppSettings["FlourStationLogo"].ToString();

                    //This is used to diaplay the logo
                   GenerateLogoRow(pdfptableHeader, strImgPath, pdfptableCustAdd, individualCustomer[0]["Address1"].ToString(), individualCustomer[0]["Address2"].ToString(), individualCustomer[0]["Address3"].ToString(), individualCustomer[0]["Town"].ToString(), individualCustomer[0]["PostCode"].ToString());

                  // CustomerAddressRow(pdfptableCustAdd, individualCustomer[0]["Address1"].ToString(), individualCustomer[0]["Address2"].ToString(), individualCustomer[0]["Address3"].ToString(), individualCustomer[0]["Town"].ToString(), individualCustomer[0]["PostCode"].ToString());    

                    ////Display header
                    GenerateReportHeader(pdfdocument, pdfptable, isCutGroup, m_nReportNo, custNo.ToString(), individualCustomer[0]["CName"].ToString(), string.Empty, DateTime.Parse(sStartDate.ToString()).ToString("dd/MM/yyyy"), DateTime.Parse(sEndDate.ToString()).ToString("dd/MM/yyyy"));

                    // GenerateReportHeader(pdfptable, pdfdocument);
                    GenerateTitleRow(pdfptable, pdfdocument);


                    //nDetailStart = nStartRow;
                    dtCustomerStatement = oTrans.GetCustomerStatement(custNo, DateTime.Parse(sStartDate.ToString()).ToString("MM/dd/yyyy"), DateTime.Parse(sEndDate.ToString()).ToString("MM/dd/yyyy"));

                    if (dtCustomerStatement.Rows.Count > 0)
                    {
                            foreach (DataRow dtRow in dtCustomerStatement.Rows)
                            {
                                 sAmount = string.Empty;

                                if (dtRow["Amount"].ToString().StartsWith("-"))
                                {
                                    sAmount = "(" + float.Parse(dtRow["Amount"].ToString()).ToString("0.00") + ")";
                                    totAmount = totAmount + float.Parse(dtRow["Amount"].ToString());
                                }
                                else
                                {
                                    sAmount = float.Parse(dtRow["Amount"].ToString()).ToString("0.00");
                                    totAmount = totAmount + float.Parse(dtRow["Amount"].ToString());
                                }

                                GenerateDataRow(pdfdocument, pdfptable, dtRow["OrdNo"].ToString(), dtRow["OrderDate"].ToString(), sAmount);
                            }

                            GenerateTotalColumn(pdfdocument, pdfptable,totAmount, ConfigurationSettings.AppSettings["TotalText"].ToString());

                            try
                            {
                                //Insert Record to history table
                                sMsg = InsertHistoryTableRecord(custNo, 0, decimal.Parse(totAmount.ToString()), 0, m_sFileName);
                              //  Console.WriteLine(sMsg);
                            }
                            catch (Exception ex)
                            {
                                message = ex.Message;
                                Console.WriteLine(message);
                            }

                           nCount++;
                    }

                    GenerateReportFooter(pdfptableFooter, individualCustomer[0]["ResellerID"].ToString());
                    


                }

               // CreatepageFooter(pdfptablePageFooter, pdfdocument);

                pdfdocument.Add(pdfptableHeader);
               // pdfdocument.Add(pdfptableCustAdd);
                pdfdocument.Add(pdfptable);
                pdfdocument.Add(pdfptableFooter);
                pdfdocument.Add(pdfptablePageFooter);
                pdfdocument.Close();
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Console.WriteLine(message);
            }
            if (nCount > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// This method is used to Generate the report header with Customer Details
        /// </summary>
        /// <param name="pdfdocument"></param>
        /// <param name="pdfptable"></param>
        /// <param name="isCustomerGroup"></param>
        /// <param name="nReportNumber"></param>
        /// <param name="nCustNo"></param>
        /// <param name="sCustName"></param>
        /// <param name="sGroupName"></param>
        /// <param name="sStartDate"></param>
        /// <param name="sEndDate"></param>
        private void GenerateReportHeader(Document pdfdocument,PdfPTable pdfptable,bool isCustomerGroup, int nReportNumber, string nCustNo, string sCustName, string sGroupName, string sStartDate, string sEndDate)
        {
            try
            {
                string sHeaderText = string.Empty;
                PdfPCell cell;

                if (isCustomerGroup)
                    sHeaderText = ConfigurationSettings.AppSettings["TableHeader7"] + DateTime.Now.ToString("dd/MM/yyyy") + "\n\n" + ConfigurationSettings.AppSettings["TableHeader1"] + nReportNumber + "\n\n" + ConfigurationSettings.AppSettings["TableHeader6"] + sGroupName + "\n\n" + ConfigurationSettings.AppSettings["TableHeader4"] + sStartDate + "          " + ConfigurationSettings.AppSettings["TableHeader5"] + sEndDate;
                else
                    sHeaderText = ConfigurationSettings.AppSettings["TableHeader7"] + DateTime.Now.ToString("dd/MM/yyyy") + "\n\n" + ConfigurationSettings.AppSettings["TableHeader1"] + nReportNumber + "\n\n" + ConfigurationSettings.AppSettings["TableHeader2"] + nCustNo + "             " + ConfigurationSettings.AppSettings["TableHeader3"] + sCustName + "\n\n" + ConfigurationSettings.AppSettings["TableHeader4"] + sStartDate + "           " + ConfigurationSettings.AppSettings["TableHeader5"] + sEndDate;

                cell = GetCell(sHeaderText, Color.BLACK, 1, 8);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.FixedHeight = 50.0f;
                cell.Colspan = 3;
                                
                cell.BackgroundColor = Color.WHITE;
                pdfptable.AddCell(cell);

            }
            catch (Exception ex)
            {
                message = ex.Message;
                Console.WriteLine(message);
            }
        }

        /// <summary>
        /// This method is used to drow customer details row for the Group staement report
        /// </summary>
        /// <param name="pdfdocument"></param>
        /// <param name="pdfptable"></param>
        /// <param name="sCustName"></param>
        /// <param name="sCustNo"></param>
        private void GenerateCustomerdetailsRow(Document pdfdocument, PdfPTable pdfptable, string sCustName, string sCustNo)
        {
            try
            {
                string sHeaderText = string.Empty;
                PdfPCell cell;

                sHeaderText = ConfigurationSettings.AppSettings["TableHeader2"] + sCustNo + "                 " + ConfigurationSettings.AppSettings["TableHeader3"] + sCustName;

                cell = GetCell(sHeaderText, Color.BLACK, 1, 8);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;
                cell.FixedHeight = 20.0f;
                cell.Colspan = 3;

                cell.BackgroundColor = Color.WHITE;
                pdfptable.AddCell(cell);

            }
            catch (Exception ex)
            {
                message = ex.Message;
                Console.WriteLine(message);
            }
        }

        /// <summary>
        /// This method is used to generate the title bar and format the column width
        /// </summary>
        /// <param name="pdfptable"></param>
        /// <param name="pdfdocument"></param>
        private void GenerateTitleRow(PdfPTable pdfptable, Document pdfdocument)
        {
            try
            {
                PdfPCell cell;
               
                // Color headColor = new Color(204, 204, 204);

                cell = GetCell(ConfigurationSettings.AppSettings["TableTitle1"], Color.BLACK, 1, 8);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.Colspan = 1;
                cell.BackgroundColor = Color.WHITE;
                pdfptable.AddCell(cell);

                cell = GetCell(ConfigurationSettings.AppSettings["TableTitle2"], Color.BLACK, 1, 8);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.Colspan = 1;
                cell.BackgroundColor = Color.WHITE;
                pdfptable.AddCell(cell);

                cell = GetCell(ConfigurationSettings.AppSettings["TableTitle3"], Color.BLACK, 1, 8);
                cell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
                cell.Colspan = 1;
                cell.BackgroundColor = Color.WHITE;

                cell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;
                cell.FixedHeight = 15.0f;
                pdfptable.AddCell(cell);

               
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Console.WriteLine(message);
            }
          
        }

       /// <summary>
       /// This method is used to Generate the Data Row
       /// </summary>
       /// <param name="pdfptable"></param>
       /// <param name="pdfdocument"></param>
       /// <param name="nInvoicNo"></param>
       /// <param name="sDate"></param>
       /// <param name="sAmount"></param>
        private void GenerateDataRow(Document pdfdocument, PdfPTable pdfptable, string nInvoicNo,string sDate,string sAmount)
        {
            int nDataFont = 8;
            int nFontStyle = 0;
            try
            {
                PdfPCell cell;

                cell = GetCell(nInvoicNo, Color.BLACK, nFontStyle, nDataFont);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.Colspan = 1;
                cell.BackgroundColor = Color.WHITE;
                pdfptable.AddCell(cell);

                cell = GetCell(sDate, Color.BLACK, nFontStyle, nDataFont);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.Colspan = 1;
                cell.BackgroundColor = Color.WHITE;
                pdfptable.AddCell(cell);

                cell = GetCell(sAmount, Color.BLACK, nFontStyle, nDataFont);
                cell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
                cell.Colspan = 1;
                cell.BackgroundColor = Color.WHITE;
                pdfptable.AddCell(cell);


            }
            catch (Exception ex)
            {
                message = ex.Message;
                Console.WriteLine(message);
            }

        }

        /// <summary>
        /// This method is used to display the Total and Grand Total column
        /// </summary>
        /// <param name="pdfdocument"></param>
        /// <param name="pdfptable"></param>
        /// <param name="nTotValue"></param>
        /// <param name="sDisplayText"></param>
        private void GenerateTotalColumn(Document pdfdocument, PdfPTable pdfptable,float nTotValue,string sDisplayText)
        {
            int nDataFont = 8;
            int nFontStyle = 1;
            try
            {
                PdfPCell cell;

                cell = GetCell(string.Empty, Color.BLACK, nFontStyle, nDataFont);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.Colspan = 1;
                cell.BackgroundColor = Color.WHITE;
                pdfptable.AddCell(cell);

                cell = GetCell(sDisplayText, Color.BLACK, nFontStyle, nDataFont);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.Colspan = 1;
                cell.BackgroundColor = Color.WHITE;
                pdfptable.AddCell(cell);

                cell = GetCell(Math.Round(double.Parse(nTotValue.ToString()), 2).ToString("0.00"), Color.BLACK, nFontStyle, nDataFont);
                cell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
                cell.Colspan = 1;
                cell.BackgroundColor = Color.WHITE;

                cell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;
                cell.FixedHeight = 15.0f;
                pdfptable.AddCell(cell);


            }
            catch (Exception ex)
            {
                message = ex.Message;
                Console.WriteLine(message);
            }
        }


        private iTextSharp.text.pdf.PdfPCell GetCell(string value, iTextSharp.text.Color color, int fontstyle, int fontSize)
        {
            Font font = FontFactory.GetFont(fontname, fontSize, fontstyle);
            iTextSharp.text.Phrase pphrase = new iTextSharp.text.Phrase(new Chunk(value, font));
            iTextSharp.text.pdf.PdfPCell cell = new iTextSharp.text.pdf.PdfPCell(pphrase);
            cell.BorderColor = color;
            //cell.FixedHeight = 17;
            return cell;
        }

        /// <summary>
        /// This method is used to insert record to history table
        /// </summary>
        /// <param name="nCustNo"></param>
        /// <param name="nGroupNo"></param>
        /// <param name="totAmount"></param>
        /// <param name="isGroup"></param>
        /// <param name="sFileName"></param>
        private string  InsertHistoryTableRecord(int nCustNo, int nGroupNo, decimal totAmount, int isGroup, string sFileName)
        {
            string msg = string.Empty;
            try
            {
                ProcessTransaction oTrans = new ProcessTransaction();
                msg =  oTrans.InsertAccountStatementHistory(nCustNo, nGroupNo, totAmount, isGroup, sFileName);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return msg;
        }

        /// <summary>
        /// This method is used to get the current report number
        /// </summary>
        /// <returns></returns>
        private int GenerateReportNumber()
        {
            int nRepNo = 0;

            //Get the current report number
            ProcessTransaction oTrans = new ProcessTransaction();
            DataTable dtRecCount = oTrans.GetAccountStatementHistoryRecCount();
            DataTable dtReportNo = oTrans.GetReportNumber();
            //if (dtRecCount.Rows.Count <= 0)
           // {
                nRepNo = int.Parse(dtReportNo.Rows[0][0].ToString()) + 1;
           // }
           // else
            //{
               // nRepNo = int.Parse(dtReportNo.Rows[0][0].ToString());
            //}

            return nRepNo;

        }

        /// <summary>
        /// This method is used to send the Customer Account Statement as Attachement
        /// </summary>
        /// <param name="sEmailAddress"></param>
        /// <param name="sAttachementName"></param>
        /// <param name="nRecurancePeriod"></param>
        private void SendEmail(string sEmailAddress, string sAttachementName,int nRecurancePeriod,int nResellerID, int nInvoiceNo)
        {

            string sFilePath = ConfigurationSettings.AppSettings["ReportPath"] + sAttachementName;
            MailMessage mail = new MailMessage();
            SmtpClient emailClient = new SmtpClient(ConfigurationSettings.AppSettings["SMTPServer"].ToString());
            emailClient.Credentials = new System.Net.NetworkCredential(ConfigurationSettings.AppSettings["SMTPUID"].ToString(), ConfigurationSettings.AppSettings["SMTPPW"].ToString());
            try
            {
                mail.To.Add(new MailAddress(sEmailAddress));
                mail.Bcc.Add(new MailAddress(ConfigurationSettings.AppSettings["InvoiceAccountBCC"].ToString()));

                mail.From = new MailAddress(ConfigurationSettings.AppSettings["MailFrom"].ToString());
                mail.Subject = ConfigurationSettings.AppSettings["MailSubject"] + " - " + nInvoiceNo.ToString();
                mail.Body = GenerateMailBody(nRecurancePeriod, nResellerID);
                mail.IsBodyHtml = true;
                System.Net.Mail.Attachment attachment = new Attachment(sFilePath); //create the attachment
                //Console.WriteLine(attachment.ToString());
                mail.Attachments.Add(attachment);	//add the attachment


                emailClient.Send(mail);
            }
            catch (Exception ex)
            {
                message = ex.Message;
               Console.WriteLine(message + ":" + ex.StackTrace);
               // Console.WriteLine("failure");
                //Console.Write(ex.
            }
            finally
            {
                mail.Dispose();
            }

        }

        /// <summary>
        /// This method is used to generate the mail body
        /// </summary>
        /// <param name="nRecurancePeriod"></param>
        /// <param name="nResellerID"></param>
        /// <returns></returns>
        private string GenerateMailBody(int nRecurancePeriod,int nResellerID)
        {
            StringBuilder sb = new StringBuilder();
            string strRecPeriod = string.Empty;

            if (nRecurancePeriod == 1)
                strRecPeriod = "Weekly";
            else if (nRecurancePeriod == 2)
                strRecPeriod = "Fortnight";
            else
                strRecPeriod = "Monthly";

            //Bread Factory
            if (nResellerID == 0)
                sb.Append(ConfigurationSettings.AppSettings["MailBodyBF1"].ToString() + "<br/><br/>");
            else if (nResellerID == 1)//Flour station
                sb.Append(ConfigurationSettings.AppSettings["MailBodyFS1"].ToString() + "<br/><br/>");

            sb.Append(ConfigurationSettings.AppSettings["MailBody1"].ToString() + strRecPeriod  + ConfigurationSettings.AppSettings["MailBody2"].ToString() + "<br/>");
            sb.Append(ConfigurationSettings.AppSettings["MailBody3"].ToString() + "<b><a href=mailto:" + ConfigurationSettings.AppSettings["SupportMailAddress"].ToString() + ">" + ConfigurationSettings.AppSettings["SupportMailAddress"].ToString() + "</a></b>" + ConfigurationSettings.AppSettings["MailBody4"].ToString() + "<br/><br/>");
            sb.Append(ConfigurationSettings.AppSettings["MailBody5"].ToString() + "<br/>");

            if (nResellerID == 0)
                sb.Append(ConfigurationSettings.AppSettings["MailBodyBF2"].ToString());
            else
                sb.Append(ConfigurationSettings.AppSettings["MailBodyFS2"].ToString());

          return sb.ToString();
        }

        /// <summary>
        /// This method is used to Email the Customer account Statement
        /// </summary>
        /// <param name="isGroup"></param>
        /// <param name="nCustNo"></param>
        /// <param name="nGroupNo"></param>
        /// <param name="sFieName"></param>
        private void SendCustomerAccountStatement(bool isGroup, int nCustNo, int nGroupNo, string sFieName, int nInvoiceNo)
        {
            ProcessTransaction oTrans = new ProcessTransaction();
            DataTable dtCustomerList = oTrans.ListAllCustomerDetails();
            DataRow[] drCustomer = null;
            if (isGroup)
            {
                //Send weekly invoice email to customers in whole group
                //drCustomer = dtCustomerList.Select("GNo = " + nGroupNo);

                //foreach (DataRow custRow in drCustomer)
                //{
                //    if (!string.IsNullOrEmpty(custRow["EmailAddress"].ToString()))
                //        SendEmail(custRow["EmailAddress"].ToString(), sFieName, int.Parse(drCustomer[0]["RecurrancePeriod"].ToString()), int.Parse(drCustomer[0]["ResellerID"].ToString()), nInvoiceNo);
                //}


                //Mail send to group email address - this is change 2012-03-16
                //string sGroupEmail = string.Empty;
                drCustomer = dtCustomerList.Select("GNo = " + nGroupNo);
                //DataTable dtGroup = oTrans.GetGroupMasterEmailbyGNo(nGroupNo);
                //if(dtGroup != null)
                //{
                //    if(dtGroup.Rows.Count > 0)
                //    {
                //        if(dtGroup.Rows[0]["Email"] != null)
                //            sGroupEmail  =  dtGroup.Rows[0]["Email"].ToString();
                //        if(!string.IsNullOrEmpty(sGroupEmail))
                //            SendEmail(sGroupEmail, sFieName, int.Parse(drCustomer[0]["RecurrancePeriod"].ToString()), int.Parse(drCustomer[0]["ResellerID"].ToString()), nInvoiceNo);
                //    }
                //}


                if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["GroupInvoiceAccountTo"].ToString()))
                {
                    SendEmail(ConfigurationSettings.AppSettings["GroupInvoiceAccountTo"].ToString(), sFieName, int.Parse(drCustomer[0]["RecurrancePeriod"].ToString()), int.Parse(drCustomer[0]["ResellerID"].ToString()), nInvoiceNo);
                    Console.WriteLine("Mail send successfully");
                }
            }
            else
            {
                drCustomer = dtCustomerList.Select("CNo = " + nCustNo);

                if (!string.IsNullOrEmpty(drCustomer[0]["EmailAddress"].ToString()))
                    SendEmail(drCustomer[0]["EmailAddress"].ToString(), sFieName, int.Parse(drCustomer[0]["RecurrancePeriod"].ToString()), int.Parse(drCustomer[0]["ResellerID"].ToString()), nInvoiceNo);
            }
        }

        /// <summary>
        /// This method is used to display Logo According to the Reseller ID
        /// </summary>
        /// <param name="pdfptableLogo"></param>
        /// <param name="imgPath"></param>
        private void GenerateLogoRow(PdfPTable pdfptableLogo,string imgPath,PdfPTable pdfptableAddress, string sAddress1, string sAddress2, string sAddress3, string sTown, string sPostCode)
        {
            try
            {

                iTextSharp.text.pdf.PdfPTable pdfpTab1 = new iTextSharp.text.pdf.PdfPTable(1);
                PdfPCell cellTab1 = new PdfPCell(new Phrase(GetPdfImageChunk(imgPath, Rectangle.ALIGN_CENTER)));
               // cellTab1.HorizontalAlignment = Rectangle.ALIGN_CENTER;
                cellTab1.PaddingLeft = 150;
                cellTab1.Colspan = 1;
                cellTab1.BackgroundColor = Color.WHITE;
                cellTab1.BorderWidth = 0;

                pdfpTab1.AddCell(cellTab1);


                PdfPCell cellTab2 = new PdfPCell(CustomerAddressRow(pdfptableAddress,sAddress1,sAddress2,sAddress3,sTown,sPostCode));
                pdfpTab1.AddCell(cellTab2);

               // PdfPCell cell = new PdfPCell(new Phrase(GetPdfImageChunk(imgPath, Rectangle.ALIGN_CENTER)));
                PdfPCell cell = new PdfPCell(pdfpTab1);
                cell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
                cell.BackgroundColor = Color.WHITE;
                cell.BorderWidth = 0;
                //cell.FixedHeight = 77.0f;
                cell.Colspan = 2;
                
                pdfptableLogo.AddCell(cell);
               
                
                //Company Address

                PdfPCell cellA = new PdfPCell(CompanyAddress());
                cellA.BackgroundColor = Color.WHITE;
                cellA.HorizontalAlignment = Rectangle.ALIGN_MIDDLE;
                cellA.BorderWidth = 0;
                cellA.Colspan = 1;
                cellA.PaddingLeft = 40;
               
                pdfptableLogo.AddCell(cellA);



            }
            catch (Exception ex)
            {
                message = ex.Message;
                Console.WriteLine(message);
            }

        }





        private PdfPTable CompanyAddress()
        {
            iTextSharp.text.pdf.PdfPTable pdfCompanyAddress = new iTextSharp.text.pdf.PdfPTable(1);
            PdfPCell cell = new PdfPCell();
            cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            cell.Colspan = 1;
            cell.BackgroundColor = Color.WHITE;
            cell.BorderWidth = 0;

            cell = GetCell(ConfigurationSettings.AppSettings["CompanyAddressLine1"], Color.WHITE, 0, 7);
            pdfCompanyAddress.AddCell(cell);

            cell = GetCell(ConfigurationSettings.AppSettings["CompanyAddressLine2"], Color.WHITE, 0, 7);
            pdfCompanyAddress.AddCell(cell);

            cell = GetCell(ConfigurationSettings.AppSettings["CompanyAddressLine3"], Color.WHITE, 0, 7);
            pdfCompanyAddress.AddCell(cell);

            cell = GetCell(ConfigurationSettings.AppSettings["CompanyAddressLine4"], Color.WHITE, 0, 7);
            pdfCompanyAddress.AddCell(cell);

            cell = GetCell(ConfigurationSettings.AppSettings["CompanyAddressLine5"], Color.WHITE, 0, 7);
            pdfCompanyAddress.AddCell(cell);

            cell = GetCell(ConfigurationSettings.AppSettings["CompanyAddressLine6"], Color.WHITE, 0, 7);
            pdfCompanyAddress.AddCell(cell);

            cell = GetCell(ConfigurationSettings.AppSettings["CompanyAddressLine7"], Color.WHITE, 0, 7);
            pdfCompanyAddress.AddCell(cell);

            cell = GetCell(string.Empty, Color.WHITE, 1, 7);
            pdfCompanyAddress.AddCell(cell);

            cell = GetCell(ConfigurationSettings.AppSettings["CompanyTel"], Color.WHITE, 0, 7);
            pdfCompanyAddress.AddCell(cell);

            cell = GetCell(ConfigurationSettings.AppSettings["CompanyFax"], Color.WHITE, 0, 7);
            pdfCompanyAddress.AddCell(cell);

            cell = GetCell(string.Empty, Color.WHITE, 1, 7);
            cell.FixedHeight = 10.0f;
            pdfCompanyAddress.AddCell(cell);

            return pdfCompanyAddress;
        }

        private PdfPTable CustomerAddressRow(PdfPTable pdfptableAddress, string sAddress1, string sAddress2, string sAddress3, string sTown, string sPostCode)
        {
            PdfPCell cell = new PdfPCell();

            cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            cell.Colspan = 1;
            cell.BackgroundColor = Color.WHITE;
            cell.BorderWidth = 0;

            if (!string.IsNullOrEmpty(sAddress1))
            {
                cell = GetCell(sAddress1, Color.WHITE, 0, 7);
                pdfptableAddress.AddCell(cell);
            }

            if (!string.IsNullOrEmpty(sAddress2))
            {
                cell = GetCell(sAddress2, Color.WHITE, 0, 7);
                pdfptableAddress.AddCell(cell);
            }

            if (!string.IsNullOrEmpty(sAddress3))
            {
                cell = GetCell(sAddress3, Color.WHITE, 0, 7);
                pdfptableAddress.AddCell(cell);
            }

            if (!string.IsNullOrEmpty(sTown))
            {
                cell = GetCell(sTown, Color.WHITE, 0, 7);
                pdfptableAddress.AddCell(cell);
            }

            if (!string.IsNullOrEmpty(sPostCode))
            {
                cell = GetCell(sPostCode, Color.WHITE, 0, 7);
                pdfptableAddress.AddCell(cell);
            }

            cell = GetCell(string.Empty, Color.WHITE, 0, 7);
            cell.FixedHeight = 10.0f;
            pdfptableAddress.AddCell(cell);

            return pdfptableAddress;
        }

        /// <summary>
        /// returns a pdf chunk that can be used to pass parameters to add image
        /// </summary>
        /// <param name="text">path to the image</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private Chunk GetPdfImageChunk(string imgpath, int alignment)
        {
            iTextSharp.text.Image imgheader = iTextSharp.text.Image.GetInstance(imgpath);
            imgheader.ScalePercent(50);
            imgheader.Alignment = alignment;
            Chunk pdfchunk;
            pdfchunk = new Chunk(imgheader, 0, 10);
            return pdfchunk;
        }

        /// <summary>
        /// This method is used to display the report title
        /// </summary>
        /// <param name="pdfptableLogo"></param>
        /// <param name="sTitle"></param>
        private void GenerateReportTitle(PdfPTable pdfptableLogo, string sTitle)
        {
            try
            {
                PdfPCell cell;

                cell = GetCell(sTitle, Color.BLACK, 1, 10);
                cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
                cell.FixedHeight = 30.0f;
                cell.BorderWidth = 0;
                cell.Colspan = 3;
                cell.BackgroundColor = Color.WHITE;

                pdfptableLogo.AddCell(cell);

            }
            catch (Exception ex)
            {
                message = ex.Message;
                Console.WriteLine(message);
            }
        }

        private void GenerateReportFooter(PdfPTable pdfptableFooter,string sResellerID)
        {
            int fSize = 7;
            try
            {
                PdfPCell cell;

                cell = GetCell(string.Empty, Color.BLACK, 0, fSize);
                cell.FixedHeight = 10.0f;
                cell.BorderWidth = 0;
                pdfptableFooter.AddCell(cell);

                cell = GetCell(sResellerID == "0" ? ConfigurationSettings.AppSettings["BFFooter1"].ToString() : ConfigurationSettings.AppSettings["FSFooter1"].ToString(), Color.BLACK, 1, 8);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.BorderWidth = 0;
                cell.BackgroundColor = Color.WHITE;
                pdfptableFooter.AddCell(cell);

                cell = GetCell(sResellerID == "0" ? ConfigurationSettings.AppSettings["BFFooter2"].ToString() : ConfigurationSettings.AppSettings["FSFooter2"].ToString(), Color.BLACK, 4, fSize);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.BorderWidth = 0;
                cell.BackgroundColor = Color.WHITE;
                pdfptableFooter.AddCell(cell);

                cell = GetCell(sResellerID == "0" ? ConfigurationSettings.AppSettings["BFFooter3"].ToString() : ConfigurationSettings.AppSettings["FSFooter3"].ToString(), Color.BLACK, 0, fSize);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.BorderWidth = 0;
                cell.BackgroundColor = Color.WHITE;
                pdfptableFooter.AddCell(cell);

                cell = GetCell(sResellerID == "0" ? ConfigurationSettings.AppSettings["BFFooter4"].ToString() : ConfigurationSettings.AppSettings["FSFooter4"].ToString(), Color.BLACK, 0, fSize);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.BorderWidth = 0;
                cell.BackgroundColor = Color.WHITE;
                pdfptableFooter.AddCell(cell);

                cell = GetCell(sResellerID == "0" ? ConfigurationSettings.AppSettings["BFFooter5"].ToString() : ConfigurationSettings.AppSettings["FSFooter5"].ToString(), Color.BLACK, 0, fSize);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.BorderWidth = 0;
                cell.BackgroundColor = Color.WHITE;
                pdfptableFooter.AddCell(cell);

                cell = GetCell(sResellerID == "0" ? ConfigurationSettings.AppSettings["BFFooter6"].ToString() : ConfigurationSettings.AppSettings["FSFooter6"].ToString(), Color.BLACK, 0, fSize);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.BorderWidth = 0;
                cell.BackgroundColor = Color.WHITE;
                pdfptableFooter.AddCell(cell);


                cell = GetCell(string.Empty, Color.BLACK, 0, fSize);
                cell.FixedHeight = 10.0f;
                cell.BorderWidth = 0;
                cell.BackgroundColor = Color.WHITE;
                pdfptableFooter.AddCell(cell);

            }
            catch (Exception ex)
            {
                message = ex.Message;
                Console.WriteLine(message);
            }
        }


        private void CreatepageFooter(PdfPTable pdfptablePageFooter, Document pdfdocument)
        {
             int fSize = 7;
             //Font phFont = new Font(fontname, 7f, Font.NORMAL,Color.BLACK);
             Font phFont = FontFactory.GetFont(fontname, fSize, 0, Color.BLACK);

             try
             {
                // PdfPCell cell = new PdfPCell();
                  
                // cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                // cell.Colspan = 1;
                // cell.BackgroundColor = Color.WHITE;
                // cell.BorderWidth = 0;

                // cell = GetCell(ConfigurationSettings.AppSettings["PageFooter1"], Color.WHITE, 1, fSize);
                //// pdfptablePageFooter.AddCell(cell);

                // cell = GetCell(ConfigurationSettings.AppSettings["PageFooter2"], Color.WHITE, 1, fSize);
                // //pdfptablePageFooter.AddCell(cell);

                // cell = GetCell(ConfigurationSettings.AppSettings["PageFooter3"], Color.WHITE, 1, fSize);
                // //pdfptablePageFooter.AddCell(cell);

                 string text = ConfigurationSettings.AppSettings["PageFooter1"] + "\n " + ConfigurationSettings.AppSettings["PageFooter2"] + "\n" + ConfigurationSettings.AppSettings["PageFooter3"];

                 Phrase pFooter = new Phrase(text, phFont);

                 HeaderFooter footer = new HeaderFooter(pFooter, false);
                 footer.Border = 0;

                 footer.Alignment = Rectangle.ALIGN_CENTER;
                 
                 pdfdocument.Footer = footer;
             }
             catch (Exception ex)
             {
                 message = ex.Message;
                 Console.WriteLine(message);
             }
        }

    }
}
