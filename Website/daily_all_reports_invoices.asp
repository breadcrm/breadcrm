<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Response.Buffer = true
deldate= request.form("deldate")
if deldate= "" then response.redirect "LoginInternalPage.asp"
dim objBakery
dim recarray1, recarray2
dim ordno
dim retcol
Dim deldate
Dim intI
Dim intN
Dim intF
Dim intJ,strBG
intN = 0

dim tname, tno, curtno
Dim obj
Dim arCreditStatements
Dim arCreditStatementDetails
Dim vCreditDate
tno=3
%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; height:1px }
-->
</style>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
intN = 0
ordno = 0
curtno=1
set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
for curtno=1 to tno
if curtno=1 then
tname="Morning"
elseif curtno=2 then
tname="Noon"
else
tname="Evening"
end if
set retcol = objBakery.GetInvoiceTotal(ordno, deldate, tname)
recarray1 = retcol("InvoiceTotal")
set retcol = Nothing

If isArray(recarray1) Then
intF = ubound(recarray1, 2) + 1
End If

if IsArray(recarray1) Then
For intI = 0 To ubound(recarray1, 2)
for intJ=1 to recarray1(15,intI)
intN = 0
If recarray1(3, intI) = "Bread Factory" Then
strLogo = "<b><font face=""Monotype Corsiva"" size=""5"">THE BREAD FACTORY</font></b>"
strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
strAddress = strAddress & "<br>" & vbCrLf
strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
strAddress = strAddress & "Watford, Herts WD17 1LA" & vbCrLf
strAddress = strAddress & "" & vbCrLf
strAddress = strAddress & "<BR>" & vbCrLf
strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
strAddress = strAddress & "</p></font>"
ElseIf recarray1(3, intI) = "Gail Force" Then
strLogo = "<b><font face=""Kunstler Script"" size=""8"">Gail Force</font></b>"
strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
strAddress = strAddress & "<br>" & vbCrLf
strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
strAddress = strAddress & "Watford, Herts WD17 1LA" & vbCrLf
strAddress = strAddress & "" & vbCrLf
strAddress = strAddress & "<BR>" & vbCrLf
strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
strAddress = strAddress & "</p></font>"
End If%>
<div align="center">
<center>
<%
strBG=""
if (recarray1(16, intI)="1") then
strBG="images/PricelessInvoiceNew.gif"
end if
%>
<table border="0" width="90%" height="100%" cellspacing="0" cellpadding="0" style="background-repeat:no-repeat">
<tr>
<td width="100%" valign="top">
<table border="0" width="90%" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%" valign="top" >
<font face="Verdana" size="1">VAT Reg No. XXXXXX</font>
<%=strAddress%>
</td>
<td width="46%" valign="top" align="center">
<img src="images/BakeryLogo.gif" width="225" height="77"><br>
<%if strBG<>"" then%><img src="<%=strBG%>" height="60" width="225"><%end if%>
</td>
<td width="28%" valign="top">
<p align="center">
<font face="Verdana" size="1">THIS IS AN INVOICE</font><br>
<font face="Verdana" size="1">PLEASE KEEP SAFE AND PASS TO YOUR ACCOUNTS DEPARTMENT</font></b><font size="1">
</font>
</td>
</tr>
<tr><td height="10"></td></tr>
</table>
<table border="0" width="90%" cellspacing="0" cellpadding="0"  align="center">
<tr>
<td width="33%"></td>
<td width="33%">
<p align="center"><font size="3" face="Verdana"><b>INVOICE<br>
</b>NUMBER: <%=recarray1(0, intI)%></font></td>
<td width="34%"></td>
</tr>
<tr><td height="10"></td></tr>
</table>
<table border="0" width="90%" cellspacing="0" cellpadding="0"  align="center">
<tr>
<td valign="top"><font face="Verdana" size="2"><b>
<% if recarray1(14, intI)<>"" then%><%=recarray1(14, intI)%><br><%end if%>
<% if recarray1(2, intI)<>"" then%><%=recarray1(2, intI)%><br><%end if%>
<% if recarray1(5, intI)<>"" then%><%=recarray1(5, intI)%><br><%end if%>
<% if recarray1(6, intI)<>"" then%><%=recarray1(6, intI)%><br><%end if%>
<% if recarray1(7, intI)<>"" then%><%=recarray1(7, intI)%><br><%end if%>
<% if recarray1(8, intI)<>"" then%><%=recarray1(8, intI)%><%end if%>
</b></font></td>
<td width="33%" valign="top"><font face="Verdana" size="2"><b>
Purchase order No.: <%=recarray1(9, intI)%><br><br>
Delivered by van number: <%=recarray1(10, intI)%><br><br>
Date: <%= Day(recarray1(1, intI)) & "/" & Month(recarray1(1, intI)) & "/" & Year(recarray1(1, intI)) %>
</b></font></td>
</tr>
<tr><td height="10"></td></tr>
</table>

<table border="1" align="center" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
<tr>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Quantity</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product code</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product name</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Unit price</b></td>
<td width="20%" align="center" bgcolor="#CCCCCC" height="20"><b>Total price</b></td>
</tr><%
set retcol = objBakery.GetInvoiceDetails(recarray1(0, intI))
recarray2 = retcol("InvoiceDetails")
if isarray(recarray2) then
for intN=0 to ubound(recarray2, 2)
if recarray1(0, intI) = recarray2(0, intN) then%>
<tr>
<td align="center"><%=recarray2(1, intN)%>&nbsp;</td>
<td align="center"><%=recarray2(2, intN)%>&nbsp;</td>
<td align="center"><%=recarray2(3, intN)%>&nbsp;</td><%
if isnull(recarray2(4, intN)) then%>
<td align="center">0.00</td><%
else%>
<td align="center"><%=FormatNumber(recarray2(4, intN), 2)%>&nbsp;</td><%
end if
if isnull(recarray2(5, intN)) then%>
<td width="20%" align="center">0.00</td><%
else%>
<td width="20%" align="center"><%=FormatNumber(recarray2(5, intN), 2)%>&nbsp;</td><%
end if%>
</tr><%
end if
next
erase recarray2
end if
set retcol = Nothing %>
</table>

<table border="0" width="90%" cellspacing="0" cellpadding="0" align="center">
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">Goods total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%"><%
if isnull(recarray1(11,intI)) then%>
<p align="center"><font size="3" face="Verdana"><b>&nbsp;0.00</b></font></p><%
else%>
<p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(recarray1(11,intI), 2)%></b></font></p><%
end if%>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">VAT total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%"><%
if isnull(recarray1(12,intI)) then%>
<p align="center"><font size="3" face="Verdana"><b>&nbsp;0.00</b></font></p><%
else%>
<p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(recarray1(12,intI), 2)%></b></font></p><%
end if%>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">Invoice total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%"><%
if isnull(recarray1(13,intI)) then%>
<p align="center"><font size="3" face="Verdana"><b>&nbsp;0.00</b></font></p><%
else%>
<p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(recarray1(13,intI), 2)%></b></font></p><%
end if%>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table border="0" width="90%" cellspacing="0" cellpadding="0"  align="center">
<tr>
	<td width="100%" height="10"></td>
</tr>
<tr>
<td width="100%">
<font face="Verdana" size="1">
<b>IMPORTANT!!</b><br>
Shortages must be reported on day of delivery. To ensure next day delivery, ring before 14:00 Mon - Fri
</font>
</td>
</tr>
<tr><td height="10"></td></tr>
<tr>
<td width="100%" align="center"><font face="Verdana" size="1"><%=strglobalinvoicefootermessage%></font></td>
</tr>
<tr><td height="10"></td></tr>
</table>

</td>
</tr>
</table>
</center>
</div>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<% 
Next
next
End if 
next
set objBakery = Nothing
Response.Flush()
%>

<%
intN = 0
vCreditDate = Request.Form ("deldate")
Set obj = server.CreateObject("bakery.daily")
obj.SetEnvironment(strconnection)
dim a1,intMonth,intDay,intYear,strCreditDate
strPreviousDayinDDMMYY=PreviousDayinDDMMYY(vCreditDate)
Set objBakery = obj.Display_CreditStatements(strPreviousDayinDDMMYY)
arCreditStatements =  objBakery("CreditStatements")
arCreditStatementDetails =  objBakery("CreditStatementDetails")

Set objBakery = Nothing
Set obj = Nothing

If isArray(arCreditStatements) Then
intF = ubound(arCreditStatements, 2) + 1
End If

if IsArray(arCreditStatements) Then
For intI = 0 To ubound(arCreditStatements, 2)
intN = 0
If arCreditStatements(11, intI) = "Bread Factory" Then
strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
strAddress = strAddress & "<br>" & vbCrLf
strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
strAddress = strAddress & "Watford, Herts WD17 1LA" & vbCrLf
strAddress = strAddress & "" & vbCrLf
strAddress = strAddress & "<BR>" & vbCrLf
strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
strAddress = strAddress & "</p></font>"
ElseIf arCreditStatements(11, intI) = "Gail Force" Then
strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
strAddress = strAddress & "<br>" & vbCrLf
strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
strAddress = strAddress & "Watford, Herts WD17 1LA" & vbCrLf
strAddress = strAddress & "" & vbCrLf
strAddress = strAddress & "<BR>" & vbCrLf
strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
strAddress = strAddress & "</p></font>"
End If%>
<BR>		

<div align="center">
<center>
<%
strBG=""
if (arCreditStatements(14, intI))="1" then
strBG="images/PricelessInvoiceNew.gif"
end if
%>
<table border="0" width="90%" height="100%" cellspacing="0" cellpadding="0" style="background-repeat:no-repeat">
<tr>
<td width="100%" valign="top">

<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="33%" valign="top">
<font face="Verdana" size="1">VAT Reg No. XXXXXX</font>
<%=strAddress%>
</td>
<td width="46%" valign="top" align="center">
<img src="images/BakeryLogo.gif" width="225" height="77"><br>
<%if strBG<>"" then%><img src="<%=strBG%>" height="60" width="225"><%end if%>
</td>
<td width="33%" valign="top" align="center">
<font face="Verdana" size="1">
THIS IS A CREDIT<br>
STATEMENT<br>
</font>
<font face="Verdana" size="1">PLEASE KEEP SAFE AND PASS TO YOU ACCOUNTS DEPARTMENT</font>
</td>
</tr>
<tr><td height="10"></td></tr>
<tr>
<td width="33%"></td>
<td width="34%">
<p align="center"><font size="3" face="Verdana"><b>Credit<br>
</b>NUMBER: <%=arCreditStatements(0, intI)%></font></td>
<td width="33%"></td>
</tr>
<tr><td height="10"></td></tr>
</table>

<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tr>
<td valign="top"><font face="Verdana" size="2">
Customer Code: <%=arCreditStatements(12, intI)%><br>
Van Number: <%=arCreditStatements(13, intI)%><br>
Customer Name: <%=arCreditStatements(2, intI)%><br>
Address: <%=arCreditStatements(4, intI)%>,&nbsp;<%=arCreditStatements(5, intI)%>,&nbsp;<%=arCreditStatements(6, intI)%>,&nbsp;<%=arCreditStatements(7, intI)%><br><br>
Date: <%=arCreditStatements(1, intI)%>
</font></td>
</tr>
<tr><td height="10"></td></tr>
</table>

<table align="center" border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
<tr>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Quantity</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product code</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product name</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Unit price</b></td>
<td width="20%" align="center" bgcolor="#CCCCCC" height="20"><b>Total price</b></td>
</tr><%
for intN = 0 to ubound(arCreditStatementDetails, 2)
if arCreditStatements(0, intI) = arCreditStatementDetails(0, intN) then
%><tr>
<td align="center"><%=arCreditStatementDetails(1, intN)%>&nbsp;</td>
<td align="center"><%=arCreditStatementDetails(2, intN)%>&nbsp;</td>
<td align="center"><%=arCreditStatementDetails(3, intN)%>&nbsp;</td>
<td align="center"><%=FormatNumber(arCreditStatementDetails(4, intN), 2)%>&nbsp;</td>
<td width="20%" align="center"><%=FormatNumber(arCreditStatementDetails(5, intN), 2)%>&nbsp;</td>
</tr><%
end if
next
%>
</table>


<table border="0" width="90%" cellspacing="0" cellpadding="0" align="center">
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">Goods total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%">
<p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(arCreditStatements(8, intI), 2)%></b></font></p>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">VAT total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%">
<p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(arCreditStatements(9, intI), 2)%></b></font></p>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">Invoice total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%">
<p align="center"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(cdbl(FormatNumber(arCreditStatements(8, intI), 2)+cdbl(FormatNumber(arCreditStatements(9, intI), 2))), 2)%></b></font></p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
</div>
<% if intI >= intF Then %>
<hr>
<br class="page" />
<%
End if
Next
End if 
Response.Flush()
%>
<p></p>
<%
'daily-report_invoice.asp
'------------------------

intN = 0
ordno = 0
curtno=1
set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
for curtno=1 to tno
if curtno=1 then
tname="Morning"
elseif curtno=2 then
tname="Noon"
else
tname="Evening"
end if
set retcol = objBakery.GetInvoiceTotalPricelessInvoice(ordno, deldate, tname)
recarray1 = retcol("InvoiceTotal")
set retcol = Nothing

If isArray(recarray1) Then
intF = ubound(recarray1, 2) + 1
End If

if IsArray(recarray1) Then
For intI = 0 To ubound(recarray1, 2)
for intJ=1 to recarray1(15,intI)
intN = 0
If recarray1(3, intI) = "Bread Factory" Then
strLogo = "<b><font face=""Monotype Corsiva"" size=""5"">THE BREAD FACTORY</font></b>"
strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
strAddress = strAddress & "<br>" & vbCrLf
strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
strAddress = strAddress & "Watford, Herts WD17 1LA" & vbCrLf
strAddress = strAddress & "" & vbCrLf
strAddress = strAddress & "<BR>" & vbCrLf
strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
strAddress = strAddress & "</p></font>"
ElseIf recarray1(3, intI) = "Gail Force" Then
strLogo = "<b><font face=""Kunstler Script"" size=""8"">Gail Force</font></b>"
strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
strAddress = strAddress & "<br>" & vbCrLf
strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
strAddress = strAddress & "Watford, Herts WD17 1LA" & vbCrLf
strAddress = strAddress & "" & vbCrLf
strAddress = strAddress & "<BR>" & vbCrLf
strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
strAddress = strAddress & "</p></font>"
End If%>
<div align="center">
<center>
<table border="0" width="90%" height="100%" cellspacing="0" cellpadding="0">
<tr>
<td width="100%" valign="top">
<table border="0" width="90%" cellspacing="0" cellpadding="0" align="center" >
<tr>
<td width="26%" valign="top" >
<font face="Verdana" size="1">VAT Reg No. XXXXXX</font>
<%=strAddress%>
</td>
<td width="46%" valign="top" align="center">
<img src="images/BakeryLogo.gif" width="225" height="77">
</td>
<td width="28%" valign="top">
<p align="center">
<font face="Verdana" size="1">THIS IS AN INVOICE</font><br>
<font face="Verdana" size="1">PLEASE KEEP SAFE AND PASS TO YOUR ACCOUNTS DEPARTMENT</font></b><font size="1">
</font>
</td>
</tr>
<tr><td height="10"></td></tr>
</table>

<table border="0" width="90%" cellspacing="0" cellpadding="0"  align="center">
<tr>
<td width="33%"></td>
<td width="33%">
<p align="center"><font size="2" face="Verdana"><b>Delivery Note for</b><br> INVOICE
NUMBER: <%=recarray1(0, intI)%></font></td>
<td width="34%"></td>
</tr>
<tr><td height="10"></td></tr>
</table>
<table border="0" width="90%" cellspacing="0" cellpadding="0"  align="center">
<tr>
<td valign="top"><font face="Verdana" size="2"><b>
<% if recarray1(11, intI)<>"" then%><%=recarray1(11, intI)%><br><%end if%>
<% if recarray1(2, intI)<>"" then%><%=recarray1(2, intI)%><br><%end if%>
<% if recarray1(5, intI)<>"" then%><%=recarray1(5, intI)%><br><%end if%>
<% if recarray1(6, intI)<>"" then%><%=recarray1(6, intI)%><br><%end if%>
<% if recarray1(7, intI)<>"" then%><%=recarray1(7, intI)%><br><%end if%>
<% if recarray1(8, intI)<>"" then%><%=recarray1(8, intI)%><%end if%>
</b></font></td>
<td width="33%" valign="top"><font face="Verdana" size="2"><b>
Purchase order No.: <%=recarray1(9, intI)%><br><br>
Delivered by van number: <%=recarray1(10, intI)%><br><br>
Date: <%= Day(recarray1(1, intI)) & "/" & Month(recarray1(1, intI)) & "/" & Year(recarray1(1, intI)) %>
</b></font></td>
</tr>
<tr><td colspan="2"></td></tr>
</table>

<table border="1" align="center" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
<tr>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Quantity</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product code</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product name</b></td>
</tr><%
set retcol = objBakery.GetInvoiceDetails(recarray1(0, intI))
recarray2 = retcol("InvoiceDetails")
if isarray(recarray2) then
for intN=0 to ubound(recarray2, 2)
if recarray1(0, intI) = recarray2(0, intN) then%>
<tr>
<td align="center"><%=recarray2(1, intN)%>&nbsp;</td>
<td align="center"><%=recarray2(2, intN)%>&nbsp;</td>
<td align="center"><%=recarray2(3, intN)%>&nbsp;</td>

</tr><%
end if
next
erase recarray2
end if
set retcol = Nothing %>
</table>

<table border="0" width="90%" cellspacing="0" cellpadding="0"  align="center">
<tr>
	<td width="100%" height="10"></td>
</tr>
<tr>
<td width="100%">
<font face="Verdana" size="1">
<b>IMPORTANT!!</b><br>
Shortages must be reported on day of delivery. To ensure next day delivery, ring before 14:00 Mon - Fri
</font>
</td>
</tr>
<tr><td height="10"></td></tr>
<tr>
<td width="100%" align="center"><font face="Verdana" size="1"><%=strglobalinvoicefootermessage%></font></td>
</tr>
<tr><td>&nbsp;</td></tr>
</table>

</td>
</tr>
</table>
</center>
</div>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<% 
Next
next
End if 
next
Response.Flush()
%>
<%
intN = 0

vCreditDate = Request.Form ("deldate")

strPreviousDayinDDMMYY=PreviousDayinDDMMYY(vCreditDate)

Set obj = server.CreateObject("bakery.daily")
obj.SetEnvironment(strconnection)
Set objBakery = obj.Display_CreditStatementsPricelessInvoice(strPreviousDayinDDMMYY)
arCreditStatements =  objBakery("CreditStatements")
arCreditStatementDetails =  objBakery("CreditStatementDetails")

Set objBakery = Nothing
Set obj = Nothing

If isArray(arCreditStatements) Then
intF = ubound(arCreditStatements, 2) + 1
End If

if IsArray(arCreditStatements) Then
For intI = 0 To ubound(arCreditStatements, 2)
intN = 0
If arCreditStatements(11, intI) = "Bread Factory" Then
strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
strAddress = strAddress & "<br>" & vbCrLf
strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
strAddress = strAddress & "Watford, Herts WD17 1LA" & vbCrLf
strAddress = strAddress & "" & vbCrLf
strAddress = strAddress & "<BR>" & vbCrLf
strAddress = strAddress & "Orders: &nbsp;&nbsp;&nbsp; 020 7041 6898<br>" & vbCrLf
strAddress = strAddress & "Accounts: &nbsp;020 8457 2081<br>" & vbCrLf
strAddress = strAddress & "Fax: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; XXXXXXXX" & vbCrLf
strAddress = strAddress & "</p></font>"
ElseIf arCreditStatements(11, intI) = "Gail Force" Then
strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
strAddress = strAddress & "<br>" & vbCrLf
strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
strAddress = strAddress & "Watford, Herts WD17 1LA" & vbCrLf
strAddress = strAddress & "" & vbCrLf
strAddress = strAddress & "<BR>" & vbCrLf
strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
strAddress = strAddress & "</p></font>"
End If%>
<BR>		

<div align="center">
<center>

<table border="0" width="90%" height="100%" cellspacing="0" cellpadding="0">
<tr>
<td width="100%" valign="top">

<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="33%" valign="top">
<font face="Verdana" size="1">VAT Reg No. XXXXXX</font>
<%=strAddress%>
</td>
<td width="46%" valign="top" align="center">
<img src="images/BakeryLogo.gif" width="225" height="77">
</td>
<td width="33%" valign="top" align="center">
<font face="Verdana" size="1">
THIS IS A CREDIT<br>
STATEMENT<br>
</font>
<font face="Verdana" size="1">PLEASE KEEP SAFE AND PASS TO YOU ACCOUNTS DEPARTMENT</font>
</td>
</tr>
<tr><td height="10"></td></tr>
<tr>
<td width="33%"></td>
<td width="34%">
<p align="center"><font size="3" face="Verdana"><b>Credit<br>
</b>NUMBER: <%=arCreditStatements(0, intI)%></font></td>
<td width="33%"></td>
</tr>
<tr><td height="10"></td></tr>
</table>

<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tr>
<td valign="top"><font face="Verdana" size="2">
Customer Code: <%=arCreditStatements(12, intI)%><br>
Van Number: <%=arCreditStatements(13, intI)%><br>
Customer Name: <%=arCreditStatements(2, intI)%><br>
Address: <%=arCreditStatements(4, intI)%>,&nbsp;<%=arCreditStatements(5, intI)%>,&nbsp;<%=arCreditStatements(6, intI)%>,&nbsp;<%=arCreditStatements(7, intI)%><br><br>
Date: <%=arCreditStatements(1, intI)%>
</font></td>
</tr>
<tr><td height="10"></td></tr>
</table>

<table align="center" border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
<tr>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Quantity</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product code</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product name</b></td>

</tr><%
for intN = 0 to ubound(arCreditStatementDetails, 2)
if arCreditStatements(0, intI) = arCreditStatementDetails(0, intN) then
%><tr>
<td align="center"><%=arCreditStatementDetails(1, intN)%>&nbsp;</td>
<td align="center"><%=arCreditStatementDetails(2, intN)%>&nbsp;</td>
<td align="center"><%=arCreditStatementDetails(3, intN)%>&nbsp;</td>

</tr><%
end if
next
%>
</table>
</td>
</tr>
</table>
</center>
</div>
<% if intI >= intF Then %>
<hr>
<br class="page" />
<%
End if
Next
End if
set objBakery = Nothing
%>
<p></p>
</body>
</html>