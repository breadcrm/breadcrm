<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form1.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form1.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form1.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form1.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form1.day.value=t.getDate()
	document.form1.month.value=t.getMonth()
	document.form1.year.value=t.getFullYear()
	
	document.form1.day1.value=t1.getDate()
	document.form1.month1.value=t1.getMonth()
	document.form1.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}
//-->
</SCRIPT>
<script language="Javascript">
<!--
function IsDataValid()
{
  var ok = true;
  if(document.form1.txtfrom.value=='' || document.form1.txtto.value=='')
  {
    //alert('Please select valid dates for the from and until fields...');
    ok=false;
  }
  else if(isNaN(Date.parse(document.form1.txtfrom.value)))
  {
    //alert('select a valid date in the from field...');
    ok=false;
  }
  else if(isNaN(Date.parse(document.form1.txtto.value)))
  {
    //alert('select a valid date in the to field...');
    ok=false;
  }
  else if(Date.parse(document.form1.txtfrom.value) > Date.parse(document.form1.txtto.value))
  {
    //alert('from date should be less than to date...');
    ok=false;
  }
  if(ok==true)
    return true;
  else
  {
    document.form1.action="report_sales.asp"
    return true
  }
}
//-->>
</script>
</head>
<%
dim objBakery, retcol,  i, j
dim recarray1, recarray2, recarray3, recarray4, recarray5, recarray6
dim fno, a1, ptype, dtime, pno, pgno, cno, cgno, fromdt, todt

fno=Request.Form("lstfno")
ptype=Request.Form("lstptype")
dtime=Request.Form("lstdtime")
pno=Request.Form("chkpno")
pgno=Request.Form("chkpgno")
cno=Request.Form("chkcno")
cgno=Request.Form("chkcgno")
fromdt=Request.Form("txtfrom")
todt=Request.Form("txtto")
if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
   todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if
'if fno <> "" and pno <> "" and pgno <> "" and ptype <> "" and dtime <> "" and cno <> "" and cgno <> "" and fromdt <> "" and todt <> "" and Request.Form("search") = "Compile Report" then
if fromdt <> "" and todt <> "" and Request.Form("search") = "Compile Report" then
  Response.Redirect "report_sales1.asp?fno="&fno&"&pno="&pno&"&pgno="&pgno&"&ptype="&Server.URLEncode(ptype)&"&dtime="&dtime&"&cno="&cno&"&cgno="&cgno&"&fromdt="&fromdt&"&todt="&todt
end if
set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
if dtime="A" or dtime="" then
  set retcol = objBakery.GetCustomers("A",1,0)
else
  if dtime = "-1" then 
    dtime =0
  else
    if instr(1,dtime,"~") >  0 then
      a1 = split(dtime,"~")
      dtime=a1(0)
    else
      dtime=0
    end if
  end if
  set retcol = objBakery.getcustomers("A",1,0,dtime)
end if
recarray1 = retcol("Customers")
set retcol = objBakery.GetCustomerGroups("A",1,0)
recarray2 = retcol("CustomerGroups")
if fno = "-1" or fno="" then
  set retcol = objBakery.GetProducts("A",1,0)
else
  if instr(1,fno,"~") >  0 then
    a1=split(fno,"~")
    fno=a1(0)
  else
    fno=0
  end if
  set retcol = objBakery.getproducts("A",1,0,fno)
end if
recarray3 = retcol("Products")
set retcol = objBakery.GetProductGroups("A",1,0)
recarray4 = retcol("ProductGroups")
set retcol = objBakery.GetFacilities("A",1,0)
recarray5 = retcol("Facilities")
set retcol = objBakery.GetDeliveryTimes("A",1,0)
recarray6 = retcol("DeliveryTimes")

dim apno, apgno, acno, acgno
apno = Request.Form("chkpno")
if apno <> "" and apno <> "-1" then
  apno = split(apno,",")
end if

apgno = Request.Form("chkpgno")
if apgno <> "" and apgno <> "-1" then
  apgno = split(apgno,",")
end if

acno = Request.Form("chkcno")
if acno <> "" and acno <> "-1" then
  acno = split(acno,",")
end if

acgno = Request.Form("chkcgno")
if acgno <> "" and acgno <> "-1" then
  acgno = split(acgno,",")
end if

%>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<form action="report_sales.asp" method="post" name="form1" onSubmit="return IsDataValid();">
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
     <td width="100%"><b><font size="3">Report - Sales<br>
      &nbsp;</font></b>
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="90%">
        <tr>
          <td><b>Facility</b></td>
          <td>
            <select size="1" name="lstfno" style="font-family: Verdana; font-size: 8pt" onChange="document.form1.submit();">
              <option value="-1">All</option><%
              if isarray(recarray5) then
                for i=0 to ubound(recarray5,2)%>
                  <option <%if clng(fno)=recarray5(0,i) then Response.Write "Selected"%> value="<%Response.Write(recarray5(0,i) & "~" & recarray5(1,i))%>"><%Response.Write(recarray5(1,i))%></option><%
                next
              end if%>
            </select>
          </td>
          <td>&nbsp;
          <!--Goto: <a href="#pg">Product Groups</a> | <a href="#pt">Production Type</a> | <a href="#cg">Customer Groups</a>-->
          </td>
        </tr>
        
        </table>
        <table border="1" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="95%">
        <tr>
          <td colspan="3"><b>Products</b></td>
        </tr>
        <tr>
          <td colspan="3" bgcolor="#000000">
            <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0">
            <tr>
                <td bgcolor="#CCCCCC"><b>ALL</b></td>
                <td width="10" bgcolor="#CCCCCC">
                  <input type="checkbox" name="chkpno" value="-1" <%
                  if not isarray(apno) then
                    if apno = "-1" then Response.Write "Checked"
                  end if%>
                  style="font-family: Verdana; font-size: 8pt"></td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
              </tr><%
              if isarray(recarray3) then
                for i=0 to ubound(recarray3,2) step 5%>
                <tr>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray3(1,i)) = ""  then Response.Write("no name") else Response.Write(mid(recarray3(1,i),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkpno" value="<%=recarray3(0,i)%>" <%
                if isarray(apno) then
                for j=0 to ubound(apno)
                  if trim(apno(j))=trim(recarray3(0,i)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%if i+1 <= ubound(recarray3,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray3(1,i+1)) = ""  then Response.Write("no name") else Response.Write(mid(recarray3(1,i+1),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkpno" value="<%=recarray3(0,i+1)%>" <%
                if isarray(apno) then
                for j=0 to ubound(apno)
                  if trim(apno(j))=trim(recarray3(0,i+1)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+2 <= ubound(recarray3,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray3(1,i+2)) = ""  then Response.Write("no name") else Response.Write(mid(recarray3(1,i+2),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkpno" value="<%=recarray3(0,i+2)%>" <%
                if isarray(apno) then
                for j=0 to ubound(apno)
                  if trim(apno(j))=trim(recarray3(0,i+2)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+3 <= ubound(recarray3,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray3(1,i+3)) = ""  then Response.Write("no name") else Response.Write(mid(recarray3(1,i+3),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkpno" value="<%=recarray3(0,i+3)%>" <%
                if isarray(apno) then
                for j=0 to ubound(apno)
                  if trim(apno(j))=trim(recarray3(0,i+3)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+4 <= ubound(recarray3,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray3(1,i+4)) = ""  then Response.Write("no name") else Response.Write(mid(recarray3(1,i+4),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkpno" value="<%=recarray3(0,i+4)%>" <%
                if isarray(apno) then
                for j=0 to ubound(apno)
                  if trim(apno(j))=trim(recarray3(0,i+4)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                </tr><%
                next
              end if%>
            </table></td>
        </tr>
        <tr>
          <td colspan="3">&nbsp;</td>
        </tr>
         <tr>
          <td colspan="3"><b>Product Groups</b>
          <a id="pg"></a>
          </td>
        </tr>
        <tr>
          <td colspan="3" bgcolor="#000000">
            <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0">
            <tr>
                <td bgcolor="#CCCCCC"><b>ALL</b></td>
                <td width="10" bgcolor="#CCCCCC">
                   <input type="checkbox" name="chkpgno" value="-1" <%
                   if not isarray(apgno) then
                     if apgno = "-1" then Response.Write "Checked"
                   end if%>
                   style="font-family: Verdana; font-size: 8pt">
                </td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
              </tr><%
              if isarray(recarray4) then
                for i=0 to ubound(recarray4,2) step 5%>
                <tr>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray4(1,i)) = ""  then Response.Write("no name") else Response.Write(mid(recarray4(1,i),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkpgno" value="<%=recarray4(0,i)%>" <%
                if isarray(apgno) then
                for j=0 to ubound(apgno)
                  if trim(apgno(j))=trim(recarray4(0,i)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%if i+1 <= ubound(recarray4,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray4(1,i+1)) = ""  then Response.Write("no name") else Response.Write(mid(recarray4(1,i+1),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkpgno" value="<%=recarray4(0,i+1)%>" <%
                if isarray(apgno) then
                for j=0 to ubound(apgno)
                  if trim(apgno(j))=trim(recarray4(0,i+1)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+2 <= ubound(recarray4,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray4(1,i+2)) = ""  then Response.Write("no name") else Response.Write(mid(recarray4(1,i+2),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkpgno" value="<%=recarray4(0,i+2)%>" <%
                if isarray(apgno) then
                for j=0 to ubound(apgno)
                  if trim(apgno(j))=trim(recarray4(0,i+2)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+3 <= ubound(recarray4,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray4(1,i+3)) = ""  then Response.Write("no name") else Response.Write(mid(recarray4(1,i+3),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkpgno" value="<%=recarray4(0,i+3)%>" <%
                if isarray(apgno) then
                for j=0 to ubound(apgno)
                  if trim(apgno(j))=trim(recarray4(0,i+3)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+4 <= ubound(recarray4,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray4(1,i+4)) = ""  then Response.Write("no name") else Response.Write(mid(recarray4(1,i+4),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkpgno" value="<%=recarray4(0,i+4)%>" <%
                if isarray(apgno) then
                for j=0 to ubound(apno)
                  if trim(apgno(j))=trim(recarray4(0,i+4)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                </tr><%
                next
              end if%>
            </table></td>
        </tr>
        <tr>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
          <td><b>Production type&nbsp;</b>
          <a id="pt"></a>
          </td>
          <td><b>&nbsp;</b></td>
          <td><b>Delivery time</b></td>
        </tr>
        <tr>
          <td><select size="1" name="lstptype" style="font-family: Verdana; font-size: 8pt">
              <option value="A">All</option>
              <option value="N">Net Sales</option>
              <option value="C">Credits</option>
              <option value="S">Samples</option>
            </select></td>
          <td>&nbsp;</td>
          <td>
            <select onChange="document.form1.submit();" size="1" name="lstdtime" style="font-family: Verdana; font-size: 8pt">
            <option selected value="-1">All</option><%
            if isarray(recarray6) then
              for i=0 to ubound(recarray6,2)%>
                <option <%if clng(dtime)=recarray6(0,i) then Response.Write "Selected"%> value="<%Response.Write(recarray6(0,i) & "~" & recarray6(1,i))%>"><%=recarray6(1,i)%></option><%
              next
            end if%>
            </select>
            </td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
        </tr>
       <tr>
          <td colspan="3"><b>Customers</b></td>
        </tr>
        <tr>
          <td colspan="3" bgcolor="#000000">
            <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0">
            <tr>
                <td bgcolor="#CCCCCC"><b>ALL</b></td>
                <td width="10" bgcolor="#CCCCCC">
                <input type="checkbox" name="chkcno" value="-1" <%
                if not isarray(acno) then
                  if acno = "-1" then Response.Write "Checked"
                end if%>
                style="font-family: Verdana; font-size: 8pt"></td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
              </tr><%
              if isarray(recarray1) then
                for i=0 to ubound(recarray1,2) step 5%>
                <tr>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray1(1,i)) = ""  then Response.Write("no name") else Response.Write(mid(recarray1(1,i),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkcno" value="<%=recarray1(0,i)%>" <%
                if isarray(acno) then
                for j=0 to ubound(acno)
                  if trim(acno(j))=trim(recarray1(0,i)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%if i+1 <= ubound(recarray1,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray1(1,i+1)) = ""  then Response.Write("no name") else Response.Write(mid(recarray1(1,i+1),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkcno" value="<%=recarray1(0,i+1)%>" <%
                if isarray(acno) then
                for j=0 to ubound(acno)
                  if trim(acno(j))=trim(recarray1(0,i+1)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+2 <= ubound(recarray1,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray1(1,i+2)) = ""  then Response.Write("no name") else Response.Write(mid(recarray1(1,i+2),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkcno" value="<%=recarray1(0,i+2)%>" <%
                if isarray(acno) then
                for j=0 to ubound(acno)
                  if trim(acno(j))=trim(recarray1(0,i+2)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+3 <= ubound(recarray1,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray1(1,i+3)) = ""  then Response.Write("no name") else Response.Write(mid(recarray1(1,i+3),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkcno" value="<%=recarray1(0,i+3)%>" <%
                if isarray(acno) then
                for j=0 to ubound(acno)
                  if trim(acno(j))=trim(recarray1(0,i+3)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+4 <= ubound(recarray1,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray1(1,i+4)) = ""  then Response.Write("no name") else Response.Write(mid(recarray1(1,i+4),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkcno" value="<%=recarray1(0,i+4)%>" <%
                if isarray(acno) then
                for j=0 to ubound(acno)
                  if trim(acno(j))=trim(recarray1(0,i+4)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                </tr><%
                next
              end if%>
            </table></td>
        </tr>
        <tr>
          <td colspan="3">&nbsp;</td>
        </tr>
         <tr>
          <td colspan="3"><b>Customer Groups</b>
          <a id="cg"></a>
          </td>
        </tr>
        <tr>
          <td colspan="3" bgcolor="#000000">
            <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0">
            <tr>
                <td bgcolor="#CCCCCC"><b>ALL</b></td>
                <td width="10" bgcolor="#CCCCCC">
                  <input type="checkbox" name="chkcgno" value="-1" <%
                  if not isarray(acgno) then
                    if acgno = "-1" then Response.Write "Checked"
                  end if%>
                  style="font-family: Verdana; font-size: 8pt">
                </td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
              </tr><%
              if isarray(recarray2) then
                for i=0 to ubound(recarray2,2) step 5%>
                <tr>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray2(1,i)) = ""  then Response.Write("no name") else Response.Write(mid(recarray2(1,i),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkcgno" value="<%=recarray2(0,i)%>" <%
                if isarray(acgno) then
                for j=0 to ubound(acgno)
                  if trim(acgno(j))=trim(recarray2(0,i)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%if i+1 <= ubound(recarray2,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray2(1,i+1)) = ""  then Response.Write("no name") else Response.Write(mid(recarray2(1,i+1),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkcgno" value="<%=recarray2(0,i+1)%>" <%
                if isarray(acgno) then
                for j=0 to ubound(acgno)
                  if trim(acgno(j))=trim(recarray2(0,i+1)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+2 <= ubound(recarray2,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray2(1,i+2)) = ""  then Response.Write("no name") else Response.Write(mid(recarray2(1,i+2),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkcgno" value="<%=recarray2(0,i+2)%>" <%
                if isarray(acgno) then
                for j=0 to ubound(acgno)
                  if trim(acgno(j))=trim(recarray2(0,i+2)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+3 <= ubound(recarray2,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray2(1,i+3)) = ""  then Response.Write("no name") else Response.Write(mid(recarray2(1,i+3),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkcgno" value="<%=recarray2(0,i+3)%>" <%
                if isarray(acgno) then
                for j=0 to ubound(acgno)
                  if trim(acgno(j))=trim(recarray2(0,i+3)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+4 <= ubound(recarray2,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray2(1,i+4)) = ""  then Response.Write("no name") else Response.Write(mid(recarray2(1,i+4),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkcgno" value="<%=recarray2(0,i+4)%>" <%
                if isarray(acgno) then
                for j=0 to ubound(acgno)
                  if trim(acgno(j))=trim(recarray2(0,i+4)) then 
                    Response.Write "Checked"
                    exit for
                  end if
                next
                end if%>
                style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                </tr><%
                next
              end if%>
            </table></td>
        </tr>
        <tr>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3">
		  <table width="100%"  border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
          
		
		<tr>
          <td class="button"><b>From </b></td>
          <td><b>To</b></td>
          <td></td>
        </tr>
        <tr>
          <td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form1.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form1.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form1.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom size="12" onFocus="blur();" name="txtfrom" value="<%=replace(fromdt,"from","")%>" onChange="setCalCombo(this.form,document.form1.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form1.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form1.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form1.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto  name="txtto" onFocus="blur();" size="12" value="<%=replace(todt,"to","")%>" onChange="setCalCombo1(this.form,document.form1.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td><td width="100">
          <input type="submit"  value="Compile Report" name="Search" style="font-family: Verdana; font-size: 8pt">
          </td>
        </tr>
        </table>
     	</td>
		</tr>
      </table>
    </td>
  </tr>
  
</table>
</form>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
  </center>
</div>
<br><br><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
</body>
</html>