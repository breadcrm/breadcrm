<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
	dim fromdtr,todtr
	dim objRep
	
	fromdt=request("dt")
	sortOrder=request("sort")
	
	dim arr
	arr = Split(fromdt,"/")
	fromdtr = arr(2) & "-" & arr(1) & "-" & arr(0)
	
	
	set objRep = server.CreateObject("Bakery.Reports")
	objRep.SetEnvironment(strconnection)
	set retcol = objRep.SalesHistoryReport(fromdt,CInt(sortOrder))
	recarray = retcol("Report")

	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=7_day_check_rep.xls" 
	%>
      <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%"  colspan="11"><b>7 Day Check Report For the End Date &lt;<%=fromdt%>&gt; <% if strStatus<> "" then%> - <%=strStatusLabel%><%end if%></b></td>
        </tr>
        <tr>
          <td width="10%" bgcolor="#CCCCCC"><b>Cus. No&nbsp;</b></td>
          <td width="35%" bgcolor="#CCCCCC"><b>Customer Name&nbsp;</b></td>
          <td width="5%" bgcolor="#CCCCCC"><b>Day -7</b></td>        
          <td width="5%" bgcolor="#CCCCCC"><b>Day -6</b></td>
          <td width="5%" bgcolor="#CCCCCC"><b>Day -5</b></td>
          <td width="5%" bgcolor="#CCCCCC"><b>Day -4</b></td>   
          <td width="5%" bgcolor="#CCCCCC"><b>Day -3</b></td>
          <td width="5%" bgcolor="#CCCCCC"><b>Day -2</b></td>
          <td width="5%" bgcolor="#CCCCCC"><b>Day -1</b></td>
          <td width="5%" bgcolor="#CCCCCC"><b>Day 0</b></td>
          <td width="5%" bgcolor="#CCCCCC"><b>Day 1</b></td>
        </tr>
		
		<%if isarray(recarray) then%>
				<%for i=0 to UBound(recarray,2)%>
					<tr>
						<td><%=recarray(0,i)%></td>
						<td><%=recarray(1,i)%></td>
						<td align="right"><%=FormatNumber(recarray(2,i),2)%></td> 
						<td align="right"><%=FormatNumber(recarray(3,i),2)%></td>
						<td align="right"><%=FormatNumber(recarray(4,i),2)%></td>
						<td align="right"><%=FormatNumber(recarray(5,i),2)%></td>
						<td align="right"><%=FormatNumber(recarray(6,i),2)%></td> 
						<td align="right"><%=FormatNumber(recarray(7,i),2)%></td>
						<td align="right"><%=FormatNumber(recarray(8,i),2)%></td>
						<td align="right"><%=FormatNumber(recarray(9,i),2)%></td>	
						<td align="right"><%=FormatNumber(recarray(10,i),2)%></td>
					</tr>
				<%next%>
          <%else%>
			<tr>
				<td colspan="6" width="100%" bgcolor="#CCCCCC"><b>0 - No Records Found...</b></td>
			</tr>
          <%end if%>
      </table>
</body>
</html>
