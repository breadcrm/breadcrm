<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%
strSearchSubCustomerGroup = replace(Request.Form("SearchSubCustomerGroup"),"'","''")
strSubGroupName = replace(Request.Form("SubGroupName"),"'","''")
strSubGroupName = replace(Request.Form("SubGroupName"),"'","''")


if request.form("delete")="yes" then
	Set obj = server.CreateObject("bakery.customer")
	obj.SetEnvironment(strconnection)
	vSGNo=request.form("sgno")
	vSGName=request.form("sgname")
	strDelete = obj.DeleteSubGroup(vSGNo)
	LogAction "Customer Sub Group Deleted", "No: " & vSGNo , ""
	set obj=nothing
end if
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{

  if (theForm.SubGroupName.value == "")
  {
    alert("Please enter a value for the \"Sub Group Name\" field.");
    theForm.SubGroupName.focus();
    return (false);
  }

  return (true);
}

function validate(frm){
	if(confirm("Would you like to delete " + frm.sgname.value + " ?")){
		frm.submit();		
	}
}
//-->
</Script>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<br>
<table border="0" width="95%" cellspacing="0" align="center" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
<tr>
   <td width="100%"><b><font size="3">Sub Groups</font></b></td>
  </tr>
</table>
<br>
<table border="0" align="center" width="500" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%" align="center"><b><font size="3">List of Customer Sub Groups</font></b><br>
        <br>
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" width="100%" bordercolor="#111111">
		<form method="post" action="SubGroup.asp" name="frmForm">      
        <tr>
           <td align="center"><b> Customer Sub Group:</b> <input type="text" name="SearchSubCustomerGroup" value="<%=strSearchSubCustomerGroup%>" size="20" style="font-family: Verdana; font-size: 8pt">
          <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
       </form>
	   <tr>
	  	 <td height="25" valign="bottom"><a href="SubGroupAdd.asp"><strong><font color="#000099">Add New Sub Group</font></strong></a></td>
	   </tr>
	   <% if strDelete="Ok" then%>
	   <tr>
	  	 <td height="25" align="center"><font color="#FF0000">Sub Group <b><%=vSGName%></b> deleted successfully</font></td>
	   </tr>
	   <% elseif strDelete="Fail" then%>
	   <tr>
	  	 <td height="25" align="center"><font color="#FF0000">Sub Group <b><%=vSGName%></b> can not be deleted, as it is already assigned to a customer (s).</font></td>
	   </tr>
	   <%end if%>
      </table>
		<br>
      <table border="0" width="750" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" bgcolor="#999999">

	<%
	vPageSize = 999999
	vpagecount = Request.Form("pagecount") 
	
	select case Request.Form("Direction")
		case ""
			session("Currentpage")  = 1
		case "Next"
			if clng(session("Currentpage")) < clng(vpagecount) then	
				session("Currentpage")  = session("Currentpage")+1
			End if 
		case "Previous"
			if session("Currentpage") > 1 then
				session("Currentpage")  = session("Currentpage")-1
			end if
		case "First"
			session("Currentpage") = 1
		case "Last"
			session("Currentpage")  = Request.Form("pagecount")
	end select
	vSessionpage = session("Currentpage")
	vsessionpage = 0
	set objBakery = server.CreateObject("Bakery.reports")
	objBakery.SetEnvironment(strconnection)
	set CustomerCol = objBakery.SubGroupList(strSearchSubCustomerGroup)
	arCustomerGroup = CustomerCol("SubGroup")
	set CustomerCol = nothing
	set objBakery = nothing
	If isArray(arCustomerGroup) Then
		vRecordcount=ubound(arCustomerGroup,2)+1
	%>
 		<tr bgcolor="#FFFFFF">
			<td colspan="4" align = "left" height="35"><b>Total number of Customer Sub Groups: <%=vRecordcount%></b></td>
		</tr>	
   		<tr height="25">
          <td bgcolor="#CCCCCC" width="30" align="center"><b>No</b></td>
          <td bgcolor="#CCCCCC" width="180"><b> Sub Group Name</b></td>
          <td bgcolor="#CCCCCC"><b>Customer Code with Name</b></td>
		  <td bgcolor="#CCCCCC" align="center" width="150"><b>Action</b></td>
		
        </tr>
<% 
		set objBakery1 = server.CreateObject("Bakery.Customer")
		objBakery1.SetEnvironment(strconnection)
		For i = 0 To ubound(arCustomerGroup,2)
			set CustomerGroupCol = objBakery1.CustomersFromCustomerSubGroup(arCustomerGroup(0,i))
		    strCustomerGroup = CustomerGroupCol("Customers")
			if (i mod 2 =0) then
				strBgColour="#FFFFFF"
			else
				strBgColour="#E6E6E6"
			end if
%>         
        <tr bgcolor="<%=strBgColour%>">
              <td align="left" valign="top"><b><%=arCustomerGroup(0,i)%></b></td>
              <td valign="top"><%=arCustomerGroup(1,i)%></td>
              <td valign="top"><%=strCustomerGroup%></td>
			  <td align="center">
			   <table cellpadding="0" cellspacing="0" border="0" width="100%">
			   <tr>
			   <form method="post" action="SubGroupAdd.asp">
			   <td width="75" align="center">
			    <input type = "hidden" name="sgno" value = "<%=arCustomerGroup(0,i)%>">
				<input type = "hidden" name="sgname" value = "<%=arCustomerGroup(1,i)%>">
				<input type="submit" value=" Edit " name="B1" style="font-family: Verdana; font-size: 8pt">
			   </td>
			   </form>
			   <form method="POST" action="SubGroup.asp">
			  	<td width="75" align="center">
				  <input type = "hidden" name="sgno" value = "<%=arCustomerGroup(0,i)%>">
				  <input type = "hidden" name="sgname" value = "<%=arCustomerGroup(1,i)%>">
				  <input type = "hidden" name="delete" value = "yes">
				  <input type="button" onClick="validate(this.form);" value="Delete" name="B1" style="font-family: Verdana; font-size: 8pt">
				 
			  </td>
			   </form>
			  </tr>
			  </table>
			  </td>	
        </tr>
<%
		Next
		set CustomerGroupCol = nothing
		set objBakery1 = nothing
	Else
%>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">Sorry no items found</td>
        </tr>
<%End if%>
      </table>
    </td>
  </tr>
</table>
<br><br>
</form>
</body>
</html>