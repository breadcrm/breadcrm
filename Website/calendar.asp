<%@ LANGUAGE="VBSCRIPT" %>
<% Response.Buffer = True %>
<% Response.Expires = -1 %>
<% Response.AddHeader "PRAGMA", "NO-CACHE" %>
<%
If Request.Querystring("Page") <> "" Then
   PageName = Request.Querystring("Page")
   Session("PageName") = PageName
Else
   PageName = Session("PageName")
End If
If Request.Querystring("Form") <> "" Then
   FormName = Request.Querystring("Form")
   Session("FormName") = FormName
Else
   FormName = Session("FormName")
End If
If Request.Querystring("Element") <> "" Then
   ElementName = Request.Querystring("Element")
   Session("ElementName") = ElementName
Else
   ElementName = Session("ElementName")
End If
%>
<html> 
<head> 
<title>Calendar</title> 
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<script LANGUAGE="javascript"> 
function calpopulate(dte){
	window.opener.document.<%=formname & "." & elementname%>.value = dte;
	window.opener.document.<%=formname & "." & elementname%>.focus();
	self.close()
} 
</script>
<% 

If IsDate(Request.QueryString("Date")) Then 
BuildDate=Request.QueryString("Date") 
Else 

If Request.Querystring("BMonth") = "" Then 
BMonth = Month(Now) 
Else 
BMonth = Request.Querystring("BMonth") 
End If 

If Request.QueryString("BYear") <> "" Then 
	BuildDate = "1" & "/" & BMonth & "/" & Request.QueryString("BYear") 
Else 
BuildDate = "1" & "/" & BMonth & "/" & Right(Year(Now), 2) 
End If 

End If 

Session("CurrentDate")=BuildDate 

'This gives the position of weekday for that date 
BuildDayValue = Weekday(BuildDate) 

CurrentMonth = Month(BuildDate) 
%> 
</head> 
<body bgcolor="#ffffff" onBlur="javascript:self.focus ();" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
<% 
'BuildDate=DateAdd("d", -1, BuildDate) 
If CurrentMonth < 12 then 
	NextMonth=CurrentMonth+1 & "&BYear=" & Year(BuildDate) 
Else 
	NextMonth="1&BYear=" & Year(DateAdd ("yyyy", 1, BuildDate)) 
End if 

If CurrentMonth > 1 then 
	PreviousMonth=CurrentMonth-1 & "&BYear=" & Year(BuildDate) 
Else 
	PreviousMonth= "12&BYear=" & Year(DateAdd ("yyyy", -1, BuildDate)) 
End If 
%> 
<table border="0" cellpadding="1" cellspacing="1" width="210" align="center">
<tr>
	<td colspan="7">
		<font face="Verdana" size="2"><b>
			<%=MonthName(CurrentMonth)%>&nbsp;<%=Year(BuildDate)%>
		</b></font>
	</td>
</tr> 
<tr> 
	<td width="30" bgcolor="#C0C0C0"><b><font face="Verdana" size="1" color="#000000">Sun</font></b></td>
	<td width="30" bgcolor="#C0C0C0"><b><font face="Verdana" size="1" color="#000000">Mon</font></b></td>
	<td width="30" bgcolor="#C0C0C0"><b><font face="Verdana" size="1" color="#000000">Tue</font></b></td>
	<td width="30" bgcolor="#C0C0C0"><b><font face="Verdana" size="1" color="#000000">Wed</font></b></td>
	<td width="30" bgcolor="#C0C0C0"><b><font face="Verdana" size="1" color="#000000">Thu</font></b></td>
	<td width="30" bgcolor="#C0C0C0"><b><font face="Verdana" size="1" color="#000000">Fri</font></b></td>
	<td width="30" bgcolor="#C0C0C0"><b><font face="Verdana" size="1" color="#000000">Sat</font></b></td> 
</tr> 
<tr> 
<% 
DayPosition=1 
'Now loop through table build with blanks until first day of month 
'is in position 
For I = 1 to BuildDayValue-1 
%> 
	<td width="30" bgcolor="#CCCCCC"><font face="Verdana" size="1">&nbsp;</font></td> 
<% 
DayPosition=DayPosition+1 
Next 

Do Until CurrentMonth <> Month(BuildDate) 
	While DayPosition<>8
	
	month1 = Month(BuildDate)
	if len(month1) = 1 then
		month1 = "0" & month1
	end if
	day1 = Day(BuildDate)
	if len(day1) = 1 then
		day1 = "0" & day1
	end if


%>
<td width="30" <%If Day(BuildDate)=Day(Now) Then Response.Write "bgcolor=#CCCCCC" Else Response.Write  "bgcolor=#CCCCCC" %>>
<font face="Verdana" size="1">
<a href="javascript:onClick=<%Response.Write "calpopulate('" & Day1 & "/" & Month1 & "/" & Year(BuildDate) %>')" style="font-family: Verdana; color: #000000; font-weight: bold"><%=Day(BuildDate)%></a>
</font>
</td> 
<% 
	DayPosition=DayPosition+1 
	BuildDate=DateAdd("d", 1, BuildDate) 
	If CurrentMonth <> Month(BuildDate) then 
		DayPosition=8 
	End If 
Wend 
DayPosition=1 
%> 
</tr> 
<% 
Loop 
%> 
</table> 

<table border="0" cellpadding="2" cellspacing="0" width="210" align="center">
<tr>
	<td><a href="calendar.asp?BMonth=<%=PreviousMonth%>&Element=<%=ElementName%>"><img src="images/back.gif" border="0" WIDTH="34" HEIGHT="14"></a> </td>
	<td align="right"><a href="calendar.asp?BMonth=<%=NextMonth%>&Element=<%=ElementName%>"><img src="images/forward.gif" border="0" WIDTH="34" HEIGHT="14"></td>
</tr> 
</table>
<br>
</body> 
</html> 