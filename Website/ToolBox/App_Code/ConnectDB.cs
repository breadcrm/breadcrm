﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

/// <summary>
/// Summary description for ConnectDB
/// </summary>
public class ConnectDB
{
    private string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
    SqlConnection conn;
    private string strSql = string.Empty;
    string message = string.Empty;

 	public ConnectDB()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    /// <summary>
    /// This method is used to List Customer of perticular Date
    /// </summary>
    public DataTable DataList(string spName,string paramenter)
    {
        conn = new SqlConnection(strConnectionString);
        DataTable dt = new DataTable();
       
        try
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();

            strSql = "exec "+ spName + " " + paramenter;

            SqlDataAdapter da = new SqlDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);
        }
        catch (Exception ex)
        {
            message = ex.Message;
        }
        finally
        {
            conn.Close();
        }


        return dt;
    }


}
