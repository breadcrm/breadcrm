﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //lblMessage.Text = "";
            //LoadDates();

            //DateTime TommorowDate = DateTime.Today.AddDays(1);

            //ddlYear.SelectedValue = TommorowDate.Year.ToString();
            //ddlMonth.SelectedValue = TommorowDate.Month < 10 ? "0" + TommorowDate.Month.ToString() : TommorowDate.Month.ToString();
            //ddlDay.SelectedValue = TommorowDate.Day.ToString();

            string stryear = Request.QueryString["syear"].ToString();
            string strmonth = Request.QueryString["smonth"].ToString();
            string strdate = Request.QueryString["sdate"].ToString();

            strmonth =  int.Parse(strmonth) < 10 ? "0" + strmonth : strmonth;

            if (stryear != "" && strmonth != "" && strdate != "")
            {
                CreateDATFile obj = new CreateDATFile();
                try
                {
                    obj.WriteCustomerFile(stryear + "-" + strmonth + "-" + strdate);
                    obj.WriteProductFile(stryear + "-" + strmonth + "-" + strdate);

                    string Day = strdate.Length == 1 ? "0" + strdate : strdate;

                    obj.WriteOrderFile(stryear + "-" + strmonth + "-" + strdate,
                        stryear.Substring(2, 2) + strmonth + Day);

                    obj.WriteRouteFile(stryear + "-" + strmonth + "-" + strdate,
                        stryear.Substring(2, 2) + strmonth + Day);

                    obj.WriteLog("Files successfuly exported.");
                }
                catch (Exception err)
                {
                    obj.WriteLog(err.Message.ToString());
                }
            }
        }        
        
    }    
}
