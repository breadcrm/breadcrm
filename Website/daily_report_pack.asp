<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
deldate= request.form("deldate")
van= request.form("van")
dtype=Request.Form("deltype")
if deldate= "" or van = "" or dtype="" then response.redirect "daily_report.asp"

set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
set DisplayDailyPackingSheet= object.DisplayDailyPackingSheet(deldate,van,dtype)
vRecArray = DisplayDailyPackingSheet("Packingsheet")
set DisplayDailyPackingSheet= nothing
set object = nothing
Dim mPage,mLine

mLine = 7
if isarray(vRecArray) then 
	mPage = 1
	cno = vRecArray(2,i)
	for i = 0 to ubound(vRecArray,2)
	newcno = vRecArray(2,i)      
	if cno <> newcno or i = 0 then
		cno = newcno               
	
		if i <>0 then	 
			mLine = mline  + 3			
			if mLine >= PrintPgSize and i < ubound(vRecArray,2) Then 
				mPage = mPage + 1
				mLine = 0
			End if
		end if  	   
		mLine = mLine + 2
	end if
	mLine = mLine + 1	
	if mLine >= PrintPgSize and i < ubound(vRecArray,2) Then 
		mPage = mPage + 1
		mLine = 0		
	End if	
	next
Else
	mPage = 1		
end if
i=0%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; }
-->
</style>



</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<div align="center">     
<table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
<td>
<p align="center"><b><font size="3">Packing Sheet - <%=dtype%></font><b></p>
</td>
</tr>
<tr>
<td>
<br>
<br>
Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
To be packed on: <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br>
VAN: <%=van%><br>
</td>
</tr>

<tr>
<td>
Number of pages for this report: <%=mPage%><BR>&nbsp; 
</td>
</tr>


</table>
</div>
<%mLine = 7%>

<%
if isarray(vRecArray) then 
	mPage = 1
	cno = vRecArray(2,i)
	for i = 0 to ubound(vRecArray,2)
	newcno = vRecArray(2,i)      
	if cno <> newcno or i = 0 then
	cno = newcno            
	%>        
	<%if i <>0 then%>
	<tr>
	<td width="15%" height="20"></td>
	<td width="45%" height="20"><p align="right"><b>TOTAL:&nbsp;&nbsp; </b></p></td>
	<td width="15%" bgcolor="#CCCCCC" height="20"><%=total%>&nbsp;</td>
	<td width="25%" bgcolor="#CCCCCC" height="20">&nbsp;</td>
	</tr>
	<tr>
	<td width="15%" height="20">
	<td width="45%" height="20">
	<td width="15%" height="20"><b>TOTAL PIECES IN PACKAGE:&nbsp;&nbsp; </b></td>
	<td width="25%" bgcolor="#CCCCCC" height="20"></td>
	</tr>		
	</table>
</div></br>	<% 
	mLine = mline  + 3
	total = 0
	if mLine >= PrintPgSize and i < ubound(vRecArray,2) Then 
		mPage = mPage + 1
		mLine = 0%>		
		<br class="page" /><%
	End if%>
	<%end if%>  	   
<div align="center">      
<table border="2" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
	<tr>
	<td width="15%" height="20"><b>Customer Code</b></td>
	<td width="45%" height="20"><b><%=vRecArray(2,i)%></b></td>
	<td width="15%" height="20"><b>Customer Name</b></td>
	<td width="25%" height="20"><b><%=vRecArray(3,i)%></b></td>
	</tr>
	<tr>
	<td width="15%" height="20"><b>Product code</b></td>
	<td width="45%" height="20"><b>Product name</b></td>
	<td width="15%" height="20"><b>Quantity</b></td>
	<td width="25%" height="20"><b>Short in delivery</b></td>
	</tr>
	<%
	mLine = mLine + 3
	end if
	%>
	<tr>
	<td width="15%" height="20"><%=vRecArray(4,i)%></td>
	<td width="45%" height="20"><%=vRecArray(5,i)%></td>
	<td width="15%" height="20"><%=vRecArray(7,i)%></td>
	<td width="25%" height="20"></td>
	</tr><%
	mLine = mLine + 1
	total = total + vRecArray(7,i) 	
	if mLine >= PrintPgSize and i < ubound(vRecArray,2) Then 
		mPage = mPage + 1
		mLine = 0%>		
		</table>
		</div>
		<br class="page" />
		<div align="center">      
		<table border="2" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%		
	End if
	
	next
	%>
	<%
	if total>0 then
	%>
	<tr>
	<td width="15%" height="20"></td>
	<td width="45%" height="20"><p align="right"><b>TOTAL:&nbsp;&nbsp; </b></p></td>
	<td width="15%" bgcolor="#CCCCCC" height="20"><%=total%>&nbsp;</td>
	<td width="25%" bgcolor="#CCCCCC" height="20">&nbsp;</td>
	</tr>
	<tr>
	<td width="15%" height="20">
	<td width="45%" height="20">
	<td width="15%" height="20"><b>TOTAL PIECES IN PACKAGE:&nbsp;&nbsp; </b></td>
	<td width="25%" bgcolor="#CCCCCC" height="20"></td>
	</tr>
</table>
</div>
<% if i < ubound(vRecArray,2) Then %>
<br class="page" />
<% End if %>
	<%
	end if
	else
	%>
<div align="center">      
<table border="2" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
	<tr>
	<td align = "center" width="100%" height="20">Sorry no items found</td>
	</tr>
</table>
</div>
<%
end if
%>
</body>
</html>