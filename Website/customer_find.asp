<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>


<script Language="JavaScript">
<!--
function validate(frm){
	if(confirm("Would you like to delete " + frm.name.value + " ?")){
		frm.submit();		
	}
}
//-->
</Script>

</head>

<%
Dim Obj,ObjCustomer
Dim arCustomer
Dim arGroup

Dim strCustomerName,strCustNo,strGroup
Dim vPageSize
Dim strDirection
Dim vpagecount
Dim vCurrentPage
Dim i,StrOrderType
Dim vParaCustNo
dim vCno
dim delete
dim vRecordCount
Dim strProdCode

vPageSize = 50
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vCurrentPage	= Request.Form("CurrentPage") 


StrOrderType = replace(Request.Form("ordertype"),"'","''")
strCustomerName = replace(Request.Form("txtCustName"),"'","''")
strCustNo = replace(Request.Form("txtCustNo"),"'","''")
strGroup = replace(Request.Form("txtGroup"),"'","''")
strProdCode = Trim(replace(Request.Form("txtProdCode"),"'","''"))


If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if
if strCustNo <> "" Then
	if not IsNumeric(strCustNo) Then
		vParaCustNo = 0
	Else
		vParaCustNo = strCustNo 	
	End if  
End if
if StrOrderType = "" then
	StrOrderType = "R"
end if

Set Obj = CreateObject("Bakery.Customer")
Obj.SetEnvironment(strconnection)
if request.form("delete")="yes" then
	vcno=request.form("CustNo")
	delete = obj.DeleteCustomer(vcno)
	
	LogAction "Customer Deleted", "No: " & vcno , vcno
end if

'Response.Write vPageSize & "," & vCurrentPage & "," & strCustomerName & "," & vParaCustNo & "," & strGroup & "," & StrOrderType
stop
Set ObjCustomer = obj.Display_CustomerList(vPageSize,vCurrentPage,strCustomerName,vParaCustNo,strGroup,StrOrderType,strProdCode)
arCustomer = ObjCustomer("Customer")
vpagecount  = ObjCustomer("Pagecount")
vRecordCount = ObjCustomer("Recordcount")

set objCustomer = obj.Display_Grouplist()
arGroup = objCustomer("GroupList")	

Set Obj = Nothing
Set ObjCustomer = Nothing
%>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>  
  
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Find customer<br>
      &nbsp;</font></b>
      <form method="POST"  id="frmCustomer" name="frmCustomer">
				<table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
				  <tr>
				    <td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
				    <td><b>OrderType:</b></td>
				    <td><b>Customer Code:</b></td>
				    <td><b>Customer Name:</b></td>
					<td><b>Customer Group:</b></td>
				    <td><b>Product Code:</b></td>
				    <td></td>
				  </tr>
				  <tr>
				    <td></td>
				    <td><select size="1" name="OrderType">
                    <option value="All" <%if StrOrderType = "All" then response.write "Selected"%>>All</option>
                    <option value="R" <%if StrOrderType = "R" or StrOrderType = "" then response.write "Selected"%>>Regular</option>
                    <option value="S" <%if StrOrderType = "S" then response.write "Selected"%>>Sample</option>
                    </select></td>
				    <td><input type="text" name="txtCustNo"  value="<%=strCustNo%>"size="20" style="font-family: Verdana; font-size: 8pt"></td>
				    <td><input type="text" name="txtCustName" value="<%=strCustomerName%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
				    <td>
							<select size="1" name="txtGroup" style="font-family: Verdana; font-size: 8pt">
							<option value=" ">Select</option><%
								If IsArray(arGroup) then
									For i = 0 to UBound(arGroup,2)%>
										<option value="<%=arGroup(0,i)%>" <%if Trim(arGroup(0,i)) = Trim(strGroup) Then Response.Write "Selected"%>><%=arGroup(1,i)%></option><%
									Next 						
								End if%>
							</select>
				    </td>
				    <td><input type="text" name="txtProdCode" value="<%=strProdCode%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
				    <td>
				      <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
				  </tr>
				  <tr>
				    <td></td>
				    <td></td>
				    <td></td>
				    <td></td>
				    <td></td>
				    <td></td>
				  </tr>
				</table>
			</Form>	
		<table border="0" align="center" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">      
			<%'if vCurrentPage <> 1 then%>
			<tr>
				<td colspan = "2"></td>
			</tr>			
			<%if IsArray(arCustomer) Then%>
				<tr>
	 			  <td align = "left"><b>Total no of customers : <%=vRecordcount%></b></td>
				  <td align="right"><b>Page <%=vCurrentPage%> of <%=vpagecount%></b></td>          
				</tr>
			<%End if%>	
		</table> 
	  	<%'else%>
	  	<!--
				<tr>
					<td align = "right"><b>Total no of customers : <%=vRecordcount%></b></td>
				</tr>			
		-->
		<%'End if%>
    <table border="0" bgcolor="#999999" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
        <tr>
          <td width="15%" bgcolor="#CCCCCC"><b>Customer Code</b></td>
          <td width="60%" bgcolor="#CCCCCC"><b>Customer Name</b></td>

          <td colspan="3" bgcolor="#CCCCCC" align="center"><b>Action</b></td>
		  
        </tr><%
					if IsArray(arCustomer) Then 
						for i = 0 to UBound(arCustomer,2)
							if (i mod 2 =0) then
								strBgColour="#FFFFFF"
							else
								strBgColour="#F3F3F3"
							end if
						%>
							<tr bgcolor="<%=strBgColour%>">
							  <td width="182"><%=arCustomer(0,i)%></td>
							  <td width="293"><%=arCustomer(1,i)%></td>
							 
							  <td align="center">							  
							  <table width="100%" cellpadding="2" border="0" cellspacing="0" align="center">
		  					  <tr>
							  <form method="POST" action="customer_view.asp" name="frmCustList<%=i%>" >
							  <td align="center">
									<input type="submit" value="View" name="B1" style="font-family: Verdana; font-size: 8pt; width:60px">
									<input type="hidden" name="CustNo" value="<%=arCustomer(0,i)%>">			  
							  </td>
							  </form>
							  <%if session("UserType") <> "N" and strconnection<>"vdbArchiveConn" Then%>
							  <form method="POST" action="customer_new.asp">
							  <td align="center">
									<input type="submit" value="Edit" name="B1" style="font-family: Verdana; font-size: 8pt; width:60px">
									<input type="hidden" name="CustNo" value="<%=arCustomer(0,i)%>">			  
							  </td>
							  </form>
							  <%end if%>
							  <form method="POST">
								<td align="center" style="display:none;">	
									<input type = "hidden" name="name" value = "<%=arCustomer(1,i)%>">
									<%if session("UserType") <> "U" and session("UserType") <> "N" and strconnection<>"vdbArchiveConn" Then%>
										<input type = "hidden" name="delete" value = "yes"><input type="button" onClick="validate(this.form);" value="Delete" name="B1" style="font-family: Verdana; font-size: 8pt; width:60px">
										<%
									End if%>
									<input type="hidden" name="CustNo" value="<%=arCustomer(0,i)%>">			  
							  </td>
							  </form>
							  </tr>
							  </table>
							  
							  </td>	
							 						  
							</tr>
							<%
						Next
					else%>
						<tr>
							<td colspan="3" width="570">Sorry no items found</td>
							
						</tr><%
					End if%>        
       
      </table>
    </td>
  </tr>
</table>

	
	<table align="right" border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		<FORM Name="Navigate">
			<tr height="30">					
				<td width="187"><%				
		
					if vCurrentPage <> 1  and vCurrentPage <>0 then%>
						<input type="button" value="FirstPage" Onclick="document.frmFirst.submit()" id=button1 name=button1 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187" ><%
					if clng(vCurrentPage) > 1 then%>
						<input type="button" value="PreviousPage" Onclick="document.frmPrevious.submit()" id=button3 name=button3 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%				
					if clng(vCurrentPage) < vPagecount then%>
						<input type="button" value="NextPage" Onclick="document.frmNext.submit()" id=button5 name=button5 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) <> vPagecount and vPagecount > 0  then%>
						<input type="button" value="LastPage" Onclick="document.frmLast.submit()" id=button7 name=button7 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>					
			</tr>
		</FORM>
	</table>
	
  </center>
</div>


	<FORM METHOD="post" NAME="frmFirst">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="First">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">				
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<INPUT TYPE="hidden" NAME="txtGroup"			VALUE="<%=strGroup%>">
		<INPUT TYPE="hidden" NAME="OrderType"			VALUE="<%=StrOrderType%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"		VALUE="<%=strProdCode%>">
	</FORM>

	<FORM METHOD="post" NAME="frmPrevious">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Previous">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<INPUT TYPE="hidden" NAME="txtGroup"			VALUE="<%=strGroup%>">
		<INPUT TYPE="hidden" NAME="OrderType"			VALUE="<%=StrOrderType%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"		VALUE="<%=strProdCode%>">
	</FORM>

	<FORM METHOD="post" NAME="frmNext">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Next">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<INPUT TYPE="hidden" NAME="txtGroup"			VALUE="<%=strGroup%>">
		<INPUT TYPE="hidden" NAME="OrderType"			VALUE="<%=StrOrderType%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"		VALUE="<%=strProdCode%>">		
	</FORM>

	<FORM METHOD="post" NAME="frmLast">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Last">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<INPUT TYPE="hidden" NAME="txtGroup"			VALUE="<%=strGroup%>">
		<INPUT TYPE="hidden" NAME="OrderType"			VALUE="<%=StrOrderType%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"		VALUE="<%=strProdCode%>">
	</FORM>
</body>

</html><%
if IsArray(arCustomer) Then Erase arCustomer
if IsArray(arGroup) Then Erase arGroup
%>