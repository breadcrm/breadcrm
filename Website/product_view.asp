<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->

<%
vpno= request.form("pno")

if vpno = "" then response.redirect "product_find.asp"

set object = Server.CreateObject("bakery.product")
object.SetEnvironment(strconnection)
set DisplayProductDetail= object.DisplayProductDetail(vpno)
vRecArray1 = DisplayProductDetail("ProductDetail")
vRecArray2 = DisplayProductDetail("ProductAdditiveDetail")
vRecArray3 = DisplayProductDetail("ProductPriceDetail")
set vListofMultipleProducts= object.ListofMultipleProducts(vpno)
vRecArray = vListofMultipleProducts("MultipleProduct")


set DisplayProductGroupDetail= object.DisplayOnlineProductGroupByPNo(vpno)
vRecArray5 = DisplayProductGroupDetail("ProductGroupList")


if vRecArray5 <> null Then

For i = 0 to UBound(vRecArray5,2)

        
	  If strProductGroupList = "" OR strProductGroupList = Empty  Then
	    strProductGroupList = vRecArray5(0,i)
	  Else
	    strProductGroupList = strProductGroupList & " ," & vRecArray5(0,i) 
	  End IF
 
	Next

Else
    strProductGroupList = "All"
End if

set vListofMultipleProducts= nothing
set DisplayProductDetail= nothing
set object = nothing

if not (isarray(vRecArray1)) then response.redirect "product_find.asp"

%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Product Information<br>
      &nbsp;</font></b>
      <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td width="50%" valign="top">
            <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="3" height="260">
              <tr>
                <td width="50%"><strong>Is Multiple Product</strong></td>
                <td width="50%">
				<%
				if isarray(vRecArray1) then 
					strismultipleproduct=vRecArray1(32,0)
					if (strismultipleproduct=1) then
						strismultipleproductvalue="Yes"
					else
						strismultipleproductvalue="No"
					end if	
				end if
				
				%>
				<strong><%=strismultipleproductvalue%></strong>
				</td>
              </tr>
              <tr>
                <td width="50%" height="16"><b><font size="2">Name</font></b></td>
                <td width="50%" height="16"><b><font size="2">
				<%=vRecArray1(1,0)%></font></b>                </td>
              </tr>
              <tr>
                <td width="50%" height="16"><b><font size="2">Code</font></b></td>
                <td width="50%" height="16"><b><font size="2">
				<%=vRecArray1(17,0)%></font></b>                </td>
              </tr>
              <tr>
                <td width="50%" height="16"><b><font size="2">Old Code</font></b></td>
                <td width="50%" height="16"><b><font size="2"><%=vRecArray1(0,0)%></font></b>			  </td>
              </tr>
              <tr>
                <td width="50%" height="13">Dough Name</td>
                <td width="50%" height="13"><%=vRecArray1(2,0)%></td>
              </tr>
  				<%
			  if (strismultipleproduct=0) then
			  %>
			   <tr>
                <td width="50%" height="13">Production Category </td>
                <td width="50%" height="13"><%=vRecArray1(24,0)%></td>
              </tr>
<%
			  end if 
			  %>
              <tr>
                <td width="50%" bgcolor="#E1E1E1" height="13"><b>Additives</b></td>
                <td width="50%" bgcolor="#E1E1E1" height="13">&nbsp;</td>
              </tr>
              <tr>
                <td width="100%" colspan="2" bgcolor="#E1E1E1" height="13">
                  <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="50%"><b>Name</b></td>
                      <td width="50%"><b>Qty (grams)</b></td>
                    </tr>
                    <tr>
                      <td width="50%"><%if isarray(vRecArray2) then%><%=vRecArray2(1,0)%><%end if%>				  </td>
                      <td width="50%"><%if isarray(vRecArray2) then%><%=vRecArray2(2,0)%><%end if%>                      </td>
                    </tr>					
                    <tr>
                      <td width="50%"><%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >0 then%><%=vRecArray2(1,1)%><%end if%><%end if%>				  </td>
                      <td width="50%"><%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >0 then%><%=vRecArray2(2,1)%><%end if%><%end if%>                      </td>
                    </tr>
                    <tr>
                      <td width="50%"><%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >1 then%><%=vRecArray2(1,2)%><%end if%><%end if%>				  </td>
                      <td width="50%"><%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >1 then%><%=vRecArray2(2,2)%><%end if%><%end if%>                      </td>
                    </tr>
                    <tr>
                      <td width="50%"><%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >2 then%><%=vRecArray2(1,3)%><%end if%><%end if%>				  </td>
                      <td width="50%"><%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >2 then%><%=vRecArray2(2,3)%><%end if%><%end if%>                      </td>
                    </tr>
                    <tr>
                      <td width="50%"><%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >3 then%><%=vRecArray2(1,4)%><%end if%><%end if%>				  </td>
                      <td width="50%"><%if isarray(vRecArray2) then%><%if ubound(vRecArray2,2) >3 then%><%=vRecArray2(2,4)%><%end if%><%end if%>                      </td>
                    </tr>
                  </table>                </td>
              </tr>
              <tr>
                <td width="50%" height="13"></td>
                <td width="50%" height="13"></td>
              </tr>
               <%
			  if (strismultipleproduct=0) then
			  %>
              <tr>
                <td width="50%" bgcolor="#E1E1E1" height="13"><b>Size</b></td>
                <td width="50%" bgcolor="#E1E1E1" height="13">&nbsp;</td>
              </tr>
              <tr>
                <td width="50%" bgcolor="#E1E1E1" height="26">Pre Baking (g)<br>
                  <%=vRecArray1(3,0)%>                </td>
                <td width="50%" bgcolor="#E1E1E1" height="26">Post Baking (g)<br>
                  <%=vRecArray1(4,0)%>                </td>
              </tr>
              <tr>
                <td width="50%" bgcolor="#E1E1E1" height="13"></td>
                <td width="50%" bgcolor="#E1E1E1" height="13">&nbsp;</td>
              </tr>
 <%
			  end if 
			  %>
              <tr>
                <td width="50%" height="13"></td>
                <td width="50%" height="13"></td>
              </tr>
  			  <%
			  if (strismultipleproduct=0) then
			  %>
			  <tr>
                <td width="50%" height="13">Consolidation type for dividing</td>
                <td width="50%" height="13"><%if (vRecArray1(25,0)="" or isnull(vRecArray1(25,0))) then%>None<%else%><%=vRecArray1(25,0)%><%end if%></td>
              </tr>
              <tr>
                <td width="50%" height="13">Shape</td>
                <td width="50%" height="13"><%=vRecArray1(5,0)%>                </td>
              </tr>
              <tr>
                <td width="50%" height="13">Is it sliced</td>
                <td width="50%" height="13"><%=vRecArray1(6,0)%></td>
              </tr>
              <tr>
                <td width="50%" height="13">Where is it produced</td>
                <td width="50%" height="13"><%=vRecArray1(7,0)%></td>
              </tr>
			   <%
			  end if
			  %>
				<tr>
			    <td width="50%" height="13">VAT Code</td>
			    <td width="50%" height="13">
			    <%=vRecArray1(18,0)%>			    </td>
			  </tr>
			  
			   <%
			  if (strismultipleproduct=0) then
			  %>
            <tr>
              <td width="50%" height="13">48 hrs Product</td>
              <td width="50%" height="13"><%
               if vRecArray1(20,0) = "Y" then 
								Response.Write "Yes"
               Else
								Response.Write "No"
               End if%>              </td>
            </tr>              
                     
              <tr>
                <td width="50%" height="1">No of products</td>
                <td width="50%" height="1"><%=vRecArray1(21,0)%></td>
              </tr>
			  <tr style="display:none;">
                <td width="50%" height="1">Type1</td>
                <td width="50%" height="1"><%=vRecArray1(22,0)%></td>
              </tr>
			  <tr style="display:none;">
			    <td height="1">Type2</td>
			    <td height="1"><%=vRecArray1(23,0)%></td>
			  </tr>
			  <%
			  end if
			  %>
			   <tr style="display:none;">
			    <td height="1">Type3 For Packing</td>
			    <td height="1"><%=vRecArray1(27,0)%></td>
			  </tr>
			  
			   <tr style="display:none;">
			    <td height="1">Type4 For Packing</td>
			    <td height="1"><%=vRecArray1(28,0)%></td>
			  </tr>
			  <tr style="display:none;">
                <td width="50%" height="1">Is Product visible to non Gails customers ?</td>
                <td width="50%" height="1"><% if vRecArray1(26,0) = "1" Then Response.Write("Yes") else Response.Write("No") end if%></td>
              </tr>
               <tr style="display:none;">
                <td width="50%" height="1">VC Product</td>
                <td width="50%" height="1"><% if vRecArray1(35,0) = "1" Then Response.Write("Yes") else Response.Write("No") end if%></td>
              </tr>
               <tr style="display:none;">
                <td width="50%" height="1">Customer Group for online product availability</td>
                <td width="50%" height="1" valign="top"><%=strProductGroupList%></td>
              </tr>
<tr>
			  	<td colspan="2">
				<%
				if isarray(vRecArray) then
				%>
				<br>
				<table border="0" bgcolor="#999999" width="500" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
				<tr bgcolor="#CCCCCC">
				<td width="100"><b>Product Code</b></td>
				<td><b>Product Name</b></td>
				<td width="50"><b>Qty</b></td>
				</tr>
				<%
				For i = 0 to UBound(vRecArray,2)
					if (i mod 2 =0) then
						strBgColour="#FFFFFF"
					else
						strBgColour="#F3F3F3"
					end if
				%>
				<tr bgcolor="<%=strBgColour%>">
				<td><%=vRecArray(0,i)%></b></td>
				<td><%=vRecArray(1,i)%></td>
				<td><%=vRecArray(2,i)%></td>
				</tr>
				<%
				next
				%>
				</table>
				<%
				end if
				%>
				</td>
			  </tr>
            </table>
  
			<br>
			<iframe frameborder="0" scrolling="auto" src="ProductUploadFiles.asp?PNo=<%=vRecArray1(17,0)%>" width="570px" height="150px;"></iframe>
          </td>
          <td width="50%" valign="top">
<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="3">
  <tr>
    <td width="67%" bgcolor="#E1E1E1"><b>Price (�)</b></td>
    <td width="33%" bgcolor="#E1E1E1">&nbsp;
  <%=formatnumber(vRecArray3(2,0),2)%>
    </td>
  </tr>
  <tr style="display:none;">
    <td width="100%" colspan="2" bgcolor="#E1E1E1">
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0">
        <tr>
          <td width="40">&nbsp;</td>
		  <td width="80" align="center"><b>A</b></td>
		  <td width="80" align="center"><b>A1</b></td>
		  <td width="80" align="center"><b>B</b></td>
		  <td width="80" align="center"><b>C</b></td>
		  <td width="80" align="center"><b>D</b></td>
          <td></td>
        </tr>
		<tr>
          <td width="40">&nbsp;</td>
		  <td align="center"><%=formatnumber(vRecArray3(2,0),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,4),2)%></td>
           <td align="center"><%if isarray(vRecArray3) then%><%if ubound(vRecArray3,2) >0 then%><%=formatnumber(vRecArray3(2,1),2)%><%end if%><%end if%></td>
          <td align="center"><%if isarray(vRecArray3) then%><%if ubound(vRecArray3,2) >1 then%><%=formatnumber(vRecArray3(2,2),2)%><%end if%><%end if%></td>
          <td align="center"><%if isarray(vRecArray3) then%><%if ubound(vRecArray3,2) >2 then%><%=formatnumber(vRecArray3(2,3),2)%><%end if%><%end if%></td>
          <td></td>
		</tr>
		<tr>
			<td width="40"></td>
			<td colspan="7" height="5"></td>
		</tr>
		<tr>
          <td width="40">&nbsp;</td>
		  <td width="80" align="center"><b>B1</b></td>
          <td width="80" align="center"><b>B2</b></td>
          <td width="80" align="center"><b>B3</b></td>
		  <td width="80" align="center"><b>B4</b></td>
		  <td width="80" align="center"><b>B5</b></td>
		  <td></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
		   <td align="center"><%=formatnumber(vRecArray3(2,5),2)%></td>
          <td align="center"><%=formatnumber(vRecArray3(2,6),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,7),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,8),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,9),2)%></td>
		  <td></td>
		</tr>
		<tr>
          <td width="40">&nbsp;</td>
		   
		  <td width="80" align="center"><b>B6</b></td>
          <td width="80" align="center"><b>B7</b></td>
          <td width="80" align="center"><b>B8</b></td>
		  <td width="80" align="center"><b>B9</b></td>
		  <td width="80" align="center"><b>B10</b></td>
		  <td width="80" align="center"><b>B11</b></td>
		  <td width="80" align="center"><b>B12</b></td>
		  <td></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
		  <td align="center"><%=formatnumber(vRecArray3(2,10),2)%></td>
          <td align="center"><%=formatnumber(vRecArray3(2,11),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,12),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,13),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,15),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,16),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,17),2)%></td>
		  <td></td>
		</tr>

		<tr>
          <td width="40">&nbsp;</td>
		   
		  <td width="80" align="center"><b>B13</b></td>
		  <td width="80" align="center"><b>B14</b></td>
		  <td width="80" align="center"><b>B15</b></td>
		 <td width="80" align="center"><b>G</b></td>
		  <td></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
		  <td align="center"><%=formatnumber(vRecArray3(2,18),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,19),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,20),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,14),2)%></td>
		  <td></td>
		</tr>

		<tr>
          <td width="40">&nbsp;</td>
		   
		  <td width="80" align="center"><b>D1</b></td>
		  <td width="80" align="center"><b>D2</b></td>
		  <td width="80" align="center"><b>D3</b></td>
		  <td width="80" align="center"><b>D4</b></td>
		  <td width="80" align="center"><b>D5</b></td>
		  <td width="80" align="center"><b>H</b></td>
		  <td width="80" align="center"><b>I</b></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
		  <td align="center"><%=formatnumber(vRecArray3(2,21),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,22),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,23),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,24),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,25),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,26),2)%></td>
		  <td align="center"><%=formatnumber(vRecArray3(2,27),2)%></td>
		</tr>

        <tr>
          <td align="center" colspan="5"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="67%">&nbsp;</td>
    <td width="33%">
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1"><b>Product Cost</b></td>
    <td width="33%" bgcolor="#E1E1E1">
  <b>&nbsp; �</b></td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">Ingredient cost according to recipe</td>
    <td width="33%" bgcolor="#E1E1E1">
  <%=formatnumber(vRecArray1(8,0),2)%>
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">Ingredient factor</td>
    <td width="33%" bgcolor="#E1E1E1">
  <%=vRecArray1(9,0)%>
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1"><b>Total Ingredient cost</b></td>
    <td width="33%" bgcolor="#E1E1E1">
  <b><%=formatnumber(vRecArray1(10,0),2)%></b>
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">Labour cost</td>
    <td width="33%" bgcolor="#E1E1E1">
  <%=formatnumber(vRecArray1(11,0),2)%>
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">Packaging</td>
    <td width="33%" bgcolor="#E1E1E1">
  <%=formatnumber(vRecArray1(12,0),2)%>
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">Hygine</td>
    <td width="33%" bgcolor="#E1E1E1">
  <%=formatnumber(vRecArray1(13,0),2)%>
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">Utilities</td>
    <td width="33%" bgcolor="#E1E1E1">
  <%=formatnumber(vRecArray1(14,0),2)%>
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">Rent, rates &amp; other</td>
    <td width="33%" bgcolor="#E1E1E1">
  <%=formatnumber(vRecArray1(15,0),2)%>
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">&nbsp;</td>
    <td width="33%" bgcolor="#E1E1E1">&nbsp;
  
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1"><b>TOTAL</b></td>
    <td width="33%" bgcolor="#E1E1E1">
  <b><%=formatnumber(vRecArray1(16,0),2)%></b>
    </td>
  </tr>
  <tr>
    <td width="67%" bgcolor="#E1E1E1">&nbsp;</td>
    <td width="33%" bgcolor="#E1E1E1">&nbsp;
   </td>
  </tr>
  <tr>
    <td width="67%">&nbsp;</td>
    <td width="33%">
    </td>
  </tr>
 <%
  if (strismultipleproduct=0) then
  %>
  <tr>
    <td width="67%">Baking Order</td>
    <%if vRecArray1(30,0) <> Empty or vRecArray1(30,0) <> "" Then %>
    <td width="33%"><%=vRecArray1(30,0) %></td>
    <%else %>
    <td width="33%">-</td>
    <%End If %>
  </tr>
    <%End If %>
  <tr>
    <td width="67%" >Tray Quantity</td>
    <td width="33%" >
  <%=vRecArray1(31,0)%>
    </td>
  </tr>
 <tr>
    <td width="67%" >Qty per box</td>
    <td width="33%" >
  <%=vRecArray1(41,0)%>
    </td>
  </tr>
  <tr style="display:none;">
    <td width="67%" >New Product Code</td>
    <td width="33%" >
  <%=vRecArray1(36,0)%>
    </td>
  </tr>
  <tr style="display:none;">
    <td width="67%" >New Product Name</td>
    <td width="33%" >
  <%=vRecArray1(40,0)%>
    </td>
  </tr>
  <!--
   <tr>
    <td width="67%" >Product Type</td>
    <td width="33%" >
  <%=vRecArray1(42,0)%>
    </td>
  </tr>
   <tr>
    <td width="67%" >Product Category</td>
    <td width="33%" >
  <%=vRecArray1(43,0)%>
    </td>
  </tr>
   <tr>
    <td width="67%" >Product Group</td>
    <td width="33%" >
  <%=vRecArray1(44,0)%>
    </td>
  </tr>
   <tr>
    <td width="67%" >Delivery Temperature</td>
    <td width="33%" >
  <%=vRecArray1(45,0)%>
    </td>
  </tr>
  --->
</table>
	 
   
</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
  </center>
</div>

	<p align="center"><input type="button" value=" Back " onClick="history.back()"></p>

</body>

</html>