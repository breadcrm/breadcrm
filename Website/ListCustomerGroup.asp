<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->

<%

vPageSize = 999999

strCustomerGroupName = replace(Request.Form("CustomerGroup"),"'","''")
vpagecount = Request.Form("pagecount") 

select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(vpagecount) then	
			session("Currentpage")  = session("Currentpage")+1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage")-1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select
vSessionpage = session("Currentpage")
vsessionpage = 0
set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set CustomerCol = objBakery.Display_CustomerGroupList(strCustomerGroupName)
arCustomerGroup = CustomerCol("Customer")
set CustomerCol = nothing
set objBakery = nothing
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script LANGUAGE="javascript" src="includes/script.js"></script>
<script Language="JavaScript">
<!--
function CHKD(E) {
	
	if (E.checked)
		(E.parentElement).parentElement.style.backgroundColor ='#999999'; 
	else
		(E.parentElement).parentElement.style.backgroundColor ='#cccccc';
}

function SearchValidation(){
		if((CheckEmpty(document.frmForm.sname.value) == "true") && (CheckEmpty(document.frmForm.scode.value) == "true") && (CheckEmpty(document.frmForm.iname.value) == "true")){ 
			alert("Please enter a value");
			return false;
		}
		return true;
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;

<table border="0" align="center" width="500" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Message to Customer Group</font></b><br><br>
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" width="100%" bordercolor="#111111">
		<form method="post" action="ListCustomerGroup.asp" name="frmForm">      
        <tr>
           <td align="center"><b>Customer Group:</b> <input type="text" name="CustomerGroup" value="<%=strCustomerGroupName%>" size="20" style="font-family: Verdana; font-size: 8pt">
          <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
       </form>
      </table>
		<br>
      <table border="0" width="750" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" bgcolor="#999999">

	<%
	If isArray(arCustomerGroup) Then
		vRecordcount=ubound(arCustomerGroup,2)
	%>
 		<tr bgcolor="#FFFFFF">
					<td colspan="4" align = "left" height="35"><b>Total number of Customer Groups: <%=vRecordcount%></b></td>
		</tr>	
   		<tr height="25">
          <td bgcolor="#CCCCCC" width="30" align="center"><b>GNo</b></td>
          <td bgcolor="#CCCCCC" width="180"><b>Customer Group Name</b></td>
          <td bgcolor="#CCCCCC" width="400"><b>Customer Code with Name</b></td>
		  <td width="170" bgcolor="#CCCCCC" align="center"><b>Message</b></td>
        </tr>
<% 
		set objBakery1 = server.CreateObject("Bakery.Customer")
		objBakery1.SetEnvironment(strconnection)
		For i = 0 To ubound(arCustomerGroup,2)
			set CustomerGroupCol = objBakery1.CustomersFromCustomerGroup(arCustomerGroup(0,i))
		    strCustomerGroup = CustomerGroupCol("Customers")
		
			'if (arCustomerGroup(3,i)<>"") then
			if (i mod 2 =0) then
				strBgColour="#FFFFFF"
			else
				strBgColour="#E6E6E6"
			end if
%>         
        <tr bgcolor="<%=strBgColour%>">
              <td align="left" valign="top"><b><%=arCustomerGroup(0,i)%></b></td>
              <td valign="top"><%=arCustomerGroup(1,i)%></td>
              <td valign="top"><%=strCustomerGroup%></td>
			  <form method="POST" action="CustomerGroupMessageUpdate.asp" name="deleteForm">
			  <td align="center" valign="top">
			  	<textarea name="CustomerGroupNotes" cols="20" rows="3"><%=arCustomerGroup(3,i)%></textarea>
				<input type = "hidden" name="CustomerGroupNo" value = "<%=arCustomerGroup(0,i)%>">
			  	<input type="submit" value="Update" name="Update" onClick="return confirm('Do you want to update this customer group message?')" style="font-family: Verdana; font-size: 8pt">
			  </td>
			  </form>
        </tr>
<%
		Next
		set CustomerGroupCol = nothing
		set objBakery1 = nothing
	Else
%>

        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">Sorry no items found</td>
        </tr>
<%
	End if
%>
	
      </table>
    </td>
  </tr>
</table>
<br>
</body>
</html>
<%
If IsArray(arCustomerGroup) Then Erase arCustomerGroup
%>