<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayIng()	
vingarray =  detail("Ingredients")

set detail = object.ListMotherIngradient()	
vMotherIngradientarray =  detail("MotherIngradient") 

set detail = Nothing
set object = Nothing
%>

<%

if request.form("MotherIngradient")<>"" then
    Set object = Server.CreateObject("bakery.Inventory")
	object.SetEnvironment(strconnection)
    set detail2 = object.DisplayIngredient(cint(request.form("MotherIngradient")))	
    vingarray2 =  detail2("InRec")

    set detail = Nothing
    set object = Nothing
    
    strMotherIngradient=request.form("MotherIngradient")
end if
if (strMotherIngradient="") then
    strMotherIngradient="0"
end if 

%>


<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script Language="JavaScript">
<!--
function FrontPage_Form1_Validator(theForm)
{
  if (theForm.MotherIngradient.value == "")
  {
    alert("Please enter a value for the \"Mother Ingradient\" field.");
    theForm.MotherIngradient.focus();
    return (false);
  }
  if (theForm.I1.selectedIndex <= 0)
  {
    alert("Please select one of the \"I1\" options.");
    theForm.I1.focus();
    return (false);
  }

  if (theForm.qty1.value == "")
  {
    alert("Please enter a value for the \"qty1\" field.");
    theForm.qty1.focus();
    return (false);
  }
  return (true);
}

function SubmitForm()
{
    document.FrontPage_Form1.action="AddMotherIngredient.asp";
    document.FrontPage_Form1.submit();
}

//-->
</script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<form method="post" name="FrontPage_Form1" action="save_mother_ingredients.asp" onSubmit="return FrontPage_Form1_Validator(this)">
<div align="center">
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Add 
	
	Mother Ingredient<br>
      &nbsp;</font></b>
<%
if Request.QueryString ("status") = "error" then
	Response.Write "<br><font size=""2"" ><b>Dough already exists go back to change dough name</b></font><br><br>"
elseif Request.QueryString ("status") = "ok" then
	Response.Write "<br><font size=""2"" ><b>Dough " & request.querystring("doughname") & " created successfully</b></font><br><br>"
end if
%>

<%if Request.QueryString ("status")="" then%>
      <table align="center" border="0" width="600" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0">
        <tr height="25">          
          <td width="150"><b>Mother Ingredient&nbsp;</b></td>
          <td>
		   <select size="1" name="MotherIngradient" onChange="SubmitForm();" style="font-family: Verdana; font-size: 8pt">
			<option value = "">Select</option>
			<%
			if IsArray(vMotherIngradientarray) Then  	
				for i = 0 to ubound(vMotherIngradientarray,2)%>  				
			 <option value="<%=vMotherIngradientarray(0,i)%>" <%if strMotherIngradient<>"" then%><%if cint(strMotherIngradient)= cint(vMotherIngradientarray(0,i)) then response.write " Selected"%><%end if%>><%=vMotherIngradientarray(2,i)%></option>
			  <%
			   next                 
			End if
			%>   
		  </select>
		  </td>
        </tr>
	  </table>
	  <br>
      <table align="center" border="0" width="600" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">       
        <tr>
          <td width="29%" height="13"><b>&nbsp;</b></td>
          <td width="71%" height="13"></td>
        </tr>
        <tr>
          <td width="29%" height="13"><b>Ingredient</b></td>
          <td width="71%" height="13"><b>Quantities required to produce 1 &nbsp;&nbsp;kg of dough</b></td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I1" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then 
							if ubound(vingarray2,2)>=0 then
								if 	vingarray2(1,0)=vingarray(0,i) then%>
								<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
								<%else%>
									<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
								<%end if%>
						<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>					
					<%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty1" value='<%=vingarray2(2,0) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I2" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then 
							if ubound(vingarray2,2)>0 then
								if 	vingarray2(1,1)=vingarray(0,i) then%>
								<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
								<%else%>
									<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
								<%end if%>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>					
					<%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty2" value='<%=vingarray2(2,1) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I3" style="font-family: Verdana; font-size: 8pt">
					<option value="" selected="selected">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then 
						if ubound(vingarray2,2)>1 then
							if 	vingarray2(1,2)=vingarray(0,i) then%>
							<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>							
					<%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty3" value='<%=vingarray2(2,2) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I4" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then 
						if ubound(vingarray2,2)>2 then
							if 	vingarray2(1,3)=vingarray(0,i) then%>
							<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>							
					<%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty4" value='<%=vingarray2(2,3) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I5" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then 
						if ubound(vingarray2,2)>3 then
							if 	vingarray2(1,4)=vingarray(0,i) then%>
							<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>							
					<%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty5" value='<%=vingarray2(2,4) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I6" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then
						if ubound(vingarray2,2)>4 then
							if 	vingarray2(1,5)=vingarray(0,i) then%>
							<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>							
					<%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty6" value='<%=vingarray2(2,5) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I7" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then
						if ubound(vingarray2,2)>5 then
							if 	vingarray2(1,6)=vingarray(0,i) then%>
							<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>						
					<%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty7" value='<%=vingarray2(2,6) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I8" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
				<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then
						if ubound(vingarray2,2)>6 then
							if 	vingarray2(1,7)=vingarray(0,i) then%>
							<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>							
					<%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty8" value='<%=vingarray2(2,7) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I9" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then
						if ubound(vingarray2,2)>7 then
							if 	vingarray2(1,8)=vingarray(0,i) then%>
							<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>							
					<%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty9" value='<%=vingarray2(2,8) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I10" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then
						if ubound(vingarray2,2)>8 then
							if 	vingarray2(1,9)=vingarray(0,i) then%>
							<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>							
					<%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty10" value='<%=vingarray2(2,9) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I11" style="font-family: Verdana; font-size: 8pt">
          					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then
						if ubound(vingarray2,2)>9 then
							if 	vingarray2(1,10)=vingarray(0,i) then%>
							<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>							
					<%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty11" value='<%=vingarray2(2,10) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I12" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then
						if ubound(vingarray2,2)>10 then
							if 	vingarray2(1,11)=vingarray(0,i) then%>
							<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>							
					<%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty12" value='<%=vingarray2(2,11) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I13" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then
						if ubound(vingarray2,2)>11 then
							if 	vingarray2(1,12)=vingarray(0,i) then%>
							<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>							
					<%next%>          
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty13" value='<%=vingarray2(2,12) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I14" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then
						if ubound(vingarray2,2)>12 then
							if 	vingarray2(1,13)=vingarray(0,i) then%>
							<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>	
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>						
					<%next%>            
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty14" value='<%=vingarray2(2,13) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I15" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                    	<%
						if isarray(vingarray2) then
						if ubound(vingarray2,2)>13 then
							if 	vingarray2(1,14)=vingarray(0,i) then%>
							<option value="<%=vingarray(0,i)%>" selected="selected"><%=vingarray(2,i)%></option>
							<%else%>
								<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
							<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>
						<%else%>
							<option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
						<%end if%>						
					<%next%>            
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty15" value='<%=vingarray2(2,14) %>' size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="53">&nbsp;</td>
          <td width="71%" height="53"><p><input type="submit" value=" Save " name="B1" style="font-family: Verdana; font-size: 8pt"></p>
          </td>
        </tr>
      </table>
	   <%end if%>
  </center>
    </td>
  </tr>
</table>


</div>

</form>


</body>

</html>