<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/top.inc" -->
<%
deldate= request.form("deldate")
facility= request.form("facility")
dtype=Request.Form("deltype")
if deldate= "" or facility = "" or dtype = "" then response.redirect "daily_report.asp"
'Response.Write deldate & "," & facility & "," & dtype
set object = Server.CreateObject("bakery.daily")
set col1= object.DailyPackingSheetReport(deldate,facility,dtype)
vRecArray = col1("DailyPackingSheet")
vtotcustarray = col1("TotalCustomers")
if isarray(vtotcustarray) then
  totpages = ubound(vtotcustarray,2)+1
else
  totpages = 0
end if
curpage=0
    
Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
set object = nothing
dim curvan, prevvan
dim curcno, prevcno

Dim mPage,mLine
Dim mFontSize
Dim mFooterSize
Dim mFooterFontSize
Dim mExtraLine

if facility = 11 OR facility = 12 Then
	mFontSize = 14
	mFooterSize =5
	mFooterFontSize = 5
	mExtraLine = 12
Else
	mFontSize = 8
	mFooterSize =3
	mFooterFontSize = 3
	mExtraLine = 10
End if


'Page Count

mLine = 18
	if mFontSize = 14 Then
	mLine = 16
Else
	mLine = 8
End if
	  if isarray(vrecarray) then
			mPage = 1
	    currec=0
	    totqty=0
	    curcno = vrecarray(0,0)
	    curvan = vrecarray(5,0)
	    curpage=1	  
        for i = 0 to ubound(vRecArray,2)
          currec = clng(currec) + 1
          if clng(currec) = 1 then
            if mFontSize = 14 Then
							mLine = mline + 4
						else
							mLine = mline + 3
						end if	
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0
						End if
           end if
           if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then
						if mFontSize = 14 Then
							mLine = mLine + 2
						else
							mLine = mLine + 1
						end if	
          end if
          if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
						mPage = mPage + 1						
						mLine = 0
          End if          
		  if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
		    curvan = vrecarray(5,i)
		    curcno = vrecarray(0,i)
		    i=clng(i)-1
            if mFontSize = 14 Then
							mLine = mLine +4           
						else
							mLine = mLine +3           
						end if	
						
         if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then 						         
						mPage = mPage + 1
						mLine = 0	
          End if
            if i < ubound(vrecarray,2) then
            end if            
		    curpage = curpage + 1
		    currec = 0
		    totqty=0
		  end if
	    next
	    
      else
				mPage = 1
	  end if
i = 0

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; }
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: <%=mFontSize%>pt">
  <tr>
    <td width="100%">
      <center><b><font size="3">PACKING SHEET - <%=dtype%><br></font></b></center>
      <center><b><font size="3">(divided to Van and Customers)<br></font></b></center>
      <table border="0" width="90%" style="font-family: Verdana; font-size: <%=mFontSize%>pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><br>
            <br>
            <b>Date of report:</b> <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            <b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><BR>
            <b>Facility:</b> <%=Facilityname %><BR>
            <b>To the Attention of:</b> <%=attn%><BR>
            <b>By Fax Number:</b> <%=fax%><BR>
            </td>
        </tr>
        <tr>
          <td colspan="2"><br>
            Number of pages for this report: <%=mPage%>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table>
</center>
</div><%
if mFontSize = 14 Then
	mLine = 16
Else
	mLine = 8
End if%>


<div align="center">
<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
	  if isarray(vrecarray) then
			mPage = 1
	    currec=0
	    totqty=0
	    curcno = vrecarray(0,0)
	    curvan = vrecarray(5,0)
	    curpage=1	  
        for i = 0 to ubound(vRecArray,2)
          currec = clng(currec) + 1
          if clng(currec) = 1 then%>
            <tr>
            <td colspan="3">
              <b>Van : <%=vrecarray(5,i)%></b><br>
              <b>Customer : <%=vrecarray(0,i)%> - <%=vrecarray(1,i)%></b>
            </td>            
            </tr>
            <tr>            
            <td align="left" bgcolor="#CCCCCC" height="20" width="85%"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Qty</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="5%"><b>SID</b></td>
            </tr><%
            if mFontSize = 14 Then
							mLine = mline + 4
						else
							mLine = mline + 3
						end if	
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
						End if
           end if
           if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then%>
          <tr>            
            <td align="left" width="85%"><%=left(vrecarray(3,i),50)%>&nbsp;</td>
            <td align="center" width="10%"><%=vrecarray(4,i)%>&nbsp;</td><%
            totqty = clng(totqty) + vrecarray(4,i)%>
            <td align="center" width="5%">&nbsp;</td>
          </tr><%
						if mFontSize = 14 Then
							mLine = mLine + 2
						else
							mLine = mLine + 1
						end if	
          end if
          if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
						mPage = mPage + 1						
						mLine = 0%>		
						</table>						
						<br class="page" />						
						<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
          End if
          
		  if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
		    curvan = vrecarray(5,i)
		    curcno = vrecarray(0,i)
		    i=clng(i)-1%>
		    <tr>
		      <td width="85%">
		        <p align="right"><b><font size="<%=mFooterFontSize%>" face="Verdana">Total&nbsp;&nbsp; </font></b>
              </td>
              <td width="10%" align="center" >
                <p align="center"><font size="<%=mFooterFontSize%>" face="Verdana"><%=totqty%></font></p>
              </td>
              <td width="5%">&nbsp;</td>
            </tr>
		    </table>
						<br>
            <br><%
            if mFontSize = 14 Then
							mLine = mLine +4           
						else
							mLine = mLine +3           
						end if	
         if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then 						
         
						mPage = mPage + 1
						mLine = 0%>								
						<br class="page" /><%						
          End if
            if i < ubound(vrecarray,2) then%>
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%						
            end if            
		    curpage = curpage + 1
		    currec = 0
		    totqty=0
		  end if
	    next%>
	    <tr>
		  <td  width="85%">
		    <p align="right"><b><font size="<%=mFooterFontSize%>" face="Verdana">Total&nbsp;&nbsp; </font></b>
          </td>
          <td width="10%" align="center">
            <p align="center"><font size="<%=mFooterFontSize%>" face="Verdana"><%=totqty%></font></p>
          </td>
          <td width="5%">&nbsp;</td>
        </tr><%
      else%>
        <tr>
        <td width="100%" colspan = "4" height="20">Sorry no items found</td>
        </tr><%
	  end if%>
      </table>
</div>
</body>
</html>