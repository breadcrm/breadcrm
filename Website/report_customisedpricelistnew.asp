<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script language="Javascript">
<!--
function SubmitData(f)
{
  //alert(parseInt(f.txtprice.value));
  if(f.txtprice.value=='' || f.txtprice.value==null || isNaN(f.txtprice.value) == true)
  {
      alert('Please enter a valid value for price...');
      return false;
  }
  /*else if(parseInt(f.txtprice.value)==0 || parseInt(f.txtprice.value) == 'NaN' );
  {
    alert('Please enter a valid value for price...');
      return false;
  } */   
  return true;
}
//-->
</script>
</head>
<%
dim objBakery
dim recarray1, recarray2, recarray3, recarray4
dim retcol
dim totpages
dim vSessionpage
dim i, msg, a1
dim ptno, ptname, dno, pno, pname, modrecs, price, dname

ptno = Request.Form("lstptno")
dno = Request.Form("lstdno")
pno = Request.Form("txtpcode")
pname = Request.Form("txtpname")
price = Request.Form("txtprice")

if dno = "" then
  dno = "0"
  dname=""
else
  a1=split(dno,"~")
  dno=a1(0)
  dname=a1(1)
end if

if ptno = "" then
  ptno = "0"
end if

if not isnumeric(price) then
  price = "0"
end if

'response.Write"pno = " & pno & "<br>"
'response.Write"pname = " & pname & "<br>"
'response.Write"dno = " & dno & "<br>"
'response.Write"dname = " & dname & "<br>"
'response.Write"ptno = " & ptno & "<br>"
'response.Write"ptname = " & ptname & "<br>"
'response.Write"price = " & price & "<br>"

vSessionPage="1"
if Request.Form("submit2") <> "Add Price" and Request.Form("submit2") <> "Change Price" then
  totpages = Request.Form("pagecount")
  select case Request.Form("Direction")
	case ""
	  	session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(totpages) then	
			session("Currentpage")  = session("Currentpage") + 1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage") - 1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
  end select
  vSessionpage = session("Currentpage")
end if
  
'response.Write"vSessionPage = " & vSessionPage & "<br>"
'response.Write"pagesize = " & pagesize & "<br>"
'response.Write"ptname = " & ptname & "<br>"

set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.CustomPriceList(ptno,vSessionPage,0)
 
recarray2 = retcol("CustomPrices1")
'totpages = retcol("pagecount")

set retcol = objBakery.GetDoughDetails(0)
recarray4 = retcol("DoughDetails")


'Response.Write "Totpages = " & totpages & "<br>"

if Request.Form("submit2") = "Add Price" or Request.Form("submit2") = "Change Price" then
  modrecs = objBakery.ChangecustomPrice(pno, ptno, price)
  modrecs = "Result: " & modrecs & " records modified"
end if
'Response.write "submit1 = " &  Request.form("submit1") & "<br>"
'Response.write "submit2 = " &  Request.form("submit2") & "<br>"
%>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Report - Create Customised Price List<br>
      &nbsp;</font></b>
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <form name="frmsearch" method="post" action="report_customisedpricelistnew.asp">
        <tr>
          <td><b>Price List Name</b></td>
          <td colspan="2">
            <select name="lstptno" style="font-family: Verdana; font-size: 8pt"><%
            if isarray(recarray2) then
              for i=0 to ubound(recarray2,2)
                if clng(ptno) = recarray2(0,i) and trim(ptno) <> "" then
                  ptname=recarray2(1,i)%>
                  <option value="<%=recarray2(0,i)%>" selected><%=recarray2(1,i)%></option><%
                elseif ptno = "0" and i=0 then
                  ptno = recarray2(0,i)
                  ptname=recarray2(1,i)%>
                  <option value="<%=recarray2(0,i)%>" selected><%=recarray2(1,i)%></option><%
                else%>
                  <option value="<%=recarray2(0,i)%>"><%=recarray2(1,i)%></option><%
                end if
              next
            else%>
              <option value="-1" selected>No records found</option><%
            end if%>
            </select>
          </td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          <td><b>Product Code:</b></td>
          <td><b>Product Name:</b></td>
          <td><b>Dough Name</b></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><input type="text" name="txtpno" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td><input type="text" name="txtpname" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td><select size="1" name="lstdno" style="font-family: Verdana; font-size: 8pt"><%
            if isarray(recarray4) then
              for i=0 to ubound(recarray4,2)
                if dno <> "" and clng(dno) = recarray4(0,i) then%>
                <option value="<%Response.Write(recarray4(0,i) & "~" & recarray4(1,i))%>" selected><%=recarray4(1,i)%></option><%
                else%>
                <option value="<%Response.Write(recarray4(0,i) & "~" & recarray4(1,i))%>"><%=recarray4(1,i)%></option><%
                end if
              next
            end if%>
            </select>
          </td>
          <td>
            <input type="Submit" value="Find" name="submit1" style="font-family: Verdana; font-size: 8pt">
          </td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        </form>
      </table>
    </td>
  </tr>
</table>
  </center>
</div>
<br><%
set retcol = objBakery.GetProductDetails(ptno,pno,pname,dno,1,0)
recarray1 = retcol("ProductDetails")%>
<div align="center">
  <center>
      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="3"><b><font size="2">Customised Price List
          <%msg=""
            if pno <> "" then
             msg = "for """ & pno &  """ and"
            end if
            
            if  pname <> "" then
              msg = msg & " for """ & pname & """ and"
            end if
            
            if  dname <> "" then
              msg = msg & " for """ & dname & """ and"
            end if
            'Response.Write "msg = " &  msg
            if msg <> "" and msg <> "0" then
            msg = mid(msg,1,len(msg)-3)
            Response.Write msg
            end if%>
          </font></b></td>
        </tr>
        <tr>
          <td bgcolor="#CCCCCC"><b>Product Code</b></td>
          <td bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td bgcolor="#CCCCCC"><b>Price</b></td>
          <td bgcolor="#CCCCCC"><b>Action</b></td>
        </tr><%
        if isarray(recarray1) then
          for i=0 to ubound(recarray1,2)%>
           <tr>
           <td><%=recarray1(0,i)%></td>
           <td><%=recarray1(1,i)%></td>
           <form method="post" action="report_customisedpricelistnew.asp" name="frmadd<%=i%>" onSubmit="return SubmitData(this);">
           <td>
            <input type="text" name="txtprice" size="5" value="<%=recarray1(2,i)%>" style="font-family: Verdana; font-size: 8pt">
           </td>
           <input type="hidden" name="txtpcode" value="<%=recarray1(0,i)%>">
           <input type="hidden" name="txtpname" value="<%=recarray1(1,i)%>">
           <input type="hidden" name="lstptno" value="<%=ptno%>">
           <td><%
           if isnull(recarray1(2,i)) or recarray1(2,i) = 0 then%>
            <input type="submit" value="Add Price" name="submit2" style="font-family: Verdana; font-size: 8pt"><%
            else%>
            <input type="submit" value="Change Price" name="submit2" style="font-family: Verdana; font-size: 8pt"><%
            end if%>
           </td>
           </form>
           </tr><%
          next
        elseif Request.form("submit1") = "Find" then%>
          <tr>
           <td colspan="4"><b>No records found...</b></td>
          </tr><%
        end if%>
      </table>
  </center>
</div>
<br><%
if modrecs <> "" then%>
<div align="center">
  <font size="2"><b><%=modrecs%></font></b>
</div><%
end if%>
<div align="center">
  <table width="720" border="0" cellspacing="1" cellpadding="2" align="center">
  <tr> 
  <% if clng(vSessionpage) <> 1 and vSessionpage <> "" then %>
  <td>
    <form name="frmFirstPage" action="report_customisedpricelistnew.asp" method="post">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="hidden" name="Direction" value="First">
    <input type="submit" name="submit" value="First Page">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) < clng(totpages) then %>
  <td>
    <form name="frmNextPage" action="report_customisedpricelistnew.asp" method="post">
    <input type="hidden" name="Direction" value="Next">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Next Page">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) > 1 then %>
  <td>
    <form name="frmPreviousPage" action="report_customisedpricelistnew.asp" method="post">
    <input type="hidden" name="Direction" value="Previous">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Previous Page">
    </form>
  </td>
  <%end if%>
  <%if clng(vSessionPage) <> clng(totpages) and clng(totpages) > 0 then %>
  <td>
    <form name="frmLastPage" action="report_customisedpricelistnew.asp" method="post">
    <input type="hidden" name="Direction" value="Last">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Last Page">
    </form>
  </td>
  <% end if%>
  </tr>
  </table>
</div>
</body>
</html>