﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Generate_SalesCreditReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            ProcessReportAllInvoices ds = new ProcessReportAllInvoices();
           // ds.ExecuteSQL("spEmailPDFFiles '" + Request.QueryString["deldate"].Replace("'", "''") + "'");
            ds.ExecuteSQL("spExecuteSalesCreditReportJob");
          //  dbo.spCreateSalesCreditReportMonthly 'D:\breadreport\test_thilina1.csv','-99',-99,'2016-12-01','2016-12-31';
            lblMsg.Text = "Report created successfully!";
            lblMsg.ForeColor = System.Drawing.Color.Black;
        }
        catch (Exception ex)
        {
            lblMsg.Text = "Error creating reports!" + "<br />" + ex.Message;
            lblMsg.ForeColor = System.Drawing.Color.Red;
        }
    }
}
