using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

/// <summary>
/// Summary description for DailyPDFGenerator
/// </summary>
public class DailyPDFGenerator
{
	public DailyPDFGenerator(){}

    public static int GenerateDocs(DateTime genDate)
    {
        Support support = new Support();

        int totDocs = 0;
        string dumpFolder = support.DailyPdfFolder;

        ProcessReportAllInvoices ds = new ProcessReportAllInvoices();
        DataTable invNos = ds.GetInvoiceNoListToGenerateDailyPDF(genDate);

        int totInvDocs = 0;
        int totCrdDocs = 0;

        string sFooter = GetFooterText();

        StreamWriter log;

        if (!File.Exists("D:\\Website\\Bakery\\CRM\\CreateBakeryPDF\\log.txt"))
        {
            log = new StreamWriter("D:\\Website\\Bakery\\CRM\\CreateBakeryPDF\\log.txt");
        }
        else
        {
            log = File.AppendText("D:\\Website\\Bakery\\CRM\\CreateBakeryPDF\\log.txt");
        }

        foreach (DataRow row in invNos.Rows)
        {
            try
            {
                string strCreatedFile = string.Empty;
                totInvDocs++;
                int invNo = (int)row["ordNo"];
                int cno = (int)row["cno"];

                string cname = "";
                if (row["CustNameInAutomatedDocFileName"] != DBNull.Value)
                    cname = (string)row["CustNameInAutomatedDocFileName"];
                cname = cname.Replace('\'', ' ');
                string fileName = string.Format("Inv_{0}_{1}.pdf", cname, invNo);
                string filePath = System.Web.HttpContext.Current.Server.MapPath(dumpFolder + fileName);
                if (File.Exists(filePath))
                    File.Delete(filePath);
                ds.ExecuteSQL("DELETE FROM PDFFiles WHERE PDFFileName = '" + fileName + "'");
                strCreatedFile = PrintPDFInvoice.GenerateDoc(invNo, dumpFolder + fileName, false, sFooter);
                ds.ExecuteSQL(string.Format("INSERT PDFFiles (CNo,OrderDate,PDFFileName) VALUES({0},'{1}','{2}')", cno, genDate.ToString("yyyy-MM-dd"), fileName));

                log.WriteLine(totInvDocs.ToString() + " - " + DateTime.Now + " File Created : -" + strCreatedFile);
                log.WriteLine("--------------------------------------------------------------------------");

            }
            catch (Exception ex)
            {
                log.WriteLine(DateTime.Now);
                log.WriteLine("ERROR : -" + ex.Message.ToString());
                log.WriteLine("--------------------------------------------------------------------------");
            }
        }



        DataTable crdNos = ds.GetCreditNoteNoListToGenerateDailyPDF(genDate);

        foreach (DataRow row in crdNos.Rows)
        {
            try
            {
                string strCreatedFile2 = string.Empty;
                totCrdDocs++;
                int crdNo = (int)row["CrNo"];
                int cno = (int)row["CNo"];
                string cname = "";

                if (row["CustNameInAutomatedDocFileName"] != DBNull.Value)
                    cname = (string)row["CustNameInAutomatedDocFileName"];

                cname = cname.Replace('\'', ' ');
                DateTime docDate = (DateTime)row["CrDate"];
                string fileName = string.Format("Crd_{0}_{1}.pdf", cname, crdNo);
                string filePath = System.Web.HttpContext.Current.Server.MapPath(dumpFolder + fileName);
                if (File.Exists(filePath))
                    File.Delete(filePath);
                ds.ExecuteSQL("DELETE FROM PDFFiles WHERE PDFFileName = '" + fileName + "'");
                strCreatedFile2 = PrintPDFCreditNote.GenerateDoc(crdNo, dumpFolder + fileName, false);
                ds.ExecuteSQL(string.Format("INSERT PDFFiles (CNo,OrderDate,PDFFileName) VALUES({0},'{1}','{2}')", cno, docDate.ToString("yyyy-MM-dd"), fileName));

                log.WriteLine(totCrdDocs.ToString() + " - " + DateTime.Now + " File Created : -" + strCreatedFile2);
                log.WriteLine("--------------------------------------------------------------------------");
            }
            catch (Exception ex)
            {
                log.WriteLine(DateTime.Now);
                log.WriteLine("ERROR : -" + ex.Message.ToString());
                log.WriteLine("--------------------------------------------------------------------------");
            }
        }

        totDocs = totCrdDocs + totInvDocs;

        log.Close();

        return totDocs;

    }

    public static string GetFooterText()
    {
        string footer = string.Empty;
        string newFooter = string.Empty;
        ProcessReportAllInvoices a = new ProcessReportAllInvoices();
        footer = a.GetFooterNote();
        newFooter = footer.Replace("<BR>", "\n");
        newFooter = newFooter.Replace("<P>", "");
        newFooter = newFooter.Replace("</P>", "");
        newFooter = newFooter.Replace("^^^", "'");
        return newFooter;
    }
}
