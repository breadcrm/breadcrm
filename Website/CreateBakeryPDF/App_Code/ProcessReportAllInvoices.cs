using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Odbc;
using System.Configuration;
using System.Data;
using System.Web;

    public class ProcessReportAllInvoices
    {

        private string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        OdbcConnection conn;
        private StringBuilder strbQuery;
        private string strSql = string.Empty;

        public ProcessReportAllInvoices() { }

        public void ExecuteSQL(string sql)
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            OdbcCommand cmd = new OdbcCommand(sql, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        //this methods accepts 3 parameters vlngOrdNo=order no ,vdatDate=date,vstrDaySession=(morning,noon,evening)
        public DataTable GetInvoiceNoListToGenerateDailyPDF(DateTime vdatDate)
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);//clear the varibale

            strSql = "select ordNo,ordermaster.cno,customermaster.CustNameInAutomatedDocFileName from ordermaster inner join  customermaster on ordermaster.cno = customermaster.cno " + 
                     "where customermaster.DailyAutomatedInvoice = '1' and datediff(d,orddate,convert(datetime,'" + vdatDate.ToString("yyyy-MM-dd") + "',101)) = 0 " +
                 //    " and customermaster.cno = 10046 " + 
                     "ORDER BY ordermaster.cno, ordNo";

            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();

            da.Fill(dt);

            return dt;  
        }

        public DataTable GetCreditNoteNoListToGenerateDailyPDF(DateTime vdatDate)
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);//clear the varibale

            strSql = "SELECT CrNo,OrderMaster.cno,customermaster.CustNameInAutomatedDocFileName,CrDate FROM CreditMaster INNER JOIN OrderMaster ON CreditMaster.OrdNo = OrderMaster.OrdNo  INNER JOIN  CustomerMaster on OrderMaster.CNo = CustomerMaster.CNo " +
                     "WHERE CustomerMaster.DailyAutomatedCredit = '1' and datediff(d,crdate,convert(datetime,'" + vdatDate.ToString("yyyy-MM-dd") + "',101)) = 1 " +
                     "ORDER BY OrderMaster.CNo, CrNo";

            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);

            return dt;  
        }

        public DataTable GetInvoiceTotal(long vlngOrdNo, DateTime vdatDate, string vstrDaySession)
        {   

            conn = new OdbcConnection(strConnectionString);
            DataTable  dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);//clear the varibale

            strbQuery.Append( "select  ordNo,Orddate,CName= (select CName from customermaster where cno=a.cno)," +
                        "Custof = (Select Custof from customermaster where cno=a.cno)," +
                       // "OCCode= (select OCCode from customermaster where cno=a.cno)," +
                       "cno, " +
                        "CAddressI= (select CAddressI from customermaster where cno=a.cno)," +
                        "CAddressII= (select CAddressII from customermaster where cno=a.cno)," +
                        "CTown= (select CTown from customermaster where cno=a.cno)," +
                        "CPostCode= (select CPostCode from customermaster where cno=a.cno)," +
                        "pono," +
                        /*"Vehcle = case when datename(dw,orddate) not in('Saturday','Sunday') then (select week_vno from customermaster where cno=a.cno) else (select weekend_vno from customermaster where cno=a.cno) end," + */
                        "Vehcle = DBO.Fn_GetVanNo(orddate,a.cno)," + 
                        "price," +
                        "(select sum((qty * x.price * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) from orderdetail x,productmaster y,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno =a.ordno) Vat," +
                        "price + (select sum((qty * x.price * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) from orderdetail x,productmaster y,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno =a.ordno) TotalPrice," +
                        "a.cno,NoofInvoices= (select NoofInvoices from customermaster where cno=a.cno),PricelessInvoice= (select PricelessInvoice from customermaster where cno=a.cno)," +
                        "ResellerPrice," +
                        "(select sum((qty * x.ResellerPrice * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) from orderdetail x,productmaster y,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno =a.ordno) EndCustomerVat," +
                        "ResellerPrice + (select sum((qty * x.ResellerPrice * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) from orderdetail x,productmaster y,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno =a.ordno) EndCustomerTotalPrice," +
                        "ResellerID= (select ResellerID from customermaster where cno=a.cno)" +
                        ",CustomerNotes= (select CustomerNotes from customermaster where cno=a.cno)" +
                        ",GroupLogo = (select case when LogoPath is null Then '' Else LogoPath End As LogoPath  from dbo.GroupMaster where GNo = (select GNo from CustomerMaster where CNo = a.CNo))" + 
                        ",Telephone = (select case when Telephone is null Then '' Else Telephone End As Telephone  from dbo.GroupMaster where GNo = (select GNo from CustomerMaster where CNo = a.CNo))" +
                        "from ordermaster a");

            if (vlngOrdNo>0)
                strbQuery.Append(" Where ordno = " + vlngOrdNo + " and");
            else if (vdatDate!=null) //ElseIf IsDate(orddt) Then
                strbQuery.Append(" Where datediff(d,orddate,convert(datetime,'" + vdatDate.ToString("dd/MM/yyyy") + "',103)) = 0" + " and");

            if (vstrDaySession.Substring(1,3)=="All")
                vstrDaySession = "Morning','Noon','Evening"; 
           
            strSql =strbQuery.ToString();
            string strRight = strSql.Substring(strbQuery.Length - 4, 4);
            if  (vstrDaySession!="" )              
                if (strRight==" and")
                    strbQuery.Append(" a.tno in (select tno from tripmaster where tname in ('" + vstrDaySession + "'))");
                else
                    strbQuery.Append(" where a.tno in (select tno from tripmaster where tname in ('" + vstrDaySession + "'))");

            strSql=string.Empty;
            strSql =strbQuery.ToString();

            strRight = string.Empty;
            strRight=strSql.Substring(strbQuery.Length - 4, 4);
            if (strRight == " and")
                 strbQuery.Append(strSql.Substring(1,strbQuery.Length - 4));

            strbQuery.Append(" Order by Vehcle,ordNo");
            strSql = string.Empty;
            strSql = strbQuery.ToString();

            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);

 
            return dt;        
        }

        public DataTable GetInvoiceHeader(long vlngOrdNo)
        {

            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);//clear the varibale

            strbQuery.Append("select  ordNo,Orddate,CName= (select CName from customermaster where cno=a.cno)," +
                        "Custof = (Select Custof from customermaster where cno=a.cno)," +
                // "OCCode= (select OCCode from customermaster where cno=a.cno)," +
                       "cno, " +
                        "CAddressI= (select CAddressI from customermaster where cno=a.cno)," +
                        "CAddressII= (select CAddressII from customermaster where cno=a.cno)," +
                        "CTown= (select CTown from customermaster where cno=a.cno)," +
                        "CPostCode= (select CPostCode from customermaster where cno=a.cno)," +
                        "pono," +
                        /*"Vehcle= case when datename(dw,orddate) not in('Saturday','Sunday') then (select week_vno from customermaster where cno=a.cno) else (select weekend_vno from customermaster where cno=a.cno) end," +*/
                        "Vehcle = DBO.Fn_GetVanNo(orddate,a.cno)," +
                        "price," +
                        "(select sum((qty * x.price * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) from orderdetail x,productmaster y,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno =a.ordno) Vat," +
                        "price + (select sum((qty * x.price * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) from orderdetail x,productmaster y,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno =a.ordno) TotalPrice," +
                        "a.cno,NoofInvoices= (select NoofInvoices from customermaster where cno=a.cno),PricelessInvoice= (select PricelessInvoice from customermaster where cno=a.cno)," +
                        "ResellerPrice," +
                        "(select sum((qty * x.ResellerPrice * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) from orderdetail x,productmaster y,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno =a.ordno) EndCustomerVat," +
                        "ResellerPrice + (select sum((qty * x.ResellerPrice * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) from orderdetail x,productmaster y,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno =a.ordno) EndCustomerTotalPrice," +
                        "ResellerID= (select ResellerID from customermaster where cno=a.cno)" +
                        ",CustomerNotes= (select CustomerNotes from customermaster where cno=a.cno)" +
                        ",GroupLogo = (select case when LogoPath is null Then '' Else LogoPath End As LogoPath  from dbo.GroupMaster where GNo = (select GNo from CustomerMaster where CNo = a.CNo)) " + 
                        ",Telephone = (select case when Telephone is null Then '' Else Telephone End As Telephone  from dbo.GroupMaster where GNo = (select GNo from CustomerMaster where CNo = a.CNo)) " +
                        "from ordermaster a");

            if (vlngOrdNo > 0)
                strbQuery.Append(" Where ordno = " + vlngOrdNo.ToString());

            strSql = strbQuery.ToString();

            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public DataTable GetInvoiceDetails(long vlngOrdNo)
        {
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);//clear the varibale
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery.Append("select ordno,qty,pno,pname = (select pname from productmaster where pno=a.pno),price,qty*price Tprice,ResellerPrice,qty*ResellerPrice TResellerPrice from orderdetail a where ordno=" + vlngOrdNo + " order by pname asc ");
            strSql = string.Empty;
            strSql = strbQuery.ToString();
            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);

            return dt;            
           
        }

        public DataTable DisplayResellerDetail(long vlngRID)
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State!=ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);

            strbQuery.Append("Select RID, ResellerName, Address, Town, Postcode, Telephone, Fax, PTNo, Logo, InvoiceFooter, VATRegNo, Notes, OCCode,Factor from Reseller where RID = " + vlngRID);
            strSql = string.Empty;
            strSql = strbQuery.ToString();
            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);

            return dt;       
        }

        public DataTable Display_CreditStatements(DateTime vdtCreditDate , string cno, int nIndividualCreditNote, string listofCNO)
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);

            if (!string.IsNullOrEmpty(listofCNO))
                listofCNO = listofCNO.Replace("|", "','");

            strbQuery.Append("select Crno,convert(char,crdate,103) Crdate,CName= (select CName from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)), " +
                                    "OCCode= (select OCCode from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)), " +
                                     "CAddressI= (select CAddressI from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)), " +
                                     "CAddressII= (select CAddressII from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)), " +
                                     "CTown= (select CTown from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)), " +
                                     "CPostCode= (select CPostCode from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)), " +
                                     "(select sum(tot) from (select sum(z.qty * x.price) tot from orderdetail x,productmaster y,creditdetail z where x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno union select sum(z.qty * x.price) tot from OrderDetailForMultipleProduct x,productmaster y,creditdetail z where x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno) A) Goodstotal,  " +
                                     "(select sum(totvat) from (select sum((z.qty * x.price * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) totvat from orderdetail x,productmaster y,creditdetail z,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and  z.crno=a.crno union select sum((z.qty * x.price * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) totvat from OrderDetailForMultipleProduct x,productmaster y,creditdetail z,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and  z.crno=a.crno) A)  Vat, " +
                                     "(select sum(tot) from (select sum(z.qty * x.price) tot from orderdetail x,productmaster y,creditdetail z where x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno union select sum(z.qty * x.price) tot from OrderDetailForMultipleProduct x,productmaster y,creditdetail z where x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno)A) + (select sum(totval) from (select sum((z.qty * x.price * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) totval from OrderDetailForMultipleProduct x,productmaster y,creditdetail z,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno union select sum((z.qty * x.price * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) totval from orderdetail x,productmaster y,creditdetail z,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno) A) Totvalue, " +
                                     "Custof = (select Custof from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)),cno=(select cno from ordermaster where ordno=a.ordno)" +
                /*",Vehcle= case when datename(dw,orddate) not in('Saturday','Sunday') then (select week_vno from customermaster where cno=b.cno) else (select weekend_vno from customermaster where cno=b.cno) end,PricelessInvoice= (select PricelessInvoice from customermaster where cno=b.cno)," + */
                                     ",Vehcle = DBO.Fn_GetVanNo(orddate,b.cno),PricelessInvoice= (select PricelessInvoice from customermaster where cno=b.cno)," +
                                     "(select sum(z.qty * x.ResellerPrice) from orderdetail x,productmaster y,creditdetail z where x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno) EndCustomerGoodstotal, " +
                                     "(select sum((z.qty * x.ResellerPrice * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) from orderdetail x,productmaster y,creditdetail z,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and  z.crno=a.crno) EndCustomerVat, " +
                                     "(select sum(z.qty * x.ResellerPrice) from orderdetail x,productmaster y,creditdetail z where x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno) + (select sum((z.qty * x.ResellerPrice * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) from orderdetail x,productmaster y,creditdetail z,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno) ResellerPriceTotvalue " +
                                     ",ResellerID= (select ResellerID from customermaster where cno=(select cno from ordermaster where ordno=a.ordno))" +
                                     ",c.ordno " +
                                     ",GroupLogo = (select case when LogoPath is null Then '' Else LogoPath End As LogoPath  from dbo.GroupMaster where GNo = (select GNo from CustomerMaster where CNo = b.CNo)) " +
                                     ",Telephone = (select case when Telephone is null Then '' Else Telephone End As Telephone  from dbo.GroupMaster where GNo = (select GNo from CustomerMaster where CNo = b.CNo)) ");
                                  //   ",Telephone = (select case when Telephone is null Then '' Else Telephone End As Telephone  from dbo.GroupMaster where GNo = (select GNo from CustomerMaster where CNo = b.CNo)) " +
                                  //   " from creditmaster a,customermaster b,ordermaster c  where b.cno=c.cno and a.ordno=c.ordno and datediff(d,convert(datetime,'" + vdtCreditDate.ToString("dd/MM/yyyy") + "',103),crdate) = 0 and c.CNo = '" + cno + "' order by Crno");

                if (nIndividualCreditNote == 0)
                    strbQuery.Append(" from creditmaster a,customermaster b,ordermaster c  where b.cno=c.cno and a.ordno=c.ordno and datediff(d,convert(datetime,'" + vdtCreditDate.ToString("dd/MM/yyyy") + "',103),crdate) = 0 and c.CNo = '" + cno + "' order by Crno");
                else if (nIndividualCreditNote == 1)
                    strbQuery.Append(" from creditmaster a,customermaster b,ordermaster c  where b.cno=c.cno and a.ordno=c.ordno and datediff(d,convert(datetime,'" + vdtCreditDate.ToString("dd/MM/yyyy") + "',103),crdate) = 0 and c.CNo NOT IN ('" + listofCNO + "') order by Crno");
                      
                                  
            strSql = string.Empty;
            strSql = strbQuery.ToString();
            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);

            return dt;  
        }

        public DataTable Display_CreditStatements(int CrNo)
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);

            strbQuery.Append("select Crno,convert(char,crdate,103) Crdate,CName= (select CName from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)), " +
                                    "OCCode= (select OCCode from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)), " +
                                     "CAddressI= (select CAddressI from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)), " +
                                     "CAddressII= (select CAddressII from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)), " +
                                     "CTown= (select CTown from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)), " +
                                     "CPostCode= (select CPostCode from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)), " +
                                     "(select sum(tot) from (select sum(z.qty * x.price) tot from orderdetail x,productmaster y,creditdetail z where x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno union select sum(z.qty * x.price) tot from OrderDetailForMultipleProduct x,productmaster y,creditdetail z where x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno) A) Goodstotal,  " +
                                     "(select sum(totvat) from (select sum((z.qty * x.price * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) totvat from orderdetail x,productmaster y,creditdetail z,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and  z.crno=a.crno union select sum((z.qty * x.price * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) totvat from OrderDetailForMultipleProduct x,productmaster y,creditdetail z,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and  z.crno=a.crno) A)  Vat, " +
                                     "(select sum(tot) from (select sum(z.qty * x.price) tot from orderdetail x,productmaster y,creditdetail z where x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno union select sum(z.qty * x.price) tot from OrderDetailForMultipleProduct x,productmaster y,creditdetail z where x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno)A) + (select sum(totval) from (select sum((z.qty * x.price * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) totval from OrderDetailForMultipleProduct x,productmaster y,creditdetail z,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno union select sum((z.qty * x.price * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) totval from orderdetail x,productmaster y,creditdetail z,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno) A) Totvalue, " +
                                     "Custof = (select Custof from customermaster where cno=(select cno from ordermaster where ordno=a.ordno)),cno=(select cno from ordermaster where ordno=a.ordno)" +
                                     /*",Vehcle= case when datename(dw,orddate) not in('Saturday','Sunday') then (select week_vno from customermaster where cno=b.cno) else (select weekend_vno from customermaster where cno=b.cno) end,PricelessInvoice= (select PricelessInvoice from customermaster where cno=b.cno)," +*/
                                     ",Vehcle = DBO.Fn_GetVanNo(orddate,b.cno),PricelessInvoice= (select PricelessInvoice from customermaster where cno=b.cno)," +
                                     "(select sum(z.qty * x.ResellerPrice) from orderdetail x,productmaster y,creditdetail z where x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno) EndCustomerGoodstotal, " +
                                     "(select sum((z.qty * x.ResellerPrice * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) from orderdetail x,productmaster y,creditdetail z,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and  z.crno=a.crno) EndCustomerVat, " +
                                     "(select sum(z.qty * x.ResellerPrice) from orderdetail x,productmaster y,creditdetail z where x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno) + (select sum((z.qty * x.ResellerPrice * case when vat = 0 then 0 when vat = 1 then dbo.fnGetVat(orddate) when vat = 2 then 0 end)/100) from orderdetail x,productmaster y,creditdetail z,OrderMaster OM where OM.ordno = x.ordno and x.pno=y.pno and x.ordno=a.ordno and x.pno=z.pno and z.crno=a.crno) ResellerPriceTotvalue " +
                                     ",ResellerID= (select ResellerID from customermaster where cno=(select cno from ordermaster where ordno=a.ordno))" +
                                     ",c.ordno " +
                                     ",GroupLogo = (select case when LogoPath is null Then '' Else LogoPath End As LogoPath  from dbo.GroupMaster where GNo = (select GNo from CustomerMaster where CNo = b.CNo)) " +
                                     ",Telephone = (select case when Telephone is null Then '' Else Telephone End As Telephone  from dbo.GroupMaster where GNo = (select GNo from CustomerMaster where CNo = b.CNo)) " +
                                     " from creditmaster a,customermaster b,ordermaster c  where b.cno=c.cno and a.ordno=c.ordno and Crno = " + CrNo.ToString() + " order by Crno");
            strSql = string.Empty;
            strSql = strbQuery.ToString();
            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);

            return dt;
        }

        public DataTable Display_CreditStatementDetails(DateTime vdtCreditDate, long vlngCrNo)
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);

            strbQuery.Append("select a.crno,qty,pno,pname=(select pname from productmaster where pno=a.pno),(select price from orderdetail where ordno=(select ordno from creditmaster where crno=a.crno) and pno=a.pno) Uprice, " +
                            "(select a.qty * price from orderdetail where ordno=(select ordno from creditmaster where crno=a.crno) and pno=a.pno) price,(select ResellerPrice from orderdetail where ordno=(select ordno from creditmaster where crno=a.crno) and pno=a.pno)" +
                            "EndCustomerUprice, (select a.qty * ResellerPrice from orderdetail where ordno=(select ordno from creditmaster where crno=a.crno) and pno=a.pno) ResellerPrice from creditdetail a,creditmaster b where a.crno=b.crno and datediff(d,convert(datetime,'" + vdtCreditDate.ToString("dd/MM/yyyy") + "',103),crdate) = 0 and a.crno= " + vlngCrNo + " order by pname asc");
            strSql = string.Empty;
            strSql = strbQuery.ToString();
            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);

            return dt;  
        }

        public DataTable Display_CreditStatementDetails(int CrNo)
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);

            strbQuery.Append("select a.crno,qty,pno,pname=(select pname from productmaster where pno=a.pno),Uprice=isnull((select price from orderdetail where ordno=(select ordno from creditmaster where crno=a.crno) and pno=a.pno),dbo.fnGetMultiProductPriceForCredit(a.pno," + CrNo + ")), " +
                            "price=isnull((select a.qty * price from orderdetail where ordno=(select ordno from creditmaster where crno=a.crno) and pno=a.pno),a.qty*dbo.fnGetMultiProductPriceForCredit(a.pno," + CrNo + ")),(select ResellerPrice from orderdetail where ordno=(select ordno from creditmaster where crno=a.crno) and pno=a.pno)" +
                            "EndCustomerUprice, (select a.qty * ResellerPrice from orderdetail where ordno=(select ordno from creditmaster where crno=a.crno) and pno=a.pno) ResellerPrice from creditdetail a,creditmaster b where a.crno=b.crno and a.crno= " + CrNo + " order by pname asc");
            strSql = string.Empty;
            strSql = strbQuery.ToString();
            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);

            return dt;  
        }

        public DataTable DailyCreditResellerList(DateTime vdtOrdDate)
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);

            strbQuery.Append("select Distinct a.ResellerID,a.ResellerCreditID,b.ResellerName,b.Address,"+
                "b.Town,b.Postcode,a.CreditDate from ResellerCredit a ,"+
                "Reseller b Where a.ResellerID=b.RID AND "+
                "datediff(d,a.CreditDate,convert(datetime,'" + vdtOrdDate.ToString("dd/MM/yyyy") + "',103)) = 0 and a.Resellerid = b.RId");
           
            strSql = string.Empty;
            strSql = strbQuery.ToString();
            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);

            return dt;  
        }

        public DataTable DailyInvoiceResellerList(DateTime vdtOrdDate)
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);

            strbQuery.Append("select Distinct a.ResellerID,a.ResellerInvoiceID,"+
            "b.ResellerName,b.Address,b.Town,b.Postcode,a.OrdDate from ResellerInvoice a ,"+
            "Reseller b Where a.ResellerID=b.RID AND datediff(d,a.OrdDate,"+
            "convert(datetime,'" + vdtOrdDate.ToString("dd/MM/yyyy") + "',103)) = 0 and a.Resellerid = b.RId");
            
            strSql = string.Empty;
            strSql = strbQuery.ToString();
            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);

            return dt;  
        }

        public DataTable GetResellerInvoiceDetails(long vlngResellerID, DateTime vdtOrdDate,string vstrDaySession, long vlngInvoiceNo )
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);

            string strType = vstrDaySession.Replace("Morning", "1").Replace("Noon", "2").Replace("Evening", "3").Replace("All", "");            
            //DateTime dtValue = DateTime.Parse(vdtOrdDate.ToString("dd/MM/yyyy"));  
            strbQuery.Append("exec dbo.sp_ResellerInvoice " + vlngResellerID + ",'" + new DateTime(vdtOrdDate.Year, vdtOrdDate.Month, vdtOrdDate.Day).ToString("MM/dd/yyyy") + "','" + strType + "'," + vlngInvoiceNo);            
            strSql = string.Empty;
            strSql = strbQuery.ToString();
            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);

            return dt;  
        }

        public DataTable GetResellerCreditNoteDetails(long vlngResellerID, DateTime vdtOrdDate)
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);
           
            //DateTime dtValue = DateTime.Parse(vdtOrdDate.ToString("yyyy/MM/dd"));
            //DateTime pdate = new DateTime(vdtOrdDate.Year, vdtOrdDate.Month, vdtOrdDate.Day).ToString("MM/dd/yyyy");

            //string strPDate = new DateTime(vdtOrdDate.Year, vdtOrdDate.Month, vdtOrdDate.Day).ToString("MM/dd/yyyy");

            strbQuery.Append("exec dbo.sp_ResellerCreditNote " + vlngResellerID + ",'" + new DateTime(vdtOrdDate.Year, vdtOrdDate.Month, vdtOrdDate.Day).ToString("MM/dd/yyyy") + "'");
            strSql = string.Empty;
            strSql = strbQuery.ToString();
            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);

            return dt;
        }

        public string GetResellerCreditNoteInvoiceNos(string vRCreditNo)
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);

            strbQuery.Append(string.Format("SELECT dbo.fnGetResellerInvNos({0}) InvNos",vRCreditNo));
            strSql = string.Empty;
            strSql = strbQuery.ToString();
            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);
            string invNos = "";
            if (dt.Rows.Count > 0)
            {
                invNos = dt.Rows[0][0].ToString();
            }
            return invNos;

        }

        public string GetFooterNote()
        {
            conn = new OdbcConnection(strConnectionString);
            DataTable dt = null;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            strbQuery = new StringBuilder();
            if (strbQuery != null)
                strbQuery.Remove(0, strbQuery.Length);

            strbQuery.Append(string.Format("SELECT TOP 1 FooterText FROM InvoiceFooter Order by CreatedDate DESC"));
            strSql = string.Empty;
            strSql = strbQuery.ToString();
            OdbcDataAdapter da = new OdbcDataAdapter(strSql, conn);
            dt = new DataTable();
            da.Fill(dt);
            string footerText = "";
            if (dt.Rows.Count > 0)
            {
                footerText = dt.Rows[0][0].ToString();
            }
            return footerText;

        }

    }
