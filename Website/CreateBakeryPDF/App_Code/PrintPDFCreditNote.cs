using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for PrintInvoice
/// </summary>
public class PrintPDFCreditNote
{
    public static string GenerateDoc(int docNo, string fileName, bool printUnDistributableDoc)
    {
        ProcessReportAllInvoices a = new ProcessReportAllInvoices();
        GeneratePdfDoc generateInvoice = new GeneratePdfDoc();
        string generatedFileName = "";

        DataTable dtCredit = a.Display_CreditStatements(docNo);

        //Invoice item Details
        DataTable dtInvoiceDetails = a.Display_CreditStatementDetails(docNo);

        //int intPriceLessInvoice = 1;
        int i = 0;
        int totDocsToPrint = 1;
        if (dtCredit.Rows[i][14].ToString() == "1")
        {
            generateInvoice.SetPricelessInv = "Y";
            generateInvoice.SetDeliveryNote= "Y";

            if (printUnDistributableDoc)
            {
                totDocsToPrint = 2;
                generateInvoice.PrintNotDistributableLogo = true;
                generateInvoice.SetPricelessInv = "N";
                generateInvoice.SetDeliveryNote= "N";

            }

            generateInvoice.SetPriceLessPrinted = 2;
        }
        else
        {
            generateInvoice.SetPricelessInv = "N";                           
            generateInvoice.SetDeliveryNote = "N";
            generateInvoice.SetPriceLessPrinted = 1;
        }

        for (int docCount = 1; docCount <= totDocsToPrint; docCount++)
        {

            if (docCount == 2)
            {
                generateInvoice.PrintNotDistributableLogo = false;
                generateInvoice.SetPricelessInv = "Y";                           
                generateInvoice.SetDeliveryNote = "Y";
                generateInvoice.SetPriceLessPrinted = 2;
            }

            generateInvoice.SetCreditStatement = "Y";

            //credit Header details
            generateInvoice.SetInvoiceTable = dtInvoiceDetails;
            generateInvoice.DocType = "Credit";
            generateInvoice.SetInvoiceNo = "Credit Number: " + dtCredit.Rows[i]["Crno"].ToString().Replace(@"\n", Environment.NewLine);

            StringBuilder strbAddress = new StringBuilder();

            if (dtCredit.Rows[i][4].ToString() != "" || dtCredit.Rows[i][4].ToString() != string.Empty)
                strbAddress.Append(dtCredit.Rows[i][4].ToString() + ",");
            if (dtCredit.Rows[i][5].ToString() != "" || dtCredit.Rows[i][5].ToString() != string.Empty)
                strbAddress.Append(dtCredit.Rows[i][5].ToString() + ",");
            if (dtCredit.Rows[i][6].ToString() != "" || dtCredit.Rows[i][6].ToString() != string.Empty)
                strbAddress.Append(dtCredit.Rows[i][6].ToString() + ",");
            if (dtCredit.Rows[i][7].ToString() != "" || dtCredit.Rows[i][7].ToString() != string.Empty)
                strbAddress.Append(dtCredit.Rows[i][7].ToString());


            string strAddress = "Invoice No: " + dtCredit.Rows[i][19].ToString() + "\n" + "Customer Code: " + dtCredit.Rows[i][12].ToString() + "\n" + "Van #: " + dtCredit.Rows[i][13].ToString() + "\n" + "Customer Name: " + dtCredit.Rows[i][2].ToString() + "\n" + "Address: " + strbAddress.ToString() + "\n\n" + "Credit Date:" + dtCredit.Rows[i][1].ToString();
            generateInvoice.SetCustomerInfo = strAddress.Replace(@"\n", Environment.NewLine);
            string strInvoiceInfo = "";
            generateInvoice.SetInvoiceInfo = strInvoiceInfo.Replace(@"\n", Environment.NewLine);

            //Check whether reseller id is available if so show reseller details else customer details
            if (dtCredit.Rows[i]["resellerID"].ToString() == "0" || dtCredit.Rows[i]["resellerID"].ToString() == "")
            {
                if (dtCredit.Rows[i]["Custof"].ToString() == "Bread Factory")
                {
                    if (!string.IsNullOrEmpty(dtCredit.Rows[0][20].ToString()))
                        generateInvoice.SetAddress = new Support().GetValuesFromConfig("BF").ToString().Replace(@"\n", Environment.NewLine) + "Telephone : " + (dtCredit.Rows[0][21].ToString() != string.Empty ? dtCredit.Rows[0][21].ToString() : "_"); 
                    else
                        generateInvoice.SetAddress = new Support().GetValuesFromConfig("BF_ContactDetails").ToString().Replace(@"\n", Environment.NewLine);
                    
                    generateInvoice.SetVATNO = new Support().GetValuesFromConfig("VATNOBF").ToString().Replace(@"\n", Environment.NewLine) + "\n\n";
                    generateInvoice.SetInvoiceVerb = new Support().GetValuesFromConfig("BFInvoiceVerbCreditStatement").ToString().Replace(@"\n", Environment.NewLine);
                    generateInvoice.SetinvFooter = "";// new Support().GetValuesFromConfig("BFInvoiceFooter").ToString().Replace(@"\n", Environment.NewLine);

                    if (!string.IsNullOrEmpty(dtCredit.Rows[0][20].ToString()))
                        generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GroupLogoLocation").ToString() + dtCredit.Rows[0][20].ToString();
                    else
                        generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("BFImage").ToString();
                }
                else if (dtCredit.Rows[i]["Custof"].ToString() == "Gail Force")
                {
                    if (!string.IsNullOrEmpty(dtCredit.Rows[0][20].ToString()))
                        generateInvoice.SetAddress = new Support().GetValuesFromConfig("GF").ToString().Replace(@"\n", Environment.NewLine) + "Telephone : " + (dtCredit.Rows[0][21].ToString() != string.Empty ? dtCredit.Rows[0][21].ToString() : "_");
                    else
                        generateInvoice.SetAddress = new Support().GetValuesFromConfig("GF_ContactDetails").ToString().Replace(@"\n", Environment.NewLine);
                    generateInvoice.SetVATNO = new Support().GetValuesFromConfig("VATNOGF").ToString().Replace(@"\n", Environment.NewLine) + "\n\n"; 
                    generateInvoice.SetInvoiceVerb = new Support().GetValuesFromConfig("BFInvoiceVerbCreditStatement").ToString().Replace(@"\n", Environment.NewLine);
                    generateInvoice.SetinvFooter = "";//new Support().GetValuesFromConfig("BFInvoiceFooter").ToString().Replace(@"\n", Environment.NewLine);

                    if (!string.IsNullOrEmpty(dtCredit.Rows[0][20].ToString()))
                        generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GroupLogoLocation").ToString() + dtCredit.Rows[0][20].ToString();
                    else
                        generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GFImage").ToString();
                }
            }

            else if (dtCredit.Rows[i]["resellerID"].ToString() == "1")
            {
                //has reseller id
                string strResellerAddress = string.Empty;
                DataTable dtReseller = a.DisplayResellerDetail(long.Parse(dtCredit.Rows[i]["resellerID"].ToString()));

                if (!string.IsNullOrEmpty(dtCredit.Rows[0][20].ToString()))
                    strResellerAddress = "\n" + dtReseller.Rows[0][1].ToString() + "\n" + dtReseller.Rows[0][2].ToString() + "\n" + dtReseller.Rows[0][3].ToString() + "\n" + dtReseller.Rows[0][4].ToString() + "\n\n" + "Telephone: " + (dtCredit.Rows[0][21].ToString() != string.Empty ? dtCredit.Rows[0][21].ToString() : "_");
                else
                    strResellerAddress = "\n" + dtReseller.Rows[0][1].ToString() + "\n" + dtReseller.Rows[0][2].ToString() + "\n" + dtReseller.Rows[0][3].ToString() + "\n" + dtReseller.Rows[0][4].ToString() + "\n\n" + "Telephone: " + dtReseller.Rows[0][5].ToString() + "\n" + "Fax: " + dtReseller.Rows[0][6].ToString();


                //strResellerAddress = "\n" + dtReseller.Rows[0][1].ToString() + "\n" + dtReseller.Rows[0][2].ToString() + "\n" + dtReseller.Rows[0][3].ToString() + "\n" + dtReseller.Rows[0][4].ToString() + "\n\n" + "Telephone: " + dtReseller.Rows[0][5].ToString() + "\n" + "Fax: " + dtReseller.Rows[0][6].ToString();
                generateInvoice.SetAddress = strResellerAddress.Replace(@"\n", Environment.NewLine); ;
                generateInvoice.SetVATNO = dtReseller.Rows[0]["VATRegNo"].ToString() + "\n\n";
                generateInvoice.SetinvFooter = "";//dtReseller.Rows[0]["InvoiceFooter"].ToString().Replace(@"\n", Environment.NewLine);


                if (!string.IsNullOrEmpty(dtCredit.Rows[0][20].ToString()))
                    generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GroupLogoLocation").ToString() + dtCredit.Rows[0][20].ToString();
                else
                    generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("ResellerImagePath").ToString() + dtReseller.Rows[0]["Logo"].ToString();
            }

            //credit totals
            if (dtCredit.Rows[i][8].ToString() != "")
                generateInvoice.SetGoodsTotal = float.Parse(dtCredit.Rows[i][8].ToString());
            else
                generateInvoice.SetGoodsTotal = 0;

            if (dtCredit.Rows[i][9].ToString() != "")
                generateInvoice.SetVatTotal = float.Parse(dtCredit.Rows[i][9].ToString());
            else
                generateInvoice.SetVatTotal = 0;

            if (dtCredit.Rows[i][10].ToString() != "")
                generateInvoice.SetInvoiceTotal = float.Parse(dtCredit.Rows[i][10].ToString());
            else
                generateInvoice.SetInvoiceTotal = 0;

            string tmpFName = generateInvoice.GenerateBakeryDoc(docNo, fileName, false);
            if (tmpFName != null)
                generatedFileName = tmpFName;

            generateInvoice.SetPricelessInv = "N";
            generateInvoice.SetDeliveryNote = "N";
            generateInvoice.SetPriceLessPrinted = 1;
        }

        generateInvoice.CloseBakeryDoc();

        return generatedFileName;
    }
}
