using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.IO;
/// <summary>
/// Summary description for BakeryPDFGenSrv
/// </summary>
[WebService(Namespace = "http://www.breadltd.co.uk/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class BakeryPDFGenSrv : System.Web.Services.WebService {

    public BakeryPDFGenSrv () 
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public int GenerateDailyPDFDocs(string deldate) {
        DateTime genDate = DateTime.ParseExact(deldate, "yyyy-MM-dd", null);
        int totDocsGenerated = DailyPDFGenerator.GenerateDocs(genDate);
        return totDocsGenerated;
    }
    
}

