using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Web;

    public class InvoiceLogic
    {
        GenerateInvoice generateInvoice = new GenerateInvoice();
        string message = string.Empty;
        int intPriceLessInvoice = 1;
        string m_printCreditCNO = string.Empty;
        string[] arrCNO = null;

        public InvoiceLogic() { }

        public string GenInvoiceReports(DateTime dtDate)
        {
            DateTime vdtDate = dtDate;
            
            try
            {
                
                
                
                for (int intCurtno = 1; intCurtno <= 3; intCurtno++)
                {
                    Invoices(intCurtno, vdtDate);

                   
                }


               // CreditStatements(vdtDate, "634");
               // CreditStatements(vdtDate);
                //if(!string.IsNullOrEmpty(m_printCreditCNO))
                //    CreditStatements(vdtDate, "0", 1, m_printCreditCNO);
                ResellerInvoice(vdtDate);
                CreditReseller(vdtDate);
                generateInvoice.ClosePDF();

                return message;
            }
            catch (Exception ex)
            {
                message = ex.Message + "Stack " + ex.StackTrace + "dt" + vdtDate.ToString() + "inner exception " + ex.InnerException.Source  ;
                return message;
            }
        }

        public void Invoices(int intCurtno, DateTime vdtDate)
        {
            try
            {
                string sFooterText = GetFooterText();
                string strTName = string.Empty;
                string sCNO = string.Empty;
                //get day session
                if (intCurtno == 1)
                    strTName = "Morning";
                else if (intCurtno == 2)
                    strTName = "Noon";
                else if (intCurtno == 3)
                    strTName = "Evening";

                //get all invoices 
                ProcessReportAllInvoices a = new ProcessReportAllInvoices();
                DataTable dsInvoiceTotal = a.GetInvoiceTotal(0, vdtDate, strTName);

                //check whether has invoices 
                if (dsInvoiceTotal.Rows.Count > 0)
                {
                    int intRowCount = 0;
                    intRowCount = dsInvoiceTotal.Rows.Count;

                    //Loop through the invoice master
                    for (int i = 0; i <= intRowCount - 1; i++)
                    {
                        //get no of coppies to be prited of current job
                        int intNOI2Bprinted = int.Parse(dsInvoiceTotal.Rows[i]["NoofInvoices"].ToString());//No Of Invoices to b printed
                        int intDeliveryNote = int.Parse(dsInvoiceTotal.Rows[i][16].ToString());

                        generateInvoice.SetDeliveryNote = "N";
                        if (intDeliveryNote == 1)
                        {
                            generateInvoice.SetDeliveryNote = "Y";
                            //intNOI2Bprinted = 1;
                        }


                        for (int x = 1; x <= intNOI2Bprinted; x++)
                        {
                            intPriceLessInvoice = 1;
                            if (dsInvoiceTotal.Rows[i][16].ToString() == "1")
                            {
                                generateInvoice.SetPricelessInv = "Y";
                                if (generateInvoice.SetDeliveryNote=="Y")
                                    intPriceLessInvoice = 1;
                            }
                            else
                            {
                                generateInvoice.SetPricelessInv = "N";
                            }

                            for (int z = 1; z <= intPriceLessInvoice; z++)
                            {
                                if (z == 2)
                                {
                                    generateInvoice.SetPriceLessPrinted = 2;
                                }

                                //Invoice item Details
                                DataTable dtInvoiceDetails = a.GetInvoiceDetails(long.Parse(dsInvoiceTotal.Rows[i]["ordNo"].ToString()));
                                generateInvoice.SetInvoiceTable = dtInvoiceDetails;
                                //Invoice Header details
                                if (intDeliveryNote != 1)
                                {
                                    generateInvoice.SetInvoiceNo = "Invoice #: " + dsInvoiceTotal.Rows[i]["ordNo"].ToString().Replace(@"\n", Environment.NewLine);
                                }
                                else if (intDeliveryNote == 1)
                                {
                                    generateInvoice.SetInvoiceNo = "Delivery Note for INVOICE  #: " + dsInvoiceTotal.Rows[i]["ordNo"].ToString().Replace(@"\n", Environment.NewLine);
                                }

                                StringBuilder strbAddress = new StringBuilder();
                                sCNO = dsInvoiceTotal.Rows[i][14].ToString();

                                if (dsInvoiceTotal.Rows[i][14].ToString() != "" || dsInvoiceTotal.Rows[i][14].ToString() != string.Empty)
                                    strbAddress.Append(dsInvoiceTotal.Rows[i][14].ToString() + "\n");
                                if (dsInvoiceTotal.Rows[i][2].ToString() != "" || dsInvoiceTotal.Rows[i][2].ToString() != string.Empty)
                                    strbAddress.Append(dsInvoiceTotal.Rows[i][2].ToString() + "\n");
                                if (dsInvoiceTotal.Rows[i][5].ToString() != "" || dsInvoiceTotal.Rows[i][5].ToString() != string.Empty)
                                    strbAddress.Append(dsInvoiceTotal.Rows[i][5].ToString() + "\n");
                                if (dsInvoiceTotal.Rows[i][6].ToString() != "" || dsInvoiceTotal.Rows[i][6].ToString() != string.Empty)
                                    strbAddress.Append(dsInvoiceTotal.Rows[i][6].ToString() + "\n");
                                if (dsInvoiceTotal.Rows[i][7].ToString() != "" || dsInvoiceTotal.Rows[i][7].ToString() != string.Empty)
                                    strbAddress.Append(dsInvoiceTotal.Rows[i][7].ToString() + "\n");
                                if (dsInvoiceTotal.Rows[i][8].ToString() != "" || dsInvoiceTotal.Rows[i][8].ToString() != string.Empty)
                                    strbAddress.Append(dsInvoiceTotal.Rows[i][8].ToString() + "\n"); 
                               
                               
                                string strCustomerAddress = strbAddress.ToString();
                                generateInvoice.SetCustomerInfo = strCustomerAddress.Replace(@"\n", Environment.NewLine);
                                string strInvoiceInfo = "Purchase Order No: " + dsInvoiceTotal.Rows[i]["pono"].ToString() + "\n\n" + "van #: " + dsInvoiceTotal.Rows[i]["Vehcle"].ToString() + "\n\n" + "Date: " + DateTime.Parse(dsInvoiceTotal.Rows[i]["OrdDate"].ToString()).ToString("dd/MM/yyyy");
                                generateInvoice.SetInvoiceInfo = strInvoiceInfo.Replace(@"\n", Environment.NewLine);

                                //Check whether reseller id is available if so show reseller details else FACTORY DETAILS
                                if (dsInvoiceTotal.Rows[i]["resellerID"].ToString() == "0" || dsInvoiceTotal.Rows[i]["resellerID"].ToString() == "")
                                {
                                    if (dsInvoiceTotal.Rows[i]["Custof"].ToString() == "Bread Factory")
                                    {
                                        if (!string.IsNullOrEmpty(dsInvoiceTotal.Rows[i][22].ToString()))
                                            generateInvoice.SetAddress = new Support().GetValuesFromConfig("BF").ToString().Replace(@"\n", Environment.NewLine) + "Telephone : " + (dsInvoiceTotal.Rows[i][23].ToString() != string.Empty ? dsInvoiceTotal.Rows[i][23].ToString() : "_"); 
                                        else
                                            generateInvoice.SetAddress = new Support().GetValuesFromConfig("BF_ContactDetails").ToString().Replace(@"\n", Environment.NewLine);
                                        generateInvoice.SetVATNO = new Support().GetValuesFromConfig("VATNOBF").ToString();
                                        generateInvoice.SetInvoiceVerb = new Support().GetValuesFromConfig("BFInvoiceVerb").ToString().Replace(@"\n", Environment.NewLine);
                                       // generateInvoice.SetinvFooter = new Support().GetValuesFromConfig("BFInvoiceFooter").ToString().Replace(@"\n", Environment.NewLine);
                                        generateInvoice.SetinvFooter = sFooterText;
                                        generateInvoice.SetinvFooter1 = new Support().GetValuesFromConfig("BFInvoiceFooter1").ToString().Replace(@"\n", Environment.NewLine);
                                       // generateInvoice.SetinvFooter1 = "invoice";//new Support().GetValuesFromConfig("BFInvoiceFooter1").ToString().Replace(@"\n", Environment.NewLine);

                                        if (!string.IsNullOrEmpty(dsInvoiceTotal.Rows[i][22].ToString()))
                                        {
                                            generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GroupLogoLocation").ToString() + dsInvoiceTotal.Rows[i][22].ToString();
                                        }
                                        else
                                        {

                                            if (z != 2 && generateInvoice.SetPricelessInv == "Y" && generateInvoice.SetDeliveryNote != "Y")
                                                generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("PriceLessImage").ToString();
                                            else
                                                generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GFImage").ToString();
                                        }

                                    }
                                    else if (dsInvoiceTotal.Rows[i]["Custof"].ToString() == "Gail Force")
                                    {
                                        if (!string.IsNullOrEmpty(dsInvoiceTotal.Rows[i][22].ToString()))
                                             generateInvoice.SetAddress = new Support().GetValuesFromConfig("GF").ToString().Replace(@"\n", Environment.NewLine) + "Telephone : " + (dsInvoiceTotal.Rows[i][23].ToString() != string.Empty ? dsInvoiceTotal.Rows[i][23].ToString() : "_"); 
                                        else
                                            generateInvoice.SetAddress = new Support().GetValuesFromConfig("GF_ContactDetails").ToString().Replace(@"\n", Environment.NewLine); ;
                                        generateInvoice.SetVATNO = new Support().GetValuesFromConfig("VATNOGF").ToString();
                                        generateInvoice.SetInvoiceVerb = new Support().GetValuesFromConfig("GFInvoiceVerb").ToString().Replace(@"\n", Environment.NewLine);
                                       // generateInvoice.SetinvFooter = new Support().GetValuesFromConfig("BFInvoiceFooter").ToString().Replace(@"\n", Environment.NewLine);
                                        generateInvoice.SetinvFooter = sFooterText + new Support().GetValuesFromConfig("BFInvoiceFooter").ToString().Replace(@"\n", Environment.NewLine);

                                        if (!string.IsNullOrEmpty(dsInvoiceTotal.Rows[i][22].ToString()))
                                        {
                                            generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GroupLogoLocation").ToString() + dsInvoiceTotal.Rows[i][22].ToString();
                                        }
                                        else
                                        {

                                            if (z != 2 && generateInvoice.SetPricelessInv == "Y" && generateInvoice.SetDeliveryNote == "N")
                                                generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("PriceLessImage").ToString();
                                            else
                                                generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GFImage").ToString();
                                        }
                                    }
                                }
                                else if (dsInvoiceTotal.Rows[i]["resellerID"].ToString() == "1")
                                {
                                    //has reseller id
                                    string strResellerAddress = string.Empty;
                                    DataTable dtReseller = a.DisplayResellerDetail(long.Parse(dsInvoiceTotal.Rows[i]["resellerID"].ToString()));

                                    if (!string.IsNullOrEmpty(dsInvoiceTotal.Rows[i][22].ToString()))
                                    {
                                        strResellerAddress = dtReseller.Rows[0][2].ToString() + dtReseller.Rows[0][3].ToString() + dtReseller.Rows[0][4].ToString() + "\n\nTelephone : " + (dsInvoiceTotal.Rows[i][23].ToString() != string.Empty ? dsInvoiceTotal.Rows[i][23].ToString() : "_"); 
                                        generateInvoice.SetAddress = strResellerAddress;
                                    }
                                    else
                                    {
                                        strResellerAddress = dtReseller.Rows[0][2].ToString() + dtReseller.Rows[0][3].ToString() + dtReseller.Rows[0][4].ToString() + dtReseller.Rows[0][5].ToString() + dtReseller.Rows[0][6].ToString();
                                        generateInvoice.SetAddress = strResellerAddress;
                                    }
                                    generateInvoice.SetVATNO = dtReseller.Rows[0]["VATRegNo"].ToString();
                                    //generateInvoice.SetinvFooter = new Support().GetValuesFromConfig("BFInvoiceFooter").ToString().Replace(@"\n", Environment.NewLine);
                                    generateInvoice.SetinvFooter = sFooterText;
                                    generateInvoice.SetinvFooter1 = new Support().GetValuesFromConfig("BFInvoiceFooter2").ToString().Replace(@"\n", Environment.NewLine);

                                    //get reseller footer deails from databse
                                    // generateInvoice.SetinvFooter = dtReseller.Rows[0]["InvoiceFooter"].ToString();

                                    if (!string.IsNullOrEmpty(dsInvoiceTotal.Rows[i][22].ToString()))
                                    {
                                        generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GroupLogoLocation").ToString() + dsInvoiceTotal.Rows[i][22].ToString();
                                    }
                                    else
                                    {
                                        generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("ResellerImagePath").ToString() + dtReseller.Rows[0]["Logo"].ToString();
                                    }
                                }

                                //Invoice totals
                                generateInvoice.SetGoodsTotal = float.Parse(dsInvoiceTotal.Rows[i][11].ToString());
                                generateInvoice.SetVatTotal = float.Parse(dsInvoiceTotal.Rows[i][12].ToString());
                                generateInvoice.SetInvoiceTotal = float.Parse(dsInvoiceTotal.Rows[i][13].ToString());
                                generateInvoice.GenerateBakeryInvoice();
                                if (z == 2)
                                {
                                    generateInvoice.SetPriceLessPrinted = 1;
                                }

                            }
                        }
                        generateInvoice.SetDeliveryNote = "N";




                        if (!string.IsNullOrEmpty(sCNO))
                        {
                            CreditStatements(vdtDate, sCNO, 0, string.Empty);
                            m_printCreditCNO = m_printCreditCNO + "|" + sCNO;
                             
                        }
                        
                       
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message + "Stack " + ex.StackTrace + "dt" + vdtDate.ToString();
            }

        }

        public void CreditStatements(DateTime vdtDate, string cno, int nIndividualCreditNote, string listofCNO)
        {
            try
            {
                //get all credit statements 
                ProcessReportAllInvoices a = new ProcessReportAllInvoices();
                
                //bcause of dateformat i -1 from AddDays method actually here its substract date
                DataTable dtCredit = a.Display_CreditStatements(vdtDate.AddDays(-1), cno, nIndividualCreditNote, listofCNO);
                //Added by thilina to test
             //   DataTable dtCredit = a.Display_CreditStatements(vdtDate, cno);

                //check availability of credit statements 
                if (dtCredit.Rows.Count > 0)
                {
                    int intRowCount = 0;
                    intRowCount = dtCredit.Rows.Count;
                    //Loop through the credit statements
                    for (int i = 0; i <= intRowCount - 1; i++)
                    {
                        //Invoice item Details
                        DataTable dtInvoiceDetails = a.Display_CreditStatementDetails(vdtDate.AddDays(-1), long.Parse(dtCredit.Rows[i]["Crno"].ToString()));

                        //int intPriceLessInvoice = 1;
                        if (dtCredit.Rows[i][14].ToString() == "1")
                        {
                            generateInvoice.SetPricelessInv = "Y";
                            generateInvoice.SetDeliveryNote= "Y";
                            generateInvoice.SetPriceLessPrinted = 2;
                        }
                        else
                        {
                            generateInvoice.SetPricelessInv = "N";                           
                            generateInvoice.SetDeliveryNote = "N";
                            generateInvoice.SetPriceLessPrinted = 1;
                        }

                        generateInvoice.SetCreditStatement = "Y";

                        //credit Header details
                        generateInvoice.SetInvoiceTable = dtInvoiceDetails;
                        generateInvoice.SetInvoiceNo = "Credit \n Number: " + dtCredit.Rows[i]["Crno"].ToString().Replace(@"\n",Environment.NewLine);

                        StringBuilder strbAddress = new StringBuilder();

                        if (dtCredit.Rows[i][4].ToString() != "" || dtCredit.Rows[i][4].ToString() != string.Empty)
                            strbAddress.Append(dtCredit.Rows[i][4].ToString()+"," );
                        if (dtCredit.Rows[i][5].ToString() != "" || dtCredit.Rows[i][5].ToString() != string.Empty)
                            strbAddress.Append(dtCredit.Rows[i][5].ToString()+ ",");
                        if (dtCredit.Rows[i][6].ToString() != "" || dtCredit.Rows[i][6].ToString() != string.Empty)
                            strbAddress.Append(dtCredit.Rows[i][6].ToString()+ ",");
                        if (dtCredit.Rows[i][7].ToString() != "" || dtCredit.Rows[i][7].ToString() != string.Empty)
                            strbAddress.Append(dtCredit.Rows[i][7].ToString());
                       

                        string strAddress = "Invoice No: " + dtCredit.Rows[i][19].ToString() + "\n" + "Customer Code: " + dtCredit.Rows[i][12].ToString() + "\n" + "Van Number: " + dtCredit.Rows[i][13].ToString() + "\n" + "Customer Name: " + dtCredit.Rows[i][2].ToString() + "\n" + "Address: " + strbAddress.ToString() + "\n\n" + "Date:" + dtCredit.Rows[i][1].ToString();
                        generateInvoice.SetCustomerInfo = strAddress.Replace(@"\n", Environment.NewLine);
                        string strInvoiceInfo = "";
                        generateInvoice.SetInvoiceInfo = strInvoiceInfo.Replace(@"\n", Environment.NewLine);
                        
                        //Check whether reseller id is available if so show reseller details else customer details
                        if (dtCredit.Rows[i]["resellerID"].ToString() == "0" || dtCredit.Rows[i]["resellerID"].ToString() == "")
                        {
                            if (dtCredit.Rows[i]["Custof"].ToString() == "Bread Factory")
                            {
                                if (!string.IsNullOrEmpty(dtCredit.Rows[i][20].ToString()))
                                        generateInvoice.SetAddress = new Support().GetValuesFromConfig("BF").ToString().Replace(@"\n", Environment.NewLine) + "Telephone : " + (dtCredit.Rows[i][21].ToString() != string.Empty ? dtCredit.Rows[i][21].ToString() : "_"); 
                                else
                                    generateInvoice.SetAddress = new Support().GetValuesFromConfig("BF_ContactDetails").ToString().Replace(@"\n", Environment.NewLine);
                                
                                generateInvoice.SetVATNO = new Support().GetValuesFromConfig("VATNOBF").ToString().Replace(@"\n", Environment.NewLine);
                                generateInvoice.SetInvoiceVerb = new Support().GetValuesFromConfig("BFInvoiceVerbCreditStatement").ToString().Replace(@"\n", Environment.NewLine);
                                generateInvoice.SetinvFooter = "";// new Support().GetValuesFromConfig("BFInvoiceFooter").ToString().Replace(@"\n", Environment.NewLine);
                                

                                if (!string.IsNullOrEmpty(dtCredit.Rows[i][20].ToString()))
                                {
                                    generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GroupLogoLocation").ToString() + dtCredit.Rows[i][20].ToString();
                                }
                                else
                                {
                                    generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("BFImage").ToString();
                                }
                            }
                            else if (dtCredit.Rows[i]["Custof"].ToString() == "Gail Force")
                            {
                                if (!string.IsNullOrEmpty(dtCredit.Rows[i][20].ToString()))
                                    generateInvoice.SetAddress = new Support().GetValuesFromConfig("GF").ToString().Replace(@"\n", Environment.NewLine) + "Telephone : " + (dtCredit.Rows[i][21].ToString() != string.Empty ? dtCredit.Rows[i][21].ToString() : "_"); 
                                else
                                    generateInvoice.SetAddress = new Support().GetValuesFromConfig("GF_ContactDetails").ToString().Replace(@"\n", Environment.NewLine);
                                generateInvoice.SetVATNO = new Support().GetValuesFromConfig("VATNOGF").ToString().Replace(@"\n", Environment.NewLine); ;
                                generateInvoice.SetInvoiceVerb = new Support().GetValuesFromConfig("BFInvoiceVerbCreditStatement").ToString().Replace(@"\n", Environment.NewLine);
                                generateInvoice.SetinvFooter = "";//new Support().GetValuesFromConfig("BFInvoiceFooter").ToString().Replace(@"\n", Environment.NewLine);

                                if (!string.IsNullOrEmpty(dtCredit.Rows[i][20].ToString()))
                                {
                                    generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GroupLogoLocation").ToString() + dtCredit.Rows[i][20].ToString();
                                }
                                else
                                {
                                    generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GFImage").ToString();
                                }
                            }
                        }  

                        else if (dtCredit.Rows[i]["resellerID"].ToString() == "1")
                        {
                            //has reseller id
                            string strResellerAddress = string.Empty;
                            DataTable dtReseller = a.DisplayResellerDetail(long.Parse(dtCredit.Rows[i]["resellerID"].ToString()));

                            if (!string.IsNullOrEmpty(dtCredit.Rows[i][20].ToString()))
                            {
                                strResellerAddress = dtReseller.Rows[0][2].ToString() + dtReseller.Rows[0][3].ToString() + dtReseller.Rows[0][4].ToString() + "\n\nTelephone : " + (dtCredit.Rows[i][21].ToString() != string.Empty ? dtCredit.Rows[i][21].ToString() : "_");
                                generateInvoice.SetAddress = strResellerAddress;
                            }
                            else
                            {

                                strResellerAddress = dtReseller.Rows[0][2].ToString() + "\n" + dtReseller.Rows[0][3].ToString() + "\n" + dtReseller.Rows[0][4].ToString() + "\n" + dtReseller.Rows[0][5].ToString() + "\n" + dtReseller.Rows[0][6].ToString();
                                generateInvoice.SetAddress = strResellerAddress.Replace(@"\n", Environment.NewLine);
                            }
                            generateInvoice.SetVATNO = dtReseller.Rows[0]["VATRegNo"].ToString();
                            generateInvoice.SetinvFooter = "";//dtReseller.Rows[0]["InvoiceFooter"].ToString().Replace(@"\n", Environment.NewLine);

                            if (!string.IsNullOrEmpty(dtCredit.Rows[i][20].ToString()))
                            {
                                generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GroupLogoLocation").ToString() + dtCredit.Rows[i][20].ToString();
                            }
                            else
                            {
                                generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("ResellerImagePath").ToString() + dtReseller.Rows[0]["Logo"].ToString();
                            }
                        }

                        //credit totals
                        if (dtCredit.Rows[i][8].ToString() != "")
                            generateInvoice.SetGoodsTotal = float.Parse(dtCredit.Rows[i][8].ToString());
                        else
                            generateInvoice.SetGoodsTotal = 0;

                        if (dtCredit.Rows[i][9].ToString() != "")
                            generateInvoice.SetVatTotal = float.Parse(dtCredit.Rows[i][9].ToString());
                        else
                            generateInvoice.SetVatTotal = 0;

                        if (dtCredit.Rows[i][10].ToString() != "")
                            generateInvoice.SetInvoiceTotal = float.Parse(dtCredit.Rows[i][10].ToString());
                        else
                            generateInvoice.SetInvoiceTotal = 0;

                        generateInvoice.GenerateBakeryInvoice();
                        generateInvoice.SetPricelessInv = "N";
                        generateInvoice.SetDeliveryNote = "N";
                        generateInvoice.SetPriceLessPrinted = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message + "Stack " + ex.StackTrace + "dt" + vdtDate.ToString();
            }
        }


        public void ResellerInvoice(DateTime vdtDate)
        {

            float fltConverter = 0;

            try
            {
                generateInvoice.SetstrReseller="Y";             
            
                //get all credit statements 
                ProcessReportAllInvoices a = new ProcessReportAllInvoices();               
                DataTable dtResellerInv = a.DailyInvoiceResellerList(vdtDate);
                //generateInvoice.SetPricelessInv = "N";
                //check availability of credit statements 
                if (dtResellerInv.Rows.Count > 0)
                {
                    int intRowCount = 0;
                    intRowCount = dtResellerInv.Rows.Count;
                    //Loop through the credit statements
                    for (int i = 0; i <= intRowCount - 1; i++)
                    {
                        //Invoice item Details
                        
                        DataTable dtResellerInvDetails = a.GetResellerInvoiceDetails(long.Parse(dtResellerInv.Rows[i]["ResellerID"].ToString()), vdtDate, "", long.Parse(dtResellerInv.Rows[i]["ResellerInvoiceID"].ToString()));

                        generateInvoice.SetInvoiceTable = dtResellerInvDetails;
                        generateInvoice.SetInvoiceNo = "Reseller INVOICE # : " + dtResellerInv.Rows[i]["ResellerInvoiceID"].ToString().Replace(@"\n",Environment.NewLine);
                        //generateInvoice.SetCustomerInfo = "Customer Info";
                        generateInvoice.SetInvoiceInfo = "Date :" + DateTime.Parse(dtResellerInv.Rows[0][6].ToString()).ToString("dd/MM/yyyy"); ;

                        string strResellerAddress = string.Empty;
                        DataTable dtReseller = a.DisplayResellerDetail(long.Parse(dtResellerInv.Rows[i]["resellerID"].ToString()));
                        strResellerAddress = dtResellerInv.Rows[0][0].ToString() + "\n" + dtResellerInv.Rows[0][2].ToString() + "\n" + dtResellerInv.Rows[0][3].ToString() + "\n" + dtResellerInv.Rows[0][4].ToString() + "\n" + dtResellerInv.Rows[0][5].ToString().Replace(@"\n", Environment.NewLine);

                        generateInvoice.SetInvoiceVerb = new Support().GetValuesFromConfig("BFInvoiceVerb").ToString().Replace(@"\n", Environment.NewLine);

                        //IN RESELLER INVOICE CUSTOMER ADDRESS IS THE RESELLER ADDRESS
                        generateInvoice.SetCustomerInfo = strResellerAddress;
                        //ALWAYS TOP ADDRESS IS BREAD FACTORY
                        generateInvoice.SetAddress = new Support().GetValuesFromConfig("BF_ContactDetails").ToString().Replace(@"\n", Environment.NewLine);

                        generateInvoice.SetVATNO = new Support().GetValuesFromConfig("ResellerVATNO").ToString();
                        generateInvoice.SetinvFooter = new Support().GetValuesFromConfig("BFInvoiceFooter").ToString().Replace(@"\n", Environment.NewLine);
                        generateInvoice.SetinvFooter1 = new Support().GetValuesFromConfig("BFInvoiceFooter1").ToString().Replace(@"\n", Environment.NewLine);
                       // generateInvoice.SetinvFooter1 = "reseller invoive";//new Support().GetValuesFromConfig("BFInvoiceFooter1").ToString().Replace(@"\n", Environment.NewLine);

                        //generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("ResellerImagePath").ToString() + dtReseller.Rows[0]["Logo"].ToString();
                        generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("BFImage").ToString() ;
                       // generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("ResellerImagePath").ToString();
                        float fltGoodsTotal = 0;
                        float fltVATTotal = 0;
                        float fltInvTotal = 0;
                        //Get Reseller Totals from detailtable
                        for (int x = 0; x <= dtResellerInvDetails.Rows.Count - 1; x++)
                        {
                            float.TryParse(dtResellerInvDetails.Rows[x][4].ToString(), out fltConverter);
                            fltGoodsTotal = fltGoodsTotal + fltConverter;
                            float.TryParse(dtResellerInvDetails.Rows[x][5].ToString(), out fltConverter);
                            fltVATTotal = fltVATTotal + fltConverter;
                            float.TryParse(dtResellerInvDetails.Rows[x][4].ToString(), out fltConverter);
                            fltInvTotal = fltGoodsTotal + fltVATTotal;
                        }

                        generateInvoice.SetGoodsTotal = fltGoodsTotal;
                        generateInvoice.SetVatTotal = fltVATTotal;
                        generateInvoice.SetInvoiceTotal = fltInvTotal;

                        generateInvoice.GenerateBakeryInvoice();
                    }
                }
                generateInvoice.SetstrReseller = "N"; 
            }
            catch (Exception ex)
            {
                message = ex.Message + "Stack " + ex.StackTrace + "dt" + vdtDate.ToString();
            }
        }

        public void CreditReseller(DateTime vdtDate)
        {
            try
            {
                //get all credit statements 
                ProcessReportAllInvoices a = new ProcessReportAllInvoices();

                //bcause of dateformat i -1 from AddDays method actually here its substract date
                //DataTable dtCreditReseller= a.DailyInvoiceResellerList(vdtDate.AddD(-1));
               
               DataTable dtCreditReseller = a.DailyCreditResellerList(vdtDate.AddDays(-1));

               generateInvoice.SetPricelessInv = "N";
               generateInvoice.SetstrReseller = "N"; 

                //check availability of credit statements 
                if (dtCreditReseller.Rows.Count > 0)
                {
                    int intRowCount = 0;
                    intRowCount = dtCreditReseller.Rows.Count;
                    //Loop through the credit statements
                    for (int i = 0; i <= intRowCount - 1; i++)
                    {
                        generateInvoice.SetResellerCreditNotePrinted = "N";
                        if (i == 0)
                            generateInvoice.SetResellerCreditNotePrinted = "Y";

                        // item Details
                        DataTable dtCreditResellerDetails = a.GetResellerCreditNoteDetails(long.Parse(dtCreditReseller.Rows[i]["ResellerID"].ToString()), vdtDate.AddDays(-1));

                        //credit Header details
                        generateInvoice.SetInvoiceTable = dtCreditResellerDetails;
                        generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("BFImage").ToString();
                        generateInvoice.SetInvoiceNo = "Reseller Credit \n NUMBER:" + dtCreditReseller.Rows[i]["ResellerCreditID"].ToString().Replace(@"\n",Environment.NewLine);
                        //generateInvoice.SetCustomerInfo = "Customer Info";
                        //generateInvoice.SetInvoiceInfo = "Invoice Info";

                        string strResellerAddress = string.Empty;
                        string invNos = "Invoice No(s): " + a.GetResellerCreditNoteInvoiceNos(dtCreditReseller.Rows[i]["ResellerCreditID"].ToString());

                        strResellerAddress = invNos + "\n" + dtCreditReseller.Rows[0][0].ToString() + "\n" + dtCreditReseller.Rows[0][1].ToString() + "\n" + dtCreditReseller.Rows[0][2].ToString() + "\n" + dtCreditReseller.Rows[0][3].ToString() + "\n" + dtCreditReseller.Rows[0][4].ToString() + "\n" + dtCreditReseller.Rows[0][5].ToString();
                        //IN CREDIT NOTE CUSTOMER ADDRESS IS THE RESELLER ADDRESS
                        generateInvoice.SetCustomerInfo = strResellerAddress.Replace(@"\n",Environment.NewLine);
                        //ALWAYS TOP ADDRESS IS BREAD FACTORY
                        generateInvoice.SetAddress = new Support().GetValuesFromConfig("BF_ContactDetails").ToString().Replace(@"\n", Environment.NewLine);

                        generateInvoice.SetInvoiceInfo = "Date :" + DateTime.Parse(dtCreditReseller.Rows[i][6].ToString()).ToString("dd/MM/yyyy"); ;
                        generateInvoice.SetInvoiceVerb = new Support().GetValuesFromConfig("BFInvoiceVerbCreditStatement").ToString().Replace(@"\n", Environment.NewLine);

                        DataTable dtReseller = a.GetResellerCreditNoteDetails(long.Parse(dtCreditReseller.Rows[i]["resellerID"].ToString()), vdtDate.AddDays(-1));
                        generateInvoice.SetVATNO = new Support().GetValuesFromConfig("ResellerVATNO").ToString();
                        generateInvoice.SetinvFooter = new Support().GetValuesFromConfig("BFInvoiceFooter").ToString().Replace(@"\n", Environment.NewLine);
                        generateInvoice.SetinvFooter1 = new Support().GetValuesFromConfig("BFInvoiceFooter1").ToString().Replace(@"\n", Environment.NewLine);
                       // generateInvoice.SetinvFooter1 = "creditt reseller";//new Support().GetValuesFromConfig("BFInvoiceFooter2").ToString().Replace(@"\n", Environment.NewLine);

                        float fltGoodsTotal = 0;
                        float fltVATTotal = 0;
                        float fltInvTotal = 0;
                        //Get Reseller Totals from detailtable
                        for (int x = 0; x <= dtCreditResellerDetails.Rows.Count - 1; x++)
                        {
                            fltGoodsTotal = fltGoodsTotal + float.Parse(dtCreditResellerDetails.Rows[x][3].ToString());
                            fltVATTotal = fltVATTotal + float.Parse(dtCreditResellerDetails.Rows[x][5].ToString());
                            fltInvTotal = fltInvTotal + float.Parse(dtCreditResellerDetails.Rows[x][4].ToString()); ;
                        }

                        generateInvoice.SetGoodsTotal = fltGoodsTotal;
                        generateInvoice.SetVatTotal = fltVATTotal;
                        generateInvoice.SetInvoiceTotal = fltGoodsTotal + fltVATTotal;

                        generateInvoice.GenerateBakeryInvoice();
                        generateInvoice.SetResellerCreditNotePrinted = "N";
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message + "Stack " + ex.StackTrace + "dt" + vdtDate.ToString();
            }
        }


        #region hidden
       /* public void CreditReseller(DateTime vdtDate)
        {
            try
            {
                //get all credit statements 
                ProcessReportAllInvoices a = new ProcessReportAllInvoices();

                //bcause of dateformat i -1 from AddDays method actually here its substract date
                //DataTable dtCreditReseller= a.DailyInvoiceResellerList(vdtDate.AddD(-1));
                DataTable dtCreditReseller = a.DailyCreditResellerList(vdtDate.AddDays(-1));

                generateInvoice.SetPricelessInv = "N";

                //check availability of credit statements 
                if (dtCreditReseller.Rows.Count > 0)
                {
                    int intRowCount = 0;
                    intRowCount = dtCreditReseller.Rows.Count;
                    //Loop through the credit statements
                    for (int i = 0; i <= intRowCount - 1; i++)
                    {
                        generateInvoice.SetResellerCreditNotePrinted = "N";
                        if (i == 0)
                            generateInvoice.SetResellerCreditNotePrinted = "Y";

                        // item Details
                        DataTable dtCreditResellerDetails = a.GetResellerInvoiceDetails(long.Parse(dtCreditReseller.Rows[i]["ResellerID"].ToString()), vdtDate.AddDays(-1), "", long.Parse(dtCreditReseller.Rows[i]["ResellerCreditID"].ToString()));

                        //credit Header details
                        generateInvoice.SetInvoiceTable = dtCreditResellerDetails;
                        generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("BFImage").ToString();
                        generateInvoice.SetInvoiceNo = "Reseller Credit Number:" + dtCreditReseller.Rows[i]["ResellerCreditID"].ToString();
                        //generateInvoice.SetCustomerInfo = "Customer Info";
                        //generateInvoice.SetInvoiceInfo = "Invoice Info";

                        string strResellerAddress = string.Empty;
                        strResellerAddress = dtCreditReseller.Rows[0][3].ToString() + dtCreditReseller.Rows[0][4].ToString() + dtCreditReseller.Rows[0][5].ToString();
                        //IN CREDIT NOTE CUSTOMER ADDRESS IS THE RESELLER ADDRESS
                        generateInvoice.SetCustomerInfo = strResellerAddress;
                        //ALWAYS TOP ADDRESS IS BREAD FACTORY
                        generateInvoice.SetAddress = new Support().GetValuesFromConfig("BF").ToString().Replace(@"\n", Environment.NewLine);

                        DataTable dtReseller = a.GetResellerCreditNoteDetails(long.Parse(dtCreditReseller.Rows[i]["resellerID"].ToString()), vdtDate.AddDays(-1));
                        generateInvoice.SetVATNO = new Support().GetValuesFromConfig("ResellerVATNO").ToString();
                        generateInvoice.SetinvFooter = new Support().GetValuesFromConfig("CreditNote").ToString();

                        float fltGoodsTotal = 0;
                        float fltVATTotal = 0;
                        float fltInvTotal = 0;
                        //Get Reseller Totals from detailtable
                        for (int x = 0; x <= dtCreditResellerDetails.Rows.Count - 1; x++)
                        {
                            fltGoodsTotal = fltGoodsTotal + float.Parse(dtCreditResellerDetails.Rows[x][3].ToString());
                            fltVATTotal = fltVATTotal + float.Parse(dtCreditResellerDetails.Rows[x][5].ToString());
                            fltInvTotal = fltInvTotal + float.Parse(dtCreditResellerDetails.Rows[x][4].ToString()); ;
                        }

                        generateInvoice.SetGoodsTotal = fltGoodsTotal;
                        generateInvoice.SetVatTotal = fltVATTotal;
                        generateInvoice.SetInvoiceTotal = fltInvTotal;

                        generateInvoice.GenerateBakeryInvoice();
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message + "Stack " + ex.StackTrace + "dt" + vdtDate.ToString();
            }
        }
        * */

        public string GetFooterText()
        {
            string footer = string.Empty;
            string newFooter = string.Empty;
            ProcessReportAllInvoices a = new ProcessReportAllInvoices();
            footer = a.GetFooterNote();
            newFooter = footer.Replace("<BR>", "\n");
            newFooter = newFooter.Replace("<P>", "");
            newFooter = newFooter.Replace("</P>", "");
            newFooter = newFooter.Replace("^^^", "'");
            return newFooter;
        }

        #endregion
        
    }

