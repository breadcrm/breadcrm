using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;

public class Support
    {

        public Support() { }

        public string GetValuesFromConfig(string key)
        {
            return ConfigurationManager.AppSettings[key].ToString();
        }

        public string DailyPdfFolder
        {
            get
            {
                return @"DailyPdfFiles/";  
            }
        }        

        public string GetFoldername
        {
            get
            {
                return @"TEMPPDFFILES/";  //System.Configuration.ConfigurationManager.AppSettings["FolderPath"].ToString();
            }
        }

        public string GetTempFilename
        {
            get
            {
                return "TempPDFRpt_"; //System.Configuration.ConfigurationManager.AppSettings["TempFileName"].ToString();
            }
        }

        public string GetNewFileName()
        {

            DirectoryInfo directoryInfo = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath( GetFoldername) );
            FileInfo[] fileInfo = directoryInfo.GetFiles();
            int[] arrFileNames = new int[fileInfo.Length];
            int fvalue = 0;

            for (int i = 0; i < fileInfo.Length; i++)
            {
                string fileName = fileInfo[i].Name;
                int index = fileName.IndexOf("_");
                int lengthIndex = fileName.IndexOf(".");
                int.TryParse(fileName.Substring(index + 1, lengthIndex - 1 - index), out fvalue);

                arrFileNames[i] = fvalue;
            }

            sortArray(arrFileNames, arrFileNames.Length);
            fvalue = 0;
            if (arrFileNames.Length > 0)
                int.TryParse(arrFileNames[arrFileNames.Length - 1].ToString(), out fvalue);

            fvalue = fvalue + 1;

            System.Web.HttpContext.Current.Session["FileName"] = GetFoldername + GetTempFilename + fvalue.ToString() + ".pdf";

            return GetFoldername + GetTempFilename + fvalue.ToString() + ".pdf";

        }

        public string GetNewFileName(string docType,int docNo)
        {
            Guid guid = Guid.NewGuid();

            string fname = GetFoldername + guid.ToString() + ".pdf";
            return fname;
        }

        // Bubble Sort Algorithm
        public void sortArray(int[] a, int length)
        {
            try
            {
                int x = length;
                int i;
                int j;
                int temp;

                for (i = (x - 1); i >= 0; i--)
                {
                    for (j = 1; j <= i; j++)
                    {
                        if (a[j - 1] > a[j])
                        {
                            temp = a[j - 1];
                            a[j - 1] = a[j];
                            a[j] = temp;
                        }
                    }
                }
            }
            catch
            {
            }
        }

    }
