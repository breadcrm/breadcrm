using System;
using System.Collections.Generic;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Web;

public class GenerateInvoice
{

    #region Variables

    string fontname = "Monotype Corsiva";
    float fsize = 10;
    float tblwidth = 90;
    float cellHeight = 50;
    Document pdfdocument;
    iTextSharp.text.pdf.PdfWriter writer;
    iTextSharp.text.Color brobercolor = new iTextSharp.text.Color(110, 136, 194);
    string invoiceNo = string.Empty;
    string customerInfo = string.Empty;
    string invoiceInfo = string.Empty;
    System.Data.DataTable dtInvoice;
    int pageBreak = 30;

    private float goodsTotal = 0;
    float VatTotal = 0;
    float InvoiceTotal = 0;

    private string purchaseOrdNo;
    private string deliveryVanNo;
    private string ordDate;
    private string strAddress;
    private string VATNO;
    private string invFooter;
    private string invFooter1;
    private string strLogoPath;
    private string strInvoiceVerb;
    private string strPricelessInv;
    private int intPriceLessPrinted;
    private string strResellerCreditNotePrinted;
    private string strReseller;
    private string strDeliveryNote;
    private string strCreditStatement;
  


    #endregion


    #region Methods

    public GenerateInvoice()
    {
    }

    public void ClosePDF()
    {
        if(HttpContext.Current.Session["IsCreated"]=="Created")
            pdfdocument.Close();
    }

    public void GenerateBakeryInvoice()
    {
        string fileName=string.Empty ;

        if (HttpContext.Current.Session["IsCreated"] != "Created")
        {
            pdfdocument = new Document();

            fileName = new Support().GetNewFileName();
            writer = iTextSharp.text.pdf.PdfWriter.GetInstance(pdfdocument, File.Open(System.Web.HttpContext.Current.Server.MapPath(fileName), FileMode.Create));
            pdfdocument.SetMargins(20, 20, 40, 20);
            pdfdocument.Open();
        }

        HttpContext.Current.Session["IsCreated"] = "Created";

        CreateInvoice();            
    }

    public string GenerateBakeryDoc(int docNo, string fileName)
    {
        pdfdocument = new Document();
        
        if (string.IsNullOrEmpty(fileName))
        {
            fileName = new Support().GetNewFileName("", docNo);
        }

        FileStream fstrm = File.Open(System.Web.HttpContext.Current.Server.MapPath(fileName), FileMode.Create);
        writer = iTextSharp.text.pdf.PdfWriter.GetInstance(pdfdocument, fstrm);
        
        pdfdocument.SetMargins(20, 20, 40, 20);

        pdfdocument.Open();

        CreateInvoice();
        
        pdfdocument.Close();

        return fileName;

    }

    private void CreateInvoice()
    {
        try
        {
            int loopCount = 0;
			pageBreak=30;
            pdfdocument.NewPage();
            bool isTableAdded = false;
            iTextSharp.text.pdf.PdfPTable pdfptable;

            pdfptable = new iTextSharp.text.pdf.PdfPTable(16);
            pdfptable.DefaultCell.Padding = 2;
            //pdfptable.DefaultCell.Height = 30;
            pdfptable.WidthPercentage = tblwidth;
            pdfptable.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_CENTER;

            /*
             * Invoice detail will come here
             */
            AddInvoiceHeader(pdfptable);
            AddInvoiceTableHeader(pdfptable);

            for (int i = 0; i < dtInvoice.Rows.Count; i++)
            {
                if (loopCount < pageBreak)
                {
                    AddInvoiceDetail(pdfptable, i);
                }
                else
                {
                    if (loopCount == pageBreak)
                        pageBreak = 42;  
                    
					pdfdocument.Add(pdfptable);
                    isTableAdded = true;
                    pdfdocument.NewPage();
                    pdfptable = new iTextSharp.text.pdf.PdfPTable(16);
                    pdfptable.DefaultCell.Padding = 1;
                    pdfptable.WidthPercentage = tblwidth;
                    pdfptable.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_CENTER;

                    AddInvoiceTableHeader(pdfptable);
                    AddInvoiceDetail(pdfptable, i);

                    loopCount = 0;
                    isTableAdded = false;
                }

                loopCount += 1;
            }
            AddInvoiceTotals(pdfptable);
            //if (strCreditStatement!="Y")
                AddInvoiceFooter(pdfptable);

            if (!isTableAdded)
                pdfdocument.Add(pdfptable);
        }
        catch (Exception ex)
        {
            //message = ex.Message + "Stack " + ex.StackTrace + "dt" + vdtDate.ToString();
        }
    }

    private void AddInvoiceHeader(PdfPTable pdfptable)
    {
        //creditNote Header
        //if (strResellerCreditNotePrinted == "Y" || SetstrReseller=="Y")
        if ( SetstrReseller == "Y")
        {
            fsize = 13;
            PdfPCell cell1 = GetCell("Reseller Invoices & Credit Notes", Color.RED, 1);
            cell1.Phrase.Font.Color = Color.RED;
            cell1.HorizontalAlignment=Rectangle.ALIGN_LEFT;
            cell1.Border=Rectangle.NO_BORDER;          
            cell1.Colspan=16;
            //cell1.Height = cellHeight;
            pdfptable.AddCell(cell1);
            
            pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 16));
        }
        
        //vatno
        fsize = 8;
        pdfptable.AddCell(SetHeader(Rectangle.NO_BORDER, "VAT Reg No. " + VATNO, Rectangle.ALIGN_LEFT, fsize, 4));
        pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 12));

        //Company Address or reseller address
        pdfptable.AddCell(SetHeader(Rectangle.NO_BORDER, strAddress, Rectangle.ALIGN_LEFT, fsize, 4));
        pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 2));            

        //Logo       
        PdfPCell cell = new PdfPCell(new Phrase(GetPdfChunk(SetLogoPath, Rectangle.ALIGN_CENTER)));
        cell.Border = Rectangle.NO_BORDER;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        pdfptable.AddCell(cell );
       
        //invoice verb
        pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 4));
        pdfptable.AddCell(SetHeader(Rectangle.NO_BORDER, strInvoiceVerb, Rectangle.ALIGN_CENTER, fsize, 6));
        
        //Invoice No
        fsize = 12;
        pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 16));
        cell = GetCell(invoiceNo, Color.BLACK, 1);
        cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
        cell.Colspan = 16;
        cell.Border = Rectangle.NO_BORDER;
        pdfptable.AddCell(cell);
        //pdfptable.AddCell(SetHeader(Rectangle.NO_BORDER, invoiceNo, Rectangle.ALIGN_CENTER, fsize, 16));
        pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 16));
	

        //Customer Address
        int intFontStyle = 1;
        if (strCreditStatement == "Y")
            intFontStyle = 0;


		fsize = 10;
        cell = GetCell(customerInfo, Color.BLACK, intFontStyle);
        cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
        cell.Colspan = 10;
        cell.Border = Rectangle.NO_BORDER;
        pdfptable.AddCell(cell);
        
        //Invoice date
        cell = GetCell(invoiceInfo, Color.BLACK, 1);
        cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
        cell.Colspan =10;
        cell.Border = Rectangle.NO_BORDER;
        pdfptable.AddCell(cell);

        //pdfptable.AddCell(SetHeader(Rectangle.NO_BORDER, invoiceInfo, Rectangle.ALIGN_LEFT, fsize, 8));

        pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 16));
        fsize = 10;
    }

    private void AddInvoiceTableHeader(PdfPTable pdfptable)
    {
        PdfPCell cell;
        Color headColor = new Color(204, 204, 204);

        cell = GetCell(new Support().GetValuesFromConfig("Column1").ToString(), Color.BLACK, 1);
        cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
        cell.Colspan = 1;
        cell.BackgroundColor = headColor;
        pdfptable.AddCell(cell);

        cell = GetCell(new Support().GetValuesFromConfig("Column2").ToString(), Color.BLACK, 1);
        cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
        cell.Colspan = 2;
        cell.BackgroundColor = headColor;
        pdfptable.AddCell(cell);


        if (strPricelessInv == "Y" && intPriceLessPrinted == 2 || strDeliveryNote=="Y")
        {
            cell = GetCell(new Support().GetValuesFromConfig("Column3").ToString(), Color.BLACK, 1);
            cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            cell.Colspan = 13;
            cell.BackgroundColor = headColor;
            pdfptable.AddCell(cell);
        }

        if ((strPricelessInv == "N" || intPriceLessPrinted == 1 || intPriceLessPrinted == 0) && strDeliveryNote == "N")
        {
            cell = GetCell(new Support().GetValuesFromConfig("Column3").ToString(), Color.BLACK, 1);
            cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            cell.Colspan = 9;
            cell.BackgroundColor = headColor;
            pdfptable.AddCell(cell);

            cell = GetCell(new Support().GetValuesFromConfig("Column4").ToString(), Color.BLACK, 1);
            cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            cell.Colspan = 2;
            cell.BackgroundColor = headColor;
            pdfptable.AddCell(cell);

            cell = GetCell(new Support().GetValuesFromConfig("Column5").ToString(), Color.BLACK, 1);
            cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            cell.Colspan = 2;
            cell.BackgroundColor = headColor;
            pdfptable.AddCell(cell);
        }
        //else if (strPricelessInv == "Y" && intPriceLessPrinted==2)
            //pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 4));
       
    }

    private void AddInvoiceDetail(PdfPTable pdfptable,int i)
    {
        PdfPCell cell;

        if (strReseller != "Y" && strResellerCreditNotePrinted != "Y")
        {
            cell = GetCell(dtInvoice.Rows[i][1].ToString(), Color.BLACK, 0);
            cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            cell.Colspan = 1;
            pdfptable.AddCell(cell);

            cell = GetCell(dtInvoice.Rows[i][2].ToString(), Color.BLACK, 0);
            cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            cell.Colspan = 2;
            pdfptable.AddCell(cell);

            if (strPricelessInv == "Y" && intPriceLessPrinted == 2 || strDeliveryNote == "Y")
            {
                cell = GetCell(dtInvoice.Rows[i][3].ToString(), Color.BLACK, 0);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.Colspan = 13;
                pdfptable.AddCell(cell);
                
            }


            if ((strPricelessInv == "N" || intPriceLessPrinted == 1 || intPriceLessPrinted == 0)  && strDeliveryNote == "N")
            {
                float fltConverter = 0;

                cell = GetCell(dtInvoice.Rows[i][3].ToString(), Color.BLACK, 0);
                cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                cell.Colspan = 9;
                pdfptable.AddCell(cell);

                float.TryParse(dtInvoice.Rows[i][4].ToString(), out fltConverter);
                cell = GetCell(fltConverter.ToString("n2"), Color.BLACK, 0);
                cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
                cell.Colspan = 2;
                pdfptable.AddCell(cell);
                fltConverter = 0;

                //total price
                float.TryParse(dtInvoice.Rows[i][5].ToString(), out fltConverter);
                cell = GetCell(fltConverter.ToString("n2"), Color.BLACK, 0);
                cell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
                cell.Colspan = 2;
                pdfptable.AddCell(cell);
            }
            //else if (strPricelessInv == "Y" && intPriceLessPrinted == 2)
           // {
              //  pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 4));
           // }
        }
        else if (strReseller == "Y" || strResellerCreditNotePrinted == "Y")
        {
            cell = GetCell(dtInvoice.Rows[i][2].ToString(), Color.BLACK, 0);
            cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            cell.Colspan = 1;
            pdfptable.AddCell(cell);

            cell = GetCell(dtInvoice.Rows[i][0].ToString(), Color.BLACK, 0);
            cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            cell.Colspan = 2;
            pdfptable.AddCell(cell);

            cell = GetCell(dtInvoice.Rows[i][1].ToString(), Color.BLACK, 0);
            cell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            cell.Colspan = 9;
            pdfptable.AddCell(cell);

            if (strPricelessInv == "N" || intPriceLessPrinted == 1 || intPriceLessPrinted == 0)
            {
                float fltConverter = 0;
                float.TryParse(dtInvoice.Rows[i][3].ToString(), out fltConverter);
                cell = GetCell(fltConverter.ToString("n2"), Color.BLACK, 0);
                cell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
                cell.Colspan = 2;
                pdfptable.AddCell(cell);
                fltConverter = 0;

                float.TryParse(dtInvoice.Rows[i][4].ToString(), out fltConverter);
                cell = GetCell(fltConverter.ToString("n2"), Color.BLACK, 0);
                cell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
                cell.Colspan = 2;
                pdfptable.AddCell(cell);
            }
            else if (strPricelessInv == "Y" && intPriceLessPrinted == 2)
            {
                pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 4));
            }
        }
       // else if (strResellerCreditNotePrinted=="Y")
        //{
        
        //}



    }

    private void AddInvoiceTotals(PdfPTable pdfptable)
    {
        if ((strPricelessInv == "N" || intPriceLessPrinted == 1 || intPriceLessPrinted == 0 )&& strDeliveryNote != "Y")
        {
            pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 10));
            PdfPCell cellGoodsTotal = GetCell(new Support().GetValuesFromConfig("InvoiceGoodsTotal").ToString(), Color.BLACK, 1);
            cellGoodsTotal.Border = Rectangle.NO_BORDER;
            cellGoodsTotal.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            cellGoodsTotal.Colspan = 4;
            pdfptable.AddCell(cellGoodsTotal);


            PdfPCell cellGoodsTotalVal = GetCell(float.Parse(goodsTotal.ToString()).ToString("n2"), Color.BLACK, 1);
            cellGoodsTotalVal.Colspan = 4;
            cellGoodsTotalVal.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            pdfptable.AddCell(cellGoodsTotalVal);

            fsize = 6;
            pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 10));
            PdfPCell cellInvVAT = GetCell(new Support().GetValuesFromConfig("InvoiceVAT").ToString(), Color.BLACK, 1);
            cellInvVAT.Border = Rectangle.NO_BORDER;
            cellInvVAT.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            cellInvVAT.Colspan = 4;
            pdfptable.AddCell(cellInvVAT);

            
            PdfPCell cellVatTotalVal = GetCell(float.Parse(VatTotal.ToString()).ToString("n2"), Color.BLACK, 1);
            cellVatTotalVal.Colspan = 4;
            cellVatTotalVal.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            pdfptable.AddCell(cellVatTotalVal);

            fsize = 10;

            pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 10));
            PdfPCell cellInvTotal = GetCell(new Support().GetValuesFromConfig("InvoiceTotal").ToString(), Color.BLACK, 1);
            cellInvTotal.Border = Rectangle.NO_BORDER;
            cellInvTotal.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            cellInvTotal.Colspan = 4;
            pdfptable.AddCell(cellInvTotal);

            PdfPCell cellInvoiceTotalVal = GetCell(float.Parse(InvoiceTotal.ToString()).ToString("n2"), Color.BLACK, 1);
            cellInvoiceTotalVal.Colspan = 4;
            cellInvoiceTotalVal.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            pdfptable.AddCell(cellInvoiceTotalVal);

            //pdfptable.AddCell(SetHeader(Rectangle.RECTANGLE, float.Parse(InvoiceTotal.ToString()).ToString("n2"), Rectangle.ALIGN_CENTER, fsize, 2));

            pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 4));
        }
    }

    private void AddInvoiceFooter(PdfPTable pdfptable)
    {        
        
        pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 16));

        fsize = 9;
        PdfPCell cell = GetCell("IMPORTANT !!", Color.BLACK,1);
        cell.Border = Rectangle.NO_BORDER;
        cell.Colspan = 16;
        cell.Column.Alignment = Rectangle.ALIGN_LEFT;
        //pdfptable.AddCell(cell);
        pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 16));

        fsize = 8;
        pdfptable.AddCell(SetHeader(Rectangle.NO_BORDER, invFooter, Rectangle.ALIGN_LEFT, fsize, 16));               
        pdfptable.AddCell(GetEmptyCell(Rectangle.NO_BORDER, 16));
        pdfptable.AddCell(SetHeader(Rectangle.NO_BORDER, invFooter1, Rectangle.ALIGN_CENTER, fsize, 16));        
        fsize = 10;
    }

    
    private iTextSharp.text.pdf.PdfPCell GetEmptyCell(int border, int colspan)
    {
        iTextSharp.text.pdf.PdfPCell cell = GetCell("", 0);
        cell.Colspan = colspan;
        cell.Border = border;

        return cell;
    }

    private iTextSharp.text.pdf.PdfPCell SetHeader(int border,string content,int alignment,float fontsize,int colspan)
    {
        PdfPCell cell = GetCell(content, border);
        cell.HorizontalAlignment = alignment;
        cell.Border = border;           
        cell.Colspan = colspan;
        return cell;
    }
    
   
    private iTextSharp.text.pdf.PdfPCell GetCell(string value, int border)
    {
       	Font font = FontFactory.GetFont(fontname, fsize , Font.NORMAL);
        iTextSharp.text.pdf.PdfPCell cell = new iTextSharp.text.pdf.PdfPCell(new iTextSharp.text.Phrase(value, font));
        cell.Border = border;
        return cell;
    }

    private iTextSharp.text.pdf.PdfPCell GetCell(string value, iTextSharp.text.Color color, int fontstyle)
    {
        Font font = FontFactory.GetFont(fontname, fsize, fontstyle);
        iTextSharp.text.Phrase pphrase = new iTextSharp.text.Phrase(new Chunk(value, font));
        iTextSharp.text.pdf.PdfPCell cell = new iTextSharp.text.pdf.PdfPCell(pphrase);
        cell.BorderColor = color;
		//cell.FixedHeight = 17;
        return cell;
    }

   
    private Chunk GetPdfChunk(string text, string fontname, float fontsize)
    {
        Chunk pdfchunk;
        pdfchunk = new Chunk(text, FontFactory.GetFont(fontname, fontsize));
        return pdfchunk;
    }

    /// <summary>
    /// returns a pdf chunk that can be used to pass parameters to new phrases
    /// </summary>
    /// <param name="text">path to the image</param>
    /// <returns></returns>
    /// <remarks></remarks>
    private Chunk GetPdfChunk(string imgpath,int alignment)
    {
        iTextSharp.text.Image imgheader = iTextSharp.text.Image.GetInstance(imgpath); 
        imgheader.ScalePercent(50);
        imgheader.Alignment = alignment;
        Chunk pdfchunk;
        pdfchunk = new Chunk(imgheader, 0, 0);
        return pdfchunk;
    }

    #endregion

    #region Properties
    
    public string SetInvoiceVerb
    {
        get { return strInvoiceVerb; }
        set { strInvoiceVerb = value; }
    }

    public string SetLogoPath
    {
        get { return strLogoPath ; }
       // set { strLogoPath = HttpContext.Current.Server.MapPath( value); }
        set { strLogoPath = value; }
    }


    public string SetinvFooter
    {
        get { return invFooter; }
        set { invFooter = value; }
    }

    public string SetinvFooter1
    {
        get { return invFooter1; }
        set { invFooter1 = value; }
    }

    public string SetVATNO
    {
        get { return VATNO; }
        set { VATNO = value; }
    }

    public string SetAddress
    {
        get { return strAddress; }
        set { strAddress = value; }
    }

    public string SetdeliveryVanNo
    {
        get { return deliveryVanNo; }
        set { deliveryVanNo = value; }
    }



    public string SetpurchaseOrdNo
    {
        get { return purchaseOrdNo; }
        set { purchaseOrdNo = value; }
    }

    public string setOrdDate
    {
        get { return ordDate; }
        set { ordDate = value; }
    }

    public float SetGoodsTotal
    {
        get { return goodsTotal; }
        set { goodsTotal = value; }
    }

    public float SetVatTotal
    {
        get { return VatTotal; }
        set { VatTotal = value; }
    }

    public float SetInvoiceTotal
    {
        get { return InvoiceTotal; }
        set { InvoiceTotal = value; }
    }


    public string SetInvoiceNo
    {
        set
        {
            invoiceNo = value;
        }
    }

    public string SetCustomerInfo
    {
        set
        {
            customerInfo = value;
        }
    }

    public string SetInvoiceInfo
    {
        set
        {
            invoiceInfo = value;
        }
    }

    public System.Data.DataTable SetInvoiceTable
    {
        set
        {
            dtInvoice = value;
        }
    }


    public string SetPricelessInv
    {
        get { return strPricelessInv; }
        set { strPricelessInv = value; }
    }
    public int SetPriceLessPrinted
    {
        get { return intPriceLessPrinted; }
        set { intPriceLessPrinted = value; }
    }


    public string SetResellerCreditNotePrinted
    {
        get { return strResellerCreditNotePrinted; }
        set { strResellerCreditNotePrinted = value; }
    }


    public string SetstrReseller
    {
        get { return strReseller; }
        set { strReseller = value; }
    }


    public string SetDeliveryNote
    {
        get { return strDeliveryNote; }
        set { strDeliveryNote = value; }
    }

     public string  SetCreditStatement
    {
        get { return strCreditStatement; }
        set { strCreditStatement = value; }
    }


    #endregion

}
