using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for PrintInvoice
/// </summary>
public class PrintPDFInvoice
{
    public static string GenerateDoc(int docNo,string fileName,bool printUnDistributableDoc, string sFooter)
    {
        ProcessReportAllInvoices a = new ProcessReportAllInvoices();
        GeneratePdfDoc generateInvoice = new GeneratePdfDoc();
        string generatedFileName = "";
       // string sFooterText = GetFooterText();

        int intPriceLessInvoice = 1;

        DataTable dsInvoiceTotal = a.GetInvoiceHeader(docNo);
        
        //get no of coppies to be prited of current job
        //int intNOI2Bprinted = int.Parse(dsInvoiceTotal.Rows[0]["NoofInvoices"].ToString());//No Of Invoices to b printed
        int intDeliveryNote = 0;

        generateInvoice.SetDeliveryNote = "N";

        //if (intDeliveryNote == 1)
        //{
        //    generateInvoice.SetDeliveryNote = "Y";
        //}

        //for (int x = 1; x <= intNOI2Bprinted; x++)
        //{
            intPriceLessInvoice = 1;
            if (dsInvoiceTotal.Rows[0][16].ToString() == "1")
            {
                generateInvoice.SetPricelessInv = "Y";
                generateInvoice.SetDeliveryNote = "Y";
                //if (generateInvoice.SetDeliveryNote == "Y")
                //    intPriceLessInvoice = 1;
                if (printUnDistributableDoc)
                {
                    generateInvoice.SetDeliveryNote = "N";
                    intPriceLessInvoice = 2;
                    generateInvoice.PrintNotDistributableLogo = true;
                }
            }
            else
            {
                generateInvoice.SetPricelessInv = "Y";
            }

            for (int z = 1; z <= intPriceLessInvoice; z++)
            {
                if (z == 2)
                {
                    generateInvoice.PrintNotDistributableLogo = false;
                    generateInvoice.SetPriceLessPrinted = 2;
                    generateInvoice.SetDeliveryNote = "Y";
                    generateInvoice.SetPricelessInv = "N";
                    intDeliveryNote = 1;
                }

                //Invoice item Details
                DataTable dtInvoiceDetails = a.GetInvoiceDetails(long.Parse(dsInvoiceTotal.Rows[0]["ordNo"].ToString()));
                generateInvoice.SetInvoiceTable = dtInvoiceDetails;
                //Invoice Header details
                if (intDeliveryNote != 1)
                {
                    generateInvoice.DocType = "INVOICE";
                    //generateInvoice.SetInvoiceNo = "NUMBER: " + dsInvoiceTotal.Rows[0]["ordNo"].ToString().Replace(@"\n", Environment.NewLine);
                    generateInvoice.SetInvoiceNo = "INVOICE #: " + dsInvoiceTotal.Rows[0]["ordNo"].ToString().Replace(@"\n", Environment.NewLine);
                }
                else if (intDeliveryNote == 1)
                {
                    generateInvoice.DocType = "Delivery Note for";
                    generateInvoice.SetInvoiceNo = "INVOICE #: " + dsInvoiceTotal.Rows[0]["ordNo"].ToString().Replace(@"\n", Environment.NewLine);

                
                }

                StringBuilder strbAddress = new StringBuilder();

                if (dsInvoiceTotal.Rows[0][14].ToString() != "" || dsInvoiceTotal.Rows[0][14].ToString() != string.Empty)
                    strbAddress.Append(dsInvoiceTotal.Rows[0][14].ToString() + "\n");
                if (dsInvoiceTotal.Rows[0][2].ToString() != "" || dsInvoiceTotal.Rows[0][2].ToString() != string.Empty)
                    strbAddress.Append(dsInvoiceTotal.Rows[0][2].ToString() + "\n");
                if (dsInvoiceTotal.Rows[0][5].ToString() != "" || dsInvoiceTotal.Rows[0][5].ToString() != string.Empty)
                    strbAddress.Append(dsInvoiceTotal.Rows[0][5].ToString() + "\n");
                if (dsInvoiceTotal.Rows[0][6].ToString() != "" || dsInvoiceTotal.Rows[0][6].ToString() != string.Empty)
                    strbAddress.Append(dsInvoiceTotal.Rows[0][6].ToString() + "\n");
                if (dsInvoiceTotal.Rows[0][7].ToString() != "" || dsInvoiceTotal.Rows[0][7].ToString() != string.Empty)
                    strbAddress.Append(dsInvoiceTotal.Rows[0][7].ToString() + "\n");
                if (dsInvoiceTotal.Rows[0][8].ToString() != "" || dsInvoiceTotal.Rows[0][8].ToString() != string.Empty)
                    strbAddress.Append(dsInvoiceTotal.Rows[0][8].ToString() + "\n");


                string strCustomerAddress = strbAddress.ToString();
                generateInvoice.SetCustomerInfo = strCustomerAddress.Replace(@"\n", Environment.NewLine);
                string strInvoiceInfo = "Purchase Order No: " + dsInvoiceTotal.Rows[0]["pono"].ToString() + "\n\n" + "van #: " + dsInvoiceTotal.Rows[0]["Vehcle"].ToString() + "\n\n" + "Date: " + DateTime.Parse(dsInvoiceTotal.Rows[0]["OrdDate"].ToString()).ToString("dd/MM/yyyy");
                generateInvoice.SetInvoiceInfo = strInvoiceInfo.Replace(@"\n", Environment.NewLine);

                //Check whether reseller id is available if so show reseller details else FACTORY DETAILS
                if (dsInvoiceTotal.Rows[0]["resellerID"].ToString() == "0" || dsInvoiceTotal.Rows[0]["resellerID"].ToString() == "")
                {
                    if (dsInvoiceTotal.Rows[0]["Custof"].ToString() == "Bread Factory")
                    {
                        if (!string.IsNullOrEmpty(dsInvoiceTotal.Rows[0][22].ToString()))
                            generateInvoice.SetAddress = new Support().GetValuesFromConfig("BF").ToString().Replace(@"\n", Environment.NewLine) + "Telephone : " + (dsInvoiceTotal.Rows[0][23].ToString() != string.Empty ? dsInvoiceTotal.Rows[0][23].ToString() : "_");
                        else
                            generateInvoice.SetAddress = new Support().GetValuesFromConfig("BF_ContactDetails").ToString().Replace(@"\n", Environment.NewLine);
                        
                        generateInvoice.SetVATNO = new Support().GetValuesFromConfig("VATNOBF").ToString() + "\n\n";
                        generateInvoice.SetInvoiceVerb = new Support().GetValuesFromConfig("BFInvoiceVerb").ToString().Replace(@"\n", Environment.NewLine);
                        //generateInvoice.SetinvFooter = new Support().GetValuesFromConfig("BFInvoiceFooter").ToString().Replace(@"\n", Environment.NewLine);
                        generateInvoice.SetinvFooter = sFooter;
                        generateInvoice.SetinvFooter1 = new Support().GetValuesFromConfig("BFInvoiceFooter1").ToString().Replace(@"\n", Environment.NewLine);
                        
                        //if (z != 2 && generateInvoice.SetPricelessInv == "Y" && generateInvoice.SetDeliveryNote != "Y")
                        //    generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("PriceLessImage").ToString();
                        //else

                        if (!string.IsNullOrEmpty(dsInvoiceTotal.Rows[0][22].ToString()))
                            generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GroupLogoLocation").ToString() + dsInvoiceTotal.Rows[0][22].ToString();
                        else
                            generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("BFImage").ToString();

                    }
                    else if (dsInvoiceTotal.Rows[0]["Custof"].ToString() == "Gail Force")
                    {
                        if (!string.IsNullOrEmpty(dsInvoiceTotal.Rows[0][22].ToString()))
                            generateInvoice.SetAddress = new Support().GetValuesFromConfig("GF").ToString().Replace(@"\n", Environment.NewLine) + "Telephone : " + (dsInvoiceTotal.Rows[0][23].ToString() != string.Empty ? dsInvoiceTotal.Rows[0][23].ToString() : "_");
                        else
                            generateInvoice.SetAddress = new Support().GetValuesFromConfig("GF_ContactDetails").ToString().Replace(@"\n", Environment.NewLine);

                        generateInvoice.SetVATNO = new Support().GetValuesFromConfig("VATNOGF").ToString() + "\n\n";
                        generateInvoice.SetInvoiceVerb = new Support().GetValuesFromConfig("GFInvoiceVerb").ToString().Replace(@"\n", Environment.NewLine);
                      //  generateInvoice.SetinvFooter = new Support().GetValuesFromConfig("BFInvoiceFooter").ToString().Replace(@"\n", Environment.NewLine);
                        generateInvoice.SetinvFooter = sFooter;
                        generateInvoice.SetinvFooter1 = new Support().GetValuesFromConfig("BFInvoiceFooter1").ToString().Replace(@"\n", Environment.NewLine);

                        //if (z != 2 && generateInvoice.SetPricelessInv == "Y" && generateInvoice.SetDeliveryNote == "N")
                        //    generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("PriceLessImage").ToString();
                        //else
                        if(!string.IsNullOrEmpty(dsInvoiceTotal.Rows[0][22].ToString()))
                            generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GroupLogoLocation").ToString() + dsInvoiceTotal.Rows[0][22].ToString();
                        else
                            generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GFImage").ToString();
                    }
                }
                else if (dsInvoiceTotal.Rows[0]["resellerID"].ToString() == "1")
                {
                    //has reseller id
                    string strResellerAddress = string.Empty;
                    DataTable dtReseller = a.DisplayResellerDetail(long.Parse(dsInvoiceTotal.Rows[0]["resellerID"].ToString()));

                    if (!string.IsNullOrEmpty(dsInvoiceTotal.Rows[0][22].ToString()))
                        strResellerAddress = "\n" + dtReseller.Rows[0][1].ToString() + "\n" + dtReseller.Rows[0][2].ToString() + "\n" + dtReseller.Rows[0][3].ToString() + "\n" + dtReseller.Rows[0][4].ToString() + "\n\n" + "Telephone: " + (dsInvoiceTotal.Rows[0][23].ToString() != string.Empty ? dsInvoiceTotal.Rows[0][23].ToString() : "_");
                    else
                        strResellerAddress = "\n" + dtReseller.Rows[0][1].ToString() + "\n" + dtReseller.Rows[0][2].ToString() + "\n" + dtReseller.Rows[0][3].ToString() + "\n" + dtReseller.Rows[0][4].ToString() + "\n\n" + "Telephone: " + dtReseller.Rows[0][5].ToString() + "\n" + "Fax: " + dtReseller.Rows[0][6].ToString();
                   
                    generateInvoice.SetAddress = strResellerAddress;
                    generateInvoice.SetVATNO = dtReseller.Rows[0]["VATRegNo"].ToString() + "\n";
                   // generateInvoice.SetinvFooter = new Support().GetValuesFromConfig("BFInvoiceFooter").ToString().Replace(@"\n", Environment.NewLine);
                    generateInvoice.SetinvFooter = sFooter;
                    generateInvoice.SetinvFooter1 = new Support().GetValuesFromConfig("BFInvoiceFooter2").ToString().Replace(@"\n", Environment.NewLine);

                    //get the reseller footer details from database
                    //generateInvoice.SetinvFooter = dtReseller.Rows[0]["InvoiceFooter"].ToString();

                    if (!string.IsNullOrEmpty(dsInvoiceTotal.Rows[0][22].ToString()))
                        generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("GroupLogoLocation").ToString() + dsInvoiceTotal.Rows[0][22].ToString();
                    else
                       generateInvoice.SetLogoPath = new Support().GetValuesFromConfig("ResellerImagePath").ToString() + dtReseller.Rows[0]["Logo"].ToString();

                    
                }

                //Invoice totals
                generateInvoice.SetGoodsTotal = float.Parse(dsInvoiceTotal.Rows[0][11].ToString());
                generateInvoice.SetVatTotal = float.Parse(dsInvoiceTotal.Rows[0][12].ToString());
                generateInvoice.SetInvoiceTotal = float.Parse(dsInvoiceTotal.Rows[0][13].ToString());

                string tmpFName = generateInvoice.GenerateBakeryDoc((int)dsInvoiceTotal.Rows[0]["ordNo"], fileName, false);
                if (tmpFName != null)
                    generatedFileName = tmpFName;
                
                if (z == 2)
                {
                    generateInvoice.SetPriceLessPrinted = 1;
                }

            }
        //}

            generateInvoice.CloseBakeryDoc();

        generateInvoice.SetDeliveryNote = "N";

        return generatedFileName;
    }

    public static string GetFooterText()
    {
        string footer = string.Empty;
        string newFooter = string.Empty;
        ProcessReportAllInvoices a = new ProcessReportAllInvoices();
        footer = a.GetFooterNote();
        newFooter = footer.Replace("<BR>", "\n");
        newFooter = newFooter.Replace("<P>", "");
        newFooter = newFooter.Replace("</P>", "");
        newFooter = newFooter.Replace("^^^", "'");
        return newFooter;
    }
}
