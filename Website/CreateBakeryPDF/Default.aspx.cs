using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.Page 
{

    string retval = "Fail";
    string callBackURL = string.Empty;
  //  DateTime dtValue = new DateTime(2012,10,24);
    DateTime dtValue = new DateTime(2015, 10, 06);

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["IsCreated"] = "";
        Session["FileName"] = "";


        if (!IsPostBack)
        {
            if (Request.QueryString["dtvalue"] != null)
            {
                dtValue = DateTime.ParseExact(Request.QueryString["dtvalue"].ToString(), "d/M/yyyy", null);////"10/06/2008"
            }
            if (Request.QueryString["callbackurl"] != null)
                callBackURL = Request.QueryString["callbackurl"].ToString();

            retval = CreateReport();

            if (!string.IsNullOrEmpty(retval))
            {
                Response.Write("Date : " + Request.QueryString["dtvalue"]);
                Response.Write("<br />");
                Response.Write("File Name : " + Session["FileName"].ToString());
                Response.Write("<br />");
                Response.Write(retval);
                Response.Write("<br />");
                Response.Write(callBackURL);
                Response.Write("<br />");
                Response.Write(callBackURL + "?FilePath=CreateBakeryPDF/" + Session["FileName"].ToString() + "&Status=" + retval);
                Response.Write(dtValue.ToString());
            }
            else
            {
                if (Session["FileName"].ToString() != "")
                    Response.Redirect(callBackURL + "?FilePath=CreateBakeryPDF/" + Session["FileName"].ToString() + "&Status=done");
                else
                    Response.Redirect(callBackURL + "?Status=NoRows");
            }
        }
    }

    private string CreateReport()
    {        
        InvoiceLogic invoiceLogic = new InvoiceLogic();
        return invoiceLogic.GenInvoiceReports(dtValue);//
    }

}
