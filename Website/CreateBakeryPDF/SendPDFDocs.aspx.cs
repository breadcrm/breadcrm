using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SendPDFDocs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            ProcessReportAllInvoices ds = new ProcessReportAllInvoices();
            ds.ExecuteSQL("spEmailPDFFiles '" + Request.QueryString["deldate"].Replace("'","''") + "'");
            lblMsg.Text = "Mails successfully sent!";
            lblMsg.ForeColor = System.Drawing.Color.Black;
        }
        catch (Exception ex)
        { 
            lblMsg.Text = "Error sending Mails!" + "<br />" + ex.Message;
            lblMsg.ForeColor = System.Drawing.Color.Red;        
        }
    }
}
