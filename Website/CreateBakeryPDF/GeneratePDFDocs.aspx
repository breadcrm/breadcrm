<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GeneratePDFDocs.aspx.cs" Inherits="GeneratePDFDocs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Bakery - Generate PDF Docs</title>
    <script language="javascript" type="text/javascript">
    <!-- Begin
    var imgRoot = "../images/";
    var imgArray = new Array();
    imgArray[0] = new Image();
    imgArray[0].src = imgRoot + "w1.gif";
    imgArray[1] = new Image();
    imgArray[1].src = imgRoot + "w2.gif";
    imgArray[2] = new Image();
    imgArray[2].src = imgRoot + "w3.gif";
    imgArray[3] = new Image();
    imgArray[3].src = imgRoot + "w4.gif";

    function rollImages(index) 
    {
     document.images['animation'].src = imgArray[index].src;
     var functionCall = "rollImages(" + (index + 5) % 4 + ")";
     setTimeout(functionCall,300);
    }

    function submit() 
    {
            rollImages(0);
            document.form1.submit();
    }
    //  End -->
    </script>    
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <p align="center">&nbsp;</p>
    <asp:PlaceHolder ID="phGenerate" runat="server">
        <p align="center">
        <font size="2" face="Verdana">
        Daily pdf files generation is in progress, Please wait...
        <br />
        <br />
        <img name="animation" src="../images/w1.gif" border="0" WIDTH="41" HEIGHT="35">
        
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phCompleteMsg" runat="server" Visible="false">
        <p align="center">
        <font size="2" face="Verdana">        
        <asp:Label ID="lblMsg" runat="server" Text="" />
        </font>
        </p>
    </asp:PlaceHolder>
    <p align="center">&nbsp;</p>  
    <p align="center">&nbsp;</p>    
    </div>
    </form>
</body>
</html>
<%= AnimationInitializeScript%>

