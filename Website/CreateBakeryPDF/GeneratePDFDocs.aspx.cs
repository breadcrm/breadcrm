using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class GeneratePDFDocs : System.Web.UI.Page
{
    protected string AnimationInitializeScript = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            GenerateDocs();
            phGenerate.Visible = false;
            phCompleteMsg.Visible = true;
            lblMsg.Text = "Daily pdf files generation completed!";
        }
        else
        {
            AnimationInitializeScript = 
@"
<script language=""javascript"">
submit(); 
//  End -->
</script>
";
        }
    }

    private void GenerateDocs()
    {
       // DateTime genDate = DateTime.ParseExact(Request.QueryString["deldate"], "yyyy-MM-dd", null);
        DateTime genDate = DateTime.ParseExact("2015-05-18", "yyyy-MM-dd", null);
        DailyPDFGenerator.GenerateDocs(genDate);
    }
}
