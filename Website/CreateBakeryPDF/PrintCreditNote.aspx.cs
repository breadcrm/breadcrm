using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class PrintCreditNote : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["docno"]))
        {
            int crdNo = int.Parse(Request.QueryString["docno"]);
            string generatedFile = PrintPDFCreditNote.GenerateDoc(crdNo, null, true);

            Response.Clear();
            Response.ContentType = "application/pdf";

            Response.WriteFile(generatedFile);

            Response.Flush();

            File.Delete(System.Web.HttpContext.Current.Server.MapPath(generatedFile));

            Response.End();
        }
    }
}
