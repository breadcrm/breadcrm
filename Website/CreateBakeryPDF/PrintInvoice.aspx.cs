using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class PrintInvoice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["docno"]))
        {
            int invNo = int.Parse(Request.QueryString["docno"]);

            string sFooter = GetFooterText();

            string generatedFile = PrintPDFInvoice.GenerateDoc(invNo, null, true, sFooter);

            Response.Clear();
            Response.ContentType = "application/pdf";

            Response.WriteFile(generatedFile);

            Response.Flush();

            File.Delete(System.Web.HttpContext.Current.Server.MapPath(generatedFile));

            Response.End();
        }
    }

    public string GetFooterText()
    {
        string footer = string.Empty;
        string newFooter = string.Empty;
        ProcessReportAllInvoices a = new ProcessReportAllInvoices();
        footer = a.GetFooterNote();
        newFooter = footer.Replace("<BR>", "\n");
        newFooter = newFooter.Replace("<P>", "");
        newFooter = newFooter.Replace("</P>", "");
        newFooter = newFooter.Replace("^^^", "'");
        return newFooter;
    }
}
