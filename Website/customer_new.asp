<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%

Dim obj,object,detail,vFacarray,vVecarray
Dim arGroup
Dim arSubGroup
Dim objCustomer
Dim arCustomer
Dim objCustomerSettings
Dim arCustomerSettings
Dim strName,strOldCode,strAdd1,strAdd2
DIm strTown,strPostcode,strTele,strType,strOrderType
Dim strOtherType,strSYear,strOriCustomer,strPayTerm
Dim strDayContactName,strDayContactTele,strMakerName
Dim strMakerTele,strPrice,strWeekVan,strWeekendVan,strDelivery
Dim strGroup,strStandingOrder,strPurchaseOrder,strStatus
Dim strComment,strNotes,vPriceTypeValue,strPricelessInvoice,strPricelessInvoiceChecked,strCustomerNotes
Dim strCustNameInAutomatedDocFileName
Dim i
Dim vCustNo
Dim strRecordExistTag
Dim strTypeTag
Dim objResult
dim delete
dim strNickname
dim strFax
dim strNoofInvoices,ObjCus,strFactor 
dim strVNo_Mon, strVNo_Tue, strVNo_Wed, strVNo_Thu, strVNo_Fri, strVNo_Sat, strVNo_Sun
dim strGECSVChecked
Dim vPackingSheetType
Dim strCentralKitchanRpt
Dim strCentralKitchanRptChecked
Dim strWSCentralKitchanRpt
Dim strWSCentralKitchanRptChecked
Dim strVCCustomerChecked,strVCCustomer,strSVCCustomer,strSVCCustomerChecked
strGECSVChecked="Checked"

Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayFacility()
vFacarray =  detail("Facility")
set detail = object.DisplayVec()
vVecarray =  detail("Vec")
set detail = object.GetPriceTypeValues()
vPriceTypeValue =  detail("PriceTypeValue")
Dim sAction, nEmailCount
nEmailCount = Request.Form("EmailCount")
sEmailValues = Request.Form("EmailValues")
sAction = Request.Form("Action")



if sAction = "" Then
    nEmailCount = 1
End If

strEmail2 = sEmailValues
if  (strEmail2 <> "" ) Then
arrEmail = Split(strEmail2,",")
End if

strPrice = Request.Form("selPrice") 
if (isnumeric(strPrice)) then
	set detail = object.GetPriceTypeName(strPrice)
	vPriceTypeName =  detail("PriceTypeName")
end if

strGroup = Request.Form("selGroup")
if (isnumeric(strGroup)) then
	set detail = object.GetGroupName(strGroup)
	strGroupName =  detail("GroupName")
end if
	
set detail = Nothing
set object = Nothing

dim vResellers,strResellerID,strPricePercentage
Set object = Server.CreateObject("bakery.Reseller")
object.SetEnvironment(strconnection)
set detail = object.ListResellers()
vResellers =  detail("Resellers")
set detail = Nothing
set object = Nothing

Set obj = CreateObject("Bakery.Customer")
obj.SetEnvironment(strconnection)
if Trim(Request.Form("Update")) <> "" Then
	vCustNo = Request.Form("CustNo") 

	if request.form("type")= "Delete" then
		delete = obj.DeleteCustomer(vCustNo )
		LogAction "Customer Deleted", "Name: " & Request.Form("txtName"), vCustNo
		response.redirect "customer_find.asp"
	end if
	
	if request.form("type")= "Stop" then
		LogAction "Customer Deactivated", "Name: " & Request.Form("txtName"), ""
		delete = obj.StopActivateCustomer(vCustNo,"S")
		response.redirect "customer_find.asp"
	end if
	
	if request.form("type")= "Active" then
		LogAction "Customer Activated", "Name: " & Request.Form("txtName"), ""
		delete = obj.StopActivateCustomer(vCustNo,"A")
		response.redirect "customer_find.asp"
	end if

	strName = Request.Form("txtName")  
	strOldCode  = Request.Form("txtOldCode")  
	strAdd1  = Request.Form("txtAdd1")  
	strAdd2 =   Request.Form("txtAdd2")  
	strTown = Request.Form("txtTown")  
	strPostcode = Request.Form("txtPostcode")  
	strTele = Request.Form("txtTele")	
	
	if Trim(Request.Form("selType")) <> "" Then    
		strType = Request.Form("selType")  
	Else
		strType = Request.Form("txtOtherType")  
	End if			
	
	strSYear = Request.Form("txtSYear")  
	strOriCustomer = "Bread Factory" 'Request.Form("selOriCustomer")  
	strPayTerm = Request.Form("selPayTerm")  
	strDayContactName = Request.Form("txtDayContactName")  
	strDayContactTele = Request.Form("txtDayContactTele")  
	strMakerName = Request.Form("txtMakerName")  
	strMakerTele = Request.Form("txtMakerTele")  
	
	strFactor = Request.Form("Factor") 
	 
	'strWeekVan = Request.Form("selWeekVan")  
	'strWeekendVan = Request.Form("selWeekendVan")  
	
	strVNo_Mon = Request.Form("selVNo_Mon")
	strVNo_Tue = Request.Form("selVNo_Tue")
	strVNo_Wed = Request.Form("selVNo_Wed")
	strVNo_Thu = Request.Form("selVNo_Thu")
	strVNo_Fri = Request.Form("selVNo_Fri")
	strVNo_Sat = Request.Form("selVNo_Sat")
	strVNo_Sun = Request.Form("selVNo_Sun")
	
	

	strSubGroupName = Request.Form("SubGroupName")	
	if strSubGroupName="" or not isnumeric(strSubGroupName) then
		strSubGroupName=0
	end if
	strPurchaseOrder = Request.Form("selPurchaseOrder")  	
	strRecordExistTag = Request.Form("RecordExistTag") 
	strStandingOrder = Request.Form("selStatdingOrd") 
	vPackingSheetType = Request.Form("selPackingSheetType")  
	strOrderType = Request.Form("selOrderType")

	strComment=request.Form("txtComment")
	strAccountManager=request.Form("txtAccountManager")
	'session("AccountManagerName") = strAccountManager
	strNotes=left(request.Form("txtNotes"),745)
		
	strNickName = Request.Form("txtNickName")
	strFax = Request.Form("txtFax")
	strNoofInvoices = Request.Form("selNoofInvoices")
	strMinimumOrder=Request.Form("txtMinimumOrder")
	
	if (strMinimumOrder="") then
		strMinimumOrder="null"
	end if
	
	if (not (IsNumeric(strMinimumOrder))) then
		strMinimumOrder="null"
	end if
	
	strdeliverychargepno=Request.Form("deliverychargepno")
	
	strPricelessInvoice = Request.Form("PricelessInvoice")
	if (strPricelessInvoice="1") then
		strPricelessInvoiceChecked="Checked"
	else
		strPricelessInvoice="0"
	end if
	
	strDailyAutomatedInvoice = Request.Form("DailyAutomatedInvoice")
	if (strDailyAutomatedInvoice="1") then
		strDailyAutomatedInvoiceChecked="Checked"
	else
		strDailyAutomatedInvoice="0"
	end if
	
	strDailyAutomatedCredit = Request.Form("DailyAutomatedCredit")
	if (strDailyAutomatedCredit="1") then
		strDailyAutomatedCreditChecked="Checked"
	else
		strDailyAutomatedCredit="0"
	end if
	
	
	strcustomercategoryid = Request.Form("customercategoryid")
	if (not isnumeric(strcustomercategoryid)) then
		strcustomercategoryid=0
	end if
	strGECSV = Request.Form("GECSV")
	
	strTickBoxes = Request.Form("TickBoxes")
	strVCCustomer = Request.Form("VCCustomer")
	strSVCCustomer = Request.Form("SVCCustomer")
	
	'strGECSV = 0
	
	if (strGECSV="1") then
		strGECSV = 1
		strGECSVChecked="Checked"
	else
		strGECSV=0
	end if
	
	strCentralKitchanRpt = Request.Form("CentralKitchenRpt")
	
	if (strCentralKitchanRpt = "1") then
		strCentralKitchanRpt = 1
	else
		strCentralKitchanRpt=0
	end if
	
	strWSCentralKitchanRpt = Request.Form("WSCentralKitchenRpt")
	strDelivery  = Request.Form("txtDelivery") 
	if (strWSCentralKitchanRpt = "1") then
		strWSCentralKitchanRpt = 1
	else
		strWSCentralKitchanRpt=0
	end if
		
	strCustNameInAutomatedDocFileName = Request.Form("txtCustNameInAutomatedDocFileName")
	
	if Request.Form("selStatus") <> "" Then
		strStatus = Request.Form("selStatus") 
	Else
		strStatus = "A"
	End if
	
	'strResellerID = Request.Form("ResellerID")
	strResellerID=0 
	strCustomerNotes = Request.Form("CustomerNotes") 
	'if strResellerID<>"" then
		'dim arrResellerID
		'arrResellerID=split(strResellerID,"///")
		'strResellerID=arrResellerID(0)
	'end if
	
	'if strResellerID="" then
		'strResellerID=0
	'end if
	'Response.Write vCustNo & "," & strName & "," & strOldCode & "," & strAdd1 & "," & strAdd2 & "," & strTown & "," & strPostcode & "," & strTele & "," & strType & "," & strSYear & "," & strOriCustomer & "," & strPayTerm & "," & strDayContactName & "," & strDayContactTele & "," & strMakerName & "," & strMakerTele & "," & strPrice & "," & strWeekVan & "," & strWeekendVan & "," & strGroup & "," & strPurchaseOrder & "," & strStatus & "," & strSubGroupName	
	'response.end
	 Dim tmpCno
	 tmpCno = 0
	 if(vCustNo <> "") Then
		tmpCno = CLng(vCustNo)
	 End if
	 Dim ValidFileName
	 
	 ValidFileName = True
	 
	 if(strCustNameInAutomatedDocFileName <> "") Then
		ValidFileName = obj.IsUniqueCustNameInAutomatedDocFileName(tmpCno,cstr(strCustNameInAutomatedDocFileName))
	 End if
	
	 if(ValidFileName) Then
	 
		Dim blnNewCus
		
		blnNewCus = false
		
		if(vCustNo = "") then
			blnNewCus = true	
		end if
	 
		'set objCustomer = obj.Save_Customer(vCustNo,strName,strOldCode,strAdd1,strAdd2,strTown,strPostcode,strTele,strType,strSYear,strOriCustomer,strPayTerm,strDayContactName,strDayContactTele,strMakerName,strMakerTele,strDelivery,strPrice,strGroup,strPurchaseOrder,strStatus,strRecordExistTag,strStandingOrder,strOrderType,strNickName,strFax,strNoofInvoices,strComment,strNotes,strPricelessInvoice,strResellerID,strCustomerNotes,strSubGroupName,strDailyAutomatedInvoice,strDailyAutomatedCredit,strCustNameInAutomatedDocFileName,strVNo_Mon, strVNo_Tue, strVNo_Wed, strVNo_Thu, strVNo_Fri, strVNo_Sat, strVNo_Sun, strGECSV, vPackingSheetType, strCentralKitchanRpt,strWSCentralKitchanRpt,strAccountManager,strcustomercategoryid,strMinimumOrder,strdeliverychargepno)	 	
		set objCustomer = obj.Save_Customer(vCustNo,strName,strOldCode,strAdd1,strAdd2,strTown,strPostcode,strTele,strType,strSYear,strOriCustomer,strPayTerm,strDayContactName,strDayContactTele,strMakerName,strMakerTele,strDelivery,strPrice,strGroup,strPurchaseOrder,strStatus,strRecordExistTag,strStandingOrder,strOrderType,strNickName,strFax,strNoofInvoices,strComment,strNotes,strPricelessInvoice,strResellerID,strCustomerNotes,strSubGroupName,strDailyAutomatedInvoice,strDailyAutomatedCredit,strCustNameInAutomatedDocFileName,strVNo_Mon, strVNo_Tue, strVNo_Wed, strVNo_Thu, strVNo_Fri, strVNo_Sat, strVNo_Sun, strGECSV, vPackingSheetType, strCentralKitchanRpt,strWSCentralKitchanRpt,strcustomercategoryid,strMinimumOrder,strdeliverychargepno,strVCCustomer,strSVCCustomer)	 	
 
		if blnNewCus then
			LogAction "Customer Added", "Name:" & strName	, ""
		else
			strhidcustomerNameold = Request.Form("hidcustomerNameold")
			strhidStandingOrderold = Request.Form("hidStandingOrderold")
			strhidPriceold = Request.Form("hidPriceold")
			strhidselVNo_Monold = Request.Form("hidselVNo_Monold")
			strhidselVNo_Tueold = Request.Form("hidselVNo_Tueold")
			strhidselVNo_Wedold = Request.Form("hidselVNo_Wedold")
			strhidselVNo_Thuold = Request.Form("hidselVNo_Thuold")
			strhidselVNo_Friold = Request.Form("hidselVNo_Friold")
			strhidselVNo_Satold = Request.Form("hidselVNo_Satold")
			strhidselVNo_Sunold = Request.Form("hidselVNo_Sunold")
			
			strdetail=""
			
			if (strName<>strhidcustomerNameold) then 
				if (strdetail="") then
					strdetail=strdetail & "Name: " & strName 
				else
					strdetail=strdetail & "; Name: " & strName
				end if
			end if
			
			if (strStandingOrder<>strhidStandingOrderold) then 
				if (strStandingOrder="Y") then
					strStandingOrderValue="Yes"
				else
					strStandingOrderValue="No"
				end if
				
				if (strdetail="") then
					strdetail=strdetail & "Standing Order: " & strStandingOrderValue 
				else
					strdetail=strdetail & "; Standing Order: " & strStandingOrderValue
				end if
			end if
			
			if (strPrice<>strhidPriceold) then 
				if (vPriceTypeName="F" and strFactor<>"") then
					vPriceTypeName=vPriceTypeName & ", Formula: " & strFactor
				end if
				
				if (strdetail="") then
					strdetail=strdetail & "Price Policy: " & vPriceTypeName 
				else
					strdetail=strdetail & "; Price Policy: " & vPriceTypeName
				end if
			end if
			
			
			strvandetails=""
			
			if (strVNo_Mon<>strhidselVNo_Monold) then 
				if (strvandetails="") then
					strvandetails=strvandetails & " Van Details: Mon-" & strVNo_Mon 
				else
					strvandetails=strvandetails & ", Mon-" & strVNo_Mon
				end if
			end if
			
			if (strVNo_Tue<>strhidselVNo_Tueold) then 
				if (strvandetails="") then
					strvandetails=strvandetails & " Van Details: Tue-" & strVNo_Tue 
				else
					strvandetails=strvandetails & ", Tue-" & strVNo_Tue
				end if
			end if
			
			if (strVNo_Wed<>strhidselVNo_Wedold) then 
				if (strvandetails="") then
					strvandetails=strvandetails & " Van Details: Wed-" & strVNo_Wed 
				else
					strvandetails=strvandetails & ", Wed-" & strVNo_Wed
				end if
			end if
			
			if (strVNo_Thu<>strhidselVNo_Thuold) then 
				if (strvandetails="") then
					strvandetails=strvandetails & " Van Details: Thu-" & strVNo_Thu 
				else
					strvandetails=strvandetails & ", Thu-" & strVNo_Thu
				end if
			end if
			
			if (strVNo_Fri<>strhidselVNo_Friold) then 
				if (strvandetails="") then
					strvandetails=strvandetails & " Van Details: Fri-" & strVNo_Fri 
				else
					strvandetails=strvandetails & ", Fri-" & strVNo_Fri
				end if
			end if
			
			if (strVNo_Sat<>strhidselVNo_Satold) then 
				if (strvandetails="") then
					strvandetails=strvandetails & " Van Details: Sat-" & strVNo_Sat 
				else
					strvandetails=strvandetails & ", Sat-" & strVNo_Sat
				end if
			end if
			
			if (strVNo_Sun<>strhidselVNo_Sunold) then 
				if (strvandetails="") then
					strvandetails=strvandetails & " Van Details: Sun-" & strVNo_Sun 
				else
					strvandetails=strvandetails & ", Sun-" & strVNo_Sun
				end if
			end if
			
			if (strdetail<>"" and strvandetails<>"") then
				strdetail=strdetail & "; " & strvandetails
			else
				strdetail=strdetail & strvandetails
			end  if
			
			strhidDeliveryTimeOld = Request.Form("hidDeliveryTimeOld")			
			if (strDelivery<>strhidDeliveryTimeOld) then 
				if (strdetail="") then
					strdetail=strdetail & "Delivery Time: " & strDelivery 
				else
					strdetail=strdetail & "; Delivery Time: " & strDelivery
				end if
			end if
			
			strhidCentralKitchenRptOld=Request.Form("hidCentralKitchenRptOld")
			
			if (cstr(strCentralKitchanRpt)<>cstr(strhidCentralKitchenRptOld)) then 
				
				if (strCentralKitchanRpt="1") then
					strCentralKitchanRptValue="Yes"
				else
					strCentralKitchanRptValue="No"
				end if
				
				if (strdetail="") then
					strdetail=strdetail & "Include in Central Kitchen Report: " & strCentralKitchanRptValue 
				else
					strdetail=strdetail & "; Include in Central Kitchen Report: " & strCentralKitchanRptValue
				end if
			end if
			
			strhidOriCustomerOld = Request.Form("hidOriCustomerOld")
			if (strOriCustomer<>strhidOriCustomerOld) then 
				if (strdetail="") then
					strdetail=strdetail & "Original Customer of: " & strOriCustomer 
				else
					strdetail=strdetail & "; Original customer of: " & strOriCustomer
				end if
			end if
			
			strhidCustomerGroupOld = Request.Form("hidCustomerGroupOld")
			if (trim(strGroup)<>"") then
				if (strGroup<>strhidCustomerGroupOld) then 
					if (strdetail="") then
						strdetail=strdetail & "Customer Group: " & strGroupName 
					else
						strdetail=strdetail & "; Customer Group: " & strGroupName
					end if
				end if
			end if
			
			strhidPurchaseOrderOld = Request.Form("hidPurchaseOrderOld")
			if (strPurchaseOrder<>"") then
				if (strPurchaseOrder="Y") then
					strPurchaseOrderValue="Yes"
				else
					strPurchaseOrderValue="No"
				end if
				
				if (strPurchaseOrder<>strhidPurchaseOrderOld) then 
					if (strdetail="") then
						strdetail=strdetail & "Purchase Order: " & strPurchaseOrderValue 
					else
						strdetail=strdetail & "; Purchase Order: " & strPurchaseOrderValue
					end if
				end if
			end if
			
			strhidtxtAdd1Old = Request.Form("hidtxtAdd1Old")
			if (strAdd1<>strhidtxtAdd1Old) then 
				if (strdetail="") then
					strdetail=strdetail & "Address: " & strAdd1 
				else
					strdetail=strdetail & "; Address: " & strAdd1
				end if
			end if
			
			strhidselPackingSheetTypeOld = Request.Form("hidselPackingSheetTypeOld")
			if (vPackingSheetType<>"") then
				if (vPackingSheetType="1") then
					strPackingSheetTypeValue="Type 1"
				else
					strPackingSheetTypeValue="Type 2"
				end if
				
				if (vPackingSheetType<>strhidselPackingSheetTypeOld) then 
					if (strdetail="") then
						strdetail=strdetail & "Packing Sheet Type: " & strPackingSheetTypeValue 
					else
						strdetail=strdetail & "; Packing Sheet Type: " & strPackingSheetTypeValue
					end if
				end if
			end if
			
			strhidselTypeOld = Request.Form("hidselTypeOld")
			if (strType<>strhidselTypeOld) then 
				if (strdetail="") then
					strdetail=strdetail & "Type: " & strType 
				else
					strdetail=strdetail & "; Type: " & strType
				end if
			end if
			
			if (strdetail="") then
				strdetail="There are no change in this customer"
			end if
					
			LogAction "Customer Edited",strdetail,vCustNo
		end if  	
		  	
		vCustNo = objCustomer("CustomerNo")	
		
		if (strPrice="6" or strPrice="18" ) then 
			set ObjCus=obj.UpdateCustomerFormulaPriceBand(vCustNo,strFactor)	
		else
			set ObjCus=obj.DeleteCustomerFormulaPriceBand(vCustNo)
		end if
		 
		set ObjCus=  nothing
		   
 		if objCustomer("UpdateStatus") = "OK" Then
			vCustNo = objCustomer("CustomerNo")	
			
			'Set Customer statement settings
			
			strEmail = request.Form("txtEmail")
			nStatReq = request.Form("selStatementRequired")
			
			nRecuranPeriod = request.Form("RecurrencePeriodType")
			
			if nRecuranPeriod = 1 Then
				strWeekDate =  request.Form("RecurrencePeriodWeeklyType")
				
				strStartDate = GetWeeklyRecPeriodStartDate(strWeekDate)
			ElseIf nRecuranPeriod = 2 Then
			
				strStartDate = request.Form("txtStartDateFortnight")
			
			ElseIf nRecuranPeriod = 3 Then 
			
				strStartDate = request.Form("txtStartDateMonthly")
			
			End If

			'nRecuranPeriod = 0
			'strStartDate = ""
			
			nIsGroup = request.Form("selStatementSent")
			
			if request.Form("Action") = "Add" and vCustNo <> "" Then
			set ObjStatement = obj.SaveCustomerStatementSettings(CInt(vCustNo),CInt(nStatReq),strEmail,Cint(nRecuranPeriod),FormatDateInsert(strStartDate),CInt(nIsGroup))
			
			End If
			
			if request.Form("Action") = "Edit" Then
			
			set ObjStatement = obj.UpdateCustomerStatementSettings(CInt(vCustNo),CInt(nStatReq),strEmail,Cint(nRecuranPeriod),strStartDate,CInt(nIsGroup))
			
			End IF
			
			'start - Save Customer multiple Email address
			
			if  (sEmailValues <> "" ) Then
            arrEmail2 = Split(sEmailValues,",")
            End if

            If UBound(arrEmail2) > 0 Then
                set CustomerEmailGroupDelete= obj.DeleteMultipleEmailByGNO(cint(vCustNo),0)
            End If

            For i = 0 To UBound(arrEmail2)
		
			'for a group insert the group number and ser the IsGroup option 1
			set CustomerEmailGroupSave= obj.CustomerGroupEmailList(0,cint(vCustNo),arrEmail2(i),0)
			
			Next
            set CustomerEmailGroupSave= nothing
			'End - Save Customer multiple Email address
				 
			set obj = Nothing
			Set objCustomer = Nothing
			Response.Redirect "customer_finish.asp?Name="&Server.URLEncode(strName)&"&CustNo="&vCustNo&"&RecordExistTag="&strRecordExistTag		 
		Else
			Response.Redirect "customer_finish.asp?Name="&Server.URLEncode(strName)&"&RecordExistTag="&strRecordExistTag&"&ErrTag=True"
		End if	
	Else
		Response.Redirect "customer_finish.asp?Name="&Server.URLEncode(strName)&"&FileName="&Server.URLEncode(strCustNameInAutomatedDocFileName)&"&DuplicateFileNameTag=True"
	End If
Else
	'Update Existing Customer	
	If Trim(Request.Form("CustNo")) <> "" Then		
		strRecordExistTag = "True"
		vCustNo = Request.Form("CustNo") 
		Set ObjCustomer = obj.Display_CustomerDetail(vCustNo)
		arCustomer = ObjCustomer("Customer")						
		if IsArray(arCustomer) then	
			strcusno = arCustomer(0,0)	
			strName = arCustomer(1,0)
			strOldCode  = arCustomer(2,0)
			strAdd1  = arCustomer(3,0)
			strAdd2 =   arCustomer(4,0)
			strTown = arCustomer(5,0)
			strPostcode = arCustomer(6,0)
			strTele = arCustomer(7,0)						
			strType = arCustomer(8,0)			
			strSYear = arCustomer(9,0)
			strOriCustomer = arCustomer(10,0)
			
			strPayTerm = arCustomer(11,0)
			strDayContactName = arCustomer(12,0)
			strDayContactTele = arCustomer(13,0)
			strMakerName = arCustomer(14,0)
			strMakerTele = arCustomer(15,0)
			
			strPrice = arCustomer(16,0)
			strWeekVan = arCustomer(17,0)
			strWeekendVan = arCustomer(18,0)
			strGroup = arCustomer(19,0)
			strPurchaseOrder = arCustomer(20,0)
			strStatus = arCustomer(22,0)
			strStandingOrder = arCustomer(25,0)
			strOrderType = arCustomer(26,0)
			strNickName = arCustomer(27,0)
			strFax = arCustomer(28,0)
			strNoofInvoices = arCustomer(29,0)
			strComment=arCustomer(30,0)
			strNotes=arCustomer(31,0)
			strFactor=arCustomer(32,0)
			strPricelessInvoice=arCustomer(33,0)
			if (strPricelessInvoice="1") then
				strPricelessInvoiceChecked="Checked"
			end if
			'strResellerID=arCustomer(34,0)
			strCustomerNotes=arCustomer(36,0)
			strSubGroupName = arCustomer(38,0)
			
			strDailyAutomatedInvoice=arCustomer(39,0)
			if (strDailyAutomatedInvoice="1") then
				strDailyAutomatedInvoiceChecked="Checked"
			end if
			
			strDailyAutomatedCredit=arCustomer(40,0)
			if (strDailyAutomatedCredit="1") then
				strDailyAutomatedCreditChecked="Checked"
			end if
			
			strGECSV=arCustomer(49,0)
			if (strGECSV=1) then
				strGECSVChecked="Checked"
			else
			 	strGECSVChecked="" 	
			end if
					
			
			strCustNameInAutomatedDocFileName = arCustomer(41,0)
			
			
			
			strVNo_Mon = arCustomer(42,0)
			strVNo_Tue = arCustomer(43,0)
			strVNo_Wed = arCustomer(44,0)
			strVNo_Thu = arCustomer(45,0)
			strVNo_Fri = arCustomer(46,0)
			strVNo_Sat = arCustomer(47,0)
			strVNo_Sun = arCustomer(48,0)	
			
			vPackingSheetType = arCustomer(50,0)	
			
			strMinimumOrder=arCustomer(58,0)
			strGroupMinimumOrder=arCustomer(59,0)
			strdeliverychargepno=arCustomer(60,0)
			
			strVCCustomer=arCustomer(61,0)
			'response.write(strVCCustomer)
			
			if (strVCCustomer) then
				strVCCustomerChecked="Checked"
			end if
			strSVCCustomer=arCustomer(62,0)
			if (strSVCCustomer) then
				strSVCCustomerChecked="Checked"
			end if
			'response.write(strSVCCustomer)
			'response.end
		End if
		
		strCentralKitchanRpt = arCustomer(51,0)	
		if (strCentralKitchanRpt=true) then
			strCentralKitchanRptValueNew=1
			strCentralKitchanRptChecked="Checked"
		else
			strCentralKitchanRptValueNew=0
			strCentralKitchanRptChecked="" 	
		end if
		strWSCentralKitchanRpt = arCustomer(52,0)	
		if (strWSCentralKitchanRpt=true) then
			strWSCentralKitchanRptChecked="Checked"
		else
			strWSCentralKitchanRptChecked="" 	
		end if		
		strDelivery = arCustomer(53,0)
		
		strAccountManager=arCustomer(54,0)
		'session("AccountManagerName") = strAccountManager
		strcustomercategoryid=arCustomer(55,0)
		strcustomercategoryvalue=arCustomer(56,0)
		strgroupcustomercategoryid=arCustomer(57,0)
		Set objCustomerSettings = obj.GetetCustomerStatementSettings(vCustNo)
		arCustomerSettings = objCustomerSettings("CustomerStatement")
		
		if IsArray(arCustomerSettings) then		
		
			strEmail = arCustomerSettings(3,0)
			nStatReq  = arCustomerSettings(2,0)
			nRecuranPeriod = arCustomerSettings(4,0)
			strStartDate  = arCustomerSettings(5,0)
			nIsGroup = arCustomerSettings(6,0)
			
			if nRecuranPeriod = "1" Then
			
				sRecSelectedDay = WeekdayName(weekday(CDate(strStartDate)))
			
			End if
			
		
		End if
		
		if sAction <> "AddEmail" Then
		    if vCustNo <> "" then
                
                sEmailValues = DisplayEmailEditMode (vCustNo)
            sAction = "Edit"

            End If

            strEmail2 = sEmailValues
            if  (strEmail2 <> "" ) Then
            arrEmail = Split(strEmail2,",")
            End if

            if vCustNo <> "" then
            nEmailCount = UBound(arrEmail) + 1
            End If
            
          End If  
				
	Else
		strRecordExistTag = "False"	
	End if		
End if

set objCustomer = obj.Display_Grouplist()
arGroup = objCustomer("GroupList")	

set objCustomer = obj.Display_SubGrouplist()
arSubGroup = objCustomer("SubGroupList")

set objCustomer = obj.Display_CustomerCategoryList()
arCustomerCategory = objCustomer("CustomerCategoryList")	

Dim arGroupWithEmailAutomateInfo
set objCustomer = obj.Display_GrouplistWithEmailAutomateInfo()
arGroupWithEmailAutomateInfo = objCustomer("GroupList")	

set obj = Nothing
Set objCustomer = Nothing
set obj = Nothing
Set objCustomer = Nothing

Dim Grp_AutoEmailSend

Grp_AutoEmailSendInv = ""
Grp_AutoEmailSendCrd = ""
Grp_PricelessInvoice = ""

If IsArray(arGroupWithEmailAutomateInfo) then
	For i = 0 to UBound(arGroupWithEmailAutomateInfo,2)
		if i > 0 then
			Grp_AutoEmailSendInv = Grp_AutoEmailSendInv & ","
			Grp_AutoEmailSendCrd = Grp_AutoEmailSendCrd & ","
			Grp_PricelessInvoice = Grp_PricelessInvoice & ","
		end if
		
		if trim(cstr(arGroupWithEmailAutomateInfo(2,i))) <> "" then
			Grp_AutoEmailSendInv = Grp_AutoEmailSendInv & cstr(arGroupWithEmailAutomateInfo(2,i))
		else
			Grp_AutoEmailSendInv = Grp_AutoEmailSendInv & "0"
		end if
		
		if trim(cstr(arGroupWithEmailAutomateInfo(3,i))) <> "" then
			Grp_AutoEmailSendCrd = Grp_AutoEmailSendCrd & cstr(arGroupWithEmailAutomateInfo(3,i))
		else
			Grp_AutoEmailSendCrd = Grp_AutoEmailSendCrd & "0"
		end if	
		
		if trim(cstr(arGroupWithEmailAutomateInfo(4,i))) <> "" then
			Grp_PricelessInvoice = Grp_PricelessInvoice & cstr(arGroupWithEmailAutomateInfo(4,i))
		else
			Grp_PricelessInvoice = Grp_PricelessInvoice & "0"
		end if	
		
	Next
End if
	
%>
<html>
<!--#INCLUDE FILE="includes/head.inc" -->
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" onLoad="disableDetails();ResellerPriceBandShow();resetAutoSendEmailCtls(false);" text="#000000" style="font-family: Verdana; font-size: 8pt">
<script LANGUAGE="javascript" src="includes/Script.js"></script>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<script Language="JavaScript">
<!--

var Grp_AutoEmailSendInv = new Array (<%=Grp_AutoEmailSendInv%>);
var Grp_AutoEmailSendCrd = new Array (<%=Grp_AutoEmailSendCrd%>);
var Grp_PricelessInvoice = new Array (<%=Grp_PricelessInvoice%>);

var Curr_AutoEmailSendInv = '<%=strDailyAutomatedInvoiceChecked%>';
var Curr_AutoEmailSendCrd = '<%=strDailyAutomatedCreditChecked%>';
var Curr_PricelessInvoice = '<%=strPricelessInvoiceChecked%>';

var ori_grpIndex = 0;

function resetAutoSendEmailCtls(clearValues)
{
	var selGroup = document.getElementById('selGroup');
	var index = selGroup.selectedIndex;
	
	var DailyAutomatedInvoice = document.getElementById('DailyAutomatedInvoice');
	var DailyAutomatedCredit = document.getElementById('DailyAutomatedCredit');	
	var PricelessInvoice = document.getElementById('PricelessInvoice');	
	
	if(clearValues)
	{
		DailyAutomatedInvoice.checked  = false;
		DailyAutomatedCredit.checked = false;
		PricelessInvoice.checked = false;
	}
	else
	{
		//load for the first time
		//save original val
		ori_grpIndex = index;
	}
	
	if (index == 0)
	{
		DailyAutomatedInvoice.disabled = 'disabled';
		DailyAutomatedCredit.disabled = 'disabled';
		PricelessInvoice.disabled = 'disabled';
		return;
	}
	
	if (Grp_AutoEmailSendInv[index-1] == 1)
	{
		DailyAutomatedInvoice.disabled = '';
		if(index == ori_grpIndex)
		{
			if (Curr_AutoEmailSendInv == 'Checked')
				DailyAutomatedInvoice.checked  = true;
			else
				DailyAutomatedInvoice.checked  = false;
		}
		else
			DailyAutomatedInvoice.checked  = true;
	}
	else
	{
		DailyAutomatedInvoice.disabled = 'disabled';
	}
	
	if (Grp_AutoEmailSendCrd[index-1] == 1)
	{
		DailyAutomatedCredit.disabled = '';
		if(index == ori_grpIndex)
		{
			if (Curr_AutoEmailSendCrd == 'Checked')
				DailyAutomatedCredit.checked  = true;
			else
				DailyAutomatedCredit.checked  = false;
		}
		else		
			DailyAutomatedCredit.checked = true;
	}
	else
	{
		DailyAutomatedCredit.disabled = 'disabled';
	}
	
	if (Grp_PricelessInvoice[index-1] == 1)
	{
		PricelessInvoice.disabled = '';
		if(index == ori_grpIndex)
		{
			if (Curr_PricelessInvoice == 'Checked')
				PricelessInvoice.checked  = true;
			else
				PricelessInvoice.checked  = false;
		}
		else
			PricelessInvoice.checked  = true;
	}
	else
	{
		PricelessInvoice.disabled = 'disabled';
	}	
	
}
	
function SelectGE()
{
	if(document.Form1.ResellerID.selectedIndex==0)
	{
		//unchecked the GE checked box for Reseller = 0 (flour station customer)
		document.getElementById("GECSV").checked = true;
	}
	else
	{
		document.getElementById("GECSV").checked = false;
	}
}	
						
function ResellerPriceBandShow(){
	if(document.Form1.ResellerID.selectedIndex==0)
	{
		document.getElementById("ResellerPriceBandShow1").style.display = "none";
		
	}
	else 
	{
		document.getElementById("ResellerPriceBandShow1").style.display = "";
		var arrPriceBand= new Array ("","A","B","C","D","A1","F","","","","","","","","","","","","F1")
		s=document.Form1.ResellerID.value
		p=s.split("///")
		if (p[2]==0)
			document.getElementById("PriceBandReseller").innerHTML= arrPriceBand[p[1]];
		else
			document.getElementById("PriceBandReseller").innerHTML= arrPriceBand[p[1]] + ": " + p[2];	
			
			
	}
}

function disableDetails()
{
	  if (document.Form1.selPrice.value=="6" || document.Form1.selPrice.value=="18" )
	  	document.getElementById("showdata").style.display = "";  
	  else
	  	document.getElementById("showdata").style.display = "none";
}

function calculateFvalue()
{
	  if (document.Form1.selPrice.value=="6")
	  {
	 	vFactor=document.Form1.Factor.value
		//alert(vFactor)
		if ((((vFactor / vFactor) != 1) && (vFactor != 0)) || (vFactor==""))
		{
				alert('Please enter only numbers or decimal points in Factor');
				document.Form1.Factor.focus()
				return false;
		}
		//else
			//document.Form1.Fvalue.value= document.Form1.Factor.value*document.Form1.TypeA1.value;
	  	
	  }
	  else
	  	alert("Plese select F value in the Default Price")	
}

function validate(theForm,type){
debugger;
if (type == 'Edit')
{
	document.Form1.Action.value = "Edit";
}
else if(type == 'Add')
{
	document.Form1.Action.value = "Add";
}


  if (document.Form1.txtName.value == "")
  {
    alert("Please enter a value for the \"Name\" field.");
    document.Form1.txtName.focus();
    return (false);
  }

  if (document.Form1.txtName.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Name\" field.");
    document.Form1.txtName.focus();
    return (false);
  }

  if (document.Form1.txtAdd1.value == "")
  {
    alert("Please enter a value for the \"Address\" field.");
    document.Form1.txtAdd1.focus();
    return (false);
  }

  if (document.Form1.txtAdd1.value.length > 100)
  {
    alert("Please enter at most 100 characters in the \"Address\" field.");
    document.Form1.txtAdd1.focus();
    return (false);
  }


  /*
  if (document.Form1.customercategoryid.value=="")
  {
    alert("The select a  Customer Category");
    document.Form1.customercategoryid.focus();
    return (false);
  }
  
  if (document.Form1.Factor.value != "" && document.Form1.selPrice.value!="6")
  {
    alert("Plese select F value in the Default Price");
    document.Form1.selPrice.focus();
    return (false);
  }
  */
//  	if (document.Form1.selPrice.value=="6")
//	{
//		vFactor=document.Form1.Factor.value
//		if ((((vFactor / vFactor) != 1) && (vFactor != 0)) || (vFactor==""))
//		{
//			alert('Please enter only numbers or decimal points in Factor');
//			document.Form1.Factor.focus()
//			return false;
//		}
//	}
		
	/*
	if (document.Form1.selWeekVan.selectedIndex < 0)
	{
	  alert("Please select one of the \"Weekday\" options.");
	  document.Form1.selWeekVan.focus();
	  return (false);
	}

	
	if (document.Form1.selWeekVan.selectedIndex == 0)
	{
	  alert("The first \"Weekday\" option is not a valid selection.  Please choose one of the other options.");
	  document.Form1.selWeekVan.focus();
	  return (false);
	}

	if (document.Form1.selWeekendVan.selectedIndex < 0)
	{
	  alert("Please select one of the \"Weekend\" options.");
	  document.Form1.selWeekendVan.focus();
	  return (false);
	}
	

	
	if (document.Form1.selWeekendVan.selectedIndex == 0)
	{
	  alert("The first \"Weekend\" option is not a valid selection.  Please choose one of the other options.");
	  document.Form1.selWeekendVan.focus();
	  return (false);
	}
	*/
	
	var cboPreFxes = Array('Mon','Tue','Wed','Thu','Fri','Sat','Sun');
	var days = Array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
	for(wd = 0; wd < 7; wd++)
	{
		var cbo = document.getElementsByName('selVNo_' + cboPreFxes[wd])[0];
		if(cbo.selectedIndex == 0)
		{
			alert("The first \"" + days[wd] + "\" option is not a valid selection.  Please choose one of the other options.");
			cbo.focus();
			return (false);
		}
	}	
  
//  	if (document.Form1.selOriCustomer.selectedIndex < 0)
//	{
//	  alert("Please select one of the \"Original customer of\" options.");
//	  document.Form1.selOriCustomer.focus();
//	  return (false);
//	}

//	if (document.Form1.selOriCustomer.selectedIndex == 0)
//	{
//	  alert("The first \"Original customer of\" option is not a valid selection.  Please choose one of the other options.");
//	  document.Form1.selOriCustomer.focus();
//	  return (false);
//	}
  
 // if (document.Form1.selOrderType.options(document.Form1.selOrderType.selectedIndex).value == "R")
 // {
  
//   if (document.Form1.txtOldCode.value == "")
//  {
//    alert("Please enter a value for the \"Old code\" field.");
//    document.Form1.txtOldCode.focus();
//    return (false);
//  }

//  if (document.Form1.txtOldCode.value.length > 50)
//  {
//    alert("Please enter at most 50 characters in the \"Old code\" field.");
//    document.Form1.txtOldCode.focus();
//    return (false);
//  }


  if (document.Form1.txtTown.value == "")
  {
    alert("Please enter a value for the \"Town\" field.");
    document.Form1.txtTown.focus();
    return (false);
  }

  if (document.Form1.txtTown.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Town\" field.");
    document.Form1.txtTown.focus();
    return (false);
  }

  if (document.Form1.txtPostcode.value == "")
  {
    alert("Please enter a value for the \"Postcode\" field.");
    document.Form1.txtPostcode.focus();
    return (false);
  }

  if (document.Form1.txtPostcode.value.length > 20)
  {
    alert("Please enter at most 20 characters in the \"Postcode\" field.");
    document.Form1.txtPostcode.focus();
    return (false);
  }

  if (document.Form1.txtTele.value == "")
  {
    alert("Please enter a value for the \"Telephone\" field.");
    document.Form1.txtTele.focus();
    return (false);
  }

  if (document.Form1.txtTele.value.length > 20)
  {
    alert("Please enter at most 20 characters in the \"Telephone\" field.");
    document.Form1.txtTele.focus();
    return (false);
  }

   if (document.Form1.selType.selectedIndex < 0)
  {
    alert("Please select one of the \"Type\" options.");
    document.Form1.selType.focus();
    return (false);
  }

  if (document.Form1.selType.selectedIndex == 0)
  {
    alert("The first \"Type\" option is not a valid selection.  Please choose one of the other options.");
    document.Form1.selType.focus();
    return (false);
  }

	if (document.Form1.D1.selectedIndex < 0)
	{
	  alert("Please select one of the \"Group\" options.");
	  document.Form1.D1.focus();
	  return (false);
	}

	if (document.Form1.D1.selectedIndex == 0)
	{
	  alert("The first \"Group\" option is not a valid selection.  Please choose one of the other options.");
	  document.Form1.D1.focus();
	  return (false);
	}
	
	if(document.Form1.selStatementRequired.selectedIndex == 1)
	{
		//Validate Email
		if(document.Form1.txtEmail.value == "")
		{
			alert("Please enter \"Email\" to send the Statement.");
	  		document.Form1.txtEmail.focus();
	  		return (false);
		}
		
		//Validate Recurrence period 
		var recCount = 0
		for (var i=0; i < document.Form1.RecurrencePeriodType.length; i++)
	    {
	  	  if (document.Form1.RecurrencePeriodType[i].checked)
		  {
		  	recCount++;
		  }
	   }
	   
	   
	   if (recCount == 0)
	   {
	   		alert("Please select \"Recurrence period\" to send the Statement.");
	  		return (false);
	   } 
	   
	   if (document.Form1.RecurrencePeriodType[0].checked)
	   {
	   		var recCount2 = 0
			for (var i=0; i < document.Form1.RecurrencePeriodWeeklyType.length; i++)
			{
			  if (document.Form1.RecurrencePeriodWeeklyType[i].checked)
			  {
				recCount2++;
			  }
		   }
		   
		    if (recCount2 == 0)
		   {
				alert("Please select \"Day\" to send the Statement.");
				return (false);
		   }  
	   }
	   
	   if (document.Form1.RecurrencePeriodType[1].checked)
	   {
	   		if(document.Form1.txtStartDateFortnight.value == "")
			{
				alert("Please enter \"Start Date\" to send the Statement.");
	  			document.Form1.txtStartDateFortnight.focus();
	  			return (false);
			}
	   }
	   
	   if (document.Form1.RecurrencePeriodType[2].checked)
	   {
	   		if(document.Form1.txtStartDateMonthly.value == "")
			{
				alert("Please enter \"Start Date\" to send the Statement.");
	  			document.Form1.txtStartDateMonthly.focus();
	  			return (false);
			}
	   }
	   
	   
//	}
	
	if(document.Form1.txtEmail.value != "")
	{
		if(!isValidEmail(document.Form1.txtEmail.value))
		{
			alert("Please enter valid \"Email\" ");
	  		document.Form1.txtEmail.focus();
	  		return false;
		}		
	}
	
	if (document.Form1.selPurchaseOrder.selectedIndex < 0)
	{
	  alert("Please select one of the \"Purchase Order\" options.");
	  document.Form1.selPurchaseOrder.focus();
	  return (false);
	}

	if (document.Form1.selPurchaseOrder.selectedIndex == 0)
	{
	  alert("The first \"Purchase Order\" option is not a valid selection.  Please choose one of the other options.");
	  document.Form1.selPurchaseOrder.focus();
	  return (false);
	}

	if (document.Form1.selStatdingOrd.selectedIndex < 0)
	{
	  alert("Please select one of the \"Standing Order\" options.");
	  document.Form1.selStatdingOrd.focus();
	  return (false);
	}

	if (document.Form1.selStatdingOrd.selectedIndex == 0)
	{
	  alert("The first \"Standing Order\" option is not a valid selection.  Please choose one of the other options.");
	  document.Form1.selStatdingOrd.focus();
	  return (false);
	}	
  }
  
 if(!ValidateCustNameInAutomatedDocFileName())
	return false;
  
  if(!(type=="Add")){
		if(confirm("Would you like to " + type + " this customer?")){
			return true;
		}else{
			return false;
		}
	}	
	else
	{
		return true;	
	}
		
}
function deleteCustomer(){
	if(confirm("Would you like to Delete this customer?")){
		document.Form1.type.value='Delete';
		document.Form1.submit();
		return false;
	}else{
		return true;
	}
}


function StopActiveCustomer(mTag){
	if(mTag=="S"){
		if(confirm("Would you like to Stop this customer?")){
			document.Form1.type.value='Stop';
			document.Form1.submit();
			return false;
		}else{
			return true;
		}
	}	
	else{
		if(confirm("Would you like to Active this customer?")){
			document.Form1.type.value='Active';
			document.Form1.submit();
			return false;
		}else{
			return true;
		}		
	}
}


function ValidateCustNameInAutomatedDocFileName()
{
	var DailyAutomatedInvoice = document.getElementById('DailyAutomatedInvoice');
	var DailyAutomatedCredit = document.getElementById('DailyAutomatedCredit');	
	
	var txtCustNameInAutomatedDocFileName = document.getElementById('txtCustNameInAutomatedDocFileName');
	var CustNameInAutomatedDocFileName = txtCustNameInAutomatedDocFileName.value;

	if ((DailyAutomatedInvoice.checked == true) || (DailyAutomatedCredit.checked == true))
	{
		if(CustNameInAutomatedDocFileName.trim()=='')
		{
			
			alert('Please enter a value for \'Electronic Invoice\' field!');
			txtCustNameInAutomatedDocFileName.focus();
			return false;			
		}
		var specialchars = "\/:*?<>|";
		if(charCount(CustNameInAutomatedDocFileName,specialchars)>0)
		{
			alert('Electronic Invoice cannot contain any of the following characters\n' + specialchars);
			return false;
		}
	}
	return true;
}


function charCount(str, Chars) {
	var counter = 0;
    for (i = 0; i < str.length; i++) {
        var char = str.charAt(i);
        if (Chars.indexOf(char) > -1) {
            counter++;
        }
    }
    return counter;
}


String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/,"");
}

function resetDefVehicle()
{
	var defVCbo = document.getElementsByName('VNo_Def')[0];
	var cboPreFxes = Array('Mon','Tue','Wed','Thu','Fri','Sat','Sun');
	for(wd = 0; wd < 7; wd++)
	{
		var cbo = document.getElementsByName('selVNo_' + cboPreFxes[wd])[0];
		cbo.selectedIndex = defVCbo.selectedIndex;
	}

}

function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}
 
function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}

//validate email
function isValidEmail(str) {
   return (str.indexOf(".") > 2) && (str.indexOf("@") > 0);
 
}


function OnChange_RecurrencePeriod()
{
//unchecked date selection
	if (!document.Form1.RecurrencePeriodType[0].checked)
	{
		for (var i=0; i < document.Form1.RecurrencePeriodWeeklyType.length; i++)
		{
			document.Form1.RecurrencePeriodWeeklyType[i].checked = false;
		  
		}
	}
	
	if (!document.Form1.RecurrencePeriodType[1].checked)
		document.Form1.txtStartDateFortnight.value = ""
		
	if (!document.Form1.RecurrencePeriodType[2].checked)
		document.Form1.txtStartDateMonthly.value = ""	
	
}

function onClick_AddEmail() {
    debugger;
   // alert('test');
    document.Form1.Action.value = "AddEmail";
    document.Form1.Update.value = "";
   // alert(document.FrontPage_Form1.Action.value);
    document.Form1.EmailCount.value = parseInt(document.Form1.EmailCount.value) + 1;
    var count = document.Form1.EmailCount.value;

    var emailText;
    emailText = '';

    for (i = 1; i <= (count - 1); i++) {

        if (emailText != '') {
            emailText = emailText + ',' + document.getElementById("email" + i.toString()).value;

        }
        else {
            emailText = document.getElementById("email" + i.toString()).value;

        }

    }
//alert(emailText);
    document.Form1.EmailValues.value = emailText;
    document.Form1.submit();
}

function onClick_Save(action) {
    debugger;
    if (validate(this.form,action)) {
        document.Form1.Action.value = action;

        var count = document.Form1.EmailCount.value;

        var emailText;
        emailText = '';

        for (i = 1; i <= count ; i++) {

            if (emailText != '') {
                emailText = emailText + ',' + document.getElementById("email" + i.toString()).value;

            }
            else {
                emailText = document.getElementById("email" + i.toString()).value;

            }

        }

        document.Form1.EmailValues.value = emailText;
        
        
        document.Form1.submit();
    }
}


function EnabledInvoiceANDCreditNoteOption()
{
     var count = document.Form1.EmailCount.value;
     
     for (i = 1; i <= count ; i++) {
     
        var strValue = document.getElementById("email" + i.toString()).value;
           
        var DailyAutomatedInvoice = document.getElementById('DailyAutomatedInvoice');
	    var DailyAutomatedCredit = document.getElementById('DailyAutomatedCredit');	   
             
        if(strValue.trim().length > 0 )
        {            
            DailyAutomatedInvoice.disabled = '';
		    DailyAutomatedCredit.disabled = '';
		    return ;
        }
        else
        {
            DailyAutomatedInvoice.disabled = 'disabled';
		    DailyAutomatedCredit.disabled = 'disabled';
		    return ;
        }
     }
     
}


function onClick_EditAccManager(action) {

   
   var strCusNo = document.getElementById("CustomerNo").value; 
   var strAccMgrName = document.getElementById("txtAccountManager").value; 
  
   var myWindow = window.open("AmendAccountManager.asp?CNo=" + strCusNo + "&Name=" + strAccMgrName, "", "width=500, height=200, resizable=yes");
}

//-->
</script>
<!--#INCLUDE FILE="nav.inc" -->

<div align="center">
  <center>
  <form method="POST"  name="Form1">
  <INPUT type="hidden" name="hidcustomerNameold" value="<%=strName%>"> 
  <INPUT type="hidden" name="hidStandingOrderold" value="<%=strStandingOrder%>"> 
  <INPUT type="hidden" name="hidPriceold" value="<%=strPrice%>"> 
  <input type="hidden" name="EmailCount" value="<%=nEmailCount%>">
<input type="hidden" name="EmailValues" value="<%=sEmailValues%>">
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">

  <tr>
    <td width="100%"><br><b><font size="3"><%
    if strRecordExistTag = "True" Then
			Response.Write "Edit customer<br>"
    Else
			Response.Write "Create new customer<br>"
    End if%>
      &nbsp;</font></b>
      
      <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td width="50%" valign="top">
          <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="100%" height="455">
       <!-- <tr>
          <td height="28"><b>Reseller Name:</b></td>
          <td height="28">
           <table border="0" width="100%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt">
		  <tr>
		  <td align="left" width="100" >
		  <select size="1" name="ResellerID" onChange="ResellerPriceBandShow();SelectGE()" style="font-family: Verdana; font-size: 8pt">
		   	  <option value="">- - - Select - - -</option>
			  <%
			  dim arrPriceBand
			  arrPriceBand=array ("","A","B","C","D","A1","F","F1")
			  dim strResellerPriceBand
			
			  for i = 0 to ubound(vResellers,2)%>
				   <% if (vResellers(0,i)=cint(strResellerID)) then%>
						<option value="<%=vResellers(0,i)%>///<%=vResellers(2,i)%>///<%=vResellers(3,i)%>" selected="selected"><%=vResellers(1,i)%></option>
				   <%else%>
						<option value="<%=vResellers(0,i)%>///<%=vResellers(2,i)%>///<%=vResellers(3,i)%>"><%=vResellers(1,i)%></option>
				   <%end if%>
			   <%next%>
           </select>          </td>
		  <td width="100" align="left" id="ResellerPriceBandShow1">
		  		<label id="PriceBandReseller"><%=strResellerPriceBand%></label>		  </td>
		  <td>&nbsp;</td>
		  </tr>
		  </table>		  </td>
        </tr>-->
		<tr>
          <td height="22">Order Type<font color="#FF0000">*</font></td>
          <td height="22">
          <select size="1" name="selOrderType" style="font-family: Verdana; font-size: 8pt">
			  <% if strOrderType = "" Then %>
			  <option value="R" Selected>Regular</option>
			  <option value="S">Sample</option>
			  <% Else %>
				<option value="R" <% if strOrderType = "R" Then Response.Write "Selected" %>>Regular</option>
				<option value="S" <% if strOrderType = "S" Then Response.Write "Selected" %>>Sample</option>
			  <% End if %>
            </select>          </td>
        </tr>
		<%
		if (strcusno<>"") then
		%>
		<tr height="22">
          <td><b>Customer Code</b></td>
          <td height="19"><strong><%=strcusno%></strong></td>
        </tr>
		<%
		end if
		%>
        <tr>
          <td height="19"><b>Name</b><font color="#FF0000"> *</font></td>
          <td height="19">
          <input type="text" name="txtName" value="<%=strName%>" size="42" maxlength="50" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Nick Name </td>
          <td height="19">
          <input type="text" name="txtNickName" value="<%=strNickName%>" size="42" maxlength="50" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Old code </td>
          <td height="19">
          <input type="text" name="txtOldCode" value="<%=strOldCode%>" size="42" maxlength="30" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Address<font color="#FF0000"> *</font></td>
          <td height="19">
          <input type="text" name="txtAdd1" value="<%=strAdd1%>" size="42" maxlength="100" style="font-family: Verdana; font-size: 8pt">
		  <input type="hidden" name="hidtxtAdd1Old" value="<%=strAdd1%>">
		  </td>
        </tr>
        <tr>
          <td height="19"></td>
          <td height="19"><input type="text" name="txtAdd2" value="<%=strAdd2%>" maxlength="100" size="42" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Town <font color="#FF0000">*</font></td>
          <td height="19">
          <input type="text" name="txtTown" value="<%=strTown%>" size="42" maxlength="50" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Postcode <font color="#FF0000">*</font></td>
          <td height="19">
          <input type="text" name="txtPostcode" value="<%=strPostcode%>" size="42" maxlength="20" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Telephone <font color="#FF0000">*</font></td>
          <td height="19">
          <input type="text" name="txtTele" value="<%=strTele%>" size="42" maxlength="20" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Email for Accounting</td>
          <td height="19"><input type="text" name="txtEmail" value="<%=strEmail%>" size="42" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Fax </td>
          <td height="19">
          <input type="text" name="txtFax" value="<%=strFax%>" size="42" maxlength="20" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="51">Type <font color="#FF0000">*</font><br>
            <br>
            <br>          </td>
          <td height="51">
          <select size="1" name="selType" style="font-family: Verdana; font-size: 8pt">
							<option value=" ">Select</option>
              <option value="Air Lines" <%if Trim(strType) = "Air Lines" Then Response.Write "Selected"%>>Air Lines</option>
              <%if Trim(strType) = "Air Lines" Then strTypeTag = "True"%>
              <option value="Bar/Pub" <%if Trim(strType) = "Bar/Pub" Then Response.Write "Selected"%>>Bar/Pub</option>
              <%if Trim(strType) = "Bar/Pub" Then strTypeTag = "True"%>
             
              <option value="Clubs" <%if Trim(strType) = "Clubs" Then Response.Write "Selected"%>>Clubs</option>
              <%if Trim(strType) = "Clubs" Then strTypeTag = "True"%>
             
              <option value="Event Catering" <%if Trim(strType) = "Event Catering" Then Response.Write "Selected"%>>Event Catering</option>
              <%if Trim(strType) = "Event Catering" Then strTypeTag = "True"%>
              <option value="Fine Dining" <%if Trim(strType) = "Fine Dining" Then Response.Write "Selected"%>>Fine Dining</option>
              <%if Trim(strType) = "Fine Dining" Then strTypeTag = "True"%>
              
              <option value="Hotel" <%if Trim(strType) = "Hotel" Then Response.Write "Selected"%>>Hotel</option>
              <%if Trim(strType) = "Hotel" Then strTypeTag = "True"%>
              
	          <option value="Other" <%if Trim(strType) = "Other" Then Response.Write "Selected"%>>Other</option>
              <%if Trim(strType) = "Other" Then strTypeTag = "True"%>           
             
              <option value="Restaurants" <%if Trim(strType) = "Restaurants" Then Response.Write "Selected"%>>Restaurants</option>
              <%if Trim(strType) = "Restaurants" Then strTypeTag = "True"%>
             
              <option value="Sporting Events" <%if Trim(strType) = "Sporting Events" Then Response.Write "Selected"%>>Sporting Events</option>
              <%if Trim(strType) = "Sporting Events" Then strTypeTag = "True"%>           

            </select>
			<input type="hidden" name="hidselTypeOld" value="<%=strType%>">
			</td>
		</tr>
        <tr>
          <td height="19">Start Date:</td>
          <td height="19">
          <input type="text" name="txtSYear" value="<%=strSYear%>"  Maxlength="8" size="13" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr style ="display:none;">
          <td height="22">Original customer of: <font color="#FF0000">*</font></td>
          <td height="22">
          <select size="1" name="selOriCustomer" style="font-family: Verdana; font-size: 8pt">
			  <option value="">Select</option>
              <option value="Gail Force" <%if Trim(strOriCustomer) = "Gail Force" Then Response.Write "Selected"%>>Gail Force</option>
              <option value="Bread Factory" <%if Trim(strOriCustomer) = "Bread Factory" Then Response.Write "Selected"%>>Bread Factory</option>
            </select>
			<input type="hidden" name="hidOriCustomerOld" value="<%=strOriCustomer%>">
			</td>
        </tr>
        <tr>
          <td height="26">Payment Term:</td>
          <td height="26"><select size="1" name="selPayTerm" style="font-family: Verdana; font-size: 8pt">
			  <option value="0">Select</option>
              <option value="30 Days" <%if Trim(strPayTerm) = "30 Days" Then Response.Write "Selected"%>>30 Days</option>
              <option value="45 Days" <%if Trim(strPayTerm) = "45 Days" Then Response.Write "Selected"%>>45 Days</option>
              <option value="60 Days" <%if Trim(strPayTerm) = "60 Days" Then Response.Write "Selected"%>>60 Days</option>
            </select></td>
        </tr>
		<tr>
          <td height="26"><nobr>Minimum Order Amount:</nobr></td>
          <td height="26">&pound; <input type="text" name="txtMinimumOrder" value="<%=strMinimumOrder%>"  Maxlength="5" size="7" onKeyPress="if ( (event.keyCode > 32 && event.keyCode < 48 ) || (event.keyCode > 57 && event.keyCode < 255)) event.returnValue = false;" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
		 <tr>
          <td height="26"><nobr>Group Minimum Order:</nobr></td>
          <td height="26">
		   <%
		  if (strGroupMinimumOrder<>"") then
		  %>
		  &pound;<%=FormatNumber(strGroupMinimumOrder)%>
		  <%
		  end if
		  %>
		  </td>
        </tr>
		<tr>
          <td height="26"><nobr>Delivery Charge Option:</nobr></td>
          <td height="26">
		  <select size="1" name="deliverychargepno" style="font-family: Verdana; font-size: 8pt">
			  <option value="">Select</option>
              <option value="99-91-3523" <%if Trim(strdeliverychargepno) = "99-91-3523" Then Response.Write "Selected"%>>(Out of London)</option>
              <option value="99-91-1092" <%if Trim(strdeliverychargepno) = "99-91-1092" Then Response.Write "Selected"%>>(Normal Customer)</option>
           
            </select>
		  </td>
        </tr>
        <tr>
          <td height="26">No of Invoices:</td>
          <td height="26">
          <select size="1" name="selNoofInvoices" style="font-family: Verdana; font-size: 8pt">
			  <option value="1" <%if strNoofInvoices="1" then response.write " Selected"%>>1</option>
			  <option value="2" <%if strNoofInvoices="2" then response.write " Selected"%>>2</option>
			  <option value="3" <%if strNoofInvoices="3" then response.write " Selected"%>>3</option>
			  <option value="4" <%if strNoofInvoices="4" then response.write " Selected"%>>4</option>
			  <option value="5" <%if strNoofInvoices="5" then response.write " Selected"%>>5</option>
			  <option value="6" <%if strNoofInvoices="6" then response.write " Selected"%>>6</option>
			  <option value="7" <%if strNoofInvoices="7" then response.write " Selected"%>>7</option>
			  <option value="8" <%if strNoofInvoices="8" then response.write " Selected"%>>8</option>
			  <option value="9" <%if strNoofInvoices="9" then response.write " Selected"%>>9</option>
			  <option value="10" <%if strNoofInvoices="10" then response.write " Selected"%>>10</option>
            </select></td>
        </tr>
        <tr>
          <td height="1"></td>
          <td height="1"></td>
        </tr>
		 <tr height="26">
          <td>Sales Person:</td>
          <td><input type="text" name="txtComment" value="<%=strComment%>" size="25" maxlength="30" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
		 <tr height="26">
          <td>Account Manager:</td>
          <td><input type="text" name="txtAccountManager" id="txtAccountManager" value="<%=strAccountManager%>" size="25" maxlength="50" style="font-family: Verdana; font-size: 8pt"  <% if session("UserType") <> "S" AND session("UserType") <> "A" then%>  readonly="readonly" <%End if %> >
          
          <% if session("UserType") = "S" OR session("UserType") = "A" then%> 
          <input type="button" value="Edit Account Manager" name="EditA" style="font-family: Verdana; font-size: 8pt" onClick="onClick_EditAccManager('EditA');">
          <%End If %>
          </td>
        </tr>
		 <tr>
          <td height="19">Priceless Invoice/Credit:</td>
          <td height="19"><input type="checkbox" name="PricelessInvoice" id="PricelessInvoice" style="font-family: Verdana; font-size: 8pt" value="1" <%=strPricelessInvoiceChecked%>></td>
        </tr>
		 <tr>
          <td height="19">Daily Automated Invoice:</td>
          <td height="19"><input type="checkbox" name="DailyAutomatedInvoice" id="DailyAutomatedInvoice" style="font-family: Verdana; font-size: 8pt" value="1" <%=strDailyAutomatedInvoiceChecked%>></td>
        </tr>
		
		 <tr>
          <td height="19">Daily Automated Credit:</td>
          <td height="19"><input type="checkbox" name="DailyAutomatedCredit" id="DailyAutomatedCredit" style="font-family: Verdana; font-size: 8pt" value="1" <%=strDailyAutomatedCreditChecked%>></td>
        </tr>
		 <tr style="display:none;">
		   <td height="19">Send GE CSV file:</td>
		   <td height="19"><input type="checkbox" name="GECSV" id="GECSV" style="font-family: Verdana; font-size: 8pt" value="1" <%=strGECSVChecked%>></td>
		   </tr>
		<tr>
          <td height="19">Electronic Invoice:</td>
          <td height="19"><input type="text" name="txtCustNameInAutomatedDocFileName" style="font-family: Verdana; font-size: 8pt" id="txtCustNameInAutomatedDocFileName" value="<%=strCustNameInAutomatedDocFileName%>" size="15" maxlength="15" /></td>
        </tr>  
        <tr>
          <td height="19">Email for Electronic Invoice:</td>
          <td height="19"><input type="text" name="email1" id="email1" size="35" value="<%If UBound(arrEmail) >= 0 Then %><%=arrEmail(0)%><%Else %>""<%End IF %>" style="font-family: Verdana; font-size: 8pt" maxlength="100" onKeyUp="EnabledInvoiceANDCreditNoteOption();">
          <input type="submit" value=" + " name="B1" onClick="onClick_AddEmail();" style="font-family: Verdana; font-size: 9pt; font-weight:bold;">
          </td>
        </tr>
        <%Call CreateMoreEmail() %>          
		<tr>
          <td colspan="2" height="26"><b>Day to Day contact</b></td>
        </tr>
        <tr>
          <td height="19">Name</td>
          <td height="19">
          <input type="text" name="txtDayContactName" value="<%=strDayContactName%>" size="42" style="font-family: Verdana; font-size: 8pt" Maxlength="50"></td>
        </tr>
        <tr>
          <td height="19">Phone</td>
          <td height="19">
          <input type="text" name="txtDayContactTele" value="<%=strDayContactTele%>" size="42" style="font-family: Verdana; font-size: 8pt" Maxlength="20"></td>
        </tr>
        <tr>
          <td colspan="2" height="13"><b>Decision Maker</b></td>
        </tr>
        <tr>
          <td height="19">Name</td>
          <td height="19">
          <input type="text" name="txtMakerName" value="<%=strMakerName%>" size="42" style="font-family: Verdana; font-size: 8pt" Maxlength="50">
		  <input type="hidden" name="hidMakerNameOld" value="<%=strMakerName%>">
		  </td>
        </tr>
        <tr>
          <td height="19">Phone</td>
          <td height="19">
          <input type="text" name="txtMakerTele" value="<%=strMakerTele%>" size="42" style="font-family: Verdana; font-size: 8pt" Maxlength="20">
		  <input type="hidden" name="hidMakerTeleOld" value="<%=strMakerTele%>">
		  </td>
        </tr>
        <tr>
          <td width="22%">Customer Category</td>
          <td colspan="8">
		  <%
		  if (Isnumeric(strgroupcustomercategoryid) and strgroupcustomercategoryid<>0) then
		  %>
		  <%=strcustomercategoryvalue%>
			<input type="hidden" name="customercategoryid" value="<%=strcustomercategoryid%>">
			<%
			else
			%>
			<select size="1" name="customercategoryid" style="font-family: Verdana; font-size: 8pt" >
				<option value="0">---Select---</option>
				<%
				If IsArray(arCustomerCategory) then
					For i = 0 to UBound(arCustomerCategory,2)%>
						<option value="<%=arCustomerCategory(0,i)%>" <%if arCustomerCategory(0,i)=strcustomercategoryid Then Response.Write "Selected"%>><%=arCustomerCategory(1,i)%></option><%
					Next 						
				End if
				%>
            </select>
			
			<%
			end if
			%>
			</td>
        </tr>
      </table>
	  </td>
          <td width="50%" valign="top">
		  <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="100%;  ">
        <tr style="display:none;">
          <td bgcolor="#E1E1E1" width="22%"><strong>Pricing Policy</strong></td>
          <td colspan="8" bgcolor="#E1E1E1">&nbsp;</td>
        </tr>
        <tr style="display:none;" >
          <td bgcolor="#E1E1E1" width="22%">Default Price <font color="#FF0000">
          *</font></td>
          <td colspan="8" bgcolor="#E1E1E1">
			<%
				if (strPrice="") then
					strPrice="27"
				end if
			%>
			<select size="1" name="selPrice" style="font-family: Verdana; font-size: 8pt" onChange="disableDetails()">
             
			  <option value="5" <%if Trim(strPrice) = "5" Then Response.Write "Selected"%>>A1</option>
              <option value="1" <%if Trim(strPrice) = "1" Then Response.Write "Selected"%>>A</option>
			  <option value="2" <%if Trim(strPrice) = "2" Then Response.Write "Selected"%>>B</option>
              <option value="3" <%if Trim(strPrice) = "3" Then Response.Write "Selected"%>>C</option>
              <option value="4" <%if Trim(strPrice) = "4" Then Response.Write "Selected"%>>D</option>
			  <option value="6" <%if Trim(strPrice) = "6" Then Response.Write "Selected"%>>F</option>
			  
			  <option value="7" <%if Trim(strPrice) = "7" Then Response.Write "Selected"%>>B1</option>
			  <option value="8" <%if Trim(strPrice) = "8" Then Response.Write "Selected"%>>B2</option>
			  <option value="9" <%if Trim(strPrice) = "9" Then Response.Write "Selected"%>>B3</option>
			  <option value="10" <%if Trim(strPrice) = "10" Then Response.Write "Selected"%>>B4</option>
			  <option value="11" <%if Trim(strPrice) = "11" Then Response.Write "Selected"%>>B5</option>
			  <option value="12" <%if Trim(strPrice) = "12" Then Response.Write "Selected"%>>B6</option>
			  <option value="13" <%if Trim(strPrice) = "13" Then Response.Write "Selected"%>>B7</option>
			  <option value="14" <%if Trim(strPrice) = "14" Then Response.Write "Selected"%>>B8</option>
			  <option value="15" <%if Trim(strPrice) = "15" Then Response.Write "Selected"%>>B9</option>
			  <option value="19" <%if Trim(strPrice) = "19" Then Response.Write "Selected"%>>B10</option>
			  <option value="20" <%if Trim(strPrice) = "20" Then Response.Write "Selected"%>>B11</option>
			  <option value="21" <%if Trim(strPrice) = "21" Then Response.Write "Selected"%>>B12</option>
			  <option value="22" <%if Trim(strPrice) = "22" Then Response.Write "Selected"%>>B13</option>
			  <option value="23" <%if Trim(strPrice) = "23" Then Response.Write "Selected"%>>B14</option>
			  <option value="24" <%if Trim(strPrice) = "24" Then Response.Write "Selected"%>>B15</option>
			  <option value="17" <%if Trim(strPrice) = "17" Then Response.Write "Selected"%>>G</option>
			  <option value="18" <%if Trim(strPrice) = "18" Then Response.Write "Selected"%>>F1</option>
			  <option value="25" <%if Trim(strPrice) = "25" Then Response.Write "Selected"%>>D1</option>
			  <option value="26" <%if Trim(strPrice) = "26" Then Response.Write "Selected"%>>D2</option>
			  <option value="27" <%if Trim(strPrice) = "27" Then Response.Write "Selected"%>>D3</option>
			  <option value="28" <%if Trim(strPrice) = "28" Then Response.Write "Selected"%>>D4</option>
			  <option value="29" <%if Trim(strPrice) = "29" Then Response.Write "Selected"%>>D5</option>
			  <option value="30" <%if Trim(strPrice) = "30" Then Response.Write "Selected"%>>H</option>
			  <option value="31" <%if Trim(strPrice) = "31" Then Response.Write "Selected"%>>I</option>
		    </select>
			<span id="showdata">
			 Formula
			<input type="hidden" value="<%=vPriceTypeValue%>" name="TypeA1">
			<input name="Factor" type="text" value="<%=strFactor%>" style="color: #000000; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px" size="7" maxlength="7" onKeypress="if ((event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;">
			</span>
			<!--
			<input name="Apply" type="button" style="width:45px; font-size:12px; font-family:Arial,Verdana" value="Apply" onClick="calculateFvalue()">
			<input name="Fvalue" type="text" size="2" onFocus="this.blur()" style="color: #000000; font-weight:bold; background-color: #E1E1E1; border: 0px solid #000000;">
			-->			</td>
        </tr>
       <!-- <tr>
          <td bgcolor="#E1E1E1" width="22%">&nbsp;</td>
          <td colspan="8" bgcolor="#E1E1E1">&nbsp;</td>
        </tr>-->
        <tr>
          <td width="22%"></td>
          <td colspan="8"></td>
        </tr>
        <tr>
          <td width="22%"><b>Van Details</b></td>
          <td colspan="8"></td>
        </tr>
        <!--
        <tr>
          <td width="22%">Monday - Friday<font color="#FF0000"> *</font></td>
          <td width="46%">
		   <select size="1" name="selWeekVan" style="font-family: Verdana; font-size: 8pt">
		   	  <option value>Select</option>
			  <%
			  strWeekVan=Trim(strWeekVan)
			  if not isnumeric(strWeekVan) then
					strWeekVan=100	
			  end if
			  for i = 0 to ubound(vVecarray,2)%>
				   <% if (i=cint(strWeekVan)) then%>
						<option value="<%=vVecarray(0,i)%>" selected="selected"><%=vVecarray(0,i)%></option>
				   <%else%>
						<option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
				   <%end if%>
			   <%next%>
         </select>
		 </td>
        </tr>
        <tr>
          <td width="22%">Saturday - Sunday <font color="#FF0000">*</font></td>
          <td width="46%">
          <select size="1" name="selWeekendVan" style="font-family: Verdana; font-size: 8pt">
			  <option value>Select</option>        
			  <%
			  strWeekendVan=Trim(strWeekendVan)
			  if not isnumeric(strWeekendVan) then
					strWeekendVan=100	
			  end if
			  for i = 0 to ubound(vVecarray,2)%>
				   <% if (i=cint(strWeekendVan)) then%>
						<option value="<%=vVecarray(0,i)%>" selected="selected"><%=vVecarray(0,i)%></option>
				   <%else%>
						<option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
				   <%end if%>
			   <%next%>	
             </select>
			 </td>
        </tr>
        -->
        <%If vCustNo = "" Then%>
        <tr>
			<td width="22%">
					Select Vehicle&nbsp;			</td>
			<td colspan="8">
					<select size="1" name="VNo_Def" style="font-family: Verdana; font-size: 8pt" onChange="resetDefVehicle()">
							<option value>Select</option>					
							<%for i = 0 to ubound(vVecarray,2)%>
									<option value="<%=vVecarray(0,i)%>" ><%=vVecarray(0,i)%></option>
							<%next%>	
					</select>	
			   		&nbsp;For Mon-Sun			</td>
        </tr>
        <%End If%>
        <tr>
          <td width="22%">Monday<font color="#FF0000"> *</font></td>
          <td colspan="8">
		   <select size="1" name="selVNo_Mon" style="font-family: Verdana; font-size: 8pt">
		   	  <option value>Select</option>
			  <%
			  strVNo_Mon = Trim(strVNo_Mon)
			  if not isnumeric(strVNo_Mon) then
					strVNo_Mon = "100"	
			  end if
			  %>
			  <%for i = 0 to ubound(vVecarray,2)%>
				   <%if (i = cint(strVNo_Mon)) then%>
						<option value="<%=vVecarray(0,i)%>" selected="selected"><%=vVecarray(0,i)%></option>
				   <%else%>
						<option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
				   <%end if%>
			   <%next%>
         </select> <input type="hidden" name="hidselVNo_Monold" value="<%=strVNo_Mon%>">		 </td>
        </tr>        
        <tr>
          <td width="22%">Tuesday<font color="#FF0000"> *</font></td>
          <td colspan="8">
		   <select size="1" name="selVNo_Tue" style="font-family: Verdana; font-size: 8pt">
		   	  <option value>Select</option>
			  <%
			  strVNo_Tue = Trim(strVNo_Tue)
			  if not isnumeric(strVNo_Tue) then
					strVNo_Tue = "100"	
			  end if
			  %>
			  <%for i = 0 to ubound(vVecarray,2)%>
				   <%if (i = cint(strVNo_Tue)) then%>
						<option value="<%=vVecarray(0,i)%>" selected="selected"><%=vVecarray(0,i)%></option>
				   <%else%>
						<option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
				   <%end if%>
			   <%next%>
         </select>		  <input type="hidden" name="hidselVNo_Tueold" value="<%=strVNo_Tue%>"></td>
        </tr>
        <tr>
          <td width="22%">Wednesday<font color="#FF0000"> *</font></td>
          <td colspan="8">
		   <select size="1" name="selVNo_Wed" style="font-family: Verdana; font-size: 8pt">
		   	  <option value>Select</option>
			  <%
			  strVNo_Wed = Trim(strVNo_Wed)
			  if not isnumeric(strVNo_Wed) then
					strVNo_Wed = "100"	
			  end if
			  %>
			  <%for i = 0 to ubound(vVecarray,2)%>
				   <%if (i = cint(strVNo_Wed)) then%>
						<option value="<%=vVecarray(0,i)%>" selected="selected"><%=vVecarray(0,i)%></option>
				   <%else%>
						<option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
				   <%end if%>
			   <%next%>
         </select>		  <input type="hidden" name="hidselVNo_Wedold" value="<%=strVNo_Wed%>"></td>
        </tr>
        <tr>
          <td width="22%">Thursday<font color="#FF0000"> *</font></td>
          <td colspan="8">
		   <select size="1" name="selVNo_Thu" style="font-family: Verdana; font-size: 8pt">
		   	  <option value>Select</option>
			  <%
			  strVNo_Thu = Trim(strVNo_Thu)
			  if not isnumeric(strVNo_Thu) then
					strVNo_Thu = "100"	
			  end if
			  %>
			  <%for i = 0 to ubound(vVecarray,2)%>
				   <%if (i = cint(strVNo_Thu)) then%>
						<option value="<%=vVecarray(0,i)%>" selected="selected"><%=vVecarray(0,i)%></option>
				   <%else%>
						<option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
				   <%end if%>
			   <%next%>
         </select>		  <input type="hidden" name="hidselVNo_Thuold" value="<%=strVNo_Thu%>"></td>
        </tr>
        <tr>
          <td width="22%">Friday<font color="#FF0000"> *</font></td>
          <td colspan="8">
		   <select size="1" name="selVNo_Fri" style="font-family: Verdana; font-size: 8pt">
		   	  <option value>Select</option>
			  <%
			  strVNo_Fri = Trim(strVNo_Fri)
			  if not isnumeric(strVNo_Fri) then
					strVNo_Fri = "100"	
			  end if
			  %>
			  <%for i = 0 to ubound(vVecarray,2)%>
				   <%if (i = cint(strVNo_Fri)) then%>
						<option value="<%=vVecarray(0,i)%>" selected="selected"><%=vVecarray(0,i)%></option>
				   <%else%>
						<option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
				   <%end if%>
			   <%next%>
         </select>		  <input type="hidden" name="hidselVNo_Friold" value="<%=strVNo_Fri%>"></td>
        </tr>
        <tr>
          <td width="22%">Saturday<font color="#FF0000"> *</font></td>
          <td colspan="8">
		   <select size="1" name="selVNo_Sat" style="font-family: Verdana; font-size: 8pt">
		   	  <option value>Select</option>
			  <%
			  strVNo_Sat = Trim(strVNo_Sat)
			  if not isnumeric(strVNo_Sat) then
					strVNo_Sat = "100"	
			  end if
			  %>
			  <%for i = 0 to ubound(vVecarray,2)%>
				   <%if (i = cint(strVNo_Sat)) then%>
						<option value="<%=vVecarray(0,i)%>" selected="selected"><%=vVecarray(0,i)%></option>
				   <%else%>
						<option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
				   <%end if%>
			   <%next%>
         </select>		  <input type="hidden" name="hidselVNo_Satold" value="<%=strVNo_Sat%>"></td>
        </tr>
        <tr>
          <td width="22%">Sunday<font color="#FF0000"> *</font></td>
          <td colspan="8">
		   <select size="1" name="selVNo_Sun" style="font-family: Verdana; font-size: 8pt">
		   	  <option value>Select</option>
			  <%
			  strVNo_Sun = Trim(strVNo_Sun)
			  if not isnumeric(strVNo_Sun) then
					strVNo_Sun = "100"	
			  end if
			  %>
			  <%for i = 0 to ubound(vVecarray,2)%>
				   <%if (i = cint(strVNo_Sun)) then%>
						<option value="<%=vVecarray(0,i)%>" selected="selected"><%=vVecarray(0,i)%></option>
				   <%else%>
						<option value="<%=vVecarray(0,i)%>"><%=vVecarray(0,i)%></option>
				   <%end if%>
			   <%next%>
         </select>		  <input type="hidden" name="hidselVNo_Sunold" value="<%=strVNo_Sun%>"></td>
        </tr>                                                
        <tr>
          <td width="22%"></td>
          <td colspan="8"></td>
        </tr>
        
        <tr>
          <td width="22%">Customer Group <font color="#FF0000">*</font></td>
          <td colspan="8">
          <select size="selGroupTag" name="D1" style="font-family: Verdana; font-size: 8pt">							     
          	  <option <%if Trim(Request.Form("CustNo")) = "" and Trim(strGroup) = "" or IsNull(strGroup)  Then Response.Write "Selected"%>>Select</option>
              <option <%if Trim(strGroup) <> "" Then Response.Write "Selected"%>>Yes</option>
              <option <%if Trim(Request.Form("CustNo")) <> "" and Trim(strGroup) = "" or IsNull(strGroup)  Then Response.Write "Selected"%>>No</option>              
            </select></td>
        </tr>
        
        <tr>
          <td width="22%">If yes, which one</td>
          <td colspan="8"><select size="1" name="selGroup" id="selGroup" style="font-family: Verdana; font-size: 8pt" onChange="resetAutoSendEmailCtls(true);">
						<option value=" ">Select</option><%
						If IsArray(arGroupWithEmailAutomateInfo) then
							For i = 0 to UBound(arGroup,2)%>
								<option value="<%=arGroupWithEmailAutomateInfo(0,i)%>" <%if Trim(arGroupWithEmailAutomateInfo(0,i)) = Trim(strGroup) Then Response.Write "Selected"%>><%=arGroupWithEmailAutomateInfo(1,i)%></option><%
							Next 						
						End if%>
            </select>
			<input type="hidden" name="hidCustomerGroupOld" value="<%=strGroup%>">			</td>
        </tr>
		 <tr>
          <td width="22%">Sub Group</td>
          <td colspan="8"><select size="1" name="SubGroupName" style="font-family: Verdana; font-size: 8pt">
						<option value="">Select</option><%
						If IsArray(arSubGroup) then
							For i = 0 to UBound(arSubGroup,2)%>
								<option value="<%=arSubGroup(0,i)%>" <%if arSubGroup(0,i)=strSubGroupName Then Response.Write "Selected"%>><%=arSubGroup(1,i)%></option><%
							Next 						
						End if%>
            </select></td>
        </tr>
        <tr>
          <td width="22%"></td>
          <td colspan="8"></td>
        </tr>
     
        <tr>
          <td>Statement Required </td>
          <td colspan="8"> <select size="1" name="selStatementRequired" style="font-family: Verdana; font-size: 8pt">
			  <option value="0">Select</option>         
              <option value="1" <%if Trim(nStatReq) = "True" Then Response.Write "Selected"%>>Yes</option>
              <option value="0" <%if Trim(nStatReq) = "False" Then Response.Write "Selected"%>>No</option>
            </select>			</td>
        </tr>
        <tr>
          <td rowspan="5" valign="top">Recurrence period </td>
          <td width="8%"></td>
          <td width="38%" colspan="7"></td>
        </tr>
		
		
        <tr>
          <td rowspan="2"><label>
            <input name="RecurrencePeriodType" type="radio" value="1" class="radioinput" id="RecurrencePeriodType" <%if Trim(nRecuranPeriod) = "1" Then Response.Write "Checked"%> onClick="OnChange_RecurrencePeriod()">
          Weekly</label></td>
          <td>Mon</td>
          <td>Tue</td>
          <td>Wed</td>
          <td>Thu</td>
          <td>Fri</td>
          <td>Sat</td>
          <td>Sun</td>
        </tr>
        <tr>
          <td><label>
            <input name="RecurrencePeriodWeeklyType" type="radio" value="Monday" class="radioinput" id="RecurrencePeriodWeeklyType" <%if Trim(sRecSelectedDay) = "Monday" Then Response.Write "Checked"%>>
          </label></td>
          <td><label>
            <input name="RecurrencePeriodWeeklyType" type="radio" value="Tuesday" class="radioinput" id="RecurrencePeriodWeeklyType" <%if Trim(sRecSelectedDay) = "Tuesday" Then Response.Write "Checked"%>>
          </label></td>
          <td><label>
            <input name="RecurrencePeriodWeeklyType" type="radio" value="Wednesday" class="radioinput" id="RecurrencePeriodWeeklyType" <%if Trim(sRecSelectedDay) = "Wednesday" Then Response.Write "Checked"%>>
          </label></td>
          <td><label>
            <input name="RecurrencePeriodWeeklyType" type="radio" value="Thursday" class="radioinput" id="RecurrencePeriodWeeklyType" <%if Trim(sRecSelectedDay) = "Thursday" Then Response.Write "Checked"%>>
          </label></td>
          <td><label>
            <input name="RecurrencePeriodWeeklyType" type="radio" value="Friday" class="radioinput" id="RecurrencePeriodWeeklyType" <%if Trim(sRecSelectedDay) = "Friday" Then Response.Write "Checked"%>>
          </label></td>
          <td><label>
            <input name="RecurrencePeriodWeeklyType" type="radio" value="Saturday" class="radioinput" id="RecurrencePeriodWeeklyType" <%if Trim(sRecSelectedDay) = "Saturday" Then Response.Write "Checked"%>>
          </label></td>
          <td><label>
            <input name="RecurrencePeriodWeeklyType" type="radio" value="Sunday" class="radioinput" id="RecurrencePeriodWeeklyType" <%if Trim(sRecSelectedDay) = "Sunday" Then Response.Write "Checked"%>>
          </label></td>
        </tr>
        <tr>
          <td>
            <input name="RecurrencePeriodType" type="radio" value="2" class="radioinput" id="radio" <%if Trim(nRecuranPeriod) = "2" Then Response.Write "Checked"%> onClick="OnChange_RecurrencePeriod()">
          Fortnight            </td>
          <td colspan="7"><nobr>Start Date
            <input name="txtStartDateFortnight" type="text" id="txtStartDateFortnight" style="font-family: Verdana; font-size: 8pt" size="13"  Maxlength="10" onChange="setCalCombo(this.form,document.form.txtStartDateFortnight)" <%if Trim(nRecuranPeriod) = "2" Then%> value="<%=FormatDate(strStartDate)%>" <%End if%>>&nbsp;<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif"></nobr></td>
        </tr>
        <tr>
          <td><label>
           <input name="RecurrencePeriodType" type="radio" value="3" class="radioinput" id="RecurrencePeriodType" <%if Trim(nRecuranPeriod) = "3" Then Response.Write "Checked"%> onClick="OnChange_RecurrencePeriod()">
          Monthly</label></td>
          <td colspan="7"><nobr>Start Date 
            <input name="txtStartDateMonthly" type="text" id="txtStartDateMonthly" style="font-family: Verdana; font-size: 8pt" size="13"  Maxlength="10" onChange="setCalCombo1(this.form,document.form.txtStartDateFortnight)" <%if Trim(nRecuranPeriod) = "3" Then%> value="<%=FormatDate(strStartDate)%>" <%End if%>>&nbsp;<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif"></nobr></td>
        </tr>
        <tr>
          <td>Statement to be sent </td>
          <td colspan="8"><select size="1" name="selStatementSent" id="selGroup" style="font-family: Verdana; font-size: 8pt" onChange="resetAutoSendEmailCtls(true);">
						<option value="0" <%if Trim(nIsGroup) = "False" Then Response.Write "Selected"%>>Own Account</option>
						<option value="1" <%if Trim(nIsGroup) = "True" Then Response.Write "Selected"%>>Group</option>
            </select></td>
        </tr>
        <tr>
          <td width="22%">Does the customer require a Purchase Order <font color="#FF0000">
          *</font></td>
          <td colspan="8">
          <select size="1" name="selPurchaseOrder" style="font-family: Verdana; font-size: 8pt">
			  <option value="">Select</option>         
              <option value="Y" <%if Trim(strPurchaseOrder) = "Y" Then Response.Write "Selected"%>>Yes</option>
              <option value="N" <%if Trim(strPurchaseOrder) = "N" Then Response.Write "Selected"%>>No</option>
            </select>
			<input type="hidden" name="hidPurchaseOrderOld" value="<%=strPurchaseOrder%>">			</td>
        </tr>
        <tr>
          <td width="22%">&nbsp;</td>
          <td colspan="8"></td>
        </tr>
        
        <tr>
          <td width="22%">Standing Order <font color="#FF0000">*</font></td>
          <td colspan="8">
						<select size="1" name="selStatdingOrd" style="font-family: Verdana; font-size: 8pt">
			  		    <option value="">Select</option>						
					    <option value="Y" <%if Trim(strStandingOrder) = "Y" Then Response.Write "Selected"%>>Yes</option>
					    <option value="N" <%if Trim(strStandingOrder) = "N" Then Response.Write "Selected"%>>No</option>					    
					  </select>          </td>
        </tr>        
        <!--        <%                if strRecordExistTag = "True" Then%>					<tr>					   <td width="50%">Status</td>					   <td width="50%"><select size="1" name="selStatus" style="font-family: Verdana; font-size: 8pt">					       <option value="A" <%if Trim(strStatus) = "A" Then Response.Write "Selected"%>>Active</option>					       <option value="S" <%if Trim(strStatus) = "S" Then Response.Write "Selected"%>>Stopped</option>					       <option value="D" <%if Trim(strStatus) = "D" Then Response.Write "Selected"%>>Deleted</option>					     </select></td>					 </tr><%				End if%>				    		-->
        <tr height="26">
          <td><b>Comment</b></td>
          <td colspan="8"><textarea name="txtNotes" cols="25" rows="5"><%=strNotes%></textarea></td>
        </tr>
        <tr height="26">
          <td><b>Customer Notes</b></td>
          <td colspan="8"><textarea name="CustomerNotes" cols="25" rows="5"><%=strCustomerNotes%></textarea></td>
        </tr>
         <tr height="26" style="display:none;">
          <td><b>Packing Sheet Type</b></td>
          <td colspan="8">
          <select size="1" name="selPackingSheetType" style="font-family: Verdana; font-size: 8pt">
  		    				
		    <option value="1" <%if Trim(vPackingSheetType) = "1" Then Response.Write "Selected"%>>Type 1</option>
		    <option value="2" <%if Trim(vPackingSheetType) = "2" Then Response.Write "Selected"%>>Type 2</option>					    
		  </select> 
           <input type="hidden" name="hidselPackingSheetTypeOld" value="<%=vPackingSheetType%>">          </td>
        </tr>
       <tr>
          <td height="19"><b> Delivery Time </b> </td>
          <td height="19">
          <input type="text" name="txtDelivery" value="<%=strDelivery%>" size="42" style="font-family: Verdana; font-size: 8pt" Maxlength="20">
		  <input type="hidden" name="hidDeliveryTimeOld" value="<%=strDelivery%>">		  </td>
        </tr>
		        <tr style="display:none;">
		   <td height="19">Include in Central Kitchen Report</td>
		   <td height="19"><input type="checkbox" name="CentralKitchenRpt" id="Checkbox1" value="1" <%=strCentralKitchanRptChecked%> />
		   <input type="hidden" name="hidCentralKitchenRptOld" value="<%=strCentralKitchanRptValueNew%>">		   </td>
		</tr>
        <tr style="display:none;">
		   <td height="19">Include in Whole Sale Central Kitchen Report</td>
		   <td height="19"><input type="checkbox" name="WSCentralKitchenRpt" id="Checkbox2" value="1" <%=strWSCentralKitchanRptChecked%> /></td>
		</tr>
		 
		 <tr height="24" style="display:none;">
		   <td>VC Customer</td>
		   <td><input type="checkbox" name="VCCustomer" id="VCCustomer" style="font-family: Verdana; font-size: 8pt" value="1" <%=strVCCustomerChecked%>></td>
		 </tr>
		 <tr height="24" style="display:none;">
		   <td>SVC Customer</td>
		   <td><input type="checkbox" name="SVCCustomer" id="SVCCustomer" style="font-family: Verdana; font-size: 8pt" value="1" <%=strSVCCustomerChecked%>></td>
		</tr>
		<tr>
          <td width="22%"><font color="#FF0000"><strong>* Mandatory fields</strong></font></td>
          <td colspan="8"></td>
        </tr>
        <tr>
          <td width="22%">&nbsp;</td>
          <td colspan="8"></td>
        </tr>
        <tr>
					<input type="hidden" name="type" value>
          			<%if strRecordExistTag = "True"  Then%>						
	  				  <td width="22%" align="right"><input type="button" value="Edit customer" name="Edit" style="font-family: Verdana; font-size: 8pt" onClick="onClick_Save('Edit');">
					  
					 <!-- <input type="button" value="Edit customer" name="Edit" style="font-family: Verdana; font-size: 8pt" onClick="btnEdit_Onclick(this.form);">-->					  </td>
  						<td colspan="8"><%
  							if Trim(strStatus) = "A" Then
  							'According to BST -69 comment the below
  								'if session("UserType") <> "U" Then%>
  									<!--<input type="button" value="Delete customer" name="Delete" style="font-family: Verdana; font-size: 8pt" onClick="return deleteCustomer();">&nbsp;--><%
  								'End if%>	
  								<input type="button" value="Stop customer" name="Delete" style="font-family: Verdana; font-size: 8pt" onClick="return StopActiveCustomer('S');">
  							<%
  							Elseif Trim(strStatus) = "S" Then%>	
  								<input type="button" value="Activate customer" name="Delete" style="font-family: Verdana; font-size: 8pt" onClick="return StopActiveCustomer('A');"><%
  							End if%>					  </td>					
  						<%Else%>
					  <td width="28%" align="right"><input type="button" value="Add customer" name="Search" style="font-family: Verdana; font-size: 8pt" onClick="onClick_Save('Add');"></td>
						<td width="4%"></td>
					<%End if%>	
        </tr>
      </table></td>
        </tr>
      </table>
      
      

    </td>
  </tr>   
  <input type="hidden" name="Update" value="True">
  <input type="hidden" name="RecordExistTag" value="<%=strRecordExistTag%>">
  <input type="hidden" name="CustNo" value="<%=vCustNo%>">
  <input type="hidden" name="CustomerNo" id="CustomerNo" value="<%=vCustNo%>">
    <input type="hidden" name="Action" value="">
</table>

</form>
 <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtStartDateFortnight",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtStartDateMonthly",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
  </center>
</div>
</body>
</html><%
If IsArray(arGroup) Then Erase arGroup
If IsArray(arSubGroup) Then Erase arSubGroup
If IsArray(arCustomer) Then Erase arCustomer

'This is used to get the weekly recurrence period start date
Private Function GetWeeklyRecPeriodStartDate(weekD)

Dim currentDay 
Dim sStartDate
Dim sSelectedDate
dDate = Now() 
currentDay = WeekdayName(weekday(date()))
 
dStartDay = DateAdd("d", -1 * (DatePart("W", dDate) - 1),dDate)
dEndDay = DateAdd("d", 6, dStartDay)
 
 if currentDay = weekD Then
 	
	sStartDate = date() - 7 	
 	
else

	select case weekD
	case "Sunday":
		sStartDate = Cdate(dStartDay) - 7
     case "Monday":
		sSelectedDate =  DateAdd("d", 1, dStartDay)
		sStartDate = Cdate(sSelectedDate) - 7
     case "Tuesday":
		sSelectedDate =  DateAdd("d", 2, dStartDay)
		sStartDate = Cdate(sSelectedDate) - 7    
	 case "Wednesday":
		sSelectedDate =  DateAdd("d", 3, dStartDay)
		sStartDate = Cdate(sSelectedDate) - 7    
	 case "Thursday":
		sSelectedDate =  DateAdd("d", 4, dStartDay)
		sStartDate = Cdate(sSelectedDate) - 7    
	 case "Friday":
		sSelectedDate =  DateAdd("d", 5, dStartDay)
		sStartDate = Cdate(sSelectedDate) - 7    
	 case "Saturday":
		sSelectedDate =  DateAdd("d", 6, dStartDay)
		sStartDate = Cdate(sSelectedDate) - 7    
	 case else
     	sStartDate =  date() - 7
	End select
 End If

GetWeeklyRecPeriodStartDate = FormatDate(sStartDate)

End Function

Function FormatDate(strDate)

    Dim strYYYY
    Dim strMM
    Dim strDD

        strYYYY = CStr(DatePart("yyyy", strDate))

        strMM = CStr(DatePart("m", strDate))
        If Len(strMM) = 1 Then strMM = "0" & strMM

        strDD = CStr(DatePart("d", strDate))
        If Len(strDD) = 1 Then strDD = "0" & strDD

		' FormatDate = strYYYY & "-" & strMM & "-" & strDD
        FormatDate = strDD & "/" & strMM & "/" & strYYYY

End Function 

Function FormatDateInsert(strDate)
  
    Dim strYYYY
    Dim strMM
    Dim strDD

        strYYYY = CStr(DatePart("yyyy", strDate))

        strMM = CStr(DatePart("m", strDate))
        If Len(strMM) = 1 Then strMM = "0" & strMM

        strDD = CStr(DatePart("d", strDate))
        If Len(strDD) = 1 Then strDD = "0" & strDD

		 FormatDateInsert = strYYYY & "-" & strMM & "-" & strDD
       ' FormatDate = strMM & "/" & strDD & "/" & strYYYY

End Function 



Sub CreateMoreEmail()

Dim rowCount, strEmail, arrEmail


rowCount = nEmailCount


strEmail = sEmailValues


if  (strEmail <> "" ) Then
arrEmail = Split(strEmail,",")
End if


For i = 2 To rowCount
 
 
If sAction = "Edit" Then
%>
 
<tr>
<td height="19">&nbsp;</td>
<td height="19"><input type="text" name="email<%=i%>" id="email<%=i%>" size="35"  <%if UBound(arrEmail) >= 1 Then %>    value="<%=arrEmail(i-1)%>"  <%else %>   value=""  <%End If %>   style="font-family: Verdana; font-size: 8pt" maxlength="100" onkeyup="EnabledInvoiceANDCreditNoteOption();">&nbsp;</td>
</tr>  

<%

Else 
%>

 
<tr>
    <td height="19">&nbsp;</td>
    <td height="19"><input type="text" name="email<%=i%>" id="email<%=i%>" size="35" <%if i <> CInt(rowCount) Then %>     <%if UBound(arrEmail) >= 1 Then %>    value="<%=arrEmail(i-1)%>"  <%else %>   value=""  <%End If %>  <%else %> value=""  <%End If %>  style="font-family: Verdana; font-size: 8pt" maxlength="100" onkeyup="EnabledInvoiceANDCreditNoteOption();">&nbsp;</td>
</tr> 

<%
End If

Next
End sub

Function DisplayEmailEditMode(cno)

Dim strEmailList

strEmailList = ""
set object = Server.CreateObject("bakery.customer")
object.SetEnvironment(strconnection)
set listCustomerGroupEmailList = object.ListCustomerGroupEmailList(cno, 0)
arrlistCustomerGroupEmailList = listCustomerGroupEmailList("CustomerGroupEmailList")

If IsArray(arrlistCustomerGroupEmailList) then
	For j = 0 to UBound(arrlistCustomerGroupEmailList,2)
		
		if (strEmailList = "") Then        
		    strEmailList =  arrlistCustomerGroupEmailList(3,j)
		Else     			
			strEmailList = strEmailList + "," + arrlistCustomerGroupEmailList(3,j)	
			
	    End If				
	Next 	
End If

DisplayEmailEditMode = strEmailList
End Function
%>

