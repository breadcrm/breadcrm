<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function ExportToExcelFile(){
	fromdate=document.form.txtfrom.value
	todate=document.form.txtto.value
	ResellerID1=document.form.ResellerID.value
	document.location="ExportToExcelFile_mon_report_Indirect_customer_brief.asp?fromdt=" + fromdate + "&todt=" +todate + "&ResellerID=" +ResellerID1
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim recarray
dim retcol
dim fromdt, todt, i,TotalDeliveries,Totalturnover,Averageturnover,strResellerID


fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")
strResellerID=Request.form("ResellerID")
if strResellerID="" then
	strResellerID=0
end if

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

if isdate(fromdt) and isdate(todt) then
set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.Display_Indirect_customer_brief(fromdt,todt,strResellerID)
recarray = retcol("Customer")
end if

dim vResellers,object,detail
Set object = Server.CreateObject("bakery.Reseller")
object.SetEnvironment(strconnection)
set detail = object.ListResellers()
vResellers =  detail("Resellers")
set detail = Nothing
set object = Nothing

%>
<div align="center">
<center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">

 
 <tr>
    <td width="100%"><b><font size="3">&nbsp;Indirect Customer Report</font></b>
	  <form method="post" action="mon_report_Indirect_customer_brief.asp" name="form">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
      <tr>
          <td colspan="4" height="15">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
			  <tr>
				<td width="65">&nbsp;<strong>Reseller:</strong></td>
				<td>
					<select size="1" name="ResellerID" style="font-family: Verdana; font-size: 8pt">
					  <option value="0">- - - Select - - -</option>
					  <%
					  for i = 0 to ubound(vResellers,2)%>
						   <% if (vResellers(0,i)=cint(strResellerID)) then%>
								<option value="<%=vResellers(0,i)%>" selected="selected"><%=vResellers(1,i)%></option>
						   <%else%>
								<option value="<%=vResellers(0,i)%>"><%=vResellers(1,i)%></option>
						   <%end if%>
					   <%next%>
				   	</select>
				</td>
			  </tr>
			</table>
			</td>
      	</tr>
		<tr>
          <td></td>
          <td><b>From:</b></td>
          <td><b>To:</b></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
         <td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom onFocus="blur();" size="12"  name="txtfrom" value="<%=fromdt%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
				</td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=todt%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td>  <td>
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
	  </form>
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
	  <% if request("Search")<>"" then%>
      <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%" colspan="5"><b>Indirect Customer Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
		 <tr>
          <td colspan="5" align="left" height="30">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>
        <tr>
          <td width="5%"  bgcolor="#CCCCCC"><b>Customer Code&nbsp;</b></td>
          <td width="40%" bgcolor="#CCCCCC"><b>Customer Name&nbsp;</b></td>
          <td width="15%" bgcolor="#CCCCCC" align="right"><b>Total Number of Deliveries&nbsp;</b></td>
          <td width="15%" bgcolor="#CCCCCC" align="right"><b>Total Turnover&nbsp;</b></td>
          <td width="20%" bgcolor="#CCCCCC" align="right"><b>Average turnover per delivery&nbsp;</b></td>
        </tr>
		<tr>
		<%if isarray(recarray) then%>
		<% 				
		TotalDeliveries = 0
		Totalturnover = 0.00
		Averageturnover = 0.00
		
		%>
		<%for i=0 to UBound(recarray,2)%>
          <td width="5%"><b><%=recarray(0,i)%></b>&nbsp;</td>
          <td width="40%"><b><%=recarray(1,i)%></b>&nbsp;</td>
          <td width="15%" align="right" bgcolor="#CCCCCC"><b><%=recarray(2,i)%></b></td> 
          <td width="15%" align="right"><b><%if recarray(3,i)<> "" then Response.Write formatnumber(recarray(3,i),2) else Response.Write "0.00" end if%></b></td>
          <td width="20%" align="right" bgcolor="#CCCCCC"><b>        
          <%
          	if recarray(3,i)<> "" and recarray(3,i)<> "" then
           		if recarray(3,i)>recarray(2,i) then
		           Response.Write formatnumber(recarray(3,i)/recarray(2,i),2)
		        else
		         	Response.Write "0.00" 
		        end if
	        else
	         	Response.Write "0.00" 
           end if
           %>
           </b></td>
          </tr>
          <%
          if recarray(2,i) <> "" then
             TotalDeliveries = TotalDeliveries + recarray(2,i)
          end if
          if recarray(3,i)<> "" then 
						Totalturnover = Totalturnover + formatnumber(recarray(3,i),2)
				  End if
				  if recarray(4,i)<> "" then
						Averageturnover = Averageturnover +  formatnumber(recarray(4,i),2)
					End if
          %>
          <%next%>
          <tr>
           <td width="45%" colspan=2>&nbsp;<b>Grand Total</b></td>
          <td width="15%" align="right" bgcolor="#CCCCCC"><b><%=TotalDeliveries%></b></td> 
          <td width="15%" align="right"><b><%=formatNumber(Totalturnover,2)%></b></td>
          <td width="20%" align="right" bgcolor="#CCCCCC"><b>
          <%
          'code added by sv 12/05/05
          'Averageturnover = Totalturnover/TotalDeliveries
          %>
          <%'=formatNumber(Averageturnover,2)%>
          </b>&nbsp;</td>
          </tr>
          <%
          else%>
          <td colspan="5" width="100%" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td><%
          end if%>
        </tr>
		</table>
	  <%end if%>
    </td>
  </tr>
</table>
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;<br><br><br><br><br><br>
</center>
</div>
</body>
<%if IsArray(recarray) then erase recarray%>
</html>