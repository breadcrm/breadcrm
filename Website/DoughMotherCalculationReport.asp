<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title>Dough Mother Calculation Report</title>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" >
<!--#INCLUDE FILE="nav.inc" -->
<%
dim objBakery
dim recarray
dim retcol
Dim TotQuantity,TotTurnover
dim recsecondmixarray
dim arrIName
set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set retsecondmixcol = objBakery.MotherCalculationReport()    
recsecondmixarray = retsecondmixcol("MotherCalculationReport")
set detail = Nothing
set object = Nothing
%>
<p align="center"><b><font size="3">Mother Calculation Report<br></font></b></p>
<table align="center" border="0" width="60%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
	<tr height="24" style="font-weight:bold">
		<td>Delivery date : <%=NextDayinDDMMYY(NextDayinDDMMYY(day(now()) & "/" & month(now()) & "/" & year(now())))%></td>
	</tr>
</table>
<table align="center" border="0" bgcolor="#999999" width="60%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
<tr height="24" style="font-weight:bold">
    <td bgcolor="#CCCCCC">Mother Name</td>    
    <td bgcolor="#CCCCCC" width="150" align="right">Weight (Kg)</td>	    
</tr>    
   <!--mother report-->
    <%if isarray(recsecondmixarray) then%>
	    <%
	        for m=0 to UBound(recsecondmixarray,2)
	    	if (m mod 2 =0) then
				strBgColour="#FFFFFF"
			else
				strBgColour="#F3F3F3"
			end if
		%>
            <tr height="22" bgcolor="<%=strBgColour%>"> 	    
	            <td><%=recsecondmixarray(0,m)%></td>
	            <td align="right"><%=formatnumber(recsecondmixarray(1,m))%></td>	
            </tr>    	    	            
	    <%
	        next
	    %>
    <%else%>
	<tr><td bgcolor="#FFFFFF" colspan="2" height="40"><p align="center"><font color="#FF0000"><strong>There are no records found.</strong></font></p></td></tr>	
    <%end if%>    
</table>
<br><br>
</body>
<%if IsArray(recarray) then erase recarray  %>
</html>
