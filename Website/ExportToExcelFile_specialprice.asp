<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
	Server.ScriptTimeout=6000
dim objBakery
dim recarray
dim retcol
dim totpages
dim vSessionpage
dim i


vSessionpage = 1
totpages = 1

set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.SpecialPriceList(vSessionPage,0)
totpages = retcol("pagecount")
recarray = retcol("SpecialPrices")
	
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=specialprice.xls" 
%>
		  
<table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
      
        <tr>
          <td colspan="4"><b><font size="3">Report - Special prices</font><font size="2"><br>
            </font></b></td>
            <%
          if isarray(recarray) then%>
          <td><font size="2"><b>Page <%=vsessionpage%> of <%=totpages%></b></font></td><%
          else%>
          <td>&nbsp;</td><%
          end if%>
        </tr>
        <tr>
          <td bgcolor="#CCCCCC"><b>Customer&nbsp;No</b></td>
          <td bgcolor="#CCCCCC"><b>Customer&nbsp; Name</b></td>
          <td bgcolor="#CCCCCC"><b>Products</b></td>
          <td bgcolor="#CCCCCC"><b>Default price</b></td>
          <td bgcolor="#CCCCCC"><b>Regular Price</b></td>
          <td bgcolor="#CCCCCC"><b>Special Price</b></td>
          <td bgcolor="#CCCCCC"><b>Percentage discount</b></td>
        </tr>
        <%
        if isarray(recarray) then
          for i=0 to ubound(recarray,2)%>
          <%if recarray(5,i)<>recarray(6,i) then%>
          <tr>
          <td><%=recarray(0,i)%></td>
          <td><%=recarray(2,i)%></td>
          <td><%=recarray(1,i)%></td>
          <td><%=formatnumber(recarray(4,i),2)%></td>
          <td><%=formatnumber(recarray(5,i),2)%></td>
          <td>� <%=formatnumber(recarray(6,i),2)%></td>
          <td><%=formatnumber(recarray(7,i),2)%></td>
          </tr>
          <%end if%>          
          <%
          next
        else%>
        <tr>
          <td colspan="6"><b>No Records found...</b></td>
        </tr><%
        end if%>
      </table>		 