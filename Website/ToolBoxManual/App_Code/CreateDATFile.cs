﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Data;

/// <summary>
/// Summary description for CreateDATFile
/// </summary>
public class CreateDATFile
{
    const string CUSTOMER_FILENAME = "CUS.DAT";
    const string PRODUCT_FILENAME = "PRD.DAT";    

    private string orderFileName;

    public string OrderFileName
    {
        get { return orderFileName; }
        set { orderFileName = value; }
    }

    private string routeFileName;

    public string RouteFileName
    {
        get { return routeFileName; }
        set { routeFileName = value; }
    }

	public CreateDATFile()
	{
		//
		// TODO: Add constructor logic here
		//

	}
    
    public void WriteCustomerFile(string parameter)
    {
        string[] column = new string[] {"CNo", "Title", "Name1","Name2","MatchCode","barcode","street","zipcode",
            "city","Telephone","Customerblocked","Invoicecustomer","labelcustomerstore"};

        WriteFile(parameter, CUSTOMER_FILENAME, "ToolBoxCustomer", column);
    }

    public void WriteProductFile(string parameter)
    {
        string[] column = new string[] {"PNo", "PName", "MatchCode","barcode","productgroup1","productgroup2","productgroup3","wreath",
            "unit","weightapieceKg","weightapiecegrams","productblocked","basketquantity1", "basketquantity2"};

        WriteFile(parameter, PRODUCT_FILENAME, "ToolBoxProduct", column);
    }

    public void WriteOrderFile(string parameter,string Date)
    {
        string[] ColumnHeader = new string[] { "indicator", "CNo", "WaveNo", "deliverynoteno", "absolutevalue" };
        string[] ColumnDetail = new string[] { "indicator", "PNo", "Qty", "QtyDecimal", "price", "pricedecimal", "infotext" };

        GenerateOrderFileName(Date, 1);
        WriteOrder(parameter, "ToolBoxOrdHeader", "ToolBoxOrdDet", ColumnHeader, ColumnDetail);
    }

    public void WriteRouteFile(string parameter, string Date)
    {
        string[] ColumnHeader = new string[] { "indicator", "routeNo", "routename", "Waveno", "departuretime", "routeposition" };
        string[] ColumnDetail = new string[] { "indicator", "CNo", "unloadingtime", "customerposition" };

        GenerateRouteFileName(Date);
        WriteRoute(parameter, "ToolBoxRouteHeader", "ToolBoxRouteDet", ColumnHeader, ColumnDetail);
    }

    private void WriteFile(string Parameter, string FileName, string SPName, string[] Column)
    {
        string path = ConfigurationManager.AppSettings["FilePath"] + FileName;

        if (!File.Exists(path))
        {
            using (var file = File.Create(path))
            {
                file.Close();
            }
        }

        using (StreamWriter tw = File.CreateText(path))
        {
            ConnectDB obj = new ConnectDB();
            DataTable dt = obj.DataList(SPName, "'" + Parameter + "'");

            for (int index = 0; index < dt.Rows.Count; index++)
            {
                for (int numberofColum = 0; numberofColum < Column.Count(); numberofColum++)
                {
                    tw.Write(dt.Rows[index][Column[numberofColum]].ToString() + ";");
                }
                tw.WriteLine();

            }

            tw.Close();
        }
    }
    
    private void WriteOrder(string Parameter, string SPNameHeader, string SPNameDetail, string[] ColumnHeader, string[] ColumnDetail)
    {
        string path = OrderFileName;

        using (StreamWriter tw = File.CreateText(path))
        {
            ConnectDB obj = new ConnectDB();
            DataTable dtHeader = obj.DataList(SPNameHeader, "'" + Parameter + "'");

            for (int indexHeader = 0; indexHeader < dtHeader.Rows.Count; indexHeader++)
            {
                for (int numberofColum = 0; numberofColum < ColumnHeader.Count(); numberofColum++)
                {
                    tw.Write(dtHeader.Rows[indexHeader][ColumnHeader[numberofColum]].ToString() + ";");
                }
                tw.WriteLine();

                DataTable dtDetail = obj.DataList(SPNameDetail, dtHeader.Rows[indexHeader]["deliverynoteno"].ToString().Replace("\"",""));

                for (int indexDetail = 0; indexDetail < dtDetail.Rows.Count; indexDetail++)
                {
                    for (int numberofColum = 0; numberofColum < ColumnDetail.Count(); numberofColum++)
                    {
                        tw.Write(dtDetail.Rows[indexDetail][ColumnDetail[numberofColum]].ToString() + ";");
                    }
                    tw.WriteLine();
                }
            }
            tw.Close();
        }
    }

    private void WriteRoute(string Parameter, string SPNameHeader, string SPNameDetail, string[] ColumnHeader, string[] ColumnDetail)
    {
        string path = RouteFileName;

        using (StreamWriter tw = File.CreateText(path))
        {
            ConnectDB obj = new ConnectDB();
            DataTable dtHeader = obj.DataList(SPNameHeader, "'" + Parameter + "'");

            for (int indexHeader = 0; indexHeader < dtHeader.Rows.Count; indexHeader++)
            {
                for (int numberofColum = 0; numberofColum < ColumnHeader.Count(); numberofColum++)
                {
                    tw.Write(dtHeader.Rows[indexHeader][ColumnHeader[numberofColum]].ToString() + ";");
                }
                tw.WriteLine();

                DataTable dtDetail = obj.DataList(SPNameDetail, "'" + Parameter + "' , " + dtHeader.Rows[indexHeader]["routeNo"].ToString().Replace("\"", "") );

                for (int indexDetail = 0; indexDetail < dtDetail.Rows.Count; indexDetail++)
                {
                    for (int numberofColum = 0; numberofColum < ColumnDetail.Count(); numberofColum++)
                    {
                        tw.Write(dtDetail.Rows[indexDetail][ColumnDetail[numberofColum]].ToString() + ";");
                    }
                    tw.WriteLine();
                }
            }
            tw.Close();
        }
    }


    private void GenerateOrderFileName(string Date, int Count)
    {
        string path = ConfigurationManager.AppSettings["FilePath"] + Date + "O" + Count.ToString() + ".DAT";

        if (!File.Exists(path))
        {
            using (var file = File.Create(path))
            {
                OrderFileName = path;
                file.Close();
            }
        }
        else
        {
            GenerateOrderFileName(Date, ++Count);
        }
    }

    private void GenerateRouteFileName(string Date)
    {
        string path = ConfigurationManager.AppSettings["FilePath"] + Date + "RT.DAT";

        if (!File.Exists(path))
        {
            using (var file = File.Create(path))
            {
                RouteFileName = path;
                file.Close();
            }
        }
        else
        {
            RouteFileName = path;
        }
    }

    public void WriteLog(string ErrorMessage)
    {
        string path = ConfigurationManager.AppSettings["FilePath"] + "Log.txt";

        if (!File.Exists(path))
        {
            using (var file = File.Create(path))
            {
                file.Close();
            }
        }

        using (StreamWriter tw = File.CreateText(path))
        {
            tw.WriteLine(DateTime.Today.ToShortDateString() + " " + DateTime.Today.ToShortTimeString() + " - " + ErrorMessage);
            tw.Close();
        }
    }
   
   


}
