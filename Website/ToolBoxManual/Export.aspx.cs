﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblMessage.Text = "";
            LoadDates();

            DateTime TommorowDate = DateTime.Today.AddDays(1);

            ddlYear.SelectedValue = TommorowDate.Year.ToString();
            ddlMonth.SelectedValue = TommorowDate.Month < 10 ? "0" + TommorowDate.Month.ToString() : TommorowDate.Month.ToString();
            ddlDay.SelectedValue = TommorowDate.Day.ToString();
             
        }        
        
    }

    protected void LoadDates()
    {
        int numberofYears = DateTime.Today.Year - 13;
        for (int index = numberofYears; index <= DateTime.Today.Year + 1; index++)
        {
            ddlYear.Items.Add(index.ToString());
        }
        ddlYear.SelectedValue = DateTime.Today.Year.ToString();

        LoadDays(int.Parse(ddlYear.SelectedValue), int.Parse(ddlMonth.SelectedValue), ddlDay);
       
    }

    private void LoadDays(int Year,int Month,DropDownList ddlList)
    {
        int numebrofDays = DateTime.DaysInMonth(Year, Month);
        for (int index = 1; index <= numebrofDays; index++)
        {
            ddlList.Items.Add(index.ToString());
        }
    }

    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        LoadDays(int.Parse(ddlYear.SelectedValue), int.Parse(ddlMonth.SelectedValue), ddlDay);
    }

    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        LoadDays(int.Parse(ddlYear.SelectedValue), int.Parse(ddlMonth.SelectedValue), ddlDay);
    }

    protected void ddlDay_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMessage.Text = "";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)    
    {
        CreateDATFile obj = new CreateDATFile();
        try
        {
            lblMessage.Text = "";
            obj.WriteCustomerFile(ddlYear.SelectedValue.ToString() + "-" + ddlMonth.SelectedValue.ToString() + "-" + ddlDay.SelectedValue.ToString());
            obj.WriteProductFile(ddlYear.SelectedValue.ToString() + "-" + ddlMonth.SelectedValue.ToString() + "-" + ddlDay.SelectedValue.ToString());

            string Day = ddlDay.SelectedValue.ToString().Length == 1 ? "0" + ddlDay.SelectedValue.ToString() : ddlDay.SelectedValue.ToString();

            obj.WriteOrderFile(ddlYear.SelectedValue.ToString() + "-" + ddlMonth.SelectedValue.ToString() + "-" + ddlDay.SelectedValue.ToString(),
                ddlYear.SelectedValue.Substring(2, 2) + ddlMonth.SelectedValue.ToString() + Day);

            obj.WriteRouteFile(ddlYear.SelectedValue.ToString() + "-" + ddlMonth.SelectedValue.ToString() + "-" + ddlDay.SelectedValue.ToString(),
                ddlYear.SelectedValue.Substring(2, 2) + ddlMonth.SelectedValue.ToString() + Day);

            lblMessage.Text = "Files successfuly exported.";

            obj.WriteLog("Files successfuly exported.");
        }
        catch (Exception err)
        {
            obj.WriteLog(err.Message.ToString());
        }
    }


    
}
