﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Export.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

</head>
<body>
    <form id="form1" runat="server">   
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager> 
         <asp:UpdateProgress runat="server" id="PageUpdateProgress" AssociatedUpdatePanelID="Panel">
            <ProgressTemplate>
                <span style="font-family: verdana; font-size: 8pt;">Please wait...</span>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel runat="server" id="Panel">
            <ContentTemplate>       
         <div><asp:Label ID="lblMessage" runat="server" Font-Names="Verdana" Font-Size="8pt" 
                 ForeColor="Green"></asp:Label></div>  
                <table style="font-family: verdana; font-size: 8pt; height: 10px;">    
                  <tr>                                              
                        <td align="left">
                            Date:</td>
                        <td align="right">
                            <asp:DropDownList ID="ddlDay" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="ddlDay_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="right">
                            <asp:DropDownList ID="ddlMonth" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="ddlMonth_SelectedIndexChanged">
                                <asp:ListItem Value="01">January</asp:ListItem>
                                <asp:ListItem Value="02">February</asp:ListItem>
                                <asp:ListItem Value="03">March</asp:ListItem>
                                <asp:ListItem Value="04">April</asp:ListItem>
                                <asp:ListItem Value="05">May</asp:ListItem>
                                <asp:ListItem Value="06">June</asp:ListItem>
                                <asp:ListItem Value="07">July</asp:ListItem>
                                <asp:ListItem Value="08">August</asp:ListItem>
                                <asp:ListItem Value="09">September</asp:ListItem>
                                <asp:ListItem Value="10">October</asp:ListItem>
                                <asp:ListItem Value="11">November</asp:ListItem>
                                <asp:ListItem Value="12">December</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="right">
                            <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="ddlYear_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                       
                    </tr>
                  <tr>                                              
                        <td colspan="4" style="height: 10px">
                            &nbsp;</td>
                       
                    </tr>
                  <tr>                                              
                        <td>
                            &nbsp;</td>
                        <td align="right" colspan="3">
                            <asp:Button ID="btnSubmit" runat="server" Font-Names="Verdana" Font-Size="8pt" 
                                onclick="btnSubmit_Click" Text="Generate Files" />
                        </td>
                       
                    </tr>
        </table>       
               
        </ContentTemplate>
       <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click"  />
        </Triggers> 
        </asp:UpdatePanel>

    </div>
    </form>
</body>
</html>
