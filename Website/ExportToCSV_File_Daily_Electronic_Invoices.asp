<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/ExportToCSV_File_Daily_Electronic_Invoices_Path.inc" -->

<%
deldate= request("deldate")
if deldate= "" then response.redirect "LoginInternalPage.asp"
adeldate=split(deldate,"/")
if isarray(adeldate) then
	strDateText=adeldate(0) & Monthname(adeldate(1)) & adeldate(2)
end if
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Daily Electronic Invoices - CSV</title>
<SCRIPT language=JavaScript>
function clearPreloadPage() 
{ 
  if (document.getElementById){
    document.getElementById('prepage').style.visibility='hidden';
  }
  else{
  if (document.layers){ //NS
    document.prepage.visibility = 'hidden';
  }
  else { //IE
    document.all.prepage.style.visibility = 'hidden';
  }
  }
} //end function
</SCRIPT>
</head>
<body onLoad="clearPreloadPage()" topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<DIV id=prepage style="FONT-SIZE: 11px; FONT-FAMILY: arial; TOP: 10px; HEIGHT: 20px; TEXT-ALIGN: center">
<TABLE width="100%" align="center">
  
  <TR>
    <TD align="center" c>
	<!--Loading ....-->
     <img src="images/LoadingAnimation.gif" width="201" height="33"><br>
	 <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><b>Please wait...</b></font>
	 </TD>
	 </TR>
	 </TABLE>
	 </DIV>

<%
 	Dim fso	
	Dim oFolder
	Set fso = Server.CreateObject("Scripting.FileSystemObject")		
		If fso.FolderExists(strFolder) Then
			Set oFolder = fso.GetFolder(strFolder)
			On Error Resume Next
			For Each oFile In oFolder.Files
				oFile.Delete True 'setting force to true
			Next
			DeleteAllFiles = oFolder.Files.Count = 0
			Set oFolder = Nothing	
		End If
	Set fso = Nothing	
	
	set objBakery = server.CreateObject("Bakery.MgmReports")
	objBakery.SetEnvironment(strconnection)
	set retcol = objBakery.InvoiceExportToCSVOrder(deldate)
	vRecArray = retcol("InvoiceExportToCSVOrder")
	set retcol=nothing
  	Set fso = server.CreateObject("Scripting.FileSystemObject")
	
	if isarray(vRecArray) then 
  			for intI = 0 to ubound(vRecArray,2)
			strGNo=vRecArray(0, intI)
			'strCusNo=vRecArray(1, intI)
			set retco2 = objBakery.InvoiceDetailsExportToCSV(strGNo,deldate)
			vRecArray1 = retco2("InvoiceDetails")
  			if isarray(vRecArray1) then 
				file = strFolder & strGNo & "_" & strDateText & "_Invoice" & ".csv"
				'If Not fso.FileExists (file) Then
				Set textFile = fso.CreateTextFile (file,True)
				
				response.Write("created2" & textFile)
				textFile.WriteLine "Invoice No, Type, Week Ending Date, Co-op Store code, Supplier Product Code, Quantity, Delivery Note No"
				for intJ = 0 to ubound(vRecArray1, 2)
					textFile.WriteLine vRecArray1(0, intJ) & ",Invoice," & deldate & "," & vRecArray1(3, intJ) & "," & vRecArray1(2, intJ) & "," & vRecArray1(1, intJ) & "," & vRecArray1(0, intJ)
				next
				textFile.close
			end if
			set retco2=nothing
		next
  	end if
	deldate1=deldate
	deldate=PreviousDayinDDMMYY(deldate)
	set retcol = objBakery.CreditExportToCSVOrder(deldate)
	vRecArray2 = retcol("CreditExportToCSVOrder")
	set retcol=nothing
    'strDateText=Day(deldate1) & Monthname(month(deldate1)) & year(deldate1)
	if isarray(vRecArray2) then 
  		
		for intI = 0 to ubound(vRecArray2,2)
		
			strGNo=vRecArray2(0, intI)
			
			set retco2 = objBakery.CreditDetailsExportToCSV(strGNo,deldate)
			vRecArray3 = retco2("CreditDetails")
  			if isarray(vRecArray3) then 
				file = strFolder & strGNo & "_" & strDateText & "_Credit" & ".csv"
				'If Not fso.FileExists (file) Then
				Set textFile = fso.CreateTextFile (file,True)
				textFile.WriteLine "Invoice No, Type, Week Ending Date, Co-op Store code, Supplier Product Code, Quantity, Delivery Note No"
				for intJ = 0 to ubound(vRecArray3, 2)
					textFile.WriteLine vRecArray3(4, intJ) & ",Credit," & deldate1 & "," & vRecArray3(3, intJ) & "," & vRecArray3(2, intJ) & "," & vRecArray3(1, intJ) & "," & vRecArray3(4, intJ) 
				next
				textFile.close
			end if
			set retco2=nothing
		next
  	end if
	
	Set fso = Nothing
	set objBakery=nothing	
%>
<br><br>
<h3 align="center">Daily Electronic Invoices export to CSV file</h3>
<br>
<h4 align="center"><font color="#006633">Successfully Exported.</font></h4> 
<p align="center"><input type="button" value="Close" onClick="window.close()"></p>
<SCRIPT>
    if (document.getElementById) {
          prepage.style.display="none";
    } 
</SCRIPT>

<SCRIPT>
    if (document.getElementById) {
          prepage.style.display="none";
    } 
</SCRIPT>
</body>
</html>
