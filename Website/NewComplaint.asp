<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->

<%
vPageSize = 50
vpagecount = Request.Form("pagecount") 

select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(vpagecount) then	
			session("Currentpage")  = session("Currentpage")+1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage")-1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select
vSessionpage = session("Currentpage")

strsearchby=replace(Request.Form("searchby"),"'","''")
strsearchtext=replace(Request.Form("searchtext"),"'","''")

Set obj = server.CreateObject("bakery.credit")
obj.SetEnvironment(strconnection)
if request.form("delete")="yes" then
	vid=request.form("id")
	delete = obj.DeleteNewComplaint(vid)
end if
Set objNewComplaints = obj.DisplayNewComplaints(strsearchby,strsearchtext,vsessionpage,vPageSize)

arNewComplaints = objNewComplaints("NewComplaints")
vpagecount =   objNewComplaints("Pagecount") 
vRecordCount = objNewComplaints("Recordcount")

Set objNewComplaints = Nothing
Set obj = Nothing
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script LANGUAGE="javascript" src="includes/script.js"></script>
<script Language="JavaScript">
<!--
function validate(frm){
	if(confirm("Would you like to delete '" + frm.name.value + "'?")){
		frm.submit();		
	}
}
//-->
</Script>
<SCRIPT LANGUAGE="Javascript">
<!--
function SearchValidation(){
		if((CheckEmpty(document.frmForm.sname.value) == "true") && (CheckEmpty(document.frmForm.scode.value) == "true") && (CheckEmpty(document.frmForm.iname.value) == "true")){ 
			alert("Please enter a value");
			return false;
		}
		return true;
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="950" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">List of Complaints<br>
      &nbsp;</font></b>
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" align="center" bordercolor="#111111">
		<form method="post" action="NewComplaint.asp" name="frmForm">      
         <tr height="40">
          <td>
		  <select name="searchby">
		  	<option value="">Select</option>
			<option value="cusno" <% if (strsearchby="cusno") then %> selected="selected" <% end if %>>Customer Code</option>
			<option value="invoiceno" <% if (strsearchby="invoiceno") then %> selected="selected" <% end if %>>Invoice No</option>
			<option value="complaint" <% if (strsearchby="complaint") then %> selected="selected" <% end if %>>Complaint</option>
			<option value="pname" <% if (strsearchby="pname") then %> selected="selected" <% end if %>>Product Name</option>
			<option value="pno" <% if (strsearchby="pno") then %> selected="selected" <% end if %>>Product Code</option>
		  </select>
		  </td>
          <td><input type="text" name="searchtext" value="<%=strsearchtext%>" size="50" maxlength="300" style="font-family: Verdana; font-size: 8pt"></td>
          <td>
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt" onClick="return SearchValidation();"></td>
        </tr>
		</form>
		</table>
		 <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#111111">
		<tr>
			<form method="POST" action="ComplaintAdd.asp" STYLE="margin: 0px; padding: 0px;">
          <td align="left" height="30">
		  <input type="submit" value="Add New Complaint" name="B1" style="font-family: Verdana; font-size: 8pt; width:135px">
          </td>
		  </form>
		</tr>
      </table>

       <table border="0" bgcolor="#999999" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">

	
			<tr bgcolor="#FFFFFF" height="25">
	 			  <td align = "left" colspan="4"><b>Total no of Complaints : <%=vRecordcount%></b></td>
				  <td align="right" colspan="4"><b>Page <%=vSessionpage%> of <%=vpagecount%></b></td>          
				</tr>
	
  
      
        <tr bgcolor="#CCCCCC">
		  <td bgcolor="#CCCCCC" width="15"><b>No</b></td>
		  <td width="90" bgcolor="#CCCCCC"><nobr><b>Customer Code</b></nobr></td>
		  <td width="65" bgcolor="#CCCCCC"><nobr><b>Invoice No</b></nobr></td>
		  <td width="80" bgcolor="#CCCCCC"><nobr><b>Invoice Date</b></nobr></td>
		  <td width="100" bgcolor="#CCCCCC"><nobr><b>Complaint Date</b></nobr></td>
		   <td width="60" bgcolor="#CCCCCC"><nobr><b>Product Name</b></nobr></td>
          <td bgcolor="#CCCCCC"><b>Complaint</b></td>          
          <td bgcolor="#CCCCCC" align="center" width="210"><b>Action</b></td>
        </tr>
<% 
	if IsArray(arNewComplaints) Then
		For i = 0 To ubound(arNewComplaints,2)
		if (i mod 2 =0) then
			strBgColour="#FFFFFF"
		else
			strBgColour="#F3F3F3"
		end if
%>         
        <tr bgcolor="<%=strBgColour%>">
		  <td valign="top"><%=(vPageSize*(vsessionpage-1))+i+1%>.&nbsp;</td>
		  <td valign="top"><%=arNewComplaints(1,i)%></td>
		  <td valign="top"><%=arNewComplaints(2,i)%></td>
		  <td valign="top"><%=arNewComplaints(3,i)%></td>
		  <td valign="top"><%=arNewComplaints(4,i)%></td>
		  <td valign="top"><%=arNewComplaints(6,i)%></td>
          <td valign="top"><%=Replace(arNewComplaints(5,i), vbCrLf,"<br>")%></td>
		  <td align="center" valign="top">
		  <table cellpadding="2" border="0" cellspacing="0" align="center">
		  <tr>
          <%if session("UserType") <> "N" and strconnection<>"vdbArchiveConn" Then%>
		   <form method="POST" action="ComplaintView.asp" STYLE="margin: 0px; padding: 0px;">
          <td align="center">
		  <input type = "hidden" name="id" value = "<%=arNewComplaints(0,i)%>">
		  <input type="submit" value="View" name="B1" style="font-family: Verdana; font-size: 8pt; width:70px">
          </td>
		  </form>
		  
		  <form method="POST" action="ComplaintAdd.asp" STYLE="margin: 0px; padding: 0px;">
          <td align="center">
		  <input type = "hidden" name="id" value = "<%=arNewComplaints(0,i)%>">
		  <input type="submit" value="Edit" name="B1" style="font-family: Verdana; font-size: 8pt; width:70px">
          </td>
		  </form>
          <form method="POST" STYLE="margin: 0px; padding: 0px;">
		  <%if trconnection<>"vdbArchiveConn" Then%>
		  <td align="center">
		  <input type = "hidden" name="id" value = "<%=arNewComplaints(0,i)%>">
          <input type = "hidden" name="delete" value = "yes">
		  <input type = "hidden" name="name" value = "<%=arNewComplaints(1,i)%>">
		  <input type="button" onClick="validate(this.form);" value="Delete" name="B1" style="font-family: Verdana; font-size: 8pt; width:70px">
          </td>
		  <%
		  end if
		  %>
		  </form>
		  </tr>
		  </table>
		  </td>
			<%end if%>
        </tr>
<%
		Next
%>

<table border="0" cellspacing="1" cellpadding="2" align="center">
 <tr>
 	<td height="10"></td>
 </tr>
  <tr> 
   
	<% if vSessionpage <> 1 and vsessionpage <> 0 then %>
	 <td width="120" align="center">
		<form action="NewComplaint.asp" method="post" name="frmFirst">
	        <input type="submit" name="Submit2" value="First Page">
			<input type="hidden" name="Direction" value="First">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">
			<input type="hidden" name="searchby" value="<%=strsearchby%>">
			<input type="hidden" name="searchtext" value="<%=strsearchtext%>">
		</form>
		 </td>
	<% End if %>
   
   
	<% if clng(vSessionpage) > 1 then %>
	 <td width="120" align="center">
		<form action="NewComplaint.asp" method="post" name="frmPrevious">
	        <input type="submit" name="Submit5" value="Previous Page">
			<input type="hidden" name="Direction" value="Previous">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">			
			<input type="hidden" name="searchby" value="<%=strsearchby%>">
			<input type="hidden" name="searchtext" value="<%=strsearchtext%>">
		</form>
		 </td>
	<% End if %>
   
    
	<% if clng(vSessionpage) < vPagecount then %>
	<td width="120" align="center">
		<form action="NewComplaint.asp" method="post" name="frmNext">
	        <input type="submit" name="Submit4" value="Next Page">
			<input type="hidden" name="Direction" value="Next">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">			
			<input type="hidden" name="searchby" value="<%=strsearchby%>">
			<input type="hidden" name="searchtext" value="<%=strsearchtext%>">
		</form>
	 </td>
	<% End if %>
   
   
	<% if clng(vSessionpage) <> vPagecount and vPagecount > 0  then %>
	 <td width="120" align="center">
		<form action="NewComplaint.asp" method="post" name="frmLast">
	        <input type="submit" name="Submit3" value="Last Page">
			<input type="hidden" name="Direction" value="Last">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">	
			<input type="hidden" name="searchby" value="<%=strsearchby%>">
			<input type="hidden" name="searchtext" value="<%=strsearchtext%>">
		</form>
		 </td>
	<% End if %>
   
  </tr>
</table>
<%
	Else
%>

        <tr>
          <td colspan="8" bgcolor="#FFFFFF" align="center" height="40"><font color="#FF0000"><strong>Sorry no items found.</strong></font></td>
        </tr>
<%
	End if
%>
      </table>
    </td>
  </tr>
</table>
<br><br>

  </center>
</div>
</body>
</html>
<%
If IsArray(arNewComplaints) Then Erase arNewComplaints
%>