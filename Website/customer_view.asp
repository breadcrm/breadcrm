<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%

Dim Obj,ObjCustomer,objCustomerSettings,ObjCustomerOnlineDetails
Dim arCustomer,arSpecialProductPrice,arStandingOrder,arCustomerSettings,arCustomerOnlineDetails
Dim vCustNo,vpno,delete,strrowclass
Dim MonTotalOrderQty,TueTotalOrderQty,WedTotalOrderQty,ThuTotalOrderQty,FriTotalOrderQty,SatTotalOrderQty,SunTotalOrderQty
Dim MonTotalOrderPrice,TueTotalOrderPrice,WedTotalOrderPrice,ThuTotalOrderPrice,FriTotalOrderPrice,SatTotalOrderPrice,SunTotalOrderPrice

Dim i,k
Dim prno,vWno,strBgColour
if Request.Form("CustNo") <> "" then
	vCustNo	= Request.Form("CustNo")
else
	vCustNo	= Request.querystring("CustNo")
end if

Set Obj = CreateObject("Bakery.Customer")
Obj.SetEnvironment(strconnection)
if request.form("delete")="yes" then
	vpno=request.form("pno")
	delete = obj.DeleteCustomerStandingOrder(vCustNo,vpno)
	LogAction "Std Order Product Deleted", "PNo: " & vpno , vCustNo
end if

if request.form("clear")="yes" then
	vpno=request.form("pno")
	delete = obj.ClearCustomerStandingOrderQty(vCustNo,vpno)
	LogAction "Std Order Edited", vpno & " Cleared" , vCustNo
end if

if request.form("clearall")="yes" then
	delete = obj.ClearAllCustomerStandingOrderQty(vCustNo)
	LogAction "Std Order Edited", "Std Order Cleared" , vCustNo
end if

Set ObjCustomer = obj.Display_CustomerDetail(vCustNo)
arSpecialProductPrice = ObjCustomer("SpecialPrice")
arCustomer = ObjCustomer("Customer")
arStandingOrder = ObjCustomer("StandingOrder")
stop
Set objCustomerSettings = obj.GetetCustomerStatementSettings(vCustNo)
arCustomerSettings = objCustomerSettings("CustomerStatement")

Set ObjCustomerOnlineDetails = obj.Display_OnlineOrderingDetailsByCNo(vCustNo)
arCustomerOnlineDetails = ObjCustomerOnlineDetails("OnlineDetails")


Dim listCustomerGroupEmailList, arrlistCustomerGroupEmailList,strEmail,j 
set listCustomerGroupEmailList = obj.ListCustomerGroupEmailList(vCustNo, 0)
arrlistCustomerGroupEmailList = listCustomerGroupEmailList("CustomerGroupEmailList")
	stop
	
	 if IsArray(arrlistCustomerGroupEmailList) Then
	For j = 0 to UBound(arrlistCustomerGroupEmailList,2)
		
	if (strEmail = "") Then        
		    strEmail =  arrlistCustomerGroupEmailList(3,j)
		Else     			
			strEmail = strEmail + "<br>" + arrlistCustomerGroupEmailList(3,j)	
			
	    End If				
	Next 
    End if

Set Obj = Nothing
Set ObjCustomer = Nothing
%>


<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>

<script Language="JavaScript">
<!--
function validate(frm){
	if(confirm("Would you like to delete " + frm.pname.value + " ?")){
		frm.submit();		
	}
}

function validateclear(frm){
	if(confirm("Would you like to clear " + frm.pname.value + " ?")){
		frm.submit();		
	}
}

function validateclearall(frm){
	if(confirm("Are you sure you want to clear all typical order?\nThis is not reversible.")){
		frm.submit();		
	}
}


//-->
</Script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<%if IsArray(arCustomer) Then%> 
<div align="center">
  <center>       
		<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		  <tr>
		    <td width="100%"><br> <b><font size="3">View customer&nbsp;&nbsp;&nbsp;&nbsp;<%if session("UserType") <> "N" Then%> <A HREF="customer_confirmed.asp?CustNo=<%=arCustomer(0,0)%>">View Confirmed Orders</A>&nbsp;&nbsp;&nbsp;&nbsp;<A HREF="javascript:NewWindow('customer_product_updates.asp?CustNo=<%=arCustomer(0,0)%>','Quantity','450','250','yes','center');">Delete daily quantity for specific delivery</A><%end if%>
				<br> 
		      &nbsp;<br>
		      <%if delete = "Ok" then%>
		      <%Response.Write "<br><font size=""3"" color=""#FF0000""><b>Product deleted sucessfully</b></font><br><br>"%>
		      <%end if%>
		      </font></b>
		      
		      <table border="0" width="100%" cellspacing="0" cellpadding="2">
		        <tr>
		          <td valign="top">
                <table border="0" width="100%" cellspacing="0" cellpadding="2">
		        <tr>
		          <td valign="top">  
          		<table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt;" width="90%">
		        <%
				strResellerID=arCustomer(34,0)
				if (arCustomer(34,0)<>"" and arCustomer(34,0)<>"0") then
				%>
				<tr style="display: none;">
		          <td><font size="2">Reseller Name:</font>&nbsp;</td>
		          <td><font size="2"><%=arCustomer(35,0)%></font>&nbsp;</td>
		      	</tr>
				<%end if%>
				<tr>
		          <td width="200"><b><font size="2">Customer Code</font>:&nbsp;</b></td>
		          <td><b><font size="2"><%=arCustomer(0,0)%></font>&nbsp;</b></td>
		      	</tr>
		        <tr>
		          <td><b><font size="2">Name:</font></b></td>
		          <td><b><font size="2"><%=arCustomer(1,0)%></font>&nbsp;</b></td>
		        </tr>
		        <tr>
		          <td><font size="2">Nick Name:</font></td>
		          <td><%=arCustomer(27,0)%>&nbsp;</td>
		        </tr>
		        <tr>
		          <td><b>Old Code:</b></td>
		          <td><%=arCustomer(2,0)%></td>
		        </tr>
		        
		        <tr>
		          <td valign="top">Address:</td>
		          <td valign="top"><%=arCustomer(3,0)%>
				  	<%
				  	if (arCustomer(4,0)<>"") then
				  		response.write(arCustomer(4,0))
					end if
					%>	
				  </td>
		        </tr>
		        
		        <tr>
		          <td>Town:</td>
		          <td><%=arCustomer(5,0)%></td>
		        </tr>
		        <tr>
		          <td>Postcode:</td>
		          <td><%=arCustomer(6,0)%></td>
		        </tr>
		        <tr>
		          <td>Telephone:</td>
		          <td><%=arCustomer(7,0)%></td>
		        </tr>
		        <tr>
		          <td>Email for Accounting:</td>
		          <td><%=arCustomerSettings(3,0)%></td>
		        </tr>
		        <tr>
		          <td>Fax:</td>
		          <td><%=arCustomer(28,0)%></td>
		        </tr>
				<tr>
		          <td>Sales Person: </td>
		          <td><%=arCustomer(30,0)%></td>
		        </tr>
				<tr>
		          <td>Account Manager: </td>
		          <td><%=arCustomer(54,0)%></td>
		        </tr>
				<tr>
		          <td>Priceless Invoice/Credit:</td>
		          <td><%if (arCustomer(33,0)="1") then%>Yes<%else%>No<%end if%></td>
		        </tr>
				<tr>
		          <td>Daily Automated Invoice:</td>
		          <td><%if (arCustomer(39,0)="1") then%>Yes<%else%>No<%end if%></td>
		        </tr>
				  <tr>
		          	<td>Daily Automated Credit:</td>
		          	<td><%if (arCustomer(40,0)="1") then%>Yes<%else%>No<%end if%></td>
		          </tr>
		       	  <tr style="display:none;">
		          	<td>Send GE CSV File:</td>
		          	<td><%if (arCustomer(49,0)="1") then%>Yes<%else%>No<%end if%></td>
		          </tr>
		           <tr>
		          	<td valign="top">Email for Electronic Invoice:</td>
		          	<td><%=strEmail%></td>
		          </tr>			          
		          <tr style="display:none;">
		          	<td>Packing Sheet Type:</td>
		          	<td><%if (arCustomer(50,0)="1") then%>Type 1<%else%>Type 2<%end if%></td>
		          </tr>
		          <tr>
		          	<td>Delivery Time:</td>
		          	<td><%=arCustomer(53,0)%></td>
		          </tr>		          
		          <tr style="display:none;">
		          	<td>Include in Central Kitchen Report:</td>
		          	<td><%if (arCustomer(51,0)= True) then%>Yes<%else%>No<%end if%></td>
		          </tr>
		          <tr style="display:none;">
		          	<td>Include in Whole Sale Central Kitchen Report:</td>
		          	<td><%if (arCustomer(52,0)= True) then%>Yes<%else%>No<%end if%></td>
		          </tr>
				  <tr>
		          	<td>Customer Category:</td>
		          	<td><%=arCustomer(56,0)%></td>		
 				  </tr>
				  <!--
				  <tr>
		          	<td>Tick Boxes:</td>
		          	<td><%=arCustomer(61,0)%></td>		
 				  </tr>
				  <tr>
		          	<td>VC Customer:</td>
		          	<td><%=arCustomer(62,0)%></td>		
 				  </tr>
				  <tr>
		          	<td>SVC Customer:</td>
		          	<td><%=arCustomer(63,0)%></td>		
 				  </tr>
				  -->
				</table>
				</td>
			  <td>
			  <iframe frameborder="0" scrolling="auto" src="AmendOrderMessages.asp?CNo=<%=arCustomer(0,0)%>" width="570px" height="360px;"></iframe><br>
			  
			   <iframe frameborder="0" scrolling="auto" src="CustomerUploadFiles.asp?CNo=<%=arCustomer(0,0)%>" width="570px" height="150px;"></iframe>
			  
			  </td>
			  </tr>
			  </table>
			  <br>
				<table border="0" cellspacing="1" cellpadding="2" style="font-family: Verdana; font-size: 8pt;" width="39%">
		        <tr>
		          <td width="120"><b></b></td>
		          <td width="200"><b>Name</b></td>
		          <td width="150"><b>Phone</b></td>
		        </tr>
		        <tr>
		          <td>Day to Day contact</td>
		          <td><%=arCustomer(12,0)%></td>
		          <td><%=arCustomer(13,0)%></td>
		        </tr>
		        <tr>
		          <td>Decision Maker</td>
		          <td><%=arCustomer(14,0)%></td>
		          <td><%=arCustomer(15,0)%></td>
		        </tr>
		        
		      </table>
		      <br>
		      </td>
		        </tr>
		      </table>
		      
		      

		    </td>
		  </tr>
		</table>
	

<table border="0" cellspacing="1" cellpadding="3" style="font-family: Verdana; font-size: 8pt;" width="90%">  
 <tr>
	  <td width="15%">Type</td>
	  <td width="35%"><%=arCustomer(8,0)%>&nbsp;</td>
	  <td width="10%"><b>Van Details</b></td>
	  <td  width="40%"></td>
  </tr>
  <tr>
	  <td>Start Date</td>
	  <td><%=arCustomer(9,0)%>&nbsp;</td>
	  <td>Monday</td>
	  <td><%=arCustomer(42,0)%>&nbsp;</td>
  </tr>
  <tr style="display:none;">
	  <td>Original customer of</td>
	  <td><%=arCustomer(10,0)%>&nbsp;</td>
	  <td>Tuesday</td>
	  <td><%=arCustomer(43,0)%>&nbsp;</td>
  </tr>
  <tr>
	  <td>Payment term</td>
	  <td><%=arCustomer(11,0)%>&nbsp;</td>
	  <td>Wednesday</td>
	  <td><%=arCustomer(44,0)%>&nbsp;</td>
  </tr>
  <tr style="display:none;">
	  <td>Pricing Policy</td>
	  <td><%=arCustomer(23,0)%>
	  <% if arCustomer(23,0)="F" then%>, Factor - <%=arCustomer(32,0)%> <%end if%>
	  &nbsp;</td>
	  <td>Thursday</td>
	  <td><%=arCustomer(45,0)%>&nbsp;</td>
  </tr>
  <tr>
	  <td>Status</td>
	  <td>
	  	<%
		if Trim(arCustomer(22,0)) = "A" Then
			Response.Write "Active"
		Elseif Trim(arCustomer(22,0)) = "S" Then
			Response.Write "Stopped"
		Elseif Trim(arCustomer(22,0)) = "D" Then
			Response.Write "Deleted"	
		End if
		%>
		</td>
	  <td>Friday</td>
	  <td><%=arCustomer(46,0)%>&nbsp;</td>
  </tr>  
  <tr>
	  <td>No of Invoices</td>
	  <td><%=arCustomer(29,0)%>&nbsp;&nbsp;</td>
	  <td>Saturday</td>
	  <td><%=arCustomer(47,0)%>&nbsp;</td>
  </tr>
  <tr>
	  <td>Customer Group</td>
	  <td>
	  <%
	  if Trim(arCustomer(24,0)) <> "" Then
			Response.Write "Yes"
	  Else
			Response.Write "No"
	  End if%>&nbsp;
	  </td>
	  <td>Sunday</td>
	  <td><%=arCustomer(48,0)%>&nbsp;</td>
  </tr>
  <tr>
	  <td>If yes, which one</td>
	  <td><%=arCustomer(24,0)%>&nbsp;</td>	
	  <td colspan="2"></td>
  </tr>
  <tr >
	  <td>Sub Group </td>
	  <td><%=arCustomer(37,0)%></td>
	  <td colspan="2"></td>
  </tr>
  <tr>
	<td>Statement Required </td>
	<td>
	<%
  	if Trim(arCustomerSettings(2,0)) = "True" Then
		Response.Write "Yes"
  	Else
		Response.Write "No"
 	End if 
  	%>
	</td>
	<td colspan="2"></td>
  </tr>
 	<tr>
	<td>Recurrence period</td>
	<td>
	<%
  	if Trim(arCustomerSettings(4,0)) = "1" Then
		Response.Write "Weekly - Start Day  "  + WeekdayName(weekday(CDate(arCustomerSettings(5,0))))
  	Elseif Trim(arCustomerSettings(4,0)) = "2" Then
		Response.Write "Fortnight - Start Date  " + FormatDate(CStr(arCustomerSettings(5,0)))
  	Elseif Trim(arCustomerSettings(4,0)) = "3" Then
		Response.Write "Monthly  -  Start Date  " + FormatDate(CStr(arCustomerSettings(5,0)))
  	End if  
  	%>
	</td>
	<td colspan="2"></td>
  </tr>
  <tr>
	<td>Statement to be sent</td>
	<td>
	<%
  	if Trim(arCustomerSettings(6,0)) = "True" Then
		Response.Write "Group"
  	Else
		Response.Write "Own Account"
 	End if  
  	%>
	</td>
	<td colspan="2"></td>
  </tr>
  <tr>
	<td>Purchase Order</td>
	<td>
	<%
	  if Trim(arCustomer(20,0)) = "Y" Then
		Response.Write "Yes"
	  Else
		Response.Write "No"
	  End if	  
	%>&nbsp;
	</td>
	<td colspan="2"></td>
</tr>
  <tr >
		  <td valign="top">Standing Order</td>
<td valign="top"><%
if Trim(arCustomer(25,0)) = "Y" Then
	Response.Write "Yes"
Else
	Response.Write "No"
end if%>&nbsp;				 </td>
<td colspan="2"></td>
  </tr>
  <tr >
	  <td>Comment</td>
	  <td><%=arCustomer(31,0)%></td>
	  <td colspan="2"></td>
  </tr>
  <tr >
  	<td>Customer Notes</td>
  	<td><%=arCustomer(36,0)%></td>	
	<td colspan="2"></td>	
  </tr>
  <%if IsArray(arCustomer) Then%> 
  <tr >
	  <td>Online Ordering Group Name</td>
	  <td><%=arCustomerOnlineDetails(2,0)%></td>
	  <td colspan="2"></td>
  </tr>
  <tr >
	  <td>Online Ordering User Name</td>
	  <td><%=arCustomerOnlineDetails(0,0)%></td>
	 <td colspan="2"></td>
  </tr>
  <tr >
	  <td>Online Ordering Password</td>
	  <td><%=arCustomerOnlineDetails(1,0)%></td>
	  <td colspan="2"></td>
  </tr>
  <tr >
	  <td>Minimum Order Amount</td>
	  <td>
	  <%
	  if (arCustomer(58,0)<>"") then
	  %>
	  &pound;<%=FormatNumber(arCustomer(58,0))%>
	  <%
	  end if
	  %>				  
	  </td>
	  <td colspan="2"></td>
  </tr>
  <tr >
	  <td>Group Minimum Order</td>
	  <td>
	  <%
	  if (arCustomer(59,0)<>"") then
	  %>
	  &pound;<%=FormatNumber(arCustomer(59,0))%>
	  <%
	  end if
	  %>				  
	  </td>
	 <td colspan="2"></td>
  </tr>
  <tr >
	  <td>Delivery Charge Option</td>
	  <td><%=arCustomer(60,0)%></td>
	  <td colspan="2"></td>
  </tr>
  <%End if %>
</table>
  </center>
</div>

<%End if%>
<br>
<div align="center">

  <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
  <form method="post" action="ExportToExcelFileStandingOrders.asp">
  <input type="hidden" name="CustNo" value="<%=vCustNo%>">
  <tr>
  <td align="right" height="30"><input type="submit" value="Export to Excel File" name="ExporttoExcelFile" id="ExporttoExcelFile" style="font-family: Verdana; font-size: 8pt; width:130px"></td>
  </tr>
  </form>
  </table>
    <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" >
        <tr class="pagehead">
          <td colspan="10"><b><font size="2">Typical Order</font></b></td>
        </tr>
        <tr class="tablehead">
          <td bgcolor="#CCCCCC"><b>Action</b></td>
          <td bgcolor="#CCCCCC"><b>Product code</b></td>
          <td bgcolor="#CCCCCC"><b>Product name</b></td>


          <td bgcolor="#CCCCCC" colspan="7">
            <p align="center"><b>Delivery Days</b></td>
        </tr>
        <tr class="tablehead">
          <td colspan = "3" bgcolor="#CCCCCC"></td>
          <td bgcolor="#CCCCCC" align="center"><b>Mon</b></td>
          <td bgcolor="#E1E1E1" align="center"><b>Tue</b></td>
          <td bgcolor="#CCCCCC" align="center"><b>Wed</b></td>
          <td bgcolor="#E1E1E1" align="center"><b>Thu</b></td>
          <td bgcolor="#CCCCCC" align="center"><b>Fri</b></td>
          <td bgcolor="#E1E1E1" align="center"><b>Sat</b></td>
          <td bgcolor="#CCCCCC" align="center"><b>Sun</b></td>
        </tr><%        
        MonTotalOrderQty=0
		TueTotalOrderQty=0
		WedTotalOrderQty=0
		ThuTotalOrderQty=0
		FriTotalOrderQty=0
		SatTotalOrderQty=0
		SunTotalOrderQty=0
		
		MonTotalOrderPrice=0
		TueTotalOrderPrice=0
		WedTotalOrderPrice=0
		ThuTotalOrderPrice=0
		FriTotalOrderPrice=0
		SatTotalOrderPrice=0
		SunTotalOrderPrice=0
		
        if IsArray(arStandingOrder) Then
					i = 0					
					Do While i<=UBound(arStandingOrder,2)
					if (i mod 2 =0) then
						strrowclass="tablealtrow"
					else
						strrowclass="tablerow"
					end if
					%>
						<TR class="<%=strrowclass%>">
							<td>
									<table border="0" width="92" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">								
									<tr>
									<td>&nbsp;
									</td>
									<td>&nbsp;
									</td>
									</tr>
									<tr>
									<td width="56">
									<%if session("UserType") <> "N" Then%>
									<form method="POST">
  							        <input type = "hidden" name="CustNo" value = "<%=vCustNo%>">
  							        <input type = "hidden" name="pno" value = "<%=arStandingOrder(0,i)%>">
  							        <input type = "hidden" name="pname" value = "<%=arStandingOrder(6,i)%>">							        
  							        <input type = "hidden" name="clear" value = "yes">
  							        <input type="button" onClick="validateclear(this.form);" value="Clear" name="B2" style="font-family: Verdana; font-size: 8pt; width:50px">
							        </form>
									<%end if%>
									</td>
									<td width="5">&nbsp;</td>
									<td width="36">
									<%if session("UserType") <> "N" Then%>
									<form method="POST">
									<input type="button" onClick="NewWindow('customer_products1.asp?cname=<%=Server.URLEncode(arCustomer(1,0))%>&CustNo=<%=vCustNo%>&ProdCode=<%=Server.URLEncode(arStandingOrder(0,i))%>','profile','850','300','yes','center');return false" onFocus="this.blur()" value="Edit" name="B1" style="font-family: Verdana; font-size: 8pt; width:50px">									
									</form>
									<%end if%>
									</td>
									<td width="5">&nbsp;</td>
									<td width="56">
									<%if session("UserType") <> "N" Then%>
									<form method="POST">
  							        <input type = "hidden" name="CustNo" value = "<%=vCustNo%>">
  							        <input type = "hidden" name="pno" value = "<%=arStandingOrder(0,i)%>">
  							        <input type = "hidden" name="pname" value = "<%=arStandingOrder(6,i)%>">							        
  							        <input type = "hidden" name="delete" value = "yes">
  							        <input type="button" onClick="validate(this.form);" value="Delete" name="B1" style="font-family: Verdana; font-size: 8pt; width:50px">
							        </form>
									<%end if%>
									</td>
									</tr>
									
									</table>							

							</td>
							<td><%=arStandingOrder(0,i)%><b></b></td>
							<td><%=arStandingOrder(6,i)%></td><%						
							prno = arStandingOrder(0,i) 
							Do While i<=UBound(arStandingOrder,2) and prno = arStandingOrder(0,i) 
								vWNo = arStandingOrder(1,i)
								if vWno mod 2 <> 1 then%>
									<td align="center" bgcolor="#E1E1E1"><%															
								Else%>
									<td align="center" ><%															
								End if%>
									<table border="0" width="100%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
										<%if i <=18 then%>
										<tr>
										  <td width="33%" align="center"><font size="1"><b>M</b></font></td>
										  <td width="33%" align="center"><font size="1"><b>N</b></font></td>
										  <td width="34%" align="center"><font size="1"><b>E</b></font></td>
										</tr>
										<%end if%>
										<tr><%
											Do While i <= UBound(arStandingOrder,2)	and prno = arStandingOrder(0,i) and vWNo = arStandingOrder(1,i)					
												if Trim(arStandingOrder(5,i)) <> "" Then
												if (i mod 21=0 or i mod 21=1 or i mod 21=2) then
													MonTotalOrderQty=MonTotalOrderQty+arStandingOrder(5,i)
													MonTotalOrderPrice=MonTotalOrderPrice + (cdbl(arStandingOrder(7,i))*arStandingOrder(5,i))
												end if
												if (i mod 21=3 or i mod 21=4 or i mod 21=5) then
													TueTotalOrderQty=TueTotalOrderQty+arStandingOrder(5,i)
													TueTotalOrderPrice=TueTotalOrderPrice + (cdbl(arStandingOrder(7,i))*arStandingOrder(5,i))
												end if
												if (i mod 21=6 or i mod 21=7 or i mod 21=8) then
													WedTotalOrderQty=WedTotalOrderQty+arStandingOrder(5,i)
													WedTotalOrderPrice=WedTotalOrderPrice + (cdbl(arStandingOrder(7,i))*arStandingOrder(5,i))
												end if
												if (i mod 21=9 or i mod 21=10 or i mod 21=11) then
													ThuTotalOrderQty=ThuTotalOrderQty+arStandingOrder(5,i)
													ThuTotalOrderPrice=ThuTotalOrderPrice + (cdbl(arStandingOrder(7,i))*arStandingOrder(5,i))
												end if
												if (i mod 21=12 or i mod 21=13 or i mod 21=14) then
													FriTotalOrderQty=FriTotalOrderQty+arStandingOrder(5,i)
													FriTotalOrderPrice=FriTotalOrderPrice + (cdbl(arStandingOrder(7,i))*arStandingOrder(5,i))
												end if
												if (i mod 21=15 or i mod 21=16 or i mod 21=17) then
													SatTotalOrderQty=SatTotalOrderQty+arStandingOrder(5,i)
													SatTotalOrderPrice=SatTotalOrderPrice + (cdbl(arStandingOrder(7,i))*arStandingOrder(5,i))
												end if
												if (i mod 21=18 or i mod 21=19 or i mod 21=20) then
													SunTotalOrderQty=SunTotalOrderQty+arStandingOrder(5,i)
													SunTotalOrderPrice=SunTotalOrderPrice + (cdbl(arStandingOrder(7,i))*arStandingOrder(5,i))
												end if
												%>
													<td width="33%" align="center"><b><%=arStandingOrder(5,i)%></b></td><%
												Else%>
													<td width="33%" align="center"><b>-</b></td><%
												End if 												
												i = i+1
												if i > UBound(arStandingOrder,2) then exit do							
											Loop%>
										</tr>
									</table>								
									</td><%
								if i > UBound(arStandingOrder,2) then exit do												
							Loop%>        						
						</tr>	
						<%
						if i > UBound(arStandingOrder,2) then exit do																			
					loop
					%>
					<tr bgcolor="#CCCCCC" height="25">
						<td colspan="3" align="right"><strong>Total Order Value:</strong></td>
						<td align="center"><strong><%=MonTotalOrderPrice%></strong></td>
						<td align="center"><strong><%=TueTotalOrderPrice%></strong></td>
						<td align="center"><strong><%=WedTotalOrderPrice%></strong></td>
						<td align="center"><strong><%=ThuTotalOrderPrice%></strong></td>
						<td align="center"><strong><%=FriTotalOrderPrice%></strong></td>
						<td align="center"><strong><%=SatTotalOrderPrice%></strong></td>
						<td align="center"><strong><%=SunTotalOrderPrice%></strong></td>
					</tr>
					<tr bgcolor="#CCCCCC" height="25">
						<td colspan="3" align="right"><strong>Total Order Qty:</strong></td>
						<td align="center"><strong><%=MonTotalOrderQty%></strong></td>
						<td align="center"><strong><%=TueTotalOrderQty%></strong></td>
						<td align="center"><strong><%=WedTotalOrderQty%></strong></td>
						<td align="center"><strong><%=ThuTotalOrderQty%></strong></td>
						<td align="center"><strong><%=FriTotalOrderQty%></strong></td>
						<td align="center"><strong><%=SatTotalOrderQty%></strong></td>
						<td align="center"><strong><%=SunTotalOrderQty%></strong></td>
					</tr>
					<%
        End if
		%> 
      </table>
  </div>
  <br>
<table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
<form method="POST">
<td align="right">

	<input type = "hidden" name="CustNo" value = "<%=vCustNo%>">
	<input type = "hidden" name="clearall" value = "yes">
	<input type="button" onClick="validateclearall(this.form);" value="Clear All Typical Order" name="B2" style="font-family: Verdana; font-size: 8pt; width:160px">&nbsp;&nbsp;

<input type="button" value=" Upload Product " style="font-family: Verdana; font-size: 8pt; width:120px" onClick="NewWindow('Upload_products.asp?CustNo=<%=vCustNo%>','UploadProduct','750','300','yes','center');">
</td>
</form>
</tr>
</table>
<div align="center">
  <center>

      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td bgcolor="#FFFFFF" colspan="4"><b><font size="2">Prices</font></b></td>
        </tr>
        </table>
		 <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" bgcolor="#999999">
		<tr height="22">
          <td bgcolor="#CCCCCC"><b>Product code</b></td>
          <td bgcolor="#CCCCCC"><b>Product name</b></td>
          <td bgcolor="#CCCCCC"><b>Normal Price</b></td>
          <td bgcolor="#CCCCCC"><b>Special Price</b></td>
        </tr><%        
					if IsArray(arSpecialProductPrice) Then
						k=1
						For i = 0 to UBound(arSpecialProductPrice,2)
						if (k mod 2 =0) then
							strBgColour="#CCCCCC"
						else
							strBgColour="#F3F3F3"
						end if	
						k=k+1					
						%>
							<tr bgcolor="<%=strBgColour%>" height="22">
								<td><%=arSpecialProductPrice(0,i)%></td>
								<td><%=arSpecialProductPrice(1,i)%></td>
								<td><%=formatnumber(arSpecialProductPrice(2,i),2)%></td>
								<td><%=formatnumber(arSpecialProductPrice(3,i),2)%></td>
							</tr><%	
						Next
					End if%>	
      </table>
  </center>
</div>

	<p align="center"><input type="button" value=" Back " onClick="history.back()"><br></p>



</body>
</html><%
If IsArray(arSpecialProductPrice) Then Erase arSpecialProductPrice
If IsArray(arCustomer) Then Erase arCustomer
If IsArray(arStandingOrder) Then erase arStandingOrder

Function FormatDate(strDate)
    stop
	Dim strYYYY
    Dim strMM
    Dim strDD

        strYYYY = CStr(DatePart("yyyy", strDate))

        strMM = CStr(DatePart("m", strDate))
        If Len(strMM) = 1 Then strMM = "0" & strMM

        strDD = CStr(DatePart("d", strDate))
        If Len(strDD) = 1 Then strDD = "0" & strDD

		 FormatDate = strYYYY & "-" & strMM & "-" & strDD
       ' FormatDate = strMM & "-" & strDD & "-" & strYYYY

End Function 
%>