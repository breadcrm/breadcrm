<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
vCustNo= request("CustNo")
vCreditNo= request("CreditNo")

if vCustNo= "" or vCreditNo= "" then response.redirect "credit_find.asp"
stop
set object = Server.CreateObject("bakery.credit")
object.SetEnvironment(strconnection)
set DisplayCreditDetail= object.DisplayCreditDetail(vCustNo,vCreditNo)
vRecArray1 = DisplayCreditDetail("CustomerDetail")
vRecArray2 = DisplayCreditDetail("CreditDetail")
vRecArray3 = DisplayCreditDetail("CreditSummary")
set DisplayCreditDetail= nothing
set object = nothing
stop
if not (isarray(vRecArray1) and isarray(vRecArray2)) then  response.redirect "credit_find.asp"
    intRID=vRecArray1(13,0)
    strLogo1 = vRecArray1(14,0)
    strTele = vRecArray1(15,0)
    
    if strTele = "" Then
        strTele = "_"
    End If
	
	if intRID<>"" and intRID<>"0" then
		set object = Server.CreateObject("bakery.Reseller")
		object.SetEnvironment(strconnection)
		set DisplayResellerDetail= object.DisplayResellerDetail(intRID)
		vRecArrayReseller = DisplayResellerDetail("ResellerDetail")
		set DisplayResellerDetail= nothing
		set object = nothing
	end if	

   
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; height:1px }
-->
</style>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<div align="center">
  <center>
  <%
  If vRecArray1(10, 0) = "Bread Factory" Then
			strLogo = "<b><font face=""Monotype Corsiva"" size=""5"">THE BREAD FACTORY</font></b>"
			strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		    strAddress = strAddress & "<br>" & vbCrLf
		    strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
		    strAddress = strAddress & "Watford, Herts WD17 1LA<br>" & vbCrLf
		    strAddress = strAddress & "</p>" & vbCrLf
		    strAddress = strAddress & "<p>" & vbCrLf
		     if strLogo1 <>  "" then
		        strAddress = strAddress & "Telephone: &nbsp; " & strTele & vbCrLf
		     else
		        strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
		        strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
		     end if   
		    strAddress = strAddress & "</p></font>"
	  ElseIf vRecArray1(10, 0) = "Gail Force" Then
			strLogo = "<b><font face=""Kunstler Script"" size=""7"">Gail Force</font></b>"
			strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		    strAddress = strAddress & "<br>" & vbCrLf
		    strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
		    strAddress = strAddress & "Watford, Herts WD17 1LA<br>" & vbCrLf
		    strAddress = strAddress & "</p>" & vbCrLf
		    strAddress = strAddress & "<p>" & vbCrLf
		    if strLogo1 <>  "" then
		        strAddress = strAddress & "Telephone: &nbsp; " & strTele & vbCrLf
		    else
		        strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
		        strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
		    end if
		    strAddress = strAddress & "</p></font>"
	  End If
	if intRID<>"" and intRID<>"0" then
	if isarray(vRecArrayReseller) then
		strResellerName=vRecArrayReseller(1,0)
		strResellerAddress1=vRecArrayReseller(2,0)
		strResellerTown=vRecArrayReseller(3,0)
		strResellerPostcode=vRecArrayReseller(4,0)
		strResellerTelephone=vRecArrayReseller(5,0)
		strResellerFax=vRecArrayReseller(6,0)
		strCompanyLogo="images/ResellerLogos/" & vRecArrayReseller(8,0)
		strResellerInvoiceFooterMessage=vRecArrayReseller(9,0)
		strVATRegNo=vRecArrayReseller(10,0)
		
		strResellerAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		if strResellerName<>"" then
			strResellerAddress = strResellerAddress & "<b>" & strResellerName & "</b><br>" & vbCrLf
		end if
		if strResellerAddress1<>"" then
			strResellerAddress = strResellerAddress & strResellerAddress1 & "<br>"& vbCrLf
		end if
		if strResellerTown<>"" then
			strResellerAddress = strResellerAddress & strResellerTown & "<br>" & vbCrLf
		end if
		if strResellerPostcode<>"" then
			strResellerAddress = strResellerAddress & strResellerPostcode & "<br>" & vbCrLf
		end if
		strResellerAddress = strResellerAddress & "<BR>" & vbCrLf
		
		if strLogo1 = "" then
		    if strResellerTelephone<>"" then
			    strResellerAddress = strResellerAddress & "Telephone: " &  strResellerTelephone & "<br>" & vbCrLf
		    end if
		    if strResellerFax<>"" then
			    strResellerAddress = strResellerAddress & "Fax:" &  strResellerFax & "<br>" & vbCrLf
		    end if
		    
		else
		
		  strResellerAddress = strResellerAddress & "Telephone: " &  strTele & "<br>" & vbCrLf   
		
		End if    
			strResellerAddress = strResellerAddress & "</p></font>"
	end if
end if
	strBG=""
	if (vRecArray1(12, 0))="1" then
		strBG="images/PricelessInvoiceNew.gif"
	end if
	%>
  <table border="0" width="90%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="28%" valign="top">
            <%if intRID<>"" and intRID<>"0" then%>
			<font face="Verdana" size="1">VAT Reg No. <%=strVATRegNo%></font>
			<%=strResellerAddress%>
			<%else%>
			<font face="Verdana" size="1">VAT Reg No. XXXXXX</font>
			<%=strAddress%>
			<%end if%>
          </td>
          <td width="38%" valign="top" align="center">
           <%if strLogo1 <> "" then%>
            <img src="images/CustomerGroupLogo/<%=strLogo1%>"><br />
           <%elseif intRID<>"" and intRID<>"0" then%>
			<img src="<%=strCompanyLogo%>">
			<%else%>
			<img src="images/BakeryLogo.gif" width="225" height="77">
			<%end if%><br>
		  <%if strBG<>"" then%><img src="<%=strBG%>" height="60" width="225"><%end if%>
          </td>
         <td width="33%" valign="top" align="center">
			<font face="Verdana" size="2">
			THIS IS A CREDIT<br>
			STATEMENT<br>
			</font>
			<font face="Verdana" size="1">PLEASE KEEP SAFE AND PASS TO YOUR ACCOUNTS DEPARTMENT</font>
           </td>
        </tr>
        <tr>
          <td colspan="3" align="center" height="35"><b><font face="Verdana" size="3">Credit Number: <%=vCreditNo%></font></b></td>
        </tr>
      </table>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%" valign="top">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="400">
      <tr>
          <td><font face="Verdana" size="2">
		  <b>Invoice No:  <%=vRecArray1(8,0)%></b><%'Invoice Number%><br>
		  Customer Code: <%=vRecArray1(1,0)%><%'=vRecArray1(1,0)%><br>
		  Van Number: <%=vRecArray1(11,0)%><br>
		  Customer Name: <%=vRecArray1(0,0)%><br>
		  Address: <%=vRecArray1(2,0)%>&nbsp;<%=vRecArray1(3,0)%>,&nbsp;<%=vRecArray1(4,0)%>,&nbsp;<%=vRecArray1(5,0)%><br><br>
		  Credit Date:<%=day(vRecArray1(7,0))%>/<%=month(vRecArray1(7,0))%>/<%=year(vRecArray1(7,0))%>
		 </font>
		 </td>
        </tr>
      </table>
	<br>
      <table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td width="13%" bgcolor="#CCCCCC" align="center"><b>Product Code</b></td>
          <td width="22%" bgcolor="#CCCCCC" align="center"><b>Product Name</b></td>
          <!--
		  <td width="8%" bgcolor="#CCCCCC" align="center"><b> Qty Ordered</b></td>
		  -->
          <td width="9%" bgcolor="#CCCCCC" align="center"><b> Qty Credited</b></td>
		  <!--
          <td width="14%" bgcolor="#CCCCCC" align="center"><b>Reason for credit</b></td>
          <td width="9%" bgcolor="#CCCCCC" align="center"><b>Other</b></td>
		  -->
		  <td width="9%" height="20" align="center" bgcolor="#CCCCCC"><b>Unit price</b></td>
		  <td width="16%" bgcolor="#CCCCCC" height="20" align="center"><b>Total price</b>&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <%
		dim Total
		Total=0
		for i = 0 to ubound(vRecArray2,2)%>
        <tr>
          <td><%=vRecArray2(0,i)%>&nbsp;</td>
          <td><%=vRecArray2(1,i)%>&nbsp;</td>
          <!--
		  <td align="center"><%=vRecArray2(2,i)%>&nbsp;</td>
		  -->
          <td align="center"><%=vRecArray2(3,i)%>&nbsp;</td>
          <!--
		  <td align="center"><%=vRecArray2(4,i)%>&nbsp;</td>
          <td><%=vRecArray2(5,i)%>&nbsp;</td>
		  -->
		  	<td align="center"><%=FormatNumber(vRecArray2(6,i),2)%>&nbsp;&nbsp;</td>
			<td align="center"><%=FormatNumber(cdbl(vRecArray2(6,i))*vRecArray2(3,i),2)%>&nbsp;&nbsp;&nbsp;</td>
    	</tr>
        <%
	
			Total=Total+(cdbl(vRecArray2(6,i))*vRecArray2(3,i))
		next
		%>        
        
      </table>
	  <div align="left">
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="77%">
              <p align="right"><b><font size="2" face="Verdana">Goods
              total&nbsp;&nbsp; </font></b></td>
            <td width="24%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%">
                   	<p align="center"><font size="2" face="Verdana"><b>&nbsp;�<%=FormatNumber(vRecArray3(0,0),2)%></b>&nbsp;&nbsp;</font></p>
				  </td>
                </tr>
              </table>
            </td>
            </tr>
          <tr>
            <td>
              <p align="right"><b><font size="2" face="Verdana">VAT
              total&nbsp;&nbsp; </font></b></td>
            <td width="23%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2" rules="none">
                <tr>
                  <td width="100%">
                   	<p align="center"><font size="2" face="Verdana"><b>&nbsp;� <%=FormatNumber(vRecArray3(1,0),2)%></b></font>&nbsp;&nbsp;</p>
                   </td>
                </tr>
              </table>
            </td>
            </tr>
          <tr>
            <td>
              <p align="right"><b><font size="2" face="Verdana">Credit
              total&nbsp;&nbsp; </font></b></td>
            <td width="23%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%">
                    <p align="center"><font size="2" face="Verdana"><b>&nbsp;�<%=FormatNumber((cdbl(FormatNumber(vRecArray3(0,0),2))+cdbl(FormatNumber(vRecArray3(1,0),2))),2)%></b></font>&nbsp;&nbsp;</p>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          </table>
      </div><br>
    </td>
  </tr>
</table>
</center>
</div>
<% if (vRecArray1(12, 0))="1" then%>
<p>&nbsp;</p>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<div align="center">
  <center>
  <%
  If vRecArray1(10, 0) = "Bread Factory" Then
			strLogo = "<b><font face=""Monotype Corsiva"" size=""5"">THE BREAD FACTORY</font></b>"
			strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		    strAddress = strAddress & "<br>" & vbCrLf
		    strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
		    strAddress = strAddress & "Watford, Herts WD17 1LA<br>" & vbCrLf
		    strAddress = strAddress & "</p>" & vbCrLf
		    strAddress = strAddress & "<p>" & vbCrLf
		    if strLogo1 <>  "" then
		        strAddress = strAddress & "Telephone: &nbsp; " & strTele & vbCrLf
		    else
		        strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
		        strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
		    end if    
		    strAddress = strAddress & "</p></font>"
	  ElseIf vRecArray1(10, 0) = "Gail Force" Then
			strLogo = "<b><font face=""Kunstler Script"" size=""7"">Gail Force</font></b>"
			strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		    strAddress = strAddress & "<br>" & vbCrLf
		    strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
		    strAddress = strAddress & "Watford, Herts WD17 1LA<br>" & vbCrLf
		    strAddress = strAddress & "</p>" & vbCrLf
		    strAddress = strAddress & "<p>" & vbCrLf
		    
		    if strLogo1 <>  "" then
		        strAddress = strAddress & "Telephone: &nbsp; " & strTele & vbCrLf
		    else		    
		       	strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
		        strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
		    end if
		    strAddress = strAddress & "</p></font>"
	  End If
	  %>

  <table border="0" width="90%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="28%" valign="top">
          <font face="Verdana" size="1">VAT Reg No. XXXXXX</font>
          <%=strAddress%>
          </td>
          <td width="38%" valign="top" align="center">
          <%if strLogo1 <> "" then%>
            <img src="images/CustomerGroupLogo/<%=strLogo1%>"><br />
          <%else %>  
          <img src="images/BakeryLogo.gif" width="225" height="77">
          <%end if%>
          </td>
         <td width="33%" valign="top" align="center">
			<font face="Verdana" size="2">
			THIS IS A CREDIT<br>
			STATEMENT<br>
			</font>
			<font face="Verdana" size="1">PLEASE KEEP SAFE AND PASS TO YOU ACCOUNTS DEPARTMENT</font>
           </td>
        </tr>
        <tr>
          <td colspan="3" align="center" height="35"><b><font face="Verdana" size="3">Credit Number: <%=vCreditNo%></font></b></td>
        </tr>
      </table>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%" valign="top">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="400">
      <tr>
          <td><font face="Verdana" size="2">
		  <b>Invoice No:  <%=vRecArray1(8,0)%></b><%'Invoice Number%><br>
		  Customer Code: <%=vRecArray1(1,0)%><%'=vRecArray1(1,0)%><br>
		  Van Number: <%=vRecArray1(11,0)%><br>
		  Customer Name: <%=vRecArray1(0,0)%><br>
		  Address: <%=vRecArray1(2,0)%>&nbsp;<%=vRecArray1(3,0)%>,&nbsp;<%=vRecArray1(4,0)%>,&nbsp;<%=vRecArray1(5,0)%><br><br>
		  Credit Date:<%=day(vRecArray1(7,0))%>/<%=month(vRecArray1(7,0))%>/<%=year(vRecArray1(7,0))%>
		 </font>
		 </td>
        </tr>
      </table>
	<br>
      <table border="1" width="95%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td width="13%" bgcolor="#CCCCCC" align="center"><b>Product Code</b></td>
          <td width="22%" bgcolor="#CCCCCC" align="center"><b>Product Name</b></td>
          <td width="9%" bgcolor="#CCCCCC" align="center"><b> Qty Credited</b></td>
		</tr>
        <%
		
		Total=0
		for i = 0 to ubound(vRecArray2,2)%>
        <tr>
          <td><%=vRecArray2(0,i)%>&nbsp;</td>
          <td><%=vRecArray2(1,i)%>&nbsp;</td>
          <td align="center"><%=vRecArray2(3,i)%>&nbsp;</td>
        </tr>
        <%
		Total=Total+(cdbl(vRecArray2(6,i))*vRecArray2(3,i))
		next%>        
        
      </table>
	  <br>
    </td>
  </tr>
</table>
</center>
</div>
<% end if%>
</body>
</html>