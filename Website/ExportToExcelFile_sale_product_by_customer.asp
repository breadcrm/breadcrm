<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
	fromdt=request("fromdt")
	todt=request("todt")
	vProduct = Request("Product")
	vProductID=Request("ProductID")
	if vCustomer="" then
		vCustomer=0
	end if
	if fromdt ="" then
    fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
	  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
	end if
	
	vismultipleproduct = Request("ismultipleproduct")
	
	set objBakery = server.CreateObject("Bakery.reports")
	objBakery.SetEnvironment(strconnection)
	set CustomerCol = objBakery.Display_CustomerList()
	arCustomer = CustomerCol("Customer")
	
	set CustomerCol = nothing
	if isdate(fromdt) and isdate(todt) then
		set retcol = objBakery.Display_Products_By_Customer(fromdt,todt,"",vProductID)
		recarray = retcol("ProductsByCustomer")
		set vListMultipleProductsForProduct= objBakery.ListMultipleProductsForProduct(vProductID,fromdt,todt)
			vRecArray1 = vListMultipleProductsForProduct("MultiProducts")			
	set retcol = nothing
	end if
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=sale_product_by_customer.xls" 
	%>
	<table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
   		<tr>
          <td colspan="6">
		  <b>Sales Report - Product By Customers From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt;</b><br><br>
		  <b>Product Code: <%=vProductID%></b><br><br>
		  <b>Product Name: <%=vproduct%></b><br><br>
		  
		   <%If vismultipleproduct = 1 then 
		   
				if isarray(vRecArray) then
		   %>
				   <b>Product Breakdown:</b><br>
			<%
				   For i = 0 to UBound(vRecArray,2)		   
						response.write(vRecArray(2,i) & " x " &  vRecArray(1,i) & "<br>")
				   next
		  		end if 
			%>
			 <br>
			 <%
		   end if 
		   %>
		  
		  </td>
        	
		</tr>
        <tr height="25">
		 <td width="8%" bgcolor="#CCCCCC"><b>Customer Code&nbsp;</b></td>
		  <td width="53%"  bgcolor="#CCCCCC"><b>Customer Name&nbsp;</b></td>
		  <td width="11%" bgcolor="#CCCCCC"><b>Week Van No.&nbsp;</b></td>
		  <td width="12%" bgcolor="#CCCCCC"><b>Weekend Van No.&nbsp;</b></td>
          <td width="12%" bgcolor="#CCCCCC"><b>Number of Products&nbsp;</b></td>
          <td width="10%" bgcolor="#CCCCCC" align="right"><b>Turnover&nbsp;</b></td>
          
        </tr>
				
				<%if isarray(recarray) then%>
				<% 				
			
				Totalturnover = 0.00
				vNumber = 0
				
				
				%>
				<%for i=0 to UBound(recarray,2)%>
				<tr height="25">
				<td width="8%"><%=recarray(5,i)%>&nbsp;</td>
				<td width="53%"><b><%if recarray(0,i)<> "" then Response.Write recarray(0,i) end if%></b>&nbsp;</td>
				<td width="10%"><%=recarray(3,i)%>&nbsp;</td>
				<td width="12%"><%=recarray(4,i)%>&nbsp;</td>
				<td width="12%" align="right"><b><%if recarray(1,i)<> "" then Response.Write formatnumber(recarray(1,i),0) else Response.Write "0" end if%></b></td> 
				<td width="11%" align="right"><b><%if recarray(2,i)<> "" then Response.Write formatnumber(recarray(2,i),2) else Response.Write "0.00" end if%></b></td>
                    
          		</tr>
          <%
       
				  if recarray(1,i)<> "" then 
						vNumber = vNumber + formatnumber(recarray(1,i),0)
				  End if
				  if recarray(2,i)<> "" then 
						Totalturnover = Totalturnover + formatnumber(recarray(2,i),2)
				  End if
				
          %>
          <%next%>
          <tr height="25">
           <td colspan="4" align="right"><b>Grand Total</b>&nbsp;</td>
           <td width="12%" align="right" ><b><%=formatnumber(vNumber,0)%></b></td> 
           <td width="11%" align="right" ><b><%=formatnumber(Totalturnover,2)%></b></td> 
                    
          </tr>
          <%
          else%>
          <td colspan="6" bgcolor="#CCCCCC" height="25"><b>0 - No records matched...</b></td> <% 
          end if%>
        </tr>

      </table>
	  
	   <%if isarray(vRecArray1) then%>
	 <br><br>
	  <%for j=0 to UBound(vRecArray1,2)
	  	
	    set retcol = objBakery.Display_Multiple_Products_By_Customer(fromdt,todt,"",vProductID,"",vRecArray1(0,j))
		 recarray = retcol("MultiProductsByCustomer")
	  %>
	   <b>Product Code: <%=vRecArray1(0,j)%></b><br>
	   <b>Product Name: <%=vRecArray1(1,j)%></b><br><br>
	  <b>
	  Product Breakdown:<br>
	  <%=vRecArray1(2,j)%> x <%=vproduct%><br><br>
	  </b>
	  
	  <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
		<tr>
		
		 <td width="8%" bgcolor="#CCCCCC"><nobr><b>Customer Code&nbsp;</b></nobr></td>
		  <td width="45%"  bgcolor="#CCCCCC"><b>Customer Name&nbsp;</b></td>
		  <td width="12%" bgcolor="#CCCCCC"><b>Week Van No.&nbsp;</b></td>
		  <td width="15%" bgcolor="#CCCCCC"><b>Weekend Van No.&nbsp;</b></td>
          <td width="18%" bgcolor="#CCCCCC" align="right"><b>Number of Products&nbsp;</b></td>
          <td width="12%" bgcolor="#CCCCCC" align="right"><b>Turnover&nbsp;</b></td>
          
        </tr>
				<tr>
				<%if isarray(recarray) then%>
				<% 				
			
				Totalturnover = 0.00
				vNumber = 0
				
				
				%>
				<%for i=0 to UBound(recarray,2)%>
				<td width="8%"><%=recarray(5,i)%>&nbsp;</td>
				<td width="45%"><b><%if recarray(0,i)<> "" then Response.Write recarray(0,i) end if%></b>&nbsp;</td>
				<td width="12%"><%=recarray(3,i)%>&nbsp;</td>
				<td width="15%"><%=recarray(4,i)%>&nbsp;</td>
				<td width="18%" align="right"><b><%if recarray(1,i)<> "" then Response.Write formatnumber(recarray(1,i),0) else Response.Write "0" end if%>&nbsp;</b></td> 
				<td width="12%" align="right"><b><%if recarray(2,i)<> "" then Response.Write formatnumber(recarray(2,i),2) else Response.Write "0.00" end if%>&nbsp;</b></td>
                    
          </tr>
          <%
       
				  if recarray(1,i)<> "" then 
						vNumber = vNumber + formatnumber(recarray(1,i),0)
				  End if
				  if recarray(2,i)<> "" then 
						Totalturnover = Totalturnover + formatnumber(recarray(2,i),2)
				  End if
				
          %>
          <%next%>
          <tr>
           <td colspan="4" align="right"><b>Grand Total</b>&nbsp;</td>
           <td width="18%" align="right" ><b>&nbsp;<%=formatnumber(vNumber,0)%>&nbsp;</b></td> 
           <td width="12%" align="right" ><b>&nbsp;<%=formatnumber(Totalturnover,2)%>&nbsp;</b></td> 
                    
          </tr>
          <%
          else%>
          <td colspan="6" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td> <% 
          end if%>
      
      </table>
	  <%
	  set retcol = nothing
	  %>
	   <br><br>
	  <%
	  next
	  end if
	  %>
	  
	  
	  
	  <p align="center"><input type="button" value=" Back " onClick="history.back()"></p>
    </td>
  </tr>
</table>
</center>
</div>
</body>
<%
set objBakery = nothing
if IsArray(recarray) then erase recarray %>
</html>