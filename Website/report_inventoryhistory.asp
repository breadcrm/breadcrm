<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form1.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form1.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form1.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form1.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form1.day.value=t.getDate()
	document.form1.month.value=t.getMonth()
	document.form1.year.value=t.getFullYear()
	
	document.form1.day1.value=t1.getDate()
	document.form1.month1.value=t1.getMonth()
	document.form1.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}
//-->
</SCRIPT>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()"> 
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim recarray1, recarray2
dim retcol
dim totpages
dim vSessionpage
dim i, a1

dim fno, fname
dim fromdt
dim todt
dim qty, lstqty
dim ino

fno = Request.Form("lstfno")
qty = Request.Form("lstqty")
lstqty=qty
fromdt = Request.Form("txtfrom")
todt = Request.Form("txtto")

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

if instr(1,fno,"~") > 0 then
  a1=split(fno,"~")
  fno=a1(0)
  fname=a1(1)
else
  fname="facility"
end if

'Response.Write "fno = " & fno & "<br>"
'Response.Write "qty = " & qty & "<br>"
'Response.Write "fromdt = " & fromdt & "<br>"
'Response.Write "todt = " & todt & "<br>"

'Response.end

totpages = Request.Form("pagecount")
select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(totpages) then	
			session("Currentpage")  = session("Currentpage") + 1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage") - 1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select
vSessionpage = session("Currentpage")

set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)

set retcol = objBakery.getFacilities("A",1,0)
recarray1 = retcol("Facilities")

if trim(fno) <> "" and isdate(fromdt) and isdate(todt) then
  if trim(ino) = "" then ino = "-1"
  dim tfno
  tfno=fno
  if clng(fno) = 90 then tfno = "11,12,90,99"
  set retcol = objBakery.InventoryHistory(tfno,fromdt,todt,ino,vSessionpage,0)
  totpages = retcol("pagecount")
  recarray2 = retcol("InvHistory")
end if
%>
<div align="center">
 
  
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">

 <tr>
    <td width="100%"><b><font size="3">Inventory Management<br>
      &nbsp;</font></b>
     <form name="form1" method="post" action="report_inventoryhistory.asp">
	  <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0">
        <tr>
          <td><b>Facility</b></td>
          <td>
            <select size="1" name="lstfno" style="font-family: Verdana; font-size: 8pt"><%
              if isarray(recarray1) then
                for i=0 to ubound(recarray1,2)
                if recarray1(0,i) = 15 or recarray1(0,i) = 18 or recarray1(0,i) = 90 then%>
                 <option <%if clng(fno)=recarray1(0,i) then Response.Write("Selected")%> value="<%=recarray1(0,i)%>~<%=recarray1(1,i)%>"><%=recarray1(1,i)%></option><%
                 end if
                next
              else%>
                <option value="-1">No records found</option><%
              end if%>
            </select>
          </td>
        </tr>
        <tr>
          <td></td>
          <td> <table border="0" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0">
              <tr>
                <td><b>from</b></td>
               
                <td><b>Until</b></td>
              </tr>
              <tr>
                 <td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form1.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form1.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form1.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom onFocus="blur();" size="12"  name="txtfrom" value="<%=replace(fromdt,"from","")%>" onChange="setCalCombo(this.form,document.form1.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form1.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form1.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form1.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=replace(todt,"to","")%>" onChange="setCalCombo1(this.form,document.form1.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td>   </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td><b>of</b></td>
          <td>
          <select size="1" name="lstqty" style="font-family: Verdana; font-size: 8pt" onChange="form1.submit()">
            <option value="1" <%if lstqty="1" then Response.Write ("Selected")%> >Quantity used for production</option>
            <option value="2"  <%if lstqty="2" then Response.Write ("Selected")%>>Total inventory used</option>
            <!--<option value="3"  <%if lstqty="3" then Response.Write ("Selected")%>>Both</option>-->
          </select>
          </td>
        </tr>
        <tr>
          <td></td>
 
          <td>
            <p align="left">&nbsp;<br>
            <input type="submit" value="Submit" name="B1" style="font-family: Verdana; font-size: 8pt"></p>
        </td>
        </tr>
      </table>
	   </center>
	  </form>
	   <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
    <p align="left">
   <br>
    <b><font size="2">Inventory History for &lt;<%=fname%>&gt; between
    &lt;<%=fromdt%>&gt; and &lt;<%=todt%>&gt;</font></b>
    <div align="left"><%
    if isarray(recarray2) then%>
       <font size="2"><b>Page <%=vsessionpage%> of <%=totpages%></b></font><%
    end if%>
    </div>
      <table border="0" width="50%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td bgcolor="#CCCCCC"><b>Ingredient</b></td>
          <td bgcolor="#CCCCCC" align="center"><b>Unit</b></td>
        </tr><%
        if isarray(recarray2) then
          for i=0 to ubound(recarray2,2)%>
          <tr>
          <td><%=recarray2(1,i)%></td><%
          if lstqty = "1" then
            qty = recarray2(3,i)
          elseif lstqty = "2" then
            qty = recarray2(4,i)
          elseif lstqty = "3" then
            qty = recarray2(5,i)
          else
            qty = "0"
          end if%>
          <td align="center"><%=formatnumber(qty,2)%></td>
          </tr><%
          next
        elseif instr(1,lcase(Request.ServerVariables("HTTP_REFERER")),"report_inventoryhistory.asp") > 0 then%>
          <tr>
            <td colspan="2">
              <b>No Records matched...</b>
            </td>
          </tr><%
        end if%>
      </table>
    </td>
  </tr>
</table>
</div>
<br>
<div align="center">
  <table width="720" border="0" cellspacing="1" cellpadding="2" align="center">
  <tr> 
  <% if clng(vSessionpage) <> 1 then %>
  <td>
    <form name="frmFirstPage" action="report_inventoryhistory.asp" method="post">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="hidden" name="Direction" value="First">
    <input type="submit" name="submit" value="First Page">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) < clng(totpages) then %>
  <td>
    <form name="frmNextPage" action="report_inventoryhistory.asp" method="post">
    <input type="hidden" name="Direction" value="Next">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Next Page">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) > 1 then %>
  <td>
    <form name="frmPreviousPage" action="report_inventoryhistory.asp" method="post">
    <input type="hidden" name="Direction" value="Previous">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Previous Page">
    </form>
  </td>
  <%end if%>
  <%if clng(vSessionPage) <> clng(totpages) and clng(totpages) > 0 then %>
  <td>
    <form name="frmLastPage" action="report_inventoryhistory.asp" method="post">
    <input type="hidden" name="Direction" value="Last">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Last Page">
    </form>
  </td>
  <% end if%>
  </tr>
  </table>
</div>
</body>
</html>