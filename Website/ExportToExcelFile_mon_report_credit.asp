<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
dim objBakery
dim recarray
dim retcol
dim vYear, vMonth,i
Dim TotInvAmount,TotCrdAmount


	fromdt=Request("fromdt")
	todt=Request("todt")

	if isdate(fromdt) and isdate(todt) then
		set objBakery = server.CreateObject("Bakery.reports")
		objBakery.SetEnvironment(strconnection)
		set retcol = objBakery.Display_credits(fromdt,todt)
		recarray = retcol("Credits")
	end if
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=ExportToExcelFile_mon_report_credit.xls" 

%>
	
      	<table border="1" cellspacing="0" cellpadding="0">
		<tr>
          <td colspan="21"><b>Credit Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt;</b></td>
        </tr>
        <tr>
		  <td width="8%" bgcolor="#CCCCCC"><b>Credit No</b></td>
		 <td width="8%" bgcolor="#CCCCCC"><b>Date of Credit</b></td>
		  <td width="8%" bgcolor="#CCCCCC"><b>Invoice No</b></td>
		  <td width="8%" bgcolor="#CCCCCC"><b>Date of Invoice</b></td>
		  <td width="8%" bgcolor="#CCCCCC"><b>Customer Code</b></td>
		  <td width="18%" bgcolor="#CCCCCC"><b>Customer Name</b></td>
		  <td width="10%" bgcolor="#CCCCCC"><b>Name of person making Complaint</b></td>
		  <td width="4%" bgcolor="#CCCCCC"><b>Mon Van No</b></td>
		  <td width="4%" bgcolor="#CCCCCC"><b>Tue Van No</b></td>
		  <td width="4%" bgcolor="#CCCCCC"><b>Wed Van No</b></td>
		  <td width="4%" bgcolor="#CCCCCC"><b>Thu Van No</b></td>
		  <td width="4%" bgcolor="#CCCCCC"><b>Fri Van No</b></td>
		  <td width="4%" bgcolor="#CCCCCC"><b>Sat Van No</b></td>
		  <td width="4%" bgcolor="#CCCCCC"><b>Sun Van No</b></td>    
		  <td width="8%" bgcolor="#CCCCCC"><b>Product Code</b></td>
		  <td width="18%" bgcolor="#CCCCCC"><b>Product Name</b></td>
		  <td width="11%" bgcolor="#CCCCCC"><b>Reason for Credit</b></td>
		  <td width="11%" bgcolor="#CCCCCC"><b>Responsibility for Complaint</b></td>
		  <td width="5%" bgcolor="#CCCCCC"><b>Comments</b></td>
		  <td width="6%" bgcolor="#CCCCCC"><b>Units</b></td>
		  <td width="8%" bgcolor="#CCCCCC" align="right"><b>Value</b></td>
		</tr>
<%if isarray(recarray) then%>
<% GrandTotal = 0.00 %>
<%for i=0 to UBound(recarray,2)%>
<tr style="font-weight:bold">
<td><%=recarray(0,i)%></td>
<td><%=FormatDateInDDMMYYYY(recarray(20,i))%></td>
<td><%=recarray(21,i)%></td>
<td><%=FormatDateInDDMMYYYY(recarray(22,i))%></td>
<td><%=recarray(2,i)%></td>
<td><%=recarray(1,i)%></td>
<td><%=recarray(19,i)%>&nbsp;</td>
<td><%=recarray(10,i)%></td>
<td><%=recarray(11,i)%></td>
<td><%=recarray(12,i)%></td>
<td><%=recarray(13,i)%></td>
<td><%=recarray(14,i)%></td>
<td><%=recarray(15,i)%></td>
<td><%=recarray(16,i)%></td>
<td><%=recarray(7,i)%>&nbsp;</td>
<td><%=recarray(3,i)%></td>
<td><%=recarray(5,i)%></td>
<td><%=recarray(18,i)%>&nbsp;</td>
<td><%=recarray(17,i)%>&nbsp;</td>
<td align="right"><%=recarray(4,i)%></td>
<td align="right"><b>&nbsp;<%if recarray(6,i)<> "" then Response.Write formatnumber(recarray(6,i),2) else Response.Write "0.00" end if%></b></td>

</tr>
<%
  if recarray(6,i) <> "" then
	 GrandTotal = GrandTotal + formatnumber(recarray(6,i),2)
  end if
%>
<%next%>
<tr>
  <td width="10%" colspan="20"  bgcolor="#CCCCCC" align="right"><b>Grand Total</b>&nbsp;&nbsp;</td>
  <td width="10%"  bgcolor="#CCCCCC" align="right"><b>&nbsp;<%=formatNumber(GrandTotal,2)%></b></td>
</tr>
<%else%>
  <td bgcolor="#FFFFFF" colspan="21" align="center" height="50"><b><font color="#FF0000">0 - No records matched...</font></b></td>
<%end if%>
</tr>
</table>
</td>
</tr>
</table>


