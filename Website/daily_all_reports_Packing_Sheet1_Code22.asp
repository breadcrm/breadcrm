<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Response.Buffer
Dim deldate
deldate= request("deldate")
if deldate= "" then response.redirect "LoginInternalPage.asp"




call DisplayPackingSheetReport1()
%>


<%
Sub DisplayPackingSheetReport1()

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Packing Sheet Report 1 (Code 22)</title>
<style type="text/css">

br.page { page-break-before: always; height:1px }

table.report { border-top:2px solid #000000; border-right:2px solid #000000; }
table.report td { border-bottom:2px solid #000000; border-left:2px solid #000000; }

</style>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">

<%
set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
set col1= object.GetPackingSheetReport1(deldate,1,"22")
vRecArray = col1("PackingSheetReport1")

curpage=0
mFontSize = 8
mFooterSize =3
mFooterFontSize = 3
mExtraLine = 10
mLine = 8
mLineCount = 0
mTotalLineCount = 30
if isarray(vrecarray) then
mPage = 1
currec=0
totqty=0
curcno = vrecarray(0,0)
curvan = vrecarray(5,0)
'curBakeorder = vrecarray(11,0)
curpage=1	

else
mPage = 1
end if

i = 0

%>

<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: <%=mFontSize%>pt">
  <tr style="height:60px;">
    <td width="100%">
    <center><b><font size="3">PACKING SHEET 1 (Code 22)<br></font></b></center>
    </td>
  </tr>
</table>
</center>
</div>
<%
if mFontSize = 14 Then
	mLine = 16
Else
	mLine = 8
End if
mLineCount = 3

if isarray(vrecarray) then
mTotalPage = mPage
mPage = 1		
currec=0
totqty=0

curpage=1	
PreVanNo = 0
PreCustNo = 0 
PreBakeOrder = 0 
for i = 0 to ubound(vRecArray,2)

curcno = vrecarray(0,i)
curvan = vrecarray(5,i)
'curBakeorder = vrecarray(11,i)
currec = clng(currec) + 1
 
 if i = 0 Then
PreCustNo = vrecarray(0,i)
PreVanNo = vrecarray(5,i)
'curBakeorder = vrecarray(11,i)
 End if
 
 if curvan <> PreVanNo OR i = 0 Then
 
 if i <> 0 Then
 %>
 <br class="page" />
 <%
 mLineCount = 0
 
 End if

%>

<div align="center">
<br /><br />
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 10pt">
 <tr>
  <td >
    <b>Date of report :</b> <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
    <b>To be packed on :</b> <%=deldate%> <BR>
   
    </td>
    <td>
    <b>Number of Lines on Van <%=curvan %> : <%=vrecarray(14,i)%></b> <br>
    <b>Number of products on Van <%=curvan %> : <%=vrecarray(13,i)%></b><BR>
    </td>
</tr>
</table>
</div>
<%
mLineCount = mLineCount + 5

End if

%>


 <% 
 'if curcno <> PreCustNo OR curBakeorder <> PreBakeOrder OR i = 0 Then
 if curcno <> PreCustNo OR i = 0 Then 
 PreCustNo = vrecarray(0,i)
 if mLineCount = mTotalLineCount Then
 %>
 <br class="page" />
 <%
 mLineCount = 0
 Else
 
    if mTotalLineCount - mLineCount <= vrecarray(15,i) + 5 Then
    %>
  <br class="page" />
    <%
     mLineCount = 0
    End If
 
 End IF
 
  
  
 %>
 <div align="center">
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: <%=mFontSize%>pt">

 <tr style="vertical-align:middle; height:60px;"><td colspan="2" >
  <b>Van : <%=vrecarray(5,i)%></b><br />
  <b>Customer :<%=vrecarray(0,i)%> - <%=vrecarray(1,i)%> <% if vrecarray(7,i) <> "" or vrecarray(7,i) <> null Then response.Write ("(" & Trim(vrecarray(7,i)) & ")") End If%></b></td></tr>
 <tr>
 <td colspan="2">
 <table border="0" width="90%" cellspacing="0" cellpadding="2" class="report" style="font-family: Verdana; font-size: <%=mFontSize%>pt" >
  <tr>            
    <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Packing Order</b></td> 
	<td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Total/Van</b></td>
    <td align="center" bgcolor="#CCCCCC" height="20" width="65%"><b>Product Name</b></td>
    <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Customer Quantity</b></td>
    <td align="center" bgcolor="#CCCCCC" height="20" width="5%"><b>SID</b></td>
    </tr>
    <%
    mLineCount = mLineCount + 4
    End if %>
    
  
    
   <% 
  ' if curcno = PreCustNo OR curBakeorder = PreBakeOrder then
   if curcno = PreCustNo then%>
          <tr>            
            <td align="center" width="10%"><i><%=vrecarray(10,i)%></i>&nbsp;</td>
			<td align="center" width="10%"><i>[<%=left(vrecarray(12,i),50)%>]</i>&nbsp;</td>
            <td align="left" width="65%"><%=vrecarray(3,i)%>&nbsp;</td>
             <td align="center" width="10%" style="font-family: Verdana; font-size: 9pt"><b><%=vrecarray(4,i)%></b>&nbsp;</td>
			<td align="left" width="5%">&nbsp;</td>
			<%
            totqty = clng(totqty) + vrecarray(4,i)
            mLineCount = mLineCount + 1
			%>
           
          </tr>

          <% 
       
   end if
        
		  %>
          
          <%
          If i <> ubound(vRecArray,2) Then
            'If curcno <> vrecarray(0,i + 1) or curBakeorder <> vrecarray(11,i + 1) Then
            If curcno <> vrecarray(0,i + 1) Then
             %>
              <tr style="height:40px">
              <td colspan="2" style="border-bottom:none; border-left:none;">&nbsp;</td>
              <td align="left" width="60%"><B>TOTAL</B></td>
              <td align="center" width="5%" style="font-family: Verdana; font-size: 9pt"><B><%=totqty%></B></td>
              <td align="left" width="5%">&nbsp;</td>
              </tr>
              <%
              mLineCount = mLineCount + 2
              
              totqty = 0%>
               </table>
             </td>
             </tr>
             </table>
            </div>
             <%
            End If
         Else %> 
          <tr style="height:40px">
          <td colspan="2" style="border-bottom:none; border-left:none;">&nbsp;</td>
          <td align="left" width="60%"><B>TOTAL</B></td>
          <td align="center" width="5%"><B><%=totqty%></B></td>
          <td align="left" width="5%">&nbsp;</td>
          </tr>
           </table>
             </td>
             </tr>
             </table>
            </div>
          <%
          mLineCount = mLineCount + 2
          End if %>
          

<%
   PreVanNo = curvan
   PreCustNo = curcno
  ' PreBakeOrder = curBakeorder
Response.Flush()
Next
Else
%>

<div align="center">
<br /><br />
<table border="0" class="report" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: <%=mFontSize%>pt"> 
<tr style="height:60px;">
  <td >
    <b>Date of report :</b> <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
    <b>To be packed on :</b> <%=deldate%> <BR>
   
    </td>
</tr>
<tr>
<td width="100%" height="20" align="center">Sorry no customer details found</td>
</tr>
</table>
</div>

<%

End If%>
</body>
</html>
<%End Sub%>

