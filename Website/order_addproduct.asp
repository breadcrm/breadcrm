<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%
Dim Obj,ObjProduct,ObjResult
Dim arProduct
Dim strProdName,strProdCode
Dim vPageSize
Dim strDirection
Dim vpagecount
Dim vCurrentPage
Dim i
Dim vOrderNo
Dim vQty
Dim strAddProdCode
stop
vPageSize = 999999
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vCurrentPage	= Request.Form("CurrentPage") 
strProdName = Request.Form("txtProdName")
strProdCode = Request.Form("txtProdCode")
strhidDeliveryDate = Request("hidDeliveryDate")


'vOrderNo = Request.Form("OrderNo") 

if Trim(Request.Form("OrderNo")) <> "" Then
	vOrderNo = Request.Form("OrderNo")
Else
	vOrderNo = Request.QueryString("OrderNo")
End if	

strDateDiff= Request("vDateDiff") 

If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if

Set Obj = CreateObject("Bakery.Orders")
Obj.SetEnvironment(strconnection)

if Trim(Request.Form("Update")) <> "" Then
	if Trim(Request.Form("txtqty")) <> "" And IsNumeric(Request.Form("txtqty")) Then
		vQty = Request.Form("txtqty") 
		strAddProdCode = Request.Form("txtProdCode") 				
		ObjResult = obj.Update_AdditionalTypicalOrderItem(vOrderNo,strAddProdCode,vQty) 		
		if ObjResult = "OK" Then
		
			strAction="Delivery Date: " & strhidDeliveryDate & ", Product Added: " & strAddProdCode & " (qty: " & vQty & ")"
			
			cusNo = Obj.GetCustomerIdByOrderId(vOrderNo)
			
			LogAction "Order Confirmed",  strAction, cusNo
			Response.Redirect "order_new1.asp?OrderNo="&vOrderNo
		else
			strAction="Delivery Date: " & strhidDeliveryDate & ", Product Added: " & strAddProdCode & " (qty: " & vQty & ")"
			
			cusNo = Obj.GetCustomerIdByOrderId(vOrderNo)
			
			LogAction "Order Confirmed - Log Failed",  strAction, cusNo
			Response.Redirect "order_new1.asp?OrderNo="&vOrderNo	
		end if
	End if 
End if  


Set ObjProduct = obj.Display_AdditionalTypicalOrderList(vPageSize,vCurrentPage,strProdCode,strProdName,vOrderNo)
arProduct = ObjProduct("Product")
vpagecount  = ObjProduct("Pagecount")

Set Obj = Nothing
Set ObjProduct = Nothing
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<SCRIPT LANGUAGE="Javascript">
<!--
function ConfirmIs48Product(LateProd,DateDiff,frmindex)
{
	if (LateProd=="Y" && DateDiff<=1)
	{
		if (confirm("This is a 48 hrs product & requires 2 day notice - are you sure you want to proceed?"))
		{
			return true;
		}
		else
		{
			document.forms[frmindex].txtQty.value="";
			return false;
		}	
	}
	//if (LateProd=="Y" && DateDiff<=1)
//	{
//		alert("This product is a 48 hours product so you can't amend for this delivery date.")
//		document.forms[frmindex].txtQty.value="";
//		return false;
//	}
	return true	
}
	
//-->
</SCRIPT>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Add Product<br>
      &nbsp;</font></b>
      <form name="frmAddSearch" method="post">
	  <Input type="hidden" name="vDateDiff" value="<%=strDateDiff%>">
	  <Input type="hidden" name="hidDeliveryDate" value="<%=strhidDeliveryDate%>">
				<table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
				  <tr>
				    <td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
				    <td><b>Product code:</b></td>
				    <td><b>Product Name:</b></td>
				    <td></td>
				  </tr>
				  <tr>
				    <td></td>
				    <td><input type="text" name="txtProdCode" value="<%=strProdCode%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
				    <td><input type="text" name="txtProdName" value="<%=strProdName%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
				    <td><input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
				  </tr>
				  <tr>
				    <td><b>&nbsp;</b></td>
				    <td></td>
				    <td></td>
				    <td></td>
				  </tr>
				</table>
				<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
      </Form>
      
      <!--
        <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">      
				<tr>
					<td height="20"></td>
				</tr><%				
				if IsArray(arProduct) Then%>
					<tr>
					  <td align="right"><b>Page <%=vCurrentPage%> of <%=vpagecount%></b></td>          
					</tr><%
				End if%>	
			</table>

	-->

      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="25%" bgcolor="#CCCCCC"><b>Product Code</b></td>
          <td width="25%" bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td width="25%" bgcolor="#CCCCCC"><b>Qty</b></td>
          <td width="25%" bgcolor="#CCCCCC"><b>Action</b></td>
        </tr><%
        If (not (strProdCode= "" and strProdName="")) and IsArray(arProduct) Then
					For I = 0 to UBound(arProduct,2)%>        
						<FORM name="frmAddProd<%=i%>" method="post">
						<Input type="hidden" name="vDateDiff" value="<%=strDateDiff%>">
						<Input type="hidden" name="LateProd<%=I%>" value="<%=arProduct(2,I)%>">
						  <Input type="hidden" name="hidDeliveryDate" value="<%=strhidDeliveryDate%>">
							<tr>
							  <td><%=arProduct(0,i)%></td>
							  <td><%=arProduct(1,i)%></td>
							  <td><input type="text" onChange="ConfirmIs48Product(document.frmAddProd<%=I%>.LateProd<%=I%>.value,document.frmAddProd<%=I%>.vDateDiff.value,<%=I+1%>)" name="txtQty" size="7" maxlength="6" style="font-family: Verdana; font-size: 8pt"></td>
							  <td><input type="submit" value="Add Product" name="B1" style="font-family: Verdana; font-size: 8pt"></td>							  
							</tr>
							<input type="hidden"  name="txtProdCode"  value="<%=arProduct(0,i)%>">
							<input type="hidden"  name="Update"  value="True">
							<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
						</Form>
						<%						
					Next
				else%>
					<td Colspan="4">Sorry no items found</td>	<%	
				End if%>	 
        
        <tr>
          <td></td>
          <td><input onClick="Javascript:document.back.submit();" type="button" value="Go Back" name="Go Back" style="font-family: Verdana; font-size: 8pt"></td>
          <form method="POST" action="order_new1.asp" name="back">
          <input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
          <td></td>
          <td></td>
          </form>
        </tr>
      </table>
    </td>
  </tr>
</table>

	<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		<FORM Name="Navigate">
		<Input type="hidden" name="vDateDiff" value="<%=strDateDiff%>">
			<tr>					
				<td width="187"><%				
		
					if vCurrentPage <> 1 then%>
						<input type="button" value="FirstPage" Onclick="document.frmFirst.submit()" id=button1 name=button1 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) > 1 then%>
						<input type="button" value="PreviousPage" Onclick="document.frmPrevious.submit()" id=button3 name=button3 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%				
					if clng(vCurrentPage) < vPagecount then%>
						<input type="button" value="NextPage" Onclick="document.frmNext.submit()" id=button5 name=button5 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) <> vPagecount and vPagecount > 0  then%>
						<input type="button" value="LastPage" Onclick="document.frmLast.submit()" id=button7 name=button7 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>					
			</tr>
		</FORM>
	</table>
  </center>
</div>


	<FORM METHOD="post" NAME="frmFirst">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="First">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">				
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"			VALUE="<%=strProdCode%>">
		<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
		<Input type="hidden" name="vDateDiff" value="<%=strDateDiff%>">
	</FORM>

	<FORM METHOD="post" NAME="frmPrevious">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Previous">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"			VALUE="<%=strProdCode%>">
		<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
		<Input type="hidden" name="vDateDiff" value="<%=strDateDiff%>">
	</FORM>

	<FORM METHOD="post" NAME="frmNext">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Next">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"			VALUE="<%=strProdCode%>">
		<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
		<Input type="hidden" name="vDateDiff" value="<%=strDateDiff%>">
	</FORM>

	<FORM METHOD="post" NAME="frmLast">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Last">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"			VALUE="<%=strProdCode%>">
		<input type="hidden"  name="OrderNo"  value="<%=vOrderNo%>">
		<Input type="hidden" name="vDateDiff" value="<%=strDateDiff%>">
	</FORM>
  </center>
</div>


</body>

</html><%
If IsArray(arProduct) Then Erase arProduct
%>