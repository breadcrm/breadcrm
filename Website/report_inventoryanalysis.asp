<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form1.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form1.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form1.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form1.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form1.day.value=t.getDate()
	document.form1.month.value=t.getMonth()
	document.form1.year.value=t.getFullYear()
	
	document.form1.day1.value=t1.getDate()
	document.form1.month1.value=t1.getMonth()
	document.form1.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}
//-->
</SCRIPT>
<script language="Javascript">
<!--
//alert(Date.parse("01/05/2003") +' = ' + Date.parse("30/05/2003"))
function IsDataValid()
{
  if(document.form1.txtfrom.value=='' || document.form1.txtto.value=='')
  {
    alert('Please select valid dates for the from and until fields...');
    return false;
  }
  else if(isNaN(Date.parse(document.form1.txtfrom.value)))
  {
    alert('select a valid date in the from field...');
    return false;
  }
  else if(isNaN(Date.parse(document.form1.txtto.value)))
  {
    alert('select a valid date in the to field...');
    return false;
  }
  else if(Date.parse(document.form1.txtfrom.value) > Date.parse(document.form1.txtto.value))
  {
    alert('from date should be less than to date...');
    return false;
  }
  else
    return true;
 }
//-->
</script>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim recarray1, recarray2, recarray3
dim retcol
dim totpages
dim vSessionpage
dim i

dim ino
dim fno
dim fromdt
dim todt
dim fname
dim a1

fno = Request.Form("lstfno")
if fno <> "" then
  a1 = split(fno,"~")
  fno = a1(0)
  fname=a1(1)
else
  fname="facility"
end if
ino = Request.Form("chkino")
fromdt = Request.Form("txtfrom")
todt = Request.Form("txtto")

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if
'Response.Write "chkweekvno = " & Request.Form("chkweekvno") & "<br>"
'Response.Write "chkcno = " & Request.Form("chkcno") & "<br>"
'Response.Write "chkgno = " & Request.Form("chkgno") & "<br>"
'Response.end

totpages = Request.Form("pagecount")
select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(totpages) then	
			session("Currentpage")  = session("Currentpage") + 1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage") - 1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select
vSessionpage = session("Currentpage")

set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
if fno="" then fno=0
dim tfno
tfno=fno
if clng(fno) = 90 then tfno = "11,12,90,99"
'Response.Write fno & "," & tfno
if fno <> "" and fno <> 0 then
  set retcol = objBakery.GetIngredients("A",1,0,tfno)
  recarray1 = retcol("Ingredients")
end if

set retcol = objBakery.getFacilities("A",1,0)
recarray3 = retcol("Facilities")

if trim(fno) <> "" and isdate(fromdt) and isdate(todt) then
  if trim(ino) = "" then ino = "-1"
  set retcol = objBakery.InventoryAnalysis(tfno,fromdt,todt,ino,vSessionpage,0)
  totpages = retcol("pagecount")
  recarray2 = retcol("InvAnalysis")
end if%>
<div align="center">
<center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Inventory Management<br>
      &nbsp;</font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt">
        <form action="report_inventoryanalysis.asp" method="post" name="form1" onSubmit="return IsDataValid();">
        <tr>
          <td><b>Facility</b></td>
          <td>
             <select size="1" onChange="document.form1.submit()" name="lstfno" style="font-family: Verdana; font-size: 8pt">
             <option value="0~">Select</option><%
              if isarray(recarray3) then
                for i=0 to ubound(recarray3,2)
                  if recarray3(0,i) = 15 or recarray3(0,i) = 18 or recarray3(0,i) = 90 then%>
                     <option <%if clng(fno)=recarray3(0,i) then Response.Write "Selected"%> value="<%=recarray3(0,i)%>~<%=recarray3(1,i)%>"><%=recarray3(1,i)%></option><%
                  end if
                next
              else%>
                <option value="-1">No records found</option><%
              end if%>
             </select>
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">from</td>
                <td width="50%">Until</td>
              </tr>
              <tr>
                <td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form1.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form1.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form1.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom onFocus="blur();" size="12"  name="txtfrom" value="<%=replace(fromdt,"from","")%>" onChange="setCalCombo(this.form,document.form1.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form1.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form1.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form1.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=replace(todt,"to","")%>" onChange="setCalCombo1(this.form,document.form1.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2" bgcolor="#000000">
            <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0">
            <tr>
                <td width="75" bgcolor="#CCCCCC"><b>ALL</b></td>
                <td width="10" bgcolor="#CCCCCC"><input type="checkbox" name="chkino" value="-1" style="font-family: Verdana; font-size: 8pt"></td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
              </tr><%
              if isarray(recarray1) then
                for i=0 to ubound(recarray1,2) step 5%>
                <tr>
                <td width="75" align="right" bgcolor="#FFFFFF"><%if trim(recarray1(1,i)) = ""  then Response.Write("no name") else Response.Write(mid(recarray1(1,i),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkino" value="<%=recarray1(0,i)%>" style="font-family: Verdana; font-size: 8pt">
                </td>
                <%if i+1 <= ubound(recarray1,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray1(1,i+1)) = ""  then Response.Write("no name") else Response.Write(mid(recarray1(1,i+1),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkino" value="<%=recarray1(0,i+1)%>" style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+2 <= ubound(recarray1,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray1(1,i+2)) = ""  then Response.Write("no name") else Response.Write(mid(recarray1(1,i+2),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkino" value="<%=recarray1(0,i+2)%>" style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+3 <= ubound(recarray1,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray1(1,i+3)) = ""  then Response.Write("no name") else Response.Write(mid(recarray1(1,i+3),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkino" value="<%=recarray1(0,i+3)%>" style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                <%if i+4 <= ubound(recarray1,2) then%>
                <td align="right" bgcolor="#FFFFFF"><%if trim(recarray1(1,i+4)) = ""  then Response.Write("no name") else Response.Write(mid(recarray1(1,i+4),1,25))%></td>
                <td width="10" bgcolor="#FFFFFF">
                <input type="checkbox" name="chkino" value="<%=recarray1(0,i+4)%>" style="font-family: Verdana; font-size: 8pt">
                </td>
                <%else%>
                <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
                <%end if%>
                </tr><%
                next
              end if%>
            </table>
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <p align="right">&nbsp;&nbsp;<br>
            <input type="submit" value="Submit" name="B1" style="font-family: Verdana; font-size: 8pt"></p>
        </td>
        </tr>
        </form>
		 <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
      </table>
    <p align="left">
   <br>
    <b><font size="2">Inventory Management Report for &lt;<%=fname%>&gt; between
    &lt;<%=fromdt%>&gt; and &lt;<%=todt%>&gt;</font></b>
    <div align="right"><%
    if isarray(recarray2) then%>
       <font size="2"><b>Page <%=vsessionpage%> of <%=totpages%></b></font><%
    end if%>
    </div>
  <center>
      <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="50%" bgcolor="#CCCCCC" rowspan="2"><b>Ingredient</b></td>
          <td width="2%" bgcolor="#CCCCCC" rowspan="2" align="Left"><b>Unit</b></td>
          <td width="12%" bgcolor="#CCCCCC" rowspan="2" align="Left"><b>Qty. used for Production</b></td>
          <td width="12%" bgcolor="#CCCCCC" rowspan="2" align="Left"><b>Inventory Change</b></td>
          <td  width="12%" bgcolor="#CCCCCC" align="Left" colspan="2"><b>Difference</b></td>
          <td  width="12%" bgcolor="#CCCCCC" rowspan="2" align="Left"><b>% Spent for Production</b></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#CCCCCC"><b>In Kg</b></td>
          <td align="center" bgcolor="#CCCCCC"><b>Value</b></td>
        </tr><%
        if isarray(recarray2) then
          for i=0 to ubound(recarray2,2)%>
          <tr>
          <td  width="50%" align="Left"><%=recarray2(1,i)%></td>
          <td  width="2%" align="Left"><%=recarray2(2,i)%></td>
          <td  width="12%" align="right"><%=formatnumber(recarray2(3,i),2)%></td>
          <td  width="12%" align="Left"><%=recarray2(4,i)%></td>
          <td  width="12%" align="right"><%=formatnumber(recarray2(5,i),2)%></td>
          <td  width="12%" align="right">� <%=formatnumber(recarray2(6,i),2)%></td>
          <td  width="12%" align="Left"><%=recarray2(7,i)%></td>
          </tr><%
          next
        elseif instr(1,lcase(Request.ServerVariables("HTTP_REFERER")),"report_inventoryanalysis.asp") > 0 then%>
          <tr>
            <td colspan="6">
              <b>No Records matched...</b>
            </td>
          </tr><%
        end if%>
      </table>
    </center>
    </td>
  </tr>
</table>
</div>
<br>
<div align="center">
  <table width="720" border="0" cellspacing="1" cellpadding="2" align="center">
  <tr> 
  <% if clng(vSessionpage) <> 1 then %>
  <td>
    <form name="frmFirstPage" action="report_inventoryanalysis.asp" method="post">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="hidden" name="Direction" value="First">
    <input type="submit" name="submit" value="First Page">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) < clng(totpages) then %>
  <td>
    <form name="frmNextPage" action="report_inventoryanalysis.asp" method="post">
    <input type="hidden" name="Direction" value="Next">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Next Page">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) > 1 then %>
  <td>
    <form name="frmPreviousPage" action="report_inventoryanalysis.asp" method="post">
    <input type="hidden" name="Direction" value="Previous">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Previous Page">
    </form>
  </td>
  <%end if%>
  <%if clng(vSessionPage) <> clng(totpages) and clng(totpages) > 0 then %>
  <td>
    <form name="frmLastPage" action="report_inventoryanalysis.asp" method="post">
    <input type="hidden" name="Direction" value="Last">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Last Page">
    </form>
  </td>
  <% end if%>
  </tr>
  </table>
</div>
</body>
</html>