<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Response.Buffer
%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function ExportToExcelFile(){
	fromdate=document.form.txtfrom.value
	todate=document.form.txtto.value
	vstatus=document.form.Status.value
	document.location="ExportToExcelFile_mon_report_product.asp?fromdt=" + fromdate + "&todt=" +todate + "&status=" +vstatus
}
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}
function Hide()
{
	document.getElementById("HidePanel").style.display = "none";
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal();Hide()">
<!--#INCLUDE FILE="nav.inc" -->
<br>
<DIV id="HidePanel">
	<P align="center"><b><font size="2" face="Verdana" color="#0099CC">Report generation in progress, Please wait...</font></b></P>
	<p align="center"><img name="animation" src="images/LoadingAnimation.gif" border="0" WIDTH="201" HEIGHT="33"></p>
</DIV>

<%
strStatus=request("Status")
if strStatus="A" then
	strStatusLabel="Active Products"
elseif  strStatus="D" then
	strStatusLabel="Deleted Products"
else
	strStatusLabel=""
end if
dim objBakery
dim recarray
dim retcol
dim fromdt, todt,i
Dim UnitsSold,Price_A,AveragePrice,TotalTurnover,Price_A1,TotVAT,TotNetTO
if strStatus="0" then
	strStatus=""
elseif strStatus="" then
	strStatus="A"
end if

fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

if isdate(fromdt) and isdate(todt) then
set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.Display_product(fromdt,todt,strStatus)
recarray = retcol("Product")
end if
%>
<div align="center">
<center>
<table border="0" width="2500" cellspacing="0" cellpadding="5" style="font-family: Verdana; font-size: 8pt">

 <tr>
    <td width="100%"><b><font size="3">Product Full Report<br>
      &nbsp;</font></b>
	  <form method="post" action="mon_report_product.asp" name="form">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td><b>Status</b></td>
          <td><b>From:</b></td>
          <td><b>To:</b></td>
          <td></td>
        </tr>
        <tr>
          <td>
		  
		   <select name="Status">
		  	<option value="0">All</option>
			<option value="A" <% if strStatus="A" then%> selected="selected" <%end if%>>Active</option>
			<option value="D" <% if strStatus="D" then%> selected="selected" <%end if%>>Deleted</option>
		  </select>
		  </td>
          <td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom onFocus="blur();" size="12"  name="txtfrom" value="<%=fromdt%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=todt%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td> <td>
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
	  </form>
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
      	<table border="1" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
         <tr>
          <td colspan="41" align="left" height="30">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>
		<tr>
          <td colspan="41"><b>Product Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; <% if strStatus<>"" then%> - <%=strStatusLabel%><%end if%></b></td>
        </tr>
        <tr>
          <td width="110" bgcolor="#CCCCCC"><b>Product Code&nbsp;</b></td>
		  <td width="110" bgcolor="#CCCCCC"><b>Old Product Code</b></td>
          <td width="225" bgcolor="#CCCCCC"><b>Product Name&nbsp;</b></td>
		  
		  <td width="125" bgcolor="#CCCCCC"><b>Dough</b></td>
		  <td width="75" bgcolor="#CCCCCC"><b>Pre Size</b></td>
		  <td width="75" bgcolor="#CCCCCC"><b>Post Size</b></td>
		  <td width="50" bgcolor="#CCCCCC"><b>Shape</b></td>
		  <td width="100" bgcolor="#CCCCCC"><b>Slice</b></td>
		  <td width="150" bgcolor="#CCCCCC"><b>Facility</b></td>
		  <td width="70" bgcolor="#CCCCCC"><b>VAT Code</b></td>
		  <td width="70" bgcolor="#CCCCCC"><b>48 hrs Product</b></td>
		  <td width="70" bgcolor="#CCCCCC"><b>No of products</b></td>
		  <td width="70" bgcolor="#CCCCCC"><b>Type1</b></td>
		  <td width="70" bgcolor="#CCCCCC"><b>Type2</b></td>
          <td width="70" bgcolor="#CCCCCC" align="right"><b>Number of Units Sold&nbsp;</b></td>
          <td width="55" bgcolor="#CCCCCC" align="right"><b>Price A</b></td>
		  <td width="55" bgcolor="#CCCCCC" align="right"><b>Price A1</b></td>
		  <td width="55" bgcolor="#CCCCCC" align="right"><b>Price B</b></td>
		  <td width="55" bgcolor="#CCCCCC" align="right"><b>Price C</b></td>
		  <td width="55" bgcolor="#CCCCCC" align="right"><b>Price D</b></td>
		  <td width="80" bgcolor="#CCCCCC" align="center"><b>Ingredient Factor</b></td>
		  <td width="55" bgcolor="#CCCCCC" align="center"><b>Labour Cost</b></td>
		  <td width="55" bgcolor="#CCCCCC" align="center"><b>Packaging</b></td>
		  <td width="55" bgcolor="#CCCCCC" align="center"><b>Hygiene</b></td>
		  <td width="55" bgcolor="#CCCCCC" align="center"><b>Utilities</b></td>
		  <td width="80" bgcolor="#CCCCCC" align="center"><b>Rent Rate & other</b></td>
		  <td width="80" bgcolor="#CCCCCC" align="center"><b>VC Product</b></td>
          <td width="70" bgcolor="#CCCCCC" align="right"><b>Average Price Sold&nbsp;</b></td>
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Gross Turnover</b></td>  
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>VAT</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Net Turnover</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>New Product Code</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Bespoke For</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Outside of London Product</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Ordering Restriction</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>New Product Name</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Qty per box</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Product Type</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Product Category</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Product Group</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Delivery Temperature</b></td> 
        </tr>
       <%if isarray(recarray) then%>
		<%
		UnitsSold = 0
		Price_A = 0.00
		Price_A1 = 0.00
		AveragePrice = 0.00
		TotalTurnover = 0.00
		TotVAT = 0.00
		TotNetTO = 0.00
		%>
		<%for i=0 to UBound(recarray,2)

if (i mod 200=0) then
			Response.Flush()
		end if
%>
		<tr>
          <td><%=recarray(0,i)%></td>
		  <td><%=recarray(18,i)%></td>
          <td><%=recarray(1,i)%></td>
		  
		  <td><%=recarray(6,i)%></td>
		  <td><%=recarray(7,i)%></td>
		  <td><%=recarray(8,i)%></td>
		  <td><%=recarray(9,i)%></td>
		  <td><%=recarray(10,i)%></td>
		  <td><%=recarray(11,i)%></td>
		  <td><%=recarray(19,i)%>&nbsp;</td>
		  <td><%=recarray(20,i)%>&nbsp;</td>
		  <td><%=recarray(21,i)%>&nbsp;</td>
		  <td><%=recarray(16,i)%>&nbsp;</td>
		  <td><%=recarray(17,i)%>&nbsp;</td>
		  <td align="right" bgcolor="#CCCCCC"><%=recarray(2,i)%></td>
          <td align="right"><%if recarray(3,i)<> "" then Response.Write formatnumber(recarray(3,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(12,i)<> "" and not isnull(recarray(12,i)) then Response.Write formatnumber(recarray(12,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(13,i)<> "" and not isnull(recarray(13,i)) then Response.Write formatnumber(recarray(13,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(14,i)<> "" then Response.Write formatnumber(recarray(14,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(15,i)<> "" then Response.Write formatnumber(recarray(15,i),2) else Response.Write "0.00" end if%></td>
      	  
		  <td align="center"><%=recarray(22,i)%>&nbsp;</td>
		  <td align="center"><%=recarray(23,i)%>&nbsp;</td>
		  <td align="center"><%=recarray(24,i)%>&nbsp;</td>
		  <td align="center"><%=recarray(25,i)%>&nbsp;</td>
		  <td align="center"><%=recarray(26,i)%>&nbsp;</td>
		  <td align="center"><%=recarray(27,i)%>&nbsp;</td>
		 <td align="center"><%=recarray(31,i)%>&nbsp;</td>	
		  <td align="right" bgcolor="#CCCCCC"><%if recarray(4,i)<> "" then Response.Write formatnumber(recarray(4,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(5,i)<> "" then Response.Write formatnumber(recarray(5,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(30,i)<> "" then Response.Write formatnumber(recarray(30,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(29,i)<> "" then Response.Write formatnumber(recarray(29,i),2) else Response.Write "0.00" end if%></td>
         <td align="center"><%=recarray(32,i)%>&nbsp;</td>	
         <td align="center"><%=recarray(33,i)%>&nbsp;</td>	
         <td align="center"><%=recarray(34,i)%>&nbsp;</td>
         <td align="center"><%=recarray(35,i)%>&nbsp;</td>
         <td align="center"><%=recarray(36,i)%>&nbsp;</td>
         <td align="center"><%=recarray(37,i)%>&nbsp;</td>
		 <td><%=recarray(38,i)%>&nbsp;</td>
		 <td><%=recarray(39,i)%>&nbsp;</td>
		 <td><%=recarray(40,i)%>&nbsp;</td>
		 <td><%=recarray(41,i)%>&nbsp;</td> 		
         </tr>
          <%
          if recarray(2,i) <> "" then
          		UnitsSold = UnitsSold + recarray(2,i)
          End if 
		  'if recarray(12,i)<> "" and  isnumeric(recarray(12,i)) then 
          	'	Price_A1=Price_A1+formatnumber(recarray(12,i),2)
		  'END IF
		  'if recarray(3,i)<> "" then 
			'	Price_A = Price_A + formatnumber(recarray(3,i),2)
		  'End if
		  if recarray(4,i)<> "" then
				AveragePrice = AveragePrice + formatnumber(recarray(4,i),2)
		  End if
		  If recarray(5,i)<> "" then 
				TotalTurnover = TotalTurnover + formatnumber(recarray(5,i),2)
		  End if
		  
		  If recarray(30,i)<> "" then 
				TotVAT = TotVAT + formatnumber(recarray(30,i),2)
		  End if
		  
		  If recarray(29,i)<> "" then 
				TotNetTO = TotNetTO + formatnumber(recarray(29,i),2)
		  End if
		  		  		  
          %>
          <%next%>
          <tr>
       		<td colspan="14" align="right"><b>&nbsp;&nbsp;Grand Total</b></td>
          	<td align="right" bgcolor="#CCCCCC"><b><%=UnitsSold%></b></td>
          	<td align="right"><b><%'=formatnumber(Price_A,2)%></b>&nbsp;</td>
			<td align="right"><b><%'=formatnumber(Price_A1,2)%></b>&nbsp;</td>
			<td align="right" colspan="10">&nbsp;</td>
			
          	<td align="right" bgcolor="#CCCCCC"><b><%=formatnumber(AveragePrice,2)%></b></td>
          	<td align="right"><b><%=formatnumber(TotalTurnover,2)%></b></td>
          	<td align="right"><b><%=formatnumber(TotVAT,2)%></b></td>
          	<td align="right"><b><%=formatnumber(TotNetTO,2)%></b></td>
          	<td align="right"></td>
          	<td align="right"></td>
          	<td align="right"></td>
          	<td align="right"></td>
          	<td align="right"></td>
          	<td align="right"></td>
			<td align="right"></td>
			<td align="right"></td>
			<td align="right"></td>
			<td align="right"></td>
          </tr>
          <%else%>
          	<tr><td colspan="41"  bgcolor="#CCCCCC"><b>0 - No records matched...</b></td></tr>
		  <%end if%>
      </table>
    </td>
  </tr>
</table>
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;<br><br><br><br><br><br>
</center>
</div>
</body>
<%if IsArray(recarray) then erase recarray %>
</html>
