<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%
strSearchBy = replace(Request.Form("SearchBy"),"'","''")
strSearchText = replace(Request.Form("SearchText"),"'","''")

if request.form("delete")="yes" then
	Set obj = server.CreateObject("bakery.customer")
	obj.SetEnvironment(strconnection)
	vSGNo=request("sgno")
	vSGName=request.form("gname")
	strDelete = obj.DeleteCustomerGroup(vSGNo)
	If strDelete <> "Fail" Then
		LogAction "Customer Group Deleted", "No: " & vSGNo , ""
	End If
	set obj=nothing
end if
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{

  if (theForm.SubGroupName.value == "")
  {
    alert("Please enter a value for the \"Sub Group Name\" field.");
    theForm.SubGroupName.focus();
    return (false);
  }

  return (true);
}

function validate(frm1){
	if(confirm("Would you like to delete " + frm1.gname.value + " ?")){
		frm1.submit();		
	}
}
//-->
</Script>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<br>
<table border="0" width="100%" cellspacing="0" align="center" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
<tr>
   <td width="100%">&nbsp;&nbsp;<b><font size="3">Customer Groups</font></b></td>
  </tr>
</table>
<br>
<table border="0" align="center" width="980" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%" align="center"><b><font size="2">List of Customer  Groups</font></b><br>
        <br>
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#111111">
		<form method="post" action="CustomerGroup.asp" name="frmForm">      
        <tr>
           <td>
		   <select name="SearchBy">
		   		<option value="">- - - Select - - - </option>
				<option value="GNo" <% if (strSearchBy="GNo") then%> selected="selected" <%end if%>>Group No</option>
				<option value="GName" <% if (strSearchBy="GName") then%> selected="selected" <%end if%>>Group Name</option>
				<option value="Email" <% if (strSearchBy="Email") then%> selected="selected" <%end if%>>Email</option>
				<option value="ElectronicInvoice" <% if (strSearchBy="ElectronicInvoice") then%> selected="selected" <%end if%>>CSV File Group Invoice</option>
				<option value="DailyAutomatedGroupInvoice" <% if (strSearchBy="DailyAutomatedGroupInvoice") then%> selected="selected" <%end if%>>Daily Automated Group Invoice</option>
				<option value="DailyAutomatedGroupCredit" <% if (strSearchBy="DailyAutomatedGroupCredit") then%> selected="selected" <%end if%>>Daily Automated Group Credit</option>
				 <option value="PricelessInvoiceCredit" <% if (strSearchBy="PricelessInvoiceCredit") then%> selected="selected" <%end if%>>Priceless Invoice/Credit</option> 
				<option value="Notes" <% if (strSearchBy="Notes") then%> selected="selected" <%end if%>>Notes</option>
		  </select>
		   </td>
		   <td>
		   <input type="text" name="SearchText" value="<%=strSearchText%>" size="20" style="font-family: Verdana; font-size: 8pt">
          </td>
		  <td>
		  <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt">
		  </td>
        </tr>
       </form>
	    </table>
		<table width="100%" border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#111111">
	
	   <tr>
	  	 <td height="15"><a href="CustomerGroupAdd.asp"><strong><font color="#000099">Add New Customer Group</font></strong></a></td>
	   </tr>
	   <% if strDelete="Ok" then%>
	   <tr>
	  	 <td height="25" align="center"><font color="#FF0000"> Group <b><%=vSGName%></b> has been deleted successfully.</font></td>
	   </tr>
	   <% elseif strDelete="Fail" then%>
	   <tr>
	  	 <td height="25" align="center"><font color="#FF0000"> Group <b><%=vSGName%></b> can not be deleted, as it is already assigned to a customer (s).</font></td>
	   </tr>
	   <%end if%>
      </table>
		
      

	<%
	vPageSize = 25
	vpagecount = Request.Form("pagecount") 
	
	select case Request.Form("Direction")
		case ""
			session("Currentpage")  = 1
		case "Next"
			if clng(session("Currentpage")) < clng(vpagecount) then	
				session("Currentpage")  = session("Currentpage")+1
			End if 
		case "Previous"
			if session("Currentpage") > 1 then
				session("Currentpage")  = session("Currentpage")-1
			end if
		case "First"
			session("Currentpage") = 1
		case "Last"
			session("Currentpage")  = Request.Form("pagecount")
	end select
	vSessionpage = session("Currentpage")
	vsessionpage = 0
	set objBakery = server.CreateObject("Bakery.Customer")
	objBakery.SetEnvironment(strconnection)
	if (strSearchBy="ElectronicInvoice") then
		if (lcase(strSearchText)="yes") then
			strSearchText="1"
		elseif (lcase(strSearchText)="no") then
			strSearchText="0"
		end if
	end if
	
	if (strSearchBy="DailyAutomatedGroupInvoice") then
		if (lcase(strSearchText)="yes") then
			strSearchText="1"
		elseif (lcase(strSearchText)="no") then
			strSearchText="0"
		end if
	end if
	
	if (strSearchBy="DailyAutomatedGroupCredit") then
		if (lcase(strSearchText)="yes") then
			strSearchText="1"
		elseif (lcase(strSearchText)="no") then
			strSearchText="0"
		end if
	end if
	
	if (strSearchBy="PricelessInvoiceCredit") then
		if (lcase(strSearchText)="yes") then
			strSearchText="1"
		elseif (lcase(strSearchText)="no") then
			strSearchText="0"
		end if
	end if
	
	set CustomerCol = objBakery.CustomerGroupList(strSearchBy, strSearchText,"")
	arCustomerGroup = CustomerCol("CustomerGroupList")
	set CustomerCol = nothing
	set objBakery = nothing
	If isArray(arCustomerGroup) Then
	vRecordcount=ubound(arCustomerGroup,2)+1
	%>
 		<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
		<tr bgcolor="#FFFFFF">
			<td colspan="8" align = "right" height="25"><b>Total number of Customer  Groups: <%=vRecordcount%></b></td>
		</tr>
		</table>	
   		<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" bgcolor="#999999">
		<tr height="25">
		  <td bgcolor="#CCCCCC" width="30"><nobr><b>No</b></nobr></td>
          <td bgcolor="#CCCCCC" width="60"><nobr><b>Group No&nbsp;</b></nobr></td>
          <td bgcolor="#CCCCCC" width="220"><b>Group Name</b></td>
		  <td bgcolor="#CCCCCC" width="110"><b>CSV File Group Invoice</b></td>
		  <td bgcolor="#CCCCCC" width="110"><b>Daily Automated Invoice</b></td>
		  <td bgcolor="#CCCCCC" width="110"><b>Daily Automated Credit</b></td>
		  
         <td bgcolor="#CCCCCC" width="110"><b>Priceless Invoice/Credit</b></td>
		 
		  <td bgcolor="#CCCCCC" align="center"><b>Action</b></td>
		
        </tr>
		<% 
		For i = 0 To ubound(arCustomerGroup,2)
			if (i mod 2 =0) then
				strBgColour="#FFFFFF"
			else
				strBgColour="#E6E6E6"
			end if
		%>         
        <tr bgcolor="<%=strBgColour%>">
              <td align="left"><%=i+1%>.&nbsp;</td>
			  <td align="left"><%=arCustomerGroup(0,i)%></td>
              <td><%=arCustomerGroup(1,i)%></td>              
			  <td><%=arCustomerGroup(3,i)%></td>
			  <td><%=arCustomerGroup(4,i)%></td>
			  <td><%=arCustomerGroup(5,i)%></td>
			   
			  <td><%=arCustomerGroup(15,i)%></td>
			  
			  <td align="center">
			   <table cellpadding="0" cellspacing="0" border="0" width="100%">
			   <tr>
			   <form method="post" action="CustomerGroupView.asp">
			   <td width="75" align="center">
			    <input type = "hidden" name="sgno" value = "<%=arCustomerGroup(0,i)%>">
				<input type="submit" value=" View " name="B1" style="font-family: Verdana; font-size: 8pt">
			   </td>
			   </form>
			   <form method="post" action="CustomerGroupAdd.asp" enctype="multipart/form-data">
			   <td width="75" align="center">
			    <input type = "hidden" name="sgno" value = "<%=arCustomerGroup(0,i)%>">
			   <!-- <input type = "hidden" name="Action" value = "Edit">-->
				<input type="submit" value=" Edit " name="B1" style="font-family: Verdana; font-size: 8pt">
			   </td>
			   </form>
			   <form method="POST" action="CustomerGroup.asp" name="frm">
			  	<td width="75" align="center">
				  <input type = "hidden" name="sgno" value = "<%=arCustomerGroup(0,i)%>">
				  <input type = "hidden" name="gname" value = "<%=arCustomerGroup(1,i)%>">
				  <input type = "hidden" name="delete" value = "yes">
				  <input type="button" onClick="validate(this.form);" value="Delete" name="B1" style="font-family: Verdana; font-size: 8pt">
				</td>
			   </form>
			  </tr>
			  </table>
			  </td>	
        </tr>
	<%
		Next
		set CustomerGroupCol = nothing
		set objBakery1 = nothing
	Else
	%>
    <tr>
    	   <td colspan="78 align="center" bgcolor="#FFFFFF"><strong>Sorry no items found</strong></td>
    </tr>
	<%End if%>
    </table>
    </td>
  </tr>
</table>
<a name="bottom"></a>
<br><br>
</form>
</body>
</html>