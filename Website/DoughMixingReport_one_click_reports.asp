<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
//-->
</SCRIPT>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim retcol
dim fromdt, todt , i
Dim TotQuantity,TotTurnover
dim arrProductCategory
dim arrSecondMixProductCategory
dim vDoughType

'from Scenario-2
dim retsecondmixcol

'from Scenario-3
dim recsecondmixarrayNew
dim retsecondmixcolNew

fromdt = Request.form("deldate")
todt = Request.Form("deldate")
vDoughType = Request.Form("dtype")
vFacility = Request.Form("facility")
strlblDoughType=request("lblDoughType")
strDoughType = "-1"

if Trim(fromdt) = "" or Trim(todt) = "" or trim(vDoughType) = "" or trim(vFacility) = "" then response.redirect "LoginInternalPage.asp"

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
   todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayDoughNameDoughMixingReport(vDoughType)
vDoughTypearray =  detail("DoughName")

set detail = Nothing
%>

<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
 <tr>
    <td width="100%"><b><font size="3"><%if vDoughType = 1 then if vFacility = "0" then response.Write("Direct") else response.Write("Stanmore English") end if else response.Write("48 Hours") end if%> Dough Mixing Report<br>
      &nbsp;</font></b>
	  <form method="post" action="DoughMixingReport_one_click_reports.asp?dtype=<%=vDoughType%>&facility=<%=vFacility%>" name="form" onSubmit="return CheckSearch(this)">
	  </form>
<%

set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
dim NoOfLoops

If strDoughType = "-1" Then
	NoOfLoops = UBound(vDoughTypearray,2)
Else
	NoOfLoops = 0
End If

dim IsDataAvailbale, IsSecondMixDataAvailbale
IsDataAvailbale = 0 'default value is 0
IsSecondMixDataAvailbale = 0 'default value is 0

for d=0 to NoOfLoops

dim arrIName
dim recarray
dim vReportType
'from Scenario-2
dim arrSecondMixIName
dim recsecondmixarray

Set objGeneral = Server.CreateObject("bakery.general")
objGeneral.SetEnvironment(strconnection)
If strDoughType = "-1" Then
	set retdoughtypecol = objGeneral.DoughMixingReportType(cint(vDoughTypearray(0,d)))
	recarrayreportype = retdoughtypecol("ReportType")
Else
	set retdoughtypecol = objGeneral.DoughMixingReportType(cint(strDoughType))
	recarrayreportype = retdoughtypecol("ReportType")
End If

if isarray(recarrayreportype) then
	vReportType = recarrayreportype(0,0)
end if

if vReportType = "" then
	vReportType = "1"
end if

If isdate(fromdt) and isdate(todt) Then
	If vReportType = "1" Then'FIRST CONDITION
		If strDoughType = "-1" Then
			set retcol = objBakery.DoughMixingReport(cint(vDoughTypearray(0,d)), fromdt, todt, cint(vFacility),0,0)
			recarray = retcol("DoughMixingReport")
		Else
			set retcol = objBakery.DoughMixingReport(cint(strDoughType), fromdt, todt, cint(vFacility),0,0)
			recarray = retcol("DoughMixingReport")
		End If
	Elseif vReportType = "0" Then'SECOND CONDITION
		If strDoughType = "-1" Then
			set retcol = objBakery.DoughMixSummaryReport(cint(vDoughTypearray(0,d)), fromdt, todt, cint(vFacility),0,0)
			recarray = retcol("DoughMixSummaryReport")
			
			set retsecondmixcol = objBakery.DoughSecondMixReport(cint(vDoughTypearray(0,d)),fromdt,todt)
			recsecondmixarray=retsecondmixcol("DoughSecondMixReport")
			
			set retsecondmixcolNew = objBakery.DoughSecondMixReportNew(cint(vDoughTypearray(0,d)), fromdt, todt, cint(vFacility),0,0)
			recsecondmixarrayNew=retsecondmixcolNew("DoughSecondMixReport")
		Else
			set retcol = objBakery.DoughMixSummaryReport(cint(strDoughType), fromdt, todt, cint(vFacility),0,0)
			recarray = retcol("DoughMixSummaryReport")
			
			set retsecondmixcol = objBakery.DoughSecondMixReport(cint(strDoughType),fromdt,todt)
			recsecondmixarray=retsecondmixcol("DoughSecondMixReport")
			
			set retsecondmixcolNew = objBakery.DoughSecondMixReportNew(cint(strDoughType), fromdt, todt, cint(vFacility),0,0)
			recsecondmixarrayNew=retsecondmixcolNew("DoughSecondMixReport")
		End If
	Elseif vReportType = "3" Then'THIRD CONDITION
		'get the data for the first mix
		If strDoughType = "-1" Then
			set retcol = objBakery.DoughMixingReportTwoStageMixing(cint(vDoughTypearray(0,d)), fromdt, todt, 0, cint(vFacility),0,0)
			recarray = retcol("DoughMixingReport")
		Else
			set retcol = objBakery.DoughMixingReportTwoStageMixing(cint(strDoughType), fromdt, todt, 0, cint(vFacility),0,0)
			recarray = retcol("DoughMixingReport")
		End If
		
		'get the data for the secod mix
		If strDoughType = "-1" Then
			set retsecondmixcol = objBakery.DoughMixingReportTwoStageMixing(cint(vDoughTypearray(0,d)), fromdt, todt, 1, cint(vFacility),0,0)
			recsecondmixarray = retsecondmixcol("DoughMixingReport")
		Else
			set retsecondmixcol = objBakery.DoughMixingReportTwoStageMixing(cint(strDoughType), fromdt, todt, 1, cint(vFacility),0,0)
			recsecondmixarray = retsecondmixcol("DoughMixingReport")
		End If
	End If
End If

'if (Request("Search")<>"") then
	If vReportType = "1" Then'FIRST CONDITION
		if isarray(recarray) then
			'IName -Ingredient Name
			set objIName = objBakery.GetFilterData(recarray,3,arrIName)
			arrIName = objIName("FilterData")
			
			'Production Category
			set objProductCategory = objBakery.GetFilterData(recarray,2,arrProductCategory)
			arrProductCategory = objProductCategory("FilterData")
			
			'OK, it says that there are data availble to show
			IsDataAvailbale = 1
		Else
			arrIName = "Fail"
			arrProductCategory = "Fail"
		end if
	ElseIf vReportType = "0" Then'SECOND CONDITION
		'first mix
		if isarray(recarray) then
			'Production Category
			set objProductCategory = objBakery.GetFilterData(recarray,2,arrProductCategory)
			arrProductCategory = objProductCategory("FilterData")
			
			'IName -Ingredient Name
			set objIName = objBakery.GetFilterData(recarray,3,arrIName)
			arrIName = objIName("FilterData")
			
			'OK, it says that there are data availble to show
			IsDataAvailbale = 1
		else
			arrProductCategory = "Fail"
			arrIName = "Fail"
		end if
		
		'second mix
		if isarray(recsecondmixarray) then
			'DName
			set objSecondmixDName = objBakery.GetFilterData(recsecondmixarray,1,arrSecondMixDName)
			arrSecondMixDName = objSecondmixDName("FilterData")
						
			'Production Category
			set objSecondmixProductCategory = objBakery.GetFilterData(recsecondmixarray,2,arrSecondMixProductCategory)
			arrSecondMixProductCategory = objSecondmixProductCategory("FilterData")
			
			'IName -Ingredient Name
			set objSecondmixIName = objBakery.GetFilterData(recsecondmixarray,3,arrSecondMixIName)
			arrSecondMixIName = objSecondmixIName("FilterData")
		else
			arrSecondMixDName = "Fail"
			arrSecondMixProductCategory = "Fail"
			arrSecondMixIName = "Fail"
		end if
		
		'second mix - NEW
		If isarray(recsecondmixarrayNew) Then
			'DName
			set objSecondmixDNameNew = objBakery.GetFilterData(recsecondmixarrayNew,1,arrSecondMixDNameNew)
			arrSecondMixDNameNew = objSecondmixDNameNew("FilterData")
			
			'IName -Ingredient Name
			set objSecondmixINameNew = objBakery.GetFilterData(recsecondmixarrayNew,2,arrSecondMixINameNew)
			arrSecondMixINameNew = objSecondmixINameNew("FilterData")
		Else
			arrSecondMixDNameNew = "Fail"
			arrSecondMixINameNew = "Fail"
		End If
	ElseIf vReportType = "3" Then'THIRD CONDITION
		'first mix
		if isarray(recarray) then
			'IName -Ingredient Name
			set objIName = objBakery.GetFilterData(recarray,3,arrIName)
			arrIName = objIName("FilterData")
			
			'Production Category
			set objProductCategory = objBakery.GetFilterData(recarray,2,arrProductCategory)
			arrProductCategory = objProductCategory("FilterData")
			
			'OK, it says that there are data availble to show
			IsDataAvailbale = 1
		Else
			arrIName = "Fail"
			arrProductCategory = "Fail"
		end if
		
		'second mix
		if isarray(recsecondmixarray) then
			'IName - Ingredent Name
			set objSecondMixIName = objBakery.GetFilterData(recsecondmixarray,3,arrSecondMixIName)
			arrSecondMixIName = objSecondMixIName("FilterData")
			
			'Production Category
			set objSecondMixProductCategory = objBakery.GetFilterData(recsecondmixarray,2,arrSecondMixProductCategory)
			arrSecondMixProductCategory = objSecondMixProductCategory("FilterData")
			
			'OK, it says that there are data availble to show
			IsSecondMixDataAvailbale = 1
		else
			arrSecondMixIName = "Fail"
			arrSecondMixProductCategory = "Fail"
		end if
	End If
'end if

set object = Nothing
%>

<%If vReportType = "1" Then%>
<!-- START OF MAIN DOUGH WITHOUT SECOND MIX CATEGORY -->
<%'if (Request("Search")<>"") then%>
<%if isarray(recarray) then%>
<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<%
	if strDoughType = "-1" then
		If isarray(arrProductCategory) then
%>
 <form method="post"name="frmReport">
<tr>
  <td width="100%"  colspan='<%=UBound(arrProductCategory) %>' height="40"><b>Dough Name - <label id="lblDoughType"><%=vDoughTypearray(1,d)%></label></b></td>
</tr>
</form>
<%
		End If
	Else
%>
 <form method="post"name="frmReport">
<tr>
  <td width="100%"  colspan='<%=UBound(arrProductCategory) %>' height="40"><b>Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
</tr>
</form>
<%
	End If ' End for [strDoughType = "-1"] check
	
    'Set objGeneral = Server.CreateObject("bakery.general")
    dim objIndex
    dim n, m, p
    dim weight, arrProductCategoryTotal (50), arrProductCategoryTotal1 (50)
    
    if not isempty(arrIName) then
        objGeneral.BubbleSort(arrIName)
    end if
        
    if not isempty(arrProductCategory) then        
        objGeneral.BubbleSort(arrProductCategory)
    end if
	
	set objGeneral = Nothing
%>

<tr>
<td colspan='<%=UBound(arrProductCategory) %>'>
	<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
	<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
    <%if isarray(arrIName) then%>
		<td width="170" bgcolor="#CCCCCC">&nbsp;</td>
	    <%for m=0 to UBound(arrIName)
			arrProductCategoryTotal(m)=0
			arrProductCategoryTotal1(m)=0
		%>
	        <td align="right" bgcolor="#CCCCCC"><%=arrIName(m)%></td>	
	    <%next%>
			<td align="right" bgcolor="#CCCCCC" width="120">Total Weight (Kg)</td>	
    <%
	else
		If strDoughType <> "-1" Then
	%>
		<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
	<%
		End If
	end if
	%>
	</tr>

<%
If isarray(arrProductCategory) Then
	strGrandTotalWeight1 = 0
	for n1=0 to UBound(arrProductCategory)
		strTotalWeight1 = 0
		
		for p1=0 to UBound(arrIName)
			weight1 = 0
			for c1=0 to UBound(recarray,2)
				if(recarray(3,c1)=arrIName(p1) and recarray(2,c1) = arrProductCategory(n1)) then
				   weight1 = recarray(4,c1)
				end if
			next
			strTotalWeight1 = cdbl(strTotalWeight1)+ cdbl(weight1) 
			arrProductCategoryTotal1(p1) = cdbl(arrProductCategoryTotal1(p1))+cdbl(weight1)
		next
	strGrandTotalWeight1 = strGrandTotalWeight1 + strTotalWeight1
	next
End If
%>

<%if isarray(arrProductCategory) then%>

<tr bgcolor="#CCCCCC" style="font-weight:bold">
	<td>Total (Kg)</td>
	<%for m1=0 to UBound(arrIName)%>
		<td align="right"><%=formatnumber(arrProductCategoryTotal1(m1),3)%></td>
	<%next%>
	<td align="right"><%=formatnumber(strGrandTotalWeight1,3)%></td>
</tr>

	<%
	strGrandTotalWeight=0
	for n=0 to UBound(arrProductCategory)
		if (n mod 2 =0) then
			strBgColour="#FFFFFF"
		else
			strBgColour="#F3F3F3"
		end if
	%>
	    <tr bgcolor="<%=strBgColour%>">
	        <td><%=arrProductCategory(n)%></td>
    	    
	        <%
			strTotalWeight=0
			
	        for p=0 to UBound(arrIName)
	            weight=0
    	        
	            for c=0 to UBound(recarray,2)
	                if(recarray(3,c)=arrIName(p) and recarray(2,c) = arrProductCategory(n)) then
	                   weight = recarray(4,c)
	                end if	            	
				next	           
    	            
	        %>
	            <td align="right"><%=formatnumber(weight,3)%></td>
	        <%			
			strTotalWeight=cdbl(strTotalWeight)+ cdbl(weight) 
			arrProductCategoryTotal(p)=cdbl(arrProductCategoryTotal(p))+cdbl(weight)
	        next
			
	        %>	    
    	   	<td align="right" style="font-weight:bold"><%=formatnumber(strTotalWeight,3)%></td> 
	    </tr>
		
	<%
	
	strGrandTotalWeight = strGrandTotalWeight + strTotalWeight
	next
	%>
	<!--
	'Total at the botton were commented by Ramanan on 4th Feb 2010
	<tr bgcolor="#CCCCCC" style="font-weight:bold">
		<td>Total (Kg)</td>
		<%for m=0 to UBound(arrIName)%>
			<td align="right"><%=formatnumber(arrProductCategoryTotal(m),4)%></td>
		<%next%>
		<td align="right"><%=formatnumber(strGrandTotalWeight,4)%></td>
	</tr>
	-->
<%end if%>
</table>
</td>
</tr>
</table>
<%Else
	If strDoughType <> "-1" Then
%>
	<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
		<form method="post"name="frmReport">
		<tr>
  			<td width="100%"  colspan='' height="40"><b>Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
		</tr>
		</form>
		<tr>
			<td colspan=''>
				<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
					<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22">
						<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%
	End IF
%>
<%'end if%>
<%end if%>
<%
if IsArray(recarray) then erase recarray
if IsArray(arrIName) then erase arrIName
%>
<!-- END OF MAIN DOUGH REPORT WITHOUT SECOND MIX CATEGORY -->

<%
ElseIf vReportType = "3" Then
%>
<!-- START OF TWO STAGE MIXING CATEGORY -->

<%'if (Request("Search")<>"") then%>
<%if isarray(recarray) then%>
<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<%
	if strDoughType = "-1" then
		If isarray(arrProductCategory) then
%>
 <form method="post"name="frmReport">
<tr>
  <td width="100%"  colspan='<%=UBound(arrProductCategory) %>' height="40"><b>Dough Name - <label id="lblDoughType"><%=vDoughTypearray(1,d)%></label></b></td>
</tr>
</form>
<%
		End If
	Else
%>
 <form method="post"name="frmReport">
<tr>
  <td width="100%"  colspan='<%=UBound(arrProductCategory) %>' height="40"><b>Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
</tr>
</form>
<%
	End If ' End for [strDoughType = "-1"] check
	
    'Set objGeneral = Server.CreateObject("bakery.general")
    'dim objIndex
    'dim n, m, p
    'dim weight, arrProductCategoryTotal (50), arrProductCategoryTotal1 (50)
    
    if not isempty(arrIName) then
        objGeneral.BubbleSort(arrIName)
    end if
        
    if not isempty(arrProductCategory) then        
        objGeneral.BubbleSort(arrProductCategory)
    end if
%>

<tr>
<td colspan='<%=UBound(arrProductCategory) %>'>
	<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">
	<tr bgcolor="#CCCCCC" style="font-weight:bold" height="22">
		<td colspan="<%=ubound(arrIName) + 3%>">First mix</td>
	</tr>
	<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
    <%if isarray(arrIName) then%>
		<td width="170" bgcolor="#CCCCCC">&nbsp;</td>
	    <%for m=0 to UBound(arrIName)
			arrProductCategoryTotal(m)=0
			arrProductCategoryTotal1(m)=0
		%>
	        <td align="right" bgcolor="#CCCCCC"><%=arrIName(m)%></td>	
	    <%next%>
			<td align="right" bgcolor="#CCCCCC" width="120">Total Weight (Kg)</td>	
    <%
	else
		If strDoughType <> "-1" Then
	%>
		<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
	<%
		End If
	end if
	%>
	</tr>

<%
If isarray(arrProductCategory) Then
	strGrandTotalWeight1 = 0
	for n1=0 to UBound(arrProductCategory)
		strTotalWeight1 = 0
		
		for p1=0 to UBound(arrIName)
			weight1 = 0
			for c1=0 to UBound(recarray,2)
				if(recarray(3,c1)=arrIName(p1) and recarray(2,c1) = arrProductCategory(n1)) then
				   weight1 = recarray(4,c1)
				end if
			next
			strTotalWeight1 = cdbl(strTotalWeight1)+ cdbl(weight1) 
			arrProductCategoryTotal1(p1) = cdbl(arrProductCategoryTotal1(p1))+cdbl(weight1)
		next
	strGrandTotalWeight1 = strGrandTotalWeight1 + strTotalWeight1
	next
End If
%>

<%if isarray(arrProductCategory) then%>

<tr bgcolor="#CCCCCC" style="font-weight:bold">
	<td>Total (Kg)</td>
	<%for m1=0 to UBound(arrIName)%>
		<td align="right"><%=formatnumber(arrProductCategoryTotal1(m1),3)%></td>
	<%next%>
	<td align="right"><%=formatnumber(strGrandTotalWeight1,3)%></td>
</tr>

	<%
	strGrandTotalWeight=0
	for n=0 to UBound(arrProductCategory)
		if (n mod 2 =0) then
			strBgColour="#FFFFFF"
		else
			strBgColour="#F3F3F3"
		end if
	%>
	    <tr bgcolor="<%=strBgColour%>">
	        <td><%=arrProductCategory(n)%></td>
    	    
	        <%
			strTotalWeight=0
			
	        for p=0 to UBound(arrIName)
	            weight=0
    	        
	            for c=0 to UBound(recarray,2)
	                if(recarray(3,c)=arrIName(p) and recarray(2,c) = arrProductCategory(n)) then
	                   weight = recarray(4,c)
	                end if	            	
				next	           
    	            
	        %>
	            <td align="right"><%=formatnumber(weight,3)%></td>
	        <%			
			strTotalWeight=cdbl(strTotalWeight)+ cdbl(weight) 
			arrProductCategoryTotal(p)=cdbl(arrProductCategoryTotal(p))+cdbl(weight)
	        next
			
	        %>	    
    	   	<td align="right" style="font-weight:bold"><%=formatnumber(strTotalWeight,3)%></td> 
	    </tr>
		
	<%
	
	strGrandTotalWeight = strGrandTotalWeight + strTotalWeight
	next
	%>
<%end if%>
</table>
</td>
</tr>
</table>
<%Else
	If strDoughType <> "-1" Then
%>
	<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
		<form method="post"name="frmReport">
		<tr>
  			<td width="100%"  colspan='' height="40"><b>Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
		</tr>
		</form>
		<tr>
			<td colspan=''>
				<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
					<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22">
						<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found for first mix.</font></p></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%
	End IF
%>
<%end if%>

<%
	'This is for second mix
	if isarray(recsecondmixarray) then
%>
<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<%
    Set objSecondMixGeneral = Server.CreateObject("bakery.general")
	objSecondMixGeneral.SetEnvironment(strconnection)
    'dim objIndex
    'dim n, m, p
    dim secondmixweight, arrSecondMixProductCategoryTotal (50), arrSecondMixProductCategoryTotal1 (50)
    
    if not isempty(arrSecondMixIName) then
        objSecondMixGeneral.BubbleSort(arrSecondMixIName)
    end if
        
    if not isempty(arrSecondMixProductCategory) then        
        objSecondMixGeneral.BubbleSort(arrSecondMixProductCategory)
    end if
	
	set objSecondMixGeneral = Nothing
%>

<tr>
<td colspan='<%=UBound(arrSecondMixProductCategory) %>'>
	<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
	<tr bgcolor="#CCCCCC" style="font-weight:bold" height="22"> 
		<td colspan="<%=ubound(arrSecondMixIName) + 3%>">Second mix</td>
	</tr>
	<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
    <%if isarray(arrSecondMixIName) then%>
		<td width="170" bgcolor="#CCCCCC">&nbsp;</td>
	    <%for m=0 to UBound(arrSecondMixIName)
			arrSecondMixProductCategoryTotal(m)=0
			arrSecondMixProductCategoryTotal1(m)=0
		%>
	        <td align="right" bgcolor="#CCCCCC"><%=arrSecondMixIName(m)%></td>	
	    <%next%>
			<td align="right" bgcolor="#CCCCCC" width="120">Total Weight (Kg)</td>	
    <%
	else
		If strDoughType <> "-1" Then
	%>
		<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
	<%
		End If
	end if
	%>
	</tr>

<%
If isarray(arrSecondMixProductCategory) Then
	strSecondMixGrandTotalWeight1 = 0
	for n1=0 to UBound(arrSecondMixProductCategory)
		strSecondMixTotalWeight1 = 0
		
		for p1=0 to UBound(arrSecondMixIName)
			secondmixweight1 = 0
			for c1=0 to UBound(recsecondmixarray,2)
				if(recsecondmixarray(3,c1)=arrSecondMixIName(p1) and recsecondmixarray(2,c1) = arrSecondMixProductCategory(n1)) then
				   secondmixweight1 = recsecondmixarray(4,c1)
				end if
			next
			strSecondMixTotalWeight1 = cdbl(strSecondMixTotalWeight1)+ cdbl(secondmixweight1) 
			arrSecondMixProductCategoryTotal1(p1) = cdbl(arrSecondMixProductCategoryTotal1(p1))+cdbl(secondmixweight1)
		next
	strSecondMixGrandTotalWeight1 = strSecondMixGrandTotalWeight1 + strSecondMixTotalWeight1
	next
End If
%>

<%if isarray(arrSecondMixProductCategory) then%>

<tr bgcolor="#CCCCCC" style="font-weight:bold">
	<td>Total (Kg)</td>
	<%for m1=0 to UBound(arrSecondMixIName)%>
		<td align="right"><%=formatnumber(arrSecondMixProductCategoryTotal1(m1),3)%></td>
	<%next%>
	<td align="right"><%=formatnumber(strSecondMixGrandTotalWeight1,3)%></td>
</tr>

	<%
	strSecondMixGrandTotalWeight=0
	for n=0 to UBound(arrSecondMixProductCategory)
		if (n mod 2 =0) then
			strBgColour="#FFFFFF"
		else
			strBgColour="#F3F3F3"
		end if
	%>
	    <tr bgcolor="<%=strBgColour%>">
	        <td><%=arrSecondMixProductCategory(n)%></td>
    	    
	        <%
			strSecondMixTotalWeight=0
			
	        for p=0 to UBound(arrSecondMixIName)
	            secondmixweight=0
    	        
	            for c=0 to UBound(recsecondmixarray,2)
	                if(recsecondmixarray(3,c)=arrSecondMixIName(p) and recsecondmixarray(2,c) = arrSecondMixProductCategory(n)) then
	                   secondmixweight = recsecondmixarray(4,c)
	                end if	            	
				next	           
    	            
	        %>
	            <td align="right"><%=formatnumber(secondmixweight,3)%></td>
	        <%			
			strSecondMixTotalWeight=cdbl(strSecondMixTotalWeight)+ cdbl(secondmixweight) 
			arrSecondMixProductCategoryTotal(p)=cdbl(arrSecondMixProductCategoryTotal(p))+cdbl(secondmixweight)
	        next
			
	        %>	    
    	   	<td align="right" style="font-weight:bold"><%=formatnumber(strSecondMixTotalWeight,3)%></td> 
	    </tr>
		
	<%
	
	strSecondMixGrandTotalWeight = strSecondMixGrandTotalWeight + strSecondMixTotalWeight
	next
	%>
<%end if%>
</table>
</td>
</tr>
</table>
<%else
	if strDoughType <> "-1" then
%>
	<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
		<tr>
			<td colspan=''>
				<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
					<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22">
						<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found for second mix.</font></p></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%
	end if
%>
<%end if' This is the end for second mix report%>

<%'end if ' This is the end for Request("Search")%>
<%
if IsArray(recarray) then erase recarray
if IsArray(arrIName) then erase arrIName
if IsArray(recsecondmixarray) then erase recsecondmixarray
if IsArray(arrSecondMixIName) then erase arrSecondMixIName
%>

<!-- END OF TWO STAGE MIXING CATEGORY -->

<%
ElseIf vReportType = "0" Then
%>

<!-- START OF MAIN DOUGH WITH SECOND MIX CATEGORY -->

<%
'if (Request("Search")<>"") then
%>
<%if isarray(recarray) then%>
<table align="center" border="0" bgcolor="#FFFFFF" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<%
	if strDoughType = "-1" then
%>
<tr  bgcolor="#FFFFFF">
  <td width="100%" bgcolor="#FFFFFF" colspan='<%=UBound(arrProductCategory) +3%>' height="40"><b>Dough Name - <label id="lblDoughType"><%=vDoughTypearray(1,d)%></label></b></td>
</tr>
<%
	else
%>
<tr  bgcolor="#FFFFFF">
  <td width="100%" bgcolor="#FFFFFF" colspan='<%=UBound(arrProductCategory) +3%>' height="40"><b>Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
</tr>
<%
	end if
%>
<%
    'Set objGeneral = Server.CreateObject("bakery.general")
    'dim objIndex
    'dim n, m, p
    'dim weight, weight1, arrProductCategoryTotal (50), arrProductCategoryTotal1 (50)
    
    if not isempty(arrIName) then
        objGeneral.BubbleSort(arrIName)
    end if
        
    if not isempty(arrProductCategory) then        
        objGeneral.BubbleSort(arrProductCategory)
    end if
	
	set objGeneral = nothing
%>

<tr>
<td colspan='<%=UBound(arrProductCategory) %>'>
	<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
	<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
    <%if isarray(arrIName) then%>
		<td width="170" bgcolor="#CCCCCC">&nbsp;</td>
	    <%for m=0 to UBound(arrIName)
			arrProductCategoryTotal(m)=0
		%>
	        <td align="right" bgcolor="#CCCCCC"><%=arrIName(m)%></td>	
	    <%next%>
			<td align="right" bgcolor="#CCCCCC" width="120">Total Weight (Kg)</td>	
    <%else%>
		<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
	<%end if%>
	</tr>
	
	<%
		If isarray(arrProductCategory) Then
			strGrandTotalWeight1 = 0
			for n1=0 to UBound(arrProductCategory)
				strTotalWeight1 = 0
				
				for p1=0 to UBound(arrIName)
					weight1 = 0
					for c1=0 to UBound(recarray,2)
						if(recarray(3,c1)=arrIName(p1) and recarray(2,c1) = arrProductCategory(n1)) then
						   weight1 = recarray(4,c1)
						end if
					next
					strTotalWeight1 = cdbl(strTotalWeight1)+ cdbl(weight1) 
					arrProductCategoryTotal1(p1) = cdbl(arrProductCategoryTotal1(p1))+cdbl(weight1)
				next
			strGrandTotalWeight1 = strGrandTotalWeight1 + strTotalWeight1
			next
		End If
	%>
	
    <%if isarray(arrProductCategory) then%>
	
	<tr bgcolor="#CCCCCC" style="font-weight:bold">
		<td>Total (Kg)</td>
		<%for m1=0 to UBound(arrIName)%>
			<td align="right"><%=formatnumber(arrProductCategoryTotal1(m1),3)%></td>
		<%next%>
		<td align="right"><%=formatnumber(strGrandTotalWeight1,3)%></td>
	</tr>
	
	<%
	strGrandTotalWeight=0
	for n=0 to UBound(arrProductCategory)
		if (n mod 2 =0) then
			strBgColour="#FFFFFF"
		else
			strBgColour="#F3F3F3"
		end if
	%>
	    <tr bgcolor="<%=strBgColour%>">
	        <td><%=arrProductCategory(n)%></td>
    	    
	        <%
			strTotalWeight=0
			
	        for p=0 to UBound(arrIName)
	            weight=0
    	        
	            for c=0 to UBound(recarray,2)
	                if(recarray(3,c)=arrIName(p) and recarray(2,c) = arrProductCategory(n)) then
	                   weight = recarray(4,c)
	                end if	            	
				next	           
    	            
	        %>
	            <td align="right"><%=formatnumber(weight,3)%></td>
	        <%			
			strTotalWeight=cdbl(strTotalWeight)+ cdbl(weight) 
			arrProductCategoryTotal(p)=cdbl(arrProductCategoryTotal(p))+cdbl(weight)
	        next
			

	        %>	    
    	   	<td align="right" style="font-weight:bold"><%=formatnumber(strTotalWeight,3)%></td> 
	    </tr>
		
	<%
	
	strGrandTotalWeight=strGrandTotalWeight+strTotalWeight
	next
	%>
<%end if%>
</table>
</td>
</tr>
<tr>
    <td>
        <!--#include file = "Includes/DoughSecondMixNew.asp"-->
    </td>
</tr>
</table>
<%
else
	If strDoughType <> "-1" Then
%>
<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
		<tr>
  			<td width="100%"  colspan='' height="40"><b>Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
		</tr>
		<tr>
			<td colspan=''>
				<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
					<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22">
						<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%
	end if
%>
<%end if ' This is end for isarray(recarray) check%>
<%'end if ' This is end for (Request("Search")<>"") check%>
<%
if IsArray(recarray) then erase recarray
%>

<!-- END OF MAIN DOUGH WITH SECOND MIX CATEGORY -->

<%
End If 'This is the end of report for Report type checking

next 'This is the end of the loop for different dough types

If strDoughType = "-1" Then
	'If  IsDataAvailbale = 0 Then
	If  (IsDataAvailbale = 0) and (IsSecondMixDataAvailbale = 0) Then
%>
<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
		<td colspan=''>
			<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
				<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22">
					<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
	End If
End If
%>
</td>
</tr>
</table>
<br><br>
</body>
</html>