<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayIngType()	
vingarray =  detail("IngredientType") 

if (strMotherIngredient="") then
	strMotherIngredient="0"
end if

set detail = Nothing
set object = Nothing
%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>

<script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{

  if (theForm.ino.selectedIndex < 0)
  {
    alert("Please select one of the \"Ing Type\" options.");
    theForm.ino.focus();
    return (false);
  }

  if (theForm.ino.selectedIndex == 0)
  {
    alert("The first \"Ing Type\" option is not a valid selection.  Please choose one of the other options.");
    theForm.ino.focus();
    return (false);
  }  

  if (theForm.ingname.value == "")
  {
    alert("Please enter a value for the \"Name\" field.");
    theForm.ingname.focus();
    return (false);
  }

  if (theForm.Utype.selectedIndex < 0)
  {
    alert("Please select one of the \"Unit Type\" options.");
    theForm.Utype.focus();
    return (false);
  }

  if (theForm.Utype.selectedIndex == 0)
  {
    alert("The first \"Unit Type\" option is not a valid selection.  Please choose one of the other options.");
    theForm.Utype.focus();
    return (false);
  }

  return (true);
}
//--></script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;

<form method="POST" action="save_inventory_ingredients.asp" name="FrontPage_Form1" onSubmit="return FrontPage_Form1_Validator(this)">

<div align="center">
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Add ingredients<br>
      &nbsp;</font></b>
<%
if Request.QueryString ("status") = "error" then
	Response.Write "<br><font size=""3"" color=""#FF0000""><b>Ingredient already exists go back to change ingredient name</b></font><br><br>"
elseif Request.QueryString ("status") = "ok" then
	Response.Write "<br><font size=""3"" color=""#16A23D""><b>Ingredient " & request.querystring("ingname") & " created successfully</b></font><br><br>"
end if
%>
<%if Request.QueryString ("status") = "" then%>
      <table border="0" width="70%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
         <tr>
          <td bgcolor="#E1E1E1" colspan="2"><b>Ingredient</b></td>
        </tr>


        <tr>
          <td>Ingredient Category</td>
          <td>        
         <select size="1" name="ino">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(1,i)%></option>
                  <%next%> 


          </select></td>
        </tr>
        <tr>
          <td>Ingredient Name</td>
          <td>
          <input type="text" name="ingname" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "50"></td>
        </tr>
		 <tr>
          <td>Mother Ingredient</td>
          <td>
         	<input type="radio" name="MotherIngredient" value="1" class="radioinput"  <%if strMotherIngredient="1" then%> checked="checked" <%end if%>> Yes
		 	<input type="radio" name="MotherIngredient" value="0" class="radioinput"  <%if strMotherIngredient="0" then%> checked="checked" <%end if%>> No
		 </td>
        </tr>
        <tr>
          <td>Unit Measure</td>
          <td><select size="1" name="Utype">
          <option>Select</option>
          <option value="Kgs">Kgs</option>
          <option value="Lts">Lts</option>
          <option value="Pcs">Pcs</option>
          </select></td>
        </tr>
        <tr>
          <td></td>
          <td><input type="submit" value="Add Ingredient" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
      </table>
<%end if%>
  </center>
    </td>
  </tr>
</table>


</div>

</form>

</body>

</html>