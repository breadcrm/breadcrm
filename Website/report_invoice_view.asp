<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Invoice</title>
<style type="text/css">
<!--
br.page { page-break-before: always; height:1px }
-->
</style>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0">
<%
dim objBakery
dim recarray1, recarray2
dim retcol
dim i

dim ordno



ordno = Request.QueryString("txtordno")
ordno = Request.QueryString("txtordno")



if not isnumeric(ordno) then
 ordno = 0
end if

set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.GetInvoiceTotal(ordno)
recarray1 = retcol("InvoiceTotal")
set retcol = objBakery.GetInvoiceDetails(ordno)
recarray2 = retcol("InvoiceDetails")

dim invno, vRecArrayReseller
dim cno,cname, cadd1, cadd2, ctown, ccounty, cpostcode, pono, vehicle, orddt, strLogo, strTele



if isarray(recarray1) then
  invno = recarray1(0,0)
  cno = recarray1(14,0)
  cname = recarray1(2,0)
  cadd1 = recarray1(5,0)
  cadd2 = recarray1(6,0)
  ctown = recarray1(7,0)
  ccounty = ""
  cpostcode = recarray1(8,0)
  pono = recarray1(9,0)
  vehicle = recarray1(10,0)
  orddt = recarray1(1,0)
  intRID=recarray1(20,0)
  strLogo = recarray1(22,0)
  strTele = recarray1(23,0)
  if strTele = "" Then
    strTele = "_"
  End If
	if intRID<>"" and intRID<>"0" then
		set object = Server.CreateObject("bakery.Reseller")
		object.SetEnvironment(strconnection)
		set DisplayResellerDetail= object.DisplayResellerDetail(intRID)
		vRecArrayReseller = DisplayResellerDetail("ResellerDetail")
		set DisplayResellerDetail= nothing
		set object = nothing
	end if	

else
  invno = ""
  cno = ""
  cname = ""
  cadd1 = ""
  cadd2 = ""
  ctown = ""
  ccounty = ""
  cpostcode = ""
  pono = ""
  vehicle = ""
  orddt = ""
end if

dim qty, pcode, pname, up, tp

if isarray(recarray1) then
	If recarray1(3, intI) = "Bread Factory" Then
			'strLogo = "<b><font face=""Monotype Corsiva"" size=""5"">THE BREAD FACTORY</font></b>"
			strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		    strAddress = strAddress & "<b>The Bread Factory Ltd.</b><br> Light Work<br>" & vbCrLf
		    'strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
		    strAddress = strAddress & "Gresham House, 53 Clarendon Road, Watford, Herts WD17 1LA<br>" & vbCrLf
		    strAddress = strAddress & "</p>" & vbCrLf
		    strAddress = strAddress & "<p>" & vbCrLf
		    if strLogo <>  "" then
		        strAddress = strAddress & "Telephone: &nbsp; " & strTele & vbCrLf
		    else
		        strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
				strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
		    End If    
		    
		    strAddress = strAddress & "</p></font>"
	  ElseIf recarray1(3, intI) = "Gail Force" Then
			'strLogo = "<b><font face=""Kunstler Script"" size=""8"">Gail Force</font></b>"
			strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		    strAddress = strAddress & " Light Work<br>" & vbCrLf
		   ' strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
		    strAddress = strAddress & "Gresham House, 53 Clarendon Road, Watford, Herts WD17 1LA<br>" & vbCrLf
		    strAddress = strAddress & "</p>" & vbCrLf
		    strAddress = strAddress & "<p>" & vbCrLf
		    if strLogo <>  "" then
		        strAddress = strAddress & "Telephone: &nbsp; " & strTele & vbCrLf
		    else
		        strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
				strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
		    End If
		    strAddress = strAddress & "</p></font>"
	  End If
end if

if intRID<>"" and intRID<>"0" then
	if isarray(vRecArrayReseller) then
		strResellerName=vRecArrayReseller(1,0)
		strResellerAddress1=vRecArrayReseller(2,0)
		strResellerTown=vRecArrayReseller(3,0)
		strResellerPostcode=vRecArrayReseller(4,0)
		strResellerTelephone=vRecArrayReseller(5,0)
		strResellerFax=vRecArrayReseller(6,0)
		strCompanyLogo="images/ResellerLogos/" & vRecArrayReseller(8,0)
		strResellerInvoiceFooterMessage=vRecArrayReseller(9,0)
		strVATRegNo=vRecArrayReseller(10,0)
		
		strResellerAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		if strResellerName<>"" then
			strResellerAddress = strResellerAddress & "<b>" & strResellerName & "</b><br>" & vbCrLf
		end if
		if strResellerAddress1<>"" then
			strResellerAddress = strResellerAddress & strResellerAddress1 & "<br>"& vbCrLf
		end if
		if strResellerTown<>"" then
			strResellerAddress = strResellerAddress & strResellerTown & "<br>" & vbCrLf
		end if
		if strResellerPostcode<>"" then
			strResellerAddress = strResellerAddress & strResellerPostcode & "<br>" & vbCrLf
		end if
		strResellerAddress = strResellerAddress & "<BR>" & vbCrLf
		
		
		if strLogo =  "" then
		if strResellerTelephone<>"" then
			strResellerAddress = strResellerAddress & "Telephone: " &  strResellerTelephone & "<br>" & vbCrLf
		end if
		if strResellerFax<>"" then
			strResellerAddress = strResellerAddress & "Fax:" &  strResellerFax & "<br>" & vbCrLf
		end if
		
		else
		
		strResellerAddress = strResellerAddress & "Telephone: " &  strTele & "<br>" & vbCrLf
		
		End if
			strResellerAddress = strResellerAddress & "</p></font>"
	end if
end if
%>

<table border="0" width="90%" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td width="100%" valign="top">
	<% 
	strBG=""
	if (recarray1(16, intI))="1" then
		strBG="images/PricelessInvoiceNew.gif"
	end if
	%>
	
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="26%" valign="top">
         <%if intRID<>"" and intRID<>"0" then%>
		<font face="Verdana" size="1">VAT Reg No. <%=strVATRegNo%></font>
		<%=strResellerAddress%>
		<%else%>
		<font face="Verdana" size="1">VAT Reg No. XXXXXX</font>
		<%=strAddress%>
		<%end if%>
          </td>
          <td width="46%" valign="top" align="center">
            <%if strLogo <> "" then%>
            <img src="images/CustomerGroupLogo/<%=strLogo%>"><br />
		    <%elseif intRID<>"" and intRID<>"0" then%>
			<img src="<%=strCompanyLogo%>"><br>
			<%else%>
			<img src="images/BakeryLogo.gif" width="225" height="77"><br>
			<%end if%>
		  <%if strBG<>"" then%><img src="<%=strBG%>" height="60" width="225"><%end if%>
		  </td>
          <td width="28%" valign="top" >
            <p align="center">
            <font face="Verdana" size="1">THIS IS AN INVOICE</font><br>
            <font face="Verdana" size="1">PLEASE KEEP SAFE AND PASS TO YOUR ACCOUNTS DEPARTMENT</font></b><font size="1">
            </font>
          </td>
        </tr>
        <tr>
          <td width="26%" valign="top"><b><font face="Verdana" size="4">&nbsp;</font></b></td>
          <td width="46%" valign="top"></td>
          <td width="28%" valign="top"></td>
        </tr>
      </table><center>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="33%"></td>
          <td width="33%">
            <p align="center"><font size="3" face="Verdana"><b>INVOICE # :</b> <%=invno%></font></td>
          <td width="34%"></td>
        </tr>
        <tr>
          <td width="33%"><font size="3" face="Verdana"><b>&nbsp;</b></font></td>
          <td width="33%"></td>
          <td width="34%"></td>
        </tr>
      </table>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="top"><font face="Verdana" size="2">
		 
		  
		  <b><%=cno%><br><b><%=cname%><br>
            <%=cadd1%><br>
            <%=cadd2%><br>
            <%=ctown%><br>
            <%=cpostcode%></b>
		 	</font></td>
          <td width="33%" valign="top"><font face="Verdana" size="2"><b>Purchase
            order No.: <%=pono%><br>
            <br>
            van #: <%=vehicle%><br>
            <br>
            Date: <%Response.Write(displayBristishDate(orddt))%></b></font></td>
        </tr>
        <tr>
          <td valign="top"><font face="Verdana" size="2"><b>&nbsp;</b></font></td>
          <td width="33%" valign="top"><font face="Verdana" size="2"><b>&nbsp;</b></font></td>
        </tr>
        <tr>
          <td valign="top"><font face="Verdana" size="2"><b>&nbsp;</b></font></td>
          <td width="33%" valign="top"><font face="Verdana" size="2"><b>&nbsp;</b></font></td>
        </tr>
      </table>
      <div align="center">
        <center>
        <table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
          <tr>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Qty</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Product code</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Unit price</b></td>
            <td width="20%" align="center" bgcolor="#CCCCCC" height="20"><b>Total price</b></td>
          </tr><%
          if isarray(recarray2) then
            for i=0 to ubound(recarray2,2)%>
            <tr>
            <td align="center"><%=recarray2(1,i)%>&nbsp;</td>
            <td align="left"><%=recarray2(2,i)%>&nbsp;</td>
            <td align="left"><%=recarray2(3,i)%>&nbsp;</td>
			<%
            if isnull(recarray2(4,i)) then%>
            <td align="center">� 0.00&nbsp;</td><%
            else%>
            <td align="right">� <%=formatnumber(recarray2(4,i),2)%>&nbsp;</td><%
            end if
			
            if isnull(recarray2(5,i)) then%>
            <td width="20%" align="center">� 0.00&nbsp;</td><%
            else%>
            <td width="20%" align="right">� <%=formatnumber(recarray2(5,i),2)%>&nbsp;</td><%
            end if
			
			%>
            </tr><%
            next
          else%>
            <tr>
            <td colspan="5">
            <b>No Records Found...</b>
            </td>
            </tr><%
          end if%>
        </table>
        </center>
      </div>
      <div align="center">
        <table border="0" width="90%" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <p align="right"><b><font size="2" face="Verdana">Goods
              total&nbsp;&nbsp; </font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%"><p align="right"><font size="3" face="Verdana"><b>&nbsp;�
                    <%
					
						if isnull(recarray1(11,0)) then
						  Response.Write ("0.00")
						else
						 response.write(formatnumber(recarray1(11,0),2))
						end if
				
					%>
                  </b></font></p></td>
                </tr>
              </table>
            </td>
            </tr>
          <tr>
            <td align="right" >
              <p align="right"><b><font size="1" face="Verdana">VAT
              total&nbsp;&nbsp; </font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="20%" align="center">
                    <p align="right"><font size="1" face="Verdana"><b>&nbsp;� <%
                    	
							if isnull(recarray1(12,0)) then
								Response.Write("0.00")
							else
								response.write(formatnumber(recarray1(12,0),2))
							end if
	
						
						%>
					</b></font></p>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          <tr>
            <td>
              <p align="right"><b><font size="2" face="Verdana">Invoice
              total&nbsp;&nbsp; </font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%">
                    <p align="right"><font size="3" face="Verdana"><b>&nbsp;�<%
                   
						if isnull(recarray1(13,0)) then
							Response.Write("0.00")
						else
							response.write(formatnumber(cdbl(formatnumber(recarray1(11,0),2))+cdbl(formatnumber(recarray1(12,0),2)),2))
						end if
			
					%>
                    </b></font></p>
				  </td>
                </tr>
              </table>
            </td>
            </tr>
          </table>
      </div>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
           <td width="100%" height="10"></td>
        </tr>
        <tr>
          <td width="100%">
            <font face="Verdana" size="1">
            <%=GetFooterText()%>
           <!-- <b>IMPORTANT!!</b><br>
            &bull; All our products do (or may) contain gluten, sesame seeds and nuts � we are an artisan bakery using lots of different flours, nuts and seeds<br>
            &bull; Our bakery uses other allergens � ask if you need to know more info, we are happy to provide it<br>
			&bull; By placing an order with us, you accept our product specifications � ask us if you need to see them<br>
			&bull; You may find the odd olive stone or walnut shell in our products � we use natural products and can not guarantee they are all removed, so please let your customers know			--></font>		</td>
        </tr>
        <tr><td align="center"><font face="Verdana" size="1" color="#FF0000"><%if (recarray1(21,0)<>"") then%><%=recarray1(21,0)%><%end if%></font>&nbsp;</td></tr>
        <tr>
          <td width="100%" align="center"><font face="Verdana" size="1">
		  <%if intRID<>"" and intRID<>"0" then%>
			<%=replace(strResellerInvoiceFooterMessage,"Registered office:","<br>Registered office:")%>
			<%else%>
			<%=strglobalinvoicefootermessage%>
			<%end if%>
		 </font></td>
        </tr>
                <tr><td>&nbsp;</td></tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<% if (recarray1(16, intI))="1" then%>

<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%
if isarray(recarray1) then
	If recarray1(3, intI) = "Bread Factory" Then
			'strLogo = "<b><font face=""Monotype Corsiva"" size=""5"">THE BREAD FACTORY</font></b>"
			strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		    strAddress = strAddress & "<b>The Bread Factory Ltd.</b><br> Light Work<br>" & vbCrLf
		   ' strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
		    strAddress = strAddress & "Gresham House, 53 Clarendon Road, Watford, Herts WD17 1LA<br>" & vbCrLf
		    strAddress = strAddress & "</p>" & vbCrLf
		    strAddress = strAddress & "<p>" & vbCrLf
		     if strLogo <>  "" then
		        strAddress = strAddress & "Telephone: &nbsp; " & strTele & vbCrLf
		     Else   
		        strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
				strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
		     End if
		    strAddress = strAddress & "</p></font>"
	  ElseIf recarray1(3, intI) = "Gail Force" Then
			'strLogo = "<b><font face=""Kunstler Script"" size=""8"">Gail Force</font></b>"
			strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		    strAddress = strAddress & " Light Work<br>" & vbCrLf
		'    strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
		    strAddress = strAddress & "Gresham House, 53 Clarendon Road, Watford, Herts WD17 1LA<br>" & vbCrLf
		    strAddress = strAddress & "</p>" & vbCrLf
		    strAddress = strAddress & "<p>" & vbCrLf
		    if strLogo <>  "" then
		        strAddress = strAddress & "Telephone: &nbsp; " & strTele & vbCrLf
		    Else
		        strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
				strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
		   End If
		    strAddress = strAddress & "</p></font>"
	  End If
end if
%>
<div align="center">
<center>
<table border="0" width="90%" height="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%" valign="top">
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="26%" valign="top">
         <%if intRID<>"" and intRID<>"0" then%>
		<font face="Verdana" size="1">VAT Reg No. <%=strVATRegNo%></font>
		<%=strResellerAddress%>
		<%else%>
		<font face="Verdana" size="1">VAT Reg No. XXXXXX</font>
		<%=strAddress%>
		<%end if%>
          </td>
          <td width="46%" valign="top" align="center">
          <%
		  if intRID<>"" and intRID<>"0" then
		  %>
		  <img src="<%=strCompanyLogo%>">
		  <%
		  elseif strLogo <> "" then%>
             <img src="images/CustomerGroupLogo/<%=strLogo%>">
          <%else %>    
          <img src="images/BakeryLogo.gif" width="225" height="77">
          <%end if %>
          </td>
          <td width="28%" valign="top">
            <p align="center">
            <font face="Verdana" size="1">THIS IS AN INVOICE</font><br>
            <font face="Verdana" size="1">PLEASE KEEP SAFE AND PASS TO YOUR ACCOUNTS DEPARTMENT</font></b><font size="1">
            </font>
          </td>
        </tr>
        <tr>
          <td width="26%" valign="top"><b><font face="Verdana" size="4">&nbsp;</font></b></td>
          <td width="46%" valign="top"></td>
          <td width="28%" valign="top"></td>
        </tr>
      </table><center>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="33%"></td>
          <td width="33%">
            <p align="center"><font size="2" face="Verdana"><b>Delivery Note for</b><br>INVOICE
            # : <%=invno%></font></td>
          <td width="34%"></td>
        </tr>
        <tr>
          <td width="33%"><font size="3" face="Verdana"><b>&nbsp;</b></font></td>
          <td width="33%"></td>
          <td width="34%"></td>
        </tr>
      </table>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="top"><font face="Verdana" size="2"><b><%=cno%><br><b><%=cname%><br>
            <%=cadd1%><br>
            <%=cadd2%><br>
            <%=ctown%><br>
            <%=cpostcode%></b></font></td>
          <td width="33%" valign="top"><font face="Verdana" size="2"><b>Purchase
            order No.: <%=pono%><br>
            <br>
            van # : <%=vehicle%><br>
            <br>
            Date: <%Response.Write(displayBristishDate(orddt))%></b></font></td>
        </tr>
        <tr>
          <td valign="top"><font face="Verdana" size="2"><b>&nbsp;</b></font></td>
          <td width="33%" valign="top"><font face="Verdana" size="2"><b>&nbsp;</b></font></td>

        </tr>
        <tr>
          <td valign="top"><font face="Verdana" size="2"><b>&nbsp;</b></font></td>
          <td width="33%" valign="top"><font face="Verdana" size="2"><b>&nbsp;</b></font></td>
        </tr>
      </table>
      <div align="center">
        <center>
        <table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
          <tr>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Qty</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Product code</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20"><b>Product name</b></td>
            </tr><%
          if isarray(recarray2) then
            for i=0 to ubound(recarray2,2)%>
            <tr>
            <td align="center"><%=recarray2(1,i)%>&nbsp;</td>
            <td align="left"><%=recarray2(2,i)%>&nbsp;</td>
            <td align="left"><%=recarray2(3,i)%>&nbsp;</td>
			  </tr><%
            next
          else%>
            <tr>
            <td colspan="5">
            <b>No Records Found...</b>
            </td>
            </tr><%
          end if%>
        </table>
        </center>
      </div>
     
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%" height="10"></td>
        </tr>
        <tr>
          <td width="100%">
            <font face="Verdana" size="1">
             <%=GetFooterText()%>
<!--            <b>IMPORTANT!!</b><br>
            &bull; All our products do (or may) contain gluten, sesame seeds and nuts � we are an artisan bakery using lots of different flours, nuts and seeds<br>
            &bull; Our bakery uses other allergens � ask if you need to know more info, we are happy to provide it<br>
			&bull; By placing an order with us, you accept our product specifications � ask us if you need to see them<br>
			&bull; You may find the odd olive stone or walnut shell in our products � we use natural products and can not guarantee they are all removed, so please let your customers know	
-->			
			</font>
			</td>
        </tr>
       <tr><td align="center"><font face="Verdana" size="1" color="#FF0000"><%if (recarray1(21,0)<>"") then%><%=recarray1(21,0)%><%end if%></font>&nbsp;</td></tr>
         <tr>
          <td width="100%" align="center"><font face="Verdana" size="1">
		  <%if intRID<>"" and intRID<>"0" then%>
			<%=replace(strResellerInvoiceFooterMessage,"Registered office:","<br>Registered office:")%>
			<%else%>
			<%=strglobalinvoicefootermessage%>
			<%end if%>
		 </font></td>
        </tr>
                <tr><td>&nbsp;</td></tr>
      </table>
    </td>
  </tr>
</table>
<% end if%>
</center>
</div>
</body>
</html>

<%

Function GetFooterText()

Dim arFooter,objFooter,obj,hbody
Set obj = CreateObject("Bakery.General")
obj.SetEnvironment(strconnection)
set objFooter = obj.GetFooterText()
arFooter = objFooter("FooterMessage")

If IsArray(arFooter) Then

    hbody = Replace(arFooter(0,0), "^^^", "'") 

    GetFooterText = hbody
Else

   GetFooterText = Empty

End if

End Function

 %>