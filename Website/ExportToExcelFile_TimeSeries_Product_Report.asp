<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
dim objBakery
dim recarray
dim retcol
dim fromdt, todt,i
Dim GrandTotal, strTimeSeriesProductList, recarrayHeaderName

fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")
strUnitOption=Request.Form("UnitOption")

strTimeSeriesProductList=trim(Request.Form("TimeSeriesProductList"))
'strTimeSeriesProductList=left(strTimeSeriesProductList,len(strTimeSeriesProductList)-1)
'strTimeSeriesProductList = replace(strTimeSeriesProductList,",","','")
if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
   todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if
if isdate(fromdt) and isdate(todt) then
	set objBakery = server.CreateObject("Bakery.reports")
	objBakery.SetEnvironment(strconnection)
	set retcol = objBakery.Display_TimeSeries_Products(strUnitOption,fromdt,todt,strTimeSeriesProductList)
	recarray = retcol("Product")
	strvalue=retcol("HeaderName")	
	recarrayHeaderName=split(strvalue,",")
end if
if isarray(recarray) then
	dim  arrayGrandTotal(25)
	for j=1 to UBound(recarray,1)
		arrayGrandTotal(j-1)=0	
	next
	ColNo=UBound(recarray,1)+1
end if

	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=TimeSeries_Product_Report.xls" 

%>

<table border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt" align="center">
 <tr>
 <td><b><font size="2"><%=strUnitOption%> Time Series Product Report &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt;<br><br></font></b>
		<table border="1" width="<%=ColNo*120%>" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<%if isarray(recarray) then%>
	<tr style="font-weight:bold">
	<%for j=0 to UBound(recarrayHeaderName)%>
		
		<td bgcolor="#CCCCCC" width="120"><b><%=recarrayHeaderName(j)%></b></td>
		
	<%next%>
	</tr>
	<%for i=0 to UBound(recarray,2)%>
	<tr>
		<td align="left"><%=recarray(0,i)%></td>
	<%for j=1 to UBound(recarray,1)%>
		<td align="center"><%=recarray(j,i)%></td>
		<%
		arrayGrandTotal(j)=arrayGrandTotal(j)+recarray(j,i)
	next
	%>
	</tr>
	<%next%>
	<tr>
	  <td bgcolor="#CCCCCC" align="right"><b>&nbsp;Grand Total&nbsp;</b></td>
	  <%for j=1 to UBound(recarray,1)%>
	  <td align="center"  bgcolor="#CCCCCC"><b><%=arrayGrandTotal(j)%></b></td>
	  <%next%>
	</tr>
	<%else%>
	  <tr><td colspan="7" width="500" bgcolor="#CCCCCC" align="center"><b>0 - No records matched...</b></td></tr>
	<%end if%>
	</table>
</td>
</tr>
</table>
<p align="center"><input type="button" value=" Back " name="back" onClick="history.back()"></p>
</body>
<%if IsArray(recarray) then erase recarray %>
</html>