<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
vSno= request.form("Sno")
if vSno<> "" then
	set object = Server.CreateObject("bakery.Supplier")
	object.SetEnvironment(strconnection)
	set DisplaySupplierDetail= object.DisplaySupplierDetail(vSno)
	vRecArray = DisplaySupplierDetail("SupplierDetail")
	set DisplaySupplierDetail = nothing
	set object = nothing
end if
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script LANGUAGE="javascript" SRC="includes/Script.js"></script>

<script Language="JavaScript">
<!--
function validate(frm,type){
	if(confirm("Would you like to "+ type +" this supplier?")){

		if (type=='delete'){
			frm.type.value='Delete';
			frm.submit();		
		}	
		else{
			FrontPage_Form1_Validator(frm)
		}
		
	}
}
//-->
</Script>

<script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{

  if (theForm.name.value == "")
  {
    alert("Please enter a value for the \"name\" field.");
    theForm.name.focus();
    return;
  }

  if (theForm.name.value.length > 100)
  {
    alert("Please enter at most 100 characters in the \"name\" field.");
    theForm.name.focus();
    return;
  }

  if (theForm.address1.value == "")
  {
    alert("Please enter a value for the \"address1\" field.");
    theForm.address1.focus();
    return;
  }

  if (theForm.address1.value.length > 100)
  {
    alert("Please enter at most 100 characters in the \"address1\" field.");
    theForm.address1.focus();
    return;
  }

  if (theForm.town.value == "")
  {
    alert("Please enter a value for the \"town\" field.");
    theForm.town.focus();
    return;
  }

  if (theForm.town.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"town\" field.");
    theForm.town.focus();
    return;
  }

  if (theForm.postcode.value == "")
  {
    alert("Please enter a value for the \"postcode\" field.");
    theForm.postcode.focus();
    return;
  }

  if (theForm.postcode.value.length > 20)
  {
    alert("Please enter at most 20 characters in the \"postcode\" field.");
    theForm.postcode.focus();
    return;
  }

  if (theForm.telephone.value == "")
  {
    alert("Please enter a value for the \"telephone\" field.");
    theForm.telephone.focus();
    return;
  }

  if (theForm.telephone.value.length > 20)
  {
    alert("Please enter at most 20 characters in the \"telephone\" field.");
    theForm.telephone.focus();
    return;
  }
/*
  if (theForm.fax.value == "")
  {
    alert("Please enter a value for the \"fax\" field.");
    theForm.fax.focus();
    return;
  }

  if (theForm.fax.value.length > 20)
  {
    alert("Please enter at most 20 characters in the \"fax\" field.");
    theForm.fax.focus();
    return;
  }

  if (theForm.email.value == "")
  {
    alert("Please enter a value for the \"email\" field.");
    theForm.email.focus();
    return;
  }

  if(CheckEmail(theForm.email.value)=="true")
  { 
		alert("Please enter a valid e-mail address in the \"email\" field.");	
		theForm.email.focus();
		return;
  }	

  if (theForm.email.value.length > 100)
  {
    alert("Please enter at most 100 characters in the \"email\" field.");
    theForm.email.focus();
    return;
  }

  if (theForm.contactname.value == "")
  {
    alert("Please enter a value for the \"contactname\" field.");
    theForm.contactname.focus();
    return;
  }

  if (theForm.contactname.value.length > 100)
  {
    alert("Please enter at most 100 characters in the \"contactname\" field.");
    theForm.contactname.focus();
    return;
  }

  if (theForm.contactjobtitle.value == "")
  {
    alert("Please enter a value for the \"contactjobtitle\" field.");
    theForm.contactjobtitle.focus();
    return;
  }

  if (theForm.contactjobtitle.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"contactjobtitle\" field.");
    theForm.contactjobtitle.focus();
    return;
  }*/
  //return (true);
  theForm.submit();
}
//--></script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>

<form method="POST" action="supplier_finish.asp" onSubmit="return FrontPage_Form1_Validator(this)" name="FrontPage_Form1">

    <input type = "hidden" name = "sno" value="<%=vSno%>">

    <td width="100%"><b><font size="3"><%if vSno = "" then%>Create new<%else%>Edit<%end if%> supplier<br>
      &nbsp;</font></b>
      
      <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td width="50%" valign="top">
          <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="664">
        <tr>
          <td width="656" colspan="2"><font color="#FF0000">* Mandatory fields</font></td>
        </tr>
        <tr>
          <td width="90"><b>Name </b> <font color="red">*</font> </td>
          <td width="566">
          <input type="text" name="name" size="42" style="font-family: Verdana; font-size: 8pt" maxlength= "100" value="<%if isarray(vrecarray) then response.write vrecarray(1,0)%>"></td>
        </tr>
        <tr>
          <td width="90">Address <font color="red">*</font> </td>
          <td width="566">
          <input type="text" name="address1" size="42" style="font-family: Verdana; font-size: 8pt" maxlength= "100" value="<%if isarray(vrecarray) then response.write vrecarray(2,0)%>"></td>
        </tr>
        <tr>
          <td width="90"></td>
          <td width="566">
          <input type="text" name="address2" size="42" style="font-family: Verdana; font-size: 8pt" maxlength= "100" value="<%if isarray(vrecarray) then response.write vrecarray(3,0)%>"></td>
        </tr>
        <tr>
          <td width="90">Town <font color="red">*</font> </td>
          <td width="566">
          <input type="text" name="town" size="42" style="font-family: Verdana; font-size: 8pt" maxlength= "50" value="<%if isarray(vrecarray) then response.write vrecarray(4,0)%>"></td>
        </tr>
        <tr>
          <td width="90">Postcode <font color="red">*</font> </td>
          <td width="566">
          <input type="text" name="postcode" size="42" style="font-family: Verdana; font-size: 8pt" maxlength= "20" value="<%if isarray(vrecarray) then response.write vrecarray(5,0)%>"></td>
        </tr>
        <tr>
          <td width="90">Telephone <font color="red">*</font> </td>
          <td width="566">
          <input type="text" name="telephone" size="42" style="font-family: Verdana; font-size: 8pt" maxlength= "20" value="<%if isarray(vrecarray) then response.write vrecarray(6,0)%>"></td>
        </tr>
        <tr>
          <td width="90">Fax</td>
          <td width="566">
          <input type="text" name="fax" size="42" style="font-family: Verdana; font-size: 8pt" maxlength= "20" value="<%if isarray(vrecarray) then response.write vrecarray(7,0)%>"></td>
        </tr>
        <tr>
          <td width="90">Email</td>
          <td width="566">
          <input type="text" name="email" size="42" style="font-family: Verdana; font-size: 8pt" maxlength= "100" value="<%if isarray(vrecarray) then response.write vrecarray(8,0)%>"></td>
        </tr>
        <tr>
          <td width="90">Website</td>
          <td width="566">
          <input type="text" name="website" size="42" style="font-family: Verdana; font-size: 8pt" maxlength= "255" value="<%if isarray(vrecarray) then response.write vrecarray(9,0)%>"></td>
        </tr>
        <tr>
          <td width="90">Payment term</td>
          <td width="566">
          <select size="1" name="paymentterms" style="font-family: Verdana; font-size: 8pt">

			 <%if isarray(vrecarray) then%>
							<option value=" ">Select</option>
              <option value="30 Days"<%if vrecarray(10,0)= "30 Days" then response.write " Selected"%>>30 Days</option>
              <option value="45 Days"<%if vrecarray(10,0)= "45 Days" then response.write " Selected"%>>45 Days</option>
              <option value="60 Days"<%if vrecarray(10,0)= "60 Days" then response.write " Selected"%>>60 Days</option>
			  <%else%>
							<option value=" ">Select</option>
              <option value="30 Days">30 Days</option>
              <option value="45 Days">45 Days</option>
              <option value="60 Days">60 Days</option>
   			<%end if%>
                   
            </select></td>
        </tr>
        <tr>
          <td width="90">&nbsp;</td>
          <td width="566"></td>
        </tr>
        <tr>
          <td colspan="2" width="660"><b> Contacts</b></td>
        </tr>
        <tr>
          <td width="90">Name</td>
          <td width="566">
          <input type="text" name="contactname" size="42" style="font-family: Verdana; font-size: 8pt" maxlength= "100" value="<%if isarray(vrecarray) then response.write vrecarray(11,0)%>"></td>
        </tr>
        <tr>
          <td width="90">Job title</td>
          <td width="566">
          <input type="text" name="contactjobtitle" size="42" style="font-family: Verdana; font-size: 8pt" maxlength= "50" value="<%if isarray(vrecarray) then response.write vrecarray(12,0)%>"></td>
        </tr>
		<!--
		<%if vSno <> "" then%>
        <tr>
          <td width="90">Status</td>
          <td width="566">        
          <select siz0e="1" name="Status" style="font-family: Verdana; font-size: 8pt">
    	    <%if isarray(vrecarray) then%>
	          <option value="A"<%if vrecarray(13,0)= "A" then response.write Selected%>>Active</option>
              <option value="D"<%if vrecarray(13,0)= "A" then response.write Selected%>>Deleted</option>
			<%else%>
              <option value="A">Active</option>
              <option value="D">Deleted</option>
	        <%end if%>
            </select></td>
        </tr>  
        <%end if%>
        -->
        <tr>
          <td width="90"></td>
          <td width="566">
           	<input type = "hidden" name="type" value="">
	        <%if vSno = "" then%>
  			<input type="button" value="Add supplier" name="Search"  onclick="FrontPage_Form1_Validator(this.form);" style="font-family: Verdana; font-size: 8pt"> 			
  			<%else%>
  			<input type="button" value="Edit supplier" onClick="validate(this.form,'edit');" name="Search" style="font-family: Verdana; font-size: 8pt"> 			 			
  			<input type="button" value="Delete supplier" onClick="validate(this.form,'delete');" name="Delete" style="font-family: Verdana; font-size: 8pt"> 			 			 			
			<%end if%>
          </td>
        </tr>      

      </table></td>
        </tr>
     </table>   
      
    </td>
  </tr> </form>
</table>


  </center>
</div>


<p>&nbsp;</p>


</body>

</html>