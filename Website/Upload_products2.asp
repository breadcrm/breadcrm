<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/filepath.asp" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->

<%
Dim folderPath ,objFile, Upload, sAction, Count, File, custID, fso, folderPath2
Dim vfilename, arrXSLHeader
Dim xlApp, xlBook, xlSheet1, strMsg, j
Dim isUpdated

folderPath  = strUploadProductXSLOriginal
folderPath2  = strUploadProductXSL



custID = session("CustIDXSL")

Set objFile = Server.CreateObject("Scripting.FileSystemObject")
If objFile.FolderExists(folderPath) Then	
	
	Set Upload = Server.CreateObject("Persits.Upload.1")

    Upload.Overwritefiles = True
    Count = Upload.Save(folderPath)

    if Count = 1 Then
        For Each File in Upload.Files
	        vfilename = File.ExtractFileName
        next
	End If
End If


Set Upload = Nothing
Set objFile = Nothing


set fso = CreateObject("Scripting.FileSystemObject") 
fso.CopyFile folderPath & vfilename, folderPath2 & "productupload.xls"
set fso = Nothing


If CheckProductValidity (custID) Then
    isUpdated = "OK"
  	LogAction "Stading order Excelfile Uploaded", "File Name :" & vfilename , custID		
Else
    isUpdated = "NO"
End If


Function CheckProductValidity (pNo)
Dim obj, result, arList, isValid

Set obj = CreateObject("Bakery.Customer")
obj.SetEnvironment(strconnection)
Set result = obj.ChecktheProductValidity(pNo)
arList = result("ValidProduct")

 if isArray(arList) Then
    isValid = true
 Else
 
    isValid = false
 End If



CheckProductValidity = isValid

End Function
%>

<html>
<head>
<!--#INCLUDE FILE="includes/head.inc" -->
<script language="javascript" type="text/jscript">
    function onloadMsg() {
        if ("<%=isUpdated%>" == "OK") {
            alert("Product has been inserted successfully");
            window.opener.location.href = 'customer_view.asp?CustNo=<%=custID%>';
            window.close();
        }
    }
	
</script>
</head>
<body onLoad="onloadMsg()">
<form >
<div >
<label id="lblMessage" style="font-family: Verdana; font-size: 10pt; color:Red;"><%=strMsg%></label>
<br /><br />
  <div style="text-align:center;">  <input id="Button1" type="button" value="Close" onClick="window.close();" style="font-family: Verdana; font-size: 8pt;" />  
</div></div>
</form>
</body>
</html>
