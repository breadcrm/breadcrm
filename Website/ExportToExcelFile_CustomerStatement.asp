<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
dim cname,cno,vno,objBakery1,retcol,recarray
Server.ScriptTimeout=6000
fromdt=request("fromdt")
todt=request("todt")

	
cno = replace(Request("cno"),"'","''")
cname = replace(Request("cname"),"'","''")
ordno = replace(Request("ordno"),"'","''")

crno = replace(Request("crno"),"'","''")
pono = replace(Request("pono"),"'","''")

if not isnumeric(cno) then
	cno = 0
end if
if not isnumeric(ordno) then
	ordno = 0
end if

if not isnumeric(crno) then
	crno = 0
end if
	
	if isdate(fromdt) and isdate(todt) then
		set objBakery = server.CreateObject("Bakery.credit")
		objBakery.SetEnvironment(strconnection)
		set retcol = objBakery.ListCustomerStatement(cno,cname,ordno,crno,pono,fromdt,todt,1,0)
		recarray = retcol("Invoices")
		set objBakery = nothing
		Response.ContentType = "application/vnd.ms-excel"
		Response.AddHeader "Content-Disposition", "attachment; filename=customer_statement.xls" 
	%>
	
	<table border="1" width="100%" cellspacing="1" cellpadding="2" align="center">
        <tr>
          <td align="left"><b>Customer Code</b></td>
		  <td align="left"><b>Customer Name</b></td>
	   	  <td align="left"><b>Invoice Date</b></td>
		  <td align="left"><b>Invoice No</b></td>
          <td align="left"><b>Purchase Order No</b></td>         
          <td align="right"><b>Invoice Amount</b></td>		  
		  <td align="left"><b>Credit Date</b></td>
		  <td align="left"><b>Credit No</b></td>
		  <td align="right"><b>Credit Amount</b></td>
		  <td align="right"><b>Charge</b></td>       
        </tr>
		<%
        if isarray(recarray) then
          for i=0 to ubound(recarray,2)
		  	if (recarray(8,i)<>"") then
				dbcharge=recarray(7,i)-recarray(8,i)
			else
				dbcharge=recarray(7,i)	
			end if
			
		%>
          <tr>
			  <td><%=recarray(0,i)%></td>
			  <td><%=recarray(1,i)%></td>
			  <td align="left"><%=displayBristishDate(recarray(2,i))%></td>
			  <td align="left"><%=recarray(3,i)%></td>
			  <td align="left"><%=recarray(4,i)%></td>
			  <td align="right">&pound;<%=FormatNumber(recarray(7,i),2)%></td>
			  <td align="left"><%=displayBristishDate(recarray(5,i))%></td>
			  <td align="left"><%=recarray(6,i)%></td>
			  <td align="left"><% if (recarray(8,i)<>"") then %>&pound; <% end if%><%=FormatNumber(recarray(8,i),2)%></td>
			 <td align="right">&pound;<%=FormatNumber(dbcharge,2)%></td>
          </tr>
		  <%
            next
          else
		  %>
          <tr>
            <td colspan="10" bgcolor="#FFFFFF" class="errortext" align="center" height="40" ><b>There are no records found...</b></td>
          </tr>
		  <%
          end if
		  %>
      </table>
	   <%
          end if
	  %>
