<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%

vNoIngredient = 15
vDno = Request("Dno")
Set obj = Server.CreateObject("Bakery.Inventory")
obj.SetEnvironment(strconnection)
set ObjInventory = obj.DoughTypeView(vDno)
arDough =  ObjInventory("Dough")
arDoughInt =  ObjInventory("Ingredients")

IsTwoStageMixing = 0
if (arDough(7,0) = 3) then
	IsTwoStageMixing = 1
end if

vcolspan = 1
if IsTwoStageMixing = 1 then
	vcolspan = 2
end if

dim arTwoStageMixingDoughInt
if IsTwoStageMixing = 1 then
	set ObjInventory = obj.GetTwoStageDoughIngredient(vDno)
	arTwoStageMixingDoughInt = ObjInventory("Ingredients")
end if

Set obj = Nothing
Set ObjInventory = Nothing
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<table border="0" align="center" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3"><br>View Dough<br><br><br></font></b>
    	
	
        <table align="center" bgcolor="#CCCCCC" border="0" width="550" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">
        <tr bgcolor="#FFFFFF" height="25">
          	<td width="220"><b>Dough Name</b></td>
          	<td colspan="<%=vcolspan%>"><%=arDough(1,0)%></td>
		</tr>
        <tr bgcolor="#FFFFFF" height="25">         
         	<td><b>Dough Type&nbsp;</b></td>
         	<td colspan="<%=vcolspan%>"><%=arDough(3,0)%></td>
        </tr>
		<!--
		<tr bgcolor="#FFFFFF" height="25">
			<td><b>Second Mix Category&nbsp;</b></td>
			<td><% if (arDough(4,0)) then%>Yes<%else%>No<%end if%></td>
		</tr>
		-->
		<tr bgcolor="#FFFFFF" height="25">
			<td><b>Dough Mixing Method&nbsp;</b></td>
			<td colspan="<%=vcolspan%>"><% if (arDough(7,0) = 1) then%>Main Dough<%elseif (arDough(7,0) = 2) then%>Second Mix Category<%elseif (arDough(7,0) = 3) then%>Two Stage Mixing<%end if%></td>
		</tr>
		<% if (arDough(7,0) = 2) then%>
		<tr bgcolor="#FFFFFF" height="25">
			<td><b>Main Dough&nbsp;</b></td>
			<td colspan="<%=vcolspan%>"><%=arDough(5,0)%></td>
		</tr>
		<tr bgcolor="#FFFFFF" height="25">
			<td><strong>Amount of main dough to create<br>1 kilo of second mix&nbsp;</strong></td>
			<td colspan="<%=vcolspan%>"><%=arDough(6,0)%></td>
		</tr>
		<%end if%>
	    <tr bgcolor="#CCCCCC" height="25">
          <td><b>Ingredient</b></td>
		  <%if IsTwoStageMixing = 1 then%>
		  <td ><b>Quantities required to produce 1 kg of first mix</b></td>
		  <td ><b>Quantities required to produce 1 kg of second mix</b></td>
		  <%else%>
		  <td ><b>Quantities required to produce 1 kg of dough</b></td>
		  <%end if%>
        </tr>
		<%
		if IsTwoStageMixing = 1 then
			dbFirstMixTotal=0
			dbSecondMixTotal=0
			if IsArray(arTwoStageMixingDoughInt) Then	
						For xM = 0 to ubound(arTwoStageMixingDoughInt,2)
							if (arTwoStageMixingDoughInt(2,xM)="Y") then
							%>
							<tr bgcolor="#FFFFFF" height="25">
								<td><%=arTwoStageMixingDoughInt(1,xM)%></td>
								<td><%=arTwoStageMixingDoughInt(3,xM)%></td>
								<td><%=arTwoStageMixingDoughInt(4,xM)%></td>
							</tr>
							<%
							end if
							'dbFirstMixTotal=dbFirstMixTotal+formatnumber(arTwoStageMixingDoughInt(3,xM),3)
							'dbSecondMixTotal=dbSecondMixTotal+formatnumber(arTwoStageMixingDoughInt(4,xM),3)
							dbFirstMixTotal=dbFirstMixTotal+formatnumber(arTwoStageMixingDoughInt(3,xM),3)+formatnumber(arTwoStageMixingDoughInt(4,xM),3)
						Next		
			End if
		%>
		<tr bgcolor="#CCCCCC" height="25">
			<td><strong>Total&nbsp;</strong></td>
			<td><%=dbFirstMixTotal%>&nbsp;kg</td>
			<!--<td><%=dbSecondMixTotal%>&nbsp;kg</td>-->
			<td></td>
		</tr>
		<%
		else
			dbTotal=0
			if IsArray(arDoughInt) Then	
						For xM = 0 to ubound(arDoughInt,2)
							if (arDoughInt(2,xM)="Y") then
							%>
							<tr bgcolor="#FFFFFF" height="25">
								<td><%=arDoughInt(1,xM)%></td>
								<td><%=arDoughInt(3,xM)%></td>
							</tr>
							<%
							end if
							dbTotal=dbTotal+formatnumber(arDoughInt(3,xM),3)					
						Next		
			End if
			
			if (arDough(4,0)) then
				dbTotal=dbTotal+arDough(6,0)
			end if
		%> 
		<tr bgcolor="#CCCCCC" height="25">
			<td><strong>Total&nbsp;</strong></td>
			<td><%=dbTotal%>&nbsp;kg</td>
		</tr>
		<%
		end if
		%>
       </table> 
    	<p align="center"><input type="button" value=" Back " onClick="history.back()" name="B1" style="font-family: Verdana; font-size: 8pt"></p>
	</td>
  </tr>
</table>
</body>
</html>
<%
if IsArray(arDough) Then erase arDough 
if IsArray(arDoughInt) Then erase arDoughInt 
if IsArray(arTwoStageMixingDoughInt) Then erase arTwoStageMixingDoughInt 
%>