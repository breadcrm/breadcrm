<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
	dim fromdtr,todtr
	dim objLog, strViewTypehead, strviewType, strViewType2, strdetails
	
	fromdt=request("fromdt")
	todt=request("todt")
	strsearchby = Request("searchby")
	strsearchtext = Request("searchtext")
	strviewType= Request("viewType")
	
	if (strViewType = "0") Then
		strViewType2 = ""
		strViewTypehead = "View All Amendments"
	elseif (strViewType = "1") Then
		strViewType2 = "A" 
		strViewTypehead = "View BMS Amendments"
	elseif (strViewType = "2") Then
		strViewType2 = "O" 
		strViewTypehead = "View Online Amendments"  
	End If  

	if (strsearchby="CustomerName") then
		strCustomerName=strsearchtext
	end if
	
	if (strsearchby="User") then
		strUser=strsearchtext
	end if
	
	if (strsearchby="Action") then
		strAction=strsearchtext
	end if
	
	dim arr
	arr = Split(fromdt,"/")
	fromdtr = arr(2) & "-" & arr(1) & "-" & arr(0)
	
	arr = Split(todt,"/")
	todtr = arr(2) & "-" & arr(1) & "-" &  arr(0)	
	
	set objLog = server.CreateObject("Bakery.Log")
	objLog.SetEnvironment(strconnection)
	'set retcol = objLog.GetLogRecords(strCustomerName, "", strUser, strAction, cdate(fromdtr),cdate(todtr),0,99999,strViewType2)
	set retcol = objLog.GetLogRecords(strCustomerName, "", strUser, strAction, cdate(fromdtr),cdate(todtr),0,99999,strViewType2)
	recarray = retcol("LogRecords")
	
	

	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=Log_trace_rep.xls" 
	%>
      <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%"  colspan="6"><b>Log Activity Trace From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; <% if strStatus<>"" then%> - <%=strStatusLabel%><%end if%> (<%=strViewTypehead%>)</b></td>
        </tr>
        <tr>
          <td width="10%" bgcolor="#CCCCCC"><b>Date&nbsp;</b></td>
          <td width="10%" bgcolor="#CCCCCC"><b>Time&nbsp;</b></td>
          <td width="10%" bgcolor="#CCCCCC"><b>User&nbsp;</b></td>        
          <td width="10%" bgcolor="#CCCCCC"><b>Action&nbsp;</b></td>
          <td width="25%" bgcolor="#CCCCCC"><b>Details&nbsp;</b></td>
          <td width="25%" bgcolor="#CCCCCC"><b>Customer Name&nbsp;</b></td>   
		   <td width="10%" bgcolor="#CCCCCC"><b>From&nbsp;</b></td>          
        </tr>
		
		<%if isarray(recarray) then%>
				<%for i=0 to UBound(recarray,2)%>
					<tr>
						<td><%=recarray(2,i)%></td>
						<td><%=recarray(3,i)%></td>
						<td><%=recarray(7,i)%>&nbsp;</td> 
						<td><%=recarray(1,i)%></td>
						<td>
						<%						
						strdetails=trim(replace(replace(replace(replace(recarray(5,i),"<br>",", "),": , ",": "),":,",":"),"<br/>",", "))
						if (Right(strdetails,1)=",") then
							response.write(trim(left(strdetails,len(strdetails)-1)))
						else
							response.write(strdetails)
						end if						
						%>
						</td>
						<td><%=recarray(8,i)%>&nbsp;</td>
						<td><%If recarray(9,i) = "O" Then response.Write("Online") Else response.Write("BMS") End If  %>  &nbsp;</td>
					</tr>
				<%next%>
          <%else%>
			<tr>
				<td colspan="6" width="100%" bgcolor="#CCCCCC"><b>0 - No Logs Found...</b></td>
			</tr>
          <%end if%>
      </table>
</body>
</html>
