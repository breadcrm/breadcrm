<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
vid=request("id")
set object = Server.CreateObject("bakery.credit")
object.SetEnvironment(strconnection)
if not (isnumeric(vid)) then
	vid=0
end if	

set DisplayComplaintDetail= object.DisplayComplaintDetail(vid)
vRecArray1 = DisplayComplaintDetail("ComplaintDetail")
vRecArray2 = DisplayComplaintDetail("ReasonForCredit")
vRecArray3 = DisplayComplaintDetail("ComplaintResponsibility")
set DisplayComplaintDetail = nothing
set object = nothing
%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script LANGUAGE="javascript" SRC="includes/Script.js"></script>
<script Language="JavaScript">
<!--
function goto(url)
{
	document.location = url;
}
//-->
</script>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 12px" onLoad="ShowInvoiceNoORInvoiceDate()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="950" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 12px">
  <tr>
<%
	if IsArray(vRecArray1) Then	
		vid = vRecArray1(0,0)
		strcusno = vRecArray1(1,0)
		strisinvoiceno = vRecArray1(2,0)
		strinvoiceno = vRecArray1(3,0)
		strinvoicedate = vRecArray1(4,0)
		strnameofpersonmakingcomplaint = vRecArray1(5,0)
		strreasonforcredit = vRecArray1(11,0)
		strresponsibilityforcomplaint = vRecArray1(12,0)
		strcomplaint = vRecArray1(8,0)
		strcomplaintdate = vRecArray1(9,0)
		strpname= vRecArray1(13,0)
	end if
%>
<form method="POST">

    <input type="hidden" name="id" value="<%=vid%>">

    <td width="100%" align="center"><b><font size="3">View Complaint<br>
      &nbsp;</font></b>
      
      <table border="0" width="100%" cellspacing="0" cellpadding="2" align="center">
        <tr>
          <td>
          <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 12px" width="800" align="center">
      
	    <tr height="30">
          <td align="right" width="300"><nobr>Complaint Date:</nobr></td>
          <td><%=strcomplaintdate%></td>
        </tr>
		 <tr height="30">
          <td align="right"><nobr>Customer Code:</nobr></td>
          <td><%=strcusno%></td>
        </tr>
		<tr height="30">
          <td align="right"><nobr>Do you know Invoice No:</nobr></td>
          <td>
		  <%
		  	if (strisinvoiceno="Y") then 
		  		response.write("Yes")
			else
				response.write("No")
			end if
		  %>
		  </td>
        </tr>
		<%
		if (strisinvoiceno="Y") then
		%>
		<tr height="30" id="trinvoiceno">
          <td align="right"><nobr>Invoice No:</nobr></td>
          <td><%=strinvoiceno%></td>
        </tr>
		<%
		end if
		%>
		<tr height="30" id="trinvoicedate">
          <td align="right"><nobr>Invoice Date:</nobr></td>
          <td><%=strinvoicedate%></font>
		  </td>
        </tr>
		
		 <tr height="30">
          <td align="right"><nobr>Name of person making complaint:</nobr></td>
          <td><%=strnameofpersonmakingcomplaint%></td>
        </tr>
		 <tr height="30">
          <td align="right"><nobr>Reason for credit:</nobr></td>
          <td><%=strreasonforcredit%></td>
        </tr>
		<tr height="30">
          <td align="right"><nobr>Responsibility for Complaint:</nobr></td>
          <td><%=strresponsibilityforcomplaint%></td>
        </tr>
		<tr height="30">
          <td align="right"><nobr>Product Name:</nobr></td>
          <td><%=strpname%></td>
        </tr>
        <tr height="30">
          <td align="right" valign="top"><nobr>Comments:</nobr></td>
          <td valign="top"><%=Replace(strcomplaint, vbCrLf,"<br>")%></td>
        </tr>
       
        <tr>
          <td></td>
          <td height="45">
           
			<input type="button" value="Back" name="Back"  onClick="javascript:goto('NewComplaint.asp')" style="font-family: Verdana; font-size: 12px; width:70px">   			
          </td>
        </tr>      

      </table>
	  </td>
	  </tr>
	  </table></td>
	  </form>
        </tr>
		
     </table>
	 </center>
</div>
<p>&nbsp;</p>
</body>
</html>
