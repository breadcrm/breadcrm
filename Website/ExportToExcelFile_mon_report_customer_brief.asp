<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
	fromdt=request("fromdt")
	todt=request("todt")
	if isdate(fromdt) and isdate(todt) then
		set objBakery = server.CreateObject("Bakery.reports")
		objBakery.SetEnvironment(strconnection)
		set retcol = objBakery.Display_customer_brief(fromdt,todt)
		recarray = retcol("Customer")
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=mon_report_customer_brief.xls" 
	%>
      <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%" colspan="5"><b>Customer Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
		<tr>
          <td width="5%"  bgcolor="#CCCCCC"><b>Customer Code&nbsp;</b></td>
          <td width="40%" bgcolor="#CCCCCC"><b>Customer Name&nbsp;</b></td>
          <td width="15%" bgcolor="#CCCCCC" align="right"><b>Total Number of Deliveries&nbsp;</b></td>
          <td width="15%" bgcolor="#CCCCCC" align="right"><b>Total Turnover&nbsp;</b></td>
          <td width="20%" bgcolor="#CCCCCC" align="right"><b>Average turnover per delivery&nbsp;</b></td>
        </tr>
		<tr>
				<%if isarray(recarray) then%>
				<% 				
				TotalDeliveries = 0
				Totalturnover = 0.00
				Averageturnover = 0.00
				
				%>
				<%for i=0 to UBound(recarray,2)%>
          <td width="5%"><b><%=recarray(0,i)%></b>&nbsp;</td>
          <td width="40%"><b><%=recarray(1,i)%></b>&nbsp;</td>
          <td width="15%" align="right" bgcolor="#CCCCCC"><b><%=recarray(2,i)%></b></td> 
          <td width="15%" align="right"><b><%if recarray(3,i)<> "" then Response.Write formatnumber(recarray(3,i),2) else Response.Write "0.00" end if%></b></td>
          <td width="20%" align="right" bgcolor="#CCCCCC"><b>        
          <%
          	if recarray(3,i)<> "" and recarray(3,i)<> "" then
           		if recarray(3,i)>recarray(2,i) then
		           Response.Write formatnumber(recarray(3,i)/recarray(2,i),2)
		        else
		         	Response.Write "0.00" 
		        end if
	        else
	         	Response.Write "0.00" 
           end if
           %>
           </b></td>
          </tr>
          <%
          if recarray(2,i) <> "" then
             TotalDeliveries = TotalDeliveries + recarray(2,i)
          end if
          if recarray(3,i)<> "" then 
						Totalturnover = Totalturnover + formatnumber(recarray(3,i),2)
				  End if
				  if recarray(4,i)<> "" then
						Averageturnover = Averageturnover +  formatnumber(recarray(4,i),2)
					End if
          %>
          <%next%>
          <tr>
           <td width="45%" colspan=2>&nbsp;<b>Grand Total</b></td>
          <td width="15%" align="right" bgcolor="#CCCCCC"><b><%=TotalDeliveries%></b>&nbsp;</td> 
          <td width="15%" align="right"><b><%=formatNumber(Totalturnover,2)%></b>&nbsp;</td>
          <td width="20%" align="right" bgcolor="#CCCCCC"><b>
          <%
          'code added by sv 12/05/05
          'Averageturnover = Totalturnover/TotalDeliveries
          %>
          <%'=formatNumber(Averageturnover,2)%>
          </b>&nbsp;</td>
          </tr>
          <%
          else%>
          <td colspan="5" width="100%" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td><%
          end if%>
        </tr>

      </table>
<%end if%>
</body>
</html>