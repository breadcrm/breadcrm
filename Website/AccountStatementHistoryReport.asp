<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">-->
<!--#INCLUDE FILE="includes/head.inc" -->
<!--#INCLUDE FILE="Includes/filepath.asp" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}
//-->

function OpenPDF(file)
{
document.form.method = 'post';
document.form.action = 'OpenPDF.asp?filename=' + file;
document.form.submit();

}


function Search_Submit()
{
document.form.method = 'post';
document.form.action = 'AccountStatementHistoryReport.asp';
document.form.submit();
}

</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%


dim fromdt, todt , i
dim nReportNo,sCustomerName
dim statementarray
dim statementcol
dim sFileName

strFilePath = strCustomerStatementReportPDF
fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")
nReportNo = Request.Form("txtReportNumber")
sCustomerName = Request.Form("txtCustomerName")

if nReportNo = "" Then
nReportNo = 0
End If
stop
if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

if isdate(fromdt) and isdate(todt) then   
    set objBakery = server.CreateObject("Bakery.reports")
	objBakery.SetEnvironment(strconnection)
	set statementcol = objBakery.CustomerAccountStatementReport(cint(nReportNo), sCustomerName, fromdt, todt)
    statementarray=statementcol("CustomerAccountStatementReport")
end if




set detail = Nothing
set object = Nothing
%>
<table border="0" width="90%" align="center" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
<tr>
    <td width="100%"><b><font size="3">Account Statement History Report<br>
      &nbsp;</font></b>
	<form method="post" action="AccountStatementHistoryReport.asp" name="form">
	 <!-- <form method="" action="" name="form">-->
      <table border="0" cellspacing="0" cellpadding="0" width="100%">
	  <tr>
	  <td>
	  	<table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr height="25">          
          <td><b>Report Number : &nbsp;</b></td>
		  <td><label>
		    <input type="text" name="txtReportNumber" <% if nReportNo <> 0 Then%> value="<%=nReportNo%>" <%end if%>>
		  </label></td>
        </tr>
		 <tr height="25">          
          <td><b>Customer Name :&nbsp;</b></td>
		  <td><label>
		    <input type="text" name="txtCustomerName" value="<%=sCustomerName%>">
		  </label></td>
        </tr>
		</table>

		</td>
		</tr>
		<tr>
		<td>
        <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
		<tr height="27">
          <td><b>From:</b></td>
          <td width="350">
                    <%
					
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom onFocus="blur();" size="12"  name="txtfrom" value="<%=fromdt%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
		  </td>
					<td><b>To:</b></td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=todt%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td> <td>
            <input type="button" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt" onClick="Search_Submit();"></td>
        </tr>
        </table>
		</td>
		</tr>
		</table>
	  </form>
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>

        <table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
		<tr>
          <td width="100%"  colspan="6" height="40" bgcolor="#FFFFFF"><b>Account Statement History Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
        <tr style="font-weight:bold" bgcolor="#CCCCCC">
            <td width="15%">Report No</td>    
            <td width="25%">Customer Name</td>	  
            <td width="25%">Group Name</td>	    
            <td width="25%">Date Sent</td>
			<td width="10%">Total Amont</td>    
        </tr>    
		    <%if isarray(statementarray) then%>
	            <%
					
	                for m=0 to UBound(statementarray,2)
						
				%>
                    <tr bgcolor="#FFFFFF">   	    
	                 
 <%
  
strFilePath = strCustomerStatementReportPDF  & statementarray(5,m)

Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
If objFSO.FileExists(strFilePath) Then

 %>
					   
<td><a onClick="OpenPDF('<%=statementarray(5,m)%>')" style="cursor:pointer; text-decoration:underline; color:#0033FF"  ><%=statementarray(0,m)%></a> </td>

<%Else %>
<td><%=statementarray(0,m)%></td>

<%End If %>						
						
	                    <td><%=statementarray(1,m)%></td>	
	                    <td><%=statementarray(2,m)%></td>	
	                    <td><%=statementarray(3,m)%></td>
						<td align="right"><%=statementarray(4,m)%></td>
                    </tr>    	    	            
	            <%
	                next
	            %>
            <%else%>
			<tr><td bgcolor="#FFFFFF" colspan="6" height="40"><p align="center"><font color="#FF0000"><strong>There are no records found.</strong></font></p></td></tr>	
            <%end if%>  
            
        </table>        
    </td>
</tr>
</table>
</td>
</tr>
</table>
</body>
<%if IsArray(recarray) then erase recarray  %>
</html>




