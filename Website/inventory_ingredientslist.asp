<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim Obj,ObjIngredent
Dim objResult
Dim arIngredents
Dim strName,strType
Dim vPageSize
Dim strDirection
Dim vpagecount
Dim vCurrentPage
Dim i
Dim vIntNo
dim vRecordcount



vPageSize = 999999
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vCurrentPage	= Request.Form("CurrentPage") 

strName = replace(Request.Form("txtName"),"'","''")
strType = replace(Request.Form("txtType"),"'","''")
vIntNo = Trim(Request.Form("IntNo")) 

If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if

vCurrentPage = 0

Set Obj = CreateObject("Bakery.Customer")
Obj.SetEnvironment(strconnection)
'Delete Status Update

if vIntNo <> "" Then
	objResult = obj.UpdateIng(vIntNo)  
End if

Set ObjIngredent = obj.DisplayIngList(vPageSize,vCurrentPage,strName,strType)
arIngredents = ObjIngredent("Ingredents")
vpagecount  = ObjIngredent("Pagecount")
vRecordcount  = ObjIngredent("Recordcount")

Set Obj = Nothing
Set ObjIngredent = Nothing
%>


<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>

<script Language="JavaScript">
<!--
function validate(frm){
	if(confirm("Would you like to delete " + frm.ingname.value + "?")){
		frm.submit();		
	}
}
//-->
</Script>



</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Ingredients List<br>
      &nbsp;</font></b>
      <form name="frmOrderSearch" method="post">
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          <td><b>Ingredient Name</b></td>
          <td><b>Type</b></td>          
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><input type="text" name="txtName"  value="<%=strName%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td><input type="text" name="txtType" value="<%=strType%>"  size="20" style="font-family: Verdana; font-size: 8pt"></td>                    
          <td><input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
       
      </table>
      </form>
      <%if vCurrentPage <> 0 then%>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">      
				<tr>
					<td height="20"></td>
				</tr><%				
				if IsArray(arIngredents) Then%>
					<tr>
					  <td align="right"><b>Page <%=vCurrentPage%> of <%=vpagecount%></b></td>          
					</tr><%
				End if%>	
	  </table> 
	  <%else%>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">      
				<tr>
					<td height="20" align = "right"><b>Total no of Ingredients : <%=vRecordcount%></b></td>
				</tr>	
	  </table> 	  
	  <%end if%>
      

      <table bgcolor="#666666" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
       <tr>
          <td width="10%" bgcolor="#CCCCCC"><b>ID</b></td>
          <td width="25%" bgcolor="#CCCCCC"><b>Ingredient Name</b></td>          
          <td width="25%" bgcolor="#CCCCCC"><b>Type</b></td>  
		  <td width="20%" bgcolor="#CCCCCC"><b>Mother Ingredient</b></td>                  
          <td width="20%" bgcolor="#CCCCCC" align="center"><b>Action</b></td>
        </tr><%        
						if IsArray(arIngredents) Then 
								for i = 0 to UBound(arIngredents,2)
									if (i mod 2 =0) then
										strBgColour="#FFFFFF"
									else
										strBgColour="#F3F3F3"
									end if	
								%>
									<tr bgcolor="<%=strBgColour%>">
										<td><%=arIngredents(0,i)%></td>
										<td><%=arIngredents(1,i)%></td>										
										<td><%=arIngredents(2,i)%></td>
										<td><%=arIngredents(4,i)%></td>	
																													
										<form method="POST" id="frming<%=i%>" name="frming<%=i%>">
											<td align="center">
												<input type="button" value="Edit" name="B1" onClick="document.location.href='EditInventory_ingredients.asp?Ino=<%=arIngredents(0,i)%>'" style="width:55px;font-family: Verdana; font-size: 8pt">&nbsp;&nbsp;									
												<input type="button" value="Delete" name="B1" onClick="validate(this.form);" style="width:55px;font-family: Verdana; font-size: 8pt">
											</td>
											<input type="hidden" name="ingname" value="<%=arIngredents(1,i)%>">
											<input type="hidden" name="IntNo" value="<%=arIngredents(0,i)%>">											
										</form>
									</tr><%
								Next
						else%>
						<tr><td colspan="4">Sorry no items found</td></tr><%		
						End if%>       
      </table>
    </td>
  </tr>
</table>

	
	<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		<FORM Name="Navigate">
			<tr>					
				<td width="187"><%				
		
					if vCurrentPage <> 1 and vCurrentpage <> 0 then%>
						<input type="button" value="FirstPage" Onclick="document.frmFirst.submit()" id=button1 name=button1 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) > 1 then%>
						<input type="button" value="PreviousPage" Onclick="document.frmPrevious.submit()" id=button3 name=button3 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%				
					if clng(vCurrentPage) < vPagecount then%>
						<input type="button" value="NextPage" Onclick="document.frmNext.submit()" id=button5 name=button5 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) <> vPagecount and vPagecount > 0  then%>
						<input type="button" value="LastPage" Onclick="document.frmLast.submit()" id=button7 name=button7 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>					
			</tr>
		</FORM>
	</table>
  </center>
</div>

	<FORM METHOD="post" NAME="frmFirst">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="First">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">				
		<INPUT TYPE="hidden" NAME="txtName"				VALUE="<%=strName%>">
		<INPUT TYPE="hidden" NAME="txtType"				VALUE="<%=strType%>">		 
	</FORM>

	<FORM METHOD="post" NAME="frmPrevious">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Previous">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtName"				VALUE="<%=strName%>">
		<INPUT TYPE="hidden" NAME="txtType"				VALUE="<%=strType%>">		 
	</FORM>

	<FORM METHOD="post" NAME="frmNext">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Next">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtName"				VALUE="<%=strName%>">
		<INPUT TYPE="hidden" NAME="txtType"				VALUE="<%=strType%>">		 
	</FORM>

	<FORM METHOD="post" NAME="frmLast">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Last">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtName"				VALUE="<%=strName%>">
		<INPUT TYPE="hidden" NAME="txtType"				VALUE="<%=strType%>">		 
	</FORM>


</body>
</html><%
if IsArray(arIngredents) Then Erase arIngredents
%>