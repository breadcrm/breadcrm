<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%
stop
sAction = Request.Form("Action")
hbody=Request.Form("txtContent")
hbody = Replace(hbody, "'", "^^^") 
Dim sMsg

if sAction = "Save" Then

    Call SaveFooter(hbody)
    sAction = ""
    
    if  sMsg = "OK" then
        msgSucess = "Footer Message Added Sucessfully"
    End if
    
End if

%>

<html XMLNS:ACE>
<head>
<title>Add Invoice Footer</title>
<style>
		BODY
			{
			FONT-FAMILY: Arial;FONT-SIZE: xx-small;
			COLOR: Black;
			}
		TABLE
			{
			FONT-SIZE: xx-small;
			FONT-FAMILY: Arial
			}
</style>	
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" onLoad="Init()" style="FONT-FAMILY: Arial;FONT-SIZE: x-small;" link=Blue vlink=MediumSlateBlue alink=MediumSlateBlue>

<?import namespace="ACE" implementation="ace.htc" />		
		<script language="JavaScript">
		function Init()
			{
			//Please set editorWidth > (larger than) 603 and editorHeight > (larger than) 355.
			idContent.editorWidth = "603";
			idContent.editorHeight = "355";
			//idContent.baseUrl = "http://localhost/YusASP/";
			//idContent.baseUrlNew = "./";
			
			//Use or not use Save Button
			idContent.useSave = true;
			
			//Use or not use Button
			idContent.useBtnInsertText = true;
			idContent.useBtnStyle = true;
			idContent.useBtnParagraph = true;
			idContent.useBtnFontName = true;
			idContent.useBtnFontSize = true;
			idContent.useBtnCut = true;
			idContent.useBtnCopy = true;
			idContent.useBtnPaste = true;
			idContent.useBtnUndo = true;
			idContent.useBtnRedo = true;
			//idContent.useBtnWord = true;
			idContent.putBtnBreak()
			idContent.useBtnBold = true;
			idContent.useBtnItalic = true;
			idContent.useBtnUnderline = true;
			idContent.useBtnStrikethrough = true;
			idContent.useBtnSuperscript = true;
			idContent.useBtnSubscript = true;
			idContent.useBtnJustifyLeft = true;
			idContent.useBtnJustifyCenter = true;
			idContent.useBtnJustifyRight = true;
			idContent.useBtnJustifyFull = true;
			idContent.useBtnInsertOrderedList = true;
			idContent.useBtnInsertUnorderedList = true;
			idContent.useBtnIndent = true;
			idContent.useBtnOutdent = true;
			idContent.useBtnHorizontalLine = true;
			idContent.useBtnTable = true;
			idContent.useBtnExternalLink = true;
			idContent.useBtnInternalLink = false;
			idContent.useBtnUnlink = true;
			idContent.useBtnInternalImage  = true;
			idContent.useBtnForeground  = true;
			idContent.useBtnBackground  = true;
			idContent.useBtnAbsolute  = true;
			idContent.useBtnRemoveFormat  = true;
			idContent.useBtnInsertSymbol  = true;


			idContent.applyButtons()	
			
			//fill editor with content
			idContent.content=idContentTemp.innerHTML
			}	
			
		function OpenImgLookup()
			{
			//You can provide your own image library here. 
			//This is just to show how to use/open Image Library (using our built in ASP-based Image Library)
			var popleft=((document.body.clientWidth - 440) / 2)+window.screenLeft; 
			var poptop=(((document.body.clientHeight - 460) / 2))+window.screenTop-40;		
			window.open("Image.asp?cno=<%=cno%>","Images","scrollbars=NO,width=480,height=520,left="+popleft+",top="+poptop)
			}		
				
		function OpenLinkLookup()
			{
			//You can provide your own Internal Link Lookup here. 
			var popleft=((document.body.clientWidth - 50) / 2)+window.screenLeft; 
			var poptop=(((document.body.clientHeight - 460) / 2))+window.screenTop-40;		
			window.open("DocLinks.asp?cno=<%=cno%>","Links","scrollbars=NO,width=300,height=400,left="+popleft+",top="+poptop)
			}	
		
			
		
function Save()
          {
         
          Form1.txtContent.value = idContent.content;

          //You can validate your form here
          if (Form1.txtContent.value == "")
          {
            alert("Please fill the Footer Text");
            return ;
          }
          document.getElementById('Action').value = "Save";  
          Form1.submit();  
          }             
       </script>
<br>

<table border=0 cellpadding=2 cellspacing=0 align=center>
<tr><td style="color:Red; text-align:center;" ><%=msgSucess%></td></tr>
<tr>
<td valign=top colspan=2>
<form name="SaveForm" method="post" action="TextEditorInvoiceFooter.asp" ID=Form1>&nbsp;
  <input type="hidden" name="txtContent" ID="txtContent">
  <input type="hidden" name="Action" value="" />  
</form>
<ACE:AdvContentEditor id="idContent" value="test"   content="" onSave="Save();" onImgLookupOpen="OpenImgLookup()" onlinkLookupOpen="OpenLinkLookup()"/>
<div id=idContentTemp style="display:none"><%=GetFooterText()%></div>
<!--<div id=idContentTemp style="display:none"></div>
-->
</td>
</tr>
</table>
</body>
</html>

<%
Sub SaveFooter(hbody)
Set obj = CreateObject("Bakery.General")
obj.SetEnvironment(strconnection)


set result = obj.SaveInvoiceFooter(hbody)

if result("Sucess") = "OK" then

    sMsg = "OK"

End If

End Sub


Function GetFooterText()
stop
Dim arFooter,objFooter,obj,hbody
Set obj = CreateObject("Bakery.General")
obj.SetEnvironment(strconnection)
set objFooter = obj.GetFooterText()
arFooter = objFooter("FooterMessage")

If IsArray(arFooter) Then

    hbody = Replace(arFooter(0,0), "^^^", "'") 

    GetFooterText = hbody
Else

   GetFooterText = Empty

End if

End Function
%>