<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim Obj,ObjDough
Dim objResult
Dim arDough
Dim strName
Dim vPageSize
Dim strDirection
Dim vpagecount
Dim vCurrentPage
Dim i
dim vDoughNo, strBgColour

vPageSize = 999999
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount")
vCurrentPage	= Request.Form("CurrentPage")
vDoughNo = Trim(Request.Form("DoughNo"))
strName = replace(Request.Form("txtName"),"'","''")

If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if

vCurrentPage = 0

Set Obj = CreateObject("Bakery.customer")
Obj.SetEnvironment(strconnection)

if vDoughNo <> "" Then
	objResult = obj.UpdateDough(vDoughNo)
End if


Set ObjDough = obj.DisplayDoughListNew(vPageSize,vCurrentPage,strName)
arDough = ObjDough("Doughs")
vpagecount  = ObjDough("Pagecount")

Set Obj = Nothing
Set ObjDough = Nothing
%>


<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script Language="JavaScript">
<!--
function validate(frm){
	if(confirm("Would you like to delete " + frm.doughname.value + " ?")){
		frm.submit();		
	}
}
//-->
</Script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<br><br>
<div align="center">
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">List of Doughs</font></b>
      <form name="frmOrderSearch" method="post">
      
      <table align="center" border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td><b>Search By Dough Name&nbsp;</b></td>
          <td><input type="text" name="txtName"  value="<%=strName%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>          
          <td><input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
      
      </table>
      </form>
      <%if vCurrentPage <> 0 then%>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">      
				<tr>
					<td height="20"></td>
				</tr><%				
				if IsArray(arDough) Then%>
					<tr>
					  <td align="right"><b>Page <%=vCurrentPage%> of <%=vpagecount%></b></td>          
					</tr><%
				End if%>	
	  </table> 
	  <%end if%>      

      <table bgcolor="#666666" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
        <tr height="22">
          <td width="10%" bgcolor="#CCCCCC"><b>ID</b></td>
          <td width="30%" bgcolor="#CCCCCC"><b>Dough Name</b></td>
		  <td width="20%" bgcolor="#CCCCCC"><b>Dough Type</b></td>
		  <!--<td width="20%" bgcolor="#CCCCCC"><b>Is Second Mix Category</b></td>-->
		  <td width="20%" bgcolor="#CCCCCC"><b>Dough Mixing Method</b></td>
          <td width="20%" bgcolor="#CCCCCC" align="center"><b>Action</b></td>
        </tr>
		<%        
		if IsArray(arDough) Then 
			for i = 0 to UBound(arDough,2)
				if (i mod 2 =0) then
					strBgColour="#FFFFFF"
				else
					strBgColour="#F3F3F3"
				end if				
			%>
				 <tr bgcolor="<%=strBgColour%>">
					<td><%=arDough(0,i)%></td>
					<td><%=arDough(1,i)%></td>	
					<td><%=arDough(6,i)%></td>
					<!--<td><%=arDough(7,i)%></td>-->
					<td><%=arDough(8,i)%></td>
					<form method="POST" id="frmDough<%=i%>" name="frmDough<%=i%>">
						<input type = "hidden" name="doughname" value="<%=arDough(1,i)%>">
						<td align="center">
						<input type="button" value="View" name="btnView" onClick="document.location.href='DoughViewNew.asp?DNo=<%=arDough(0,i)%>'" style="width:55px; font-family: Verdana; font-size: 8pt">&nbsp;&nbsp;&nbsp;
						<input type="button" value="Edit" name="B1"		onclick="document.location.href='Editinventory_doughs_new.asp?DNo=<%=arDough(0,i)%>'" style="width:55px; font-family: Verdana; font-size: 8pt">&nbsp;&nbsp;&nbsp;
						<input type="button" value="Delete" name="B1" onClick="validate(this.form);" style="width:55px; font-family: Verdana; font-size: 8pt">
						</td>
						<input type="hidden" name="DoughNo" value="<%=arDough(0,i)%>">
					</form>
				</tr>
			<%
			Next
		else
		%>
		<tr><td colspan="5" align="center" bgcolor="#FFFFFF" height="22"><strong><font color="#FF0000">Sorry no items found.</font></strong></td></tr>
		<%End if%>       
      </table>
    </td>
  </tr>
</table>

<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		<FORM Name="Navigate">
			<tr>					
				<td width="187"><%				
		
					if vCurrentPage <> 1 and vCurrentPage <> 0 then%>
						<input type="button" value="FirstPage" Onclick="document.frmFirst.submit()" id=button1 name=button1 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) > 1 then%>
						<input type="button" value="PreviousPage" Onclick="document.frmPrevious.submit()" id=button3 name=button3 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%				
					if clng(vCurrentPage) < vPagecount then%>
						<input type="button" value="NextPage" Onclick="document.frmNext.submit()" id=button5 name=button5 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) <> vPagecount and vPagecount > 0  then%>
						<input type="button" value="LastPage" Onclick="document.frmLast.submit()" id=button7 name=button7 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>					
			</tr>
		</FORM>
	</table>
  </center>
</div>

	<FORM METHOD="post" NAME="frmFirst">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="First">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">				
		<INPUT TYPE="hidden" NAME="txtName"				VALUE="<%=strName%>">		
	</FORM>

	<FORM METHOD="post" NAME="frmPrevious">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Previous">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtName"				VALUE="<%=strName%>">		
	</FORM>

	<FORM METHOD="post" NAME="frmNext">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Next">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtName"				VALUE="<%=strName%>">		
	</FORM>

	<FORM METHOD="post" NAME="frmLast">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Last">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtName"				VALUE="<%=strName%>">		
	</FORM>


</body>
</html><%
if IsArray(arDough) Then Erase arDough
%>