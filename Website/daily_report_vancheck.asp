<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim intI
Dim intJ
Dim intF
Dim intN
Dim intQ
Dim intP
Dim intD
Dim intRow
Dim delType
Dim delDate
Dim vanNo
Dim arVanCheckListDetails
Dim arFacilityList
Dim arVanCheckList
Dim intTotal
Dim rowCount
intN = 0
intJ = 0
intQ = 0
intP = 1
intTotal = 0
intF = 0
intD = 0
rowCount = 0
stop
delDate= request.form("deldate")
vanNo= request.form("van")
delType=Request.Form("deltype")
strfacility= request.form("facility")

if delDate= "" or vanNo = "" or delType="" then response.redirect "daily_report.asp"

set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
set DisplayReport= object.Display_VanCheckListWithMultiProduct(vanNo, delType, delDate, strfacility)
arVanCheckListDetails = DisplayReport("VanCheckListDetails")
arFacilityList = DisplayReport("FacilityList")
arVanCheckList = DisplayReport("VanCheckList")
set DisplayReport= nothing
set object = nothing

If delType = "1" Then
	delType = "Morning"
ElseIf delType = "2" Then
	delType = "Noon"
ElseIf delType = "3" Then
	delType = "Evening"
End If

if IsArray(arFacilityList) And IsArray(arVanCheckList) Then
	intP = 1
	For intI = 0 To ubound(arFacilityList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arVanCheckList, 2)
			if (arFacilityList(0, intI) = arVanCheckList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
				End if
				if intN = 0 Then
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
			End if

			if intJ = ubound(arVanCheckList, 2) And intRow + 5 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arVanCheckList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arVanCheckList, 2) Then
					intTotal = 0
					intRow = intRow + 1
				End if
				if intI <= ubound(arFacilityList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arFacilityList, 2) Then
						intP = intP + 1
					End if
				End if
				intQ = 0				
			End if
		Next
		intD = intD + 1
	Next
Else
	intP = 1
End if

intN = 1
intJ = 0
intQ = 0
intF = 0
intD = 0
intTotal = 0
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>VAN CHECK LIST</title>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<p>&nbsp;</p><%
if IsArray(arVanCheckListDetails) Then
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
<td>
<p align="center"><font size="3"><b>VAN CHECK LIST - <%if deltype="All" then%>(Morning, Noon, Evening)<%else%>(<%=deltype%>)<%end if%></b></font></p>
</td>
</tr>
<tr>
<td>
<br><br>
<b>Summary orders for VAN: <%= vanNo %><br>
Date of report: <%= Day(Now()) & "/" & Month(Now()) & "/" & Year(Now()) %><br>
To be delivered on: <%= Day(FormatDateTime(delDate, 2)) & "/" & Month(FormatDateTime(delDate, 2)) & "/" & Year(FormatDateTime(delDate, 2)) %> (<%= delType %>)<br><br>
Number of pages for this report: <%= intP %><br><br>
</td>
</tr>
</table><%
End if
stop
if IsArray(arFacilityList) And IsArray(arVanCheckList) Then
	For intI = 0 To ubound(arFacilityList, 2)
		if intI = 0 Then
			intRow = 9
		End if
%>

<%'If arFacilityList(1, intI) =  arFacilityList(1, intI + 1) and intI = 0 Then%>

<%'End If %>
<%
		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arVanCheckList, 2)
			if (arFacilityList(0, intI) = arVanCheckList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					
%>

<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
	<tr>
	<td width="75%" height="20"><b><%= arFacilityList(1, intI) %></b></td>
	</tr>
</table>

<table align="center" border="1" width="90%" cellspacing="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000" cellpadding="0"><%
				End if
				if intN = 0 Then
%>
	<tr>
	<td width="11%" height="20"><b>Product code</b></td>
	<td width="56%" height="20"><b>Product name</b></td>
	<td width="8%" height="20"><b>Quantity</b></td>
	</tr><%
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
	%>
	<tr>
	<td width="11%" height="20"><%= arVanCheckList(2, intJ) %></td>
	<td width="56%" height="20"><%= arVanCheckList(3, intJ) %></td>
	<td width="8%" height="20"><%= arVanCheckList(4, intJ) %></td>
	</tr><%
					intTotal = intTotal + arVanCheckList(4, intJ)
					rowCount = rowCount + 1
			End if

			if intJ = ubound(arVanCheckList, 2) And intRow + 5 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arVanCheckList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arVanCheckList, 2) and rowCount >=1 Then
    %>
	<tr>
	<td width="67%" height="20" colspan="2" align="right"><b>Total&nbsp; </b></td>
	<td width="8%" height="20"><b><%= intTotal %>&nbsp;</b></td>
	</tr><%
					intTotal = 0
					intRow = intRow + 1
					rowCount = 0
				End if
%>
</table><%
				if intI <= ubound(arFacilityList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arFacilityList, 2) Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
					End if
				End if
				intQ = 0				
			End if
			
			
		Next
		intD = intD + 1
	Next
Else
%>
<table align="center" border="1" width="90%" cellspacing="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000" cellpadding="0">
	<tr>
	<td width="100%" height="20" colspan="3"><b>Sorry no records found</b></td>
	</tr>
</table><%
End if
%>
<p>&nbsp;</p>
</body>
</html><%
If IsArray(arVanCheckListDetails) Then erase arVanCheckListDetails
If IsArray(arFacilityList) Then erase arFacilityList
If IsArray(arVanCheckList) Then erase arVanCheckList
%>