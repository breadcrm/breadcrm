<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim obj
Dim objInventory
Dim arFacility,arIngerdient
Dim i
Dim strDate,strFacCode
Dim strIngStream 
Dim arSplit,arUpdate
Dim vIndex 
Dim objResult

if Trim(Request.form("txtDate")) = "" then
	strDate = Day(date()) & "/" & Month(date()) & "/" & Year(date())
	arSplit = Split(strDate,"/")
	strDate = Right( "0" & arSplit(0),2) & "/" & Right( "0" & arSplit(1),2) & "/" & arSplit(2)
else
	strDate = Trim(Request.form("txtDate"))
end if
strFacCode = Trim(Request.form("selFacCode"))

Set obj = Server.CreateObject("bakery.general")
obj.SetEnvironment(strconnection)
set objInventory = obj.DisplayFacility()	
arFacility =  objInventory("Facility") 

If Trim(Request.Form("Update")) <> "" Then
	strIngStream  = Request.Form("txtIngStream") 	
	if Trim(strIngStream) <> "" Then
		arSplit = Split(strIngStream,",")
		vindex = 0
		Redim arUpdate(1,vindex)				
		For i = 0 to UBound(arSplit) 			
			If Trim(Request.Form("qty"&arSplit(i))) <> "" Then
				If IsNumeric(Trim(Request.Form("qty"&arSplit(i))))  Then
					If Trim(Request.Form("qty"&arSplit(i))) >= 0  Then
						Redim preserve arUpdate(1,vIndex)
						arUpdate(0,vIndex) = trim(arSplit(i))
						arUpdate(1,vIndex) = Trim(Request.Form("qty"&arSplit(i)))
						vIndex = vIndex +1													
					End if
				End if
			End if
		Next				
		objResult = obj.Update_InventoryCount(strDate,strFacCode,arUpdate)						
	End if
End if

If strDate <> "" And  strFacCode <> "" Then
	set objInventory = obj.Display_InventoryCount(strDate,strFacCode)
	arIngerdient = objInventory("Ingredient")	 	
End if 

set obj = Nothing
set objInventory = Nothing
If strDate = "" Then
	strDate = Day(date()+1) & "/" & Month(date()+1) & "/" & Year(date()+1)  
End if
%>



<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT LANGUAGE=javascript>
<!--
function initValueCal(theForm,theForm2){
	y=theForm.txtDate.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm2.value=t.getDate() +'/'+eval((t.getMonth())+1) +'/'+ t.getFullYear();
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
		
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
//-->
</SCRIPT>
<script Language="JavaScript"><!--

function validateFacility(theForm){
	if(theForm.txtDate.value == ""){
    alert("Please enter a value for the \"Date\" field.");
    theForm.selFacCode.selectedIndex = 0;
    theForm.txtDate.focus();
    return;
  }  
	theForm.submit();
}


function validator(theForm){
	if(theForm.txtDate.value == ""){
    alert("Please enter a value for the \"Date\" field.");
    theForm.txtDate.focus();
    return;}  

  if(theForm.selFacCode.selectedIndex == 0){
		alert("The first \"Facility\" option is not a valid selection.  Please choose one of the other options.");
    theForm.selFacCode.focus();
    return;}	
    theForm.Update.value="True";	
		theForm.submit();
}

//-->
</script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal(document.frmIntCount,document.frmIntCount.txtDate)">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;

<form method="POST" action="inventory_count.asp" name="frmIntCount"> 
<div align="center">
 
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Stock Count<br>
      &nbsp;</font></b><%
If objResult <> "" Then
	If objResult <> "OK" then
		Response.Write "<br><font size=""3"" color=""#FF0000""><b>Error occured while making Inventory Count entry</b></font><br><br>"
	Else
		Response.Write "<br><font size=""3"" color=""#FF0000""><b>Inventory Count entry added successfully</b></font><br><br>"
	End if
End if	
%>
      <table border="0" width="70%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        
        <tr>
          <td height="15">Date</td>
          <TD width="375">
	    <%
		dim arrayMonth
		arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
	    %>
		<select name="day" onChange="setCal(this.form,document.frmIntCount.txtDate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.frmIntCount.txtDate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.frmIntCount.txtDate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt id=txtDate  name="txtDate" value="<%=strDate%>" onFocus="blur();" onChange="setCalCombo(this.form,document.frmIntCount.txtDate)">
      <IMG id=f_trigger_fr onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  </TD>
		   </tr>
       
      
        <tr>
          <td>Production Facility</td>
          <td>
						<select size="1" name="selFacCode" style="font-family: Verdana; font-size: 8pt" onChange="validateFacility(this.form)">
							<option value="">Select</option><%
							for i = 0 to ubound(arFacility ,2)  				
								if arFacility (0,i) = "15" or arFacility (0,i) = "18" or arFacility (0,i) = "90" then%>
									<option value="<%=arFacility(0,i)%>"  <%if Trim(arFacility(0,i)) = strFacCode Then Response.Write "Selected"%>><%=arFacility(1,i)%></option><%
                end if
              next%>                        
           </select>
          </td>
        </tr>
        
        <tr>
          <td colspan="2" height="10"></td>          
        </tr>
        
        <tr>
          <td><b>Ingredients</b></td>
          <td></td>
        </tr>
        
        <tr>
          <td colspan="2">          
						<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2"><%
							if IsArray(arIngerdient) Then
								strIngStream = ""%>
								<TR>
									<TD><b>Description</b></TD>
									<TD><b>Unit Measure</b></TD>
									<TD><b>Qty</b></TD>											
								</TR><%								
								For i = 0  to UBound(arIngerdient,2)%>
									<TR>
										<TD><%=arIngerdient(1,i)%></TD>
										<TD><%=arIngerdient(2,i)%></TD>
										<TD><input type="text" name="qty<%=arIngerdient(0,i)%>" value="<%=arIngerdient(3,i)%>" size="10" style="font-family: Verdana; font-size: 8pt"></TD>											
									</TR><%
									strIngStream = strIngStream & arIngerdient(0,i) & ","
								Next
								strIngStream = left(strIngStream,len(strIngStream)-1)
							elseif 	strDate <> "" And  strFacCode <> "" Then%>
								<TR>
									<TD>Sorry no items found</TD>
								<TR><%	
							End if%>		
						</table>						
          </td>          
        </tr>
          
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        
        <tr>
					<td>
                    <input type="button" value="Update" onClick="validator(this.form)" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
          <td></td>          
        </tr>
        
      </table>
		
    </td>
  </tr>
</table>
</div>
<Input type="hidden" name="Update" value="">
<Input type="hidden" name="txtIngStream" value="<%=strIngStream%>">
</form>
<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtDate",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr",
				singleClick    :    true
			});
</SCRIPT>
</body>

</html><%
if IsArray(arFacility) Then Erase arFacility
if IsArray(arIngerdient) Then Erase arIngerdient%>