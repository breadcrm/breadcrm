<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim Obj,ObjSearch
Dim arProduct
Dim arDough
Dim vCustNo
Dim strProdCode,strProdName,strDough
Dim vPageSize
Dim strDirection
Dim vpagecount
Dim vCurrentPage
Dim i
Dim vGNo

If Trim(Request.QueryString("CustNo")) <> "" Then 
	vCustNo = Request.QueryString("CustNo") 
Else
	vCustNo = Request.Form("CustNo") 
End if	

If Trim(Request.QueryString("GNo")) <> "" Then 
	vGNo = Request.QueryString("GNo") 
End if	

vPageSize = 999999
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vCurrentPage	= Request.Form("CurrentPage") 
strProdCode = Replace(Request.Form("txtProdCode"),"'","''") 
strProdName = Replace(Request.Form("txtProdName"),"'","''") 

strDough = Request.Form("selDough") 

If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if
stop
Set Obj = CreateObject("Bakery.Customer")
Obj.SetEnvironment(strconnection)
Set ObjSearch = obj.Display_SearchProductList(vPageSize,vCurrentPage,strProdCode,strProdName,strDough,vCustNo) 
arProduct = ObjSearch("ProductList")
arDough = ObjSearch("Dough")
vpagecount  = ObjSearch("Pagecount")
Set Obj = Nothing
Set ObjSearch = Nothing
stop
%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<SCRIPT LANGUAGE="Javascript">
<!--
function addStanding(vProdCode){
	document.frmProdSearch.action = "customer_products1.asp";
	document.frmProdSearch.ProdCode.value = vProdCode;	
	document.frmProdSearch.submit(); 
}

function addCustmerSpecialProductPrice(vProdCode,vGno) {
    document.frmProdSearch.action = "customer_group_specialsprice.asp";
    document.frmProdSearch.ProdCode.value = vProdCode;
    document.frmProdSearch.GNo.value = vGno;
    document.frmProdSearch.submit();
}	
//-->
</SCRIPT>



</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>

<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
<FORM name="frmProdSearch" Method="Post">	
  <tr>
    <td width="100%"><b><font size="3">Find product<br>
      &nbsp;</font></b>
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          <td><b>Product Name:</b></td>
          <td><b>Product Code:</b></td>
          <td><b>Dough Name</b></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><input type="text" name="txtProdName" value="<%=strProdName%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td><input type="text" name="txtProdCode" value="<%=strProdCode%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td><select size="1" name="selDough" style="font-family: Verdana; font-size: 8pt">
							<option value="">Select</option><%          
              					If IsArray(arDough) then
								For i = 0 to UBound(arDough,2)%>
									<option value="<%=arDough(0,i)%>" <%if Trim(strDough) = Trim(arDough(0,i)) Then Response.Write "Selected"%>><%=arDough(1,i)%></option><%
								Next 						
							End if%>
            </select></td>
          <td>
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
		
	<!--
	<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">      
				<tr>
					<td height="20"></td>
				</tr><%
				
				if IsArray(arProduct) Then%>
					<tr>
					  <td align="right"><b>Page <%=vCurrentPage%> of <%=vpagecount%></b></td>          
					</tr><%
				End if%>	
      </table>
      -->

      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">  
			
        <tr>
          <td width="33%" bgcolor="#CCCCCC"><b>Product Code</b></td>
          <td width="33%" bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td width="33%" bgcolor="#CCCCCC"><b>Dough Name</b></td>
          <td align = "center" width="34%" bgcolor="#CCCCCC"><b>Action</b></td>
        </tr>
        <% 
        stop       
        If (not (strProdCode= "" and strProdName="" and strDough = "")) and IsArray(arProduct) Then
					For i = 0 To UBound(arProduct,2)%>         
						<tr>
						  <td width="33%"><%=arProduct(0,i)%></td>
						  <td width="33%"><%=arProduct(1,i)%></td>
						  <td width="33%"><%=arProduct(2,i)%></td>          
						  <td width="34%"><%
								
								
								 if vCustNo = "-1" Then%>
								    <input type="button" value="Add Group Special Price" name="B1" style="font-family: Verdana; font-size: 8pt" onClick="addCustmerSpecialProductPrice('<%=arProduct(0,i)%>','<%=vGNo%>')">

								<%Elseif Trim(arProduct(3,i)) = "N" Then%>
									<input type="button" value="Add to Typical Order     " name="B1" style="font-family: Verdana; font-size: 8pt" onClick="addStanding('<%=arProduct(0,i)%>')"><%
								Else%>
									<input type="button" value="Update to Typical Order" name="B1" style="font-family: Verdana; font-size: 8pt" onClick="addStanding('<%=arProduct(0,i)%>')"><%
								End if%>			
									
							</td>          
						</tr><%
					Next
				Else%>
					<tr>
						<td Colspan="4">Sorry no items found</td>						
					</tr><%
				End if%>		 
      </table>
    </td>
  </tr>  
  <input type="hidden" name="CustNo" value="<%=vCustNo%>">
  <input type="hidden" name="ProdCode" value="">  
  <input type="hidden" name="GNo" value="<%=vGNo%>">  
  
</FORM>  

	<tr>
		<td colspan="4" height="30"></td>						  
	</tr>	
</table>

	<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		<FORM Name="Navigate">
			<tr>					
				<td width="187"><%				
		
					if vCurrentPage <> 1 then%>
						<input type="button" value="FirstPage" Onclick="document.frmFirst.submit()" id=button1 name=button1 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) > 1 then%>
						<input type="button" value="PreviousPage" Onclick="document.frmPrevious.submit()" id=button3 name=button3 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%				
					if clng(vCurrentPage) < vPagecount then%>
						<input type="button" value="NextPage" Onclick="document.frmNext.submit()" id=button5 name=button5 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) <> vPagecount and vPagecount > 0  then%>
						<input type="button" value="LastPage" Onclick="document.frmLast.submit()" id=button7 name=button7 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>					
			</tr>
		</FORM>
	</table>

	<FORM METHOD="post" NAME="frmFirst">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="First">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">		
		<INPUT TYPE="hidden" NAME="txtProdCode"		VALUE="<%=strProdCode%>">
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="selDough"		VALUE="<%=strDough%>">
		<input type="hidden" name="CustNo" value="<%=vCustNo%>">
	</FORM>

	<FORM METHOD="post" NAME="frmPrevious">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Previous">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"		VALUE="<%=strProdCode%>">
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="selDough"		VALUE="<%=strDough%>">
		<input type="hidden" name="CustNo" value="<%=vCustNo%>">
	</FORM>

	<FORM METHOD="post" NAME="frmNext">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Next">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"		VALUE="<%=strProdCode%>">
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="selDough"		VALUE="<%=strDough%>">
		<input type="hidden" name="CustNo" value="<%=vCustNo%>">
	</FORM>

	<FORM METHOD="post" NAME="frmLast">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Last">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtProdCode"		VALUE="<%=strProdCode%>">
		<INPUT TYPE="hidden" NAME="txtProdName"		VALUE="<%=strProdName%>">
		<INPUT TYPE="hidden" NAME="selDough"		VALUE="<%=strDough%>">
		<input type="hidden" name="CustNo" value="<%=vCustNo%>">
	</FORM>
</div>
</body>
</html><%
if IsArray(arProduct) Then Erase arProduct 
if IsArray(arDough) Then Erase arDough
%>