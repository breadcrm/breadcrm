<%@ Language=VBScript%>
<%option Explicit
Response.Buffer=false
%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--

    function onClick_Report(action) {

        document.Form1.Action.value = action;
      
        document.Form1.submit();

    }


    function Hide() {
        document.getElementById("HidePanel").style.display = "none";
    }


//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal();Hide()">
<!--#INCLUDE FILE="nav.inc" -->
<br>
<DIV id="HidePanel" style="display:none;" >
	<P align="center"><b><font size="2" face="Verdana" color="#0099CC">Report generation in progress, Please wait...</font></b></P>
	<p align="center"><img name="animation" src="images/LoadingAnimation.gif" border="0" WIDTH="201" HEIGHT="33"></p>
</DIV>
<%
Dim objBakery
Dim recarray
Dim retcol
Dim sAction
Dim fileName
Dim msgSucess
stop
sAction = Request.Form("Action")

stop

set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)

if sAction = "Report1" Then
    fileName = "D:\Electronic Invoice\reports\SOR_" & Day(now()) & Month(now()) & Year(now()) & "_" & Hour(now()) & Minute(now()) & Second(now()) & ".xls"
    set retcol = objBakery.GenerateCustomerReport1(fileName)
    
    
    if retcol("Sucess") = "OK" then
        msgSucess = "Sucessfully created the report"
    End If

    
End If

if sAction = "Report2" Then
    fileName = "D:\Electronic Invoice\reports\DAOR_" & Day(now()) & Month(now()) & Year(now()) & "_" & Hour(now()) & Minute(now()) & Second(now()) & ".xls"
    set retcol = objBakery.GenerateCustomerReport2(fileName)
    
    
    if retcol("Sucess") = "OK" then
        msgSucess = "Sucessfully created the report"
    End If

    
End If

if sAction = "Report3" Then
    fileName = "D:\Electronic Invoice\reports\CPR_" & Day(now()) & Month(now()) & Year(now()) & "_" & Hour(now()) & Minute(now()) & Second(now()) & ".xls"
    set retcol = objBakery.GenerateCustomerReport3(fileName)
    
    
    if retcol("Sucess") = "OK" then
        msgSucess = "Sucessfully created the report. FileName is - " & fileName
    End If

    
End If

%>
<div align="center">
<center>
<table border="0" width="100%" cellspacing="0" cellpadding="5" style="font-family: Verdana; font-size: 8pt">

 <tr><td style="color:Red; text-align:center;" ><%=msgSucess%></td></tr>
 <tr>
    <td width="100%"><b><font size="3">Reports<br>
      &nbsp;</font></b>
	  <form method="POST"  name="Form1" action="">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="800px">
       
          <tr>
              <td>
              Standing Orders Report
              </td>
            
              <td>
                  <input type="button" value="Generate Report" name="Search" style="font-family: Verdana; font-size: 8pt" onclick="onClick_Report('Report1');">
              </td>
              
          </tr>
            <tr>
              <td>
              All Daily Amend Orders
              </td>
            
              <td>
                  <input type="button" value="Generate Report" name="Search" style="font-family: Verdana; font-size: 8pt" onclick="onClick_Report('Report2');">
              </td>
             
          </tr>
            <tr>
              <td>
              Customer Profiles 
              </td>
            
              <td>
                  <input type="button" value="Generate Report" name="Search" style="font-family: Verdana; font-size: 8pt" onclick="onClick_Report('Report3');">
              </td>
             
          </tr>
        <tr>
          <td></td>
          <td></td>
         
        </tr>
      </table>
      <input type="hidden" name="Action" value="">
	  </form>
	
	 
    </td>
  </tr>
</table>
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;<br><br><br><br><br><br>

</center>
</div>
</body>

</html>