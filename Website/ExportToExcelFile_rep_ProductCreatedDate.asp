<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
	
dim objBakery
dim recarray
dim retcol




set objBakery = server.CreateObject("Bakery.Reports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.Display_ReportProductCreatedDateReport()
recarray = retcol("ProductReport")


	
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=ProductCreatedDate_report.xls" 
	%>
      <table border="0" width="100%" cellspacing="0" cellpadding="5" style="font-family: Verdana; font-size: 8pt">
<tr>
    <td width="100%"><b><font size="3">Product Created Date Report<br>
      &nbsp;</font></b>
      </td>
      </tr>

 
 <tr>
    <td width="100%">
	  
      	<table border="1" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
		<tr>
          <td colspan="7"><b>Product Created Date Report</b></td>
        </tr>
        <tr>
          <td width="110" bgcolor="#CCCCCC"><b>Product Number</b></td>
          <td width="250" bgcolor="#CCCCCC"><b>Product Name</b></td>
		  <td width="110" bgcolor="#CCCCCC"><b>Created Date</b></td>
		  <td width="100" bgcolor="#CCCCCC"><b>Status</b></td>
          
        </tr>
       <%if isarray(recarray) then%>
		
		<%for i=0 to UBound(recarray,2)%>
		<tr>
           <td><%=recarray(0,i)%></td>
          <td><%=recarray(1,i)%></td>
		  <td><%=recarray(2,i)%></td>
         <td><%=recarray(3,i)%></td>
         
         </tr>
         
          <%next
         
          else%>
          	<tr><td colspan="7"  bgcolor="#CCCCCC"><b>0 - No records matched...</b></td></tr>
		  <%end if%>
		  
		  
		 
     
      </table>
    </td>
  </tr>
</table>







