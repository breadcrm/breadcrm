<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->

<%
vEno= request.form("eno")
if vEno = "" then response.redirect "ResellerList.asp"

set object = Server.CreateObject("bakery.Reseller")
object.SetEnvironment(strconnection)
set DisplayResellerDetail= object.DisplayResellerDetail(vEno)
vRecArray = DisplayResellerDetail("ResellerDetail")
set DisplayResellerDetail= nothing
set object = nothing
if not isarray(vRecArray) then response.redirect "ResellerList.asp"
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<table border="0" width="100%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt" align="center">
<tr>
    <td width="100%" align="left">&nbsp;&nbsp;&nbsp;<b><font size="3">View Reseller - <%=vrecarray(1,0)%></font></b>
        <table border="0" width="600" cellspacing="0" cellpadding="2" align="center">
        <tr>
        <td valign="top"><br>
		<table border="0" cellspacing="0" cellpadding="4" style="font-family: Verdana; font-size: 8pt" width="100%">
        <tr>
          <td width="180" align="right"><b>Reseller ID&nbsp;:</b></td>
          <td><%=vrecarray(0,0)%></td>
        </tr>
        <tr>
          <td align="right"><b>Reseller Name:</b></td>
          <td><%=vrecarray(1,0)%></td>
        </tr>
		 <tr>
          <td align="right"><b>Old Code:</b></td>
          <td><%=vrecarray(12,0)%></td>
        </tr>
         <tr>
          <td align="right" valign="top"><b>Address:</b></td>
          <td><%=replace(vrecarray(2,0),vbcrlf,"<br>")%></td>
        </tr>
		 <tr>
          <td align="right"><b>Town:</b></td>
          <td><%=vrecarray(3,0)%></td>
        </tr>
		 <tr>
          <td align="right"><b>Postcode:</b></td>
          <td><%=vrecarray(4,0)%></td>
        </tr>
		 <tr>
          <td align="right"><b>Telephone:</b></td>
          <td><%=vrecarray(5,0)%></td>
        </tr>
		 <tr>
          <td align="right"><b>Fax:</b></td>
          <td><%=vrecarray(6,0)%></td>
        </tr>
		<%
			arrPriceBand=array ("","A","B","C","D","A1","F")
		%>
		<tr>
          <td align="right"><b>Price Band:</b></td>
          <td><%=arrPriceBand(vrecarray(7,0))%>
		  <% if arrPriceBand(vrecarray(7,i))="F" then%>
		  	: <%=vrecarray(13,i)%>
		  <%end if%>
		  </td>
        </tr> <tr>
          <td align="right" valign="top"><b>Company Logo:</b></td>
          <td><img src="images/ResellerLogos/<%=vrecarray(8,0)%>"></td>
        </tr> <tr>
          <td align="right" valign="top"><b>Invoice Footer Message:</b></td>
          <td><%=replace(vrecarray(9,0),vbcrlf,"<br>")%></td>
        </tr>
		 <tr>
          <td align="right"><b>VAT Reg No:</b></td>
          <td><%=vrecarray(10,0)%></td>
        </tr>
		
		 <tr>
          <td align="right" valign="top"><b>Notes:</b></td>
          <td><%=replace(vrecarray(11,0),vbcrlf,"<br>")%></td>
        </tr>
	
	</table>
	<p align="center"><input type="button" value=" Back " onClick="history.back()"></p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>