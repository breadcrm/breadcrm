<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Response.Buffer
dim objBakery
dim recarray1, recarray2
dim ordno
dim retcol
Dim deldate
Dim intI
Dim intN
Dim intF
dim dtype
Dim strLogo
Dim strAddress
Dim strLogo1
Dim strTele

intN = 0
ordno = 0
deldate = Request.Form("deldate")
dtype=Request.Form("deltype")

if deldate = "" or dtype = "" then Response.Redirect "daily_report.asp"
set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.GetInvoiceTotal(ordno, deldate,dtype)
recarray1 = retcol("InvoiceTotal")
set retcol = Nothing

If isArray(recarray1) Then
     
	intF = ubound(recarray1, 2)
	response.Write(recarray1)
	
	
End If
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Invoice</title>
<style type="text/css">
<!--
br.page{page-break-before: always; height:1px}
table{
font-family:Verdana, Arial;
font-size: 10px;
}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" style="font-family: Verdana; font-size: 8pt"><br>
<%
dim strResellerName, strResellerAddress,strResellerTown,strResellerPostcode,strResellerTelephone,strResellerFax,strCompanyLogo,strResellerInvoiceFooterMessage,strVATRegNo
dim intJ,intRID
dim object,DisplayResellerDetail,vRecArrayReseller
if IsArray(recarray1) Then
	For intI = 0 To ubound(recarray1, 2)
	
	    
	    If isArray(recarray1) Then
             strLogo1 = recarray1(22,intI)
             strTele = recarray1(23,intI)
	                	
	        if strTele = "" Then
	            strTele = "_"
	        End If
        End If
	    
	
		for intJ=1 to recarray1(15,intI)
		intN = 0
		If recarray1(3, intI) = "Bread Factory" Then
			strLogo = "<b><font face=""Monotype Corsiva"" size=""5"">THE BREAD FACTORY</font></b>"
			strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		    strAddress = strAddress & " Light Work<br>" & vbCrLf
		   ' strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
		    strAddress = strAddress & "Gresham House, 53 Clarendon Road, Watford, Herts WD17 1LA<br>" & vbCrLf
		    strAddress = strAddress & "</p>" & vbCrLf
		    strAddress = strAddress & "<p>" & vbCrLf
		     if strLogo1 <>  "" then
		        strAddress = strAddress & "Telephone: &nbsp; " & strTele & vbCrLf
		     else   
		        strAddress = strAddress & "Telephone: 020 7041 6898<br>" & vbCrLf
				strAddress = strAddress & "Fax: XXXXXXXX<br>" & vbCrLf
		    end if
		    strAddress = strAddress & "</p></font>"
	  ElseIf recarray1(3, intI) = "Gail Force" Then
			strLogo = "<b><font face=""Kunstler Script"" size=""8"">Gail Force</font></b>"
			strAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		    strAddress = strAddress & " Light Work<br>" & vbCrLf
		    'strAddress = strAddress & "Light Work<br>Gresham House, 53 Clarendon Road<br>" & vbCrLf
		    strAddress = strAddress & "Gresham House, 53 Clarendon Road, Watford, Herts WD17 1LA<br>" & vbCrLf
		    strAddress = strAddress & "</p>" & vbCrLf
		    strAddress = strAddress & "<p>" & vbCrLf
		     strAddress = strAddress & "Telephone: &nbsp; " & strTele & vbCrLf
		    'strAddress = strAddress & "Orders: &nbsp;&nbsp;&nbsp; 020 7041 6898<br>" & vbCrLf
		    'strAddress = strAddress & "Accounts: &nbsp;020 8457 2081<br>" & vbCrLf
		    'strAddress = strAddress & "Fax: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; XXXXXXXX" & vbCrLf
		    strAddress = strAddress & "</p></font>"
		End If
intRID=recarray1(20,intI)
if intRID<>"" and intRID<>"0" then
	
	set object = Server.CreateObject("bakery.Reseller")
	object.SetEnvironment(strconnection)
	set DisplayResellerDetail= object.DisplayResellerDetail(intRID)
	vRecArrayReseller = DisplayResellerDetail("ResellerDetail")
	set DisplayResellerDetail= nothing
	set object = nothing
	strResellerName=""
	strResellerAddress=""
	strResellerTown=""
	strResellerPostcode=""
	strResellerTelephone=""
	strResellerFax=""
	strCompanyLogo=""
	strResellerInvoiceFooterMessage=""
	strVATRegNo=""
	strResellerAddress=""
	if isarray(vRecArrayReseller) then
		strResellerName=vRecArrayReseller(1,0)
		strResellerAddress1=vRecArrayReseller(2,0)
		strResellerTown=vRecArrayReseller(3,0)
		strResellerPostcode=vRecArrayReseller(4,0)
		strResellerTelephone=vRecArrayReseller(5,0)
		strResellerFax=vRecArrayReseller(6,0)
		strCompanyLogo="images/ResellerLogos/" & vRecArrayReseller(8,0)
		strResellerInvoiceFooterMessage=vRecArrayReseller(9,0)
		strVATRegNo=vRecArrayReseller(10,0)
		
		strResellerAddress = "<font face=""Verdana"" size=""1""><p>" & vbCrLf
		if strResellerName<>"" then
			strResellerAddress = strResellerAddress & "<b>" & strResellerName & "</b><br>" & vbCrLf
		end if
		if strResellerAddress1<>"" then
			strResellerAddress = strResellerAddress & strResellerAddress1 & "<br>"& vbCrLf
		end if
		if strResellerTown<>"" then
			strResellerAddress = strResellerAddress & strResellerTown & "<br>" & vbCrLf
		end if
		if strResellerPostcode<>"" then
			strResellerAddress = strResellerAddress & strResellerPostcode & "<br>" & vbCrLf
		end if
		strResellerAddress = strResellerAddress & "<BR>" & vbCrLf
		
		if strLogo1 = "" then
		
		if strResellerTelephone<>"" then
			strResellerAddress = strResellerAddress & "Telephone: " &  strResellerTelephone & "<br>" & vbCrLf
		end if
		if strResellerFax<>"" then
			strResellerAddress = strResellerAddress & "Fax:" &  strResellerFax & "<br>" & vbCrLf
		end if
		
		else
		
		  strResellerAddress = strResellerAddress & "Telephone: " &  strTele & "<br>" & vbCrLf   
		
		End if
			strResellerAddress = strResellerAddress & "</p></font>"
	end if
end if
%>
<table border="0" width="90%" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td valign="top">
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="26%">
		  <%if intRID<>"" and intRID<>"0" then%>
			<font face="Verdana" size="1">VAT Reg No. <%=strVATRegNo%></font>
			<%=strResellerAddress%>
			<%else%>
			<font face="Verdana" size="1">VAT Reg No. XXXXXX</font>
			<%=strAddress%>
			<%end if%>
		  </td>
          <td width="46%" align="center">
		  	<%if strLogo1 <> "" then%>
            <img src="images/CustomerGroupLogo/<%=strLogo1%>"><br /> 
		  	<%elseif intRID<>"" and intRID<>"0" then%>
			<img src="<%=strCompanyLogo%>"><br>
			<%else%>
			<img src="images/BakeryLogo.gif" width="225" height="77"><br>
			<%end if%>
		  </td>
          <td width="28%" align="center"><b>THIS IS AN INVOICE<br>PLEASE KEEP SAFE AND PASS TO YOUR ACCOUNTS DEPARTMENT</b></td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
      </table>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
          <td width="33%"></td>
          <td width="33%" align="center"><font size="3"><b>INVOICE # :</b> <%=recarray1(0, intI)%></font></td>
          <td width="34%"></td>
      </tr>
      <tr><td colspan="3">&nbsp;</td></tr>
      </table>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="top"><font size="2"><b>
          <%=recarray1(14, intI)%><br>
          <%=recarray1(2, intI)%><br>
          <%=recarray1(5, intI)%><br>
          <%=recarray1(6, intI)%><br>
          <%=recarray1(7, intI)%><br>
          <%=recarray1(8, intI)%>
          </b></font></td>
          <td width="33%" valign="top"><font size="2"><b>
          Purchase order No.: <%=recarray1(9, intI)%><br><br>
          van #: <%=recarray1(10, intI)%><br><br>
          Date: <%= Day(recarray1(1, intI)) & "/" & Month(recarray1(1, intI)) & "/" & Year(recarray1(1, intI)) %>
          </b></font>
          </td>
        </tr>
        <tr>
          <td valign="top">&nbsp;</td>
          <td width="33%" valign="top"><font size="2"><b>Delivery: <%=recarray1(16, intI)%></b></font></td>
        </tr>
        <tr>
          <td valign="top" colspan="2">&nbsp;</td>
        </tr>
      </table>
        <table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" align="center">
          <tr bgcolor="#CCCCCC" height="20">
            <td><b>Qty</b></td>
            <td><b>Product code</b></td>
            <td><b>Product name</b></td>
            <td align="center"><b>Unit price</b></td>
            <td width="20%" align="center"><b>Total price</b></td>
          </tr><%
			set retcol = objBakery.GetInvoiceDetails(recarray1(0, intI))
			recarray2 = retcol("InvoiceDetails")
			if isarray(recarray2) then
				for intN=0 to ubound(recarray2, 2)
					if recarray1(0, intI) = recarray2(0, intN) then%>
            <tr>
            <td><%=recarray2(1, intN)%></td><td><%=recarray2(2, intN)%></td><td><%=recarray2(3, intN)%></td>
			<%if isnull(recarray2(4, intN)) then%>
            <td align="left">0.00&nbsp;</td>
			<%else%>
            <td align="right"><%=FormatNumber(recarray2(4, intN), 2)%></td><%
            end if
            if isnull(recarray2(5, intN)) then%>
            <td width="20%" align="right">0.00&nbsp;</td><%
            else%>
            <td width="20%" align="right"><%=FormatNumber(recarray2(5, intN), 2)%></td><%
            end if%>
            </tr>
			<%
			end if
			next
			erase recarray2
			else
          %>
            <tr>
            <td colspan="5">
            <b>No Records Found...</b>
            </td>
            </tr><%
			end if
			
			set retcol = Nothing
          %>
        </table>
        <table border="0" width="90%" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td><p align="right"><b><font size="2">Goods total&nbsp;&nbsp;</font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%"><%
                  if isnull(recarray1(11,intI)) then%>
                    <p align="right"><font size="3"><b>&nbsp;0.00</b></font></p><%
                  else%>
                    <p align="right"><font size="3"><b>&nbsp;<%=FormatNumber(recarray1(11,intI), 2)%></b></font></p><%
                  end if%>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          <tr>
            <td><p align="right"><b><font size="1">VAT total&nbsp;&nbsp;</font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%"><%
                  if isnull(recarray1(12,intI)) then%>
                    <p align="right"><font size="1"><b>&nbsp;0.00</b></font></p><%
                  else%>
                    <p align="right"><font size="1"><b>&nbsp;<%=FormatNumber(recarray1(12,intI), 2)%></b></font></p><%
                  end if%>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          <tr>
            <td><p align="right"><b><font size="2">Invoice total&nbsp;&nbsp;</font></b></td>
            <td width="20%" align="center">
              <table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="100%"><%
                  if isnull(recarray1(13,intI)) then%>
                    <p align="right"><font size="3"><b>&nbsp;0.00</b></font></p><%
                  else%>
                    <p align="right"><font size="3"><b>&nbsp;<%=FormatNumber(FormatNumber(recarray1(11,intI), 2)+cdbl(FormatNumber(recarray1(12,intI), 2)), 2)%></b></font></p><%
                  end if%>
                  </td>
                </tr>
              </table>
            </td>
            </tr>
          </table>
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%">&nbsp;</td>
        </tr>
        <tr>
          <td width="100%">
            <font size="1">
            <%=GetFooterText %>
<!--            <b>IMPORTANT!!<br>
            &bull; All our products do (or may) contain gluten, sesame seeds and nuts � we are an artisan bakery using lots of different flours, nuts and seeds<br>
            &bull; Our bakery uses other allergens � ask if you need to know more info, we are happy to provide it<br>
			&bull; By placing an order with us, you accept our product specifications � ask us if you need to see them<br>
			&bull; You may find the odd olive stone or walnut shell in our products � we use natural products and can not guarantee they are all removed, so please let your customers know
-->            </font>
          </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
          <td width="100%" align="center">
          <font size="1">
            <%if intRID<>"" and intRID<>"0" then%>
			<%'=replace(strResellerInvoiceFooterMessage,"Registered office:","<br>Registered office:")%>
			Proprietor: Bread Limited incorporated in England and Wales � Company No. 06295451<br>
			Registered office: The Flour Station Ltd,  Garrick Road Industrial Estate, Gresham House, 53 Clarendon Road, London NW9 6AQ
			<%else%>
			<%=strglobalinvoicefootermessage%>
			<%end if%>
          </font>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table><% if intF > intI Then 
%>
<br class="page" /><%
	End if
	next
	Response.Flush()
	Next
Else
%>
<p>&nbsp;</p>
<table border="0" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
  <tr>
    <td width="100%">
      <p align="center"><font size="3"><b>No records found</b></font></p>
    </td>
  </tr>
</table><%
End if 
Response.Flush()
%>
<p>&nbsp;</p>
<%

set objBakery = server.CreateObject("Bakery.Reseller")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.DailyInvoiceResellerList(deldate)
recarray1 = retcol("DailyInvoiceResellerList")
set retcol=nothing
if isarray(recarray1) then
%>
<%

dim TotalVAT, TotalGoods, TotalInvoice
For intI = 0 To ubound(recarray1, 2)
set retcol = objBakery.GetResellerInvoiceDetails(recarray1(0, intI),deldate,dtype,recarray1(1, intI))
recarray2 = retcol("InvoiceDetails")
if isarray(recarray2) then
TotalVAT=0
TotalGoods=0
TotalInvoice=0
%>
<div align="center">
<center>
<table border="0" width="90%" height="100%" cellspacing="0" cellpadding="0" style="background-repeat:no-repeat">
<tr>
<td width="100%" valign="top">
<table border="0" width="90%" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%" valign="top" >
<font face="Verdana" size="1">VAT Reg No. XXXXXX</font>
<%=strAddress%>
</td>
<td width="46%" valign="top" align="center">
<img src="images/BakeryLogo.gif" width="225" height="77"><br>

</td>
<td width="28%" valign="top">
<p align="center">
<font face="Verdana" size="1">THIS IS AN INVOICE</font><br>
<font face="Verdana" size="1">PLEASE KEEP SAFE AND PASS TO YOUR ACCOUNTS DEPARTMENT</font></b><font size="1">
</font>
</td>
</tr>
<tr><td height="10"></td></tr>
</table>
<table border="0" width="90%" cellspacing="0" cellpadding="0"  align="center">
<tr>
<td width="33%"></td>
<td width="33%">
<p align="center"><font size="3" face="Verdana"><b>Reseller's INVOICE 
# :</b> <%=recarray1(1, intI)%></font></td>
<td width="34%"></td>
</tr>
<tr><td height="10"></td></tr>
</table>
<table border="0" width="90%" cellspacing="0" cellpadding="0"  align="center">
<tr>
<td valign="top"><font face="Verdana" size="2"><b>
<% if recarray1(0, intI)<>"" then%><%=recarray1(0, intI)%><br><%end if%>
<% if recarray1(1, intI)<>"" then%><%=recarray1(1, intI)%><br><%end if%>
<% if recarray1(2, intI)<>"" then%><b><%=recarray1(2, intI)%></b><br><%end if%>
<% if recarray1(3, intI)<>"" then%><%=recarray1(3, intI)%><br><%end if%>
<% if recarray1(4, intI)<>"" then%><%=recarray1(4, intI)%><br><%end if%>
<% if recarray1(5, intI)<>"" then%><%=recarray1(5, intI)%><br><%end if%>
</b></font></td>
<td width="33%" valign="top"><font face="Verdana" size="2"><b>
Date: <%= Day(recarray1(6, intI)) & "/" & Month(recarray1(6, intI)) & "/" & Year(recarray1(6, intI)) %>
</b></font></td>
</tr>
<tr><td height="10"></td></tr>
</table>

<table border="1" align="center" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: 8pt">
<tr>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Qty</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product code</b></td>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Product name</b></td>
<%if (recarray1(16, intI)<>"1") then%>
<td align="center" bgcolor="#CCCCCC" height="20"><b>Unit price</b></td>
<td width="20%" align="center" bgcolor="#CCCCCC" height="20"><b>Total price</b></td>
<%end if%>
</tr><%

if isarray(recarray2) then
for intN=0 to ubound(recarray2, 2)
%>
<tr>
<td align="center"><%=recarray2(2, intN)%>&nbsp;</td>
<td align="left"><%=recarray2(0, intN)%>&nbsp;</td>
<td align="left"><%=recarray2(1, intN)%>&nbsp;</td>
<%if isnull(recarray2(3, intN)) then%>
<td align="center">0.00</td><%
else%>
<td align="right"><%=FormatNumber(recarray2(3, intN), 2)%>&nbsp;</td><%
end if
if isnull(recarray2(4, intN)) then%>
<td width="20%" align="center">0.00</td><%
else%>
<td width="20%" align="center"><%=FormatNumber(recarray2(4, intN), 2)%>&nbsp;</td><%
end if%>
</tr>
<%
if not isnull(recarray2(4, intN)) then
	TotalGoods=TotalGoods+cdbl(recarray2(4, intN))	
else
	TotalGoods=TotalGoods
end if
if not isnull(recarray2(5, intN)) then
	TotalVAT=TotalVAT+cdbl(recarray2(5, intN))	
else
	TotalVAT=TotalVAT
end if
TotalInvoice=TotalVAT+ TotalGoods

next
erase recarray2
end if
set retcol = Nothing %>
</table>
<table border="0" width="90%" cellspacing="0" cellpadding="0" align="center">
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">Goods total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%"><p align="right"><font size="3" face="Verdana"><b><%=FormatNumber(TotalGoods, 2)%></b></font></p></td>
</tr>
</table>

</td>
</tr>
<tr>
<td>
<p align="right"><b><font size="1" face="Verdana">VAT total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%"><p align="right"><font size="1" face="Verdana"><b>&nbsp;<%=FormatNumber(TotalVAT, 2)%></b></font></p></td>
</tr>
</table>

</td>
</tr>
<tr>
<td>
<p align="right"><b><font size="2" face="Verdana">Invoice total&nbsp;&nbsp; </font></b></td>
<td width="20%" align="center">
<table border="1" width="100%" bordercolor="#000000" cellspacing="0" cellpadding="2">
<tr>
<td width="100%"><p align="right"><font size="3" face="Verdana"><b>&nbsp;<%=FormatNumber(TotalInvoice, 2)%></b></font></p>
</td>
</tr>
</table>
</td>
</tr>
</table>

<table border="0" width="90%" cellspacing="0" cellpadding="0"  align="center">
<tr>
	<td width="100%" height="10"></td>
</tr>
<tr>
<td width="100%">
<font face="Verdana" size="1">
<b>IMPORTANT!!</b><br>
Shortages must be reported on day of delivery. To ensure next day delivery, ring before 14:00 Mon - Fri
</font>
</td>
</tr>
<tr><td height="10"></td></tr>
<tr>
<td width="100%" align="center"><font face="Verdana" size="1"><%=strglobalinvoicefootermessage%></font></td>
</tr>
<tr><td height="10"></td></tr>
</table>

</td>
</tr>
</table>
</center>
</div>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%
end if
Next
end if
Response.Flush()
%>
<%
set objBakery = Nothing
%>
</body>
</html><%
If IsArray(recarray1) Then erase recarray1
If IsArray(recarray2) Then erase recarray2
%>


<%

Function GetFooterText()

Dim arFooter,objFooter,obj,hbody
Set obj = CreateObject("Bakery.General")
obj.SetEnvironment(strconnection)
set objFooter = obj.GetFooterText()
arFooter = objFooter("FooterMessage")

If IsArray(arFooter) Then

    hbody = Replace(arFooter(0,0), "^^^", "'") 

    GetFooterText = hbody
Else

   GetFooterText = Empty

End if

End Function

 %>