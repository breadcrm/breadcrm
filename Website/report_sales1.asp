<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>
<%
dim objBakery, retcol,  i
dim recarray1, totpages
dim fno, pno, gno, cno, tno, dno, fromdt, todt, ptype
dim a1, vsessionpage, tot1, tot2, j

vsessionpage="1"
totpages="1"
fno = trim(Request.QueryString("fno"))
pno = trim(Request.QueryString("pno"))
gno = trim(Request.QueryString("cgno"))
cno = trim(Request.QueryString("cno"))
tno = trim(Request.QueryString("dtime"))

if pno <> "" and pno <> "-1" and instr(1,pno,"-1") > 0 then pno = replace(pno,"-1","")
if gno <> "" and gno <> "-1" and instr(1,gno,"-1") > 0 then gno = replace(gno,"-1","")
if cno <> "" and cno <> "-1" and instr(1,cno,"-1") > 0 then cno = replace(cno,"-1","")
if dno <> "" and dno <> "-1" and instr(1,dno,"-1") > 0 then dno = replace(dno,"-1","")

if tno = "-1"  then tno = 0
if fno = "" then fno = "-1"
if pno = "" then pno = "-1"
if gno = "" then gno = "-1"
if cno = "" then cno = "-1"
ptype=trim(Request.QueryString("ptype"))
if ptype <> "A" and ptype <> "N" and ptype <> "S" and ptype <> "C" then ptype=""
dno = trim(Request.QueryString("pgno"))
if dno = "" then dno = "-1"
fromdt = trim(Request.QueryString("fromdt"))
todt = trim(Request.QueryString("todt"))

if mid(pno,1,1) = "," then pno = mid(pno,2)
if mid(gno,1,1) = "," then gno = mid(gno,2)
if mid(cno,1,1) = "," then cno = mid(cno,2)
if mid(dno,1,1) = "," then dno = mid(dno,2)

'Response.Write "fno = " & fno & "<br>"
'Response.Write "pno = " & pno & "<br>"
'Response.Write "pgno = " & dno & "<br>"
'Response.Write "cno = " & cno & "<br>"
'Response.Write "cgno = " & gno & "<br>"

if instr(1,fno,"~") > 0 then
  a1=split(fno,"~")
  fno = a1(0)
end if

if instr(1,tno,"~") > 0 then
  a1=split(tno,"~")
  tno = a1(0)
end if

'1, 3-20-007, 5-20-011
if instr(1,pno,",") > 0 then
  a1=split(pno,",")
  pno=""
  for i=0 to ubound(a1)
    pno = pno & "'" & a1(i) & "',"
  next
  if pno <> "" then
    pno = mid(pno,1,len(pno)-1)
  end if
end if

if tno=0 then tno=-1
'Response.Write "fno = " & fno & "<br>"
'Response.Write "pno = " & pno & "<br>"
'Response.Write "cno = " & cno & "<br>"
'Response.Write "dno = " & dno & "<br>"
'Response.Write "gno = " & gno & "<br>"
'Response.Write "tno = " & tno & "<br>"
'Response.Write "fromdt = " & fromdt & "<br>"
'Response.Write "todt = " & todt & "<br>"
'Response.Write "ptype = " & ptype & "<br>"

set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.SalesReport(fno,fromdt,todt,pno,cno,dno,gno,tno,replace(ptype,"A",""))
recarray1 = retcol("SalesReport")

%>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="75%"><b><font size="3">Report - Sales</font></b>
    </td>
    <td width="25%"><b><font size="2">Page <%=vsessionpage%> of <%=totpages%></font></b></td>
  </tr>
</table>
  </center>
</div>
<br>
<div align="center">
  <center>
      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td bgcolor="#CCCCCC" rowspan="2"><b>Facility</b></td>
          <td bgcolor="#CCCCCC" rowspan="2">&nbsp;</td>
          <td bgcolor="#CCCCCC" colspan="2" align="center"><b>Date &lt;<%=fromdt%>&gt;
            - &lt;<%=todt%>&gt;</b></td>
        </tr>
        <tr>
          <td bgcolor="#CCCCCC" align="center"><b>No. of Products</b></td>
          <td bgcolor="#CCCCCC" align="center"><b>Turnover</b></td>
        </tr><%
        if isarray(recarray1) then
          'Response.Write "total = " & ubound(recarray1,2) & "<br>"
          on error resume next
          for i=0 to ubound(recarray1,2)%>
          <tr>
          <td><b><%=recarray1(2,i)%></b></td>
          <td></td>
          <td align="center"></td>
          <td align="center"></td>
          </tr>
          <tr>
          <td>&nbsp;- <%=recarray1(1,i)%></td>
          <td></td>
          <td align="center"><%=recarray1(3,i)%></td>
          <td align="center"><%=formatnumber(recarray1(4,i),2)%></td>
          </tr>
          <tr>
          <%if i+1 <= ubound(recarray1,2) then
          if recarray1(0,i+1)=2 then%>
          <td>&nbsp;- <%=recarray1(1,i+1)%></td>
          <td></td>
          <td align="center"><%=recarray1(3,i+1)%></td>
          <td align="center"><%=formatnumber(recarray1(4,i+1),2)%></td>
          </tr><%
          end if
          end if
          if i+2 <= ubound(recarray1,2) then
          if recarray1(0,i+2)=3 then%>
          <tr>
          <td>&nbsp;- <%=recarray1(1,i+2)%></td>
          <td></td>
          <td align="center"><%=recarray1(3,i+2)%></td>
          <td align="center"><%=recarray1(4,i+2)%></td>
          </tr><%
          end if
          end if%>
          <tr>
          <td><b>&nbsp;- Total Production</b></td>
          <td></td><%
          tot1=recarray1(3,i)
          'Response.Write "tot1=" & tot1 & ","
          j=i
          if recarray1(0,j+1)=2 then
            tot1 = clng(tot1) + recarray1(3,j+1)
            i=i+1
          end if
          'Response.Write "tot1=" & tot1 & ","
          if recarray1(0,j+2)=3 then
            tot1 = clng(tot1) + recarray1(3,j+2)
            i=i+1
          end if
          'Response.Write "tot1=" & tot1 & ","
          tot2=recarray1(4,j)
          if recarray1(0,j+1)=2 then
            tot2 = cdbl(tot2) + recarray1(4,j+1)
          end if
          if recarray1(0,j+2)=3 then
            tot2 = cdbl(tot4) + recarray1(4,j+2)
          end if%>
          <td align="center"><b><%=tot1%></b></td>
          <td align="center"><b><%=formatnumber(tot2,2)%></b>&nbsp;</td>
          </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr><%
        next
      else%>
        <tr>
          <td colspan="4">
            <b>No records found...</b>
           </td>
         </tr><%
      end if%>
      </table>
  </center>
</div>
</body>
</html>
