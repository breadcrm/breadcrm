<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%
Dim Obj,ObjOrder,objResult
Dim strDirection
Dim vCustNo
Dim i
Dim strProductStream
Dim arSplit
Dim arUpdate
Dim strPo
Dim vOrderNo
Dim vNewSampleOrdTag
Dim arTypicalOrder 
Dim	arTypicalOrderDetails 
Dim arCustomer
Dim arTrip
Dim strPoTag
Dim vLastOrdeDate
Dim strErrMsg
Dim strDate
Dim strOrderType
Dim strSampleOrder
Dim strErrTag
dim strBgColour
dim straction,strnotapplydeliverycharge

stop
strnotapplydeliverycharge = Request.Form("notapplydeliverycharge")
if Trim(Request.Form("OrderNo")) <> "" Then
	vOrderNo = Request.Form("OrderNo")	
Else
	vOrderNo = Request.QueryString("OrderNo")
End if	

strOrderType = Request.Form("OrderType")

Set Obj = CreateObject("Bakery.Orders")
Obj.SetEnvironment(strconnection)
if Trim(vOrderNo) <> "" and Trim(vOrderNo) <> "0" Then 'Order Already created
	if Trim(Request.Form("Update")) <> "" Then
		strProductStream = Request.Form("ProductStream")	
		arSplit = Split(strProductStream,",")	
		strPo = Request.Form("txtPo")
		strProductsAmended=""	
		if IsArray(arSplit) Then
			Redim arUpdate(1,UBound(arSplit)) 
			For i=0 To UBound(arSplit)
				If IsNumeric(Request.Form("qty" & arSplit(i))) Then
					arUpdate(0,i) = arSplit(i)
					arUpdate(1,i) = Request.Form("qty" & arSplit(i))
					
					if (strProductsAmended="") then
						strProductsAmended="<br>" & strProductsAmended & arSplit(i) & " (qty: " &  Request.Form("qty" & arSplit(i)) & ")"
					else
						strProductsAmended=strProductsAmended & "<br>" & arSplit(i) & " (qty: " &  Request.Form("qty" & arSplit(i)) & ")"
					end if								
				Else
					arUpdate(0,i) = arSplit(i)
					arUpdate(1,i) = 0 ' If numeric in not entred
					strErrTag = true
				End if			
			Next
			
			if strErrTag <> true Then
			
				if (strnotapplydeliverycharge<>1) then
					strnotapplydeliverycharge=0
				end if
			
				objResult = obj.Update_TypicalOrderDetails(vOrderNo,arUpdate,strPo,strnotapplydeliverycharge)
				
				objResult3=obj.SaveTempOrderMasterDeliveryCharge(vOrderNo,strnotapplydeliverycharge)
				Dim cusNo
				
				Del_Date = Request.Form("HF_DelDate")
				
				cusNo = obj.GetCustomerIdByOrderId(vOrderNo)
				
				if (Del_Date<>"") then
				    straction=straction & "Delivery Date: " & Del_Date 
				end if
				
				if (strProductsAmended<>"") then
				    straction=straction & "<br>Product Added:" & strProductsAmended
				end if
					
				if session("processed") = "yes"	then
					LogAction "New Order after Processing", straction , cusNo	
				else
					LogAction "New Sample Order", straction , cusNo
				end if	
				
				obj.Log_OrderAmend vOrderNo,session("UserNo"), Del_Date
				
			End if	
			
			if Trim(objResult) = "OK" Then
				Response.Redirect "order_Samplefinish.asp?OrderNo=" & vOrderNo
			End if 
						
		End if
	End if 
	
	Set ObjOrder = obj.Display_TypicalOrderDetails(vOrderNo)
	arTypicalOrder = ObjOrder("TypicalOrder")
	arTypicalOrderDetails = ObjOrder("TypicalOrderDetails")
	vNewSampleOrdTag = "False"
Else
	vCustNo = Request.Form("CustNo") 
	vNewSampleOrdTag = "True"
			
	if trim(Request.QueryString("ErrTag")) = "Yes" Then
		vCustNo = Request.QueryString("CustNo") 	
		strDate = Request.QueryString("txtDate") 	
		strOrderType = Request.QueryString("OrderType")		
		strSampleOrder = Request.QueryString("radSample") 
		
		if session("processed") = "yes" Then
			strErrMsg = "Entered date should be a processed date"
		Else
			strErrMsg = "Entered date should be an unprocessed date"
		End if		
		
		if Request.QueryString("ExistOrdTag") = "Yes" Then
			if Request.QueryString("radSample") = "Y" Then
				strErrMsg = "This customer has an existing sample order, please add products via Amend Order option!"
			Else
				strErrMsg = "This customer has an existing order, please add products via Amend Order option!"
			End if 				
		End if
		
	End if	
	
	Set ObjOrder = obj.Display_SampleOrderDetails(vCustNo)
	arCustomer = ObjOrder("Customer")	
	arTrip= ObjOrder("Trip")
	
End if
if strDate="" then
	if (session("processed") = "yes") then
		strDate=Day(date()) & "/" & Month(date()) & "/" & Year(date()) 
	else
		strDate=Day(date()+1) & "/" & Month(date()+1) & "/" & Year(date()+1)
	end if	
end if
'Getting Last order date
vLastOrdeDate = obj.Display_LastOrderDate()

Set Obj = Nothing
Set ObjOrder = Nothing

if IsArray(arTypicalOrder) Then
	strPoTag = arTypicalOrder(18,0) 
End if

strProductStream = ""
if IsArray(arTempOrderMasterDeliveryCharge) and strnotapplydeliverycharge="" Then
	strnotapplydeliverycharge = arTempOrderMasterDeliveryCharge(0,0) 
	
	if (strnotapplydeliverycharge) then
		strTempOrderMasterDeliveryChargeChecked="Checked"
	else
		strTempOrderMasterDeliveryChargeChecked=""
	end if
	
End if
%>


<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT LANGUAGE=javascript>
<!--
function initValueCal(theForm,theForm2){
	
	if(document.frmOrder.txtDate != null)
	{
	
	y=theForm.txtDate.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm2.value=t.getDate() +'/'+eval((t.getMonth())+1) +'/'+ t.getFullYear();
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
	}	
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
	del.value=d+'/'+ (eval(m)+1)+'/'+ y;
	}
function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
//-->
</SCRIPT>
<script LANGUAGE="javascript" src="includes/Script.js"></script>
<SCRIPT LANGUAGE="Javascript">
<!--
var strPoTag = '<%=strPoTag%>'
function validation(){		
	if(strPoTag=='Y'){
		if(document.frmOrder.txtPo.value == ''){
			alert("Please enter a Purchase order number")		
			return;
		}		
	}	
	
	//if(!(confirm("Are you sure the quantities entered are numeric values?"))){	
	//	return;	
	//}
	

	if(confirm("Would you like to confirm this order?")){ 
		document.frmOrder.submit();} 	
}


function AddOrdervalidation(){	
var strProcess = '<%=session("processed")%>'
var strLastDate = '<%=vLastOrdeDate%>'
var EnterDate

	if("<%=vNewSampleOrdTag%>" == "True"){
		if(CheckEmpty(document.frmOrder.txtDate.value)=="true"){
			alert("Please enter a Date");
			document.frmOrder.txtDate.focus();	
			return;	
		}
		
/*		if(strLastDate !=''){
			var vindex = 0;vLast=0				
			var l_Day,l_Month,l_Year,e_Day,e_Month,e_Year
			
			//Last Order Date
			vindex = strLastDate.indexOf('/',0);								
			l_Day = Number(strLastDate.substr(0,vindex));			
			vLast=vindex				
			vindex = strLastDate.indexOf('/',vLast+1);			
			l_Month = Number(strLastDate.substr(vLast+1,vindex-(vLast+1)));						
			l_Year = Number(strLastDate.substr(vindex+1));	 
						
			// Enterd Date
			EnterDate = document.frmOrder.txtDate.value;
			vindex = EnterDate.indexOf('/',0);								
			e_Day = Number(EnterDate.substr(0,vindex));			
			vLast=vindex				
			vindex = EnterDate.indexOf('/',vLast+1);			
			e_Month = Number(EnterDate.substr(vLast+1,vindex-(vLast+1)));						
			e_Year = Number(EnterDate.substr(vindex+1));	 								
			
			if(strProcess=='yes'){				
				if(DateRangCheck(l_Day,l_Month,l_Year,e_Day,e_Month,e_Year) == true){
					alert("Entered data should be less  than or equal to last order date");
					return;
				}
			}					
			else{
				if(DateRangCheck(l_Day,l_Month,l_Year,e_Day,e_Month,e_Year)!=true){
					alert("Entered data should be grater than last order date");
					return;
				}
			}
		}	
		else{
			alert("You can not create a new order, Please report to the administrator")	
		}		*/
		
		document.frmOrder.action = "Order_SampleAddProduct.asp" ;	
		document.frmOrder.submit(); 
	}
	else{
		document.frmproduct.submit(); 	
	}
	
	//	if(confirm("Would you like to confirm this order?")){ 
		//	document.frmOrder.submit();} 	
}

	
//-->
</SCRIPT>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal(document.frmOrder,document.frmOrder.txtDate)">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
  <form method="POST" action="order_sample1.asp" name="frmOrder">
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt"><%	
	if strErrTag = true Then%>
		<tr>    
		  <td width="100%">
				<font color="#FF0000" size=3><b>Invalid quantities, Please check</b></font>  
			</td>	
		</tr><%	
	End if%>
  <tr>
    
    <td width="100%"><b><font size="3"><%
    if vNewSampleOrdTag = "True" Then
			Response.Write "Create New/Sample order<br>"
    Else
			Response.Write "Amend New/Sample order<br>"
    End if%>
      &nbsp;</font></b><br>
   <font color="#FF0000" size=3><b><%=strErrMsg%></b></font>  
      
      
      
      <%
    if vNewSampleOrdTag = "False" Then' ORder is already created 
			if IsArray(arTypicalOrder) Then%>      
			  <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="400">
			    <tr>
			      <td><b>Delivery Date&nbsp;</b></td>
			      <td><%
							if IsDate(arTypicalOrder(1,0)) Then
								Response.Write Day(arTypicalOrder(1,0)) & "/" & Month(arTypicalOrder(1,0)) & "/" & Year(arTypicalOrder(1,0))						 							 	
							End if%>       
			      
			      </td>
			    </tr>
			    <tr>
			      <td><b>Day&nbsp;</b></td>
			      <td><%=arTypicalOrder(2,0)%></td>
			    </tr>
			    <tr>
			      <td>Delivery time</td>
			      <td><%=arTypicalOrder(3,0)%></td>
			    </tr>
			    <!--<tr>
			      <td>Sample order</td>
			      <td>Yes <input type="radio" value="V1" name="R1"> | No <input type="radio" value="V2" checked name="R1"></td>
			    </tr>-->
			    
			    <tr>
			      <td>Sample order</td>
			      <td><%=arTypicalOrder(4,0)%></td>
			    </tr>
			    
			    
			    <tr>
			      <td><b>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b></td>
			      <td><%=arTypicalOrder(5,0)%></td>
			    </tr>
			    <tr>
			      <td>Code</td>
			      <td><%=arTypicalOrder(6,0)%></td>
			    </tr>
			    <tr>
			      <td>Address</td>
			      <td><%=arTypicalOrder(7,0)%></td>
			    </tr>
			    <tr>
			      <td></td>
			      <td><%=arTypicalOrder(8,0)%></td>
			    </tr>
			    <tr>
			      <td>Town</td>
			      <td><%=arTypicalOrder(9,0)%></td>
			    </tr>
			    <tr>
			      <td>Postcode</td>
			      <td><%=arTypicalOrder(10,0)%></td>
			    </tr>
			    <tr>
			      <td>Telephone</td>
			      <td><%=arTypicalOrder(11,0)%></td>
			    </tr>
			    
			    <tr>
			      <td>Contact Name</td>
			      <td><%=arTypicalOrder(12,0)%></td>
			    </tr>
			    <tr>
			      <td>Contact Phone</td>
			      <td><%=arTypicalOrder(13,0)%></td>
			    </tr>
			    <tr>
			      <td>Name</td>
			      <td><%=arTypicalOrder(14,0)%></td>
			    </tr>
			    <tr>
			      <td>Phone</td>
			      <td><%=arTypicalOrder(15,0)%></td>
			    </tr>
			    <tr>
			      <td></td>
			      <td></td>
			    </tr>
			    <tr>
			      <td>Amend Order for Date</td>
			      <td><%
							if IsDate(arTypicalOrder(16,0)) Then
								Response.Write Day(arTypicalOrder(16,0)) & "/" & Month(arTypicalOrder(16,0)) & "/" & Year(arTypicalOrder(16,0))						 							 	
							End if%>       
			      </td>
			    </tr>
			    
			    <tr>
			      <td>Purchase Order</td>
			      <td>
                  <INPUT type="text" id="txtPo" name="txtPo" value="<%=arTypicalOrder(17,0)%>" maxlength="50" size="20">
			      </td>
			    </tr>
			    
			    
				
				<tr>
          <td><nobr>Do not apply a delivery charge to this order</nobr></td>
          <td>
		  <% if request("confirm")="No" then%>
		  <INPUT type="hidden" id="notapplydeliverycharge" name="notapplydeliverycharge" value="<%=strnotapplydeliverycharge%>">
		  <%
		  if (strnotapplydeliverycharge="1") then
		  	response.Write("Yes")
		  else
		  	response.Write("No")
		  end if
		  %>
		  <%else%>
		  <input type="checkbox" name="notapplydeliverycharge" id="notapplydeliverycharge" style="font-family: Verdana; font-size: 8pt" value="1" <%=strTempOrderMasterDeliveryChargeChecked%>>
		  <% end if%>
		  </td>
        </tr>
		<tr>
			      <td></td>
			      <td>&nbsp;</td>
			    </tr>
			  </table><%			  
			End if   
		Else'If it is New order
			if IsArray(arCustomer) Then%>
				<table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="600">
			    <tr>
			      <td width="158"><b>Delivery Date&nbsp;</b></td>
			      <TD width="375">
	    <%
		dim arrayMonth
		arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
	    %>
		<select name="day" onChange="setCal(this.form,document.frmOrder.txtDate)">
		  <%for i=1 to 31%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
		<select name="month" onChange="setCal(this.form,document.frmOrder.txtDate)">
		   <%for i=0 to 11%>
		  	<option value="<%=i%>"><%=arrayMonth(i)%></option>
		  <%next%>
		</select>
		<select name="year" onChange="setCal(this.form,document.frmOrder.txtDate)">
		  <%for i=2000 to Year(Date)+1%>
		  	<option value="<%=i%>"><%=i%></option>
		  <%next%>
		</select>
	  <INPUT class=Datetxt id=deldate  name="txtDate" value="<%=strDate%>" onFocus="blur();" onChange="setCalCombo(this.form,document.frmOrder.txtDate)">
      <IMG id=f_trigger_fr onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtDate",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_fr",
				singleClick    :    true
			});
	</SCRIPT>

	  </TD>
     
				 
			    </tr>
			    <tr>
			      <td>Delivery time</td>
			      <td>
							<SELECT id="selTrip" name="selTrip"><%
								if IsArray(arTrip) Then
									For i = 0 to UBound(arTrip,2)%>								
										<OPTION value="<%=arTrip(0,i)%>"><%=arTrip(1,i)%></OPTION><%
									Next
								End if%>									
							</SELECT>
			      </td>
			    </tr>		
			    <tr>
			      <td>Sample order</td>
			      <td>Yes <input type="radio" value="Y" name="radSample" <%if strOrderType = "S" OR strSampleOrder = "Y" Then  Response.Write "checked"%>><%
							if strOrderType <> "S" Then%>
							| No
                  <input type="radio" value="N" name="radSample" <%if strSampleOrder = "N" OR strSampleOrder = "" Then  Response.Write "checked" %>><%
              End if%>    
            </td>
			    </tr>
			    <tr>
			      <td><b>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b></td>
			      <td><%=arCustomer(0,0)%></td>
			    </tr>
			    <tr>
			      <td>Code</td>
			      <td><%=vCustNo%></td>
			    </tr>
			    <!--
			    <tr>
			      <td>Code</td>
			      <td><%'=arCustomer(1,0)%></td>
			    </tr>
			    -->
			    <tr>
			      <td>Address</td>
			      <td><%=arCustomer(2,0)%></td>
			    </tr>
			    <tr>
			      <td></td>
			      <td><%=arCustomer(3,0)%></td>
			    </tr>
			    <tr>
			      <td>Town</td>
			      <td><%=arCustomer(4,0)%></td>
			    </tr>
			    <tr>
			      <td>Postcode</td>
			      <td><%=arCustomer(5,0)%></td>
			    </tr>
			    <tr>
			      <td>Telephone</td>
			      <td><%=arCustomer(6,0)%></td>
			    </tr>
			    <tr>
			      <td colspan="2"><b>Key Contact</b><p><b>Day to Day contact</b></td>
			    </tr>
			    <tr>
			      <td>Name</td>
			      <td><%=arCustomer(7,0)%></td>
			    </tr>
			    <tr>
			      <td>Phone</td>
			      <td><%=arCustomer(8,0)%></td>
			    </tr>
			    <tr>
			      <td><b>Decision Maker</b></td>
			      <td><b></b></td>
			    </tr>
			    <tr>
			      <td>Name</td>
			      <td><%=arCustomer(9,0)%></td>
			    </tr>
			    <tr>
			      <td>Phone</td>
			      <td><%=arCustomer(10,0)%></td>
			    </tr>

			  </table><%
			End if
		End if%>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="33%" bgcolor="#CCCCCC"><b>Product Code</b></td>
          <td width="33%" bgcolor="#CCCCCC"><b>Product Name</b></td>          
          <td width="34%" bgcolor="#CCCCCC"><b>New Qty</b></td>
        </tr><%
        
        if IsArray(arTypicalOrderDetails) Then
					for i = 0 to UBound(arTypicalOrderDetails,2)
					
						if (i mod 2 =0) then
							strBgColour="#FFFFFF"
						else
							strBgColour="#F3F3F3"
						end if						%>
						<tr bgcolor="<%=strBgColour%>">
							<td width="33%"><%=arTypicalOrderDetails(0,i)%></td>
							<td width="33%"><%=arTypicalOrderDetails(1,i)%></td>							
							<td width="34%"><input type="text" name="qty<%=arTypicalOrderDetails(0,i)%>" size="8" value="<%=arTypicalOrderDetails(3,i)%>" style="font-family: Verdana; font-size: 8pt"></td>
						</tr><%
						strProductStream = strProductStream & arTypicalOrderDetails(0,i) & "," 
					Next         
					
					If Trim(strProductStream) <> "" Then
						strProductStream = Left(strProductStream,len(strProductStream)-1)
					End if										
        End if%>
        
        
        <tr>
          <td width="133%" colspan="4">
            <table border="0" width="100%">
              <tr>
           
                <td width="33%" align="center"><input type="button" onClick="AddOrdervalidation();" value="Add Product" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
          
          
                <td width="33%" align="center"><input type="button" onClick="Javascript:document.back.submit();" value="Go Back" name="Go Back" style="font-family: Verdana; font-size: 8pt"></td><%       
                If vNewSampleOrdTag <> "True" Then%>
									<td width="34%" align="center"><input type="button" onClick="validation()" value="Confirm Order" name="B1" style="font-family: Verdana; font-size: 8pt"></td><%
								End if%>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>   
		
		<Input type="hidden" name="ProductStream" value="<%=strProductStream%>">
		<Input type="hidden" name="OrderNo" value="<%=vOrderNo%>">		
		<Input type="hidden" name="Update" value="True">
		<Input type="hidden" name="NewSampleOrdTag" value="<%=vNewSampleOrdTag%>">       
		<Input type="hidden" name="CustNo" value="<%=vCustNo%>">      
		<Input type="hidden" name="OrderType" value="<%=strOrderType%>">       
		<INPUT type="hidden" id="HF_DelDate" name="HF_DelDate" value="<%=Day(arTypicalOrder(1,0)) & "/" & Month(arTypicalOrder(1,0)) & "/" & Year(arTypicalOrder(1,0))%>"> 
   
</table>
</form>


  </center>
</div>
     <form method="POST" action="order_Sampleaddproduct.asp" name="frmproduct">
       <Input type="hidden" name="OrderNo" value="<%=vOrderNo%>">       
       <Input type="hidden" name="NewSampleOrdTag" value="<%=vNewSampleOrdTag%>">       
       
			</form>
                <form method="POST" action="order_sample.asp" name="back">
             
                </form>

</body>

</html><%
if IsArray(arTypicalOrder) Then Erase arTypicalOrder  
if IsArray(arTypicalOrderDetails) Then Erase arTypicalOrderDetails 
if IsArray(arCustomer) Then Erase arCustomer 

%>