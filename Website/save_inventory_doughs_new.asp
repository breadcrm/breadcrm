<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
dim vDoughMixingMethod
vDName= request.form("doughname")

vIno1= request.form("I1")
vQty1= request.form("QTY1")
vsQty1= request.form("SQTY1")

vIno2= request.form("I2")
vQty2= request.form("QTY2")
vsQty2= request.form("SQTY2")

vIno3= request.form("I3")
vQty3= request.form("QTY3")
vsQty3= request.form("SQTY3")

vIno4= request.form("I4")
vQty4= request.form("QTY4")
vsQty4= request.form("SQTY4")

vIno5= request.form("I5")
vQty5= request.form("QTY5")
vsQty5= request.form("SQTY5")

vIno6= request.form("I6")
vQty6= request.form("QTY6")
vsQty6= request.form("SQTY6")

vIno7= request.form("I7")
vQty7= request.form("QTY7")
vsQty7= request.form("SQTY7")

vIno8= request.form("I8")
vQty8= request.form("QTY8")
vsQty8= request.form("SQTY8")

vIno9= request.form("I9")
vQty9= request.form("QTY9")
vsQty9= request.form("SQTY9")

vIno10= request.form("I10")
vQty10= request.form("QTY10")
vsQty10= request.form("SQTY10")

vIno11= request.form("I11")
vQty11= request.form("QTY11")
vsQty11= request.form("SQTY11")

vIno12= request.form("I12")
vQty12= request.form("QTY12")
vsQty12= request.form("SQTY12")

vIno13= request.form("I13")
vQty13= request.form("QTY13")
vsQty13= request.form("SQTY13")

vIno14= request.form("I14")
vQty14= request.form("QTY14")
vsQty14= request.form("SQTY14")

vIno15= request.form("I15")
vQty15= request.form("QTY15")
vsQty15= request.form("SQTY15")

if not isnumeric(vQty1) or vQty1 = "" or vIno1="" then
	response.redirect "inventory_doughs_new.asp"
end if

if not isnumeric(vQty2) then
	vIno2= 0
	vQty2 = 0
	vsQty2 = 0
end if
if not isnumeric(vQty3) then
	vIno3= 0
	vQty3 = 0
	vsQty3 = 0
end if
if not isnumeric(vQty4) then
	vIno4= 0
	vQty4 = 0
	vsQty4 = 0
end if
if not isnumeric(vQty5) then
	vIno5= 0
	vQty5 = 0
	vsQty5 = 0
end if
if not isnumeric(vQty6) then
	vIno6= 0
	vQty6 = 0
	vsQty6 = 0
end if
if not isnumeric(vQty7) then
	vIno7= 0
	vQty7 = 0
	vsQty7 = 0
end if
if not isnumeric(vQty8) then
	vIno8= 0
	vQty8 = 0
	vsQty8 = 0
end if
if not isnumeric(vQty9) then
	vIno9= 0
	vQty9 = 0
	vsQty9 = 0
end if
if not isnumeric(vQty10) then
	vIno10= 0
	vQty10 = 0
	vsQty10 = 0
end if
if not isnumeric(vQty11) then
	vIno11= 0
	vQty11 = 0
	vsQty11 = 0
end if
if not isnumeric(vQty12) then
	vIno12= 0
	vQty12 = 0
	vsQty12 = 0
end if
if not isnumeric(vQty13) then
	vIno13= 0
	vQty13 = 0
	vsQty13 = 0
end if
if not isnumeric(vQty14) then
	vIno14= 0
	vQty14 = 0
	vsQty14 = 0
end if
if not isnumeric(vQty15) then
	vIno15= 0
	vQty15 = 0
	vsQty15 = 0
end if
stop
set object = Server.CreateObject("bakery.Inventory")
object.SetEnvironment(strconnection)
vDoughType= request.form("DoughType")
if (vDoughType="") then
	vDoughType="0"
end if

vMarginofError = request.form("marginoferror")
if vMarginofError = "" then
	vMarginofError = "0"
end if

vSecondMixCategory= request.form("SecondMixCategory")
if (vSecondMixCategory="") then
	vSecondMixCategory="0"
end if

vMainDough= request.form("MainDough")
if (vMainDough="") then
	vMainDough="0"
end if

vSecondMixCategoryWeight= request.form("SecondMixCategoryWeight")
if (vSecondMixCategoryWeight="") then
	vSecondMixCategoryWeight="0"
end if

if (vSecondMixCategory="0") then
	vMainDough="0"
	vSecondMixCategoryWeight="0"
end if

if vSecondMixCategory = "0" then
	vDoughMixingMethod = "1"
elseif (vSecondMixCategory = "1") then
	vDoughMixingMethod = "2"
elseif (vSecondMixCategory = "2") then
	vDoughMixingMethod = "3"
end if

'Lets save the main dough and second mix category as before but with additional dough mixing method.
if (vDoughMixingMethod = "1" or vDoughMixingMethod = "2") then
set SaveDoughType = object.SaveDough(vDName, vIno1, vQty1, vIno2, vQty2, vIno3, vQty3, vIno4, vQty4, vIno5, vQty5, vIno6, vQty6, vIno7, vQty7, vIno8, vQty8, vIno9, vQty9, vIno10, vQty10, vIno11, vQty11, vIno12, vQty12, vIno13, vQty13, vIno14, vQty14, vIno15, vQty15,vDoughType,vSecondMixCategory,vMainDough,vSecondMixCategoryWeight,vDoughMixingMethod,vMarginofError)

	if SaveDoughType("Sucess") <> "OK" then
		set SaveDoughType= nothing
		set object = nothing
		response.redirect "inventory_doughs_new.asp?status=error"
	else
		set SaveDoughType= nothing
		set object = nothing
		response.redirect "inventory_doughs_new.asp?status=ok&doughname="&Server.URLEncode(vDName)
	end if
end if

if vDoughMixingMethod = "3" then
	set SaveParentDough = object.SaveParentDough(vDName, vDoughType, "0", vMainDough, vSecondMixCategoryWeight, vDoughMixingMethod, vMarginofError)
	
	if SaveParentDough("Sucess") = "OK" then
		vParentDoughNo = SaveParentDough("DNo")
	else
		vParentDoughNo = "0"
	end if
	
	if (vParentDoughNo <> "0") then
		'set SaveFirstChildDough = object.SaveChildDough(vDName & " - first mix", vIno1, vQty1, vIno2, vQty2, vIno3, vQty3, vIno4, vQty4, vIno5, vQty5, vIno6, vQty6, vIno7, vQty7, vIno8, vQty8, vIno9, vQty9, vIno10, vQty10, vIno11, vQty11, vIno12, vQty12, vIno13, vQty13, vIno14, vQty14, vIno15, vQty15,vDoughType,"0",vParentDoughNo,vSecondMixCategoryWeight,"4",vMarginofError)
		set SaveFirstChildDough = object.SaveTwoStageMixingIngredients(vParentDoughNo, vIno1, vQty1, vIno2, vQty2, vIno3, vQty3, vIno4, vQty4, vIno5, vQty5, vIno6, vQty6, vIno7, vQty7, vIno8, vQty8, vIno9, vQty9, vIno10, vQty10, vIno11, vQty11, vIno12, vQty12, vIno13, vQty13, vIno14, vQty14, vIno15, vQty15, "0")
		
		set SaveSecondChildDough = object.SaveTwoStageMixingIngredients(vParentDoughNo, vIno1, vsQty1, vIno2, vsQty2, vIno3, vsQty3, vIno4, vsQty4, vIno5, vsQty5, vIno6, vsQty6, vIno7, vsQty7, vIno8, vsQty8, vIno9, vsQty9, vIno10, vsQty10, vIno11, vsQty11, vIno12, vsQty12, vIno13, vsQty13, vIno14, vsQty14, vIno15, vsQty15, "1")
		
		if SaveSecondChildDough("Sucess") <> "OK" then
			set SaveSecondChildDough= nothing
			set object = nothing
			response.redirect "inventory_doughs_new.asp?status=error"
		else
			set SaveSecondChildDough= nothing
			set object = nothing
			response.redirect "inventory_doughs_new.asp?status=ok&doughname="&Server.URLEncode(vDName)
		end if
	end if
end if

%>