<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
set object = Server.CreateObject("bakery.customer")
object.SetEnvironment(strconnection)
strGNO=request("sgno")
if (strGNO="") then
	strGNO="0"
end if 
if strGNO<>"" then
	set DisplayCustomerGroupDetail= object.CustomerGroupList("","",strGNO)
	vCustomerGroupDetailArray = DisplayCustomerGroupDetail("CustomerGroupList")
	set DisplayCustomerGroupDetail= nothing
	set object = nothing
	strGNO = vCustomerGroupDetailArray(0,0)
	strGname = vCustomerGroupDetailArray(1,0)
	'strEmail = vCustomerGroupDetailArray(2,0)
	strElectronicInvoice = vCustomerGroupDetailArray(3,0)
	strDailyAutomatedGroupInvoice = vCustomerGroupDetailArray(4,0)
	strDailyAutomatedGroupCredit = vCustomerGroupDetailArray(5,0)
	strNotes = vCustomerGroupDetailArray(6,0)
	strAddress1 = vCustomerGroupDetailArray(7,0)
	strAddress2 = vCustomerGroupDetailArray(8,0)
	strAddress3 = vCustomerGroupDetailArray(9,0)
	strTown = vCustomerGroupDetailArray(10,0)
	strPostcode = vCustomerGroupDetailArray(11,0)
	strPackingSheetType = vCustomerGroupDetailArray(12,0)
	strTelephone = vCustomerGroupDetailArray(13,0)
	strLogo =  vCustomerGroupDetailArray(14,0)
	strPricelessInvoice =  vCustomerGroupDetailArray(15,0)
	strgroupcustomercategory =  vCustomerGroupDetailArray(17,0)
	strMinimumGroupOrder =  vCustomerGroupDetailArray(18,0)
		
	set objBakery1 = server.CreateObject("Bakery.Customer")
	objBakery1.SetEnvironment(strconnection)
	set CustomerGroupCol = objBakery1.CustomersFromCustomerGroup(cint(strGNO))
	strCustomerGroup = CustomerGroupCol("Customers")
	stop
	Set ObjCustomerGroupPrice = objBakery1.GetProductPriceforCustomerGroup(cint(strGNO),"")
    arSpecialProductPrice = ObjCustomerGroupPrice("PriceforCustomerGroup")
	
	
	Set ObjCustomerGroupVSummary = objBakery1.GetGroupPriceVarianceSummary(cint(strGNO))
    arCustomerGroupVSummary = ObjCustomerGroupVSummary("CustomerGroupPriceList")
	
	
	set listCustomerGroupEmailList = objBakery1.ListCustomerGroupEmailList(strGNO, 1)
    arrlistCustomerGroupEmailList = listCustomerGroupEmailList("CustomerGroupEmailList")
	stop
	For j = 0 to UBound(arrlistCustomerGroupEmailList,2)
		
	if (strEmail = "") Then        
		    strEmail =  arrlistCustomerGroupEmailList(3,j)
		Else     			
			strEmail = strEmail + "<br>" + arrlistCustomerGroupEmailList(3,j)	
			
	    End If				
	Next 
	
		
	CustomerGroupCol.close()
	set CustomerGroupCol=nothing
	objBakery1.close()
	set objBakery1=nothing
else
	Response.Redirect("CustomerGroup.asp")	
end if
%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>

<script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{

  if (theForm.gname.value == "")
  {
    alert("Please enter a value for the \"Name\" field.");
    theForm.gname.focus();
    return (false);
  }

  return (true);
}
//--></script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<table border="0" align="center" width="90%" cellspacing="1" cellpadding="3" style="font-family: Verdana; font-size: 8pt">
   <tr>
		<td height="25" valign="bottom"><a href="CustomerGroup.asp"><strong><font color="#000099">List of Customer Groups</font></strong></a></td>
  </tr>
  <tr>
    <td width="100%"><br><br>	
      	<table bgcolor="#CCCCCC" align="center" border="0" width="600" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="4">
        <tr height="30">
          <td colspan="2" align="center"><strong>Customer Group Details</strong></td>
        </tr>
		<tr bgcolor="#FFFFFF" height="25">
          <td width="248" align="right">Group No:</td>
          <td width="344"><%=strGNO%></td>
        </tr>
		<tr bgcolor="#FFFFFF" height="25">
          <td width="248" align="right">Group Name:</td>
          <td width="344"><%=strGname%></td>
        </tr>
		<tr bgcolor="#FFFFFF" height="25">
          <td align="right" valign="top">Email:</td>
          <td><%=strEmail%></td>
        </tr>
		<tr bgcolor="#FFFFFF" height="25">
          <td align="right">Address 1:</td>
          <td><%=strAddress1%></td>
        </tr>		
		<tr bgcolor="#FFFFFF" height="25">
          <td align="right">Address 2:</td>
          <td><%=strAddress2%></td>
        </tr>
		<tr bgcolor="#FFFFFF" height="25">
          <td align="right">Address 3:</td>
          <td><%=strAddress3%></td>
        </tr>		
		<tr bgcolor="#FFFFFF" height="25">
          <td align="right">Town:</td>
          <td><%=strTown%></td>
        </tr>		
		<tr bgcolor="#FFFFFF" height="25">
          <td align="right">Telephone:</td>
          <td><%=strTelephone%></td>
        </tr>
       	<tr bgcolor="#FFFFFF" height="25">
          <td align="right">Postcode:</td>
          <td><%=strPostcode%></td>
        </tr>  
		<tr bgcolor="#FFFFFF" height="25">
          <td align="right">CSV File Group Invoice:</td>
          <td><%=strElectronicInvoice%></td>
        </tr>
		<tr bgcolor="#FFFFFF" height="25">
          <td align="right">Daily Automated Group Invoice:</td>
          <td><%=strDailyAutomatedGroupInvoice%></td>
	   </tr>
	   <tr bgcolor="#FFFFFF" height="25">
          <td align="right">Daily Automated Group Credit:</td>
          <td><%=strDailyAutomatedGroupCredit%></td>
	   </tr>
	    
	   <tr bgcolor="#FFFFFF" height="25">
          <td align="right">Priceless Invoice/Credit:</td>
          <td><%=strPricelessInvoice%></td>
	   </tr>
	   
	   <tr bgcolor="#FFFFFF" height="25">
          <td align="right">Packing Sheet Type:</td>
		  <td>
          <%if strPackingSheetType = 1 Then %>          
          Type 1
          <%Else %>
          Type 2
          <%End if %>
		  </td>
		 </tr>
		  <tr bgcolor="#FFFFFF" height="25">
          <td align="right">Minimum Group Order Amount:</td>
          <td>
		  <%
		  if (strMinimumGroupOrder<>"") then
		  %>
		  &pound;<%=FormatNumber(strMinimumGroupOrder)%>
		  <%
		  end if
		  %>
		  </td>
	   </tr>
		 <tr bgcolor="#FFFFFF" height="25">
          <td align="right">Customer Category:</td>
          <td><%=strgroupcustomercategory%></td>
	   </tr>
		<tr bgcolor="#FFFFFF" height="25">
          <td align="right">Message:</td>
          <td><%=strNotes%></td>
		 </tr> 
		<tr bgcolor="#FFFFFF" height="25" valign="top">
          <td align="right">Customer Code with Name:</td>
          <td><%=strCustomerGroup%></td>
		 </tr> 
		 <tr bgcolor="#FFFFFF" height="25" valign="top">
          <td align="right">Company Logo :</td>
          <td> 
          <%if strLogo <> "" Then %>
          <img src="images/CustomerGroupLogo/<%=strLogo%>">
          <%End if %>
          </td>
		 </tr> 
		 <tr bgcolor="#FFFFFF" height="25" valign="top">
          <td align="right">Upload Products</td>
          <td> 
		 
		 <input type="button" value=" Upload Products " style="font-family: Verdana; font-size: 8pt" onClick="NewWindow('UploadCustomerGroupProducts1.asp?CustNo=<%=strGNO%>','UploadProduct','750','300','yes','center');">
	          </td>
		 </tr>	 
		<tr bgcolor="#FFFFFF">
        <tr height="30">
          <td></td>
          <td>
          <input type="button" value=" Back " name="B1" onClick="history.back()" style="font-family: Verdana; font-size: 9pt"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
		<td height="25" valign="bottom">
		
  <center>

      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td bgcolor="#FFFFFF" colspan="4"><b><font size="2">Group Special Pricing</font></b></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        </table>
	  <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" bgcolor="#999999">
		<tr height="22">
          <td bgcolor="#CCCCCC"><b>Product code</b></td>
          <td bgcolor="#CCCCCC"><b>Product name</b></td>
          <td bgcolor="#CCCCCC"><b>Normal Price</b></td>
          <td bgcolor="#CCCCCC"><b>Group Price</b></td>
        </tr><%        
					if IsArray(arSpecialProductPrice) Then
						k=1
						For i = 0 to UBound(arSpecialProductPrice,2)
						if (k mod 2 =0) then
							strBgColour="#CCCCCC"
						else
							strBgColour="#F3F3F3"
						end if	
						k=k+1					
						%>
							<tr bgcolor="<%=strBgColour%>" height="22">
								<td><%=arSpecialProductPrice(0,i)%></td>
								<td><%=arSpecialProductPrice(1,i)%></td>
								<td>
								<%
								if (arSpecialProductPrice(2,i)="-9999.99") then
									response.write("-0")
								else
								 	response.write(formatnumber(arSpecialProductPrice(2,i),2))
								end if
								%>
								</td>
								<td><%=formatnumber(arSpecialProductPrice(3,i),2)%></td>
							</tr><%	
						Next
					End if%>	
      </table>
  </center>

		
		</td>
  </tr>
  <tr>
  <td>
   <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0" align="center"> 
   <tr>
   <td height="40">       
   <input onClick="NewWindow('customer_products.asp?CustNo=-1&GNo=<%=strGNO%>','profile','750','300','yes','center');return false" onFocus="this.blur()" type="button" value="Add Group Special Price" name="SPrice" style="font-family: Verdana; font-size: 8pt">
   </td>
   </tr>
   </table>
  </td>
  </tr>
  
  
    <tr>
		<td height="25" valign="bottom">
		
  <center>

      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td bgcolor="#FFFFFF" colspan="3"><b><font size="2">Group Special Price Variance</font></b></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        </table>
		 <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" bgcolor="#999999">
		<tr height="22">
          <td bgcolor="#CCCCCC"><b>Product code</b></td>
          <td bgcolor="#CCCCCC"><b>Product name</b></td>
         <td bgcolor="#CCCCCC"><b>Product Variance</b></td>
        </tr><%        
					if IsArray(arCustomerGroupVSummary) Then
						k=1
						For i = 0 to UBound(arCustomerGroupVSummary,2)
						if (k mod 2 =0) then
							strBgColour="#CCCCCC"
						else
							strBgColour="#F3F3F3"
						end if	
						k=k+1					
						%>
							<tr bgcolor="<%=strBgColour%>" height="22">
								<td><%=arCustomerGroupVSummary(0,i)%></td>
								<td><%=arCustomerGroupVSummary(1,i)%></td>
								<td><input onClick="NewWindow('customer_group_price_variance.asp?PNo=<%=arCustomerGroupVSummary(0,i)%>&GNo=<%=strGNO%>','profile','750','300','yes','center');return false" onFocus="this.blur()" type="button" value="Show Variance" name="variance" style="font-family: Verdana; font-size: 8pt"></td>
								
							</tr><%	
						Next
					End if%>	
      </table>
  </center>

		
		</td>
  </tr>
 
  
</table>
<br>
</body>
</html>
<%
obj.close()
set obj=nothing
%>