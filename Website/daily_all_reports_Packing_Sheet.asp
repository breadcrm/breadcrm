<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Response.Buffer
deldate= request("deldate")
if deldate= "" then response.redirect "LoginInternalPage.asp"

dim objBakery
dim recarray1, recarray2
dim ordno
dim retcol
Dim deldate
Dim intI
Dim intN
Dim intF
Dim intJ


dim tname, tno, curtno
Dim obj
Dim arCreditStatements
Dim arCreditStatementDetails
Dim vCreditDate

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; height:1px }
-->
</style>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
'Packing sheet report
set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
%>
<!--Added by thilina-->

<%

' 16 - Flour Station - Added on 18th April 2008 by Selva
'Packing sheet report - 11-Stanmore English, 12-Stanmore French, 99-Stanmore Goods, 15-Park Royal 
		facility = "16"
		tno=3
		for curtno=1 to tno
			if curtno=1 then
				dtype="Morning"
			elseif curtno=2 then
				dtype="Noon"
			else
				dtype="Evening"
			end if
			intN=0
			stop
			set col1= object.DailyPackingSheetReportWithAllFSProducts2(deldate,facility,dtype)
			vRecArray = col1("DailyPackingSheet")
			vtotcustarray = col1("TotalCustomers")
			if isarray(vtotcustarray) then
				totpages = ubound(vtotcustarray,2)+1
			else
				totpages = 0
			end if
			curpage=0
	    
			Facilityrec = col1("Facility")
			set col1= nothing
					mFontSize = 8
					mFooterSize =3
					mFooterFontSize = 3
					mExtraLine = 10
					mLine = 8
				if isarray(vrecarray) then
					mPage = 1
					currec=0
					totqty=0
					curcno = vrecarray(0,0)
					curvan = vrecarray(5,0)
					curpage=1	  
					for i = 0 to ubound(vRecArray,2)
						currec = clng(currec) + 1
						if clng(currec) = 1 then
							if mFontSize = 14 Then
								mLine = mline + 4
							else
								mLine = mline + 3
							end if	
							if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
								mPage = mPage + 1						
								mLine = 0
							End if
						end if
						if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then
							if mFontSize = 14 Then
								mLine = mLine + 2
							else
								mLine = mLine + 1
							end if	
						end if
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0
						End if          
						if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
							curvan = vrecarray(5,i)
							curcno = vrecarray(0,i)
							i=clng(i)-1
							if mFontSize = 14 Then
								mLine = mLine +4           
							else
								mLine = mLine +3           
							end if	
						
							if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then
								mPage = mPage + 1
								mLine = 0	
							else
								if curvan <> vrecarray(5,i)   Then										
									mPage = mPage + 1
									mLine = 0
								End if
							End if
							if i < ubound(vrecarray,2) then
							end if            
							curpage = curpage + 1
							currec = 0
							totqty=0
						end if
					next
	    
				else
					mPage = 1
				end if
				i = 0
if isarray(vrecarray) then
%>

<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: <%=mFontSize%>pt">
  <tr>
    <td width="100%">
    <center><b><font size="3">Flour Station Customers PACKING SHEET - <%=dtype%><br></font></b></center>
      <center><b><font size="3">(divided to Van and Customers)<br></font></b></center>
      <table border="0" width="100%" style="font-family: Verdana; font-size: <%=mFontSize%>pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><br>
            <br>
            <b>Date of report:</b> <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            <b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><BR>
           
            </td>
        </tr>
        <tr>
          <td>Number of pages for this report: <%=mPage%></td>
          <td align="right">Page 1 of <%=mPage%><br></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
</center>
</div><%
if mFontSize = 14 Then
	mLine = 16
Else
	mLine = 8
End if%>

<div align="center">
<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
	  if isarray(vrecarray) then
		mTotalPage = mPage
		mPage = 1		
	    currec=0
	    totqty=0
	    curcno = vrecarray(0,0)
	    curvan = vrecarray(5,0)
	    curpage=1	  
        for i = 0 to ubound(vRecArray,2)
          currec = clng(currec) + 1
          if clng(currec) = 1 then%>
            <tr>
            <td colspan="4">
              <b>Van : <%=vrecarray(5,i)%></b><br>
              <b>Customer : <%=vrecarray(0,i)%> - <%=vrecarray(1,i)%> (<%=vrecarray(7,i)%>)</b>
            </td>            
            </tr>
            <tr>            
            <td align="left" bgcolor="#CCCCCC" height="20" width="25%"><b>Facility</b></td> 
			<td align="left" bgcolor="#CCCCCC" height="20" width="60%"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Qty</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="5%"><b>SID</b></td>
            </tr><%
            if mFontSize = 14 Then
							mLine = mline + 4
						else
							mLine = mline + 3
						end if	
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td align="left" colspan="2"><b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%></td>
								<td colspan="2" align="right"><%
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
						End if
           end if
           if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then%>
          <tr>            
            <td align="left" width="25%"><%=vrecarray(8,i)%>&nbsp;</td>
			<td align="left" width="60%"><%=left(vrecarray(3,i),50)%>&nbsp;</td>
            <td align="center" width="10%"><%=vrecarray(4,i)%>&nbsp;</td>
			<%
            totqty = clng(totqty) + vrecarray(4,i)
			%>
            <td align="center" width="5%">&nbsp;</td>
          </tr><%
						if mFontSize = 14 Then
							mLine = mLine + 2
						else
							mLine = mLine + 1
						end if	
          end if
          if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
						mPage = mPage + 1						
						mLine = 0%>		
						</table>						
						<br class="page" />						
						<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td align="left" colspan="2"><b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%></td>
								<td colspan="2" align="right"><%	
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
          End if
          
		  if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
		    curvan = vrecarray(5,i)
		    curcno = vrecarray(0,i)
		    i=clng(i)-1%>
		    </table>
						<br>
            <br><%
            if mFontSize = 14 Then
							mLine = mLine +4           
						else
							mLine = mLine +3           
						end if	
         if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then 						
         
						mPage = mPage + 1
						mLine = 0%>								
						<br class="page" /><%						
		  else
			if curvan <> vrecarray(5,i)   Then										
				mPage = mPage + 1
				mLine = 0%>													
				<br class="page" /><%
			End if				
          End if
          
            if i < ubound(vrecarray,2) then%>
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
							if mLine = 0 Then %>
								<tr>
									<td colspan="2" align="left"><b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%></td>
									<td colspan="2" align="right"><%
									 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
								</tr><%						
							end if         
            end if   
            
		    curpage = curpage + 1
		    currec = 0
		    totqty=0
		  end if
	    next%>
	    <%else%>
        <tr>
        <td width="100%" colspan = "4" height="20">Sorry no items found</td>
        </tr><%
	  end if%>
      </table>
</div>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%end if%>
<p>&nbsp;</p>

<%
Response.Flush()
next
set col1= nothing

%>


<!------------------->


<%
' 16 - Flour Station - Added on 18th April 2008 by Selva
'Packing sheet report - 11-Stanmore English, 12-Stanmore French, 99-Stanmore Goods, 15-Park Royal 
		facility = "11,12,99,15,16"
		tno=3
		for curtno=1 to tno
			if curtno=1 then
				dtype="Morning"
			elseif curtno=2 then
				dtype="Noon"
			else
				dtype="Evening"
			end if
			intN=0
			set col1= object.DailyPackingSheetReportWithAllFSProducts(deldate,facility,dtype)
			vRecArray = col1("DailyPackingSheet")
			vtotcustarray = col1("TotalCustomers")
			if isarray(vtotcustarray) then
				totpages = ubound(vtotcustarray,2)+1
			else
				totpages = 0
			end if
			curpage=0
	    
			Facilityrec = col1("Facility")
			set col1= nothing
					mFontSize = 8
					mFooterSize =3
					mFooterFontSize = 3
					mExtraLine = 10
					mLine = 8
				if isarray(vrecarray) then
					mPage = 1
					currec=0
					totqty=0
					curcno = vrecarray(0,0)
					curvan = vrecarray(5,0)
					curpage=1	  
					for i = 0 to ubound(vRecArray,2)
						currec = clng(currec) + 1
						if clng(currec) = 1 then
							if mFontSize = 14 Then
								mLine = mline + 4
							else
								mLine = mline + 3
							end if	
							if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
								mPage = mPage + 1						
								mLine = 0
							End if
						end if
						if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then
							if mFontSize = 14 Then
								mLine = mLine + 2
							else
								mLine = mLine + 1
							end if	
						end if
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0
						End if          
						if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
							curvan = vrecarray(5,i)
							curcno = vrecarray(0,i)
							i=clng(i)-1
							if mFontSize = 14 Then
								mLine = mLine +4           
							else
								mLine = mLine +3           
							end if	
						
							if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then
								mPage = mPage + 1
								mLine = 0	
							else
								if curvan <> vrecarray(5,i)   Then										
									mPage = mPage + 1
									mLine = 0
								End if
							End if
							if i < ubound(vrecarray,2) then
							end if            
							curpage = curpage + 1
							currec = 0
							totqty=0
						end if
					next
	    
				else
					mPage = 1
				end if
				i = 0
if isarray(vrecarray) then
%>

<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: <%=mFontSize%>pt">
  <tr>
    <td width="100%">
    <center><b><font size="3">Stanmore, Park Royal and Flour Station PACKING SHEET - <%=dtype%><br></font></b></center>
      <center><b><font size="3">(divided to Van and Customers)<br></font></b></center>
      <table border="0" width="100%" style="font-family: Verdana; font-size: <%=mFontSize%>pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><br>
            <br>
            <b>Date of report:</b> <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            <b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><BR>
           
            </td>
        </tr>
        <tr>
          <td>Number of pages for this report: <%=mPage%></td>
          <td align="right">Page 1 of <%=mPage%><br></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
</center>
</div><%
if mFontSize = 14 Then
	mLine = 16
Else
	mLine = 8
End if%>

<div align="center">
<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
	  if isarray(vrecarray) then
		mTotalPage = mPage
		mPage = 1		
	    currec=0
	    totqty=0
	    curcno = vrecarray(0,0)
	    curvan = vrecarray(5,0)
	    curpage=1	  
        for i = 0 to ubound(vRecArray,2)
          currec = clng(currec) + 1
          if clng(currec) = 1 then%>
            <tr>
            <td colspan="4">
              <b>Van : <%=vrecarray(5,i)%></b><br>
              <b>Customer : <%=vrecarray(0,i)%> - <%=vrecarray(1,i)%> (<%=vrecarray(7,i)%>)</b>
            </td>            
            </tr>
            <tr>            
            <td align="left" bgcolor="#CCCCCC" height="20" width="25%"><b>Facility</b></td> 
			<td align="left" bgcolor="#CCCCCC" height="20" width="60%"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Qty</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="5%"><b>SID</b></td>
            </tr><%
            if mFontSize = 14 Then
							mLine = mline + 4
						else
							mLine = mline + 3
						end if	
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td align="left" colspan="2"><b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%></td>
								<td colspan="2" align="right"><%
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
						End if
           end if
           if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then%>
          <tr>            
            <td align="left" width="25%"><%=vrecarray(8,i)%>&nbsp;</td>
			<td align="left" width="60%"><%=left(vrecarray(3,i),50)%>&nbsp;</td>
            <td align="center" width="10%"><%=vrecarray(4,i)%>&nbsp;</td>
			<%
            totqty = clng(totqty) + vrecarray(4,i)
			%>
            <td align="center" width="5%">&nbsp;</td>
          </tr><%
						if mFontSize = 14 Then
							mLine = mLine + 2
						else
							mLine = mLine + 1
						end if	
          end if
          if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
						mPage = mPage + 1						
						mLine = 0%>		
						</table>						
						<br class="page" />						
						<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td align="left" colspan="2"><b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%></td>
								<td colspan="2" align="right"><%	
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
          End if
          
		  if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
		    curvan = vrecarray(5,i)
		    curcno = vrecarray(0,i)
		    i=clng(i)-1%>
		    </table>
						<br>
            <br><%
            if mFontSize = 14 Then
							mLine = mLine +4           
						else
							mLine = mLine +3           
						end if	
         if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then 						
         
						mPage = mPage + 1
						mLine = 0%>								
						<br class="page" /><%						
		  else
			if curvan <> vrecarray(5,i)   Then										
				mPage = mPage + 1
				mLine = 0%>													
				<br class="page" /><%
			End if				
          End if
          
            if i < ubound(vrecarray,2) then%>
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
							if mLine = 0 Then %>
								<tr>
									<td colspan="2" align="left"><b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%></td>
									<td colspan="2" align="right"><%
									 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
								</tr><%						
							end if         
            end if   
            
		    curpage = curpage + 1
		    currec = 0
		    totqty=0
		  end if
	    next%>
	    <%else%>
        <tr>
        <td width="100%" colspan = "4" height="20">Sorry no items found</td>
        </tr><%
	  end if%>
      </table>
</div>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%end if%>
<p>&nbsp;</p>

<%
Response.Flush()
next
set col1= nothing

%>
<%
'Packing sheet report - 18-BMG 
		facility = "18"
		tno=3
		for curtno=1 to tno
			if curtno=1 then
				dtype="Morning"
			elseif curtno=2 then
				dtype="Noon"
			else
				dtype="Evening"
			end if
			intN=0
			set col1= object.DailyPackingSheetReportWithAllFSProducts(deldate,facility,dtype)
			vRecArray = col1("DailyPackingSheet")
			vtotcustarray = col1("TotalCustomers")
			if isarray(vtotcustarray) then
				totpages = ubound(vtotcustarray,2)+1
			else
				totpages = 0
			end if
			curpage=0
	    	Facilityrec = col1("Facility")
			set col1= nothing

					mFontSize = 8
					mFooterSize =3
					mFooterFontSize = 3
					mExtraLine = 10
					mLine = 8
				if isarray(vrecarray) then
					mPage = 1
					currec=0
					totqty=0
					curcno = vrecarray(0,0)
					curvan = vrecarray(5,0)
					curpage=1	  
					for i = 0 to ubound(vRecArray,2)
						currec = clng(currec) + 1
						if clng(currec) = 1 then
							if mFontSize = 14 Then
								mLine = mline + 4
							else
								mLine = mline + 3
							end if	
							if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
								mPage = mPage + 1						
								mLine = 0
							End if
						end if
						if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then
							if mFontSize = 14 Then
								mLine = mLine + 2
							else
								mLine = mLine + 1
							end if	
						end if
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0
						End if          
						if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
							curvan = vrecarray(5,i)
							curcno = vrecarray(0,i)
							i=clng(i)-1
							if mFontSize = 14 Then
								mLine = mLine +4           
							else
								mLine = mLine +3           
							end if	
						
							if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then
								mPage = mPage + 1
								mLine = 0	
							else
								if curvan <> vrecarray(5,i)   Then										
									mPage = mPage + 1
									mLine = 0
								End if
							End if
							if i < ubound(vrecarray,2) then
							end if            
							curpage = curpage + 1
							currec = 0
							totqty=0
						end if
					next
	    
				else
					mPage = 1
				end if
				i = 0
if isarray(vrecarray) then
%>

<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: <%=mFontSize%>pt">
  <tr>
    <td width="100%">
    <center><b><font size="3">BMG PACKING SHEET - <%=dtype%><br></font></b></center>
      <center><b><font size="3">(divided to Van and Customers)<br></font></b></center>
      <table border="0" width="100%" style="font-family: Verdana; font-size: <%=mFontSize%>pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><br>
            <br>
            <b>Date of report:</b> <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            <b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><BR>
           
            </td>
        </tr>
        <tr>
          <td>Number of pages for this report: <%=mPage%></td>
          <td align="right">Page 1 of <%=mPage%><br></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
</center>
</div><%
if mFontSize = 14 Then
	mLine = 16
Else
	mLine = 8
End if%>

<div align="center">
<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
	  if isarray(vrecarray) then
		mTotalPage = mPage
		mPage = 1		
	    currec=0
	    totqty=0
	    curcno = vrecarray(0,0)
	    curvan = vrecarray(5,0)
	    curpage=1	  
        for i = 0 to ubound(vRecArray,2)
          currec = clng(currec) + 1
          if clng(currec) = 1 then%>
            <tr>
            <td colspan="4">
              <b>Van : <%=vrecarray(5,i)%></b><br>
              <b>Customer : <%=vrecarray(0,i)%> - <%=vrecarray(1,i)%> (<%=vrecarray(7,i)%>)</b>
            </td>            
            </tr>
            <tr>            
            <td align="left" bgcolor="#CCCCCC" height="20" width="25%"><b>Facility</b></td> 
			<td align="left" bgcolor="#CCCCCC" height="20" width="60%"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Qty</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="5%"><b>SID</b></td>
            </tr><%
            if mFontSize = 14 Then
							mLine = mline + 4
						else
							mLine = mline + 3
						end if	
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td colspan="4" align="right"><%
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
						End if
           end if
           if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then%>
          <tr>            
            <td align="left" width="25%"><%=vrecarray(8,i)%>&nbsp;</td>
			<td align="left" width="60%"><%=left(vrecarray(3,i),50)%>&nbsp;</td>
            <td align="center" width="10%"><%=vrecarray(4,i)%>&nbsp;</td>
			<%
            totqty = clng(totqty) + vrecarray(4,i)
			%>
            <td align="center" width="5%">&nbsp;</td>
          </tr><%
						if mFontSize = 14 Then
							mLine = mLine + 2
						else
							mLine = mLine + 1
						end if	
          end if
          if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
						mPage = mPage + 1						
						mLine = 0%>		
						</table>						
						<br class="page" />						
						<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td colspan="3" align="right"><%	
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
          End if
          
		  if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
		    curvan = vrecarray(5,i)
		    curcno = vrecarray(0,i)
		    i=clng(i)-1%>
		    </table>
						<br>
            <br><%
            if mFontSize = 14 Then
							mLine = mLine +4           
						else
							mLine = mLine +3           
						end if	
         if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then 						
         
						mPage = mPage + 1
						mLine = 0%>								
						<br class="page" /><%						
		  else
			if curvan <> vrecarray(5,i)   Then										
				mPage = mPage + 1
				mLine = 0%>													
				<br class="page" /><%
			End if				
          End if
          
            if i < ubound(vrecarray,2) then%>
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
							if mLine = 0 Then %>
								<tr>
									<td colspan="3" align="right"><%
									 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
								</tr><%						
							end if         
            end if   
            
		    curpage = curpage + 1
		    currec = 0
		    totqty=0
		  end if
	    next%>
	    <%else%>
        <tr>
        <td width="100%" colspan = "4" height="20">Sorry no items found</td>
        </tr><%
	  end if%>
      </table>
</div>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%end if%>
<p>&nbsp;</p>

<%
Response.Flush()
next
set col1= nothing

%>

<p>&nbsp;</p>
<%
'Packing sheet report - 22-Cake Department
		facility = "22"
		tno=3
		for curtno=1 to tno
			if curtno=1 then
				dtype="Morning"
			elseif curtno=2 then
				dtype="Noon"
			else
				dtype="Evening"
			end if
			intN=0
			set col1= object.DailyPackingSheetReportWithAllFSProducts(deldate,facility,dtype)
			vRecArray = col1("DailyPackingSheet")
			vtotcustarray = col1("TotalCustomers")
			if isarray(vtotcustarray) then
				totpages = ubound(vtotcustarray,2)+1
			else
				totpages = 0
			end if
			curpage=0
	    	Facilityrec = col1("Facility")
			set col1= nothing

					mFontSize = 8
					mFooterSize =3
					mFooterFontSize = 3
					mExtraLine = 10
					mLine = 8
				if isarray(vrecarray) then
					mPage = 1
					currec=0
					totqty=0
					curcno = vrecarray(0,0)
					curvan = vrecarray(5,0)
					curpage=1	  
					for i = 0 to ubound(vRecArray,2)
						currec = clng(currec) + 1
						if clng(currec) = 1 then
							if mFontSize = 14 Then
								mLine = mline + 4
							else
								mLine = mline + 3
							end if	
							if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
								mPage = mPage + 1						
								mLine = 0
							End if
						end if
						if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then
							if mFontSize = 14 Then
								mLine = mLine + 2
							else
								mLine = mLine + 1
							end if	
						end if
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0
						End if          
						if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
							curvan = vrecarray(5,i)
							curcno = vrecarray(0,i)
							i=clng(i)-1
							if mFontSize = 14 Then
								mLine = mLine +4           
							else
								mLine = mLine +3           
							end if	
						
							if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then
								mPage = mPage + 1
								mLine = 0	
							else
								if curvan <> vrecarray(5,i)   Then										
									mPage = mPage + 1
									mLine = 0
								End if
							End if
							if i < ubound(vrecarray,2) then
							end if            
							curpage = curpage + 1
							currec = 0
							totqty=0
						end if
					next
	    
				else
					mPage = 1
				end if
				i = 0
if isarray(vrecarray) then
%>

<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: <%=mFontSize%>pt">
  <tr>
    <td width="100%">
    <center><b><font size="3">Cake Department PACKING SHEET - <%=dtype%><br></font></b></center>
      <center><b><font size="3">(divided to Van and Customers)<br></font></b></center>
      <table border="0" width="100%" style="font-family: Verdana; font-size: <%=mFontSize%>pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><br>
            <br>
            <b>Date of report:</b> <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            <b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><BR>
           
            </td>
        </tr>
        <tr>
          <td>Number of pages for this report: <%=mPage%></td>
          <td align="right">Page 1 of <%=mPage%><br></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
</center>
</div><%
if mFontSize = 14 Then
	mLine = 16
Else
	mLine = 8
End if%>

<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
	  if isarray(vrecarray) then
		mTotalPage = mPage
		mPage = 1		
	    currec=0
	    totqty=0
	    curcno = vrecarray(0,0)
	    curvan = vrecarray(5,0)
	    curpage=1	  
        for i = 0 to ubound(vRecArray,2)
          currec = clng(currec) + 1
          if clng(currec) = 1 then%>
            <tr>
            <td colspan="4">
              <b>Van : <%=vrecarray(5,i)%></b><br>
              <b>Customer : <%=vrecarray(0,i)%> - <%=vrecarray(1,i)%> (<%=vrecarray(7,i)%>)</b>
            </td>            
            </tr>
            <tr>            
            <td align="left" bgcolor="#CCCCCC" height="20" width="25%"><b>Facility</b></td> 
			<td align="left" bgcolor="#CCCCCC" height="20" width="60%"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Qty</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="5%"><b>SID</b></td>
            </tr><%
            if mFontSize = 14 Then
							mLine = mline + 4
						else
							mLine = mline + 3
						end if	
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td colspan="4" align="right"><%
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
						End if
           end if
           if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then%>
          <tr>            
            <td align="left" width="25%"><%=vrecarray(8,i)%>&nbsp;</td>
			<td align="left" width="60%"><%=left(vrecarray(3,i),50)%>&nbsp;</td>
            <td align="center" width="10%"><%=vrecarray(4,i)%>&nbsp;</td>
			<%
            totqty = clng(totqty) + vrecarray(4,i)
			%>
            <td align="center" width="5%">&nbsp;</td>
          </tr><%
						if mFontSize = 14 Then
							mLine = mLine + 2
						else
							mLine = mLine + 1
						end if	
          end if
          if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
						mPage = mPage + 1						
						mLine = 0%>		
						</table>						
						<br class="page" />						
						<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td colspan="4" align="right"><%	
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
          End if
          
		  if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
		    curvan = vrecarray(5,i)
		    curcno = vrecarray(0,i)
		    i=clng(i)-1%>
		    </table>
						<br>
            <br><%
            if mFontSize = 14 Then
							mLine = mLine +4           
						else
							mLine = mLine +3           
						end if	
         if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then 						
         
						mPage = mPage + 1
						mLine = 0%>								
						<br class="page" /><%						
		  else
			if curvan <> vrecarray(5,i)   Then										
				mPage = mPage + 1
				mLine = 0%>													
				<br class="page" /><%
			End if				
          End if
          
            if i < ubound(vrecarray,2) then%>
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
							if mLine = 0 Then %>
								<tr>
									<td colspan="4" align="right"><%
									 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
								</tr><%						
							end if         
            end if   
            
		    curpage = curpage + 1
		    currec = 0
		    totqty=0
		  end if
	    next%>
		
	    <%else%>
        <tr>
        <td width="100%" colspan = "4" height="20">Sorry no items found</td>
        </tr><%
	  end if%>
      </table>
<% if curtno<tno then%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%end if%>
<%end if%>
<%
Response.Flush()
next
set col1= nothing

%>
<p>&nbsp;</p>
<hr>

<!-- New Report Begin -->
<%
'Packing sheet report - new packing sheets for some large customers
		
		'Facility No with Name: 
		'11 - Stanmore English
		'12 - Stanmore French
		'15 - Park Royal
		'16 - Flour Station
		'21 - Baker & Spice (Denyer Street)
		'23 - GAILS
		'30 - Rinkoff
		'31 - Sally Clarks
		'32 - Simply Bread
		'33 - Bagel Nash
		'90 - Stanmore
		'99 - Stanmore Goods
		'100 - 	Bread Etc			
		
		facility = "11,12,15,16,21,23,30,31,32,33,90,99,100" '18, 22 exclude
		
		'Group No with Name: 
		'12 - Giraffe
		'117 - GAILs  
		'147 - Waitrose
		'151 - The Flour Station
		'154 - Carluccios
		'168 - Maison Blanc
		'213 - John Lewis
		
		'added on 8 Sep 2009
		'7 - baker & spice
		
		'added on 29 Oct 2009
		'152 - FS Waitrose 
		
		strGNo="12,117,147,151,154,168,213,7,152"
		arrayGNo=split(strGNo,",")
		
		for sGNo=lbound(arrayGNo) to ubound(arrayGNo)
			
			set objectCus = Server.CreateObject("bakery.customer")
			objectCus.SetEnvironment(strconnection)
			set DisplayCustomerGroupDetail= objectCus.CustomerGroupList("","",arrayGNo(sGNo))
			vCustomerGroupDetailArray = DisplayCustomerGroupDetail("CustomerGroupList")
			set DisplayCustomerGroupDetail= nothing
			set objectCus = nothing
			strGname = vCustomerGroupDetailArray(1,0)
	
		for curtno=1 to 3
			if curtno=1 then
				dtype="Morning"
			elseif curtno=2 then
				dtype="Noon"
			else
				dtype="Evening"
			end if
			intN=0
			set col1= object.DailyPackingSheetReportWithCustomerGroup(deldate,arrayGNo(sGNo),facility,dtype)
			vRecArray = col1("DailyPackingSheet")
			vtotcustarray = col1("TotalCustomers")
			if isarray(vtotcustarray) then
				totpages = ubound(vtotcustarray,2)+1
			else
				totpages = 0
			end if
			curpage=0
	    	Facilityrec = col1("Facility")
			set col1= nothing

					mFontSize = 8
					mFooterSize =3
					mFooterFontSize = 3
					mExtraLine = 10
					mLine = 8
				if isarray(vrecarray) then
					mPage = 1
					currec=0
					totqty=0
					curcno = vrecarray(0,0)
					curvan = vrecarray(5,0)
					curpage=1	  
					for i = 0 to ubound(vRecArray,2)
						currec = clng(currec) + 1
						if clng(currec) = 1 then
							if mFontSize = 14 Then
								mLine = mline + 4
							else
								mLine = mline + 3
							end if	
							if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
								mPage = mPage + 1						
								mLine = 0
							End if
						end if
						if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then
							if mFontSize = 14 Then
								mLine = mLine + 2
							else
								mLine = mLine + 1
							end if	
						end if
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0
						End if          
						if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
							curvan = vrecarray(5,i)
							curcno = vrecarray(0,i)
							i=clng(i)-1
							if mFontSize = 14 Then
								mLine = mLine +4           
							else
								mLine = mLine +3           
							end if	
						
							if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then
								mPage = mPage + 1
								mLine = 0	
							else
								if curvan <> vrecarray(5,i)   Then										
									mPage = mPage + 1
									mLine = 0
								End if
							End if
							if i < ubound(vrecarray,2) then
							end if            
							curpage = curpage + 1
							currec = 0
							totqty=0
						end if
					next
	    
				else
					mPage = 1
				end if
				i = 0
if isarray(vrecarray) then
%>

<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: <%=mFontSize%>pt">
  <tr>
    <td width="100%">
    <center>
	<b><font size="3">PACKING SHEET - <%=dtype%></font></b><br>
	<b><font size="2">(Stanmore English, Stanmore French, Park Royal, Flour Station, Baker & Spice (Denyer Street), GAILS, Rinkoff, Sally Clarks, Simply Bread, Bagel Nash, Stanmore, Stanmore Goods, Bread Etc)</font></b><br>
	<b><font size="2">(divided to Van and Customers)<br></font></b>
	</center>
      <table border="0" width="100%" style="font-family: Verdana; font-size: <%=mFontSize%>pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2">
            <br>
			<b><font size="2">Group Name: <%=strGname%></font></b><br><br>
            <b>Date of report:</b> <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            <b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><BR>
           
            </td>
        </tr>
        <tr>
          <td colspan="2">Number of pages for this report: <%=mPage%></td>
          <td align="right">Page 1 of <%=mPage%><br></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
</center>
</div><%
if mFontSize = 14 Then
	mLine = 16
Else
	mLine = 8
End if%>

<div align="center">
<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
	  if isarray(vrecarray) then
		mTotalPage = mPage
		mPage = 1		
	    currec=0
	    totqty=0
	    curcno = vrecarray(0,0)
	    curvan = vrecarray(5,0)
	    curpage=1	  
        for i = 0 to ubound(vRecArray,2)
          currec = clng(currec) + 1
          if clng(currec) = 1 then%>
            <tr>
            <td colspan="4">
              <b>Van : <%=vrecarray(5,i)%></b><br>
              <b>Customer : <%=vrecarray(0,i)%> - <%=vrecarray(1,i)%> (<%=vrecarray(7,i)%>)</b>
            </td>            
            </tr>
            <tr>            
            <td align="left" bgcolor="#CCCCCC" height="20" width="25%"><b>Facility</b></td> 
			<td align="left" bgcolor="#CCCCCC" height="20" width="60%"><b>Product name</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Qty</b></td>
            <td align="center" bgcolor="#CCCCCC" height="20" width="5%"><b>SID</b></td>
            </tr><%
            if mFontSize = 14 Then
							mLine = mline + 4
						else
							mLine = mline + 3
						end if	
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td colspan="4" align="right"><%
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
						End if
           end if
           if curvan = vrecarray(5,i) and curcno = vrecarray(0,i) then%>
          <tr>            
            <td align="left" width="25%"><%=vrecarray(8,i)%>&nbsp;</td>
			<td align="left" width="60%"><%=left(vrecarray(3,i),50)%>&nbsp;</td>
            <td align="center" width="10%"><%=vrecarray(4,i)%>&nbsp;</td>
			<%
            totqty = clng(totqty) + vrecarray(4,i)
			%>
            <td align="center" width="5%">&nbsp;</td>
          </tr><%
						if mFontSize = 14 Then
							mLine = mLine + 2
						else
							mLine = mLine + 1
						end if	
          end if
          if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
						mPage = mPage + 1						
						mLine = 0%>		
						</table>						
						<br class="page" />						
						<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center">
							<tr>
								<td colspan="4" align="right"><%	
								 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
							</tr><%
          End if
          
		  if curvan <> vrecarray(5,i) or curcno <> vrecarray(0,i) then
		    curvan = vrecarray(5,i)
		    curcno = vrecarray(0,i)
		    i=clng(i)-1%>
		    </table>
						<br>
            <br><%
            if mFontSize = 14 Then
							mLine = mLine +4           
						else
							mLine = mLine +3           
						end if	
         if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then 						
         
						mPage = mPage + 1
						mLine = 0%>								
						<br class="page" /><%						
		  else
			if curvan <> vrecarray(5,i)   Then										
				mPage = mPage + 1
				mLine = 0%>													
				<br class="page" /><%
			End if				
          End if
          
            if i < ubound(vrecarray,2) then%>
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
							if mLine = 0 Then %>
								<tr>
									<td colspan="4" align="right"><%
									 Response.Write "Page "  & mPage & " of " & mTotalPage%> &nbsp;</td>
								</tr><%						
							end if         
            end if   
            
		    curpage = curpage + 1
		    currec = 0
		    totqty=0
		  end if
	    next%>
	    <%else%>
        <tr>
        <td width="100%" colspan = "4" height="20">Sorry no items found</td>
        </tr><%
	  end if%>
      </table>
</div>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%end if%>
<%
next
%>
<%
Response.Flush()
next
set col1= nothing

%>
<!-- New Report End -->


<%
set object = nothing
%>
</body>
</html>