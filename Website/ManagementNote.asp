<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title>Customer Note</title>

 <script language="javascript" type="text/javascript">

    function onselect_MessageType() {
        var radioButtons = document.getElementsByName('MessageType');
        for (var x = 0; x < radioButtons.length; x++) {

            if (radioButtons[x].checked) {
                if (radioButtons[x].value == "3") {
                    document.getElementById('trGroup').style.display = "block";
                }
                else {
                    document.getElementById('trGroup').style.display = "none";
                }
            }
        }
    }


    function onClick_Select() {

        var selectedValue = '';
        var selObj = document.getElementById('ddlAllGroup');
        var i;

        for (i = 0; i < selObj.options.length; i++) {
            if (selObj.options[i].selected) {
                if (selectedValue != '')
                    selectedValue = selectedValue + ',' + selObj.options[i].value;
                else
                    selectedValue = selObj.options[i].value;
            }
        }

        var PrvselObj = document.getElementById('ddlSelectedGroup');

        for (i = 0; i < PrvselObj.options.length; i++) {
            if (selectedValue != '')
                selectedValue = selectedValue + ',' + PrvselObj.options[i].value;
            else
                selectedValue = PrvselObj.options[i].value;

        }

        document.getElementById('selectedGroup').value = selectedValue;
        document.getElementById('Action').value = "Select";
        //alert(document.getElementById('selectedGroup').value);
        document.frmMgtNote.submit();
    }

    function onClick_UnSelect() {

        var UnSelectedValue = '';
        var selObj = document.getElementById('ddlSelectedGroup');
        var i;
        for (i = 0; i < selObj.options.length; i++) {
            if (!selObj.options[i].selected) {
                if (UnSelectedValue != '')
                    UnSelectedValue = UnSelectedValue + ',' + selObj.options[i].value;
                else
                    UnSelectedValue = selObj.options[i].value;
            }
        }
        document.getElementById('UnSelectedGroup').value = UnSelectedValue;
        //alert(document.getElementById('UnSelectedGroup').value);
        document.getElementById('Action').value = "Unselect";
        document.frmMgtNote.submit();
    }

    function onClick_Save() {

        if (Validate()) {
            document.getElementById('Action').value = "Save";
            document.frmMgtNote.submit();
        }

    }

    function Validate() {
        if (document.getElementById('txtNotes').value == "") {
            alert('Please enter Message');
            document.getElementById('txtNotes').focus();
            return false;
        }

        //check group value selected or not
        var isValid;
        var radioButtons = document.getElementsByName('MessageType');
        for (var x = 0; x < radioButtons.length; x++) {

            if (radioButtons[x].checked) {
                if (radioButtons[x].value == "3") {
                    var selObj = document.getElementById('ddlSelectedGroup');
                    var selectGroupID = '';
                    if (selObj.options.length > 0) {

                        //Set the selected group value
                        var i;
                        for (i = 0; i < selObj.options.length; i++) {
                            if (selectGroupID != '')
                                selectGroupID = selectGroupID + ',' + selObj.options[i].value;
                            else
                                selectGroupID = selObj.options[i].value;
                        }

                        document.getElementById('SelectedGroupID').value = selectGroupID;
                        isValid = true;
                    }
                    else {
                        alert('Please select Group');
                        isValid = false;
                    }
                }
                else {
                    isValid = true;
                }
            }
        }

        if (isValid)
            return true;
        else
            return false;


    }

</script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<form method="post" name="frmMgtNote" action="ManagementNote.asp" >
<%
stop
Dim arGroupSelect, arGroupUnSelect
Dim sSelectedGroup, sUnSelectedGroup, nOptionValue, sAction, sGroupIDList,sNote,sPageLoad
Dim sMsg
Set obj = CreateObject("Bakery.Customer")
obj.SetEnvironment(strconnection)
sGroupIDList = 0
nOptionValue = 1
sAction = Request.Form("Action")
sSelectedGroup = Request.Form("selectedGroup")
sUnSelectedGroup = Request.Form("UnSelectedGroup")
nOptionValue = Request.Form("MessageType")
sNote = Request.Form("txtNotes")


if sAction = "Save" Then

    Call SaveMessage(sNote,nOptionValue)
    nOptionValue = 1
    sAction = ""
    sSelectedGroup = ""
    sUnSelectedGroup = ""
    sNote = ""

End if

if sSelectedGroup = "" Then
    sSelectedGroup = 0
End If

if sUnSelectedGroup = "" Then
    sUnSelectedGroup = 0
End If

if sAction = "Select" Then
    sGroupIDList = sSelectedGroup
elseif sAction = "Unselect" Then
    sGroupIDList = sUnSelectedGroup
End if

set objCustomer = obj.GetCustomizeGroupList(0, sGroupIDList)
arGroupSelect = objCustomer("GroupList")

set objCustomer = obj.GetCustomizeGroupList(1, sGroupIDList)
arGroupUnSelect = objCustomer("GroupList")


if sAction = "" Then
    Set sObjNote = obj.GetManagementNote()
    arrNote = sObjNote("MessageNote")

    if(IsArray(arrNote)) Then

        sNote = arrNote(1,0)
        nOptionValue = arrNote(2,0)

    End if

End If

%>

<table border="0" align="center" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">

  <tr>
    <td width="100%" align="center">
	<%
	if sMsg = "OK" then
		Response.Write "<font size=""2"" color=""#FF0000""><b>Message Successfully Added.</b></font><br><br>"
	end if
	%>
      <table bgcolor="#CCCCCC" align="center" border="0" width="600" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="4">
         <tr height="30">
          <td colspan="2" align="center"><strong>Customer Note Box</strong></td>
        </tr>
        <tr height="25" bgcolor="#FFFFFF">
          <td align="right" valign="top">Customer Notes:</td>
          <td><textarea name="txtNotes" cols="50" rows="5" style="font-family: Verdana; font-size: 8pt"  ><%=sNote%></textarea></td>
        </tr>
		<tr height="25" bgcolor="#FFFFFF">
          <td align="right">Save the message to:</td>
          <td>
              <input name="MessageType" type="radio" value="1"  <%if nOptionValue = 1 Then %> checked="checked" <%End If%> onClick="onselect_MessageType();" />All Customer<br />
              <input name="MessageType"  type="radio" value="2" <%if nOptionValue = 2 Then %> checked="checked" <%End If%> onClick="onselect_MessageType();" />All customers - Excluding flour station<br />
              <input name="MessageType"  type="radio" value="3" <%if nOptionValue = 3 Then %> checked="checked" <%End If%> onClick="onselect_MessageType();" />Customer Groups
          </td>
        </tr>
		<tr bgcolor="#FFFFFF" id="trGroup" <%if nOptionValue = 3 Then %>  style="display:block;" <%Else%> style="display:none;" <%End if%>>
          <td align="right"></td>
          <td>
          <table cellpadding="0" cellspacing="0" border="0">
          <tr>
          <td>
          <select id="ddlAllGroup" size="10" style="font-family: Verdana; font-size: 8pt; width:175px;" multiple="multiple" >
                <%For i = 0 to UBound(arGroupSelect,2)%>
				    <option value="<%=arGroupSelect(0,i)%>"><%=arGroupSelect(1,i)%></option>
			    <%Next %>
              </select></td>
          <td style="width:100px;" align="center">
          <input type="button" value=" >> " id="btnSelect" style="font-family: Verdana; font-size: 8pt" onClick="onClick_Select();">
          <br /><br /><br />
          <input type="button" value=" << " id="btnUnselect" style="font-family: Verdana; font-size: 8pt" onClick="onClick_UnSelect();"/>
          </td>
          <td><select id="ddlSelectedGroup" size="10" style="font-family: Verdana; font-size: 8pt; width:175px;" multiple="multiple">
                <%
                if arGroupUnSelect <> "Fail" Then
                For i = 0 to UBound(arGroupUnSelect,2)%>
				    <option value="<%=arGroupUnSelect(0,i)%>"><%=arGroupUnSelect(1,i)%></option>
			    <%Next
			    End If
			    %>
              </select></td>
          </tr>
          </table>
             </td>
        </tr>
		        <tr>
          <td></td>
          <td>
          <input type="button" value=" Save " name="B1" style="font-family: Verdana; font-size: 8pt" onClick="onClick_Save();" /></td>
        </tr>
      </table>
    </td>
  </tr>

</table>
<input type="hidden" name="selectedGroup" value="<%=sSelectedGroup%>" />
<input type="hidden" name="UnSelectedGroup" value="<%=sUnSelectedGroup%>" />
<input type="hidden" name="Action" value="" />
<input type="hidden" name="SelectedGroupID" value="" />

</form>
</body>
</html>

<%
Sub SaveMessage(sMessage, nOption)
Dim selectedGroupID,result
Set obj = CreateObject("Bakery.Customer")
obj.SetEnvironment(strconnection)
selectedGroupID = Request.Form("SelectedGroupID")

set result = obj.SaveManagementNote(sNote,nOptionValue,session("UserNo"),selectedGroupID)

if result("Sucess") = "OK" then

    sMsg = "OK"

End If

End Sub
%>
