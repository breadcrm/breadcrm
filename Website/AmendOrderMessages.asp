<%@  language="VBScript" %>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

    <script language="javascript" type="text/javascript">

        function onClick_Save() {
            if (Validate()) {
                document.getElementById('Action').value = "Save";
                document.frmCustMsg.submit();
            }
        }

        function onClick_Update() {
            if (Validate()) {
                document.getElementById('Action').value = "SaveUpdate";
                document.frmCustMsg.submit();
            }
        }

        function Validate() {
            if (document.getElementById('txtMessage').value == "") {
                alert("Please enter message")
                document.getElementById('txtMessage').focus();
                return false;

            }
            else {
                return true;
            }
            
        
        }
          function focusNext(form, elemName, evt) {
            evt = (evt) ? evt : event;
            var charCode = (evt.charCode) ? evt.charCode :
                ((evt.which) ? evt.which : evt.keyCode);
            if (charCode == 13) {
                form.elements[elemName].focus();
                return false;
            }
            return true;
        }

        function DeleteConfirmation(msgID) {

            var result = confirm('Are you sure you want to delete?');
            if (result) {
                document.getElementById('Action').value = "Delete";
                document.getElementById('MsgID').value = msgID;
                document.frmCustMsg.submit();
            }
            else {
                return false;
            }
        }

        function EditCustomerMessage(msgID) {

            document.getElementById('Action').value = "Edit";
            document.getElementById('MsgID').value = msgID;
            document.frmCustMsg.submit();
        
        }
      
    </script>

</head>
<body bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
    <%

    
Dim CustomerNo,sAction, sMessage, cNo, sMsg,sEditMsg
CustomerNo = Request.QueryString("CNo")

sAction = Request.Form("Action")
sMessage = Request.Form("txtMessage")

if sAction = "Save" Then

    Call SaveMessage(CustomerNo,sMessage)
    
    
End if

if sAction = "Delete" Then
    nMsgID  = Request.Form("MsgID")
    Call DeleteMessage(nMsgID)
End if


if sAction = "Edit" Then

    nMsgID  = Request.Form("MsgID")
    sEditMsg = GetCustomerMessageByMsgID(nMsgID)
    
End if

if sAction = "SaveUpdate" Then

     nMsgID  = Request.Form("MsgID")
    Call UpdateMessage(nMsgID,sMessage)
    
    
End if

    %>
    <form method="post" name="frmCustMsg" action="AmendOrderMessages.asp?CNo=<%=CustomerNo%>">
    <table border="0" align="center" width="100%" cellspacing="0" cellpadding="0" style="font-family: Verdana;
        font-size: 8pt">
        <tr height="25" bgcolor="#FFFFFF">
            <td align="right" valign="top">
                Message:&nbsp;
            </td>
            <td>
                <textarea name="txtNotes" id="txtNotes" cols="60" rows="3" style="font-family: Verdana; font-size: 8pt;
                    color: Red; font-weight: bold; height:40px; width:350px"><%=GetmanagementNoteBuCustNo(CustomerNo)%> 
                </textarea>
            </td>
        </tr>
    </table>
    <br />
    
   
       
            <table bgcolor="#666666" align="center" border="0" width="100%" style="font-family: Verdana;
                font-size: 8pt" cellspacing="1" cellpadding="2">
                <%Call CreateCustomerMessageList (CustomerNo)  %>
            </table>
       <br>
        
            <table bgcolor="#CCCCCC" border="0" width="100%" style="font-family: Verdana; font-size: 8pt"
                cellspacing="1" cellpadding="2">
                <tr height="40" bgcolor="#FFFFFF">
                    <td valign="middle" colspan="3" align="left">
                        <strong>Message :</strong>&nbsp;
                        <input type="text" name="txtMessage" id="txtMessage" style="width:450px;" value="<%=sEditMsg%>"   onkeypress="<%if sAction = "Edit" Then  %>return focusNext(this.form, 'btnUpdate', event)<%Else %>return focusNext(this.form, 'btnAdd', event)<%End If %>" />
                    </td>
                </tr>
                <tr bgcolor="#FFFFFF">
                    <td colspan="3">
                    <%if sAction = "Edit" Then  %>
                    <input type="button" value=" Update Message " name="btnUpdate" id="btnUpdate" style="font-family: Verdana;
                            font-size: 8pt" onClick="onClick_Update();" />
                    <%Else %>
                        <input type="button" value=" Add Message " name="btnAdd" id="btnAdd" style="font-family: Verdana;
                            font-size: 8pt" onClick="onClick_Save();" />
                    <%End If %>
                    </td>
                </tr>
            </table>
      

    <input type="hidden" name="Action" id="Action" value="" />
     <input type="hidden" name="MsgID" id="MsgID" value="<%=nMsgID%>" />
    </form>
</body>
</html>
<%
Function GetmanagementNoteBuCustNo(CustomerNo)

Dim MgtMsg
Set obj = CreateObject("Bakery.Customer")
obj.SetEnvironment(strconnection)
set objMessage = obj.GetCustomerManagementMessageByCNo(CustomerNo)
MgtMsg = objMessage("MgtMessage")

If arGroupSelect <> "Fail" Then

    GetmanagementNoteBuCustNo = MgtMsg(1,0)
Else

    GetmanagementNoteBuCustNo = Empty

End if

End Function


Sub SaveMessage(custID,sMessage)
Dim result
Set obj = CreateObject("Bakery.Customer")
obj.SetEnvironment(strconnection)
set result = obj.SaveCustomerMessage(custID,sMessage,session("UserNo"))

if result("Sucess") = "OK" then

    sMsg = "Message Successfully Added"

End If

End Sub

Sub CreateCustomerMessageList(custNo)
Dim sCustMsg
Set obj = CreateObject("Bakery.Customer")
obj.SetEnvironment(strconnection)
set objCustomerMsg = obj.GetCustomerMessageByCNo(custNo)
sCustMsg = objCustomerMsg("CustMessage")


if sMsg <> "" Then %>
<tr height="25" bgcolor="#FFFFFF">
    <td colspan="5" align="center" style="color:#006600;">
        <strong><%=sMsg%></strong>
    </td>
</tr>
<%End If 

if isArray(sCustMsg) Then
%>

<tr height="30" bgcolor="#CCCCCC">
    <td style="width: 350px;">&nbsp;<strong>Message</strong></td>
    <td style="width: 100px;">&nbsp;<strong>Date</strong></td>
    <td style="width: 100px;">&nbsp;<strong>User</strong></td>
    <%if session("UserType") = "A" or session("UserType") = "S" then%>
    <td align="center">
        <strong>Action</strong>
    </td>
	<%End if%>    
</tr>
<%

 For i = 0 to UBound(sCustMsg,2)
%>
<tr height="25" bgcolor="#FFFFFF">
    <td valign="top" style="padding-left: 5px; width: 250px; word-wrap: break-word;">
        <%=sCustMsg(0,i) %>
    </td>
    <td style="padding-left: 5px; width: 100px;">
        <%=FormatDateInDDMMYYYY(sCustMsg(1,i)) %>
    </td>
    <td style="padding-left: 5px; width: 100px;">
        <%=sCustMsg(2,i) %>
    </td>
    <%if session("UserType") = "A" or session("UserType") = "S" then%>
    <td>
	<nobr>&nbsp;<input type="button" value="Edit" name="Edit" onClick="javascript:EditCustomerMessage('<%=sCustMsg(3,i) %>');" style="font-family: Verdana; font-size: 8pt; width:50px" />
	
    <input type="button" value="Delete" name="Delete" onClick="javascript:DeleteConfirmation('<%=sCustMsg(3,i) %>');" style="font-family: Verdana; font-size: 8pt;  width:50px" />&nbsp;</nobr>
	</td>
	<%End if%>

</tr>
<%
Next
Else
%>
<tr height="25" bgcolor="#FFFFFF">
    <td colspan="5" align="center">
        There are no Messages
    </td>
</tr>
<%

End If
%>
<%
End Sub

Sub DeleteMessage(msgID)
Dim result
Set obj = CreateObject("Bakery.Customer")
obj.SetEnvironment(strconnection)
set result = obj.DeleteCustomerMessageByID(msgID, session("UserNo"))

if result("Sucess") = "OK" then

    sMsg = "Message Deleteted Successfully"

End If

End Sub


Function GetCustomerMessageByMsgID(nMsgID)

Dim custMsg
Set obj = CreateObject("Bakery.Customer")
obj.SetEnvironment(strconnection)
set objMessage = obj.GetCustomerMessageByMsgID(nMsgID)
custMsg = objMessage("CustMessage")

If IsArray(custMsg) Then

    GetCustomerMessageByMsgID = custMsg(2,0)
Else

    GetCustomerMessageByMsgID = Empty

End if

End Function


Sub UpdateMessage(nMsgID,sMessage)
Dim result
Set obj = CreateObject("Bakery.Customer")
obj.SetEnvironment(strconnection)
set result = obj.UpdateCustomerMessageByMsgID(nMsgID,sMessage,session("UserNo"))

if result("Sucess") = "OK" then

    sMsg = "Message Successfully Updated"

End If

End Sub
%>
