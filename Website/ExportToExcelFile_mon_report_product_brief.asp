<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
	fromdt=request("fromdt")
	todt=request("todt")
	
	strStatus=request("Status")
	if strStatus="A" then
		strStatusLabel="Active Products"
	elseif  strStatus="D" then
		strStatusLabel="Deleted Products"
	else
		strStatusLabel=""
	end if
	if strStatus="0" then
		strStatus=""
	elseif strStatus="" then
		strStatus="A"
	end if
	if isdate(fromdt) and isdate(todt) then
		set objBakery = server.CreateObject("Bakery.reports")
		objBakery.SetEnvironment(strconnection)
		set retcol = objBakery.Display_product_brief(fromdt,todt,strStatus)
		recarray = retcol("Product")
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=mon_report_product_brief.xls" 
	%>
		<style>
			.text{
				mso-number-format:"\@";/*force text*/
			}
		</style>
	    <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%"  colspan="6"><b>Product Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; <% if strStatus<>"" then%> - <%=strStatusLabel%><%end if%></b></td>
        </tr>
        <tr>
          <td width="15%" bgcolor="#CCCCCC"><b>Product Code&nbsp;</b></td>
          <td width="45%" bgcolor="#CCCCCC"><b>Product Name&nbsp;</b></td>
          <td width="10%" bgcolor="#CCCCCC"><b>Number of Units Sold&nbsp;</b></td>
          <td width="10%" bgcolor="#CCCCCC" align="right"><b>Price A&nbsp;</b></td>
          <td width="10%" bgcolor="#CCCCCC" align="right"><b>Average Price Sold&nbsp;</b></td>
          <td width="10%" bgcolor="#CCCCCC" align="right"><b>Total Turnover&nbsp;</b></td>  
        </tr>
		<tr>
		<%
		if isarray(recarray) then%>
		<%
		UnitsSold = 0
		Price_A = 0.00
		AveragePrice = 0.00
		TotalTurnover = 0.00
		%>
		<%for i=0 to UBound(recarray,2)%>
		<tr>
          <td class="text"><b><%=recarray(0,i)%></b></td>
          <td><b><%=recarray(1,i)%></b></td>
          <td align="right" bgcolor="#CCCCCC"><b><%=recarray(2,i)%></b></td>
          <td align="right"><b><%if recarray(3,i)<> "" then Response.Write formatnumber(recarray(3,i),2) else Response.Write "0.00" end if%></b></td>
          <td align="right" bgcolor="#CCCCCC"><b><%if recarray(4,i)<> "" then Response.Write formatnumber(recarray(4,i),2) else Response.Write "0.00" end if%></b></td>
          <td align="right"><b><%if recarray(5,i)<> "" then Response.Write formatnumber(recarray(5,i),2) else Response.Write "0.00" end if%></b></td>
        </tr>
          <%
          if recarray(2,i) <> "" then
          UnitsSold = UnitsSold + recarray(2,i)
          End if 
          if recarray(3,i)<> "" then 
					Price_A = Price_A + formatnumber(recarray(3,i),2)
					End if
					if recarray(4,i)<> "" then
					AveragePrice = AveragePrice + formatnumber(recarray(4,i),2)
					End if
					If recarray(5,i)<> "" then 
					TotalTurnover = TotalTurnover + formatnumber(recarray(5,i),2)
					End if
         %>
         <%next%>
         <tr>
          <td colspan=2><b>&nbsp;&nbsp;Grand Total</b></td>
          <td align="right" bgcolor="#CCCCCC"><b><%=UnitsSold%></b></td>
          <td align="right"><b><%=formatnumber(Price_A,2)%></b></td>
          <td align="right" bgcolor="#CCCCCC"><b><%=formatnumber(AveragePrice,2)%></b></td>
          <td align="right"><b><%=formatnumber(TotalTurnover,2)%></b></td>
          </tr>
          <%
          else%>
          <td colspan="3" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td><%
          end if%>
        </tr>

      </table>
<%end if%>
</body>
</html>
