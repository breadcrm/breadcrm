<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
vSno = request.querystring("sno")
vIno = request.querystring("ino")
vqty = request.querystring("qty")
vprice = request.querystring("price")
voften = request.querystring("often")
vFaciName = request.querystring("FaciName")

if Trim(vFaciName) <> "" Then
	arSplit = Split(vFaciName,",") 
End if

Set object = Server.CreateObject("bakery.supplier")
object.SetEnvironment(strconnection)
set detail = object.DisplaySupIngFac(vSno,vIno)	
vFacarray =  detail("SupIngFac") 
Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayIng()	
vingarray =  detail("Ingredients") 
set detail = Nothing
set object = Nothing
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>

<script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{

  <%if  vIno="" then%>

  if (theForm.ino.selectedIndex < 0)
  {
    alert("Please select one of the \"ino\" options.");
    theForm.ino.focus();
    return (false);
  }

  if (theForm.ino.selectedIndex == 0)
  {
    alert("The first \"Ingredient\" option is not a valid selection.  Please choose one of the other options.");
    theForm.ino.focus();
    return (false);
  }

  <%end if%>

  if (theForm.qty.value == ""){
    alert("Please enter a value for the \"quantity\" field.");
    theForm.qty.focus();
    return (false);}
  
  if(isNaN(theForm.qty.value)){
			alert("Invalid qty");
		  theForm.qty.focus();
			return (false);}
  
  if(theForm.qty.value<=0){
			alert("Entered qty should be grater than zero");
		  theForm.qty.focus();
			return (false);} 
  
  

  if (theForm.price.value == "")
  {
    alert("Please enter a value for the \"price\" field.");
    theForm.price.focus();
    return (false);
  }
  
  if(isNaN(theForm.price.value)){
			alert("Invalid price");
		  theForm.price.focus();
			return (false);}
  
  if(theForm.price.value<=0){
			alert("Entered price should be grater than zero");
		  theForm.price.focus();
			return (false);}  
  

  if (theForm.often.selectedIndex < 0)
  {
    alert("Please select one of the \"how often\" options.");
    theForm.often.focus();
    return (false);
  }

  if (theForm.often.selectedIndex == 0)
  {
    alert("The first \"how often\" option is not a valid selection.  Please choose one of the other options.");
    theForm.often.focus();
    return (false);
  }

  if (theForm.facility.selectedIndex < 0)
  {
    alert("Please select one of the \"Facility\" options.");
    theForm.facility.focus();
    return (false);
  }

  var numSelected = 0;
  var i;
  for (i = 0;  i < theForm.facility.length;  i++)
  {
    if (theForm.facility.options[i].selected)
        numSelected++;
  }
  if (numSelected < 1)
  {
    alert("Please select at least 1 of the \"Facility\" options.");
    theForm.facility.focus();
    return (false);
  }
  
  return (true);
}
//--></script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">

<form method="POST" action="save_supplierproducts.asp" onSubmit="return FrontPage_Form1_Validator(this)" name="FrontPage_Form1">

<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3"><br>
    <%if  vIno="" then%>Add<%Else%>Update<%End if%> products supplied by <%=request.querystring("name")%><br>
      &nbsp;</font></b>
      
      <table border="0" width="70%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2" height="150">
        <input type = "hidden" name="sno" value = "<%=vSno%>">
         <tr>
          <td bgcolor="#E1E1E1" colspan="2" height="13"><b>Ingredient&nbsp; bought from
            supplier</b></td>
        </tr>
               
        </tr>
        <tr>
          <td height="22">Ingredient</td>
          <td height="22">

		  <%if  vIno<>"" then%>
		  <%for i = 0 to ubound(vingarray,2)%>  				
          <%if vIno = trim(vingarray(0,i)) then response.write vingarray(2,i)%>
          <%next%> 		  
		  <input type ="hidden" name = "ino" value="<%=vIno%>">
		 <%else%>
          <select size="1" name="ino">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>" <%if vIno = trim(vingarray(0,i)) then response.write "Selected"%>><%=vingarray(2,i)%></option>
                  <%next%> 
          </select>
          <%end if%>
          </td>
        </tr>
        <tr>
          <td height="15">Unit Quantity</td>
          <td height="15">
          <input type="text" name="qty" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6" value="<%=vqty%>"></td>
        </tr>
        <tr>
          <td height="19">Unit Price</td>
          <td height="19">
          <input type="text" name="price" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6" value="<%=vprice%>"></td>
        </tr>
        <tr>
          <td height="22">How Often is it being ordered</td>
          <td height="22">
          <select size="1" name="often">
          <option>Select</option>
          <option value="D" <%if voften = "Daily" then response.write "Selected"%>>Daily</option>
          <option value="W" <%if voften = "Weekly" then response.write "Selected"%>>Weekly</option>
          <option value="M" <%if voften = "Monthly" then response.write "Selected"%>>Monthly</option>
          <option value="Q" <%if voften = "3 Months" then response.write "Selected"%>>Quarterly</option>
          <option value="Y" <%if voften = "Yearly" then response.write "Selected"%>>Yearly</option>
          </select></td>
        </tr>
        <tr>
          <td>Used by Facility</td>
          <td>
          <select size="5" name="facility" style="font-family: Verdana; font-size: 8pt" multiple>
				  <%for i = 0 to ubound(vfacarray,2)
							if vfacarray(0,i) = "15" or vfacarray(0,i) = "18" or vfacarray(0,i) = "90" then
								mTag = false
								if IsArray(arSplit) then
									For xm = 0 to ubound(arSplit)
										if Trim(vfacarray(1,i)) = Trim(arsplit(xm)) Then
											mTag  = True
										End if 
									Next
						    End if%>           
						    
									<option value="<%=vfacarray(0,i)%>" <%if mTag then Response.Write "Selected"%>><%=vfacarray(1,i)%></option><%
              end if                 
            next%>                   
            </select></td>
        </tr>
        <tr>
          <td height="13"></td>
          <td height="13"></td>
        </tr>
        <tr>
          <td height="21"></td>
          <td height="21">
          <input type="submit" value="<%if  vIno="" then%>Add<%Else%>Update<%End if%> Ingredient" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
      </table>


  </center>
    </td>
  </tr>
</table>


</div>

</form>

</body>
</html>