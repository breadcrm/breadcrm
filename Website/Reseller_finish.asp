<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#include file="Includes/Loader.asp"-->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%

on error resume next
Response.Buffer = True
   ' load object
   Dim load
      Set load = new Loader
   ' calling initialize method
   load.initialize

   ' File binary data
   Dim fileData
      fileData = load.getFileData("file")
   ' File name
   Dim fileName
      fileName = LCase(load.getFileName("file"))
   ' File path
   Dim filePath
      filePath = load.getFilePath("file")
   ' File path complete
   Dim filePathComplete
      filePathComplete = load.getFilePathComplete("file")
   ' File size
   Dim fileSize
      fileSize = load.getFileSize("file")
   ' File size translated
   Dim fileSizeTranslated
      fileSizeTranslated = load.getFileSizeTranslated("file")
   ' Content Type
   Dim contentType
      contentType = load.getContentType("file")
   ' No. of Form elements
   Dim countElements
      countElements = load.Count
   ' Value of text input field "name"
   
	Dim pathToFile,pathToFileOld
	strLogo1=replace(trim(load.getValue("file1")),"'","''")
	pathToFile = Server.mapPath("images/ResellerLogos/") & "\" & fileName
	pathToFileOld = Server.mapPath("images/ResellerLogos/") & "\" & strLogo1
		
	strResellerName = replace(trim(load.getValue("ResellerName")),"'","''")
	strAddress = replace(trim(load.getValue("Address")),"'","''")
	
	strTown = replace(trim(load.getValue("Town")),"'","''")
	strPostcode = replace(trim(load.getValue("Postcode")),"'","''")
	strTelephone = replace(trim(load.getValue("Telephone")),"'","''")
	strFax = replace(trim(load.getValue("Fax")),"'","''")
	
	strPriceBand = replace(trim(load.getValue("PriceBand")),"'","''")
	strPriceBandOld = replace(trim(load.getValue("PriceBandOld")),"'","''")
	
	strInvoiceFooter = replace(trim(load.getValue("InvoiceFooter")),"'","''")
	strVATRegNo = replace(trim(load.getValue("VATRegNo")),"'","''")
	strNotes = replace(trim(load.getValue("Notes")),"'","''")
	
	strOldCode = replace(trim(load.getValue("OldCode")),"'","''")
	
	vRID = replace(trim(load.getValue("RID")),"'","''")
	strFactor = replace(trim(load.getValue("Factor")),"'","''")
	if strFactor="" or not isnumeric(strFactor) then
		strFactor=0
	end if
	
	if(strPriceBand<>"6" AND strPriceBand<>"18") then
		strFactor=0
	end if
	strLogo=fileName
set object = Server.CreateObject("bakery.Reseller")
object.SetEnvironment(strconnection)
if vRID="" then
	set SaveReseller= object.SaveReseller(strResellerName, strAddress, strTown, strPostcode, strTelephone, strFax, strPriceBand, strInvoiceFooter, strVATRegNo, strNotes, strLogo, strOldCode,strFactor)
	if SaveReseller("Sucess")<>"OK" then
		Save = SaveReseller("Sucess")
	else
		LogAction "Reseller Added", "Name: " & strResellerName , ""
		Save = SaveReseller("Sucess")
		vNewRID= SaveReseller("RID")
	end if
	set SaveReseller =  nothing

else
	
	if strLogo<>"" and strLogo1<>"" then
		Dim fso	
		Set fso = Server.CreateObject("Scripting.FileSystemObject")		
		On Error Resume Next		
			Call fso.DeleteFile(pathToFileOld, True)	
		Set fso = Nothing
	end if
	
	if strLogo="" then
		strLogo=strLogo1
	end if
	' commented by mohanadas customer price should not get updated when changing the reseller price, this is instructed by dean on 20/07/2012
	'if (strPriceBandOld<>strPriceBand) then
	'	UpdateCustomerPriceBand=object.UpdateCustomerPriceBand(vRID,strPriceBand)
	'end if
	
	UpdateReseller= object.UpdateReseller(vRID,strResellerName, strAddress, strTown, strPostcode, strTelephone, strFax, strPriceBand, strInvoiceFooter, strVATRegNo, strNotes,strLogo, strOldCode,strFactor)
	LogAction "Reseller Edited", "Name: " & strResellerName , ""
end if
'Uploading file data
fileUploaded = load.saveToFile ("file", pathToFile)
Set load = Nothing
set object = nothing

%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3"><%if vRID = "" then%>New Reseller<%else%>Existing Reseller<%end if%><br>
      &nbsp;</font></b>
      
      <table align="center" border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
      		<%
			if LCase(UpdateReseller) = "ok" or LCase(Save) = "ok" Then
			%>
        <tr>
          <td colspan="2"><b>Reseller Name:&nbsp;<%=strResellerName%></b></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td></td>
        </tr>
        <tr>
          <td colspan="2"><b><%if vRID = "" then%>Has been created successfully<%else%>Has been updated successfully<%end if%></b></td>
        </tr><%
       Else%>
       <tr>
          <td colspan="2" align="center"><b>
		   <%if err.number <> 0 then%>
		  An error occured please try again
		   <%else%>            
          This Reseller already exist, Please use the back button and modify the Reseller again
          <p align="center"><input type="button" value=" Back " onClick="history.back()"></p>
		  <%end if%>            
          </b></td>
        </tr><%
       
       
       End if%> 
       <tr>
          <td colspan="2" height="45" align="center" valign="bottom"><b><a href="ResellerList.asp"><font color="#003366">Go to Reseller List</font></a></b></td>
       </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>

    </td>
  </tr>
</table>


  </center>
</div>


</body>

</html>