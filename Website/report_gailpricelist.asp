<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim recarray
dim retcol
dim totpages
dim vSessionpage
dim i
dim spaces
dim pno, pname
spaces = string(20," ")
pno = Request.Form("txtpno")
pname = Request.Form("txtpname")

totpages = Request.Form("pagecount")
select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(totpages) then	
			session("Currentpage")  = session("Currentpage") + 1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage") - 1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select
vSessionpage = session("Currentpage")
'Response.Write "pno = " & pno & "<br>"
'Response.Write "pname = " & pname & "<br>"
'Response.Write "vsessionpage = " & vsessionpage & "<br>"
'Response.Write "pagesize = " & pagesize & "<br>"
set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.GailPriceList(pno,pname,vSessionPage,0)
recarray = retcol("GailPrices")
totpages = retcol("pagecount")
%>
<div align="center">
  <center>
<form name="frmsearch" action="report_gailpricelist.asp" method="post">
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Report - Gail Force Price List<br>
      &nbsp;</font></b>
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          <td><b>Product code:</b></td>
          <td><b>Product name:</b></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><input type="text" name="txtpno" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td><input type="text" name="txtpname" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td>
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>
  </center>
</div>
<br>
<div align="center">
  <center>
      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><b><font size="2">Full Price List</font></b></td><%
          if isarray(recarray) then%>
          <td><font size="2"><b>Page <%=vsessionpage%> of <%=totpages%></b></font></td><%
          else%>
          <td>&nbsp;</td><%
          end if%>
        </tr>
        <tr>
          <td bgcolor="#CCCCCC"><b>Product Code</b></td>
          <td bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td bgcolor="#CCCCCC"><b>Price A</b></td>
        </tr><%
        if isarray(recarray) then
          for i=0 to ubound(recarray,2)%>
          <tr>
          <td><%=recarray(0,i)%></td>
          <td><%=recarray(1,i)%></td>
          <td>� <%=formatnumber(recarray(2,i),2)%></td>
          </tr><%
          next
        else%>
        <tr>
          <td colspan="3"><b>No Records matched...</b></td>
        </tr><%
        end if%>
      </table>
  </center>
</div>
<br>
<div align="center">
  <table width="720" border="0" cellspacing="1" cellpadding="2" align="center">
  <tr> 
  <% if clng(vSessionpage) <> 1 then %>
  <td>
    <form name="frmFirstPage" action="report_gailpricelist.asp" method="post">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="hidden" name="Direction" value="First">
    <input type="submit" name="submit" value="First Page">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) < clng(totpages) then %>
  <td>
    <form name="frmNextPage" action="report_gailpricelist.asp" method="post">
    <input type="hidden" name="Direction" value="Next">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Next Page">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) > 1 then %>
  <td>
    <form name="frmPreviousPage" action="report_gailpricelist.asp" method="post">
    <input type="hidden" name="Direction" value="Previous">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Previous Page">
    </form>
  </td>
  <%end if%>
  <%if clng(vSessionPage) <> clng(totpages) and clng(totpages) > 0 then %>
  <td>
    <form name="frmLastPage" action="report_gailpricelist.asp" method="post">
    <input type="hidden" name="Direction" value="Last">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Last Page">
    </form>
  </td>
  <% end if%>
  </tr>
  </table>
</div>
</body>
</html>