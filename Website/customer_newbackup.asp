<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim obj
Dim arGroup
Dim objCustomer
Dim arCustomer
Dim strName,strOldCode,strAdd1,strAdd2
DIm strTown,strPostcode,strTele,strType,strOrderType
Dim strOtherType,strSYear,strOriCustomer,strPayTerm
Dim strDayContactName,strDayContactTele,strMakerName
Dim strMakerTele,strPrice,strWeekVan,strWeekendVan
Dim strGroup,strStandingOrder,strPurchaseOrder,strStatus
Dim i
Dim vCustNo
Dim strRecordExistTag
Dim strTypeTag
Dim objResult
dim delete


Set obj = CreateObject("Bakery.Customer")
if Trim(Request.Form("Update")) <> "" Then
	vCustNo = Request.Form("CustNo") 

	if request.form("type")= "Delete" then
		delete = obj.DeleteCustomer(vCustNo )
		response.redirect "customer_find.asp"
	end if
	
	if request.form("type")= "Stop" then
		delete = obj.StopActivateCustomer(vCustNo,"S")
		response.redirect "customer_find.asp"
	end if
	
	if request.form("type")= "Active" then
		delete = obj.StopActivateCustomer(vCustNo,"A")
		response.redirect "customer_find.asp"
	end if

	strName = Request.Form("txtName")  
	strOldCode  = Request.Form("txtOldCode")  
	strAdd1  = Request.Form("txtAdd1")  
	strAdd2 =   Request.Form("txtAdd2")  
	strTown = Request.Form("txtTown")  
	strPostcode = Request.Form("txtPostcode")  
	strTele = Request.Form("txtTele")	
	
	if Trim(Request.Form("selType")) <> "" Then    
		strType = Request.Form("selType")  
	Else
		strType = Request.Form("txtOtherType")  
	End if			
	
	strSYear = Request.Form("txtSYear")  
	strOriCustomer = Request.Form("selOriCustomer")  
	strPayTerm = Request.Form("selPayTerm")  
	strDayContactName = Request.Form("txtDayContactName")  
	strDayContactTele = Request.Form("txtDayContactTele")  
	strMakerName = Request.Form("txtMakerName")  
	strMakerTele = Request.Form("txtMakerTele")  
	strPrice = Request.Form("selPrice")  
	strWeekVan = Request.Form("selWeekVan")  
	strWeekendVan = Request.Form("selWeekendVan")  
	strGroup = Request.Form("selGroup")  	
	strPurchaseOrder = Request.Form("selPurchaseOrder")  	
	strRecordExistTag = Request.Form("RecordExistTag") 
	strStandingOrder = Request.Form("selStatdingOrd")  
	strOrderType = Request.Form("selOrderType")

	if Request.Form("selStatus") <> "" Then
		strStatus = Request.Form("selStatus") 
	Else
		strStatus = "A"
	End if

	'Response.Write vCustNo & "," & strName & "," & strOldCode & "," & strAdd1 & "," & strAdd2 & "," & strTown & "," & strPostcode & "," & strTele & "," & strType & "," & strSYear & "," & strOriCustomer & "," & strPayTerm & "," & strDayContactName & "," & strDayContactTele & "," & strMakerName & "," & strMakerTele & "," & strPrice & "," & strWeekVan & "," & strWeekendVan & "," & strGroup & "," & strPurchaseOrder & "," & strStatus & "," & strRecordExistTag	
	 set objCustomer = obj.Save_Customer(vCustNo,strName,strOldCode,strAdd1,strAdd2,strTown,strPostcode,strTele,strType,strSYear,strOriCustomer,strPayTerm,strDayContactName,strDayContactTele,strMakerName,strMakerTele,strPrice,strWeekVan,strWeekendVan,strGroup,strPurchaseOrder,strStatus,strRecordExistTag,strStandingOrder,strOrderType)	 	
  
 	if objCustomer("UpdateStatus") = "OK" Then
		vCustNo = objCustomer("CustomerNo")		 
		set obj = Nothing
		Set objCustomer = Nothing
		Response.Redirect "customer_finish.asp?Name="&Server.URLEncode(strName)&"&CustNo="&vCustNo&"&RecordExistTag="&strRecordExistTag		 
	Else
		Response.Redirect "customer_finish.asp?Name="&Server.URLEncode(strName)&"&RecordExistTag="&strRecordExistTag&"&ErrTag=True"
	End if	
Else
	'Update Existing Customer	
	If Trim(Request.Form("CustNo")) <> "" Then		
		strRecordExistTag = "True"
		vCustNo = Request.Form("CustNo") 
		Set ObjCustomer = obj.Display_CustomerDetail(vCustNo)
		arCustomer = ObjCustomer("Customer")						
		if IsArray(arCustomer) then			
			strName = arCustomer(1,0)
			strOldCode  = arCustomer(2,0)
			strAdd1  = arCustomer(3,0)
			strAdd2 =   arCustomer(4,0)
			strTown = arCustomer(5,0)
			strPostcode = arCustomer(6,0)
			strTele = arCustomer(7,0)						
			strType = arCustomer(8,0)			
			strSYear = arCustomer(9,0)
			strOriCustomer = arCustomer(10,0)
			
			strPayTerm = arCustomer(11,0)
			strDayContactName = arCustomer(12,0)
			strDayContactTele = arCustomer(13,0)
			strMakerName = arCustomer(14,0)
			strMakerTele = arCustomer(15,0)
			
			strPrice = arCustomer(16,0)
			strWeekVan = arCustomer(17,0)
			strWeekendVan = arCustomer(18,0)
			strGroup = arCustomer(19,0)
			strPurchaseOrder = arCustomer(20,0)
			strStatus = arCustomer(22,0)
			strStandingOrder = arCustomer(25,0)
			strOrderType = arCustomer(26,0)
		End if		
	Else
		strRecordExistTag = "False"	
	End if		
End if

set objCustomer = obj.Display_Grouplist()
arGroup = objCustomer("GroupList")	

set obj = Nothing
Set objCustomer = Nothing

%>
<html>
<!--#INCLUDE FILE="includes/head.inc" -->
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<script LANGUAGE="javascript" src="includes/Script.js"></script>
<script Language="JavaScript">
<!--
function validate(theForm,type){
  if (document.Form1.txtName.value == "")
  {
    alert("Please enter a value for the \"Name\" field.");
    document.Form1.txtName.focus();
    return (false);
  }

  if (document.Form1.txtName.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Name\" field.");
    document.Form1.txtName.focus();
    return (false);
  }

  if (document.Form1.txtAdd1.value == "")
  {
    alert("Please enter a value for the \"Address\" field.");
    document.Form1.txtAdd1.focus();
    return (false);
  }

  if (document.Form1.txtAdd1.value.length > 100)
  {
    alert("Please enter at most 100 characters in the \"Address\" field.");
    document.Form1.txtAdd1.focus();
    return (false);
  }

  if (document.Form1.selType.selectedIndex < 0)
  {
    alert("Please select one of the \"Type\" options.");
    document.Form1.selType.focus();
    return (false);
  }

  if (document.Form1.selType.selectedIndex == 0)
  {
    alert("The first \"Type\" option is not a valid selection.  Please choose one of the other options.");
    document.Form1.selType.focus();
    return (false);
  }
  
	if (document.Form1.selWeekVan.selectedIndex < 0)
	{
	  alert("Please select one of the \"Weekday\" options.");
	  document.Form1.selWeekVan.focus();
	  return (false);
	}

	if (document.Form1.selWeekVan.selectedIndex == 0)
	{
	  alert("The first \"Weekday\" option is not a valid selection.  Please choose one of the other options.");
	  document.Form1.selWeekVan.focus();
	  return (false);
	}

	if (document.Form1.selWeekendVan.selectedIndex < 0)
	{
	  alert("Please select one of the \"Weekend\" options.");
	  document.Form1.selWeekendVan.focus();
	  return (false);
	}

	if (document.Form1.selWeekendVan.selectedIndex == 0)
	{
	  alert("The first \"Weekend\" option is not a valid selection.  Please choose one of the other options.");
	  document.Form1.selWeekendVan.focus();
	  return (false);
	}
  
  	if (document.Form1.selOriCustomer.selectedIndex < 0)
	{
	  alert("Please select one of the \"Original customer of\" options.");
	  document.Form1.selOriCustomer.focus();
	  return (false);
	}

	if (document.Form1.selOriCustomer.selectedIndex == 0)
	{
	  alert("The first \"Original customer of\" option is not a valid selection.  Please choose one of the other options.");
	  document.Form1.selOriCustomer.focus();
	  return (false);
	}
  
  if (document.Form1.selOrderType.options(document.Form1.selOrderType.selectedIndex).value == "R")
  {
  
   if (document.Form1.txtOldCode.value == "")
  {
    alert("Please enter a value for the \"Old code\" field.");
    document.Form1.txtOldCode.focus();
    return (false);
  }

  if (document.Form1.txtOldCode.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Old code\" field.");
    document.Form1.txtOldCode.focus();
    return (false);
  }


  if (document.Form1.txtTown.value == "")
  {
    alert("Please enter a value for the \"Town\" field.");
    document.Form1.txtTown.focus();
    return (false);
  }

  if (document.Form1.txtTown.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Town\" field.");
    document.Form1.txtTown.focus();
    return (false);
  }

  if (document.Form1.txtPostcode.value == "")
  {
    alert("Please enter a value for the \"Postcode\" field.");
    document.Form1.txtPostcode.focus();
    return (false);
  }

  if (document.Form1.txtPostcode.value.length > 20)
  {
    alert("Please enter at most 20 characters in the \"Postcode\" field.");
    document.Form1.txtPostcode.focus();
    return (false);
  }

  if (document.Form1.txtTele.value == "")
  {
    alert("Please enter a value for the \"Telephone\" field.");
    document.Form1.txtTele.focus();
    return (false);
  }

  if (document.Form1.txtTele.value.length > 20)
  {
    alert("Please enter at most 20 characters in the \"Telephone\" field.");
    document.Form1.txtTele.focus();
    return (false);
  }

 

	if (document.Form1.D1.selectedIndex < 0)
	{
	  alert("Please select one of the \"Group\" options.");
	  document.Form1.D1.focus();
	  return (false);
	}

	if (document.Form1.D1.selectedIndex == 0)
	{
	  alert("The first \"Group\" option is not a valid selection.  Please choose one of the other options.");
	  document.Form1.D1.focus();
	  return (false);
	}

	if (document.Form1.selPurchaseOrder.selectedIndex < 0)
	{
	  alert("Please select one of the \"Purchase Order\" options.");
	  document.Form1.selPurchaseOrder.focus();
	  return (false);
	}

	if (document.Form1.selPurchaseOrder.selectedIndex == 0)
	{
	  alert("The first \"Purchase Order\" option is not a valid selection.  Please choose one of the other options.");
	  document.Form1.selPurchaseOrder.focus();
	  return (false);
	}

	if (document.Form1.selStatdingOrd.selectedIndex < 0)
	{
	  alert("Please select one of the \"Standing Order\" options.");
	  document.Form1.selStatdingOrd.focus();
	  return (false);
	}

	if (document.Form1.selStatdingOrd.selectedIndex == 0)
	{
	  alert("The first \"Standing Order\" option is not a valid selection.  Please choose one of the other options.");
	  document.Form1.selStatdingOrd.focus();
	  return (false);
	}
  }
  
  if(!(type=="Add")){
		if(confirm("Would you like to " + type + " this customer?")){
			return true;
		}else{
			return false;
		}
	}	
	else
	{
		return true;	
	}
		
}
function deleteCustomer(){
	if(confirm("Would you like to Delete this customer?")){
		document.Form1.type.value='Delete';
		document.Form1.submit();
		return false;
	}else{
		return true;
	}
}


function StopActiveCustomer(mTag){
	if(mTag=="S"){
		if(confirm("Would you like to Stop this customer?")){
			document.Form1.type.value='Stop';
			document.Form1.submit();
			return false;
		}else{
			return true;
		}
	}	
	else{
		if(confirm("Would you like to Active this customer?")){
			document.Form1.type.value='Active';
			document.Form1.submit();
			return false;
		}else{
			return true;
		}		
	}
}

//-->
</script>
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
<form method="POST" action="customer_new.asp" name="Form1">
    <td width="100%"><b><font size="3"><%
    if strRecordExistTag = "True" Then
			Response.Write "Edit customer<br>"
    Else
			Response.Write "Create new customer<br>"
    End if%>
      &nbsp;</font></b>
      
      <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td width="50%" valign="top">
          <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="100%" height="455">
        <tr>
          <td height="22">Order Type<font color="#FF0000">*</font></td>
          <td height="22">
          <select size="1" name="selOrderType" style="font-family: Verdana; font-size: 8pt">
			  <% if strOrderType = "" Then %>
			  <option value="R" Selected>Regular</option>
			  <option value="S">Sample</option>
			  <% Else %>
				<option value="R" <% if strOrderType = "R" Then Response.Write "Selected" %>>Regular</option>
				<option value="S" <% if strOrderType = "S" Then Response.Write "Selected" %>>Sample</option>
			  <% End if %>
            </select>
          </td>
        </tr>
        <tr>
          <td height="19"><b>Name</b><font color="#FF0000"> *</font></td>
          <td height="19">
          <input type="text" name="txtName" value="<%=strName%>" size="42" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Old code <font color="#FF0000">*</font></td>
          <td height="19">
          <input type="text" name="txtOldCode" value="<%=strOldCode%>" size="42" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Address<font color="#FF0000"> *</font></td>
          <td height="19">
          <input type="text" name="txtAdd1" value="<%=strAdd1%>" size="42" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19"></td>
          <td height="19"><input type="text" name="txtAdd2" value="<%=strAdd2%>" size="42" style="font-family: Verdana; font-size: 8pt" Maxlength="100"></td>
        </tr>
        <tr>
          <td height="19">Town <font color="#FF0000">*</font></td>
          <td height="19">
          <input type="text" name="txtTown" value="<%=strTown%>" size="42" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Postcode <font color="#FF0000">*</font></td>
          <td height="19">
          <input type="text" name="txtPostcode" value="<%=strPostcode%>" size="42" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="19">Telephone <font color="#FF0000">*</font></td>
          <td height="19">
          <input type="text" name="txtTele" value="<%=strTele%>" size="42" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="51">Type <font color="#FF0000">*</font><br>
            <br>
            <br>
          </td>
          <td height="51">
          <select size="1" name="selType" style="font-family: Verdana; font-size: 8pt">
							<option value=" ">Select</option>
              <option value="Restaurants" <%if Trim(strType) = "Restaurants" Then Response.Write "Selected"%>>Restaurants</option>
              <%if Trim(strType) = "Restaurants" Then strTypeTag = "True"%>
              <option value="Hotels" <%if Trim(strType) = "Hotels" Then Response.Write "Selected"%>>Hotels</option>
              <%if Trim(strType) = "Hotels" Then strTypeTag = "True"%>
              <option value="Shops" <%if Trim(strType) = "Shops" Then Response.Write "Selected"%>>Shops</option>
              <%if Trim(strType) = "Shops" Then strTypeTag = "True"%>
              <option value="Catering" <%if Trim(strType) = "Catering" Then Response.Write "Selected"%>>Catering</option>
              <%if Trim(strType) = "Catering" Then strTypeTag = "True"%>
              <option value="Air Lines" <%if Trim(strType) = "Air Lines" Then Response.Write "Selected"%>>Air Lines</option>
              <%if Trim(strType) = "Air Lines" Then strTypeTag = "True"%>
              <option value="Deli" <%if Trim(strType) = "Deli" Then Response.Write "Selected"%>>Deli</option>
              <%if Trim(strType) = "Deli" Then strTypeTag = "True"%>
              <option value="Coffee Shop/Sandwich Bar" <%if Trim(strType) = "Coffee Shop/Sandwich Bar" Then Response.Write "Selected"%>>Coffee Shop/Sandwich Bar</option>
              <%if Trim(strType) = "Coffee Shop/Sandwich Bar" Then strTypeTag = "True"%>
              <option value="Clubs" <%if Trim(strType) = "Clubs" Then Response.Write "Selected"%>>Clubs</option>
              <%if Trim(strType) = "Clubs" Then strTypeTag = "True"%>
              <option value="Cafe" <%if Trim(strType) = "Cafe" Then Response.Write "Selected"%>>Cafe</option>
              <%if Trim(strType) = "Cafe" Then strTypeTag = "True"%>
              <option value=" " <%if Trim(strTypeTag) <> "True" Then Response.Write "Selected"%>>Other</option>
            </select><br><%
            if strTypeTag <> "True" Then%>            
							if Other<br>
							<input type="text" name="txtOtherType" value="<%=strType%>" size="42" style="font-family: Verdana; font-size: 8pt" Maxlength="50"></td><%
            Else%>
							if Other<br>
							<input type="text" name="txtOtherType" size="42" style="font-family: Verdana; font-size: 8pt" Maxlength="50"></td><%
            End if%>
        </tr>
        <tr>
          <td height="19">Start Year</td>
          <td height="19">
          <input type="text" name="txtSYear" value="<%=strSYear%>" size="13" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td height="22">Original customer of <font color="#FF0000">*</font></td>
          <td height="22">
          <select size="1" name="selOriCustomer" style="font-family: Verdana; font-size: 8pt">
			  <option value="">Select</option>
              <option value="Gail Force" <%if Trim(strOriCustomer) = "Gail Force" Then Response.Write "Selected"%>>Gail Force</option>
              <option value="Bread Factory" <%if Trim(strOriCustomer) = "Bread Factory" Then Response.Write "Selected"%>>Bread Factory</option>
            </select>
          </td>
        </tr>
        <tr>
          <td height="26">Payment term</td>
          <td height="26"><select size="1" name="selPayTerm" style="font-family: Verdana; font-size: 8pt">
			  <option value="0">Select</option>
              <option value="30 Days" <%if Trim(strPayTerm) = "30 Days" Then Response.Write "Selected"%>>30 Days</option>
              <option value="45 Days" <%if Trim(strPayTerm) = "45 Days" Then Response.Write "Selected"%>>45 Days</option>
              <option value="60 Days" <%if Trim(strPayTerm) = "60 Days" Then Response.Write "Selected"%>>60 Days</option>
            </select></td>
        </tr>
        <tr>
          <td height="1"></td>
          <td height="1"></td>
        </tr>
        <tr>
          <td colspan="2" height="26"><b>Key Contacts</b><br>
            <b>Day to Day contact</b></td>
        </tr>
        <tr>
          <td height="19">Name</td>
          <td height="19">
          <input type="text" name="txtDayContactName" value="<%=strDayContactName%>" size="42" style="font-family: Verdana; font-size: 8pt" Maxlength="50"></td>
        </tr>
        <tr>
          <td height="19">Phone</td>
          <td height="19">
          <input type="text" name="txtDayContactTele" value="<%=strDayContactTele%>" size="42" style="font-family: Verdana; font-size: 8pt" Maxlength="20"></td>
        </tr>
        <tr>
          <td colspan="2" height="13"><b>Decision Maker</b></td>
        </tr>
        <tr>
          <td height="19">Name</td>
          <td height="19">
          <input type="text" name="txtMakerName" value="<%=strMakerName%>" size="42" style="font-family: Verdana; font-size: 8pt" Maxlength="50"></td>
        </tr>
        <tr>
          <td height="19">Phone</td>
          <td height="19">
          <input type="text" name="txtMakerTele" value="<%=strMakerTele%>" size="42" style="font-family: Verdana; font-size: 8pt" Maxlength="20"></td>
        </tr>
        <tr>
          <td height="13"></td>
          <td height="13"></td>
        </tr>
      </table></td>
          <td width="50%" valign="top"><table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="100%">
        <tr>
          <td bgcolor="#E1E1E1" width="50%">Pricing Policy</td>
          <td bgcolor="#E1E1E1" width="50%">&nbsp;</td>
        </tr>
        <tr>
          <td bgcolor="#E1E1E1" width="50%">Default Price <font color="#FF0000">
          *</font></td>
          <td bgcolor="#E1E1E1" width="50%">
						<select size="1" name="selPrice" style="font-family: Verdana; font-size: 8pt">
              <option value="1" <%if Trim(strPrice) = "1" Then Response.Write "Selected"%>>A</option>
              <option value="2" <%if Trim(strPrice) = "2" Then Response.Write "Selected"%>>B</option>
              <option value="3" <%if Trim(strPrice) = "3" Then Response.Write "Selected"%>>C</option>
              <option value="4" <%if Trim(strPrice) = "4" Then Response.Write "Selected"%>>D</option>
            </select></td>
        </tr>
        <tr>
          <td bgcolor="#E1E1E1" width="50%">&nbsp;</td>
          <td bgcolor="#E1E1E1" width="50%">&nbsp;</td>
        </tr>
        <tr>
          <td width="50%"></td>
          <td width="50%"></td>
        </tr>
        <tr>
          <td width="50%"><b>Van Details</b></td>
          <td width="50%"></td>
        </tr>
        <tr>
          <td width="50%">Monday - Friday<font color="#FF0000"> *</font></td>
          <td width="50%">
          <select size="1" name="selWeekVan" style="font-family: Verdana; font-size: 8pt">
			  <option value>Select</option>
              <option value="0" <%if Trim(strWeekVan) = "0" Then Response.Write "Selected"%>>0</option>
              <option value="1" <%if Trim(strWeekVan) = "1" Then Response.Write "Selected"%>>1</option>
              <option value="2" <%if Trim(strWeekVan) = "2" Then Response.Write "Selected"%>>2</option>
              <option value="3" <%if Trim(strWeekVan) = "3" Then Response.Write "Selected"%>>3</option>
              <option value="5" <%if Trim(strWeekVan) = "5" Then Response.Write "Selected"%>>5</option>
              <option value="8" <%if Trim(strWeekVan) = "8" Then Response.Write "Selected"%>>8</option>
              <option value="9" <%if Trim(strWeekVan) = "9" Then Response.Write "Selected"%>>9</option>
            </select></td>
        </tr>
        <tr>
          <td width="50%">Saturday - Sunday <font color="#FF0000">*</font></td>
          <td width="50%">
          <select size="1" name="selWeekendVan" style="font-family: Verdana; font-size: 8pt">
			  <option value>Select</option>        
              <option value="0" <%if Trim(strWeekVan) = "0" Then Response.Write "Selected"%>>0</option>		  
              <option value="1" <%if Trim(strWeekendVan) = "1" Then Response.Write "Selected"%>>1</option>
              <option value="2" <%if Trim(strWeekendVan) = "2" Then Response.Write "Selected"%>>2</option>
              <option value="3" <%if Trim(strWeekendVan) = "3" Then Response.Write "Selected"%>>3</option>
              <option value="5" <%if Trim(strWeekendVan) = "5" Then Response.Write "Selected"%>>5</option>
              <option value="8" <%if Trim(strWeekendVan) = "8" Then Response.Write "Selected"%>>8</option>
              <option value="9" <%if Trim(strWeekendVan) = "9" Then Response.Write "Selected"%>>9</option>
            </select></td>
        </tr>
        <tr>
          <td width="50%"></td>
          <td width="50%"></td>
        </tr>
        
        <tr>
          <td width="50%">Customer Group <font color="#FF0000">*</font></td>
          <td width="50%">
          <select size="selGroupTag" name="D1" style="font-family: Verdana; font-size: 8pt">							     
          	  <option <%if Trim(Request.Form("CustNo")) = "" and Trim(strGroup) = "" or IsNull(strGroup)  Then Response.Write "Selected"%>>Select</option>
              <option <%if Trim(strGroup) <> "" Then Response.Write "Selected"%>>Yes</option>
              <option <%if Trim(Request.Form("CustNo")) <> "" and Trim(strGroup) = "" or IsNull(strGroup)  Then Response.Write "Selected"%>>No</option>              
            </select></td>
        </tr>
        
        <tr>
          <td width="50%">If yes, which one</td>
          <td width="50%"><select size="1" name="selGroup" style="font-family: Verdana; font-size: 8pt">
						<option value=" ">Select</option><%
						If IsArray(arGroup) then
							For i = 0 to UBound(arGroup,2)%>
								<option value="<%=arGroup(0,i)%>" <%if Trim(arGroup(0,i)) = Trim(strGroup) Then Response.Write "Selected"%>><%=arGroup(1,i)%></option><%
							Next 						
						End if%>
            </select></td>
        </tr>
        <tr>
          <td width="50%"></td>
          <td width="50%"></td>
        </tr>
     
        <tr>
          <td width="50%">Does the customer require a Purchase Order <font color="#FF0000">
          *</font></td>
          <td width="50%">
          <select size="1" name="selPurchaseOrder" style="font-family: Verdana; font-size: 8pt">
			  <option value="">Select</option>         
              <option value="Y" <%if Trim(strPurchaseOrder) = "Y" Then Response.Write "Selected"%>>Yes</option>
              <option value="N" <%if Trim(strPurchaseOrder) = "N" Then Response.Write "Selected"%>>No</option>
            </select></td>
        </tr>
        <tr>
          <td width="50%">&nbsp;</td>
          <td width="50%"></td>
        </tr>
        
        <tr>
          <td width="50%">Standing Order <font color="#FF0000">*</font></td>
          <td width="50%">
						<select size="1" name="selStatdingOrd" style="font-family: Verdana; font-size: 8pt">
			  		    <option value="">Select</option>						
					    <option value="Y" <%if Trim(strStandingOrder) = "Y" Then Response.Write "Selected"%>>Yes</option>
					    <option value="N" <%if Trim(strStandingOrder) = "N" Then Response.Write "Selected"%>>No</option>					    
					  </select>
          </td>
        </tr>        
        <!--        <%                if strRecordExistTag = "True" Then%>					<tr>					   <td width="50%">Status</td>					   <td width="50%"><select size="1" name="selStatus" style="font-family: Verdana; font-size: 8pt">					       <option value="A" <%if Trim(strStatus) = "A" Then Response.Write "Selected"%>>Active</option>					       <option value="S" <%if Trim(strStatus) = "S" Then Response.Write "Selected"%>>Stopped</option>					       <option value="D" <%if Trim(strStatus) = "D" Then Response.Write "Selected"%>>Deleted</option>					     </select></td>					 </tr><%				End if%>				    		-->
        
        <tr>
          <td width="50%"><font color="#FF0000">* Mandatory fields</font></td>
          <td width="50%"></td>
        </tr>
        <tr>
          <td width="50%">&nbsp;</td>
          <td width="50%"></td>
        </tr>
        <tr>
					<input type="hidden" name="type" value>
          			<%if strRecordExistTag = "True"  Then%>						
		  				<td width="50%" align="right"><input type="submit" value="Edit customer" name="Edit" style="font-family: Verdana; font-size: 8pt" onclick="return validate(this.form,'Edit')"></td>
  						<td width="50%"><%
  							if Trim(strStatus) = "A" Then
  								if session("UserType") <> "U" Then%>
  									<input type="button" value="Delete customer" name="Delete" style="font-family: Verdana; font-size: 8pt" onclick="return deleteCustomer();">&nbsp;<%
  								End if%>	
  								<input type="button" value="Stop customer" name="Delete" style="font-family: Verdana; font-size: 8pt" onclick="return StopActiveCustomer('S');">
  							<%
  							Elseif Trim(strStatus) = "S" Then%>	
  								<input type="button" value="Activate customer" name="Delete" style="font-family: Verdana; font-size: 8pt" onclick="return StopActiveCustomer('A');"><%
  							End if%>	
  						</td>					
  						<%Else%>
						<td width="50%" align="right"><input type="submit" value="Add customer" name="Search" style="font-family: Verdana; font-size: 8pt" onclick="return validate(this,'Add')"></td>
						<td width="50%"></td>
					<%End if%>	

        </tr>
      </table></td>
        </tr>
      </table>
      
      

    </td>
  </tr>   
  <input type="hidden" name="Update" value="True">
  <input type="hidden" name="RecordExistTag" value="<%=strRecordExistTag%>">
  <input type="hidden" name="CustNo" value="<%=vCustNo%>">
  </form>
</table>
  </center>
</div>
</body>
</html><%
If IsArray(arGroup) Then Erase arGroup
If IsArray(arCustomer) Then Erase arCustomer
%>