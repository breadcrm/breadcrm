<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
'stop
'deldate= request.form("deldate")
deldate= request("deldate")
if deldate= "" then response.redirect "daily_process.asp"

dim objBakery
dim recarray1, recarray2
dim ordno
dim retcol
Dim deldate
Dim intI
Dim intN
Dim intF
Dim intJ


dim tname, tno, curtno
Dim obj
Dim arCreditStatements
Dim arCreditStatementDetails
Dim vCreditDate

%>

<html>
<head>
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; height:1px}
-->
</style>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
' Internal Productions => 11-Stanmore English, 12-Stanmore French, 99-Stanmore Goods, 15-Park Royal, 18-BMG, 22-Cake Department 
' 16 - Flour Station - Added on 18th April 2008 by Selva 
  set object = Server.CreateObject("bakery.daily")
  object.SetEnvironment(strconnection)
%>
<%  
  'Flour Station - 16 - All - Added on 18th April 2008 by Selva 
  stop
  intN = 0
  facilityNo=16
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheetIP(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
end if%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%" >
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table>
<br>
<%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="18%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="18%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>
<%  
  'Stanmore English - 11 - All
  intN = 0
  facilityNo=11
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheetIP(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
end if%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%" >
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table><br>
    </td>
  </tr>
</table>

<%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="18%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="18%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%  
  '12-Stanmore French - Morning
  intN = 0
  facilityNo=12
  tname = "Morning"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheetIP(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
end if%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table><br>
    </td>
  </tr>
</table>

<%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr><%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="18%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="18%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%  
  '12-Stanmore French - Noon,Evening
  intN = 0
  facilityNo=12
  tname = "Noon','Evening"
  tname1 = "Noon, Evening"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheetIP(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
end if%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname1%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="18%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="18%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>

<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%  
  '12-Stanmore French - All
  intN = 0
  facilityNo=12
  tname = "Evening"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheetIP(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
end if%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr><%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="11%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="18%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="18%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>

<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%  
  ' 99-Stanmore Goods -All
  intN = 0
  facilityNo=12
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheetIP(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
end if%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr><%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN)%></td>
		  <td width="18%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="18%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%  
  '99-Stanmore Goods - All
  intN = 0
  facilityNo=99
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheetIP(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
end if%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
         <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="18%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="18%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>

<%end if%>
<%
'48 Hours Sour Dough Report 16-Flour Station   - All
stop
if deldate <> "" then
	deldateNextDay = NextDayinDDMMYY(deldate)
end if
  
    dtype ="All"
      intN=0
      facility = 16
          set col1= object.DailySourDoughOrderSheetViewReportIP(deldateNextDay,facility,dtype)
		vRecArray = col1("SourDoughOrdersheet")
		if isarray(vrecarray) then
		  totrecs = ubound(vrecarray,2)+1
		else
		  totrecs = 0
		end if
		curpage=0
		totpages=0
		if totrecs <> 0 then
		  if clng(totrecs) mod 20  = 0 then
			totpages = clng(totrecs)/20
			curpage=1
		  else
			totpages = fix(clng(totrecs)/20) +  1
			curpage=1
		  end if
		end if

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
intF=1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
	End If
End If
if isarray(vRecArray) then 
intTotal=0
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"><%=Facilityname%> &nbsp;48 Hours Product Report - <%=dtype%><br></font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>Facility: </b><%=Facilityname %><br>
			<b>To the Attention of: </b><%=attn%><br>
			<b>By Fax Number: </b><%=fax%><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldateNextDay%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
            Number of pages for this report: <%=intF%>
            <%Response.Write(replace(space(60)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
  curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
<%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
        For intJ = 1 To  40 'PrintPgSize - changed by selva 12 Oct 2006
			if intN <= ubound(vRecArray, 2) Then
		if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr><%
		intJ=intJ+1
		end if%>
            <tr>
            <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
            <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
            <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
            <td width="10%" height="20"><%=vRecArray(3,intN)%></td>
			<td width="14%">&nbsp;</td>
            </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
            <tr>
            <td height="75%" colspan="3" align="right"><b>Total&nbsp; </b></td>
            <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
			<td width="14%">&nbsp;</td>
            </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2">Sorry no items found</td>
	</tr>
</table><%
end if
%>
<%
end if
set col1= nothing
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%  
  '15-Park Royal - All
  intN = 0
  facilityNo=15
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheetIP(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
end if%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;<%=tname%>&nbsp;Direct Dough</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="18%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="18%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>

<%

'48 Hours Sour Dough Report Park Royal -15
if deldate <> "" then
	deldateNextDay = NextDayinDDMMYY(deldate)
end if
  
    dtype ="All"
      intN=0
      facility = 15
          set col1= object.DailySourDoughOrderSheetViewReportIP(deldateNextDay,facility,dtype)
		vRecArray = col1("SourDoughOrdersheet")
		if isarray(vrecarray) then
		  totrecs = ubound(vrecarray,2)+1
		else
		  totrecs = 0
		end if
		curpage=0
		totpages=0
		if totrecs <> 0 then
		  if clng(totrecs) mod 20  = 0 then
			totpages = clng(totrecs)/20
			curpage=1
		  else
			totpages = fix(clng(totrecs)/20) +  1
			curpage=1
		  end if
		end if

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
intF=1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
	End If
End If
if isarray(vRecArray) then 
intTotal=0
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"><%=Facilityname %> &nbsp;48 Hours Sour Dough Report - <%=dtype%><br></font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>Facility: </b><%=Facilityname %><br>
			<b>To the Attention of: </b><%=attn%><br>
			<b>By Fax Number: </b><%=fax%><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldateNextDay%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
            Number of pages for this report: <%=intF%>
            <%Response.Write(replace(space(60)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
  curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
<%
		if intI = 1 then
		%>
         <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
        For intJ = 1 To  40 'PrintPgSize - changed by selva 12 Oct 2006
			if intN <= ubound(vRecArray, 2) Then
		if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr><%
		intJ=intJ+1
		end if%>
            <tr>
            <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
            <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
            <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
            <td width="10%" height="20"><%=vRecArray(3,intN)%></td>
			<td width="14%">&nbsp;</td>
            </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
            <tr>
            <td height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
            <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
			<td width="14%">&nbsp;</td>
            </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2">Sorry no items found</td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%
end if
set col1= nothing
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%  
  '18-BMG - All
  intN = 0
  facilityNo=18
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheetIP(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
end if%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="9%"><b>Product Code</b></td>
          <td width="30%"><b>Product Name</b></td>
          <td width="8%"><b>Size</b></td>
          <td width="10%"><b>Quantity Ordered</b></td>
          <td width="8%"><b>Tray Qty</b></td>
          <td width="8%"><b>Extra Units</b></td>
	  	  <td width="12%"><b>Nos. in Tray</b></td>
	      <td width="15%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="9%"><%=vRecArray(0,intN)%></td>
          <td width="30%"><%=vRecArray(1,intN)%></td>
          <td width="8%"><%=vRecArray(2,intN)%></td>
          <td width="10%"><%=vRecArray(3,intN)%></td>
	 	  <td width="8%"><%=vRecArray(3,intN) \ vRecArray(7,intN)%></td>
	  	  <td width="8%"><%=vRecArray(3,intN) mod vRecArray(7,intN)%></td>
	  	  <td width="12%"><%=vRecArray(7,intN)%></td>
	      <td width="15%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="18%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>

<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%  
  '22-Cake Department - All
  intN = 0
  facilityNo=22
  tname = "All"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheetIP(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
end if%>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%>&nbsp;(<%=tname%>)</font></b><br>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
         <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  <td width="18%">&nbsp;</td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="75%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
		  <td width="18%">&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>
<%
'48 Hours Sour Dough Report 22-Cake Department - All
if deldate <> "" then
	deldateNextDay = NextDayinDDMMYY(deldate)
end if
  
    dtype ="All"
      intN=0
      facility = 22
          set col1= object.DailySourDoughOrderSheetIP(deldateNextDay,facility,dtype)
		vRecArray = col1("SourDoughOrdersheet")
		if isarray(vrecarray) then
		  totrecs = ubound(vrecarray,2)+1
		else
		  totrecs = 0
		end if
		curpage=0
		totpages=0
		if totrecs <> 0 then
		  if clng(totrecs) mod 20  = 0 then
			totpages = clng(totrecs)/20
			curpage=1
		  else
			totpages = fix(clng(totrecs)/20) +  1
			curpage=1
		  end if
		end if

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
intF=1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
	End If
End If
if isarray(vRecArray) then 
intTotal=0
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"><%=Facilityname %> &nbsp;48 Hours Product Report - <%=dtype%><br></font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>Facility: </b><%=Facilityname %><br>
			<b>To the Attention of: </b><%=attn%><br>
			<b>By Fax Number: </b><%=fax%><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldateNextDay%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
            Number of pages for this report: <%=intF%>
            <%Response.Write(replace(space(60)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
  curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
<%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
        For intJ = 1 To  40 'PrintPgSize - changed by selva 12 Oct 2006
			if intN <= ubound(vRecArray, 2) Then
		if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr><%
		intJ=intJ+1
		end if%>
            <tr>
            <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
            <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
            <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
            <td width="10%" height="20"><%=vRecArray(3,intN)%></td>
			<td width="17%">&nbsp;</td>
            </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
            <tr>
            <td height="75%" colspan="3" align="right"><b>Total&nbsp; </b></td>
            <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
			<td width="17%">&nbsp;</td>
            </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2">Sorry no items found</td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%
end if
set col1= nothing
%>
<%
'48 Hours Product Report 18-BMG 
    dtype ="All"
      intN=0
      facility = 18
          set col1= object.DailySourDoughOrderSheetIP(deldateNextDay,facility,dtype)
		vRecArray = col1("SourDoughOrdersheet")
		if isarray(vrecarray) then
		  totrecs = ubound(vrecarray,2)+1
		else
		  totrecs = 0
		end if
		curpage=0
		totpages=0
		if totrecs <> 0 then
		  if clng(totrecs) mod 20  = 0 then
			totpages = clng(totrecs)/20
			curpage=1
		  else
			totpages = fix(clng(totrecs)/20) +  1
			curpage=1
		  end if
		end if

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
intF=1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
	End If
End If
if isarray(vRecArray) then 
intTotal=0
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"><%=Facilityname %> &nbsp;48 Hours Product Report - <%=dtype%><br></font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>Facility: </b><%=Facilityname %><br>
			<b>To the Attention of: </b><%=attn%><br>
			<b>By Fax Number: </b><%=fax%><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldateNextDay%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
            Number of pages for this report: <%=intF%>
            <%Response.Write(replace(space(60)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
  curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
<%
		if intI = 1 then
		%>
        <tr>
          <td width="9%"><b>Product Code</b></td>
          <td width="30%"><b>Product Name</b></td>
          <td width="8%"><b>Size</b></td>
          <td width="10%"><b>Quantity Ordered</b></td>
          <td width="8%"><b>Tray Qty</b></td>
          <td width="8%"><b>Extra Units</b></td>
	      <td width="12%"><b>Nos. in Tray</b></td>
	      <td width="15%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
        For intJ = 1 To  40 'PrintPgSize - changed by selva 12 Oct 2006
			if intN <= ubound(vRecArray, 2) Then
		if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr><%
		intJ=intJ+1
		end if%>
            <tr>
			  <td width="9%"><%=vRecArray(0,intN)%></td>
			  <td width="30%"><%=vRecArray(1,intN)%></td>
			  <td width="8%"><%=vRecArray(2,intN)%></td>
			  <td width="10%"><%=vRecArray(3,intN)%></td>
			  <td width="8%"><%=vRecArray(3,intN) \ vRecArray(5,intN)%></td>
			  <td width="8%"><%=vRecArray(3,intN) mod vRecArray(5,intN)%></td>
			  <td width="12%"><%=vRecArray(5,intN)%></td>
			  <td width="15%">&nbsp;</td>
            </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
            <tr>
            <td height="75%" colspan="3" align="right"><b>Total&nbsp; </b></td>
            <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
			<td width="14%">&nbsp;</td>
            </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2">Sorry no items found</td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%
end if
set col1= nothing
%>

<%
'48 Hours Product Report 11-Stanmore English 
    dtype ="All"
      intN=0
      facility = 11
          set col1= object.DailySourDoughOrderSheetIP(deldateNextDay,facility,dtype)
		vRecArray = col1("SourDoughOrdersheet")
		if isarray(vrecarray) then
		  totrecs = ubound(vrecarray,2)+1
		else
		  totrecs = 0
		end if
		curpage=0
		totpages=0
		if totrecs <> 0 then
		  if clng(totrecs) mod 20  = 0 then
			totpages = clng(totrecs)/20
			curpage=1
		  else
			totpages = fix(clng(totrecs)/20) +  1
			curpage=1
		  end if
		end if

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
intF=1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
	End If
End If
if isarray(vRecArray) then 
intTotal=0
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"><%=Facilityname %> &nbsp;48 Hours Product Report - <%=dtype%><br></font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>Facility: </b><%=Facilityname %><br>
			<b>To the Attention of: </b><%=attn%><br>
			<b>By Fax Number: </b><%=fax%><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldateNextDay%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
            Number of pages for this report: <%=intF%>
            <%Response.Write(replace(space(60)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
  curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
<%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
        For intJ = 1 To  40 'PrintPgSize - changed by selva 12 Oct 2006
			if intN <= ubound(vRecArray, 2) Then
		if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr><%
		intJ=intJ+1
		end if%>
            <tr>
            <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
            <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
            <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
            <td width="10%" height="20"><%=vRecArray(3,intN)%></td>
			<td width="17%">&nbsp;</td>
            </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
            <tr>
            <td height="75%" colspan="3" align="right"><b>Total&nbsp; </b></td>
            <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
			<td width="17%">&nbsp;</td>
            </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2">Sorry no items found</td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%
end if
set col1= nothing
%>

<%
'48 Hours Product Report 12-Stanmore French 
    dtype ="All"
      intN=0
      facility = 12
          set col1= object.DailySourDoughOrderSheetIP(deldateNextDay,facility,dtype)
		vRecArray = col1("SourDoughOrdersheet")
		if isarray(vrecarray) then
		  totrecs = ubound(vrecarray,2)+1
		else
		  totrecs = 0
		end if
		curpage=0
		totpages=0
		if totrecs <> 0 then
		  if clng(totrecs) mod 20  = 0 then
			totpages = clng(totrecs)/20
			curpage=1
		  else
			totpages = fix(clng(totrecs)/20) +  1
			curpage=1
		  end if
		end if

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
intF=1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
	End If
End If
if isarray(vRecArray) then 
intTotal=0
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"><%=Facilityname %> &nbsp;48 Hours Product Report - <%=dtype%><br></font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>Facility: </b><%=Facilityname %><br>
			<b>To the Attention of: </b><%=attn%><br>
			<b>By Fax Number: </b><%=fax%><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldateNextDay%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
            Number of pages for this report: <%=intF%>
            <%Response.Write(replace(space(60)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
  curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
<%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
        For intJ = 1 To  40 'PrintPgSize - changed by selva 12 Oct 2006
			if intN <= ubound(vRecArray, 2) Then
		if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr><%
		intJ=intJ+1
		end if%>
            <tr>
            <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
            <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
            <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
            <td width="10%" height="20"><%=vRecArray(3,intN)%></td>
			<td width="17%">&nbsp;</td>
            </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
            <tr>
            <td height="75%" colspan="3" align="right"><b>Total&nbsp; </b></td>
            <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
			<td width="17%">&nbsp;</td>
            </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2">Sorry no items found</td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%
end if
set col1= nothing
%>

<%
'48 Hours Product Report 99-Stanmore Goods 
    dtype ="All"
      intN=0
      facility = 99
          set col1= object.DailySourDoughOrderSheetIP(deldateNextDay,facility,dtype)
		vRecArray = col1("SourDoughOrdersheet")
		if isarray(vrecarray) then
		  totrecs = ubound(vrecarray,2)+1
		else
		  totrecs = 0
		end if
		curpage=0
		totpages=0
		if totrecs <> 0 then
		  if clng(totrecs) mod 20  = 0 then
			totpages = clng(totrecs)/20
			curpage=1
		  else
			totpages = fix(clng(totrecs)/20) +  1
			curpage=1
		  end if
		end if

Facilityrec = col1("Facility")
if isarray(facilityrec) then
  facilityname = facilityrec(0,0)
  attn = facilityrec(1,0)
  fax = facilityrec(2,0)
else
  facilityname = "Unknown"
  attn = "Unknown"
  fax = "Unknown"
end if
set col1= nothing
intF=1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
	End If
End If
if isarray(vRecArray) then 
intTotal=0
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<table align="center" border="0" width="95%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"><%=Facilityname %> &nbsp;48 Hours Product Report - <%=dtype%><br></font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
        <tr>
          <td>
			<b>Facility: </b><%=Facilityname %><br>
			<b>To the Attention of: </b><%=attn%><br>
			<b>By Fax Number: </b><%=fax%><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldateNextDay%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
            Number of pages for this report: <%=intF%>
            <%Response.Write(replace(space(60)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>			
         	</td>
        </tr>
        </table>
		<br>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
  curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="95%" cellspacing="0" cellpadding="1" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000">
<%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="51%" height="20"><b>Product name</b></td>
          <td width="10%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
		  <td width="18%"><b>Quantity Done</b></td>
        </tr>
		<%
		end if
        For intJ = 1 To  41 'PrintPgSize - changed by selva 12 Oct 2006
			if intN <= ubound(vRecArray, 2) Then
		if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="5" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr><%
		intJ=intJ+1
		end if%>
            <tr>
            <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
            <td width="51%" height="20"><%=vRecArray(1,intN)%></td>
            <td width="6%" height="20"><%=vRecArray(2,intN)%></td>
            <td width="10%" height="20"><%=vRecArray(3,intN)%></td>
			<td width="17%">&nbsp;</td>
            </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
            <tr>
            <td height="75%" colspan="3" align="right"><b>Total&nbsp; </b></td>
            <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
			<td width="17%">&nbsp;</td>
            </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="95%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="1">
	<tr>
	<td colspan="2">Sorry no items found</td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%
end if
set col1= nothing
%>
<%
set object = nothing
%>
<p>&nbsp;</p>
</body>
</html>