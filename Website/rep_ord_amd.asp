<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	//y=document.form.txtto.value
	//x=y.split("/");
	//d2=x[1]+"/"+x[0]+"/"+x[2]
	//t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	//document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	//document.form.day1.value=t1.getDate()
	//document.form.month1.value=t1.getMonth()
	//document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function ExportToExcelFile(){
	fromdate=document.form.txtfrom.value
	//todate=document.form.txtto.value
	
	document.location="ExportToExcelFile_rep_ord_amend.asp?dt=" + fromdate 
}
function Hide()
{
	document.getElementById("HidePanel").style.display = "none";
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal();Hide()">
<!--#INCLUDE FILE="nav.inc" -->
<br>
<DIV id="HidePanel">
	<P align="center"><b><font size="2" face="Verdana" color="#0099CC">Report generation in progress, Please wait...</font></b></P>
	<p align="center"><img name="animation" src="images/LoadingAnimation.gif" border="0" WIDTH="201" HEIGHT="33"></p>
</DIV>

<%
dim objRep
dim recarray
dim retcol
dim fromdt, i,TotalDeliveries,Totalturnover,Averageturnover,fromdtr

On Error GoTo 0

fromdt = Request.form("txtfrom")

if fromdt = "" then
  Dim defSelectedDate
  defSelectedDate = DateAdd("d",1,Now)
  fromdt=Day(defSelectedDate) & "/" & Month(defSelectedDate) & "/" & Year(defSelectedDate)
  fromdtr=Year(defSelectedDate) & "-" & Month(defSelectedDate) & "-" & Day(defSelectedDate)
else
	dim arr
	arr = Split(fromdt,"/")
	fromdtr = arr(2) & "-" & arr(1) & "-" & arr(0)
end if


if isdate(fromdt) then
set objRep = server.CreateObject("Bakery.Reports")
objRep.SetEnvironment(strconnection)
set retcol = objRep.OrderAmendReport(fromdt)
recarray = retcol("Report")
end if
%>
<div align="center">
<center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">

 
 <tr>
    <td width="100%"><b><font size="3">Order Amends<br>
      &nbsp;</font></b>
	  <form method="post" action="rep_ord_amd.asp" name="form">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td></td>
          <td><b>Delivery Date:</b></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
         <td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom onFocus="blur();" size="12"  name="txtfrom" value="<%=fromdt%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">	
					</td>
					<td>
						<input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt">
					</td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
	  </form>
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
      <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%" colspan="6"><b>Amended Orders Delivered On &lt;<%=fromdt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
		 <tr>
          <td colspan="6" align="left" height="30">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>
        <tr>
          <td width="10%" bgcolor="#CCCCCC" ><center><b>Cus. No&nbsp;</b></center></td>
          <td width="35%" bgcolor="#CCCCCC"><center><b>Customer Name&nbsp;</b></center></td>
          <td width="12%" bgcolor="#CCCCCC"><center><b>Amended Date&nbsp;</b></center></td>        
          <td width="20%" bgcolor="#CCCCCC"><center><b>By&nbsp;</b></center></td>
          <td width="13%" bgcolor="#CCCCCC"><center><b>Standing Order&nbsp;</b></center></td>
          <td width="10%" bgcolor="#CCCCCC"><center><b>Value&nbsp;</b></center></td>     
        </tr>
		
		<%if isarray(recarray) then%>
				<%for i=0 to UBound(recarray,2)%>
					<tr>
						<td><%=recarray(6,i)%></td>
						<td><%=recarray(7,i)%></td>
						<td><%=recarray(9,i)%>&nbsp;</td> 
						<td><%=recarray(11,i)%></td>
						<td><center><%if(recarray(12,i)="Y") Then Response.Write("Yes") Else Response.Write("No") End If%></center></td>
						<td align="right"><%=FormatNumber(recarray(8,i),2)%>&nbsp;</td>
					</tr>
				<%next%>
          <%else%>
			<tr>
				<td colspan="6" width="100%" bgcolor="#CCCCCC"><b>0 - No Amended Orders Found...</b></td>
			</tr>
          <%end if%>
      </table>
    </td>
  </tr>
</table>
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;<br><br><br><br><br><br>
</center>
</div>
</body>
</html>