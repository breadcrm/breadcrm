<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
vPageSize = 999999
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vCurrentPage	= Request.Form("CurrentPage") 

strPricelessCredit = Request.Form("PricelessCredit")
if (strPricelessCredit="1") then
	strPricelessCreditChecked="Checked"
end if

strCustomerName = replace(Request.Form("txtCustName"),"'","''")
strCustNo = replace(Request.Form("txtCustNo"),"'","''")

strDate1 = Request.Form("from")
strDate2 = Request.Form("to")
strCreditNo= trim(Request.Form("txtCreditNo"))
strInvoiceNo= trim(Request.Form("txtInvoiceNo"))

If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if
vCurrentPage = 0
If strDate1 = "" Then
	strDate1 = Day(date()) & "/" & Month(date()) & "/" & Year(date())  
End if
If strDate2 = "" Then
	strDate2 = Day(date()) & "/" & Month(date()) & "/" & Year(date())  
End if

arSplit = Split(strDate1,"/")
strDate1 = Right( "0" & arSplit(0),2) & "/" & Right( "0" & arSplit(1),2) & "/" & arSplit(2)	

arSplit = Split(strDate2,"/")
strDate2 = Right( "0" & arSplit(0),2) & "/" & Right( "0" & arSplit(1),2) & "/" & arSplit(2)	

Set Obj = CreateObject("bakery.credit")
Obj.SetEnvironment(strconnection)
Set ObjCredit = obj.Display_CreditList(vPageSize,vCurrentPage,strCustomerName,strCustNo,strDate1,strDate2,strCreditNo,strInvoiceNo,strPricelessCredit)
arCredit = ObjCredit ("Credit")
vpagecount  = ObjCredit ("Pagecount")
Set Obj = Nothing
Set ObjCredit = Nothing
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.frmCustomer.from.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.frmCustomer.to.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	
	document.frmCustomer.from.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.frmCustomer.to.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.frmCustomer.day.value=t.getDate()
	document.frmCustomer.month.value=t.getMonth()
	document.frmCustomer.year.value=t.getFullYear()
	
	document.frmCustomer.day1.value=t1.getDate()
	document.frmCustomer.month1.value=t1.getMonth()
	document.frmCustomer.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}
//-->
</SCRIPT>

<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<title></title>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>  
  
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Find Credit
      &nbsp;</font></b>
      <form method="POST"  id="frmCustomer" name="frmCustomer">
				<table width="900" height="69" border="0" cellpadding="2" cellspacing="0" style="font-family: Verdana; font-size: 8pt">
				  <tr>
				    <td height="35" colspan="5"><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
			      </tr>
				  <tr>
				    <td height="26" width="148"><b>Customer Code:</b></td>
					<td height="26" colspan="4"><input type="text" name="txtCustNo"  value="<%=strCustNo%>"size="10" style="font-family: Verdana; font-size: 8pt"></td>
				  </tr>
				  <tr>
				    <td height="26" width="148"><b>Customer Name:</b></td>
					<td height="26" colspan="4"><input type="text" name="txtCustName" value="<%=strCustomerName%>" size="30" style="font-family: Verdana; font-size: 8pt" maxlength="150"></td>
				  </tr>
				  <tr>
				     <td height="26" width="148"><b>Credit No:</b></td>
					 <td height="26" colspan="4"><input type="text" name="txtCreditNo"  value="<%=strCreditNo%>"size="10" style="font-family: Verdana; font-size: 8pt"></td>
				  </tr>
				  <tr>
				     <td height="26" width="148"><b>Invoice No:</b></td>
					<td height="26" colspan="4"><input type="text" name="txtInvoiceNo"  value="<%=strInvoiceNo%>"size="10" style="font-family: Verdana; font-size: 8pt"></td>
				  </tr>
				 
				  <tr>
					  <td><b>Priceless Invoice/Credit:</b></td>
					  <td><input type="checkbox" style="border-width: 0px; margin-left: 0px; padding: 0px;" name="PricelessCredit" value="1" <%=strPricelessCreditChecked%>></td>
				  </tr>
	
				  <tr>
				    <td height="26" width="148"><b> Invoice Date From:</b></td>
					<td height="18" width="319">
                    <%
						dim arrayMonth,i
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.frmCustomer.from)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.frmCustomer.from)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						<select name="year" onChange="setCal(this.form,document.frmCustomer.from)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=from  name="from" value="<%=strDate1%>" onFocus="blur();" onChange="setCalCombo(this.form,document.frmCustomer.from)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
				    <td height="26"  width="70"><b>To:</b></td>
				    <td height="18" width="325">
                    <select name="day1" onChange="setCal1(this.form,document.frmCustomer.to)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.frmCustomer.to)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					<select name="year1" onChange="setCal1(this.form,document.frmCustomer.to)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=to  name="to" onFocus="blur();" value="<%=strDate2%>" onChange="setCalCombo1(this.form,document.frmCustomer.to)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					
				  </tr>
				 <tr>
				 <td height="26" width="148">&nbsp;</td>
				 <td height="18" colspan="4">
				      <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
				 </tr>
				  <tr>
				    <td height="13"></td>
				    <td height="13"></td>
				    <td height="13"></td>
				    <td height="13"></td>
				    <td width="7" height="13"></td>
				    <td width="7" height="13"></td>
				  </tr>
			</table>
		  </Form>	
			<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "from",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
			</SCRIPT>
			<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "to",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
			</SCRIPT>
			<%if vCurrentPage <> 0 then%>
		<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">      
			<tr>
				<td height="20"></td>
			</tr><%				
			if IsArray(arCredit) Then%>
				<tr>
				  <td align="right"><b>Page <%=vCurrentPage%> of <%=vpagecount%></b></td>          
				</tr><%
			End if%>	
		</table> 
		<%End if%>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2" bgcolor="#CCCCCC">
        <tr bgcolor="#FFFFFF">
          <td width="20%" bgcolor="#CCCCCC"><b>Invoice Date</b></td>
		  <td width="11%" bgcolor="#CCCCCC"><b>Invoice No</b></td>
		  <td width="20%" bgcolor="#CCCCCC"><b>Credit Date</b></td>
          <td width="10%" bgcolor="#CCCCCC"><b>Credit No</b></td>
		  <td width="10%" bgcolor="#CCCCCC"><b>Customer Code</b></td>
          <td width="35%" bgcolor="#CCCCCC"><b>Customer Name</b></td>
          <td width="10%" bgcolor="#CCCCCC"><b>Action</b></td>
        </tr><%
					if IsArray(arCredit) Then 
						for i = 0 to UBound(arCredit,2)%>
							<tr bgcolor="#FFFFFF">
							  <td><%=arCredit(1,i)%></td>
							   <td><%=arCredit(5,i)%></td>
							  <td><%=arCredit(0,i)%></td>
							  <td><%=arCredit(2,i)%>&nbsp;</td>
							  <td><%=arCredit(4,i)%>&nbsp;</td>
							  <td><%=arCredit(3,i)%></td>
							  <td align="left">
							  	<input type="button" onClick="NewWindow('credit_view.asp?CustNo=<%=arCredit(4,i)%>&CreditNo=<%=arCredit(2,i)%>','profile','800','400','yes','center');return false" onFocus="this.blur()" value="View Credit" name="B1" style="font-family: Verdana; font-size: 8pt">&nbsp;
								
								<%
								if (strconnection<>"vdbArchiveConn") Then
								%>
								<input type="button" onClick="NewWindow('PDFGenerator/PrintCreditNote.aspx?docno=<%=arCredit(2,i)%>','profile','700','450','yes','center');return false" onFocus="this.blur()" value="Credit in PDF" name="B1" style="font-family: Verdana; font-size: 8pt">
								<%
								else
								%>
								<input type="button" onClick="NewWindow('http://bakery.adsgrp.com/PDFGeneratorArchive/PrintCreditNote.aspx?docno=<%=arCredit(2,i)%>','profile','700','450','yes','center');return false" onFocus="this.blur()" value="Credit in PDF" name="B1" style="font-family: Verdana; font-size: 8pt">
								<%
								end if
								%>
								
                              </td>
							</tr>
<%
		Next
	Else
%>

        <tr bgcolor="#FFFFFF">
          <td colspan="8" align="center" height="40">Sorry no items found</td>
        </tr>
<%
	End if
%>
      </table>
    </td>
  </tr>
</table>

	
	<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		<FORM Name="Navigate">
			<tr>					
				<td width="187"><%				
		
					if vCurrentPage <> 1 and vCurrentPage <> 0 then%>
						<input type="button" value="FirstPage" Onclick="document.frmFirst.submit()" id=button1 name=button1 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) > 1 then%>
						<input type="button" value="PreviousPage" Onclick="document.frmPrevious.submit()" id=button3 name=button3 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%				
					if clng(vCurrentPage) < vPagecount then%>
						<input type="button" value="NextPage" Onclick="document.frmNext.submit()" id=button5 name=button5 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) <> vPagecount and vPagecount > 0  then%>
						<input type="button" value="LastPage" Onclick="document.frmLast.submit()" id=button7 name=button7 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>					
			</tr>
		</FORM>
	</table>
	
  </center>
</div>


	<FORM METHOD="post" NAME="frmFirst">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="First">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">				
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<INPUT TYPE="hidden" NAME="txtGroup"			VALUE="<%=strGroup%>">
		<input type="hidden" name="from" value="<%=strDate1%>">
		<input type="hidden" name="to" value="<%=strDate2%>">
	</FORM>

	<FORM METHOD="post" NAME="frmPrevious">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Previous">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<INPUT TYPE="hidden" NAME="txtGroup"			VALUE="<%=strGroup%>">
		<input type="hidden" name="from" value="<%=strDate1%>">
		<input type="hidden" name="to" value="<%=strDate2%>">
	</FORM>

	<FORM METHOD="post" NAME="frmNext">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Next">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<INPUT TYPE="hidden" NAME="txtGroup"			VALUE="<%=strGroup%>">
		<input type="hidden" name="from" value="<%=strDate1%>">
		<input type="hidden" name="to" value="<%=strDate2%>">
	</FORM>

	<FORM METHOD="post" NAME="frmLast">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Last">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<INPUT TYPE="hidden" NAME="txtGroup"			VALUE="<%=strGroup%>">
		<input type="hidden" name="from" value="<%=strDate1%>">
		<input type="hidden" name="to" value="<%=strDate2%>">
	</FORM>

</body>

</html>