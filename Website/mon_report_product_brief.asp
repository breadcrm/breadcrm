<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}
function ExportToExcelFile(){
	fromdate=document.form.txtfrom.value
	todate=document.form.txtto.value
	vstatus=document.form.Status.value
	document.location="ExportToExcelFile_mon_report_product_brief.asp?fromdt=" + fromdate + "&todt=" +todate + "&status=" +vstatus
}
function Hide()
{
	document.getElementById("HidePanel").style.display = "none";
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal();Hide()">
<!--#INCLUDE FILE="nav.inc" -->
<br>
<DIV id="HidePanel">
	<P align="center"><b><font size="2" face="Verdana" color="#0099CC">Report generation in progress, Please wait...</font></b></P>
	<p align="center"><img name="animation" src="images/LoadingAnimation.gif" border="0" WIDTH="201" HEIGHT="33"></p>
</DIV>
<%
dim objBakery
dim recarray
dim retcol
dim fromdt, todt,i,strStatus,strStatusLabel
Dim UnitsSold,Price_A,AveragePrice,TotalTurnover

strStatus=request("Status")
if strStatus="A" then
	strStatusLabel="Active Products"
elseif  strStatus="D" then
	strStatusLabel="Deleted Products"
else
	strStatusLabel=""
end if

fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

if isdate(fromdt) and isdate(todt) then
set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
if strStatus="0" then
	strStatus=""
elseif strStatus="" then
	strStatus="A"
end if
set retcol = objBakery.Display_product_brief(fromdt,todt,strStatus)
recarray = retcol("Product")
end if
%>
<div align="center">
<center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">

 <tr>
    <td width="100%"><b><font size="3">Product Report</font></b>
	  <form method="post" action="mon_report_product_brief.asp" name="form">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td><b>Status</b></td>
          <td><b>From:</b></td>
          <td><b>To:</b></td>
          <td></td>
        </tr>
        <tr>
          <td>
		  
		   <select name="Status">
		  	<option value="0">All</option>
			<option value="A" <% if strStatus="A" then%> selected="selected" <%end if%>>Active</option>
			<option value="D" <% if strStatus="D" then%> selected="selected" <%end if%>>Deleted</option>
		  </select>
		  </td>
          <td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom onFocus="blur();" size="12"  name="txtfrom" value="<%=fromdt%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=todt%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td>
				<td><input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
		<tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
	  </form>
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
      <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%"  colspan="6"><b>Product Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; <% if strStatus<>"" then%> - <%=strStatusLabel%><%end if%></b></td>
        </tr>
        <tr>
          <td colspan="6" align="left" height="30">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>
		<tr>
          <td width="15%" bgcolor="#CCCCCC"><b>Product Code&nbsp;</b></td>
          <td width="45%" bgcolor="#CCCCCC"><b>Product Name&nbsp;</b></td>
          <td width="10%" bgcolor="#CCCCCC" align="right"><b>Number of Units Sold</b></td>
          <td width="10%" bgcolor="#CCCCCC" align="right"><b>Price A</b></td>
          <td width="10%" bgcolor="#CCCCCC" align="right"><b>Average Price Sold</b></td>
          <td width="10%" bgcolor="#CCCCCC" align="right"><b>Total Turnover</b></td>  
        </tr>
        <tr>
			<%if isarray(recarray) then%>
			<%
			UnitsSold = 0
			Price_A = 0.00
			AveragePrice = 0.00
			TotalTurnover = 0.00
			
			%>
			<%for i=0 to UBound(recarray,2)%>
		<tr>
          <td><b><%=recarray(0,i)%></b></td>
          <td><b><%=recarray(1,i)%></b></td>
          <td align="right" bgcolor="#CCCCCC"><b><%=recarray(2,i)%></b></td>
          <td align="right"><b><%if recarray(3,i)<> "" then Response.Write formatnumber(recarray(3,i),2) else Response.Write "0.00" end if%></b></td>
          <td align="right" bgcolor="#CCCCCC"><b><%if recarray(4,i)<> "" then Response.Write formatnumber(recarray(4,i),2) else Response.Write "0.00" end if%></b></td>
          <td align="right"><b><%if recarray(5,i)<> "" then Response.Write formatnumber(recarray(5,i),2) else Response.Write "0.00" end if%></b></td>
        </tr>
          <%
          if recarray(2,i) <> "" then
          UnitsSold = UnitsSold + recarray(2,i)
          End if 
          if recarray(3,i)<> "" then 
					Price_A = Price_A + formatnumber(recarray(3,i),2)
					End if
					if recarray(4,i)<> "" then
					AveragePrice = AveragePrice + formatnumber(recarray(4,i),2)
					End if
					If recarray(5,i)<> "" then 
					TotalTurnover = TotalTurnover + formatnumber(recarray(5,i),2)
					End if
          %>
          <%next%>
          <tr>
       		<td colspan=2><b>&nbsp;&nbsp;Grand Total</b></td>
          	<td align="right" bgcolor="#CCCCCC"><b><%=UnitsSold%></b></td>
          	<td align="right"><b><%'=formatnumber(Price_A,2)%>&nbsp;</b></td>
          	<td align="right" bgcolor="#CCCCCC"><b><%=formatnumber(AveragePrice,2)%></b></td>
          	<td align="right"><b><%=formatnumber(TotalTurnover,2)%></b></td>
          </tr>
          <%
          else%>
          <td width="33%" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td><%
          end if%>
        </tr>
	   </table>
    </td>
  </tr>
</table>
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;<br><br><br><br><br><br>
</center>
</div>
</body>
<%if IsArray(recarray) then erase recarray %>
</html>
