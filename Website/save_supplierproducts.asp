<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%

vsno= request.form("sno")
vIno= request.form("ino")
vqty = request.form("qty")
vPrice = request.form("price")
voften = request.form("often")
vfacility = request.form("facility")
vFacarray = split(vfacility,",")

if (not isnumeric(vqty)) or (not isnumeric(vPrice)) then response.redirect "supplier_products.asp"

response.write vIno & "," & vSno & "," & vQty & "," & vPrice & "," & vOften

set object = Server.CreateObject("bakery.Supplier")
object.SetEnvironment(strconnection)
UpdateSupplierIng = object.UpdateSupplierIng(vIno,vSno,vQty,vPrice,vOften,vFacarray)
set object = nothing

if UpdateSupplierIng <> "Ok" then
	response.redirect "supplier_products.asp?sno="&vsno
end if
%>
<script>
window.opener.location.href='supplier_view.asp?sno=<%=vSno%>'
window.close();
</script>