<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title>Sales Report - By Products By Customers</title>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function ExportToExcelFile(){
	fromdate=document.form.txtfrom.value
	todate=document.form.txtto.value
	vProductId=document.form.ProductID.value
	vProductName=document.form.Product.value
	vismultipleproduct=document.form.ismultipleproduct.value
	document.location="ExportToExcelFile_sale_product_by_customer.asp?ProductId=" + vProductId + "&Product=" + vProductName + "&fromdt=" + fromdate + "&todt=" +todate + "&ismultipleproduct=" +vismultipleproduct
}
//-->
</SCRIPT>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<br>
<%
dim objBakery
dim recarray
dim retcol
dim fromdt, todt, i,Totalturnover
Dim vproduct,vNumber,vpcode,vdno


fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")
vproduct = Request.Form("product")
vpcode = replace(Request.Form("pcode"),"'","''")
vdno =  Request.Form("dno") 
vProductID = Request.Form("ProductID")
vismultipleproduct = Request.Form("ismultipleproduct")

'fromdt = date()
'todt = date()


if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if


set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
if isdate(fromdt) and isdate(todt) then
	set retcol = objBakery.Display_Products_By_Customer(fromdt,todt,"",vProductID,"")
	recarray = retcol("ProductsByCustomer")
		set vListofMultipleProducts= objBakery.ListofMultipleProducts(vProductID)
			vRecArray = vListofMultipleProducts("MultipleProduct")
		set vListMultipleProductsForProduct= objBakery.ListMultipleProductsForProduct(vProductID,fromdt,todt)
			vRecArray1 = vListMultipleProductsForProduct("MultiProducts")			
	set retcol = nothing
end if

%>
<div align="center">
<center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <form method="POST" name="form">
  <input type = "hidden" name = "ProductID" value="<%=vProductID%>">
  <input type = "hidden" name = "txtfrom" value="<%=fromdt%>">
  <input type = "hidden" name = "txtto" value="<%=todt%>">
  <input type = "hidden" name = "Product" value="<%=server.URLEncode(vproduct)%>">
  <input type = "hidden" name = "ismultipleproduct" value="<%=vismultipleproduct%>">
  </form>
 <tr>
    <td width="100%"><b><font size="3">Sales Report - Product By Customers</font></b><br>
        <br>
	 
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
         <tr>
          <td colspan="5" align="right" height="20" valign="bottom">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>
		<tr>
          <td colspan="6">
		  <b>Sales Report - Product By Customers From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt;</b><br><br>
		  <b>Product Code: <%=vProductID%></b><br><br>
		  <b>Product Name: <%=vproduct%></b><br><br>
		  
		   <%If vismultipleproduct = 1 then 
		   
				if isarray(vRecArray) then
		   %>
				   <b>Product Breakdown:</b><br>
			<%
				   For i = 0 to UBound(vRecArray,2)		   
						response.write(vRecArray(2,i) & " x " &  vRecArray(1,i) & "<br>")
				   next
		  		end if 
			%>
			 <br>
			 <%
		   end if 
		   %>
		  
		  </td>
        	
		</tr>
		</table>
        <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
		<tr>
		
		 <td width="8%" bgcolor="#CCCCCC"><nobr><b>Customer Code&nbsp;</b></nobr></td>
		  <td width="45%"  bgcolor="#CCCCCC"><b>Customer Name&nbsp;</b></td>
		  <td width="12%" bgcolor="#CCCCCC"><b>Week Van No.&nbsp;</b></td>
		  <td width="15%" bgcolor="#CCCCCC"><b>Weekend Van No.&nbsp;</b></td>
          <td width="18%" bgcolor="#CCCCCC" align="right"><b>Number of Products&nbsp;</b></td>
          <td width="12%" bgcolor="#CCCCCC" align="right"><b>Turnover&nbsp;</b></td>
          
        </tr>
				<tr>
				<%if isarray(recarray) then%>
				<% 				
			
				Totalturnover = 0.00
				vNumber = 0
				
				
				%>
				<%for i=0 to UBound(recarray,2)%>
				<td width="8%"><%=recarray(5,i)%>&nbsp;</td>
				<td width="45%"><b><%if recarray(0,i)<> "" then Response.Write recarray(0,i) end if%></b>&nbsp;</td>
				<td width="12%"><%=recarray(3,i)%>&nbsp;</td>
				<td width="15%"><%=recarray(4,i)%>&nbsp;</td>
				<td width="18%" align="right"><b><%if recarray(1,i)<> "" then Response.Write formatnumber(recarray(1,i),0) else Response.Write "0" end if%>&nbsp;</b></td> 
				<td width="12%" align="right"><b><%if recarray(2,i)<> "" then Response.Write formatnumber(recarray(2,i),2) else Response.Write "0.00" end if%>&nbsp;</b></td>
                    
          </tr>
          <%
       
				  if recarray(1,i)<> "" then 
						vNumber = vNumber + formatnumber(recarray(1,i),0)
				  End if
				  if recarray(2,i)<> "" then 
						Totalturnover = Totalturnover + formatnumber(recarray(2,i),2)
				  End if
				
          %>
          <%next%>
          <tr>
           <td colspan="4" align="right"><b>Grand Total</b>&nbsp;</td>
           <td width="18%" align="right" ><b>&nbsp;<%=formatnumber(vNumber,0)%>&nbsp;</b></td> 
           <td width="12%" align="right" ><b>&nbsp;<%=formatnumber(Totalturnover,2)%>&nbsp;</b></td> 
                    
          </tr>
          <%
          else%>
          <td colspan="6" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td> <% 
          end if%>
      
      </table>
	  
	 <%if isarray(vRecArray1) then%>
	 <br><br>
	  <%for j=0 to UBound(vRecArray1,2)
	  	
	     set retcol = objBakery.Display_Multiple_Products_By_Customer(fromdt,todt,"",vProductID,"",vRecArray1(0,j))
		 recarray = retcol("MultiProductsByCustomer")
	  %>
	   <b>Product Code: <%=vRecArray1(0,j)%></b><br>
	   <b>Product Name: <%=vRecArray1(1,j)%></b><br><br>
	  <b>
	  Product Breakdown:<br>
	  <%=vRecArray1(2,j)%> x <%=vproduct%><br><br>
	  </b>
	  
	  <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
		<tr>
		
		 <td width="8%" bgcolor="#CCCCCC"><nobr><b>Customer Code&nbsp;</b></nobr></td>
		  <td width="45%"  bgcolor="#CCCCCC"><b>Customer Name&nbsp;</b></td>
		  <td width="12%" bgcolor="#CCCCCC"><b>Week Van No.&nbsp;</b></td>
		  <td width="15%" bgcolor="#CCCCCC"><b>Weekend Van No.&nbsp;</b></td>
          <td width="18%" bgcolor="#CCCCCC" align="right"><b>Number of Products&nbsp;</b></td>
          <td width="12%" bgcolor="#CCCCCC" align="right"><b>Turnover&nbsp;</b></td>
          
        </tr>
				<tr>
				<%if isarray(recarray) then%>
				<% 				
			
				Totalturnover = 0.00
				vNumber = 0
				
				
				%>
				<%for i=0 to UBound(recarray,2)%>
				<td width="8%"><%=recarray(5,i)%>&nbsp;</td>
				<td width="45%"><b><%if recarray(0,i)<> "" then Response.Write recarray(0,i) end if%></b>&nbsp;</td>
				<td width="12%"><%=recarray(3,i)%>&nbsp;</td>
				<td width="15%"><%=recarray(4,i)%>&nbsp;</td>
				<td width="18%" align="right"><b><%if recarray(1,i)<> "" then Response.Write formatnumber(recarray(1,i),0) else Response.Write "0" end if%>&nbsp;</b></td> 
				<td width="12%" align="right"><b><%if recarray(2,i)<> "" then Response.Write formatnumber(recarray(2,i),2) else Response.Write "0.00" end if%>&nbsp;</b></td>
                    
          </tr>
          <%
       
				  if recarray(1,i)<> "" then 
						vNumber = vNumber + formatnumber(recarray(1,i),0)
				  End if
				  if recarray(2,i)<> "" then 
						Totalturnover = Totalturnover + formatnumber(recarray(2,i),2)
				  End if
				
          %>
          <%next%>
          <tr>
           <td colspan="4" align="right"><b>Grand Total</b>&nbsp;</td>
           <td width="18%" align="right" ><b>&nbsp;<%=formatnumber(vNumber,0)%>&nbsp;</b></td> 
           <td width="12%" align="right" ><b>&nbsp;<%=formatnumber(Totalturnover,2)%>&nbsp;</b></td> 
                    
          </tr>
          <%
          else%>
          <td colspan="6" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td> <% 
          end if%>
      
      </table>
	  <%
	  set retcol = nothing
	  %>
	   <br><br>
	  <%
	  next
	  end if
	  %>
	  
	  
	  
	  <p align="center"><input type="button" value=" Back " onClick="history.back()"></p>
    </td>
  </tr>
</table>
</center>
</div>
</body>
<%
set objBakery = nothing
if IsArray(recarray) then erase recarray %>
</html>