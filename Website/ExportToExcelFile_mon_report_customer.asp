<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
	Server.ScriptTimeout=6000
	fromdt=request("fromdt")
	todt=request("todt")
	
	if isdate(fromdt) and isdate(todt) then
		set objBakery = server.CreateObject("Bakery.reports")
		objBakery.SetEnvironment(strconnection)
		set retcol = objBakery.Display_customer(fromdt,todt)
		recarray = retcol("Customer")
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=mon_report_customer.xls" 
	%>
	<table border=1 width="3500" cellpadding="0" cellspacing="0">
		<tr>
          <td width="100%" colspan="40"><b>Customer Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
        <tr>
          <td width="120"  bgcolor="#CCCCCC"><b>Customer Code&nbsp;</b></td>
		  <td width="100" bgcolor="#CCCCCC"><b>Old Code</b></td>
          <td width="300" bgcolor="#CCCCCC"><b>Customer Name&nbsp;</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Nick Name</b></td>
		  <td width="100" bgcolor="#CCCCCC"><b>Weekly Van No&nbsp;</b></td>
		  <td width="100" bgcolor="#CCCCCC"><b>Weekend Van No&nbsp;</b></td>
		  <td width="300" bgcolor="#CCCCCC"><b>Address&nbsp;</b></td>
		  
		  <td width="90" bgcolor="#CCCCCC"><b>Town</b></td>
		  <td width="150" bgcolor="#CCCCCC"><b>Post Code&nbsp;</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Tel No</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Fax</b></td>
		  <td width="100" bgcolor="#CCCCCC"><b>Type&nbsp;</b></td>
		  <td width="75" bgcolor="#CCCCCC"><b>Year&nbsp;</b></td>
		  <td width="170" bgcolor="#CCCCCC"><b>Customer Of&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>No of Invoices&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Terms&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><nobr><b>Sales Person&nbsp;</b></nobr></td>
		  <td width="120" bgcolor="#CCCCCC"><nobr><b>Account Manager&nbsp;</b></nobr></td>
		  <td width="175" bgcolor="#CCCCCC"><b>Notes&nbsp;</b></td>
		  <td width="175" bgcolor="#CCCCCC"><b>Category&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Standing Order&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Purchase Order&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Group Name&nbsp;</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Day-to-day Contact Name</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Day-to-day Contact Phone </b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Decision maker Name</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>Decision maker Phone </b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Default Price</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Formula (if F)</b></td>
		   <td width="90" bgcolor="#CCCCCC"><b>Priceless Invoice/Credit</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>VC Customer</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>SVC Customer</b></td>
		    <td width="90" bgcolor="#CCCCCC"><b>Minimum Order Amount</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Group Minimum Order</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Delivery Charge Option</b></td>
          <td width="150" bgcolor="#CCCCCC" align="right"><b>Total Number of Deliveries&nbsp;</b></td>
          <td width="150" bgcolor="#CCCCCC" align="right"><b>Gross Turnover&nbsp;</b></td>
          <td width="150" bgcolor="#CCCCCC" align="right"><b>VAT&nbsp;</b></td>
          <td width="150" bgcolor="#CCCCCC" align="right"><b>Net Turnover&nbsp;</b></td>
          <td width="180" bgcolor="#CCCCCC" align="right"><b>Average turnover per delivery&nbsp;</b></td>
        </tr>
		
		<%if isarray(recarray) then%>
		<% 				
		TotalDeliveries = 0
		Totalturnover = 0.00
		Averageturnover = 0.00
		TotalVAT = 0.00
		TotalNetTO = 0.00		
		%>
		<%for i=0 to UBound(recarray,2)%>
         <tr>
		  <td><%=recarray(0,i)%><% 'Customer Code %>&nbsp;</td>
		  <td><%=recarray(22,i)%><% 'Customer Old code%>&nbsp;</td>
          <td><%=recarray(1,i)%><% 'Customer Name %>&nbsp;</td>
		  <td><%=recarray(21,i)%><% 'Nickname%>&nbsp;</td>
		  <td><%=recarray(5,i)%><% 'Weekly Van No%>&nbsp;</td>
		  <td><%=recarray(6,i)%><% 'Weekend Van No %>&nbsp;</td>
		  <td><%=recarray(7,i)%>&nbsp;<%=recarray(8,i)%><% 'Address1 & Address2 %>&nbsp;</td>
		 
		  <td width="90"><%=recarray(9,i)%><% 'Town%>&nbsp;</td>
		  <td><%=recarray(10,i)%><% 'Post Code %>&nbsp;</td>
		  <td width="90"><%=recarray(11,i)%><% 'Tel No%>&nbsp;</td>
		   <td><%=recarray(23,i)%><% 'Fax%>&nbsp;</td>
		  <td><%=recarray(12,i)%>&nbsp;</td>
		  <td><%=recarray(13,i)%>&nbsp;</td>
		  <td><%=recarray(14,i)%>&nbsp;</td>
		  <td><%=recarray(24,i)%><% 'No. of invoices %>&nbsp;</td>
		  <td><%=recarray(15,i)%><% 'Terms %>&nbsp;</td>
		   <td><%=recarray(16,i)%><% 'Sales Person %>&nbsp;</td>
		  <td><%=recarray(34,i)%><% 'Account Manager %>&nbsp;</td>
		  <td><%=trim(recarray(17,i))%>&nbsp;</td>
		  <td><%=trim(recarray(35,i))%>&nbsp;</td>
		  <td><%=recarray(18,i)%>&nbsp;</td>
		  <td><%=recarray(19,i)%>&nbsp;</td>
		  <td><%=recarray(20,i)%><% 'Group Name%>&nbsp;</td>
		  <td><%=recarray(25,i)%><% 'Day-to-day contact � name%>&nbsp;</td>
		  <td><%=recarray(26,i)%><% 'Day-to-day contact � phone %>&nbsp;</td>
		  <td><%=recarray(27,i)%><% 'Decision maker � name %>&nbsp;</td>
		  <td><%=recarray(28,i)%><% 'Decision maker � phone%>&nbsp;</td>
		  <td><%=recarray(29,i)%><% 'Default price %>&nbsp;</td>
		  <%if recarray(29,i)="F" then%>
		  <td bgcolor="#E6E6E6"><%=recarray(30,i)%><% 'Formula (if F)%>&nbsp;</td>
		  <%else%>
		  <td><%=recarray(30,i)%><% 'Formula (if F)%>&nbsp;</td>
		  <%end if%>
		  <td align="center">
		  <%
		  if recarray(31,i)="1" then
		  	response.write("Yes")
		  else
		  	response.write("No")
		  end if
		  %><% 'Priceless Invoice/Credit%>&nbsp;</td>
			  <td><%=recarray(36,i)%><% 'VC Customer%>&nbsp;</td>
		  <td><%=recarray(37,i)%><% 'SVC Customer%>&nbsp;</td>
		  
		   <td><%=recarray(38,i)%><% 'MinimumOrder%>&nbsp;</td>
		   <td><%=recarray(39,i)%><% 'MinimumGroupOrder%>&nbsp;</td>
		   <td><%=recarray(40,i)%><% 'deliverychargepno%>&nbsp;</td>
		  
          <td align="right" bgcolor="#CCCCCC"><%=recarray(2,i)%></td> 
          <td align="right"><%if recarray(3,i)<> "" then Response.Write formatnumber(recarray(3,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(33,i)<> "" then Response.Write formatnumber(recarray(33,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(32,i)<> "" then Response.Write formatnumber(recarray(32,i),2) else Response.Write "0.00" end if%></td>          
          <td align="right" bgcolor="#CCCCCC">
          <%
          	if recarray(3,i) <> "" and recarray(2,i) <> "" then
           		if (CDbl(recarray(3,i)) > CInt(recarray(2,i))) And CInt(recarray(2,i)) > 0 then
		           Response.Write formatnumber(CDbl(recarray(3,i))/CInt(recarray(2,i)),2)
		        else
		         	Response.Write "0.00" 
		        end if
	        else
	         	Response.Write "0.00" 
           end if
           %>
           </td>
          </tr>
          <%
          if recarray(2,i) <> "" then
             TotalDeliveries = TotalDeliveries + recarray(2,i)
          end if
          if recarray(3,i)<> "" then 
		  	Totalturnover = Totalturnover + formatnumber(recarray(3,i),2)
		  End if
		  if recarray(4,i)<> "" then
		  	Averageturnover = Averageturnover +  formatnumber(recarray(4,i),2)
		  End if
		  
          if recarray(33,i)<> "" then 
			TotalVAT = TotalVAT + formatnumber(recarray(33,i),2)
		  End if		
		  
          if recarray(32,i) <> "" then 
			TotalNetTO = TotalNetTO + formatnumber(recarray(32,i),2)
		  End if
		  		  
          %>
          <%
          
          Response.Flush()
          next%>
          <tr>
           	<td colspan=36 align="right"><b>Grand Total&nbsp;</b></td>
          	<td align="right" bgcolor="#CCCCCC"><b><%=TotalDeliveries%></b>&nbsp;</td> 
          	<td align="right"><b><%=formatNumber(Totalturnover,2)%></b>&nbsp;</td>
          	<td align="right"><b><%=formatNumber(TotalVAT,2)%></b>&nbsp;</td>
          	<td align="right"><b><%=formatNumber(TotalNetTO,2)%></b>&nbsp;</td>
          	<td align="right" bgcolor="#CCCCCC"><b><%'=formatNumber(Averageturnover,2)%>&nbsp;</b></td>
          </tr>
          <%
          else%>
          <tr><td colspan="36" width="100%" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td></tr>
		  <%end if%>
     	 </table>	  
		  <%
		  
	end if
%>
