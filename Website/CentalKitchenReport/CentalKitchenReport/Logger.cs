﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;

namespace CentalKitchenReport
{
    public class Logger
    {
        protected EventLog log;

        private string loggerName = ConfigurationManager.AppSettings["LoggerName"];
        private string loggerSource = ConfigurationManager.AppSettings["LoggerSource"];

        public Logger()
        {
            if (!EventLog.Exists(loggerName))
            {
                EventLog.CreateEventSource(loggerSource, loggerName);
            }
            log = new EventLog(loggerName);
            log.Source = loggerSource;
        }

        /// <summary>
        /// Writes the log.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="type">The type.</param>
        public virtual void WriteLog(string message, EventLogEntryType type)
        {
            log.WriteEntry(message, type);
        }
    }
}
