﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Data;
using System.Net.Mail;

namespace CentalKitchenReport
{ 
    class ExcelReportGenerator
    {       
        public void GenerateCentalKitchenReport()
        {
            Logger log = new Logger();
            try
            {                
                string sPath = string.Empty;
                string m_sFileName = string.Empty;
                object emptyValue = System.Reflection.Missing.Value;
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                xlApp = new Excel.ApplicationClass();
                xlWorkBook = xlApp.Workbooks.Add(emptyValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                Excel.Range selectedRange = null;
                Common ObjDB = new Common();
                DataTable dtReportDetails = null;
                string[] arrExcelColumns = null;
                int colCount = 1;
                string columnName = string.Empty;
                string[] arrCustID = null;
                string sCustName = string.Empty;
                int nStartRow = int.Parse(ConfigurationSettings.AppSettings["StartRow"].ToString());

                sPath = ConfigurationSettings.AppSettings["ReportPath"];
                arrExcelColumns = ConfigurationSettings.AppSettings["ExcelColumn_Letters"].Split(',');

                m_sFileName = "CentalKitchenReport_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".xls";

                GenerateReportHeader(xlWorkSheet, selectedRange);

                dtReportDetails = ObjDB.ListCentalKitchenReportDetails();

                if (dtReportDetails != null)
                {
                    if (dtReportDetails.Rows.Count > 0)
                    {
                        //Generate Table Header


                        GenerateTableHeader(xlWorkSheet, selectedRange, nStartRow, arrExcelColumns[0].ToString(), ConfigurationSettings.AppSettings["HeaderText_Pno"], 25);
                        GenerateTableHeader(xlWorkSheet, selectedRange, nStartRow, arrExcelColumns[1].ToString(), ConfigurationSettings.AppSettings["HeaderText_Name"], 50);
                        GenerateTableHeader(xlWorkSheet, selectedRange, nStartRow, arrExcelColumns[2].ToString(), ConfigurationSettings.AppSettings["HeaderText_Category"], 20);
                        GenerateTableHeader(xlWorkSheet, selectedRange, nStartRow, arrExcelColumns[3].ToString(), ConfigurationSettings.AppSettings["HeaderText_NetUnits"], 10);
                        GenerateTableHeader(xlWorkSheet, selectedRange, nStartRow, arrExcelColumns[4].ToString(), ConfigurationSettings.AppSettings["HeaderText_Price"], 10);

                        foreach (DataColumn dtColumn in dtReportDetails.Columns)
                        {
                            if (colCount >= int.Parse(ConfigurationSettings.AppSettings["CustHeader_StartValue"].ToString()))
                            {
                                columnName = dtColumn.ColumnName;
                                arrCustID = columnName.Split('_');

                                //GetCustomerName
                                sCustName = ObjDB.GetCustomerNamebyCNo(int.Parse(arrCustID[1].ToString()));
                                GenerateTableHeader(xlWorkSheet, selectedRange, nStartRow, arrExcelColumns[colCount - 1].ToString(), sCustName.ToString(), 10);
                            }


                            colCount++;
                        }

                        //Generate Table Data

                        for (int i = 0; i < dtReportDetails.Rows.Count; i++)
                        {
                            for (int j = 0; j < dtReportDetails.Columns.Count; j++)
                            {
                                //if (j != 2)
                                //    GenerateTableCell(xlWorkSheet, selectedRange, i + nStartRow + 1, arrExcelColumns[j].ToString(), dtReportDetails.Rows[i][j].ToString(), Excel.XlHAlign.xlHAlignLeft);
                                //else
                                //{
                                //    if (j == 3 || j == 4)
                                //        GenerateTableCell(xlWorkSheet, selectedRange, i + nStartRow + 1, arrExcelColumns[j].ToString(), string.Empty, Excel.XlHAlign.xlHAlignRight);
                                //    else
                                //        GenerateTableCell(xlWorkSheet, selectedRange, i + nStartRow + 1, arrExcelColumns[j].ToString(), string.Empty, Excel.XlHAlign.xlHAlignLeft);
                                //}

                                if (j == 4)
                                    GenerateTableCell(xlWorkSheet, selectedRange, i + nStartRow + 1, arrExcelColumns[j].ToString(), string.Empty, Excel.XlHAlign.xlHAlignRight);
                                else
                                    GenerateTableCell(xlWorkSheet, selectedRange, i + nStartRow + 1, arrExcelColumns[j].ToString(), dtReportDetails.Rows[i][j].ToString(), Excel.XlHAlign.xlHAlignLeft);


                            }
                        }
                    }
                    else
                    {
                        GenerateEmptyDataRow(xlWorkSheet, selectedRange);
                    }
                }
                else
                {
                    GenerateEmptyDataRow(xlWorkSheet, selectedRange);
                }

                xlWorkBook.SaveAs(sPath + m_sFileName, Excel.XlFileFormat.xlWorkbookNormal, emptyValue, emptyValue, emptyValue, emptyValue, Excel.XlSaveAsAccessMode.xlExclusive, emptyValue, emptyValue, emptyValue, emptyValue, emptyValue);
                xlWorkBook.Close(true, emptyValue, emptyValue);
                xlApp.Quit();

                //Send Email

                if (File.Exists(sPath + m_sFileName))
                    SendEmail(m_sFileName);

                log.WriteLog("Successfully created report " + DateTime.Now.ToShortDateString(), EventLogEntryType.SuccessAudit);
            }
            catch (Exception ex)
            {
                log.WriteLog(ex.Message, EventLogEntryType.Error);
            }

        }


        /// <summary>
        /// Display report Header
        /// </summary>
        /// <param name="workSheet"></param>
        private void GenerateReportHeader(Excel.Worksheet workSheet, Excel.Range selectedRange)
        {
            Logger log = new Logger();
            try
            {
                selectedRange = workSheet.get_Range("A1", "A1");
                selectedRange.Rows.RowHeight = 20;
                selectedRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                selectedRange.FormulaR1C1 = ConfigurationSettings.AppSettings["ReportTitle"].ToUpper() + " FOR "
                    + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month).ToUpper();
                selectedRange.Font.Bold = true;

                //Add created Date
                selectedRange = workSheet.get_Range("A2", "A2");
                selectedRange.Rows.RowHeight = 20;
                selectedRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                selectedRange.FormulaR1C1 = "Created Date - " + DateTime.Now.ToShortDateString();
                selectedRange.Font.Bold = true;
            }
            catch (Exception ex)
            {
                log.WriteLog(ex.Message, EventLogEntryType.Error);
            }

        }

        private void GenerateEmptyDataRow(Excel.Worksheet workSheet, Excel.Range selectedRange)
        {

            selectedRange = workSheet.get_Range("C4", "C4");
            selectedRange.Rows.RowHeight = 30;
            selectedRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            //selectedRange.Font.Color =  
            selectedRange.FormulaR1C1 = ConfigurationSettings.AppSettings["EmtryDataMessage"];
            selectedRange.Font.Bold = true;
            selectedRange.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);


        }

        private void GenerateTableHeader(Excel.Worksheet workSheet, Excel.Range selectedRange, int nRow, string sColumn, string sText, int nColumnWidth)
        {
            selectedRange = workSheet.get_Range(sColumn + nRow, sColumn + nRow);
            selectedRange.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
            selectedRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            selectedRange.Rows.RowHeight = 30;
            selectedRange.ColumnWidth = nColumnWidth;
            selectedRange.FormulaR1C1 = sText;
            selectedRange.Cells.WrapText = true;
            selectedRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
        }

        private void GenerateTableCell(Excel.Worksheet workSheet, Excel.Range selectedRange, int nRow, string sColumn, string sText, Excel.XlHAlign textAlignment)
        {
            selectedRange = workSheet.get_Range(sColumn + nRow, sColumn + nRow);
            selectedRange.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
            selectedRange.HorizontalAlignment = textAlignment; // Excel.XlHAlign.xlHAlignLeft;
            selectedRange.Rows.RowHeight = 20;
            selectedRange.FormulaR1C1 = sText;
            selectedRange.Cells.WrapText = true;

        }

        private void SendEmail(string sAttachementName)
        {
            Logger log = new Logger();
            string sFilePath = ConfigurationSettings.AppSettings["ReportPath"] + sAttachementName;
            MailMessage mail = new MailMessage();
            SmtpClient emailClient = new SmtpClient(ConfigurationSettings.AppSettings["SMTPServer"].ToString());
            emailClient.Credentials = new System.Net.NetworkCredential(ConfigurationSettings.AppSettings["SMTPUID"].ToString(), ConfigurationSettings.AppSettings["SMTPPW"].ToString());
            try
            {
                mail.To.Add(new MailAddress(ConfigurationSettings.AppSettings["MailTo"].ToString()));
                
                if(!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["MailCC"]))
                    mail.CC.Add(new MailAddress(ConfigurationSettings.AppSettings["MailCC"].ToString()));

                if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["MailBCC"]))
                    mail.Bcc.Add(new MailAddress(ConfigurationSettings.AppSettings["MailBCC"].ToString()));

                mail.From = new MailAddress(ConfigurationSettings.AppSettings["MailFrom"].ToString());
                mail.Subject = ConfigurationSettings.AppSettings["MailSubject"] + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month).ToString();
                mail.IsBodyHtml = true;
                System.Net.Mail.Attachment attachment = new Attachment(sFilePath); //create the attachment
                mail.Attachments.Add(attachment);	//add the attachment


                emailClient.Send(mail);
            }
            catch (Exception ex)
            {
                log.WriteLog(ex.Message, EventLogEntryType.Error);
            }
            finally
            {
                mail.Dispose();
            }

        }

      
    }
}
