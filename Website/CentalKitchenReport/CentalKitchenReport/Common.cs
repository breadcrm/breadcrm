﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace CentalKitchenReport
{
    class Common
    {
        string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        SqlConnection conn;
        private string strSql = string.Empty;
        string message = string.Empty;


        /// <summary>
        /// This method is used to List all Cental Kitchen Report Details
        /// </summary>
        public DataTable ListCentalKitchenReportDetails()
        {
            conn = new SqlConnection(strConnectionString);
            DataTable dtReport = new DataTable();
            int nRecOut = 0;

            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                strSql = "exec spGenerateCKReportData " + nRecOut;

                SqlDataAdapter daReport = new SqlDataAdapter(strSql, conn);
                dtReport = new DataTable();
                daReport.Fill(dtReport);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                conn.Close();
            }


            return dtReport;
        }

          /// <summary>
          /// This method is used to get the customer Name using Customer No
          /// </summary>
          /// <param name="cno"></param>
          /// <returns></returns>
        public string GetCustomerNamebyCNo(int cno)
        {
            conn = new SqlConnection(strConnectionString);
            DataTable dtCust = null;
            string sCustName = string.Empty;

            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                strSql = "exec GetCustomerNamebyCNo " + cno;

                SqlDataAdapter daCustomer = new SqlDataAdapter(strSql, conn);
                dtCust = new DataTable();
                daCustomer.Fill(dtCust);
                if (dtCust != null)
                {
                    if (dtCust.Rows.Count > 0)
                    {
                        sCustName = dtCust.Rows[0]["CName"].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                conn.Close();
            }


            return sCustName;
        }
    }
}
