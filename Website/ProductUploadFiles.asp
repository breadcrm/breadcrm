<%@  language="VBScript" %>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<!--#include file="Includes/Loader.asp"-->
<% 
dim validfilestypes
validfilestypes = Array("pdf","jpg","jpeg","doc","docx","xls","xlsx","ppt","pptx","bmp","gif","png")

LCID = 2057 
Server.ScriptTimeout = 16000
Dim ProductNo,straction, sMsg, result,	strErrorMsg
sMsg=""
strErrorMsg=""
Set obj = CreateObject("Bakery.Product")
obj.SetEnvironment(strconnection)

ProductNo = Request("PNo")
straction = Request("action")
if (ProductNo="") then
	' File Upload
	Response.Buffer = True
	' load object
	Dim load
	  Set load = new Loader
	' calling initialize method
	load.initialize
	
	' File binary data
	Dim fileData
	  fileData = load.getFileData("file")
	' File name
	Dim fileName
	  fileName = load.getFileName("file")

	straction=load.getValue("action")
	If straction = "upload" then
		ProductNo = trim(load.getValue("PNo"))
		if (fileName<>"") then			
			fileextno=InStrRev(fileName,".")
		  	fileext=Len(fileName)-fileextno
		  	fileNameext=LCase(Right(fileName,fileext))
			
			If (in_array(fileNameext,validfilestypes)) Then
				fileName=RemoveSpecialCharacters(fileName)
				fileName=ProductNo	& "-" & fileName 		
				set result = obj.SaveProductUploadFile(ProductNo,filename)
				if result("Sucess") = "OK" then
					sMsg = "File successfully uploaded."
					pathToFile = Server.mapPath("ProductUploadFiles") & "\" & fileName
					'Uploading file data
					fileUploaded = load.saveToFile ("file", pathToFile)
				elseif result("Sucess") = "Already Exists" then
					strErrorMsg = "File name already exists."
				End If
			else
				strvalidfilestypes=Join(validfilestypes,", ")
				strErrorMsg = "Invalid File. Please upload a File with extension:<br>" & strvalidfilestypes
			end if
		else
			strErrorMsg = "Please browse for a file before clicking the upload button."
		end if 		
	End if
	Set load = Nothing
end if

If straction = "Delete" then
	strfileid=request("fileid")
	strfilename=request("filename")
	set result = obj.DeleteProductUploadFile(strfileid)
	if result("Sucess") = "OK" then	
		sMsg = "File deleteted successfully"
		pathToFile = Server.mapPath("ProductUploadFiles") & "\" & strfilename
		Dim fso	
		Set fso = Server.CreateObject("Scripting.FileSystemObject")		
		On Error Resume Next		
		Call fso.DeleteFile(pathToFile, True)
		Set fso = Nothing			
	End If
End if
Dim sProductUploadFile
set objProductMsg = obj.GetProductUploadFiles(ProductNo)
sProductUploadFile = objProductMsg("ProductUploadFiles")
set obj=nothing
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script language="javascript" type="text/javascript">
function onClick_Upload()
{
	if (document.frmProductUploadFiles.file.value=="")
	{
		alert("Please browse for a file before clicking the upload button.")
		document.frmProductUploadFiles.file.focus();
    	return false;
	}
	else
	{
		var validFilesTypes=["pdf","jpg","jpeg","doc","docx","xls","xlsx","ppt","pptx","bmp","gif","png"];
		var filename =document.frmProductUploadFiles.file.value;
      	var ext=filename.substring(filename.lastIndexOf(".")+1,filename.length).toLowerCase();
		var isValidFile = false;
	  	for (var i=0; i<validFilesTypes.length; i++)
	  	{
			if (ext==validFilesTypes[i])
			{
				isValidFile=true;
				break;
			}
	  	}
		if (!isValidFile)
        {
			strvalidFilesTypes=validFilesTypes.join(", ")
			alert("Invalid File. Please upload a File with extension:\n"+ strvalidFilesTypes);
			document.frmProductUploadFiles.file.focus();
			return false;
		}
		
		document.frmProductUploadFiles.submit();
	}	
}

function DeleteConfirmation(vfileid,vfilename) 
{
	var result = confirm('Are you sure you want to delete?');
	if (result) 
	{
		document.getElementById('action').value = "Delete";
		document.getElementById('fileid').value = vfileid;
		document.getElementById('filename').value = vfilename;
		document.frmProductFiles.submit();
	}
	else 
		return false;
}
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">    
	<table border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
	<form name="frmProductUploadFiles" action="ProductUploadFiles.asp" method="post"  ENCTYPE="multipart/form-data">
	<input type="hidden" name="action" value="upload" />
	<input type="hidden" name="PNo" value="<%=ProductNo%>" />
	  <tr>
		<td><nobr><strong>Upload File:&nbsp;</strong></nobr></td>
		<td width="5"></td>
		<td><INPUT name="file" onKeyPress="if ((event.keyCode > 1 && event.keyCode < 255 )) event.returnValue = false;" TYPE="file" size="46"></td>
		<td width="5"></td>
		<td><INPUT type="submit" name="upload" onClick="return onClick_Upload();" style="font-family: Verdana;font-size: 8pt; width:60px" value="Upload"></td>
	  </tr>	  
	</form> 
	</table>
	<br>
   	<table bgcolor="#666666" align="center" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
	<%if sMsg<>"" Then %>
	<tr height="25" bgcolor="#FFFFFF">
		<td colspan="3" align="center" style="color:#006600;">
			<strong><%=sMsg%></strong>
		</td>
	</tr>
	<%
	elseif strErrorMsg<>"" Then 
	%>
	<tr height="25" bgcolor="#FFFFFF">
		<td colspan="3" align="center" style="color:#FF0000;">
			<strong><%=strErrorMsg%></strong>
		</td>
	</tr>
	<%End If%>
	
	<%if isArray(sProductUploadFile) Then%>	
	<tr height="20" bgcolor="#CCCCCC">
		<td style="width: 350px;">&nbsp;<strong>File Name</strong></td>
		<td style="width: 100px;">&nbsp;<strong>Uploaded Date</strong></td>
		<%if session("UserType") = "A" or session("UserType") = "S" then%>
		<td align="center" style="width: 80px;">
			<strong>Action</strong>
		</td>
		<%End if%>    
	</tr>
	<%For i = 0 to UBound(sProductUploadFile,2)%>
	<tr height="24" bgcolor="#FFFFFF">
		<td style="padding-left: 5px; width: 250px; word-wrap: break-word;">
			<a href="ProductUploadFiles/<%=sProductUploadFile(2,i)%>" target="_blank"><%=sProductUploadFile(2,i)%></a>
		</td>
		<td style="padding-left: 5px; width: 150px;">
			<%=sProductUploadFile(3,i)%>
		</td>	   
		<%if session("UserType") = "A" or session("UserType") = "S" then%>
			<td align="center"><a onClick="return confirm('Are you sure you want to delete?')" href="ProductUploadFiles.asp?fileid=<%=sProductUploadFile(0,i)%>&PNo=<%=ProductNo%>&action=Delete&filename=<%=sProductUploadFile(2,i)%>"><img width="58" height="19" src="images/btndelete.png" border="0" /></a></td>
		<%End if%>	
	</tr>
	<%
	Next
	Else
	%>
	<tr height="25" bgcolor="#FFFFFF">
		<td colspan="3" align="center" style="color:#FF0000;">
			<strong>There are no files.</strong>
		</td>
	</tr>
	<%End If%>
	</table>
<br>    
</body>
</html>