<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title>Sales/Credit Report</title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--

function ExportToExcelFile(){
	fromdate=document.form.txtfrom.value
	todate=document.form.txtto.value
	customer=document.form.cus.value
	CustomerGroup=document.form.CustomerGroup.value
	document.location="ExportToExcelFile_report_sale_credit.asp?customer=" + customer + "&CustomerGroup=" + CustomerGroup + "&fromdt=" + fromdate + "&todt=" +todate
}

function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function ValidateForm(){
	if (document.form.customer.value=="" && document.form.CustomerGroup.value=="")
	{
		alert ("Please select customer(s) or Customer Group.");
		document.form.customer.focus();
		return  false;
	}
	if (document.form.customer.value!="" && document.form.CustomerGroup.value!="")
	{
		alert ("Please select customer(s) or Customer Group only.");
		document.form.customer.focus();
		return  false;
	}
	return  true;
}

function onClick_CSV() {   
   
    fromdate = document.form.txtfrom.value
    todate = document.form.txtto.value
    customer = document.form.cus.value
    CustomerGroup = document.form.CustomerGroup.value
    window.open("report_sale_credit_CSV.asp?customer=" + customer + "&CustomerGroup=" + CustomerGroup + "&fromdt=" + fromdate + "&todt=" + todate, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
    //document.location = "report_sale_credit_CSV.asp?customer=" + customer + "&CustomerGroup=" + CustomerGroup + "&fromdt=" + fromdate + "&todt=" + todate

}


//-->
</SCRIPT>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;

<%
Response.Buffer
dim objBakery
dim recarray
dim retcol
dim fromdt, todt, i,Totalturnover, strCustomer, arCustomerGroup,vCustomerGroup,avCustomer,j
Dim arCustomer,CustomerCol,vCustomer
Dim fileName, sAction
stop
sAction = Request.QueryString("Action")
vCustomer = Request.Form("customer")
vCustomerGroup=Request.Form("CustomerGroup")
if vCustomerGroup<>"" then
	vCustomer=""
end if
fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")


if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
  todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)

set CustomerCol = objBakery.Display_CustomerList()
arCustomer = CustomerCol("Customer")
set CustomerCol = nothing

set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)

set CustomerCol = objBakery.Display_CustomerGroupList()
arCustomerGroup = CustomerCol("Customer")
set CustomerCol = nothing


if isdate(fromdt) and isdate(todt)  then
	set retcol = objBakery.Display_Sales_Credit(fromdt,todt,vCustomer,vCustomerGroup)
	recarray = retcol("CustSaleCredit")
	'strWeekVanName = retcol("WeekVanName")
	'strWeekVanNo = retcol("WeekVanNo")
	'strWeekEndVanName = retcol("WeekEndVanName")
	'strWeekEndVanNo = retcol("WeekEndVanNo")
	'strCustomer=retcol("CusName")
	set retcol = nothing
end if




set objBakery = nothing
%>
<div align="center">
<center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
 <tr>
    <td width="100%"><b><font size="3">Sales / Credit Report <br>
      &nbsp;</font></b>
      <form method="post" action="report_sale_credit.asp" name="form">
	  <input type="hidden" name="cus" value="<%=vCustomer%>">
	   
	  <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
       
		<tr>
		  <%
		  if vCustomer<>"" then
				avCustomer=split(vCustomer,",")
		  end if
		  %>
		  <td width="75"><b>Customer</b></td>
         <td>
		  	
			<select size="5" name="customer" multiple="multiple" style="font-family: Verdana; font-size: 8pt">
            <option value="">Select</option>
			<option value="-99" <% if (vCustomer="-99") then %> selected="selected" <% end if %>>All Customers</option>
            <%if IsArray(arCustomer) then%>
            <%for i = 0  to UBound(arCustomer,2)%>
				<%if isArray(avCustomer) then%>
				<%flag="true"%>
				<%for j=0 to ubound(avCustomer)%>					
					<%if trim(arCustomer(0,i))= Trim(avCustomer(j)) then%>
						<option value="<%=arCustomer(0,i)%>" Selected><%=arCustomer(1,i)%></option>
						<%flag="false"%>
					<%end if %>										
				<%next%>
				<%if flag="true" then%>
					<option value="<%=arCustomer(0,i)%>"><%=arCustomer(1,i)%></option>				
				<%end if%>
            	<%else%>
					<option value="<%=arCustomer(0,i)%>" <%if trim(arCustomer(0,i))= Trim(vCustomer) then response.write " Selected"%>><%=arCustomer(1,i)%></option>
				<%end if%>
			<%next%>
            <%end if%>
			
          </select>
		  </td>
		  <td colspan="3">
		 	<table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
			<tr>
			<td><b>OR</b></td>
			<td width="130" align="right"><b>Customer Group</b></td>
			<td>
			<select size="1" name="CustomerGroup" style="font-family: Verdana; font-size: 8pt">
            <option value="">Select</option>
            <%if IsArray(arCustomerGroup) then%>
            <%for i = 0  to UBound(arCustomerGroup,2)%>
            <option value="<%=arCustomerGroup(0,i)%>" <%if trim(arCustomerGroup(0,i))= Trim(vCustomerGroup) then response.write " Selected"%>><%=arCustomerGroup(1,i)%></option>
            <%next%>
            <%end if%>
          	</select>
			</td>
			</tr>
			</table>
		  </td>
		
        </tr>
		 <tr>
		<td>&nbsp;</td>
		<td><font color="#993300">If you want to select more than one customer press "<b>Ctrl</b>" and select the customers.</font></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		</tr>
        <tr>

           <td width="50"><b>From:</b></td>
						<td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom size="12" onFocus="blur();" name="txtfrom" value="<%=replace(fromdt,"from","")%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					<td width="50"><b>To:</b></td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=replace(todt,"to","")%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td><td>
            <input type="submit" value="Search" onClick="return ValidateForm()" name="Search" style="font-family: Verdana; font-size: 8pt">&nbsp;&nbsp;
            <input type="button" value="Generate CSV File" onclick="onClick_CSV()"   name="CSV" style="font-family: Verdana; font-size: 8pt">
            </td>
        </tr>
		
        <tr>
          <td colspan="5" align="right" height="30">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>
       </table>
	  </form>
	   <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
      <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%" colspan="11"><b>Sales/Credit Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
        <tr height="24">
          <td width="5%"  bgcolor="#CCCCCC"><b>Customer No</b></td>
		  <td width="20%"  bgcolor="#CCCCCC"><b>Customer Name</b></td>
          <td width="8%"  bgcolor="#CCCCCC"><b>Invoice/Credit Date</b></td>
		  <td width="8%"  bgcolor="#CCCCCC"><b>Invoice/Credit No</b></td>
		  <td width="8%"  bgcolor="#CCCCCC"><b>Product No</b></td>
		  <td width="20%"  bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td width="6%" bgcolor="#CCCCCC" align="right"><b>Ordered Qty</b></td>
          <td width="6%" bgcolor="#CCCCCC" align="right"><b>Credit Qty</b></td>
		  <td width="6%" bgcolor="#CCCCCC" align="right"><b>Unit Price</b></td>
		  <td width="6%" bgcolor="#CCCCCC" align="right"><b>Total Price</b></td>
		  <td width="6%" bgcolor="#CCCCCC" align="right"><b>Line Total </b></td>
        </tr>
			
				<%
				if isarray(recarray) then			
				Totalturnover = 0.00

				for i=0 to UBound(recarray,2)
				if (i mod 100=0) then
					Response.Flush()
				end if
				%>
		  <tr height="22">
		  <td><b><%=recarray(0,i)%></b></td>
		  <td><b><%if recarray(1,i)<> "" then Response.Write recarray(1,i) end if%></b>&nbsp;</td>
		  <td ><b><%if recarray(2,i)<> "" then Response.Write recarray(2,i) end if%></b>&nbsp;</td>
		  <td ><b><%if recarray(3,i)<> "" then Response.Write recarray(3,i) end if%></b>&nbsp;</td>
		  <td><b><%if recarray(4,i)<> "" then Response.Write recarray(4,i) end if%></b>&nbsp;</td>
		  <td><b><%if recarray(5,i)<> "" then Response.Write recarray(5,i) end if%></b>&nbsp;</td>
		  
          <td align="right"><b><%if recarray(6,i)<> "" then Response.Write formatnumber(recarray(6,i),0) else Response.Write "0" end if%></b></td> 
          <td align="right"><b><%if recarray(7,i)<> "" then Response.Write formatnumber(recarray(7,i),0) else Response.Write "0" end if%></b></td>
          <td align="right"><b><%if recarray(8,i)<> "" then Response.Write formatnumber(recarray(8,i),2) else Response.Write "0.00" end if%></b></td>
          <td align="right"><b><%if recarray(9,i)<> "" then Response.Write formatnumber(recarray(9,i),2) else Response.Write "0.00" end if%></b></td>
		  <%
          if recarray(9,i)<> "" then 
				Totalturnover = Totalturnover + formatnumber(recarray(9,i),2)
		  End if
          %>
          <td align="right" ><b>&nbsp;<%=formatnumber(Totalturnover,2)%></b></td> 
          </tr>

          <%next%>
     	  <%Response.Flush()%>

          <%
          else%>
         <td colspan="11" width="100%" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td><% 
          end if%>
        </tr>
      </table>
    </td>
  </tr>
</table>
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;<br><br><br><br><br><br>
</center>
</div>
</body>
<%if IsArray(recarray) then erase recarray%>
<%if IsArray(arCustomer) then erase arCustomer%>
</html>