<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
	fromdt=request("fromdt")
	todt=request("todt")
	
	strStatus=request("Status")
	if strStatus="A" then
		strStatusLabel="Active Products"
	elseif  strStatus="D" then
		strStatusLabel="Deleted Products"
	else
		strStatusLabel=""
	end if
	
	if strStatus="0" then
		strStatus=""
	elseif strStatus="" then
		strStatus="A"
	end if

	if isdate(fromdt) and isdate(todt) then
		set objBakery = server.CreateObject("Bakery.reports")
		objBakery.SetEnvironment(strconnection)
		set retcol = objBakery.Display_product(fromdt,todt,strStatus)
		recarray = retcol("Product")
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=mon_report_product.xls" 
	%>
	<style>
		.text{
			mso-number-format:"\@";/*force text*/
		}
	</style>
	<table border=1 width="3000" cellpadding="0" cellspacing="0">
	<tr>
          <td colspan="41" height="40" valign="top" align="center"><b>Product Report From <%=fromdt%> To <%=todt%> <% if strStatus<>"" then%> - <%=strStatusLabel%><%end if%></b></td>
    </tr>
	<tr>
          <td width="110" bgcolor="#CCCCCC"><b>Product Code&nbsp;</b></td>
		  <td width="110" bgcolor="#CCCCCC"><b>Old Product Code</b></td>
          <td width="225" bgcolor="#CCCCCC"><b>Product Name&nbsp;</b></td>
		  
		  <td width="125" bgcolor="#CCCCCC"><b>Dough</b></td>
		  <td width="75" bgcolor="#CCCCCC"><b>Pre Size</b></td>
		  <td width="75" bgcolor="#CCCCCC"><b>Post Size</b></td>
		  <td width="50" bgcolor="#CCCCCC"><b>Shape</b></td>
		  <td width="100" bgcolor="#CCCCCC"><b>Slice</b></td>
		  <td width="150" bgcolor="#CCCCCC"><b>Facility</b></td>
		  <td width="70" bgcolor="#CCCCCC"><b>VAT Code</b></td>
		  <td width="70" bgcolor="#CCCCCC"><b>48 hrs Product</b></td>
		  <td width="120" bgcolor="#CCCCCC"><b>No of products</b></td>
		  <td width="70" bgcolor="#CCCCCC"><b>Type1</b></td>
		  <td width="70" bgcolor="#CCCCCC"><b>Type2</b></td>
          <td width="120" bgcolor="#CCCCCC" align="right"><b>Number of Units Sold&nbsp;</b></td>
          <td width="65" bgcolor="#CCCCCC" align="right"><b>Price A</b></td>
		  <td width="65" bgcolor="#CCCCCC" align="right"><b>Price A1</b></td>
		  <td width="65" bgcolor="#CCCCCC" align="right"><b>Price B</b></td>
		  <td width="65" bgcolor="#CCCCCC" align="right"><b>Price C</b></td>
		  <td width="65" bgcolor="#CCCCCC" align="right"><b>Price D</b></td>
		  <td width="80" bgcolor="#CCCCCC" align="center"><b>Ingredient Factor</b></td>
		  <td width="80" bgcolor="#CCCCCC" align="center"><b>Labour Cost</b></td>
		  <td width="80" bgcolor="#CCCCCC" align="center"><b>Packaging</b></td>
		  <td width="80" bgcolor="#CCCCCC" align="center"><b>Hygiene</b></td>
		  <td width="70" bgcolor="#CCCCCC" align="center"><b>Utilities</b></td>
		  <td width="120" bgcolor="#CCCCCC" align="center"><b>Rent Rate & other</b></td>
		  <td width="80" bgcolor="#CCCCCC" align="center"><b>VC Product</b></td>
          <td width="140" bgcolor="#CCCCCC" align="right"><b>Average Price Sold&nbsp;</b></td>
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Gross Turnover</b></td>  
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>VAT</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Net Turnover</b></td>   
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>New Product Code</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Bespoke For</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Outside of London Product</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Ordering Restriction</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>New Product Name</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Qty per box</b></td>
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Product Type</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Product Category</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Product Group</b></td> 
		  <td width="70" bgcolor="#CCCCCC" align="right"><b>Delivery Temperature</b></td> 
        </tr>
	<%if isarray(recarray) then%>
		<%
		UnitsSold = 0
		Price_A = 0.00
		Price_A1=0.00
		AveragePrice = 0.00
		TotalTurnover = 0.00
		TotVAT = 0.00
		TotNetTO = 0.00		
		%>
		<%for i=0 to UBound(recarray,2)%>
		<tr>
          <td class="text"><%=recarray(0,i)%></td>
		  <td align="left"><%=recarray(18,i)%></td>
          <td><%=recarray(1,i)%></td>
		  
		  <td><%=recarray(6,i)%></td>
		  <td><%=recarray(7,i)%></td>
		  <td><%=recarray(8,i)%></td>
		  <td><%=recarray(9,i)%></td>
		  <td><%=recarray(10,i)%></td>
		  <td><%=recarray(11,i)%></td>
		  <td><%=recarray(19,i)%>&nbsp;</td>
		  <td><%=recarray(20,i)%>&nbsp;</td>
		  <td><%=recarray(21,i)%>&nbsp;</td>
		  <td><%=recarray(16,i)%>&nbsp;</td>
		  <td><%=recarray(17,i)%>&nbsp;</td>
		  <td align="right" bgcolor="#CCCCCC"><%=recarray(2,i)%></td>
          <td align="right"><%if recarray(3,i)<> "" then Response.Write formatnumber(recarray(3,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(12,i)<> "" and not isnull(recarray(12,i)) then Response.Write formatnumber(recarray(12,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(13,i)<> "" and not isnull(recarray(13,i)) then Response.Write formatnumber(recarray(13,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(14,i)<> "" then Response.Write formatnumber(recarray(14,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(15,i)<> "" then Response.Write formatnumber(recarray(15,i),2) else Response.Write "0.00" end if%></td>
      	  
		  <td align="center"><%=recarray(22,i)%>&nbsp;</td>
		  <td align="center"><%=recarray(23,i)%>&nbsp;</td>
		  <td align="center"><%=recarray(24,i)%>&nbsp;</td>
		  <td align="center"><%=recarray(25,i)%>&nbsp;</td>
		  <td align="center"><%=recarray(26,i)%>&nbsp;</td>
		  <td align="center"><%=recarray(27,i)%>&nbsp;</td>
		  <td align="center"><%=recarray(31,i)%>&nbsp;</td>	
		  <td align="right" bgcolor="#CCCCCC"><%if recarray(4,i)<> "" then Response.Write formatnumber(recarray(4,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(5,i)<> "" then Response.Write formatnumber(recarray(5,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(30,i)<> "" then Response.Write formatnumber(recarray(30,i),2) else Response.Write "0.00" end if%></td>
          <td align="right"><%if recarray(29,i)<> "" then Response.Write formatnumber(recarray(29,i),2) else Response.Write "0.00" end if%></td>          
          <td align="center"><%=recarray(32,i)%>&nbsp;</td>	
         <td align="center"><%=recarray(33,i)%>&nbsp;</td>	
         <td align="center"><%=recarray(34,i)%>&nbsp;</td>
         <td align="center"><%=recarray(35,i)%>&nbsp;</td>
         <td align="center"><%=recarray(36,i)%>&nbsp;</td>	
         <td align="center"><%=recarray(37,i)%>&nbsp;</td>	
		 <td><%=recarray(38,i)%>&nbsp;</td>
		 <td><%=recarray(39,i)%>&nbsp;</td>
		 <td><%=recarray(40,i)%>&nbsp;</td>
		 <td><%=recarray(41,i)%>&nbsp;</td>  
         </tr>
         <%
          if recarray(2,i) <> "" then
          		UnitsSold = UnitsSold + recarray(2,i)
          End if 
		  'if recarray(12,i)<> "" and  isnumeric(recarray(12,i)) then 
          	'	Price_A1=Price_A1+formatnumber(recarray(12,i),2)
		  'END IF
		  'if recarray(3,i)<> "" then 
			'	Price_A = Price_A + formatnumber(recarray(3,i),2)
		  'End if
		  if recarray(4,i)<> "" then
				AveragePrice = AveragePrice + formatnumber(recarray(4,i),2)
		  End if
		  If recarray(5,i)<> "" then 
				TotalTurnover = TotalTurnover + formatnumber(recarray(5,i),2)
		  End if
		  
		  If recarray(30,i)<> "" then 
				TotVAT = TotVAT + formatnumber(recarray(30,i),2)
		  End if
		  
		  If recarray(29,i)<> "" then 
				TotNetTO = TotNetTO + formatnumber(recarray(29,i),2)
		  End if
		  		  
          %>
          <%next%>
            <tr>
       		<td colspan=14 align="right"><b>&nbsp;&nbsp;Grand Total</b></td>
          	<td align="right" bgcolor="#CCCCCC"><b><%=UnitsSold%></b></td>
          	<td align="right"><b><%'=formatnumber(Price_A,2)%></b>&nbsp;</td>
			<td align="right"><b><%'=formatnumber(Price_A1,2)%></b>&nbsp;</td>
			<td align="right" colspan="10">&nbsp;</td>
			
          	<td align="right" bgcolor="#CCCCCC"><b><%=formatnumber(AveragePrice,2)%></b></td>
          	<td align="right"><b><%=formatnumber(TotalTurnover,2)%></b></td>
          	<td align="right"><b><%=formatnumber(TotVAT,2)%></b></td>
          	<td align="right"><b><%=formatnumber(TotNetTO,2)%></b></td>          	
          </tr>
          <%else%>
          	<tr><td colspan="41"  bgcolor="#CCCCCC"><b>0 - No records matched...</b></td></tr>
		  <%end if%>
			</table>		  
		  <%
		  
	end if
%>
