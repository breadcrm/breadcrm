<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->

<%
deldate= request.form("deldate")
facility = "0,11,12,15,16"
dtype=Request.Form("deltype")
strType3=Request.Form("Type3")
strType4=Request.Form("Type4")
strType6=Request.Form("Type6")

if (strType4="") then
	strType4=strType6
end if

Dim totSubProdCount
Dim arrType3
Dim arrType4

'call DisplayPackingSheetReport (deldate,facility,dtype)

call DisplayBurgerBunReport(deldate,facility,dtype)

Sub DisplayBurgerBunReport(deldate,facility,dtype)
    stop
    Dim delivaryDay 
    delivaryDay = Weekday(CDate(deldate),0)
    if deldate= "" or facility = "" or dtype = "" then response.redirect "daily_report.asp"
    stop
    set object = Server.CreateObject("bakery.daily")
	object.SetEnvironment(strconnection)
    set BBReport= object.GetBurgerBunReport(deldate,facility,dtype,strType3,strType4)
    vRecArray = BBReport("BurgerBunReport")

    Set object = Server.CreateObject("bakery.general")
	object.SetEnvironment(strconnection)
	set detail = object.DisplayTypeDes()	
	vType1Array =  detail("Type3ForPacking")
	vType2Array =  detail("Type4ForPacking") 

    'Drow page no
    
    set col1= nothing
	set object = nothing
	dim curvan, prevvan
	dim curcno, prevcno
	
	Dim mPage,mLine
	Dim mFontSize
	Dim mFooterSize
	Dim mFooterFontSize
	Dim mExtraLine
	
	mFontSize = 8
	mFooterSize =3
	mFooterFontSize = 3
	mExtraLine = 10
	
	mLine = 8
	  if isarray(vrecarray) then
			mPage = 1
		currec=0
		totqty=0
		curcno = vrecarray(0,0)
		curvan = vrecarray(1,0)
		curpage=1	  
		for i = 0 to ubound(vRecArray,2)
		  currec = clng(currec) + 1
		  if clng(currec) = 1 then
			if mFontSize = 14 Then
							mLine = mline + 4
						else
							mLine = mline + 3
						end if	
						if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0
						End if
		   end if
		   if curvan = vrecarray(1,i) and curcno = vrecarray(0,i) then
						if mFontSize = 14 Then
							mLine = mLine + 2
						else
							mLine = mLine + 1
						end if	
		  end if
		  if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
						mPage = mPage + 1						
						mLine = 0
		  End if          
		  if curvan <> vrecarray(1,i) or curcno <> vrecarray(0,i) then
			curvan = vrecarray(1,i)
			curcno = vrecarray(0,i)
			i=clng(i)-1
			if mFontSize = 14 Then
							mLine = mLine +4           
						else
							mLine = mLine +3           
						end if	
						
		 if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then 						         
						mPage = mPage + 1
						mLine = 0	
					else
						if curvan <> vrecarray(1,i)   Then										
							mPage = mPage + 1
							mLine = 0
						End if
		  End if
			if i < ubound(vrecarray,2) then
			end if            
			curpage = curpage + 1
			currec = 0
			totqty=0
		  end if
		next
		
	  else
				mPage = 1
	  end if
	i = 0
    
    'Drow page no
    
    %>
    <html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<title>Customer by Products Packing Sheet</title>
	<style type="text/css">
	<!--
	br.page { page-break-before: always; }
	-->
	</style>
	</head>
	<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
	
	&nbsp;&nbsp;&nbsp;<br>
	&nbsp;&nbsp;&nbsp;
	<div align="center">
	  <center>
	<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: <%=mFontSize%>pt">
	  <tr>
		<td width="100%">
		  <center><b><font size="3">Customer by Products Packing Sheet - <%=dtype%><br></font></b></center>
		 
		  <table border="0" width="90%" style="font-family: Verdana; font-size: <%=mFontSize%>pt" cellspacing="0" cellpadding="2">
			<tr>
			  <td colspan="2"><br>
				<br>
				<b>Date of report:</b> <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
				<b>To be packed on</b> <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><BR>
				
				<% if (strType3<>"") then%>
					<b>Type 3:</b> 
					<%
					stop
					strType3Text = ""
				    arrType3 = split(strType3,",") 
					if isarray(arrType3) then
					for j = 0 to ubound(arrType3)
					for i = 0 to ubound(vType1Array,2)
						if (cint(arrType3(j))=vType1Array(0,i)) then
							if strType3Text = "" Then
							    strType3Text = vType1Array(1,i)
							else
							    strType3Text = strType3Text & " , " & vType1Array(1,i)
							end if
						end if
					next
					next
					end if
					
					response.Write(strType3Text)
					%> 
					<br>
				<%end if%>
				
				<% if (strType4<>"") then%>
					<b>Type 4:</b> 
					<%
					strType4Text = ""
				    arrType4 = split(strType4,",") 
					if isarray(arrType4) then
					for j = 0 to ubound(arrType4)
					for i = 0 to ubound(vType2Array,2)
						if (cint(arrType4(j))=vType2Array(0,i)) then
							if strType4Text = "" Then
							    strType4Text = vType2Array(1,i)
							else
							    strType4Text = strType4Text & " , " & vType2Array(1,i)
							end if
						end if
					next
					next
					end if 
					response.Write(strType4Text)
					%> 
					<br>
				<%end if%>
			   </td>
			</tr>
			<%'End if %>
			<tr>
			  <td colspan="2"><br>
				Number of pages for this report: <%=mPage%>
			  </td>
			</tr>
			</table>
		</td>
	  </tr>
	</table>
	</center>
	</div><%
	if mFontSize = 14 Then
		mLine = 16
	Else
		mLine = 8
	End if%>
	
	
	<div align="center">
	<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
		  stop
		  if isarray(vrecarray) then
				mPage = 1
			currec=0
			totqty=0
			curcno = vrecarray(0,0)
			curvan = vrecarray(1,0)
			curpage=1	  
			for i = 0 to ubound(vRecArray,2)
						Temcurvan = vrecarray(5,i)
			  currec = clng(currec) + 1
			  if clng(currec) = 1 then%>
				<tr>
				<td colspan="4">
				    <b>Product Code : <%=vrecarray(0,i)%></b><br />
				   <b>Product Name : <%=vrecarray(1,i)%> </b><br />
				   <%If vrecarray(7,i) = 1 then %>
				   <br><b>Product Breakdown:<br> <%=DisplayProductBreakdown(vrecarray(0,i), vrecarray(4,i),vrecarray(8,i))%> </b>
				   <%end if %>
				</td>            
				</tr>
				<tr>
				<td align="left" bgcolor="#CCCCCC" height="20" width="15%"><b>Customer No</b></td>            
				<td align="left" bgcolor="#CCCCCC" height="20" width="60%"><b>Customer Name</b></td>
				<td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Qty</b></td>
							
				<%if delivaryDay = 1 or delivaryDay = 7 Then%>
				<td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Weekend Van No.</b></td>
				<%Else %>
				<td align="center" bgcolor="#CCCCCC" height="20" width="10%"><b>Week Van No.</b></td>
				<%End If %>
				</tr><%
				if mFontSize = 14 Then
								mLine = mline + 4
							else
								mLine = mline + 3
							end if	
							if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
								mPage = mPage + 1						
								mLine = 0%>		
								</table>						
								<br class="page" />						
								<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
							End if
			   end if
			   if curvan = vrecarray(1,i) and curcno = vrecarray(0,i) then%>
			  <tr>            
				<td align="left" width="15%"><%=vrecarray(2,i)%>&nbsp;</td>
				<td align="left" width="60%"><%=left(vrecarray(3,i),100)%>&nbsp;</td>
				<%If vrecarray(7,i) = 1 then %>
				
				<td align="center" width="10%"><%=vrecarray(4,i)%>&nbsp;</td>
				<%
				totqty = clng(totqty) + vrecarray(4,i)%>
				
				<%Else %>
				
				<td align="center" width="10%"><%=vrecarray(4,i)%>&nbsp;</td>
				<%
				totqty = clng(totqty) + vrecarray(4,i)%>
				
				<%End If %>
								
				<%if delivaryDay = 1 OR delivaryDay = 7 Then%>
				<td align="center" width="10%"><%=vrecarray(6,i)%></td>
				<%Else %>
				<td align="center" width="10%"><%=vrecarray(5,i)%></td>
				<%End if %>
			  </tr><%
							if mFontSize = 14 Then
								mLine = mLine + 2
							else
								mLine = mLine + 1
							end if	
			  end if
			  if mLine >= PrintPgSize+mExtraLine and i < ubound(vrecarray,2) Then 
							mPage = mPage + 1						
							mLine = 0%>		
							</table>						
							<br class="page" />						
							<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%
							
			  End if
			  
			  if curvan <> vrecarray(1,i) or curcno <> vrecarray(0,i) then						  
				curvan = vrecarray(1,i)
				curcno = vrecarray(0,i)		    
				i=clng(i)-1%>
				<tr>
				  <td colspan="2">
					<p align="right"><b><font size="<%=mFooterFontSize%>" face="Verdana">Total&nbsp;&nbsp; </font></b>
				  </td>
				  <td width="10%" align="center" >
					<p align="center"><font size="<%=mFooterFontSize%>" face="Verdana"><%=totqty%></font></p>
				  </td>
				  <td width="5%">&nbsp;</td>
				</tr>
				</table>
							<br>
				<br><%
				if mFontSize = 14 Then
								mLine = mLine +4           
							else
								mLine = mLine +3           
							end if	
			 if mLine >= PrintPgSize+mExtraLine - mFooterSize  and i < ubound(vrecarray,2) Then 						         
							mPage = mPage + 1
							mLine = 0%>								
							<br class="page" /><%		
						else							
							if curvan <> vrecarray(5,i)   Then										
								mPage = mPage + 1
								mLine = 0%>													
								<br class="page" /><%
							End if
						End if	
								
						
				if i < ubound(vrecarray,2) then%>
								<table border="1" width="90%" cellspacing="0" cellpadding="2" bordercolor="#000000" style="font-family: Verdana; font-size: <%=mFontSize%>pt" align="center"><%						
				end if            
				curpage = curpage + 1
				currec = 0
				totqty=0
			  end if		 
				 
			next%>
			<tr>
			  <td colspan="2">
				<p align="right"><b><font size="<%=mFooterFontSize%>" face="Verdana">Total&nbsp;&nbsp; </font></b>
			  </td>
			  <td width="10%" align="center">
				<p align="center"><font size="<%=mFooterFontSize%>" face="Verdana"><%=totqty%></font></p>
			  </td>
			  <td width="5%">&nbsp;</td>
			</tr><%
		  else%>
			<tr>
			<td width="100%" colspan = "4" height="20">Sorry no items found</td>
			</tr><%
		  end if%>
		  </table>
	</div>
	</body>
	</html>
    
    <%

End Sub

Function DisplayProductBreakdown(pno, MainProdQty,vOrderNo)
stop
totSubProdCount  = 0
Dim subProdList
Dim strSubProduct
Set obj = CreateObject("bakery.daily")
obj.SetEnvironment(strconnection)
set objMessage = obj.DisplayProdcutBreakdownforCustomerByProductSheet(pno,vOrderNo)
subProdList = objMessage("SubProductList")
stop
If isArray(subProdList) Then

    for i = 0 to ubound(subProdList,2)
    
     if strSubProduct = "" Then
        strSubProduct = cInt(subProdList(2,i))/cInt(MainProdQty) & " x " & subProdList(1,i)
        totSubProdCount = cInt(MainProdQty) * cInt(subProdList(2,i))
     else
        strSubProduct = strSubProduct & "<br>" & cInt(subProdList(2,i))/cInt(MainProdQty) & " x " & subProdList(1,i)
        totSubProdCount =  cInt(totSubProdCount) + (cInt(MainProdQty) * cInt(subProdList(2,i)))
     end if
    
    Next
Else

    strSubProduct = Empty

End if

DisplayProductBreakdown = strSubProduct

End Function


 
%>



