<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim obj,ObjInventory
Dim arIngredient,arType
Dim i
Dim vINo
Dim vType,strIngname,strUtype
Dim objResult,strMotherIngredient

Set obj = Server.CreateObject("Bakery.Inventory")
obj.SetEnvironment(strconnection)
if Trim(Request.QueryString("INo")) <> "" Then
	vINo = Request.QueryString("INo") 
Else
	vINo = Request.Form("INo") 
End if	

if Trim(Request.Form("Update")) <> "" Then
	vType = Request.Form("selType")
	strIngname = Request.Form("ingname")
	strUtype = Request.Form("Utype")
	strMotherIngredient = Request.Form("MotherIngredient")		
	objResult = obj.Update_Ingredient(vINo,vType,strIngname,strUtype,strMotherIngredient) 			
End if

set ObjInventory = obj.Display_IngredientDetails(vINo)	
arIngredient  = ObjInventory("Ingredients")
arType = ObjInventory("Type") 
set obj = Nothing
set ObjInventory = Nothing
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>

<script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{

  if (theForm.SelType.selectedIndex < 0)
  {
    alert("Please select one of the \"Ing Type\" options.");
    theForm.SelType.focus();
    return (false);
  }

  if (theForm.SelType.selectedIndex == 0)
  {
    alert("The first \"Ing Type\" option is not a valid selection.  Please choose one of the other options.");
    theForm.SelType.focus();
    return (false);
  }  

  if (theForm.ingname.value == "")
  {
    alert("Please enter a value for the \"Name\" field.");
    theForm.ingname.focus();
    return (false);
  }

  if (theForm.Utype.selectedIndex < 0)
  {
    alert("Please select one of the \"Unit Type\" options.");
    theForm.Utype.focus();
    return (false);
  }

  if (theForm.Utype.selectedIndex == 0)
  {
    alert("The first \"Unit Type\" option is not a valid selection.  Please choose one of the other options.");
    theForm.Utype.focus();
    return (false);
  }
  
  if (!(theForm.MotherIngredient[0].checked || theForm.MotherIngredient[1].checked))
  {
    alert("Please select one of the \"Mother Ingredient\" options.");
    theForm.Utype.focus();
    return (false);
  }
  
  if(confirm("Are you sure you want to update this ingredient")){
	  return (true);
  }
  else
  {
      return (false);
  }
}


function loadMsg(){
	if('<%=objResult%>' == 'Fail'){
		alert("This ingredient already exist")
	
	}
}


//--></script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;

<form method="POST" action="Editinventory_ingredients.asp" name="FrontPage_Form1" onSubmit="return FrontPage_Form1_Validator(this)">

<div align="center">
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Edit ingredients<br>
      &nbsp;</font></b>
      		<%
			if objResult = "Fail" then
				Response.Write "<br><font size=""3"" color=""#FF0000""><b>Ingredient already exists</b></font><br><br>"
			elseif objResult = "OK" then
				Response.Write "<br><font size=""3"" color=""#16A23D""><b>Ingredient updated successfully</b></font><br><br>"
			end if
			%>
      <%
		if IsArray(arIngredient) and objResult="" Then%>
      <table border="0" width="70%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
         <tr>
          <td bgcolor="#E1E1E1" colspan="2"><b>Ingredient</b></td>
        </tr>

        <tr>
          <td>Ingredient Category</td>
          <td>        
         <select size="1" name="SelType">
					<option value="">Select</option><%
					if IsArray(arType) then
						for i = 0 to ubound(arType,2)%>  				
							<option value="<%=arType(0,i)%>" <%if Trim(arType(0,i)) = Trim((arIngredient(0,0))) Then Response.Write "Selected"%> ><%=arType(1,i)%></option><%
           next 
					End if%>
          </select></td>
        </tr>
        <tr>
          <td>Ingredient Name</td>
          <td>
          <input type="text" name="ingname" value="<%=arIngredient(1,0)%>" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "50"></td>
        </tr>
		<tr>
          <td>Mother Ingredient</td>
          <td>
         	<input type="radio" name="MotherIngredient" value="1" class="radioinput"  <%if arIngredient(3,0)=true then%> checked="checked" <%end if%>> Yes
		 	<input type="radio" name="MotherIngredient" value="0" class="radioinput"  <%if arIngredient(3,0)=false then%> checked="checked" <%end if%>> No
		 </td>
        </tr>
        <tr>
          <td>Unit Type</td>
          <td><select size="1" name="Utype">
          <option>Select</option>
          <option value="Kgs" <%if Trim(arIngredient(2,0)) = "Kgs" Then Response.Write "Selected"%>>Kgs</option>
          <option value="Lts" <%if Trim(arIngredient(2,0)) = "Lts" Then Response.Write "Selected"%>>Lts</option>
          <option value="Pcs" <%if Trim(arIngredient(2,0)) = "Pcs" Then Response.Write "Selected"%>>Pcs</option>
          </select></td>
        </tr>
        <tr>
          <td></td>
          <td><input type="submit" value="Update Ingredient" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
      </table><%
	End if%>

  </center>
    </td>
  </tr>
</table>


</div>
<input type="hidden" name="INo" value="<%=vINo%>">
<input type="hidden" name="Update" value="True">
</form>
</body>

</html><%
if IsArray(arIngredient) Then erase arIngredient
if IsArray(arType) Then erase arType
%>