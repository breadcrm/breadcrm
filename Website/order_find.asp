<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim Obj,ObjCustomer
Dim arOrder
Dim strCustomerName,strCustNo
Dim vPageSize
Dim strDirection
Dim vpagecount
Dim vCurrentPage
Dim i
Dim vSDate,vEDate
Dim arSplit


vPageSize = 999999
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vCurrentPage	= Request.Form("CurrentPage") 

strCustomerName = replace(Request.Form("txtCustName"),"'","''")
strCustNo = replace(Request.Form("txtCustNo"),"'","''")
vSDate = replace(Trim(Request.Form("txtSDate")),"'","''")
vEDate = replace(Trim(Request.Form("txtEDate")),"'","''")

If vSDate = "" Then
	vSDate = Day(date()-7) & "/" & Month(date()-7) & "/" & Year(date()-7)  
End if

If vEDate = "" Then
	vEDate = Day(date()) & "/" & Month(date()) & "/" & Year(date())  
End if

if strCustNo <> "" Then
	if  not IsNumeric(strCustNo) Then
		strCustNo = 0
	End if
End if

If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if


arSplit = Split(vSDate,"/")
vSDate = Right( "0" & arSplit(0),2) & "/" & Right( "0" & arSplit(1),2) & "/" & arSplit(2)	

arSplit = Split(vEDate,"/")
vEDate = Right( "0" & arSplit(0),2) & "/" & Right( "0" & arSplit(1),2) & "/" & arSplit(2)	


Set Obj = CreateObject("Bakery.Orders")
Obj.SetEnvironment(strconnection)

Set ObjCustomer = obj.Display_FindTypicalOrderList(vPageSize,vCurrentPage,strCustomerName,strCustNo,vSDate,vEDate)
arOrder = ObjCustomer("Order")
vpagecount  = ObjCustomer("Pagecount")

Set Obj = Nothing
Set ObjCustomer = Nothing
%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" >
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Find order<br>
      &nbsp;</font></b>
      <form name="frmOrderSearch" method="post">
				<table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
					<tr>
				    <td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
				    <td><b>Customer Code:</b></td>
				    <td><b>Customer Name:</b></td>
				    <td></td>
				  </tr>
				  <tr>
				    <td></td>
				    <td><input type="text" name="txtCustNo" value="<%=strCustNo%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
				    <td><input type="text" name="txtCustName" value="<%=strCustomerName%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>                    
				    <td><input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
				  </tr>
				  
				  <tr>
				    <td><b>&nbsp;</b></td>
				    <td>From Date:<br>
				      <input type="text" name="txtSDate" value="<%=vSDate%>" size="20" onFocus="blur();" style="font-family: Verdana; font-size: 8pt"><a href="javascript:onClick=calpopup('calendar.asp?Page=BODY&amp;Form=frmOrderSearch&amp;Element=txtSDate')"><img border="0" src="images/cal.gif" WIDTH="21" HEIGHT="19"></a></td>
				    <td>To Date:<br>
				      <input type="text" name="txtEDate" value="<%=vEDate%>" size="20" onFocus="blur();" style="font-family: Verdana; font-size: 8pt"><a href="javascript:onClick=calpopup('calendar.asp?Page=BODY&amp;Form=frmOrderSearch&amp;Element=txtEDate')"><img border="0" src="images/cal.gif" WIDTH="21" HEIGHT="19"></a></td>
				    <td></td>
				  </tr>
				</table>
			</form>	
    		<%if vCurrentPage <> 1 then%>
	      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">      
				<tr>
					<td height="20"></td>
				</tr><%				
				if IsArray(arOrder) Then%>
					<tr>
					  <td align="right"><b>Page <%=vCurrentPage%> of <%=vpagecount%></b></td>          
					</tr><%
				End if%>	
			</table> 
			<%end if%>
			<form name="frmfindOrder"> 
				<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
				  <tr>
				    <td width="33%" bgcolor="#CCCCCC"><b>Order Date</b></td>
					<td width="33%" bgcolor="#CCCCCC"><b>Customer Code</b></td>
				    <td width="33%" bgcolor="#CCCCCC"><b>Customer Name</b></td>
				    <td width="34%" bgcolor="#CCCCCC"><b>Action</b></td>
				  </tr><%
				  
				  if IsArray(arOrder) Then 
						for i = 0 to UBound(arOrder,2)%>
							<tr>
								<td><%=Day(arOrder(1,i)) & "/" & Month(arOrder(1,i)) & "/" & Year(arOrder(1,i))%></td>
								<td><%=arOrder(2,i)%></td>	
								<td><%=arOrder(3,i)%></td>							
									<td><input type="button" value="view Order" name="B1" style="font-family: Verdana; font-size: 8pt" onClick="document.location.href ='order_view.asp?OrderNo=<%=arOrder(0,i)%>'"></td>
									<input type="hidden" name="OrderNo" value="<%=arOrder(0,i)%>">							
							</tr><%
						Next
					Else%>
						<tr>
							<td Colspan="3">Sorry no items found</td>						
						</tr><%	
					End if%>       
				</table>
			</form>      
    </td>
  </tr>
</table>

	<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		<form Name="Navigate">
			<tr>					
				<td width="187"><%				
		
					if vCurrentPage <> 1 then%>
						<input type="button" value="FirstPage" Onclick="document.frmFirst.submit()" id="button1" name="button1" style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) > 1 then%>
						<input type="button" value="PreviousPage" Onclick="document.frmPrevious.submit()" id="button3" name="button3" style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%				
					if clng(vCurrentPage) < vPagecount then%>
						<input type="button" value="NextPage" Onclick="document.frmNext.submit()" id="button5" name="button5" style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) <> vPagecount and vPagecount > 0  then%>
						<input type="button" value="LastPage" Onclick="document.frmLast.submit()" id="button7" name="button7" style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>					
			</tr>
		</form>
	</table>

  </center>
</div>

<form METHOD="post" NAME="frmFirst">
		<input TYPE="hidden" NAME="Direction" VALUE="First">
		<input TYPE="hidden" NAME="pagecount" VALUE="<%=vPagecount%>">
		<input TYPE="hidden" NAME="CurrentPage" VALUE="<%=vCurrentPage%>">				
		<input TYPE="hidden" NAME="txtCustName" VALUE="<%=strCustomerName%>">
		<input TYPE="hidden" NAME="txtCustNo" VALUE="<%=strCustNo%>">		
		<input TYPE="hidden" NAME="txtSDate" VALUE="<%=vSDate%>">
		<input TYPE="hidden" NAME="txtEDate" VALUE="<%=vEDate%>">			
	</form>

	<form METHOD="post" NAME="frmPrevious">
		<input TYPE="hidden" NAME="Direction" VALUE="Previous">
		<input TYPE="hidden" NAME="pagecount" VALUE="<%=vPagecount%>">		
		<input TYPE="hidden" NAME="CurrentPage" VALUE="<%=vCurrentPage%>">
		<input TYPE="hidden" NAME="txtCustName" VALUE="<%=strCustomerName%>">
		<input TYPE="hidden" NAME="txtCustNo" VALUE="<%=strCustNo%>">
		<input TYPE="hidden" NAME="txtSDate" VALUE="<%=vSDate%>">
		<input TYPE="hidden" NAME="txtEDate" VALUE="<%=vEDate%>">			
	</form>

	<form METHOD="post" NAME="frmNext">
		<input TYPE="hidden" NAME="Direction" VALUE="Next">
		<input TYPE="hidden" NAME="pagecount" VALUE="<%=vPagecount%>">		
		<input TYPE="hidden" NAME="CurrentPage" VALUE="<%=vCurrentPage%>">
		<input TYPE="hidden" NAME="txtCustName" VALUE="<%=strCustomerName%>">
		<input TYPE="hidden" NAME="txtCustNo" VALUE="<%=strCustNo%>">		
		<input TYPE="hidden" NAME="txtSDate" VALUE="<%=vSDate%>">
		<input TYPE="hidden" NAME="txtEDate" VALUE="<%=vEDate%>">	
	</form>

	<form METHOD="post" NAME="frmLast">
		<input TYPE="hidden" NAME="Direction" VALUE="Last">
		<input TYPE="hidden" NAME="pagecount" VALUE="<%=vPagecount%>">		
		<input TYPE="hidden" NAME="CurrentPage" VALUE="<%=vCurrentPage%>">
		<input TYPE="hidden" NAME="txtCustName" VALUE="<%=strCustomerName%>">
		<input TYPE="hidden" NAME="txtCustNo" VALUE="<%=strCustNo%>">		
		<input TYPE="hidden" NAME="txtSDate" VALUE="<%=vSDate%>">
		<input TYPE="hidden" NAME="txtEDate" VALUE="<%=vEDate%>">	
	</form>

</body>

</html><%
if IsArray(arOrder) Then Erase arOrder
%>