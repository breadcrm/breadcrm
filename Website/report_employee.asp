<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->

<%
Dim i
Dim obj
Dim objUser
Dim arEmployees
Dim vPageSize, vSessionpage, vpagecount, vRecordCount,vMiddleVal,vFirstVal
Dim vFilter,vFiltervalue
Dim strLink

vPageSize = "999999"

vFiltervalue = replace(Request.Form("Filtervalue"),"'","''")
vFilter =  replace(Request.Form("Filter"),"'","''")

if session("UserType")= "A" or session("UserType")= "U"then
	vFiltervalue = session("UserNo")
	vFilter =  "No"
end if

vpagecount = Request.Form("pagecount") 

select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(vpagecount) then	
			session("Currentpage")  = session("Currentpage")+1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage")-1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select
vSessionpage = session("Currentpage")

vSessionpage =0

Set obj = server.CreateObject("bakery.employee")
obj.SetEnvironment(strconnection)
Set objUser = obj.DisplayEmployee(vFilter, vFilterValue, vsessionpage, vPageSize)

arEmployees =  objUser("Employees")
vpagecount =   objUser("Pagecount") 
vRecordCount = objUser("Recordcount")

Set objUser = Nothing
Set obj = Nothing
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script LANGUAGE="javascript" src="includes/script.js"></script>
<SCRIPT LANGUAGE="Javascript">
<!--
function SearchValidation(){
	if(document.frmForm.Filtervalue.value != ""){ 
		if(CheckEmpty(document.frmForm.Filtervalue.value) == "true"){ 
			alert("Please enter a valid name");
			document.frmForm.Filtervalue.focus();
			return false;
		}
		if(document.frmForm.Filter.selectedIndex == 0){
			if(isInt(document.frmForm.Filtervalue.value) == false){
				alert("Please enter a valid employee id");
				return false;
			}
		}
	}
	return true;
}
//-->
</SCRIPT>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Report - Employee<br>
      &nbsp;</font></b>

<%if session("UserType")= "S" then%>    
      
    <div align="center">
      <center>
      
<table width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse" bordercolor="#111111">
<form method="post" action="report_employee.asp" name="frmForm">
    <tr> 
      <td class="boldtext">Search </td>
      <td>
        <select name="Filter">
          <option value="No" <%if Trim(vFilter) = "No" Then Response.Write "selected"%>>Emp No</option>
          <option value="Name" <%if Trim(vFilter) = "Name" Then Response.Write "selected"%>>Emp Name</option>
        </select>
      </td>
      <td>
        <input type="text" name="Filtervalue" value="<%=Request.Form("Filtervalue")%>" size="20">
      </td>
      <td>
        <input type="submit" name="Submit" value="Submit" onClick="return SearchValidation();">
      </td>
    </tr>
</form>
</table>


      </center>
    </div>

<%End if%>

      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">

      <%if vsessionpage<> 0 then%>
  <tr>
    <td align="right" colspan="5">		
		<%
		If isArray(arEmployees) Then
			If (session("Currentpage") * vPageSize) > vRecordCount Then
				vMiddleVal  = vRecordCount
			Else
				vMiddleVal  = (session("Currentpage") * vPageSize)
			End if

			vFirstVal = (session("Currentpage") * vPageSize) - (vPageSize - 1)
			Response.Write "Employees " & vFirstVal & " To " & vMiddleVal & " of " & vRecordCount
		End if
		%>
    </td>
  </tr>
	<%end if%>

        <tr>
          <td width="33%" bgcolor="#CCCCCC"><b>Employee Code</b></td>
          <td width="33%" bgcolor="#CCCCCC"><b>Employee Name</b></td>
          <td width="34%" bgcolor="#CCCCCC"><b>Action</b></td>
        </tr>
<% 
	if IsArray(arEmployees) Then
		For i = 0 To ubound(arEmployees,2)
%>        
        <tr>
          <td width="33%"><%=arEmployees(0,i)%></td>
          <td width="33%"><%=arEmployees(5,i)%>&nbsp;<%=arEmployees(6,i)%><b></b></td>
          <form method="POST" action="employee_view.asp">
          <td width="34%"><input type = "hidden" name="eno" value = "<%=arEmployees(0,i)%>"><input type="submit" value="View Details" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
          </form>
        </tr>
<%
		Next
	Else
%>

        <tr>
          <td>Sorry no items found</td>
        </tr>
<%
	End if
%>
      </table>
    </td>
  </tr>
</table>


  </center>
</div>


<table width="720" border="0" cellspacing="1" cellpadding="2" align="center">
  <tr> 
    <td width="180">
	<% if vSessionpage <> 1 and vSessionpage <> 0 then%>
		<form action="report_employee.asp" method="post" name="frmFirst">
	        <input type="submit" name="Submit2" value="First Page">
			<input type="hidden" name="Direction" value="First">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">
			<input type="hidden" name="Filter" value="<%=vFilter%>">
			<input type="hidden" name="Filtervalue" value="<%=vFiltervalue%>">
		</form>
	<% End if %>
    </td>
    <td width="180">
	<% if clng(vSessionpage) > 1 then %>
		<form action="report_employee.asp" method="post" name="frmPrevious">
	        <input type="submit" name="Submit5" value="Previous Page">
			<input type="hidden" name="Direction" value="Previous">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">
			<input type="hidden" name="Filter" value="<%=vFilter%>">
			<input type="hidden" name="Filtervalue" value="<%=vFiltervalue%>">
		</form>
	<% End if %>
    </td>
    <td width="180">
	<% if clng(vSessionpage) < vPagecount then %>
		<form action="report_employee.asp" method="post" name="frmNext">
	        <input type="submit" name="Submit4" value="Next Page">
			<input type="hidden" name="Direction" value="Next">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">
			<input type="hidden" name="Filter" value="<%=vFilter%>">
			<input type="hidden" name="Filtervalue" value="<%=vFiltervalue%>">
		</form>
	<% End if %>
    </td>
    <td width="180">
	<% if clng(vSessionpage) <> vPagecount and vPagecount > 0  then %>
		<form action="locationslibrary.asp" method="post" name="frmLast">
	        <input type="submit" name="Submit3" value="Last Page">
			<input type="hidden" name="Direction" value="Last">
			<input type="hidden" name="pagecount" value="<%=vpagecount%>">
			<input type="hidden" name="Filter" value="<%=vFilter%>">
			<input type="hidden" name="Filtervalue" value="<%=vFiltervalue%>">
		</form>
	<% End if %>
    </td>
  </tr>
</table>
<p><font face="Verdana, Arial, Helvetica, sans-serif" size="2"></font></p>
</body>
</html>
<%
If IsArray(arEmployees) Then Erase arEmployees
%>