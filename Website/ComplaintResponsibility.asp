<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->

<%
vcomplaintoptions = replace(Request.Form("complaintoptions"),"'","''")

Set obj = server.CreateObject("bakery.credit")
obj.SetEnvironment(strconnection)
if request.form("delete")="yes" then
	vid=request.form("id")
	delete = obj.DeleteComplaintResponsibility(vid)
end if
Set objComplaintResponsibility = obj.DisplayComplaintResponsibility(vcomplaintoptions)

arComplaintResponsibility = objComplaintResponsibility("ComplaintResponsibility")
vRecordCount = objComplaintResponsibility("Recordcount")

Set objComplaintResponsibility = Nothing
Set obj = Nothing
%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script LANGUAGE="javascript" src="includes/script.js"></script>
<script Language="JavaScript">
<!--
function validate(frm){
	if(confirm("Would you like to delete '" + frm.name.value + "'?")){
		frm.submit();		
	}
}
//-->
</Script>
<SCRIPT LANGUAGE="Javascript">
<!--
function SearchValidation(){
		if((CheckEmpty(document.frmForm.sname.value) == "true") && (CheckEmpty(document.frmForm.scode.value) == "true") && (CheckEmpty(document.frmForm.iname.value) == "true")){ 
			alert("Please enter a value");
			return false;
		}
		return true;
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="800" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">List of Complaint Responsibilities<br>
      &nbsp;</font></b>
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" align="center" bordercolor="#111111">
		<form method="post" action="ComplaintResponsibility.asp" name="frmForm">      
         <tr height="40">
          <td><strong>Complaint Option:</strong></td>
          <td><input type="text" name="complaintoptions" value="<%=vcomplaintoptions%>" size="30" style="font-family: Verdana; font-size: 8pt"></td>
          <td>
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt" onClick="return SearchValidation();"></td>
        </tr>
		</form>
		</table>
		 <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#111111">
		<tr>
			<form method="POST" action="ComplaintResponsibilityNew.asp" STYLE="margin: 0px; padding: 0px;">
          <td align="left" height="30">
		  <input type="submit" value="Add New" name="B1" style="font-family: Verdana; font-size: 8pt; width:70px">
          </td>
		  </form>
		</tr>
      </table>

       <table border="0" bgcolor="#999999" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">

	
		<tr bgcolor="#FFFFFF">
			<td colspan="4" align = "left" height="30"><b>Total no of complaint responsibilities: <%=vRecordcount%></b></td>
		</tr>	
  
      
        <tr bgcolor="#CCCCCC">
		  <td bgcolor="#CCCCCC" width="20"><b>No</b></td>
          <td bgcolor="#CCCCCC"><b>Complaint Option</b></td>
          <td width="150" bgcolor="#CCCCCC"><b>Created Date</b></td>
          <td bgcolor="#CCCCCC" align="center" width="160"><b>Action</b></td>
        </tr>
<% 
	if IsArray(arComplaintResponsibility) Then
		For i = 0 To ubound(arComplaintResponsibility,2)
		if (i mod 2 =0) then
			strBgColour="#FFFFFF"
		else
			strBgColour="#F3F3F3"
		end if
%>         
        <tr bgcolor="<%=strBgColour%>">
		  <td><%=i+1%>.&nbsp;</td>
          <td><%=arComplaintResponsibility(1,i)%></td>
          <td><%=arComplaintResponsibility(2,i)%></td>
		  <td align="center">
		  <table width="100%" cellpadding="2" border="0" cellspacing="0" align="center">
		  <tr>
          <%if session("UserType") <> "N" and strconnection<>"vdbArchiveConn" Then%>
		  <form method="POST" action="ComplaintResponsibilityNew.asp" STYLE="margin: 0px; padding: 0px;">
          <td align="center">
		  <input type = "hidden" name="id" value = "<%=arComplaintResponsibility(0,i)%>">
		  <input type="submit" value="Edit" name="B1" style="font-family: Verdana; font-size: 8pt; width:70px">
          </td>
		  </form>
          <form method="POST" STYLE="margin: 0px; padding: 0px;">
		  <%if trconnection<>"vdbArchiveConn" Then%>
		  <td align="center">
		  <input type = "hidden" name="id" value = "<%=arComplaintResponsibility(0,i)%>">
          <input type = "hidden" name="delete" value = "yes">
		  <input type = "hidden" name="name" value = "<%=arComplaintResponsibility(1,i)%>">
		  <input type="button" onClick="validate(this.form);" value="Delete" name="B1" style="font-family: Verdana; font-size: 8pt; width:70px">
          </td>
		  <%
		  end if
		  %>
		  </form>
		  </tr>
							  </table>
		  </td>
			<%end if%>
        </tr>
<%
		Next
	Else
%>

        <tr>
          <td colspan="5" bgcolor="#FFFFFF" align="center"><font color="#FF0000"><strong>Sorry no items found.</strong></font></td>
        </tr>
<%
	End if
%>
      </table>
    </td>
  </tr>
</table>
<br><br>

  </center>
</div>
</body>
</html>
<%
If IsArray(arComplaintResponsibility) Then Erase arComplaintResponsibility
%>