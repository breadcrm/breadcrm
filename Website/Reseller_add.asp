<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayFacility()	
vFacarray =  detail("Facility") 
set detail = Nothing
set object = Nothing

strRID = replace(trim(request("RID")),"'","''")
if strRID <> "" then
	set object = Server.CreateObject("bakery.Reseller")	
	object.SetEnvironment(strconnection)
	set DisplayResellerDetail= object.DisplayResellerDetail(strRID)
	vRecArray = DisplayResellerDetail("ResellerDetail")
	vRecSalaryArray = DisplayResellerDetail("ResellerSalaryDetail")
	set DisplayResellerDetail= nothing
	set object = nothing
end if
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script LANGUAGE="javascript" SRC="includes/Script.js"></script>

<script Language="JavaScript">
<!--
function validate(frm,type){
	if(confirm("Would you like to "+ type +" this Reseller?")){

		if (type=='delete'){
			frm.type.value='Delete';		
			frm.submit();}
		else{
			FrontPage_Form1_Validator(frm)	
		}	
		
	}
}
function FrontPage_Form1_Validator(theForm)
{

  if (theForm.ResellerName.value == "")
  {
    alert("Please enter a value for the \"Reseller Name\" field.");
    theForm.ResellerName.focus();
    return;
  }
  
  if (theForm.OldCode.value == "")
  {
    alert("Please enter a value for the \"Old Code\" field.");
    theForm.OldCode.focus();
    return;
  }

  if (theForm.Address.value == "")
  {
    alert("Please enter a value for the \"Address\" field.");
    theForm.Address.focus();
    return;
  }

  if (theForm.Telephone.value == "")
  {
    alert("Please enter a value for the \"Telephone\" field.");
    theForm.Telephone.focus();
    return;
  }

  if (theForm.Fax.value == "")
  {
    alert("Please enter a value for the \"Fax\" field.");
    theForm.Fax.focus();
    return;
  }
   if (theForm.PriceBand.value == "")
  {
    alert("Please enter a value for the \"Price Band\" field.");
    theForm.PriceBand.focus();
    return;
  }
  if (theForm.PriceBand.value == "6" || theForm.PriceBand.value == "18")
  {
    if (theForm.Factor.value == "")
    {
		alert("Please enter a value for the \"Factor\" field.");
		theForm.Factor.focus();
		return;
	}
  }
  if ((theForm.file.value == "") && (theForm.file1.value == ""))
  {
    alert("Please enter a value for the \"Company Logo\" field.");
    theForm.file.focus();
    return;
  }
   if (theForm.InvoiceFooter.value == "")
  {
    alert("Please enter a value for the \"Invoice Footer Message\" field.");
    theForm.InvoiceFooter.focus();
    return;
  }
  if (theForm.VATRegNo.value == "")
  {
    alert("Please enter a value for the \"VAT Reg No\" field.");
    theForm.VATRegNo.focus();
    return;
  }
  theForm.submit();
}
function checkLength(des,maxChars)
{
  if (des.value.length > maxChars) 
  {
    des.value = des.value.substring(0,maxChars);
    alert('Description cannot contain more than ' +  maxChars + ' characters!'); 
	des.focus();
    return false;
  }
}
function disableDetails()
{
	  if (document.FrontPage_Form1.PriceBand.value=="6" || document.FrontPage_Form1.PriceBand.value=="18")
	  	document.getElementById("showdata").style.display = "";  
	  else
	  	document.getElementById("showdata").style.display = "none";
}	
//-->
</Script>
</head>
<body topmargin="0" onLoad="disableDetails();" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;

<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt" align="center">
    <tr>
	<form method="POST" action="Reseller_finish.asp" enctype="multipart/form-data" onSubmit="return FrontPage_Form1_Validator(this)" name="FrontPage_Form1">
    
    <input type="hidden" name="RID" value="<%=strRID%>">
    <td width="100%"><b><font size="3"><%if strRID = "" then%>Create new<%else%>Edit<%end if%> Reseller<br>
      &nbsp;</font></b>
      
      <table border="0" width="750" cellspacing="0" cellpadding="5" align="center">
	  <tr>
	   <td colspan="2"><font size="1" color="#FF0000">* fields are mandatory</font><Td>
	  </tr>
	  <%
				if Request.QueryString("status")  = "error" Then%>	
				<tr>
          <td valign="top"><font size="2" color="#FF0000"><B>Invalid Entry</B></font><Td>
        <tr><%
				End if%>
        <tr>
          <td valign="top"><table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="100%">
        
        <tr>
          <td width="170" align="right"><b>Reseller Name</b><font color="#FF0000">*</font>&nbsp;</td>
          <td><input type="text" name="ResellerName" maxlength="50" size="42" style="font-family: Verdana; font-size: 8pt" value="<%if isarray(vrecarray) then response.write vrecarray(1,0)%>"></td>
        </tr>
		<tr>
          <td width="170" align="right"><b>Old Code</b><font color="#FF0000">*</font>&nbsp;</td>
          <td><input type="text" name="OldCode" maxlength="8" size="10" style="font-family: Verdana; font-size: 8pt" value="<%if isarray(vrecarray) then response.write vrecarray(12,0)%>"></td>
        </tr>
        <tr>
          <td valign="top" align="right"><b>Address</b><font color="#FF0000">*</font>&nbsp;</td>
          <td><textarea name="Address" wrap="virtual" rows="5" onKeyUp="checkLength(this,300)" cols="41" style="font-family: Verdana; font-size: 8pt"><%if isarray(vrecarray) then response.write vrecarray(2,0)%></textarea></td>
        </tr>
        <tr>
          <td align="right"><b>Town&nbsp;</b>&nbsp;</td>
          <td><input type="text" name="Town" maxlength="30" size="25" style="font-family: Verdana; font-size: 8pt" value="<%if isarray(vrecarray) then response.write vrecarray(3,0)%>"></td>
        </tr>
       <tr>
          <td align="right"><b>Postcode&nbsp;&nbsp;</b></td>
          <td><input type="text" name="Postcode" maxlength="10" size="25" style="font-family: Verdana; font-size: 8pt" value="<%if isarray(vrecarray) then response.write vrecarray(4,0)%>"></td>
        </tr>
		<tr>
          <td align="right"><b>Telephone</b><font color="#FF0000">*</font>&nbsp;</td>
          <td><input type="text" name="Telephone" maxlength="20" size="25" style="font-family: Verdana; font-size: 8pt" value="<%if isarray(vrecarray) then response.write vrecarray(5,0)%>"></td>
        </tr>
		<tr>
          <td align="right"><b>Fax</b><font color="#FF0000">*</font>&nbsp;</td>
          <td><input type="text" name="Fax" maxlength="20" size="25" style="font-family: Verdana; font-size: 8pt" value="<%if isarray(vrecarray) then response.write vrecarray(6,0)%>"></td>
        </tr>
		<tr>
          <td align="right"><b>Price Band</b><font color="#FF0000">*</font>&nbsp;</td>
          <td>
		  <%
		  strPriceBand=vrecarray(7,0)
		  strFactor=vrecarray(13,0)
		  if strFactor="0" then
		  	strFactor=""
		  end if
		  %>
		  <select size="1" name="PriceBand" onChange="disableDetails()" style="font-family: Verdana; font-size: 8pt">
              <option value="" selected="selected" >---Select---</option>
			  <option value="5" <%if Trim(strPriceBand) = "5" Then Response.Write "Selected"%>>A1</option>
              <option value="1" <%if Trim(strPriceBand) = "1" Then Response.Write "Selected"%>>A</option>
			  <option value="2" <%if Trim(strPriceBand) = "2" Then Response.Write "Selected"%>>B</option>
              <option value="3" <%if Trim(strPriceBand) = "3" Then Response.Write "Selected"%>>C</option>
              <option value="4" <%if Trim(strPriceBand) = "4" Then Response.Write "Selected"%>>D</option>
			  <option value="6" <%if Trim(strPriceBand) = "6" Then Response.Write "Selected"%>>F</option>
			  <option value="18" <%if Trim(strPriceBand) = "18" Then Response.Write "Selected"%>>F1</option>
		 </select>
		 <input type="hidden" name="PriceBandOld" value="<%=strPriceBand%>">
		 <span id="showdata">
			 Formula
			 <input name="Factor" type="text" value="<%=strFactor%>" style="color: #000000; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px" size="7" maxlength="7" onKeypress="if ((event.keyCode != 43) && (event.keyCode != 45) && (event.keyCode != 46) && (event.keyCode <48 || event.keyCode > 57)) event.returnValue = false;">
		</span>
		 </td>
        </tr>
		<tr>
		<td colspan="2" height="10"></td>
		</tr>
		<tr>
          <td valign="top" align="right"><b>Company Logo</b><font color="#FF0000">*</font>&nbsp;</td>
          <td>
		  <%
		  if isarray(vrecarray) then
		   		strCompanyLogo=vrecarray(8,0)
				if strCompanyLogo<>"" then
				%>
				<b><%=strCompanyLogo%></b><br>
				<img src="images/ResellerLogos/<%=strCompanyLogo%>"><br>
				<%
				end if
		  end if
		  %>
		  <input type="file" name="file" size="26">
		  <input type="hidden" name="file1" value="<%=vrecarray(8,0)%>">
		  </td>
        </tr>
		<tr>
          <td valign="top" align="right"><b>Invoice Footer Message</b><font color="#FF0000">*</font>&nbsp;</td>
          <td><textarea name="InvoiceFooter" onKeyUp="checkLength(this,1000)" rows="5" cols="41" style="font-family: Verdana; font-size: 8pt"><%if isarray(vrecarray) then response.write vrecarray(9,0)%></textarea></td>
        </tr>
		<tr>
		  <td>&nbsp;</td>
		  <td align="left"><font color="#FF0000">(This image size should be, width 225 pixels and height 75 pixels. And image type must be GIF, JPG or JPEG)</font></td>
		</tr>
		<tr>
          <td align="right"><b>VAT Reg No</b><font color="#FF0000">*</font>&nbsp;</td>
          <td><input type="text" name="VATRegNo" maxlength="20" size="25" style="font-family: Verdana; font-size: 8pt" value="<%if isarray(vrecarray) then response.write vrecarray(10,0)%>"></td>
        </tr>
		
		<tr>
          <td valign="top" align="right"><b>Notes</b>&nbsp;&nbsp;</td>
          <td><textarea name="Notes" rows="5" cols="41" onKeyUp="checkLength(this,1000)" style="font-family: Verdana; font-size: 8pt"><%if isarray(vrecarray) then response.write vrecarray(11,0)%></textarea></td>
        </tr>
	
        <tr>
          <td></td>
          <td>
			<input type = "hidden" name="type" value="">
	        <%if strRID = "" then%>
  			<input type="button" value="Save Reseller" onClick="FrontPage_Form1_Validator(this.form);"  name="Add" style="font-family: Verdana; font-size: 8pt"> 			
  			<%else%>  		
  			<input type="button" value="Save Reseller" onClick="validate(this.form,'edit');" name="Edit" style="font-family: Verdana; font-size: 8pt">
  			<%end if%>
			&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value=" Back " onClick="history.back()" style="font-family: Verdana; font-size: 8pt">
			</td>
        </tr>
      </table></td>
        </tr>
      </table>
     <br>
	 </td>
	</form>
  </tr> 
</table>
</body>
</html>