<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
	dim fromdtr,todtr
	dim objRep
	
	fromdt=request("dt")
	
	dim arr
	arr = Split(fromdt,"/")
	fromdtr = arr(2) & "-" & arr(1) & "-" & arr(0)
	
	
	set objRep = server.CreateObject("Bakery.Reports")
	objRep.SetEnvironment(strconnection)
	set retcol = objRep.OrderAmendReport(fromdt)
	recarray = retcol("Report")

	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=Amend_Ord_rep.xls" 
	%>
      <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%"  colspan="6"><b>Amended Orders Delivered On &lt;<%=fromdt%>&gt; <% if strStatus<>"" then%> - <%=strStatusLabel%><%end if%></b></td>
        </tr>
        <tr>
          <td width="10%" bgcolor="#CCCCCC" ><center><b>Cus. No&nbsp;</b></center></td>
          <td width="35%" bgcolor="#CCCCCC"><center><b>Customer Name&nbsp;</b></center></td>
          <td width="12%" bgcolor="#CCCCCC"><center><b>Amended Date&nbsp;</b></center></td>        
          <td width="20%" bgcolor="#CCCCCC"><center><b>By&nbsp;</b></center></td>
          <td width="13%" bgcolor="#CCCCCC"><center><b>Standing Order&nbsp;</b></center></td>
          <td width="10%" bgcolor="#CCCCCC"><center><b>Value&nbsp;</b></center></td>     
        </tr>
		
		<%if isarray(recarray) then%>
				<%for i=0 to UBound(recarray,2)%>
					<tr>
						<td><%=recarray(6,i)%></td>
						<td><%=recarray(7,i)%></td>
						<td><%=recarray(9,i)%>&nbsp;</td> 
						<td><%=recarray(11,i)%></td>
						<td><center><%if(recarray(12,i)="Y") Then Response.Write("Yes") Else Response.Write("No") End If%></center></td>
						<td align="right"><%=FormatNumber(recarray(8,i),2)%>&nbsp;</td>
					</tr>
				<%next%>
          <%else%>
			<tr>
				<td colspan="6" width="100%" bgcolor="#CCCCCC"><b>0 - No Amended Orders Found...</b></td>
			</tr>
          <%end if%>
      </table>
</body>
</html>
