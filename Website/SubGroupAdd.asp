<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script Language="JavaScript">
<!--
function FrontPage_Form1_Validator(theForm)
{

  if (theForm.SubGroupName.value == "")
  {
    alert("Please enter a value for the \"Sub Group Name\" field.");
    theForm.SubGroupName.focus();
    return (false);
  }

  return (true);
}
//-->
</Script>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<br>
<form method="POST" action="SubGroupAdd.asp" name="FrontPage_Form1" onSubmit="return FrontPage_Form1_Validator(this)">
<table border="0" width="95%" cellspacing="0" align="center" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
<tr>
   <td width="100%"><b><font size="3">Sub Groups</font></b><br><br><br><br><br><br>
	
      <table border="0"  align="center"width="70%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
       <%
		strSGNo=Request("sgno")
		strSGName=Request("sgname")
		strSubGroupName = Request.Form("SubGroupName") 
		if strSubGroupName<>"" then
			strSGName=strSubGroupName
		%>
		<tr>
		<td colspan="2" align="center" height="40">
		<%
		set object = Server.CreateObject("bakery.customer")
		object.SetEnvironment(strconnection)
		set SaveGroup= object.SaveSubGroup(strSubGroupName,cstr(strSGNo))
		if SaveGroup("Sucess") <> "OK" then
			set SaveGroup= nothing
			set object = nothing
		%>	
			<font size="2" color="#FF0000"><b>Sub Group already exists.</b></font>
		<%
		else
			stop
			if strSGNo = "" then
				LogAction "Customer Sub Group Added", "Name: " & strSubGroupName , ""
			else
				LogAction "Customer Sub Group Edited", "Name: " & strSubGroupName , ""
			end if
			set SaveGroup= nothing
			set object = nothing
			response.Redirect("SubGroup.asp")
		end if
		%>
		</td>
		</tr>
		<%end if%>
		<tr>
          <td width="47%" align="right"><strong>Sub Group Name:</strong> </td>
          <td width="53%">
          	<input type="hidden" name="sgno" value="<%=strSGNo%>">
		  	<input type="text" name="SubGroupName" value="<%=strSGName%>" size="35" style="font-family: Verdana; font-size: 8pt" maxlength = "50"></td>
        </tr>
        <tr>
          <td></td>
          <td>
          <input type="submit" value="Save" name="B1" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
  <td align="center" height="50"><br><br><br><a href="SubGroup.asp"><strong><font color="#000099">List Sub Groups</font></strong></a></td>
  </tr>
</table>
</form>
<br><br>

</body>
</html>