<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayIng()	
vingarray =  detail("Ingredients")

set detail = object.ListDoughType()	
vDoughTypearray =  detail("DoughType") 

set detail = object.DisplayDoughType()	
vDougharray =  detail("DoughType")

set detail = Nothing
set object = Nothing

strSecondMixCategory=request("SecondMixCategory")
if (strSecondMixCategory="") then
	strSecondMixCategory="0"
end if

%>
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css">
<!--
.radioinput
{
 color: #000000; 
 background-color:#FFF; 
 border:none;
 margin:0px;
}
-->
</style>
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{
  if (theForm.doughname.value == "")
  {
    alert("Please enter a value for the \"Dough Name\" field.");
    theForm.doughname.focus();
    return (false);
  }
  
  if (theForm.DoughType.value == "")
  {
    alert("Please enter a value for the \"Dough Type\" field.");
    theForm.DoughType.focus();
    return (false);
  }
  if (theForm.SecondMixCategory[1].checked)
  {
  		  if (theForm.MainDough.value == "")
		  {
			alert("Please enter a value for the \"Main Dough\" field.");
			theForm.MainDough.focus();
			return (false);
		  }
		  
		  if (theForm.SecondMixCategoryWeight.value == "")
		  {
			alert("Please enter a value for the \"Amount of main dough to create 1 kilo of second mix\" field.");
			theForm.SecondMixCategoryWeight.focus();
			return (false);
		  }
  
  }
 
  if (theForm.I1.selectedIndex < 0)
  {
    alert("Please select one of the \"I1\" options.");
    theForm.I1.focus();
    return (false);
  }

  if (theForm.qty1.value == "")
  {
    alert("Please enter a value for the \"qty1\" field.");
    theForm.qty1.focus();
    return (false);
  }
  return (true);
}
function ShowSecondMix(chk)
{
	if (chk.value=="1")
	{
		document.getElementById("trMainDough").style.display = "";
		document.getElementById("trSecondMixCategoryWeight").style.display = "";
	}
	else
	{
		document.getElementById("trMainDough").style.display = "none";
		document.getElementById("trSecondMixCategoryWeight").style.display = "none";
	}
}

function TotalValue (theForm)
{	
	x=theForm.value;
	if (x !="")
	{
		if (document.getElementById("TotalValue").innerHTML!="")
		{
			dTotalValue=parseFloat(x)+parseFloat(document.getElementById("TotalValue").innerHTML)
			document.getElementById("TotalValue").innerHTML=Math.round(dTotalValue*Math.pow(10,4))/Math.pow(10,4);
			document.getElementById("TotalValueKg").innerHTML="kg";
		}	
		else
		{
			document.getElementById("TotalValue").innerHTML=Math.round(parseFloat(x)*Math.pow(10,4))/Math.pow(10,4);
			document.getElementById("TotalValueKg").innerHTML="kg";
		}	
	}
}

function TotalValueLoad()
{	
	document.getElementById("TotalValueKg").innerHTML="kg";
	dTotalValue=0;
	
	boolSecondMixCategory=document.FrontPage_Form1.SecondMixCategory[1].checked;
	
	for (i=1;i<=15;i++)
	{
		vqty="qty"+i
		x=document.getElementById(vqty).value;
		if (x!="")
		{
			dTotalValue=parseFloat(x)+parseFloat(dTotalValue)
			dTotalValue=Math.round(dTotalValue*Math.pow(10,4))/Math.pow(10,4);
		}
	}
	
	if (boolSecondMixCategory)
	{
		if (document.getElementById("SecondMixCategoryWeight").value!="")
			dTotalValue+=parseFloat(document.getElementById("SecondMixCategoryWeight").value);
	}
	document.getElementById("TotalValue").innerHTML=dTotalValue;	
}

//-->
</script>

</head>

<body <%if Request.QueryString ("status")="" then%> onLoad="ShowSecondMix(document.FrontPage_Form1.SecondMixCategory)" <%end if%> topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<form method="post" name="FrontPage_Form1" action="save_inventory_doughs.asp" onSubmit="return FrontPage_Form1_Validator(this)">
<div align="center">
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Add Dough<br>
      &nbsp;</font></b>
<%
if Request.QueryString ("status") = "error" then
	Response.Write "<br><font size=""3"" color=""#FF0000""><b>Dough already exists go back to change dough name</b></font><br><br>"
elseif Request.QueryString ("status") = "ok" then
	Response.Write "<br><font size=""3"" color=""#16A23D""><b>Dough " & request.querystring("doughname") & " created successfully</b></font><br><br>"
end if
%>

<%if Request.QueryString ("status")="" then%>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0">
        <tr height="25">
          <td width="230"><b>Dough Name&nbsp;</b></td>
          <td>
          <input type="text" name="doughname" size="35" style="font-family: Verdana; font-size: 8pt" maxlength = "50"></td>
        </tr>
		<tr height="25">          
          <td><b>Dough Type&nbsp;</b></td>
          <td>
		   <select size="1" name="DoughType" style="font-family: Verdana; font-size: 8pt">
			<option value = "">Select</option>
			<%
			if IsArray(vDoughTypearray) Then  	
				for i = 0 to ubound(vDoughTypearray,2)%>  				
			 <option value="<%=vDoughTypearray(0,i)%>" <%if strDoughType<>"" then%><%if strDoughType= vDoughTypearray(0,i) then response.write " Selected"%><%end if%>><%=vDoughTypearray(1,i)%></option>
			  <%
			   next                 
			End if
			%>   
		  </select>
		  </td>
        </tr>
		<tr height="25">
		<td><b>Second Mix Category&nbsp;</b></td>
		<td>
		<input type="radio" name="SecondMixCategory"  value="0" class="radioinput" onClick="ShowSecondMix(this);TotalValueLoad()" <%if strSecondMixCategory="0" then%> checked="checked" <%end if%>> No
		<input type="radio" name="SecondMixCategory" value="1" class="radioinput"  onClick="ShowSecondMix(this);TotalValueLoad()" <%if strSecondMixCategory="1" then%> checked="checked" <%end if%>> Yes
		</td>
		</tr>
		<tr height="25" id="trMainDough">
			<td><b>Main Dough&nbsp;</b></td>
			<td>
			<select size="1" name="MainDough" style="font-family: Verdana; font-size: 8pt">
				<option value = "">Select</option>
				<%
				if IsArray(vDougharray) Then  	
					for i = 0 to ubound(vDougharray,2)%>  				
				 <option value="<%=vDougharray(0,i)%>" <%if strDoughType<>"" then%><%if strDoughType= vDougharray(0,i) then response.write " Selected"%><%end if%>><%=vDougharray(1,i)%></option>
				  <%
				   next                 
				End if
				%>   
		    </select>
			</td>
		</tr>
		<tr height="25" id="trSecondMixCategoryWeight">
		<td><b>Amount of main dough to create <br>
		  1 kilo of second mix&nbsp;</b></td>
		<td><input type="text" name="SecondMixCategoryWeight" onChange="TotalValueLoad()" size="10" style="font-family: Verdana; font-size: 8pt" maxlength = "10"></td>
		</tr>
        </table>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0" height="383">       
        <tr>
          <td width="29%" height="13"><b>&nbsp;</b></td>
          <td width="71%" height="13"></td>
        </tr>
        <tr>
          <td width="29%" height="22"><b>Ingredient</b></td>
          <td width="71%" height="22"><b>Quantities required to produce 1 &nbsp;&nbsp;kg of dough</b></td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I1" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty1" id="qty1" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I2" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty2" id="qty2" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I3" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty3" id="qty3" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I4" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty4" id="qty4" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I5" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty5" id="qty5" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I6" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty6" id="qty6" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I7" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty7" id="qty7" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I8" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty8" id="qty8" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I9" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty9" id="qty9" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I10" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty10" id="qty10" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I11" style="font-family: Verdana; font-size: 8pt">
          					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty11" id="qty11" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I12" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty12" id="qty12" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I13" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%>           
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty13" id="qty13" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I14" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%>           
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty14" id="qty14" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I15" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%>           
            </select></td>
          <td width="71%" height="19">
          <input type="text" name="qty15" id="qty15" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg</td>
        </tr>
		<tr>
          <td width="29%" height="30"><strong>Total</strong></td>
          <td width="71%" height="30"><strong><span id="TotalValue"></span>&nbsp;<span id="TotalValueKg"></span></strong></td>
        </tr>
        <tr>
          <td width="29%" height="53">&nbsp;</td>
          <td width="71%" height="53">&nbsp;
            <p><input type="submit" value="Add Dough" name="B1" style="font-family: Verdana; font-size: 8pt"></p>
          </td>
        </tr>
      </table>
  <%else%>
  
  <%end if%>


  </center>
    </td>
  </tr>
</table>


</div>

</form>


</body>

</html>