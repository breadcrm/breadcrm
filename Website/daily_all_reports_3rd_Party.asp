<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
deldate= request.form("deldate")
if deldate= "" then response.redirect "LoginInternalPage.asp"

dim objBakery
dim recarray1, recarray2
dim ordno
dim retcol
Dim deldate
Dim intI
Dim intN
Dim intF
Dim intJ


dim tname, tno, curtno
Dim obj
Dim arCreditStatements
Dim arCreditStatementDetails
Dim vCreditDate

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; height:1px}
-->
</style>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
' 3rd Parties => 31-Sally Clarks, 30-Rinkoff, 32-Simply Bread, 23-Baker & Spice (Queens Park), 21-Baker & Spice (Denier Street)   
  set object = Server.CreateObject("bakery.daily")
  object.SetEnvironment(strconnection)
%>
<%  
  'Sally Clarks - 31
  intN = 0
  facilityNo=31
  tname = "Morning','Noon','Evening"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
'if k > 0 then
%>
<%
'end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%><br>&nbsp;</font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>
			
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="65%" height="20"><b>Product name</b></td>
          <td width="11%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
        </tr><%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="4" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="65%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="11%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="90%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>


<%
'ordering sheet by van - Sally Clarks - 31
 deltype="Morning','Noon','Evening"
      intN = 0
      intJ = 0
      intQ = 0
      intP = 1
      intTotal=0
      intD = 0
   	  intRow = 1
      facilityNo = 31
      set DisplayReport= object.Display_OrderingSheetByVans(facilityNo, delType, delDate)
	  arOrdSheetVansDetails = DisplayReport("OrdSheetVansDetails")
	  arVansList = DisplayReport("VansList")
	  arOrdSheetVansList = DisplayReport("OrdSheetVansList")
	  set DisplayReport= nothing

if IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
	intP = 1
	intRow = 1
	For intI = 0 To ubound(arVansList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		if intI = 0 Or intRow = 1 Then
			intRow = intRow + 1
		End if

		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetVansList, 2)
			if (arVansList(0, intI) = arOrdSheetVansList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
						intRow = intRow + 1
					End if
				End if
				if intN = 0 Then
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
			End if

			if intJ = ubound(arOrdSheetVansList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetVansList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetVansList, 2) Then
					intTotal = 0
					intRow = intRow + 1
				End if
				if intI <= ubound(arVansList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arVansList, 2) Then
						intP = intP + 1
					End if
				End if
				intQ = 0				
			End if
		Next
		intD = intD + 1
	Next
Else
	intP = 1
End if

intN = 1
intJ = 0
intQ = 0
intF = 0
intD = 0
intTotal=0
if IsArray(arOrdSheetVansDetails) and IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
<td>
<p align="center"><font size="3"><b>ORDERING SHEET BY VANS - <%=arOrdSheetVansDetails(0, 0)%></b></font></p>
</td>
</tr>
<tr>
<td>
<br><br>
<b><%= arOrdSheetVansDetails(0, 0) %><br>
To the attention of: <%= arOrdSheetVansDetails(1, 0) %><br>
By fax number: <%= arOrdSheetVansDetails(2, 0) %></b><br><br>
Date of report: <%= Day(Now()) & "/" & Month(Now()) & "/" & Year(Now()) %><br>
To be produced and  delivered on <%= Day(FormatDateTime(delDate, 2)) & "/" & Month(FormatDateTime(delDate, 2)) & "/" & Year(FormatDateTime(delDate, 2)) %> <%if deltype="Morning" then%> before 05:00 AM<%end if%><br><br>
Number of pages for this report: <%= intP %>
<br><br>
</td>
</tr>
</table><%
if IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
	currentPage = 1
	intRow = 1
	For intI = 0 To ubound(arVansList, 2)
		if intI = 0 Then
			intRow = 13 ' Changed by Selva 14 to 13 date:Jan 12th 2006 
		End if
		if intI = 0 Or intRow = 1 Then%>
	<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
		<tr>
		<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
		</tr>
	</table><%
			intRow = intRow + 1
			currentPage = currentPage + 1
		End if
%>
<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
	<tr>
	<td width="75%" height="20"><b>Order for Van <%= arVansList(0, intI) %></b></td>
	</tr>
</table><%
		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetVansList, 2)
			if (arVansList(0, intI) = arOrdSheetVansList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
					%>
						<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
							<tr>
							<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
							</tr>
						</table><%
						intRow = intRow + 1
						currentPage = currentPage + 1
					End if
%>
<table align="center" border="1" width="90%" cellspacing="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000" cellpadding="0"><%
				End if
				if intN = 0 Then
%>
	<tr>
	<td width="12%" height="20"><b>Product code</b></td>
	<td width="54%" height="20"><b>Product name</b></td>
	<td width="9%" height="20"><b>Quantity</b></td>
	</tr><%
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
	%>
	<tr>
	<td width="12%" height="20"><%= arOrdSheetVansList(2, intJ) %></td>
	<td width="54%" height="20"><%= arOrdSheetVansList(3, intJ) %></td>
	<td width="9%" height="20"><%= arOrdSheetVansList(4, intJ) %></td>
	</tr><%
				intTotal = intTotal + arOrdSheetVansList(4, intJ)
			End if

			if intJ = ubound(arOrdSheetVansList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetVansList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetVansList, 2) Then
%>
	<tr>
	<td width="66%" height="20" colspan="2" align="right"><b>Total&nbsp; </b></td>
	<td width="9%" height="20"><b><%= intTotal %>&nbsp;</b></td>
	</tr><%
					intTotal = 0
					intRow = intRow + 1
				End if
%>
</table><%
				if intI <= ubound(arVansList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arVansList, 2) Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
					End if
				End if
				intQ = 0
			End if
		Next
		intD = intD + 1
	Next
End if
%>
<p>&nbsp;</p>
<%
End if
If IsArray(arOrdSheetVansDetails) Then erase arOrdSheetVansDetails
If IsArray(arVansList) Then erase arVansList
If IsArray(arOrdSheetVansList) Then erase arOrdSheetVansList
%>

<%  
  'Rinkoff - 30
  facilityNo=30
  intN = 0
  tname = "Morning','Noon','Evening"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
'if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
'end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%><br>&nbsp;</font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>
			
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="65%" height="20"><b>Product name</b></td>
          <td width="11%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
        </tr><%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="4" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="65%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="11%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="90%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>

<%
'ordering sheet by van - Rinkoff - 30
 deltype="Morning','Noon','Evening"
      intN = 0
      intJ = 0
      intQ = 0
      intP = 1
      intTotal=0
      intD = 0
   	  intRow = 1
      facilityNo = 30
      set DisplayReport= object.Display_OrderingSheetByVans(facilityNo, delType, delDate)
	  arOrdSheetVansDetails = DisplayReport("OrdSheetVansDetails")
	  arVansList = DisplayReport("VansList")
	  arOrdSheetVansList = DisplayReport("OrdSheetVansList")
	  set DisplayReport= nothing

if IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
	intP = 1
	intRow = 1
	For intI = 0 To ubound(arVansList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		if intI = 0 Or intRow = 1 Then
			intRow = intRow + 1
		End if

		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetVansList, 2)
			if (arVansList(0, intI) = arOrdSheetVansList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
						intRow = intRow + 1
					End if
				End if
				if intN = 0 Then
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
			End if

			if intJ = ubound(arOrdSheetVansList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetVansList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetVansList, 2) Then
					intTotal = 0
					intRow = intRow + 1
				End if
				if intI <= ubound(arVansList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arVansList, 2) Then
						intP = intP + 1
					End if
				End if
				intQ = 0				
			End if
		Next
		intD = intD + 1
	Next
Else
	intP = 1
End if

intN = 1
intJ = 0
intQ = 0
intF = 0
intD = 0
intTotal=0
if IsArray(arOrdSheetVansDetails) and IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
<td>
<p align="center"><font size="3"><b>ORDERING SHEET BY VANS - <%=arOrdSheetVansDetails(0, 0)%></b></font></p>
</td>
</tr>
<tr>
<td>
<br><br>
<b><%= arOrdSheetVansDetails(0, 0) %><br>
To the attention of: <%= arOrdSheetVansDetails(1, 0) %><br>
By fax number: <%= arOrdSheetVansDetails(2, 0) %></b><br><br>
Date of report: <%= Day(Now()) & "/" & Month(Now()) & "/" & Year(Now()) %><br>
To be produced and  delivered on <%= Day(FormatDateTime(delDate, 2)) & "/" & Month(FormatDateTime(delDate, 2)) & "/" & Year(FormatDateTime(delDate, 2)) %> <%if deltype="Morning" then%> before 05:00 AM<%end if%><br><br>
Number of pages for this report: <%= intP %>
<br><br>
</td>
</tr>
</table><%
if IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
	currentPage = 1
	intRow = 1
	For intI = 0 To ubound(arVansList, 2)
		if intI = 0 Then
			intRow = 9 ' Changed by Selva  10 TO 9
		End if
		if intI = 0 Or intRow = 1 Then%>
	<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
		<tr>
		<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
		</tr>
	</table><%
			intRow = intRow + 1
			currentPage = currentPage + 1
		End if
%>
<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
	<tr>
	<td width="75%" height="20"><b>Order for Van <%= arVansList(0, intI) %></b></td>
	</tr>
</table><%
		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetVansList, 2)
			if (arVansList(0, intI) = arOrdSheetVansList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
					%>
						<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
							<tr>
							<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
							</tr>

						</table><%
						intRow = intRow + 1
						currentPage = currentPage + 1
					End if
%>
<table align="center" border="1" width="90%" cellspacing="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000" cellpadding="0"><%
				End if
				if intN = 0 Then
%>
	<tr>
	<td width="12%" height="20"><b>Product code</b></td>
	<td width="54%" height="20"><b>Product name</b></td>
	<td width="9%" height="20"><b>Quantity</b></td>
	</tr><%
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
	%>
	<tr>
	<td width="12%" height="20"><%= arOrdSheetVansList(2, intJ) %></td>
	<td width="54%" height="20"><%= arOrdSheetVansList(3, intJ) %></td>
	<td width="9%" height="20"><%= arOrdSheetVansList(4, intJ) %></td>
	</tr><%
				intTotal = intTotal + arOrdSheetVansList(4, intJ)
			End if

			if intJ = ubound(arOrdSheetVansList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetVansList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetVansList, 2) Then
%>
	<tr>
	<td width="66%" height="20" colspan="2" align="right"><b>Total&nbsp; </b></td>
	<td width="9%" height="20"><b><%= intTotal %>&nbsp;</b></td>
	</tr><%
					intTotal = 0
					intRow = intRow + 1
				End if
%>
</table><%
				if intI <= ubound(arVansList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arVansList, 2) Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
					End if
				End if
				intQ = 0
			End if
		Next
		intD = intD + 1
	Next
End if
%>
<p>&nbsp;</p>
<%
End if


If IsArray(arOrdSheetVansDetails) Then erase arOrdSheetVansDetails
If IsArray(arVansList) Then erase arVansList
If IsArray(arOrdSheetVansList) Then erase arOrdSheetVansList
%>

<%  
  'Simply Bread - 32
  facilityNo=32
  intN = 0
  tname = "Morning','Noon','Evening"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
'if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
'end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%><br>&nbsp;</font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="65%" height="20"><b>Product name</b></td>
          <td width="11%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
        </tr><%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="4" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="65%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="11%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="90%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>

<%
'ordering sheet by van - Simply Bread - 32
 deltype="Morning','Noon','Evening"
      intN = 0
      intJ = 0
      intQ = 0
      intP = 1
      intTotal=0
      intD = 0
   	  intRow = 1
      facilityNo = 32
      set DisplayReport= object.Display_OrderingSheetByVans(facilityNo, delType, delDate)
	  arOrdSheetVansDetails = DisplayReport("OrdSheetVansDetails")
	  arVansList = DisplayReport("VansList")
	  arOrdSheetVansList = DisplayReport("OrdSheetVansList")
	  set DisplayReport= nothing

if IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
	intP = 1
	intRow = 1
	For intI = 0 To ubound(arVansList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		if intI = 0 Or intRow = 1 Then
			intRow = intRow + 1
		End if

		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetVansList, 2)
			if (arVansList(0, intI) = arOrdSheetVansList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
						intRow = intRow + 1
					End if
				End if
				if intN = 0 Then
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
			End if

			if intJ = ubound(arOrdSheetVansList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetVansList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetVansList, 2) Then
					intTotal = 0
					intRow = intRow + 1
				End if
				if intI <= ubound(arVansList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arVansList, 2) Then
						intP = intP + 1
					End if
				End if
				intQ = 0				
			End if
		Next
		intD = intD + 1
	Next
Else
	intP = 1
End if

intN = 1
intJ = 0
intQ = 0
intF = 0
intD = 0
intTotal=0
if IsArray(arOrdSheetVansDetails) and IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
<td>
<p align="center"><font size="3"><b>ORDERING SHEET BY VANS - <%=arOrdSheetVansDetails(0, 0)%></b></font></p>
</td>
</tr>
<tr>
<td>
<br><br>
<b><%= arOrdSheetVansDetails(0, 0) %><br>
To the attention of: <%= arOrdSheetVansDetails(1, 0) %><br>
By fax number: <%= arOrdSheetVansDetails(2, 0) %></b><br><br>
Date of report: <%= Day(Now()) & "/" & Month(Now()) & "/" & Year(Now()) %><br>
To be produced and  delivered on <%= Day(FormatDateTime(delDate, 2)) & "/" & Month(FormatDateTime(delDate, 2)) & "/" & Year(FormatDateTime(delDate, 2)) %> <%if deltype="Morning" then%> before 05:00 AM<%end if%><br><br>
Number of pages for this report: <%= intP %>
<br><br>
</td>
</tr>
</table><%
if IsArray(arVansList) And IsArray(arOrdSheetVansList) Then
	currentPage = 1
	intRow = 1
	For intI = 0 To ubound(arVansList, 2)
		if intI = 0 Then
			intRow = 14 ' Changed by Selva 
		End if
		if intI = 0 Or intRow = 1 Then%>
	<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
		<tr>
		<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
		</tr>
	</table><%
			intRow = intRow + 1
			currentPage = currentPage + 1
		End if
%>
<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
	<tr>
	<td width="75%" height="20"><b>Order for Van <%= arVansList(0, intI) %></b></td>
	</tr>
</table><%
		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetVansList, 2)
			if (arVansList(0, intI) = arOrdSheetVansList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
					%>
						<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
							<tr>
							<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
							</tr>

						</table><%
						intRow = intRow + 1
						currentPage = currentPage + 1
					End if
%>
<table align="center" border="1" width="90%" cellspacing="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000" cellpadding="0"><%
				End if
				if intN = 0 Then
%>
	<tr>
	<td width="12%" height="20"><b>Product code</b></td>
	<td width="54%" height="20"><b>Product name</b></td>
	<td width="9%" height="20"><b>Quantity</b></td>
	</tr><%
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
	%>
	<tr>
	<td width="12%" height="20"><%= arOrdSheetVansList(2, intJ) %></td>
	<td width="54%" height="20"><%= arOrdSheetVansList(3, intJ) %></td>
	<td width="9%" height="20"><%= arOrdSheetVansList(4, intJ) %></td>
	</tr><%
				intTotal = intTotal + arOrdSheetVansList(4, intJ)
			End if

			if intJ = ubound(arOrdSheetVansList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetVansList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetVansList, 2) Then
%>
	<tr>
	<td width="66%" height="20" colspan="2" align="right"><b>Total&nbsp; </b></td>
	<td width="9%" height="20"><b><%= intTotal %>&nbsp;</b></td>
	</tr><%
					intTotal = 0
					intRow = intRow + 1
				End if
%>
</table><%
				if intI <= ubound(arVansList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arVansList, 2) Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
					End if
				End if
				intQ = 0
			End if
		Next
		intD = intD + 1
	Next
End if
%>
<p>&nbsp;</p><%
End if


If IsArray(arOrdSheetVansDetails) Then erase arOrdSheetVansDetails
If IsArray(arVansList) Then erase arVansList
If IsArray(arOrdSheetVansList) Then erase arOrdSheetVansList
%>

<%  
  '23-Baker & Spice (Queens Park) 
  facilityNo=23
  intN = 0
  tname = "Morning','Noon','Evening"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
'if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
'end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%><br>&nbsp;</font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="65%" height="20"><b>Product name</b></td>
          <td width="11%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
        </tr><%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="4" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="65%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="11%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="90%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>

<%
'ordering sheet by customer - 23-Baker & Spice (Queens Park)
	  deldate= request.form("deldate")
      facilityNo=23
      intN = 0
      intJ = 0
      intQ = 0
      intP = 1
      intTotal = 0
      intF = 0
      intD = 0
	  intRow = 1
      delType = "All"
     set DisplayReport= object.Display_OrderingSheetByCustomer(facilityNo, delType, delDate)
     arOrdSheetCustomerDetails = DisplayReport("OrdSheetCustomerDetails")
     arCustomerList = DisplayReport("CustomerList")
     arOrdSheetCustomerList = DisplayReport("OrdSheetCustomerList")
     set DisplayReport= nothing

    
if IsArray(arCustomerList) And IsArray(arOrdSheetCustomerList) Then
	intP = 1
	intRow = 1
	For intI = 0 To ubound(arCustomerList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		if intI = 0 Or intRow = 1 Then
			intRow = intRow + 1
		End if

		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetCustomerList, 2)
			if (arCustomerList(0, intI) = arOrdSheetCustomerList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
						intRow = intRow + 1
					End if
				End if
				if intN = 0 Then
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetCustomerList, 2) Then
					intTotal = 0
					intRow = intRow + 1
				End if
				if intI <= ubound(arCustomerList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arCustomerList, 2) Then
						intP = intP + 1
					End if
				End if
				intQ = 0				
			End if
		Next
		intD = intD + 1
	Next
Else
	intP = 1
End if

intN = 1
intJ = 0
intQ = 0
intF = 0
intD = 0
intTotal=0
if IsArray(arOrdSheetCustomerDetails) and IsArray(arCustomerList) And IsArray(arOrdSheetCustomerList) Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
<td>
<p align="center"><font size="3"><b>ORDERING SHEET BY CUSTOMER - <%= arOrdSheetCustomerDetails(0, 0) %></b></font></p>
</td>
</tr>
<tr>
<td>
<br><br>
<b><%= arOrdSheetCustomerDetails(0, 0) %><br>
To the attention of: <%= arOrdSheetCustomerDetails(1, 0) %><br>
By fax number: <%= arOrdSheetCustomerDetails(2, 0) %></b><br><br>
Date of report: <%= Day(Now()) & "/" & Month(Now()) & "/" & Year(Now()) %><br>
To be produced and ready to be delivered on <%= Day(FormatDateTime(delDate, 2)) & "/" & Month(FormatDateTime(delDate, 2)) & "/" & Year(FormatDateTime(delDate, 2)) %> <%if deltype="Morning" then%> before 05:00 AM<%end if%><br><br>
Number of pages for this report: <%= intP %>
<br><br>
</td>
</tr>
</table><%
if IsArray(arCustomerList) And IsArray(arOrdSheetCustomerList) Then
	currentPage = 1
	intRow = 1
	For intI = 0 To ubound(arCustomerList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		if intI = 0 Or intRow = 1 Then%>
			<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
				<tr>
				<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
				</tr>
			</table><%
			intRow = intRow + 1
			currentPage = currentPage + 1
		End if
%>
<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
	<tr>
	<td height="20"><b>Customer Code: <%= arCustomerList(0, intI) %></b></td>
	<td height="20"><b>Customer Name: <%= arCustomerList(1, intI) %></b></td>
	<td height="20"><b>Van No.: <%= arCustomerList(2, intI) %></b></td>
	</tr>
</table><%
		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetCustomerList, 2)
			if (arCustomerList(0, intI) = arOrdSheetCustomerList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
					%>
						<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
							<tr>
							<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
							</tr>
						</table><%
						intRow = intRow + 1
						currentPage = currentPage + 1
					End if
%>
<table align="center" border="1" width="90%" cellspacing="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000" cellpadding="0"><%
				End if
				if intN = 0 Then
%>
	<tr>
	<td width="11%" height="20"><b>Product code</b></td>
	<td width="56%" height="20"><b>Product name</b></td>
	<td width="8%" height="20"><b>Quantity</b></td>
	</tr><%
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
	%>
	<tr>
	<td width="11%" height="20"><%= arOrdSheetCustomerList(4, intJ) %></td>
	<td width="56%" height="20"><%= arOrdSheetCustomerList(5, intJ) %></td>
	<td width="8%" height="20"><%= arOrdSheetCustomerList(6, intJ) %></td>
	</tr><%
				intTotal = intTotal + arOrdSheetCustomerList(6, intJ)
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetCustomerList, 2) Then
%>
	<tr>
	<td width="67%" height="20" colspan="2" align="right"><b>Total&nbsp; </b></td>
	<td width="8%" height="20"><b><%= intTotal %>&nbsp;</b></td>
	</tr><%
					intTotal = 0
					intRow = intRow + 1
				End if
%>
</table><%
				if intI <= ubound(arCustomerList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arCustomerList, 2) Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
					End if
				End if
				intQ = 0
			End if
		Next
		intD = intD + 1
	Next
	'curpage=clng(curpage)+1
End if
%>
<p>&nbsp;</p><%
End if
If IsArray(arOrdSheetCustomerDetails) Then erase arOrdSheetCustomerDetails
If IsArray(arCustomerList) Then erase arCustomerList
If IsArray(arOrdSheetCustomerList) Then erase arOrdSheetCustomerList
set DisplayReport= nothing

%>

<%  
  '21-Baker & Spice (Denier Street)
  facilityNo=21
  intN = 0
  tname = "Morning','Noon','Evening"
  set DisplayDailyFacilityOrderSheet= object.DisplayDailyFacilityOrderSheet(deldate,facilityNo,tname)
  vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
  Facility = DisplayDailyFacilityOrderSheet("Facility")
  set DisplayDailyFacilityOrderSheet= nothing
  intF=1
		If isArray(vRecArray) Then
			If ubound(vRecArray, 2) = 0 Then
				intF = 1
			Else
				intF = Round(((ubound(vRecArray, 2)+1)/PrintPgSize) + 0.49)
			End If
		End If
if isarray(vRecArray) then 
intTotal=0
'if k > 0 then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
'end if%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3"> Ordering Sheets - <%=Facility(0,0)%><br>&nbsp;</font></b>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<b>By fax number: <%=Facility(2,0)%></b><br><br>
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if tname="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %>
			<%Response.Write(replace(space(70)," ","&nbsp;"))%>
			</td>
			<td align="right" valign="bottom">
			Page 1 of <%=intF%>
			<br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then 
    curpage=1
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="14%" height="20"><b>Product code</b></td>
          <td width="65%" height="20"><b>Product name</b></td>
          <td width="11%" height="20"><b>Size</b></td>
          <td width="10%" height="20"><b>Quantity</b></td>
        </tr><%
		end if
		For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
			if intJ=1 and clng(curpage) > 1 then
		%>
		<tr>
		  <td colspan="4" height="20" align="right">Page <%=curpage%> of <%=intF%></td>
		</tr>
		<%end if%>
        <tr>
          <td width="14%" height="20"><%=vRecArray(0,intN)%></td>
          <td width="65%" height="20"><%=vRecArray(1,intN)%></td>
          <td width="11%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="10%" height="20"><%=vRecArray(3,intN) %></td>
		  </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="90%" height="20" colspan="3" align="right"><b>Total&nbsp; </b></td>
          <td width="10%" height="20"><b><%=intTotal%>&nbsp;</b></td>
        </tr><%
			End if
			intN = intN + 1
		Next
		curpage=clng(curpage)+1
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
<%end if%>

<%
'ordering sheet by customer - 21-Baker & Spice (Denier Street)
	  deldate= request.form("deldate")
   	  facilityNo=21
      intN = 0
      intJ = 0
      intQ = 0
      intP = 1
      intTotal = 0
      intF = 0
      intD = 0
	  intRow = 1
      delType = "All"
     set DisplayReport= object.Display_OrderingSheetByCustomer(facilityNo, delType, delDate)
     arOrdSheetCustomerDetails = DisplayReport("OrdSheetCustomerDetails")
     arCustomerList = DisplayReport("CustomerList")
     arOrdSheetCustomerList = DisplayReport("OrdSheetCustomerList")
     set DisplayReport= nothing

    
if IsArray(arCustomerList) And IsArray(arOrdSheetCustomerList) Then
	intP = 1
	intRow = 1
	For intI = 0 To ubound(arCustomerList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		if intI = 0 Or intRow = 1 Then
			intRow = intRow + 1
		End if

		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetCustomerList, 2)
			if (arCustomerList(0, intI) = arOrdSheetCustomerList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
						intRow = intRow + 1
					End if
				End if
				if intN = 0 Then
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetCustomerList, 2) Then
					intTotal = 0
					intRow = intRow + 1
				End if
				if intI <= ubound(arCustomerList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arCustomerList, 2) Then
						intP = intP + 1
					End if
				End if
				intQ = 0				
			End if
		Next
		intD = intD + 1
	Next
Else
	intP = 1
End if

intN = 1
intJ = 0
intQ = 0
intF = 0
intD = 0
intTotal=0
if IsArray(arOrdSheetCustomerDetails) and IsArray(arCustomerList) And IsArray(arOrdSheetCustomerList) Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<tr>
<td>
<p align="center"><font size="3"><b>ORDERING SHEET BY CUSTOMER - <%= arOrdSheetCustomerDetails(0, 0) %></b></font></p>
</td>
</tr>
<tr>
<td>
<br><br>
<b><%= arOrdSheetCustomerDetails(0, 0) %><br>
To the attention of: <%= arOrdSheetCustomerDetails(1, 0) %><br>
By fax number: <%= arOrdSheetCustomerDetails(2, 0) %></b><br><br>
Date of report: <%= Day(Now()) & "/" & Month(Now()) & "/" & Year(Now()) %><br>
To be produced and ready to be delivered on <%= Day(FormatDateTime(delDate, 2)) & "/" & Month(FormatDateTime(delDate, 2)) & "/" & Year(FormatDateTime(delDate, 2)) %> <%if deltype="Morning" then%> before 05:00 AM<%end if%><br><br>
Number of pages for this report: <%= intP %>
<br><br>
</td>
</tr>
</table><%
if IsArray(arCustomerList) And IsArray(arOrdSheetCustomerList) Then
	currentPage = 1
	intRow = 1
	For intI = 0 To ubound(arCustomerList, 2)
		if intI = 0 Then
			intRow = 9
		End if
		if intI = 0 Or intRow = 1 Then%>
			<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
				<tr>
				<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
				</tr>
			</table><%
			intRow = intRow + 1
			currentPage = currentPage + 1
		End if
%>
<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
	<tr>
	<td height="20"><b>Customer Code: <%= arCustomerList(0, intI) %></b></td>
	<td height="20"><b>Customer Name: <%= arCustomerList(1, intI) %></b></td>
	<td height="20"><b>Van No.: <%= arCustomerList(2, intI) %></b></td>
	</tr>
</table><%
		intRow = intRow + 1
		intN = 0
		intF = 0
		For intJ = 0 To ubound(arOrdSheetCustomerList, 2)
			if (arCustomerList(0, intI) = arOrdSheetCustomerList(0, intJ)) Or intF = 1 Then
				intRow = intRow + 1
				if intQ = 0 Or intJ = 0 Or intF = 1 Then
					intF = 0
					if intRow = 2 Then
					%>
						<table align="center" border="0" width="90%" cellspacing="1" style="font-family: Verdana; font-size: 8pt">
							<tr>
							<td height="20" align="right"><b>Page <%= currentPage %> of <%= intP %></b></td>
							</tr>
						</table><%
						intRow = intRow + 1
						currentPage = currentPage + 1
					End if
%>
<table align="center" border="1" width="90%" cellspacing="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000" cellpadding="0"><%
				End if
				if intN = 0 Then
%>
	<tr>
	<td width="11%" height="20"><b>Product code</b></td>
	<td width="56%" height="20"><b>Product name</b></td>
	<td width="8%" height="20"><b>Quantity</b></td>
	</tr><%
					intRow = intRow + 1
					intN = 1
				End if
				intQ = 1
	%>
	<tr>
	<td width="11%" height="20"><%= arOrdSheetCustomerList(4, intJ) %></td>
	<td width="56%" height="20"><%= arOrdSheetCustomerList(5, intJ) %></td>
	<td width="8%" height="20"><%= arOrdSheetCustomerList(6, intJ) %></td>
	</tr><%
				intTotal = intTotal + arOrdSheetCustomerList(6, intJ)
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) And intRow + 6 >= PrintPgSize Then
				intRow = PrintPgSize
			End if

			if intJ = ubound(arOrdSheetCustomerList, 2) Or intRow >= PrintPgSize Then
				intF = 1
				if intJ = ubound(arOrdSheetCustomerList, 2) Then
%>
	<tr>
	<td width="67%" height="20" colspan="2" align="right"><b>Total&nbsp; </b></td>
	<td width="8%" height="20"><b><%= intTotal %>&nbsp;</b></td>
	</tr><%
					intTotal = 0
					intRow = intRow + 1
				End if
%>
</table><%
				if intI <= ubound(arCustomerList, 2) And intRow >= PrintPgSize Then
					intRow = 1
					if intD <> ubound(arCustomerList, 2) Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
					End if
				End if
				intQ = 0
			End if
		Next
		intD = intD + 1
	Next
	'curpage=clng(curpage)+1
End if
%>
<p>&nbsp;</p>
<%
End if
If IsArray(arOrdSheetCustomerDetails) Then erase arOrdSheetCustomerDetails
If IsArray(arCustomerList) Then erase arCustomerList
If IsArray(arOrdSheetCustomerList) Then erase arOrdSheetCustomerList
set DisplayReport= nothing
%>
<%
set object = nothing
%>
<p>&nbsp;</p>
</body>
</html>