<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Response.Buffer
Dim Obj,ObjCustomer
Dim arCustomer
Dim strCustomerName,strCustNo
Dim vPageSize
Dim strDirection
Dim vpagecount
Dim vCurrentPage
Dim i,StrOrderType,processed 

if request.querystring("processed")="" then
	processed = request.form("processed")
	session("processed") = request.form("processed")
else
	processed = request.querystring("processed")
	session("processed") = request.querystring("processed")
end if

vPageSize = 50
strDirection	= Request.Form("Direction")
vpagecount		= Request.Form("pagecount") 
vCurrentPage	= Request.Form("CurrentPage") 
strCustomerName = Replace(Request.Form("txtCustName"),"'","''")
strCustNo = Replace(Request.Form("txtCustNo"),"'","''")

If strDirection = "First" Then
	vCurrentPage = 1
Elseif strDirection = "Previous" Then
	vCurrentPage = vCurrentPage - 1
Elseif strDirection = "Next" Then
	vCurrentPage = vCurrentPage +1	
Elseif strDirection = "Last" Then
	vCurrentPage = vpagecount
Else
	vCurrentPage = 1
End if

if Trim(strCustNo) <> "" then
	if not IsNumeric(strCustNo) Then
		strCustNo = 0 
	End if  
End if


Set Obj = CreateObject("Bakery.Orders")
Obj.SetEnvironment(strconnection)
Set ObjCustomer = obj.Display_CustomerList(vPageSize,vCurrentPage,strCustomerName,strCustNo)
arCustomer = ObjCustomer("Customer")
vpagecount  = ObjCustomer("Pagecount")

Set Obj = Nothing
Set ObjCustomer = Nothing
%>


<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3"><% if request("processed")="" then%>New/Sample Order List<%else%>New Order After Processing List<%end if%><br>&nbsp;</font></b>
      <form name="frmOrderSearch" method="post">
      <input type="hidden" name="processed" value = "<%=processed%>">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
          <td><b>Search By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          <td><b>Customer Code:</b></td>
          <td><b>Customer Name:</b></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td><input type="text" name="txtCustNo"  value="<%=strCustNo%>" size="20" style="font-family: Verdana; font-size: 8pt"></td>
          <td><input type="text" name="txtCustName" value="<%=strCustomerName%>"  size="20" style="font-family: Verdana; font-size: 8pt"></td>          
          <td><input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td><b>&nbsp;</b></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
      </form>
      <%if vCurrentPage <> 1 then%>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">      
				<tr>
					<td height="20"></td>
				</tr><%				
				if IsArray(arCustomer) Then%>
					<tr>
					  <td align="right"><b>Page <%=vCurrentPage%> of <%=vpagecount%></b></td>          
					</tr><%
				End if%>	
			</table> 
      <%End if%>

      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="25%" bgcolor="#CCCCCC"><b>Customer Code</b></td>
          <td width="50%" bgcolor="#CCCCCC"><b>Customer Name</b></td>                    
          <td width="25%" bgcolor="#CCCCCC"><b>Action</b></td>
        </tr><%        
						if IsArray(arCustomer) Then 
								for i = 0 to UBound(arCustomer,2)
								if (i mod 50=0) then
									Response.Flush()
								end if	
								%>
									<tr>
										<td><%=arCustomer(0,i)%></td>
										<td><%=arCustomer(1,i)%></td>										
										<form method="POST" action="order_Sample1.asp" id="frmTypical<%=i%>" name="frmTypical<%=i%>">
											<td>
                       <input type="submit" value="Create New/Sample order " name="B1" style="font-family: Verdana; font-size: 8pt"></td>
											<input type="hidden" name="CustNo" value="<%=arCustomer(0,i)%>">
											<input type="hidden" name="OrderType" value="<%=arCustomer(2,i)%>">
											<input type="hidden" name="processed" value = "<%=processed%>">
										</form>
									</tr><%
								Next
						 else%>
							<tr>
							<td Colspan="3">Sorry no items found</td>						
						</tr><%						 		
						End if%>       
      </table>
    </td>
  </tr>
</table>

	
	<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		<FORM Name="Navigate">
			<tr>					
				<td width="187"><%				
		
					if vCurrentPage <> 1 then%>
						<input type="button" value="FirstPage" Onclick="document.frmFirst.submit()" id=button1 name=button1 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) > 1 then%>
						<input type="button" value="PreviousPage" Onclick="document.frmPrevious.submit()" id=button3 name=button3 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%				
					if clng(vCurrentPage) < vPagecount then%>
						<input type="button" value="NextPage" Onclick="document.frmNext.submit()" id=button5 name=button5 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>
							
				<td width="187"><%
					if clng(vCurrentPage) <> vPagecount and vPagecount > 0  then%>
						<input type="button" value="LastPage" Onclick="document.frmLast.submit()" id=button7 name=button7 style="font-family: Verdana; font-size: 8pt"><%				
					End if%>	
				</td>					
			</tr>
		</FORM>
	</table>
  </center>
</div>

	<FORM METHOD="post" NAME="frmFirst">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="First">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">				
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<input type="hidden" name="processed" value = "<%=processed%>">
	</FORM>

	<FORM METHOD="post" NAME="frmPrevious">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Previous">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<input type="hidden" name="processed" value = "<%=processed%>">	
	</FORM>

	<FORM METHOD="post" NAME="frmNext">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Next">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<input type="hidden" name="processed" value = "<%=processed%>">		
	</FORM>

	<FORM METHOD="post" NAME="frmLast">
		<INPUT TYPE="hidden" NAME="Direction"			VALUE="Last">
		<INPUT TYPE="hidden" NAME="pagecount"			VALUE="<%=vPagecount%>">		
		<INPUT TYPE="hidden" NAME="CurrentPage"		VALUE="<%=vCurrentPage%>">
		<INPUT TYPE="hidden" NAME="txtCustName"		VALUE="<%=strCustomerName%>">
		<INPUT TYPE="hidden" NAME="txtCustNo"			VALUE="<%=strCustNo%>">
		<input type="hidden" name="processed" value = "<%=processed%>">	
	</FORM>


</body>
</html><%
if IsArray(arCustomer) Then Erase arCustomer
%>