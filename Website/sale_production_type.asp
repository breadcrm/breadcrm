<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title>Sales Report - By Production Type</title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal()">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim recarray
dim retcol
dim fromdt, todt, i,Totalturnover,vNumber


fromdt = Request.form("txtfrom")
todt = Request.Form("txtto")

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
   todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

if isdate(fromdt) and isdate(todt) then
	set objBakery = server.CreateObject("Bakery.reports")
	objBakery.SetEnvironment(strconnection)
	set retcol = objBakery.Display_ProductionType(fromdt,todt)
	recarray = retcol("ProductionType")
	set retcol = nothing
	set objBakery = nothing
end if
%>
<div align="center">
<center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">

 <tr>
    <td width="100%"><b><font size="3">Sales Report - By Production Type<br>
      &nbsp;</font></b>
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <form method="post" action="sale_production_type.asp" name="form">

		<tr>
          <td></td>
          <td><b>From:</b></td>
          <td><b>To:</b></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
         <td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom size="12" onFocus="blur();" name="txtfrom" value="<%=replace(fromdt,"from","")%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=replace(todt,"to","")%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					<td>
            <input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr></form>
		 <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
			</SCRIPT>
			<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
			</SCRIPT>
      </table>
      <table border="1" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%" colspan="3"><b>Sales Report From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
        <tr>
          <td width="60%"  bgcolor="#CCCCCC"><b>Production Type&nbsp;</b></td>
          <td width="20%" bgcolor="#CCCCCC"><b>Number of Products&nbsp;</b></td>
          <td width="20%" bgcolor="#CCCCCC" align="right"><b>Turnover&nbsp;</b></td>
          
        </tr>
			<tr>
				<%if isarray(recarray) then%>
				<% 				
				Totalturnover = 0.00
				vNumber = 0
				%>
				<%for i=0 to UBound(recarray,2)%>
					<td width="60%"><b><%if recarray(0,i)<> "" then Response.Write recarray(0,i) end if%></b>&nbsp;</td>
          <td width="20%" align="right"><b><%if recarray(1,i)<> "" then Response.Write formatnumber(recarray(1,i),0) else Response.Write "0" end if%>&nbsp;&nbsp;</b></td> 
          <td width="20%" align="right"><b><%if recarray(2,i)<> "" then Response.Write formatnumber(recarray(2,i),2) else Response.Write "0.00" end if%>&nbsp;&nbsp;</b></td>
            
          </tr>
          <%

          if recarray(1,i)<> "" then 
				 		vNumber = vNumber + formatnumber(recarray(1,i),0)
				  End if

          if recarray(2,i)<> "" then 
				 		Totalturnover = Totalturnover + formatnumber(recarray(2,i),2)
				  End if

          %>
          <%next%>
          <tr>
           <td width="60%" >&nbsp;<b>Grand Total</b></td>
           <td width="20%" align="right" ><b>&nbsp;<%=formatnumber(vNumber,0)%>&nbsp;&nbsp;</b></td> 
           <td width="20%" align="right" ><b>&nbsp;<%=formatnumber(Totalturnover,2)%>&nbsp;&nbsp;</b></td> 
                    
          </tr>
          <%
          else%>
          <td colspan="3" width="100%" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td> <% 
          end if%>
        </tr>

      </table>
    </td>
  </tr>
</table>
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;<br><br><br><br><br><br>
</center>
</div>
</body>
<%if IsArray(recarray) then erase recarray%>
</html>