<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<%
Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayIng()	
vingarray =  detail("Ingredients")

set detail = object.ListDoughType()	
vDoughTypearray =  detail("DoughType") 

set detail = object.DisplayDoughType()	
vDougharray =  detail("DoughType")

set detail = Nothing
set object = Nothing

strSecondMixCategory=request("SecondMixCategory")
if (strSecondMixCategory="") then
	strSecondMixCategory="0"
end if

%>
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css">
<!--
.radioinput
{
 color: #000000; 
 background-color:#FFF; 
 border:none;
 margin:0px;
}
-->
</style>
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{
  if (theForm.doughname.value == "")
  {
    alert("Please enter a value for the \"Dough Name\" field.");
    theForm.doughname.focus();
    return (false);
  }
  
  if (theForm.DoughType.value == "")
  {
    alert("Please enter a value for the \"Dough Type\" field.");
    theForm.DoughType.focus();
    return (false);
  }
  
  if (theForm.SecondMixCategory[1].checked)
  {
  		  if (theForm.MainDough.value == "")
		  {
			alert("Please enter a value for the \"Main Dough\" field.");
			theForm.MainDough.focus();
			return (false);
		  }
		  
		  if (theForm.SecondMixCategoryWeight.value == "")
		  {
			alert("Please enter a value for the \"Amount of main dough to create 1 kilo of second mix\" field.");
			theForm.SecondMixCategoryWeight.focus();
			return (false);
		  }
  }
 
  if (theForm.I1.selectedIndex < 0)
  {
    alert("Please select one of the \"I1\" options.");
    theForm.I1.focus();
    return (false);
  }

  if (theForm.qty1.value == "")
  {
    alert("Please enter a value for the \"qty1\" field.");
    theForm.qty1.focus();
    return (false);
  }
  
  if (theForm.SecondMixCategory[2].checked)
  {
	if (theForm.sqty1.value == "")
	{
		alert("Please enter a value for the \"qty1\" field for second mix.");
		theForm.sqty1.focus();
		return (false);
	}
  }
  
  dTotalValue=0;
  dTotalValue = document.getElementById("TotalValue").innerHTML;
  
  if (dTotalValue > 1)
  {
  	alert("Please check entered value for quantities as total exceeds 1 kg.");
	return false;
  }
  
  return (true);
}

function ShowSecondMix(chk)
{
	if (chk.value == "1")
	{
		document.getElementById("trMainDough").style.display = "";
		document.getElementById("trSecondMixCategoryWeight").style.display = "";
	}
	else
	{
		document.getElementById("trMainDough").style.display = "none";
		document.getElementById("trSecondMixCategoryWeight").style.display = "none";
	}
	
	if (chk.value == "2")
	{
		//Second mix column header
		document.getElementById("tdsqtyheader").style.display = "";
		//Second mix column total
		document.getElementById("tdsqtytotal").style.display = "";
		
		document.getElementById("tdsqty1").style.display = "";
		document.getElementById("tdsqty2").style.display = "";
		document.getElementById("tdsqty3").style.display = "";
		document.getElementById("tdsqty4").style.display = "";
		document.getElementById("tdsqty5").style.display = "";
		document.getElementById("tdsqty6").style.display = "";
		document.getElementById("tdsqty7").style.display = "";
		document.getElementById("tdsqty8").style.display = "";
		document.getElementById("tdsqty9").style.display = "";
		document.getElementById("tdsqty10").style.display = "";
		document.getElementById("tdsqty11").style.display = "";
		document.getElementById("tdsqty12").style.display = "";
		document.getElementById("tdsqty13").style.display = "";
		document.getElementById("tdsqty14").style.display = "";
		document.getElementById("tdsqty15").style.display = "";
	}
	else
	{
		//Second mix column header
		document.getElementById("tdsqtyheader").style.display = "none";
		//Second mix column total
		document.getElementById("tdsqtytotal").style.display = "none";
		
		document.getElementById("tdsqty1").style.display = "none";
		document.getElementById("tdsqty2").style.display = "none";
		document.getElementById("tdsqty3").style.display = "none";
		document.getElementById("tdsqty4").style.display = "none";
		document.getElementById("tdsqty5").style.display = "none";
		document.getElementById("tdsqty6").style.display = "none";
		document.getElementById("tdsqty7").style.display = "none";
		document.getElementById("tdsqty8").style.display = "none";
		document.getElementById("tdsqty9").style.display = "none";
		document.getElementById("tdsqty10").style.display = "none";
		document.getElementById("tdsqty11").style.display = "none";
		document.getElementById("tdsqty12").style.display = "none";
		document.getElementById("tdsqty13").style.display = "none";
		document.getElementById("tdsqty14").style.display = "none";
		document.getElementById("tdsqty15").style.display = "none";
	}
	
	//Lets change the header of first ingredent as per selection.
	if (chk.value == "2")
	{
		document.getElementById("lblDough").innerHTML = "first mix";
	}
	else
	{
		document.getElementById("lblDough").innerHTML = "dough";
	}
}

function TotalValue (theForm)
{	
	x=theForm.value;
	if (x !="")
	{
		if (document.getElementById("TotalValue").innerHTML!="")
		{
			dTotalValue=parseFloat(x)+parseFloat(document.getElementById("TotalValue").innerHTML)
			document.getElementById("TotalValue").innerHTML=Math.round(dTotalValue*Math.pow(10,4))/Math.pow(10,4);
			document.getElementById("TotalValueKg").innerHTML="kg";
		}	
		else
		{
			document.getElementById("TotalValue").innerHTML=Math.round(parseFloat(x)*Math.pow(10,4))/Math.pow(10,4);
			document.getElementById("TotalValueKg").innerHTML="kg";
		}	
	}
}

function TotalValueLoad()
{	
	document.getElementById("TotalValueKg").innerHTML="kg";
	//document.getElementById("STotalValueKg").innerHTML="kg";
	
	dTotalValue=0;
	//total of second mix ingredient
	dSTotalValue=0;
	
	boolSecondMixCategory=document.FrontPage_Form1.SecondMixCategory[1].checked;
	
	for (i=1;i<=15;i++)
	{
		vqty="qty"+i
		x=document.getElementById(vqty).value;
		if (x!="")
		{
			dTotalValue=parseFloat(x)+parseFloat(dTotalValue)
			dTotalValue=Math.round(dTotalValue*Math.pow(10,4))/Math.pow(10,4);
		}
		
		//lets calculate second mix ingredient total
		vsqty="sqty"+i
		sx=document.getElementById(vsqty).value;
		if (sx!="")
		{
			dSTotalValue=parseFloat(sx)+parseFloat(dSTotalValue)
			dSTotalValue=Math.round(dSTotalValue*Math.pow(10,4))/Math.pow(10,4);
		}
	}
	
	if (boolSecondMixCategory)
	{
		if (document.getElementById("SecondMixCategoryWeight").value!="")
			dTotalValue+=parseFloat(document.getElementById("SecondMixCategoryWeight").value);
	}
	
	document.getElementById("TotalValue").innerHTML=dTotalValue + dSTotalValue;
	//show total value of ingredients for second mix
	//document.getElementById("STotalValue").innerHTML=dSTotalValue;
}

//-->
</script>

</head>

<body <%if Request.QueryString ("status")="" then%> onLoad="ShowSecondMix(document.FrontPage_Form1.SecondMixCategory)" <%end if%> topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
<form method="post" name="FrontPage_Form1" action="save_inventory_doughs_new.asp" onSubmit="return FrontPage_Form1_Validator(this)">
<div align="center">
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3">Add Dough<br>
      &nbsp;</font></b>
<%
if Request.QueryString ("status") = "error" then
	Response.Write "<br><font size=""3"" color=""#FF0000""><b>Dough already exists go back to change dough name</b></font><br><br>"
elseif Request.QueryString ("status") = "ok" then
	Response.Write "<br><font size=""2"" ><b>Dough " & request.querystring("doughname") & " created successfully</b></font><br><br>"
end if
%>

<%if Request.QueryString ("status")="" then%>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0">
        <tr height="25">
          <td width="230"><b>Dough Name&nbsp;</b></td>
          <td>
          <input type="text" name="doughname" size="35" style="font-family: Verdana; font-size: 8pt" maxlength = "50"></td>
        </tr>
		<tr height="25">          
          <td><b>Dough Type&nbsp;</b></td>
          <td>
		   <select size="1" name="DoughType" style="font-family: Verdana; font-size: 8pt">
			<option value = "">Select</option>
			<%
			if IsArray(vDoughTypearray) Then  	
				for i = 0 to ubound(vDoughTypearray,2)%>  				
			 <option value="<%=vDoughTypearray(0,i)%>" <%if strDoughType<>"" then%><%if strDoughType= vDoughTypearray(0,i) then response.write " Selected"%><%end if%>><%=vDoughTypearray(1,i)%></option>
			  <%
			   next                 
			End if
			%>   
		  </select>
		  </td>
        </tr>
		<tr height="25">
			<td><b>Margin of Error&nbsp;</b></td>
			<td>
				<input type="text" name="marginoferror" size="10" value="0" style="font-family: Verdana; font-size: 8pt" maxlength = "5">
			</td>
		</tr>
		<tr height="25">
		<td><b>Dough Mixing Method&nbsp;</b></td>
		<td>
		<input type="radio" name="SecondMixCategory"  value="0" class="radioinput" onClick="ShowSecondMix(this);TotalValueLoad()" <%if strSecondMixCategory="0" then%> checked="checked" <%end if%>> Main Dough
		<input type="radio" name="SecondMixCategory" value="1" class="radioinput"  onClick="ShowSecondMix(this);TotalValueLoad()" <%if strSecondMixCategory="1" then%> checked="checked" <%end if%>> Second Mix Category
		<input type="radio" name="SecondMixCategory" value="2" class="radioinput"  onClick="ShowSecondMix(this);TotalValueLoad()" <%if strSecondMixCategory="2" then%> checked="checked" <%end if%>> Two Stage Mixing
		</td>
		</tr>
		<tr height="25" id="trMainDough">
			<td><b>Main Dough&nbsp;</b></td>
			<td>
			<select size="1" name="MainDough" style="font-family: Verdana; font-size: 8pt">
				<option value = "">Select</option>
				<%
				if IsArray(vDougharray) Then  	
					for i = 0 to ubound(vDougharray,2)%>  				
				 <option value="<%=vDougharray(0,i)%>" <%if strDoughType<>"" then%><%if strDoughType= vDougharray(0,i) then response.write " Selected"%><%end if%>><%=vDougharray(1,i)%></option>
				  <%
				   next                 
				End if
				%>   
		    </select>
			</td>
		</tr>
		<tr height="25" id="trSecondMixCategoryWeight">
		<td><b>Amount of main dough to create <br>
		  1 kilo of second mix&nbsp;</b></td>
		<td><input type="text" name="SecondMixCategoryWeight" onChange="TotalValueLoad()" size="10" style="font-family: Verdana; font-size: 8pt" maxlength = "10"></td>
		</tr>
        </table>
      <table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="0" height="383">       
        <tr>
          <td width="29%" height="13"><b>&nbsp;</b></td>
          <td width="36%" height="13"></td>
		  <td width="35%" height="13"></td>
        </tr>
        <tr>
          <td width="29%" height="22"><b>Ingredient</b></td>
          <td width="36%" height="22"><b>Quantities required to produce 1 &nbsp;kg of <label id="lblDough"></label></b></td>
		  <td width="35%" height="22" id="tdsqtyheader"><b>Quantities required to produce 1 &nbsp;kg of second mix</b></td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I1" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty1" id="qty1" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty1">
		  <input type="text" name="sqty1" id="sqty1" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I2" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty2" id="qty2" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty2">
          <input type="text" name="sqty2" id="sqty2" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I3" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty3" id="qty3" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty3">
          <input type="text" name="sqty3" id="sqty3" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I4" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty4" id="qty4" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty4">
          <input type="text" name="sqty4" id="sqty4" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I5" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty5" id="qty5" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty5">
          <input type="text" name="sqty5" id="sqty5" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I6" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty6" id="qty6" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty6">
          <input type="text" name="sqty6" id="sqty6" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I7" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty7" id="qty7" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty7">
          <input type="text" name="sqty7" id="sqty7" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I8" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty8" id="qty8" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty8">
          <input type="text" name="sqty8" id="sqty8" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I9" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty9" id="qty9" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty9">
          <input type="text" name="sqty9" id="sqty9" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I10" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty10" id="qty10" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty10">
          <input type="text" name="sqty10" id="sqty10" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I11" style="font-family: Verdana; font-size: 8pt">
          					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty11" id="qty11" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty11">
          <input type="text" name="sqty11" id="sqty11" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I12" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%> 
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty12" id="qty12" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty12">
          <input type="text" name="sqty12" id="sqty12" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I13" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%>           
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty13" id="qty13" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty13">
          <input type="text" name="sqty13" id="sqty13" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I14" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%>           
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty14" id="qty14" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty14">
          <input type="text" name="sqty14" id="sqty14" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
        <tr>
          <td width="29%" height="19">
          <select size="1" name="I15" style="font-family: Verdana; font-size: 8pt">
					<option value="">Select</option>
					<%for i = 0 to ubound(vingarray,2)%>  				
                  <option value="<%=vingarray(0,i)%>"><%=vingarray(2,i)%></option>
                  <%next%>           
            </select></td>
          <td width="36%" height="19">
          <input type="text" name="qty15" id="qty15" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
		  <td width="35%" height="19" id="tdsqty15">
          <input type="text" name="sqty15" id="sqty15" onChange="javacript:TotalValueLoad()" size="20" style="font-family: Verdana; font-size: 8pt" maxlength = "6">&nbsp;&nbsp;kg
		  </td>
        </tr>
		<tr>
          <td width="29%" height="30"><strong>Total</strong></td>
          <td width="36%" height="30"><strong><span id="TotalValue"></span>&nbsp;<span id="TotalValueKg"></span></strong></td>
		  <td width="35%" height="30" id="tdsqtytotal"><strong><span id="STotalValue"></span>&nbsp;<span id="STotalValueKg"></span></strong></td>
        </tr>
        <tr>
          <td width="29%" height="53">&nbsp;</td>
          <td width="71%" colspan="2" height="53">&nbsp;
            <p><input type="submit" value="Add Dough" name="B1" style="font-family: Verdana; font-size: 8pt"></p>
          </td>
        </tr>
      </table>
  <%else%>
  
  <%end if%>


  </center>
    </td>
  </tr>
</table>


</div>

</form>


</body>

</html>