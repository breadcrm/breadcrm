<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function ExportToExcelFile(){
	vYear=document.form.cboYear.value
	vMonth=document.form.cboMonth.value
	document.location="ExportToExcelFile_mon_report_late_crnotes.asp?year=" + vYear + "&month=" + vMonth 
}

function Hide()
{
	document.getElementById("HidePanel").style.display = "none";
}
//-->
</SCRIPT>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="Hide()">
<!--#INCLUDE FILE="nav.inc" -->
<br>
<DIV id="HidePanel">
	<P align="center"><b><font size="2" face="Verdana" color="#0099CC">Report generation in progress, Please wait...</font></b></P>
	<p align="center"><img name="animation" src="images/LoadingAnimation.gif" border="0" WIDTH="201" HEIGHT="33"></p>
</DIV>

<%
dim objBakery
dim recarray
dim retcol
dim vYear, vMonth,i
Dim TotInvAmount,TotCrdAmount


vYear = Request.form("cboYear")
vMonth = Request.Form("cboMonth")

if vYear = "" then
	if (Month(date()) = 1) Then
		vYear = Year(date()) - 1
		vMonth = 12
	else
		vYear = Year(date()) 
		vMonth = Month(date())-1		
	end if
end if

set objBakery = server.CreateObject("Bakery.Reports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.Display_LateCreditNotes(vYear,vMonth)
recarray = retcol("LateCredits")

%>
<div align="center">
<center>
<table border="0" width="100%" cellspacing="0" cellpadding="5" style="font-family: Verdana; font-size: 8pt">

 <tr>
    <td width="100%"><b><font size="3">Credit Note Report for Later Dates<br>
      &nbsp;</font></b>
	  <form method="post" action="mon_report_late_crnotes.asp" name="form">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr>
				<td height="18" width="150">
					Year :&nbsp;
						<select name="cboYear">
						  <%for i=2000 to Year(Date)+1%>
							<%If i=cint(vYear) Then%>
								<option value="<%=i%>" selected><%=i%></option>
							<%Else%>
								<option value="<%=i%>"><%=i%></option>
							<%End If%>
						  <%next%>
						</select>				

					</td>
				<td height="18" width="150">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						Month :&nbsp;
						<select name="cboMonth">
						   <%for i=0 to 11%>
							<%If (i+1)=cint(vMonth) Then%>
								<option value="<%=i+1%>" selected><%=arrayMonth(i)%></option>
							<%Else%>
								<option value="<%=i+1%>"><%=arrayMonth(i)%></option>
							<%End If%>
						  <%next%>
						</select>
				</td> 
				<td>
					<input type="submit" value="Search" name="Search" style="font-family: Verdana; font-size: 8pt">
				</td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
	  </form>
      	<table border="1" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
         <tr>
          <td colspan="9" align="left" height="30">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>
		<tr>
          <td colspan="9"><b>Late Credit Note Report For <%=vYear%>&nbsp;<%=arrayMonth(vMonth-1)%></b></td>
        </tr>
        <tr>
          <td width="110" bgcolor="#CCCCCC"><b>Credit Date&nbsp;</b></td>
		  <td width="110" bgcolor="#CCCCCC"><b>Credit Note No</b></td>
		  <td width="110" bgcolor="#CCCCCC"><b>Indirect Cus. Crd. No</b></td>
          <td width="85" bgcolor="#CCCCCC"><b>Customer No&nbsp;</b></td>
		  <td width="225" bgcolor="#CCCCCC"><b>Customer Name</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Invoice No</b></td>
		  <td width="110" bgcolor="#CCCCCC"><b>Invoice Date</b></td>
		  <td width="100" bgcolor="#CCCCCC" align="right"><b>Invoice Amount</b></td>
		  <td width="100" bgcolor="#CCCCCC" align="right"><b>Credit Amount</b></td>
        </tr>
       <%if isarray(recarray) then%>
		<%
		TotInvAmount = 0.00
		TotCrdAmount = 0.00
		%>
		<%for i=0 to UBound(recarray,2)%>
		<tr>
          <td><%=FormatDateInDDMMYYYY(recarray(0,i))%></td>
		  <td><%=recarray(1,i)%></td>
		  <td><%=recarray(8,i)%></td>
          <td><%=recarray(2,i)%></td>
		  <td><%=recarray(3,i)%></td>
		  <td><%=recarray(4,i)%></td>
		  <td><%=FormatDateInDDMMYYYY(recarray(5,i))%></td>
		  <td align="right"><%=formatnumber(recarray(6,i),2)%></td>
		  <td align="right"><%=formatnumber(recarray(7,i),2)%></td>
         </tr>
          <%
		  
		  If recarray(6,i)<> "" then 
				TotInvAmount = TotInvAmount + formatnumber(recarray(6,i),2)
		  End if
		  
		  If recarray(7,i)<> "" then 
				TotCrdAmount = TotCrdAmount + formatnumber(recarray(7,i),2)
		  End if
		  		  		  
          %>
          <%next%>
          <tr>
       		<td colspan=7 align="right"><b>&nbsp;&nbsp;Grand Total</b></td>
          	<td align="right"><b><%=formatnumber(TotInvAmount,2)%></b></td>
          	<td align="right"><b><%=formatnumber(TotCrdAmount,2)%></b></td>
          </tr>
          <%else%>
          	<tr><td colspan="9" bgcolor="#CCCCCC"><b>0 - No records matched...</b></td></tr>
		  <%end if%>
      </table>
    </td>
  </tr>
</table>
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;<br><br><br><br><br><br>
</center>
</div>
</body>
<%if IsArray(recarray) then erase recarray %>
</html>


