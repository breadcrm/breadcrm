<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim intI
Dim intJ
Dim intN
Dim intRow
Dim intF
Dim intTotal
intN = 0
intTotal = 0

deldate= request.form("deldate")
van= request.form("van")
dtype=Request.Form("deltype")
if deldate= "" or van = ""  or dtype = "" then response.redirect "daily_report.asp"

set object = Server.CreateObject("bakery.daily")
object.SetEnvironment(strconnection)
set DisplayDailyVanDriverSheet= object.DisplayDailyVanDriverSheet(deldate,van,dtype)
vRecArray = DisplayDailyVanDriverSheet("Vansheet")
set DisplayDailyFacilityOrderSheet= nothing
set object = nothing

intF = 1
If isArray(vRecArray) Then
	If ubound(vRecArray, 2) = 0 Then
		intF = 1
	Else
		intF = Round((ubound(vRecArray, 2)/PrintPgSize) + 0.49)
	End If
End If
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
br.page { page-break-before: always; }
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<p>&nbsp;</p>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%">
      <p align="center"><b><font size="3">Van Driver Sheet - <%=dtype%><br>
      &nbsp;</font></b>
      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
            To be packed on <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%>
            <p>Number of pages for this report: <%= intF %></p>
            <p>&nbsp;</p>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table><%
if isarray(vRecArray) then
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><p><b>VAN <%=van%></b></p></td>
	</tr>
</table><%
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr>
          <td width="11%" height="20"><b>Product code</b></td>
          <td width="56%" height="20"><b>Product name</b></td>
          <td width="8%" height="20"><b>Quantity</b></td>
        </tr><%
		end if
        For intJ = 1 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
		%>
        <tr>
          <td width="11%" height="20"><%=vRecArray(2,intN)%></td>
          <td width="56%" height="20"><%=vRecArray(3,intN)%></td>
          <td width="8%" height="20"><%=vRecArray(5,intN)%></td>
        </tr><%
				intTotal = intTotal + vRecArray(5,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
%>
        <tr>
          <td width="67%" height="20" colspan="2" align="right"><b>Total&nbsp; </b></td>
          <td width="8%" height="20"><b><%=intTotal%>&nbsp;</b></td>
        </tr><%
			End if
			intN = intN + 1
		Next
%>
</table><%
		if intI <> intF Then
%>
<br class="page" /><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p>&nbsp;</p>
</body>
</html>