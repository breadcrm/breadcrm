<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Dim intI
Dim intJ
Dim intN
Dim intRow
Dim intF
Dim intTotal
intN = 0
intTotal = 0
Response.Buffer
deldate= request.form("deldate")
strfacility= request.form("facility")
dtype=Request.Form("deltype")
if deldate= "" or strfacility = "" or dtype = "" then response.redirect "daily_report.asp"
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title></title>
<style type="text/css">
<!--
@media print { DIV.PAGEBREAK {page-break-before: always; height:1px}}
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<p>&nbsp;</p>
<p align="center"><b><font size="3"> Ordering Sheet - <%if dtype="All" then%>(Morning, Noon, Evening)<%else%>(<%=dtype%>)<%end if%></font></b></p>
<%
stop
if strfacility="0" then
	strfacility="11,12"
end if
arrayFacility=split(strfacility,",")
if isarray(arrayFacility) then 
	for i = 0 to ubound(arrayFacility)
	set object = Server.CreateObject("bakery.daily")
	object.SetEnvironment(strconnection)
	'set DisplayDailyFacilityOrderSheet= object.DisplayDailyReportFacilityOrderSheetNew(deldate,trim(arrayFacility(i)),dtype)
	set DisplayDailyFacilityOrderSheet= object.DisplayReportViewDailyReportFacilityOrderSheet(deldate,trim(arrayFacility(i)),dtype)
	vRecArray = DisplayDailyFacilityOrderSheet("Ordersheet")
	Facility = DisplayDailyFacilityOrderSheet("Facility")
	set DisplayDailyFacilityOrderSheet= nothing
	set object = nothing
	intF = 1
	If isArray(vRecArray) Then
		If ubound(vRecArray, 2) = 0 Then
			intF = 1
		Else
			intF = Round((ubound(vRecArray, 2)/PrintPgSize) + 0.49)
		End If
	End If
%>
<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td>
       <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td>
			<b>Facility: <%=Facility(0,0)%></b><br>
			<b>To the attention of: <%=Facility(1,0)%></b><br>
			<!--<b>By fax number: <%=Facility(2,0)%></b><br><br>-->
			Date of report: <%=day(now()) & "/" & month(now()) & "/" & year(now())%><br>
			To be produced and ready to be delivered on <%=deldate%> <%if dtype="Morning" then%> before 05:00 AM<%end if%><br><br>
			Number of pages for this report: <%= intF %><br><br>
          </td>
        </tr>
        </table>
    </td>
  </tr>
</table>

<%
intN=0
intTotal=0
if isarray(vRecArray) then 
	for intI = 1 to intF
%>
<table align="center" border="2" width="90%" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt; border-collapse:collapse" bordercolor="#000000"><%
		if intI = 1 then
		%>
        <tr height="18">
          <td width="9%"><nobr><b>Product Code</b></nobr></td>
          <td width="53%"><nobr><b>Product Name</b></nobr></td>
		  <td width="6%"><b>Pre Bake Size</b></td>
          <td width="6%"><b>Post Bake Size</b></td>
		  
          
		  <%
		  if vRecArray(8,intI) = 18 then
		  %>
			  <td width="5%"><b>Tray Qty</b></td>
			  <td width="5%"><b>Extra Units</b></td>
			  <td width="5%"><b>Nos. in Tray</b></td>
			  <td width="5%"><b>Order Qty</b></td>
		 <%
		  else
		  %>
		  <td width="5%"><b>Order Qty</b></td>
		 
		  <%
		  end if
		  %>
		  
		  <td width="8%"><nobr><b>Qty Done</b></nobr></td>
        </tr><%
		end if
		
        For intJ = 0 To PrintPgSize
			if intN <= ubound(vRecArray, 2) Then
		%>
        <tr height="18">
          <td width="9%"><nobr><%=vRecArray(0,intN)%></nobr></td>
          <td width="55%"><%=vRecArray(1,intN)%></td>
          
          <td width="6%"><%=vRecArray(9,intN)%></td>
		  <td width="6%"><%=vRecArray(2,intN)%></td>
		  <%
		  if vRecArray(8,intN) = 18 then
		  %>
			  <td width="5%">&nbsp<%=vRecArray(3,intN) \ vRecArray(7,intN)%></td>
			  <td width="5%">&nbsp<%=vRecArray(3,intN) mod vRecArray(7,intN)%></td>
			  <td width="5%">&nbsp<%=vRecArray(7,intN)%></td>
			  <td width="5%"><b><%=vRecArray(3,intN)%></b></td>
		  <%
		  else
		  %>
		  <td width="5%"><%=vRecArray(3,intN)%></td>
		  
		  
		  <%
		  end if
		  %>
		  
		  <td width="8%">&nbsp;</td>
        </tr><%
				intTotal = intTotal + vRecArray(3,intN)
			End if
			if intN = ubound(vRecArray, 2) Then
		%>
        <tr height="18">
          <td colspan="4" align="right"><b>Total&nbsp; </b></td>
          <td><b><%=intTotal%>&nbsp;</b></td>
		  <td>&nbsp;</td>
        </tr><%
			End if
			intN = intN + 1
		Next
%>
</table><%
		if intI <> intF Then
%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV><%
		End if
	next
else
%>
<table align="center" border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<tr>
	<td colspan="2"><b>Sorry no items found</b></td>
	</tr>
</table><%
end if
%>
<p><hr></p>
<% if i<ubound(arrayFacility) then%>
<DIV CLASS="PAGEBREAK">&nbsp;</DIV>
<%
end if
vRecArray=""
Response.Flush()
next
end if
%>
<p>&nbsp;</p>
</body>
</html>