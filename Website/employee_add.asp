<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayFacility()	
vFacarray =  detail("Facility") 
set detail = Nothing
set object = Nothing

vEno= request.form("eno")
if vEno <> "" then
	set object = Server.CreateObject("bakery.employee")	
	object.SetEnvironment(strconnection)
	set DisplayEmployeeDetail= object.DisplayEmployeeDetail(vEno)
	vRecArray = DisplayEmployeeDetail("EmployeeDetail")
	vRecSalaryArray = DisplayEmployeeDetail("EmployeeSalaryDetail")
	set DisplayEmployeeDetail= nothing
	set object = nothing
end if
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<script LANGUAGE="javascript" SRC="includes/Script.js"></script>

<script Language="JavaScript">
<!--
function validate(frm,type){
	if(confirm("Would you like to "+ type +" this employee?")){

		if (type=='delete'){
			frm.type.value='Delete';		
			frm.submit();}
		else{
			FrontPage_Form1_Validator(frm)	
		}	
		
	}
}	
//-->
</Script>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
 <script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{

  if (theForm.Usertype.selectedIndex < 0)
  {
    alert("Please select one of the \"System User\" options.");
    theForm.Usertype.focus();
    return;
  }

  if (theForm.Usertype.selectedIndex == 0)
  {
    alert("The first \"System User\" option is not a valid selection.  Please choose one of the other options.");
    theForm.Usertype.focus();
    return;
  }

  if (theForm.jobtype.selectedIndex < 0)
  {
    alert("Please select one of the \"Type\" options.");
    theForm.jobtype.focus();
    return;
  }

  if (theForm.jobtype.selectedIndex == 0)
  {
    alert("The first \"Type\" option is not a valid selection.  Please choose one of the other options.");
    theForm.jobtype.focus();
    return;
  }

  if (theForm.Location.selectedIndex < 0)
  {
    alert("Please select one of the \"Location\" options.");
    theForm.Location.focus();
    return;
  }

  if (theForm.Location.selectedIndex == 0)
  {
    alert("The first \"Location\" option is not a valid selection.  Please choose one of the other options.");
    theForm.Location.focus();
    return;
  }

  if (theForm.position.value == "")
  {
    alert("Please enter a value for the \"Position\" field.");
    theForm.position.focus();
    return;
  }

  if (theForm.position.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Position\" field.");
    theForm.position.focus();
    return;
  }

  if (theForm.firstname.value == "")
  {
    alert("Please enter a value for the \"First name\" field.");
    theForm.firstname.focus();
    return;
  }

  if (theForm.firstname.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"First name\" field.");
    theForm.firstname.focus();
    return;
  }

  if (theForm.lastname.value == "")
  {
    alert("Please enter a value for the \"Last name\" field.");
    theForm.lastname.focus();
    return;
  }

  if (theForm.lastname.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Last name\" field.");
    theForm.lastname.focus();
    return;
  }

  if (theForm.address1.value == "")
  {
    alert("Please enter a value for the \"Addres\" field.");
    theForm.address1.focus();
    return;
  }

  if (theForm.address1.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Addres\" field.");
    theForm.address1.focus();
    return;
  }

  if (theForm.town.value == "")
  {
    alert("Please enter a value for the \"Town\" field.");
    theForm.town.focus();
    return;
  }

  if (theForm.town.value.length > 50)
  {
    alert("Please enter at most 50 characters in the \"Town\" field.");
    theForm.town.focus();
    return;
  }

  if (theForm.postcode.value == "")
  {
    alert("Please enter a value for the \"Postcode\" field.");
    theForm.postcode.focus();
    return;
  }

  if (theForm.postcode.value.length > 20)
  {
    alert("Please enter at most 20 characters in the \"Postcode\" field.");
    theForm.postcode.focus();
    return;
  }
  
  if (theForm.telno.value == "")
  {
    alert("Please enter a value for the \"Telephone\" field.");
    theForm.telno.focus();
    return;
  }

  if (theForm.telno.value.length > 20)
  {
    alert("Please enter at most 20 characters in the \"Telephone\" field.");
    theForm.telno.focus();
    return;
  }
  
  

  if (theForm.email.value != ""){  
		if(CheckEmail(theForm.email.value)=="true")
		{ 
			alert("Please enter a valid e-mail address in the \"email\" field.");	
			theForm.email.focus();
			return;
		}	

		if (theForm.email.value.length > 100)
		{
		  alert("Please enter at most 100 characters in the \"email\" field.");
		  theForm.email.focus();
		  return;
		}
  }


  if (theForm.nino.value == "")
  {
    alert("Please enter a value for the \"NI Number\" field.");
    theForm.nino.focus();
    return;
  }

  if (theForm.nino.value.length > 10)
  {
    alert("Please enter at most 10 characters in the \"NI Number\" field.");
    theForm.nino.focus();
    return;
  }
  
    if (theForm.dob.value == "")
  {
    alert("Please enter a value for the \"Date of Birth\" field.");
    theForm.dob.focus();
    return;
  }

  if (theForm.dob.value.length > 10)
  {
    alert("Please enter at most 8 characters in the \"Date of Birth\" field.");
    theForm.dob.focus();
    return;
  }
  

/*
  if (theForm.salary.value == "")
  {
    alert("Please enter a value for the \"Salary\" field.");
    theForm.salary.focus();
    return;
  }

  
  if (theForm.bonus.value == "")
  {
    alert("Please enter a value for the \"Bonus\" field.");
    theForm.bonus.focus();
    return;
  }

  if (theForm.bonus.value.length > 10)
  {
    alert("Please enter at most 10 characters in the \"Bonus\" field.");
    theForm.bonus.focus();
    return;
  }
  
  var checkOK = "0123456789-.,";
  var checkStr = theForm.salary.value;
  var allValid = true;
  var decPoints = 0;
  var allNum = "";
  for (i = 0;  i < checkStr.length;  i++)
  {
    ch = checkStr.charAt(i);
    for (j = 0;  j < checkOK.length;  j++)
      if (ch == checkOK.charAt(j))
        break;
    if (j == checkOK.length)
    {
      allValid = false;
      break;
    }
    if (ch == ".")
    {
      allNum += ".";
      decPoints++;
    }
    else if (ch != ",")
      allNum += ch;
  }
  if (!allValid)
  {
    alert("Please enter only digit characters in the \"Salary\" field.");
    theForm.salary.focus();
    return;
  }

  if (decPoints > 1)
  {
    alert("Please enter a valid number in the \"Salary\" field.");
    theForm.salary.focus();
    return;
  }
  //return (true);
  
*/
  if (theForm.sdate.value == "")
  {
    alert("Please enter a value for the \"Start Date\" field.");
    theForm.sdate.focus();
    return;
  }

  if (theForm.sdate.value.length > 10)
  {
    alert("Please enter at most 8 characters in the \"Start Date\" field.");
    theForm.sdate.focus();
    return;
  }
  
  /*if (theForm.username.value == "")
  {
    alert("Please enter a value for the \"User name\" field.");
    theForm.username.focus();
    return;
  }  

  if (theForm.password.value == "")
  {
    alert("Please enter a value for the \"Password\" field.");
    theForm.password.focus();
    return;
  } */ 
  
  //return (true);
  theForm.submit();
}
//--></script>
     <tr>
	<form method="POST" action="employee_finish.asp" onSubmit="return FrontPage_Form1_Validator(this)" name="FrontPage_Form1">
    
    <input type="hidden" name="eno" value="<%=vEno%>">
    <td width="100%"><b><font size="3"><%if vEno = "" then%>Create new<%else%>Edit<%end if%> employee<br>
      &nbsp;</font></b>
      
      <table border="0" width="100%" cellspacing="0" cellpadding="2"><%
				if Request.QueryString("status")  = "error" Then%>	
				<tr>
          <td valign="top"><font size="3" color="#FF0000"><B>Invalid Entry</B></font><Td>
        <tr><%
				End if%>
        <tr>
          <td valign="top"><table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt" width="100%">
        <tr>
          <td><b>User Type<font color="#FF0000"> *</font>&nbsp;</b></td>
          <td>         
          <select size="1" name="Usertype" style="font-family: Verdana; font-size: 8pt">
              <option>Select</option>
             
			 <%if isarray(vrecarray) then%>
              <option value="S" <%if vrecarray(1,0)= "S" then response.write " Selected"%>>Super Administrator</option>
              <option value="A" <%if vrecarray(1,0)= "A" then response.write " Selected"%>>Administrator</option>
              <option value="U" <%if vrecarray(1,0)= "U" then response.write " Selected"%>>System User</option>
              <option value="N" <%if vrecarray(1,0)= "N" then response.write " Selected"%>>Basic View Only</option>
			  <%else%>
              <option value="S">Super Administrator</option>
              <option value="A">Administrator</option>
              <option value="U">System User</option>
              <option value="N">Basic View Only</option>
   			<%end if%>
   			
            </select></td>
        </tr>
        <tr>
          <td><b>Type<font color="#FF0000"> *</font>&nbsp;</b></td>
          <td>
         
          <select size="1" name="jobtype" style="font-family: Verdana; font-size: 8pt">
              <option>Select</option>
			<%'if isarray(vrecarray) then%>
<!--              <option value="Office employees" <%if vrecarray(2,0)= "Office employees" then response.write "Selected"%>>Office employees</option>
              <option value="Drivers" <%if vrecarray(2,0)= "Drivers" then response.write "Selected"%>>Drivers</option>
              <option value="Stanmore bakery staff" <%if vrecarray(2,0)= "Stanmore bakery staff" then response.write "Selected"%>>Stanmore bakery staff</option>
              <option value="Park royal bakery staff" <%if vrecarray(2,0)= "Park royal bakery staff" then response.write "Selected"%>>Park royal bakery staff</option>
              <option value="BMG bakery staff" <%if vrecarray(2,0)= "BMG bakery staff" then response.write "Selected"%>>BMG bakery staff</option>
              <option value="Baker &amp; Spice" <%if vrecarray(2,0)= "Baker & Spice" then response.write "Selected"%>>Baker &amp; Spice</option>
-->			  <%'else%>
              <option value="Office employees">Office employees</option>
              <option value="Bakery staff">Bakery staff</option>
              
              <%'end if%>
            </select></td>
        </tr>
        <tr>
          <td><b>Location<font color="#FF0000"> *</font>&nbsp;</b></td>
          <td>
          <select size="1" name="Location" style="font-family: Verdana; font-size: 8pt">
              <option>Select</option>
				  <%'for i = 0 to ubound(vfacarray,2)%>  				
                  <%'if vfacarray(0,i) = "15" or vfacarray(0,i) = "18" or vfacarray(0,i) = "90" then%>
<!--                  <option value="<%=vfacarray(0,i)%>" <%if isarray(vrecarray) then%><%if vrecarray(3,0) = vfacarray(0,i) then response.write " Selected"%><%end if%>><%=vfacarray(1,i)%></option>
-->                  <%'End if%>
                  <%'next%>  
                  <option value="1">Office1</option>
                  <option value="2">Office2</option>
                  <option value="3">Office3</option>                 
            </select></td>
        </tr>
        <tr>
          <td><b>Position<font color="#FF0000"> *</font>&nbsp;</b></td>
          <td>
         
          <input type="text" name="position" size="42" style="font-family: Verdana; font-size: 8pt" maxlength="50" value="<%if isarray(vrecarray) then response.write vrecarray(4,0)%>"></td>
        </tr>
        <tr>
          <td>First name<font color="#FF0000"> *</font></td>
          <td>
         
          <input type="text" name="firstname" size="42" style="font-family: Verdana; font-size: 8pt" maxlength="50" value="<%if isarray(vrecarray) then response.write vrecarray(5,0)%>"></td>
        </tr>
        <tr>
          <td>Last name<font color="#FF0000"> *</font></td>
          <td>
         
          <input type="text" name="lastname" size="42" style="font-family: Verdana; font-size: 8pt" maxlength="50" value="<%if isarray(vrecarray) then response.write vrecarray(6,0)%>"></td>
        </tr>
        <tr>
          <td>Nick name</td>
          <td>
          <input type="text" name="nickname" size="42" style="font-family: Verdana; font-size: 8pt" maxlength="50" value="<%if isarray(vrecarray) then response.write vrecarray(7,0)%>"></td>
        </tr>
        <tr>
          <td>Address<font color="#FF0000"> *</font></td>
          <td>
         
          <input type="text" name="address1" size="42" style="font-family: Verdana; font-size: 8pt" maxlength="50" value="<%if isarray(vrecarray) then response.write vrecarray(8,0)%>"></td>
        </tr>
        <tr>
          <td></td>
          <td>
          <input type="text" name="address2" size="42" style="font-family: Verdana; font-size: 8pt" maxlength="50" value="<%if isarray(vrecarray) then response.write vrecarray(9,0)%>"></td>
        </tr>
        <tr>
          <td>Town<font color="#FF0000"> *</font></td>
          <td>
      
          <input type="text" name="town" size="42" style="font-family: Verdana; font-size: 8pt" maxlength="50" value="<%if isarray(vrecarray) then response.write vrecarray(10,0)%>"></td>
        </tr>
        <tr>
          <td>Postcode<font color="#FF0000"> *</font></td>
          <td>
  
          <input type="text" name="postcode" size="42" style="font-family: Verdana; font-size: 8pt" maxlength="20" value="<%if isarray(vrecarray) then response.write vrecarray(11,0)%>"></td>
        </tr>
        <tr>
          <td>Telephone<font color="#FF0000"> *</font></td>
          <td>
          <input type="text" name="telno" size="42" style="font-family: Verdana; font-size: 8pt" maxlength="20" value="<%if isarray(vrecarray) then response.write vrecarray(12,0)%>"></td>
        </tr>
        <tr>
          <td>Mobile</td>
          <td>
          <input type="text" name="mobile" size="42" style="font-family: Verdana; font-size: 8pt" maxlength="20" value="<%if isarray(vrecarray) then response.write vrecarray(13,0)%>"></td>
        </tr>
        <tr>
          <td>Fax</td>
          <td>
          <input type="text" name="fax" size="42" style="font-family: Verdana; font-size: 8pt" maxlength="20" value="<%if isarray(vrecarray) then response.write vrecarray(14,0)%>"></td>
        </tr>
        <tr>
          <td>Email</td>
          <td>
          <input type="text" name="email" size="42" style="font-family: Verdana; font-size: 8pt" maxlength="100" value="<%if isarray(vrecarray) then response.write vrecarray(15,0)%>"></td>
        </tr>
        <tr>
          <td>Office Extension</td>
          <td>
          <input type="text" name="offext" size="42" style="font-family: Verdana; font-size: 8pt" maxlength="50" value="<%if isarray(vrecarray) then response.write vrecarray(16,0)%>"></td>
        </tr>
        <tr>
          <td>NI Number<font color="#FF0000"> *</font></td>
          <td>
     
          <input type="text" name="nino" size="21" style="font-family: Verdana; font-size: 8pt" maxlength="10" value="<%if isarray(vrecarray) then response.write vrecarray(17,0)%>"></td>
        </tr>
        <tr>
          <td>Date of birth&nbsp;(dd/mm/yyyy)<font color="#FF0000"> *</font></td>
          <td>
    
          <input type="text" name="dob" size="21" style="font-family: Verdana; font-size: 8pt"  maxlength="10" value="<%if isarray(vrecarray) then response.write Right("00" & day(vrecarray(18,0)),2) & "/" & Right("00" &  month(vrecarray(18,0)),2) & "/" & Year(vrecarray(18,0))%>"></td>
        </tr>
        <tr>
          <td>Salary (�s)</td>
          <td>
         
          <input type="text" name="salary" size="21" style="font-family: Verdana; font-size: 8pt" maxlength="10" value="<%if isarray(vrecarray) then response.write vrecarray(19,0)%>"></td>
        </tr>
        <tr>
          <td>Monthly Bonus (�s)</td>
          <td>
         
          <input type="text" name="bonus" size="21" style="font-family: Verdana; font-size: 8pt" maxlength="10" value="<%if isarray(vrecarray) then response.write vrecarray(24,0)%>"></td>
        </tr>
        <tr>
          <td>System Username</td>
          <td>
          <input type="text" name="username" size="21" style="font-family: Verdana; font-size: 8pt" maxlength="10" value="<%if isarray(vrecarray) then response.write vrecarray(20,0)%>"></td>
        </tr>
        <tr>
          <td>Password</td>
          <td>
          <input type="password" name="password" size="21" style="font-family: Verdana; font-size: 8pt" maxlength="10" value="<%if isarray(vrecarray) then response.write vrecarray(21,0)%>"></td>
        </tr>
        <tr>
          <td>Start Date (dd/mm/yyyy)<font color="#FF0000"> *</font></td>
          <td>
    
          <input type="text" name="sdate" size="21" style="font-family: Verdana; font-size: 8pt" maxlength="10"  value="<%if isarray(vrecarray) then response.write Right("00" & day(vrecarray(22,0)),2) & "/" & Right("00" &  month(vrecarray(22,0)),2) & "/" & Year(vrecarray(22,0)) %>"></td>
        </tr>
        <!--
		<%if vEno <> "" then%>
        <tr>
          <td>Status</td>
          <td>        
          <select size="1" name="Status" style="font-family: Verdana; font-size: 8pt">
    	    <%if isarray(vrecarray) then%>
	          <option value="A" <%if vrecarray(20,0)= "A" then response.write Selected%>>Active</option>
              <option value="D" <%if vrecarray(20,0)= "A" then response.write Selected%>>Deleted</option>
			<%else%>
              <option value="A">Active</option>
              <option value="D">Deleted</option>
	        <%end if%>
            </select></td>
        </tr>  
        <%end if%>
        -->
        <tr>
          <td></td>
          <td>
			<input type = "hidden" name="type" value="">
	        <%if vEno = "" then%>
  			<input type="button" value="Add employee" onClick="FrontPage_Form1_Validator(this.form);"  name="Add" style="font-family: Verdana; font-size: 8pt"> 			
  			<%else%>  		
  			<input type="button" value="Edit employee" onClick="validate(this.form,'edit');" name="Edit" style="font-family: Verdana; font-size: 8pt"> 			 			
  			<input type="button" value="Delete employee" onClick="validate(this.form,'delete');" name="Delete" style="font-family: Verdana; font-size: 8pt"> 			 			
			<%end if%></td>
        </tr>
      </table></td>
        </tr>
      </table>
      
      

    </td>
	</form>
  </tr> 
</table>


  </center>
</div>


</body>

</html>