<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<SCRIPT language=JavaScript type=text/javascript>

    function ExportToExcelFile() {
       
        document.location = "ExportToExcelFile_specialprice.asp"
    }

</SCRIPT>

</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim recarray
dim retcol
dim totpages
dim vSessionpage
dim i

totpages = Request.Form("pagecount")
select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(totpages) then	
			session("Currentpage")  = session("Currentpage") + 1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage") - 1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select
vSessionpage = session("Currentpage")

set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.SpecialPriceList(vSessionPage,0)
totpages = retcol("pagecount")
recarray = retcol("SpecialPrices")
%>
<div align="center">
  <center>
      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="4" >&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>        
        <tr>
          <td colspan="4"><b><font size="3">Report - Special prices</font><font size="2"><br>
            </font></b></td>
            <%
          if isarray(recarray) then%>
          <td><font size="2"><b>Page <%=vsessionpage%> of <%=totpages%></b></font></td><%
          else%>
          <td>&nbsp;</td><%
          end if%>
        </tr>
        <tr>
          <td bgcolor="#CCCCCC"><b>Customer&nbsp;No</b></td>
          <td bgcolor="#CCCCCC"><b>Customer&nbsp; Name</b></td>
          <td bgcolor="#CCCCCC"><b>Products</b></td>
          <td bgcolor="#CCCCCC"><b>Default price</b></td>
          <td bgcolor="#CCCCCC"><b>Regular Price</b></td>
          <td bgcolor="#CCCCCC"><b>Special Price</b></td>
          <td bgcolor="#CCCCCC"><b>Percentage discount</b></td>
        </tr><%
        if isarray(recarray) then
          for i=0 to ubound(recarray,2)%>
          <%if recarray(5,i)<>recarray(6,i) then%>
          <tr>
          <td><%=recarray(0,i)%></td>
          <td><%=recarray(2,i)%></td>
          <td><%=recarray(1,i)%></td>
          <td><%=formatnumber(recarray(4,i),2)%></td>
          <td><%=formatnumber(recarray(5,i),2)%></td>
          <td>� <%=formatnumber(recarray(6,i),2)%></td>
          <td><%=formatnumber(recarray(7,i),2)%></td>
          </tr>
          <%end if%>          
          <%
          next
        else%>
        <tr>
          <td colspan="6"><b>No Records found...</b></td>
        </tr><%
        end if%>
      </table>
  </center>
</div>
<br>
<div align="center">
  <table width="720" border="0" cellspacing="1" cellpadding="2" align="center">
  <tr> 
  <% if clng(vSessionpage) <> 1 then %>
  <td>
    <form name="frmFirstPage" action="report_specialprice.asp" method="post">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="hidden" name="Direction" value="First">
    <input type="submit" name="submit" value="First Page">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) < clng(totpages) then %>
  <td>
    <form name="frmNextPage" action="report_specialprice.asp" method="post">
    <input type="hidden" name="Direction" value="Next">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Next Page">
    </form>
  </td>
  <%end if%>
  <% if clng(vSessionpage) > 1 then %>
  <td>
    <form name="frmPreviousPage" action="report_specialprice.asp" method="post">
    <input type="hidden" name="Direction" value="Previous">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Previous Page">
    </form>
  </td>
  <%end if%>
  <%if clng(vSessionPage) <> clng(totpages) and clng(totpages) > 0 then %>
  <td>
    <form name="frmLastPage" action="report_specialprice.asp" method="post">
    <input type="hidden" name="Direction" value="Last">
    <input type="hidden" name="pagecount" value="<%=totpages%>">
    <input type="submit" name="submit" value="Last Page">
    </form>
  </td>
  <% end if%>
  </tr>
  </table>
</div>
</body>
</html>