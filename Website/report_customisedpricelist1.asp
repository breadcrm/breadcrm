<%@ Language=VBScript%>
<%option Explicit%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">

&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim recarray1, recarray2
dim retcol
dim totpages
dim vSessionpage
dim i
dim ptno,ptname, totrecs

ptno=Request.Form("txtptno")
ptname=Request.Form("txtptname")
totpages = Request.Form("pagecount")
select case Request.Form("Direction")
	case ""
		session("Currentpage")  = 1
	case "Next"
		if clng(session("Currentpage")) < clng(totpages) then	
			session("Currentpage")  = session("Currentpage") + 1
		End if 
	case "Previous"
		if session("Currentpage") > 1 then
			session("Currentpage")  = session("Currentpage") - 1
		end if
	case "First"
		session("Currentpage") = 1
	case "Last"
		session("Currentpage")  = Request.Form("pagecount")
end select
vSessionpage = session("Currentpage")

set objBakery = server.CreateObject("Bakery.MgmReports")
objBakery.SetEnvironment(strconnection)
if ptno = "" then
  'Response.Write "ptno = " & ptno & "<br>"
  set retcol = objBakery.CustomPriceList(0,vSessionPage,1)
  recarray1 = retcol("CustomPrices1")
  ptname="name"
else
  'Response.Write "ptno = " & ptno & "<br>"
  set retcol = objBakery.CustomPriceList(ptno,vSessionPage,0)
  recarray1 = retcol("CustomPrices1")
  recarray2 = retcol("CustomPrices2")
  totpages = retcol("pagecount")
end if
'Response.Write "totpages = " & totpages & "<br>"
%>
<div align="center">
  <center>
      <table border="0" width="90%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
        <tr>
          <td colspan="2"><b><font size="2">&lt;<%=ptname%>&gt; Price List</font></b></td><%
          if isarray(recarray2) then%>
          <td><font size="2"><b>Page 1 of 1</b></font></td><%
          else%>
          <td>&nbsp;</td><%
          end if%>
        </tr>
        <tr>
          <td bgcolor="#CCCCCC"><b>Product Code</b></td>
          <td bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td bgcolor="#CCCCCC"><b>Price</b></td>
        </tr><%
        if isarray(recarray2) then
          for i=0 to ubound(recarray2,2)%>
          <tr>
          <td><%=recarray2(0,i)%></td>
          <td><%=recarray2(1,i)%></td>
          <td>� <%if isnull(recarray2(2,i)) then Response.Write "0.00" else Response.Write formatnumber(recarray2(2,i))%></td>
          </tr><%
          next
        elseif instr(1,lcase(Request.ServerVariables("HTTP_REFERER")),"report_customisedpricelist.asp") >  0 then%>
        <tr>
          <td colspan="3"><b>No Records found...</b></td>
        </tr><%
        end if%>
      </table>
  </center>
</div>
</body>
</html>