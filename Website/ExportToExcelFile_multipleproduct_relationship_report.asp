<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
dim objBakery
dim recarray
dim retcol

set objBakery = server.CreateObject("Bakery.Reports")
objBakery.SetEnvironment(strconnection)
set retcol = objBakery.Display_ReportMultiProductRelationship()
recarray = retcol("MultipleProductList")
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=MultipleProduct_Relationship.xls" 
	
	
%>
	<table border="0" width="100%" cellspacing="0" cellpadding="5" style="font-family: Verdana; font-size: 8pt">

 <tr>
    <td width="100%"><b><font size="3">Multiple Product Relationships Report<br>
      &nbsp;</font></b>
	  
      	<table border="1" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
         <tr>
          <td colspan="7" align="left" height="30">&nbsp;<b><a href="javascript:ExportToExcelFile()" ><font color="#0000FF">Export to Excel File</font></a></b></td>
        </tr>
		<tr>
          <td colspan="7"><b>Multiple Product Relationships Report</b></td>
        </tr>
        <tr>
          <td width="110" bgcolor="#CCCCCC"><b>Group</b></td>
          <td width="110" bgcolor="#CCCCCC"><b>Multiple PNo</b></td>
		  <td width="200" bgcolor="#CCCCCC"><b>Product Name</b></td>
          <td width="110" bgcolor="#CCCCCC"><b>Sub Product No</b></td>
          <td width="200" bgcolor="#CCCCCC"><b>Sub Product Name</b></td>
		  <td width="110" bgcolor="#CCCCCC"><b>Qty</b></td>
        </tr>
       <%if isarray(recarray) then%>
		
		<%for i=0 to UBound(recarray,2)%>
		<tr>
          <td><%=recarray(0,i)%></td>
          <td><%=recarray(1,i)%></td>
		  <td><%=recarray(2,i)%></td>
          <td><%=recarray(3,i)%></td> 
		  <td><%=recarray(4,i)%></td>
		  <td align="right"><%=recarray(5,i)%></td>
         </tr>
         
          <%next
         
          else%>
          	<tr><td colspan="7"  bgcolor="#CCCCCC"><b>0 - No records matched...</b></td></tr>
		  <%end if%>
      </table>
    </td>
  </tr>
</table>

