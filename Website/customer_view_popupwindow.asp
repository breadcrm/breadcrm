<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%
dim vCustNo,obj,ObjCustomer,arCustomer
vCustNo=request("CustNo")
Set obj = CreateObject("Bakery.Customer")
Set ObjCustomer = obj.Display_CustomerDetail(vCustNo)
arCustomer = ObjCustomer("Customer")
Set ObjCustomer = Nothing
%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%'<!--#INCLUDE FILE="includes/head.inc" -->%>
<title>Bread Factory</title>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
		  <tr>
		    <td width="100%"><b><font size="3">View customer
				<br> 
		      &nbsp;<br>
		     
		     
               
          		<table border="0" width="100%" bgcolor="#999999" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="2">
		        <%
				strResellerID=arCustomer(34,0)
				if (arCustomer(34,0)<>"" and arCustomer(34,0)<>"0") then
				%>
				<tr height="20" bgcolor="#F3F3F3">
		          <td><font size="2">Reseller Name:</font>&nbsp;</td>
		          <td><font size="2"><%=arCustomer(35,0)%></font>&nbsp;</td>
		      	</tr>
				<%end if%>
				<tr height="20" bgcolor="#F3F3F3">
		          <td width="200"><b><font size="2">Code</font>:&nbsp;</b></td>
		          <td width="450"><b><font size="2"><%=arCustomer(0,0)%></font>&nbsp;</b></td>
		      	</tr>
		        <tr height="20"  bgcolor="#F3F3F3">
		          <td><b><font size="2">Name:</font></b></td>
		          <td><b><font size="2"><%=arCustomer(1,0)%></font>&nbsp;</b></td>
		        </tr>
		        <tr height="20"  bgcolor="#F3F3F3">
		          <td><font size="2">Nick Name:</font></td>
		          <td><b><font size="2"><%=arCustomer(27,0)%></font>&nbsp;</b></td>
		        </tr>
		        <tr height="20"  bgcolor="#F3F3F3">
		          <td><b>Old Code:</b></td>
		          <td><%=arCustomer(2,0)%></td>
		        </tr>
		        
		        <tr height="20"  bgcolor="#F3F3F3">
		          <td>Address:</td>
		          <td><%=arCustomer(3,0)%></td>
		        </tr>
		        <tr height="20"  bgcolor="#F3F3F3">
		          <td></td>
		          <td><%=arCustomer(4,0)%></td>
		        </tr>
		        <tr height="20"  bgcolor="#F3F3F3">
		          <td>Town:</td>
		          <td><%=arCustomer(5,0)%></td>
		        </tr>
		        <tr height="20"  bgcolor="#F3F3F3">
		          <td>Postcode:</td>
		          <td><%=arCustomer(6,0)%></td>
		        </tr>
		        <tr height="20"  bgcolor="#F3F3F3">
		          <td>Telephone:</td>
		          <td><%=arCustomer(7,0)%></td>
		        </tr>
		        <tr height="20"  bgcolor="#F3F3F3">
		          <td>Email:</td>
		          <td><%=arCustomerSettings(3,0)%></td>
		        </tr>
		        <tr height="20"  bgcolor="#F3F3F3">
		          <td>Fax:</td>
		          <td><%=arCustomer(28,0)%></td>
		        </tr>
				<tr height="20"  bgcolor="#F3F3F3">
		          <td>Sales Person: </td>
		          <td><%=arCustomer(30,0)%></td>
		        </tr>
				<tr height="20"  bgcolor="#F3F3F3">
		          <td>Account Manager: </td>
		          <td><%=arCustomer(54,0)%></td>
		        </tr>
				<tr height="20"  bgcolor="#F3F3F3">
		          <td>Priceless Invoice/Credit:</td>
		          <td><%if (arCustomer(33,0)="1") then%>Yes<%else%>No<%end if%></td>
		        </tr>
				<tr height="20"  bgcolor="#F3F3F3">
		          <td>Daily Automated Invoice:</td>
		          <td><%if (arCustomer(39,0)="1") then%>Yes<%else%>No<%end if%></td>
		        </tr>
				  <tr height="20"  bgcolor="#F3F3F3">
		          	<td>Daily Automated Credit:</td>
		          	<td><%if (arCustomer(40,0)="1") then%>Yes<%else%>No<%end if%></td>
		          </tr>
		       	  <tr height="20"  bgcolor="#F3F3F3">
		          	<td>Send GE CSV File:</td>
		          	<td><%if (arCustomer(49,0)="1") then%>Yes<%else%>No<%end if%></td>
		          </tr>		          
		          <tr height="20"  bgcolor="#F3F3F3">
		          	<td>Packing Sheet Type:</td>
		          	<td><%if (arCustomer(50,0)="1") then%>Type 1<%else%>Type 2<%end if%></td>
		          </tr>
		          <tr height="20"  bgcolor="#F3F3F3">
		          	<td>Delivery Time:</td>
		          	<td><%=arCustomer(53,0)%></td>
		          </tr>		          
		          <tr height="20"  bgcolor="#F3F3F3">
		          	<td>Include in Central Kitchen Report:</td>
		          	<td><%if (arCustomer(51,0)= True) then%>Yes<%else%>No<%end if%></td>
		          </tr>
		          <tr height="20"  bgcolor="#F3F3F3">
		          	<td>Include in Whole Sale Central Kitchen Report:</td>
		          	<td><%if (arCustomer(52,0)= True) then%>Yes<%else%>No<%end if%></td>
		          </tr>
				  <tr height="20"  bgcolor="#F3F3F3">
		          	<td>Customer Category:</td>
		          	<td><%=arCustomer(56,0)%></td>		
 				  </tr>
				</table>
				</td>
		  </tr>
		</table>
		<p><input type="button" value="Close" onClick="window.close()" style="width:70px"><br><br></p>

</body>
</html>