<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<!--#INCLUDE FILE="includes/CommFun.asp" -->
<%

on error resume next
stop
vUType = request.form("Usertype")
vJobType = request.form("jobtype")
vFNo = request.form("Location")
vDesig = request.form("position")
vFName = request.form("firstname")
vSName = request.form("lastname")
vNickName = request.form("nickname")
vAddressI = request.form("address1")
vAddressII = request.form("address2")
vTown = request.form("town")
vPostCode = request.form("postcode")
vTelNo = request.form("telno")
vMob = request.form("mobile")
vFax = request.form("fax")
vEmail = request.form("email")
vOfficeExt = request.form("offext")
vDOB = request.form("dob")
vNINo = request.form("nino")
vsalary = request.form("salary")
vS_Date = request.form("sdate")
vUName = request.form("username")
vpword = request.form("password")
vBonus = request.form("bonus")
if request.form("Status") <> "" then
	vStatus= request.form("Status")
else
	vStatus= "A"
end if

vEno= request.form("eno")

if (not isdate(vS_Date)) or (not isdate(vDOB)) then response.redirect "employee_add.asp?status=error"
if Trim(vsalary) <> "" and not IsNumeric(vsalary) Then response.redirect "employee_add.asp?status=error"
if Trim(vBonus) <> "" and not IsNumeric(vBonus) Then response.redirect "employee_add.asp?status=error"

if Trim(vsalary) = ""  then vsalary = 0
if Trim(vBonus) = "" then vBonus = 0

set object = Server.CreateObject("bakery.employee")
object.SetEnvironment(strconnection)
if request.form("type")= "Delete" then
	delete = object.DeleteEmployee(vEno)
	LogAction "Employee Deleted", "Login Id:" & vUName , ""
	response.redirect "employee_find.asp"
end if

'For Logging
Dim vUType_desc
select case vUType
	case "S"
		vUType_desc = "Super Admin"
	case "A"
		vUType_desc = "Admin"
	case "U"
		vUType_desc = "User"
	case "N"
		vUType_desc = "Viewer"						
end select

if vEno="" then

	set SaveEmployee= object.SaveEmployee(vUType, vJobType, vFNo, vDesig, vFName, vSName, vNickName, vAddressI, vAddressII, vTown, vPostCode, vTelNo, vMob, vFax, vEmail, vOfficeExt, vNINo, vDOB, vSalary, vUName, vpword, vS_Date, vStatus,vBonus)
	if SaveEmployee("Sucess")<>"OK" then
		Save = SaveEmployee("Sucess")
	else
		Save = SaveEmployee("Sucess")
		vNewEno = SaveEmployee("Eno")
		LogAction "Employee Added", "Login Id:" & vUName & ", Type:" & vUType_desc , ""
	end if
	set SaveEmployee =  nothing

else

	UpdateEmployee= object.UpdateEmployee(vUType, vJobType, vFNo, vDesig, vFName, vSName, vNickName, vAddressI, vAddressII, vTown, vPostCode, vTelNo, vMob, vFax, vEmail, vOfficeExt, vNINo, vDOB, vSalary, vUName, vpword, vS_Date, vStatus,vEno,vBonus)
	LogAction "Employee Edited", "Login Id:" & vUName & ", Type:" & vUType_desc, ""

end if

set object = nothing

%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
</head>

<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<div align="center">
  <center>
<table border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
  <tr>
    <td width="100%"><b><font size="3"><%if vEno = "" then%>New employee<%else%>Existing employee<%end if%><br>
      &nbsp;</font></b>
      
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
      		<%
			if LCase(UpdateEmployee) = "ok" or LCase(Save) = "ok" Then
			%>
        <tr>
          <td><b>Employee code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          <td><b><%if vEno = "" then%><%=vNewEno%><%else%><%=vEno%><%end if%></b></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td></td>
        </tr>
        <tr>
          <td colspan="2"><b><%if vEno = "" then%>Has been created successfully<%else%>Has been updated successfully<%end if%></b></td>
        </tr><%
       Else%>
       <tr>
          <td colspan="2"><b>
		   <%if err.number <> 0 then%>
		   An error occured please try again
		   <%else%>            
          This employee already exist, Please use the back button and modify the employee again
          <%end if%>            
          </b></td>
        </tr><%
       
       
       End if%> 
        
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>

    </td>
  </tr>
</table>


  </center>
</div>


</body>

</html>