<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<%
dim objBakery
dim recarray
dim retcol
dim vYear, vMonth,i
Dim TotInvAmount,TotCrdAmount


	vYear = Request("year")
	vMonth = Request("month")

	set objBakery = server.CreateObject("Bakery.Reports")
	objBakery.SetEnvironment(strconnection)
	set retcol = objBakery.Display_CurrentMonthCreditNotes(vYear,vMonth)
	recarray = retcol("CurrentMonthCredits")
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=Early_CreditNotes_" & CStr(vYear) & "_"  & CStr(vMonth) & ".xls" 
	
	dim arrayMonth
	arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
	
%>
	
      	<table border="1" cellspacing="0" cellpadding="0">
		<tr>
          <td colspan="9"><b>Early Credit Note Report For <%=vYear%>&nbsp;<%=arrayMonth(vMonth-1)%></b></td>
        </tr>
        <tr>
          <td width="110" bgcolor="#CCCCCC"><b>Credit Date&nbsp;</b></td>
		  <td width="110" bgcolor="#CCCCCC"><b>Credit Note No</b></td>
		  <td width="110" bgcolor="#CCCCCC"><b>Indirect Cus. Crd. No</b></td>
          <td width="85" bgcolor="#CCCCCC"><b>Customer No&nbsp;</b></td>
		  <td width="225" bgcolor="#CCCCCC"><b>Customer Name</b></td>
		  <td width="90" bgcolor="#CCCCCC"><b>Invoice No</b></td>
		  <td width="110" bgcolor="#CCCCCC"><b>Invoice Date</b></td>
		  <td width="100" bgcolor="#CCCCCC" align="right"><b>Invoice Amount</b></td>
		  <td width="100" bgcolor="#CCCCCC" align="right"><b>Credit Amount</b></td>
        </tr>
       <%if isarray(recarray) then%>
		<%
		TotInvAmount = 0.00
		TotCrdAmount = 0.00
		%>
		<%for i=0 to UBound(recarray,2)%>
		<tr>
          <td><%=FormatDateInDDMMYYYY(recarray(0,i))%></td>
		  <td><%=recarray(1,i)%></td>
		  <td><%=recarray(8,i)%></td>		  
          <td><%=recarray(2,i)%></td>		  
		  <td><%=recarray(3,i)%></td>
		  <td><%=recarray(4,i)%></td>
		  <td><%=FormatDateInDDMMYYYY(recarray(5,i))%></td>
		  <td align="right"><%=formatnumber(recarray(6,i),2)%></td>
		  <td align="right"><%=formatnumber(recarray(7,i),2)%></td>
         </tr>
          <%
		  
		  If recarray(6,i)<> "" then 
				TotInvAmount = TotInvAmount + formatnumber(recarray(6,i),2)
		  End if
		  
		  If recarray(7,i)<> "" then 
				TotCrdAmount = TotCrdAmount + formatnumber(recarray(7,i),2)
		  End if
		  		  		  
          %>
          <%next%>
          <tr>
       		<td colspan=7 align="right"><b>&nbsp;&nbsp;Grand Total</b></td>
          	<td align="right"><b><%=formatnumber(TotInvAmount,2)%></b></td>
          	<td align="right"><b><%=formatnumber(TotCrdAmount,2)%></b></td>
          </tr>
          <%else%>
          	<tr><td colspan="9"  bgcolor="#CCCCCC"><b>0 - No records matched...</b></td></tr>
		  <%end if%>
      </table>
    </td>
  </tr>
</table>	

