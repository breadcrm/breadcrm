<%@ Language=VBScript%>
<!--#INCLUDE FILE="includes/dsn.asp" -->
<!--#INCLUDE FILE="includes/top.inc" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<!--#INCLUDE FILE="includes/head.inc" -->
<title></title>
<LINK media=all href="calendar/calendar-system.css" type=text/css rel=stylesheet>
<SCRIPT src="calendar/calendar.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-en.js" type=text/javascript></SCRIPT>
<SCRIPT src="calendar/calendar-setup.js" type=text/javascript></SCRIPT>
<SCRIPT language=JavaScript type=text/javascript>
<!--
function initValueCal(){
	y=document.form.txtfrom.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	
	y=document.form.txtto.value
	x=y.split("/");
	d2=x[1]+"/"+x[0]+"/"+x[2]
	t1=new Date(d2);
	document.form.txtfrom.value=t.getDate() +'/'+ eval((t.getMonth())+1) +'/'+ t.getFullYear();
	document.form.txtto.value=t1.getDate() +'/'+ eval((t1.getMonth())+1) +'/'+ t1.getFullYear();
	document.form.day.value=t.getDate()
	document.form.month.value=t.getMonth()
	document.form.year.value=t.getFullYear()
	
	document.form.day1.value=t1.getDate()
	document.form.month1.value=t1.getMonth()
	document.form.year1.value=t1.getFullYear()
}
function setCal(theForm,del){
	y=eval(theForm.year.options[theForm.year.selectedIndex].value)
	m=eval(theForm.month.options[theForm.month.selectedIndex].value)
	d=eval(theForm.day.options[theForm.day.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day.value=28
		d=28
		del.value=d  +'/'+ (eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day.value=30
		d=30
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}
	
function setCal1(theForm,del){
	y=eval(theForm.year1.options[theForm.year1.selectedIndex].value)
	m=eval(theForm.month1.options[theForm.month1.selectedIndex].value)
	d=eval(theForm.day1.options[theForm.day1.selectedIndex].value)
	ret_val=0
	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) 
	 	ret_val=1;
    if (m==1 && ret_val==0 && d>=29)
	{
		theForm.day1.value=28
		d=28
		del.value=d  +'/'+(eval(m)+1)+'/'+  y;
	}
	if (m==1 && ret_val==1 && d>=30) 	 
	{	
		theForm.day1.value=29
		d=29
		del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
	}
	if ((m==3 || m==5 || m==8 || m==10) && d>30)
	{
		theForm.day1.value=30
		d=30
		del.value= d  +'/'+(eval(m)+1)+'/'+ y;
	}
	del.value=d  +'/'+ (eval(m)+1)+'/'+ y;
}

function setCalCombo(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day.value=t.getDate()
	theForm.month.value=t.getMonth()
	theForm.year.value=t.getFullYear()
}
function setCalCombo1(theForm,del){
	y=del.value
	x=y.split("/");
	d1=x[1]+"/"+x[0]+"/"+x[2]
	t=new Date(d1);
	theForm.day1.value=t.getDate()
	theForm.month1.value=t.getMonth()
	theForm.year1.value=t.getFullYear()
}

function DoughTypeChanged(frm)
{
	var selIdx = frm.selectedIndex;
	var vDoughType = frm[selIdx].text;
	if(vDoughType != 'Select')
	{
		if (vDoughType != 'All')
		{
			if (document.getElementById("lblDoughType")!=null)
				document.getElementById('lblDoughType').innerHTML = vDoughType;
		}
	}
	else
		if (document.getElementById("lblDoughType")!=null)
			lblDoughType.value = "";
}

function CheckSearch(frm)
{
	if (frm.DoughType.value=="")
	{
		alert("Please select a Dough Name");
		frm.DoughType.focus();
		return  false;
	}
	return  true;
}
//-->
</SCRIPT>
</head>
<body topmargin="0" leftmargin="0" bgcolor="#FFFFFF" text="#000000" style="font-family: Verdana; font-size: 8pt" onLoad="initValueCal();DoughTypeChanged(document.form.DoughType)">
<!--#INCLUDE FILE="nav.inc" -->
&nbsp;&nbsp;&nbsp;<br>
&nbsp;&nbsp;&nbsp;
<%
dim objBakery
dim retcol
dim retsecondmixcol
dim fromdt, todt , i
Dim TotQuantity,TotTurnover
dim arrProductCategory
dim arrSecondMixProductCategory
dim vDoughType

fromdt = Request.form("txtfrom")
strlblDoughType=request("lblDoughType")
todt = Request.Form("txtto")
strDoughType = Request.Form("DoughType")
vDoughType = Request.QueryString("dtype")

If vDoughType = "" Then
	vDoughType = 1
End If

if (strDoughType="") then
	strDoughType="0"
end if

if fromdt ="" then
   fromdt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
   todt=Day(date()) & "/" & Month(date()) & "/" & Year(date())
end if

Set object = Server.CreateObject("bakery.general")
object.SetEnvironment(strconnection)
set detail = object.DisplayDoughNameTwoStageMixing(vDoughType)
vDoughTypearray =  detail("DoughName")

set detail = Nothing
%>

<table align="center" border="0" width="90%" cellspacing="0" cellpadding="0" style="font-family: Verdana; font-size: 8pt">
 <tr>
    <td width="100%"><b><font size="3">Dough <%if vDoughType = 1 then response.Write("Direct") else response.Write("48 Hours") end if%> Mixing Report - Scenario 2<br>
      &nbsp;</font></b>
	  <form method="post" action="DoughMixingReportScenario2.asp?dtype=<%=vDoughType%>" name="form" onSubmit="return CheckSearch(this)">
      <table border="0" cellspacing="0" cellpadding="2" style="font-family: Verdana; font-size: 8pt">
        <tr height="35">
          
          <td colspan="4"><b>Dough Name:&nbsp;
			<select size="1" name="DoughType" style="font-family: Verdana; font-size: 8pt" <%If strDoughType <> "-1" Then response.Write("onChange=""DoughTypeChanged(this)""")%>>
			<option value = "">Select</option>
			<option value = "-1" <%if strDoughType = "-1" then response.write "Selected" %> >All</option>
			<%
			if IsArray(vDoughTypearray) Then  	
				for i = 0 to ubound(vDoughTypearray,2)%>  				
			 <option value="<%=vDoughTypearray(0,i)%>" <%if strDoughType<>"" then%><%if cint(strDoughType)= vDoughTypearray(0,i) then response.write " Selected"%><%end if%>><%=vDoughTypearray(1,i)%></option>
			  <%
			   next                 
			End if
			%>   
		  </select>
          </b></td>
          <td></td>
        </tr>
		
		
        <tr>
          <td><b>From:</b></td>
          <td height="18" width="350">
                    <%
						dim arrayMonth
						arrayMonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
				    %>
						<select name="day" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=1 to 31%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<select name="month" onChange="setCal(this.form,document.form.txtfrom)">
						   <%for i=0 to 11%>
							<option value="<%=i%>"><%=arrayMonth(i)%></option>
						  <%next%>
						</select>
						
						<select name="year" onChange="setCal(this.form,document.form.txtfrom)">
						  <%for i=2000 to Year(Date)+1%>
							<option value="<%=i%>"><%=i%></option>
						  <%next%>
						</select>
						<INPUT class=Datetxt id=txtfrom onFocus="blur();" size="12"  name="txtfrom" value="<%=fromdt%>" onChange="setCalCombo(this.form,document.form.txtfrom)">
						<IMG id=f_trigger_frfrom onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
					
					</td>
					<td><b>To:</b></td>
					  <td height="18" width="350">
                    <select name="day1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=1 to 31%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<select name="month1" onChange="setCal1(this.form,document.form.txtto)">
					   <%for i=0 to 11%>
						<option value="<%=i%>"><%=arrayMonth(i)%></option>
					  <%next%>
					</select>
					
					<select name="year1" onChange="setCal1(this.form,document.form.txtto)">
					  <%for i=2000 to Year(Date)+1%>
						<option value="<%=i%>"><%=i%></option>
					  <%next%>
					</select>
					<INPUT class=Datetxt id=txtto onFocus="blur();" name="txtto" size="12" value="<%=todt%>" onChange="setCalCombo1(this.form,document.form.txtto)">
					<IMG id=f_trigger_frto onMouseOver="this.style.background='red';" title="Date selector" style="CURSOR: pointer;" onMouseOut="this.style.background=''" src="calendar/img1.gif">
				</td> <td>
            <input type="submit" value="Search" name="Search"  style="font-family: Verdana; font-size: 8pt"></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
	  </form>
	  <SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtfrom",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frfrom",
				singleClick    :    true
			});
		</SCRIPT>
		<SCRIPT type=text/javascript>
			Calendar.setup({
				inputField     :    "txtto",
				ifFormat       :    "%d/%m/%Y",
				button         :    "f_trigger_frto",
				singleClick    :    true
			});
		</SCRIPT>
<%

set objBakery = server.CreateObject("Bakery.reports")
objBakery.SetEnvironment(strconnection)
dim NoOfLoops

If strDoughType = "-1" Then
	NoOfLoops = UBound(vDoughTypearray,2)
Else
	NoOfLoops = 0
End If

IsDataAvailbale = 0 'default value is 0
IsSecondMixDataAvailbale = 0 'default value is 0

for d=0 to NoOfLoops

dim arrIName
dim arrSecondMixIName
dim recarray
dim recsecondmixarray
dim IsDataAvailbale
dim IsSecondMixDataAvailbale

if isdate(fromdt) and isdate(todt) then
	'get the data for the first mix
	If strDoughType = "-1" Then
		set retcol = objBakery.DoughMixingReportTwoStageMixing(cint(vDoughTypearray(0,d)),fromdt,todt,0)
    	recarray = retcol("DoughMixingReport")
	Else
		set retcol = objBakery.DoughMixingReportTwoStageMixing(cint(strDoughType),fromdt,todt,0)
    	recarray = retcol("DoughMixingReport")
	End If
	
	'get the data for the secod mix
	If strDoughType = "-1" Then
		set retsecondmixcol = objBakery.DoughMixingReportTwoStageMixing(cint(vDoughTypearray(0,d)),fromdt,todt,1)
    	recsecondmixarray = retsecondmixcol("DoughMixingReport")
	Else
		set retsecondmixcol = objBakery.DoughMixingReportTwoStageMixing(cint(strDoughType),fromdt,todt,1)
    	recsecondmixarray = retsecondmixcol("DoughMixingReport")
	End If
end if

if (Request("Search")<>"") then
	'first mix
	if isarray(recarray) then
		'IName -Ingredient Name
		set objIName = objBakery.GetFilterData(recarray,3,arrIName)
		arrIName = objIName("FilterData")
		
		'Production Category
		set objProductCategory = objBakery.GetFilterData(recarray,2,arrProductCategory)
		arrProductCategory = objProductCategory("FilterData")
		
		'OK, it says that there are data availble to show
		IsDataAvailbale = 1
	Else
		arrIName = "Fail"
		arrProductCategory = "Fail"
	end if
	
	'second mix
	if isarray(recsecondmixarray) then
		'IName - Ingredent Name
		set objSecondMixIName = objBakery.GetFilterData(recsecondmixarray,3,arrSecondMixIName)
		arrSecondMixIName = objSecondMixIName("FilterData")
		
		'Production Category
		set objSecondMixProductCategory = objBakery.GetFilterData(recsecondmixarray,2,arrSecondMixProductCategory)
		arrSecondMixProductCategory = objSecondMixProductCategory("FilterData")
		
		'OK, it says that there are data availble to show
		IsSecondMixDataAvailbale = 1
	else
		arrSecondMixIName = "Fail"
		arrSecondMixProductCategory = "Fail"
	end if
end if

set object = Nothing
%>

<% if (Request("Search")<>"") then%>
<%if isarray(recarray) then%>
<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<%
	if strDoughType = "-1" then
		If isarray(arrProductCategory) then
%>
 <form method="post"name="frmReport">
<tr>
  <td width="100%"  colspan='<%=UBound(arrProductCategory) %>' height="40"><b>Dough <%if vDoughType = 1 then response.Write("Direct") else response.Write("48 Hours") end if%> Mixing Report - Scenario 2 From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; Dough Name - <label id="lblDoughType"><%=vDoughTypearray(1,d)%></label></b></td>
</tr>
</form>
<%
		End If
	Else
%>
 <form method="post"name="frmReport">
<tr>
  <td width="100%"  colspan='<%=UBound(arrProductCategory) %>' height="40"><b>Dough <%if vDoughType = 1 then response.Write("Direct") else response.Write("48 Hours") end if%> Mixing Report - Scenario 2 From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
</tr>
</form>
<%
	End If ' End for [strDoughType = "-1"] check
	
    Set objGeneral = Server.CreateObject("bakery.general")
	objGeneral.SetEnvironment(strconnection)
    dim objIndex
    dim n, m, p
    dim weight, arrProductCategoryTotal (50), arrProductCategoryTotal1 (50)
    
    if not isempty(arrIName) then
        objGeneral.BubbleSort(arrIName)
    end if
        
    if not isempty(arrProductCategory) then        
        objGeneral.BubbleSort(arrProductCategory)
    end if
%>

<tr>
<td colspan='<%=UBound(arrProductCategory) %>'>
	<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">
	<tr bgcolor="#CCCCCC" style="font-weight:bold" height="22">
		<td colspan="<%=ubound(arrIName) + 3%>">First mix</td>
	</tr>
	<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
    <%if isarray(arrIName) then%>
		<td width="170" bgcolor="#CCCCCC">&nbsp;</td>
	    <%for m=0 to UBound(arrIName)
			arrProductCategoryTotal(m)=0
			arrProductCategoryTotal1(m)=0
		%>
	        <td align="right" bgcolor="#CCCCCC"><%=arrIName(m)%></td>	
	    <%next%>
			<td align="right" bgcolor="#CCCCCC" width="120">Total Weight (Kg)</td>	
    <%
	else
		If strDoughType <> "-1" Then
	%>
		<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
	<%
		End If
	end if
	%>
	</tr>

<%
'This is written by Ramanan on 4th Feb 2010

If isarray(arrProductCategory) Then
	strGrandTotalWeight1 = 0
	for n1=0 to UBound(arrProductCategory)
		strTotalWeight1 = 0
		
		for p1=0 to UBound(arrIName)
			weight1 = 0
			for c1=0 to UBound(recarray,2)
				if(recarray(3,c1)=arrIName(p1) and recarray(2,c1) = arrProductCategory(n1)) then
				   weight1 = recarray(4,c1)
				end if
			next
			strTotalWeight1 = cdbl(strTotalWeight1)+ cdbl(weight1) 
			arrProductCategoryTotal1(p1) = cdbl(arrProductCategoryTotal1(p1))+cdbl(weight1)
		next
	strGrandTotalWeight1 = strGrandTotalWeight1 + strTotalWeight1
	next
End If

'End of bit writtedn by Ramanan
%>

<%if isarray(arrProductCategory) then%>

<tr bgcolor="#CCCCCC" style="font-weight:bold">
	<td>Total (Kg)</td>
	<%for m1=0 to UBound(arrIName)%>
		<td align="right"><%=formatnumber(arrProductCategoryTotal1(m1),3)%></td>
	<%next%>
	<td align="right"><%=formatnumber(strGrandTotalWeight1,3)%></td>
</tr>

	<%
	strGrandTotalWeight=0
	for n=0 to UBound(arrProductCategory)
		if (n mod 2 =0) then
			strBgColour="#FFFFFF"
		else
			strBgColour="#F3F3F3"
		end if
	%>
	    <tr bgcolor="<%=strBgColour%>">
	        <td><%=arrProductCategory(n)%></td>
    	    
	        <%
			strTotalWeight=0
			
	        for p=0 to UBound(arrIName)
	            weight=0
    	        
	            for c=0 to UBound(recarray,2)
	                if(recarray(3,c)=arrIName(p) and recarray(2,c) = arrProductCategory(n)) then
	                   weight = recarray(4,c)
	                end if	            	
				next	           
    	            
	        %>
	            <td align="right"><%=formatnumber(weight,3)%></td>
	        <%			
			strTotalWeight=cdbl(strTotalWeight)+ cdbl(weight) 
			arrProductCategoryTotal(p)=cdbl(arrProductCategoryTotal(p))+cdbl(weight)
	        next
			
	        %>	    
    	   	<td align="right" style="font-weight:bold"><%=formatnumber(strTotalWeight,3)%></td> 
	    </tr>
		
	<%
	
	strGrandTotalWeight = strGrandTotalWeight + strTotalWeight
	next
	%>
	<!--
	'Total at the botton were commented by Ramanan on 4th Feb 2010
	<tr bgcolor="#CCCCCC" style="font-weight:bold">
		<td>Total (Kg)</td>
		<%for m=0 to UBound(arrIName)%>
			<td align="right"><%=formatnumber(arrProductCategoryTotal(m),4)%></td>
		<%next%>
		<td align="right"><%=formatnumber(strGrandTotalWeight,4)%></td>
	</tr>
	-->
<%end if%>
</table>
</td>
</tr>
</table>
<%Else
	If strDoughType <> "-1" Then
%>
	<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
		<form method="post"name="frmReport">
		<tr>
  			<td width="100%"  colspan='' height="40"><b>Dough <%if vDoughType = 1 then response.Write("Direct") else response.Write("48 Hours") end if%> Mixing Report - Scenario 2 From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; Dough Name - <label id="lblDoughType"><%=strlblDoughType%></label></b></td>
		</tr>
		</form>
		<tr>
			<td colspan=''>
				<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
					<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22">
						<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found for first mix.</font></p></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%
	End IF
%>
<%end if%>

<%
	'This is for second mix
	if isarray(recsecondmixarray) then
%>
<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
<%
    Set objSecondMixGeneral = Server.CreateObject("bakery.general")
	objSecondMixGeneral.SetEnvironment(strconnection)
    'dim objIndex
    'dim n, m, p
    dim secondmixweight, arrSecondMixProductCategoryTotal (50), arrSecondMixProductCategoryTotal1 (50)
    
    if not isempty(arrSecondMixIName) then
        objSecondMixGeneral.BubbleSort(arrSecondMixIName)
    end if
        
    if not isempty(arrSecondMixProductCategory) then        
        objSecondMixGeneral.BubbleSort(arrSecondMixProductCategory)
    end if
%>

<tr>
<td colspan='<%=UBound(arrSecondMixProductCategory) %>'>
	<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
	<tr bgcolor="#CCCCCC" style="font-weight:bold" height="22"> 
		<td colspan="<%=ubound(arrSecondMixIName) + 3%>">Second mix</td>
	</tr>
	<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22"> 
    <%if isarray(arrSecondMixIName) then%>
		<td width="170" bgcolor="#CCCCCC">&nbsp;</td>
	    <%for m=0 to UBound(arrSecondMixIName)
			arrSecondMixProductCategoryTotal(m)=0
			arrSecondMixProductCategoryTotal1(m)=0
		%>
	        <td align="right" bgcolor="#CCCCCC"><%=arrSecondMixIName(m)%></td>	
	    <%next%>
			<td align="right" bgcolor="#CCCCCC" width="120">Total Weight (Kg)</td>	
    <%
	else
		If strDoughType <> "-1" Then
	%>
		<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
	<%
		End If
	end if
	%>
	</tr>

<%
'This is written by Ramanan on 4th Feb 2010

If isarray(arrSecondMixProductCategory) Then
	strSecondMixGrandTotalWeight1 = 0
	for n1=0 to UBound(arrSecondMixProductCategory)
		strSecondMixTotalWeight1 = 0
		
		for p1=0 to UBound(arrSecondMixIName)
			secondmixweight1 = 0
			for c1=0 to UBound(recsecondmixarray,2)
				if(recsecondmixarray(3,c1)=arrSecondMixIName(p1) and recsecondmixarray(2,c1) = arrSecondMixProductCategory(n1)) then
				   secondmixweight1 = recsecondmixarray(4,c1)
				end if
			next
			strSecondMixTotalWeight1 = cdbl(strSecondMixTotalWeight1)+ cdbl(secondmixweight1) 
			arrSecondMixProductCategoryTotal1(p1) = cdbl(arrSecondMixProductCategoryTotal1(p1))+cdbl(secondmixweight1)
		next
	strSecondMixGrandTotalWeight1 = strSecondMixGrandTotalWeight1 + strSecondMixTotalWeight1
	next
End If

'End of bit writtedn by Ramanan
%>

<%if isarray(arrSecondMixProductCategory) then%>

<tr bgcolor="#CCCCCC" style="font-weight:bold">
	<td>Total (Kg)</td>
	<%for m1=0 to UBound(arrSecondMixIName)%>
		<td align="right"><%=formatnumber(arrSecondMixProductCategoryTotal1(m1),3)%></td>
	<%next%>
	<td align="right"><%=formatnumber(strSecondMixGrandTotalWeight1,3)%></td>
</tr>

	<%
	strSecondMixGrandTotalWeight=0
	for n=0 to UBound(arrSecondMixProductCategory)
		if (n mod 2 =0) then
			strBgColour="#FFFFFF"
		else
			strBgColour="#F3F3F3"
		end if
	%>
	    <tr bgcolor="<%=strBgColour%>">
	        <td><%=arrSecondMixProductCategory(n)%></td>
    	    
	        <%
			strSecondMixTotalWeight=0
			
	        for p=0 to UBound(arrSecondMixIName)
	            secondmixweight=0
    	        
	            for c=0 to UBound(recsecondmixarray,2)
	                if(recsecondmixarray(3,c)=arrSecondMixIName(p) and recsecondmixarray(2,c) = arrSecondMixProductCategory(n)) then
	                   secondmixweight = recsecondmixarray(4,c)
	                end if	            	
				next	           
    	            
	        %>
	            <td align="right"><%=formatnumber(secondmixweight,3)%></td>
	        <%			
			strSecondMixTotalWeight=cdbl(strSecondMixTotalWeight)+ cdbl(secondmixweight) 
			arrSecondMixProductCategoryTotal(p)=cdbl(arrSecondMixProductCategoryTotal(p))+cdbl(secondmixweight)
	        next
			
	        %>	    
    	   	<td align="right" style="font-weight:bold"><%=formatnumber(strSecondMixTotalWeight,3)%></td> 
	    </tr>
		
	<%
	
	strSecondMixGrandTotalWeight = strSecondMixGrandTotalWeight + strSecondMixTotalWeight
	next
	%>
	<!--
	'Total at the botton were commented by Ramanan on 4th Feb 2010
	<tr bgcolor="#CCCCCC" style="font-weight:bold">
		<td>Total (Kg)</td>
		<%for m=0 to UBound(arrSecondMixIName)%>
			<td align="right"><%=formatnumber(arrSecondMixProductCategoryTotal(m),4)%></td>
		<%next%>
		<td align="right"><%=formatnumber(strSecondMixGrandTotalWeight,4)%></td>
	</tr>
	-->
<%end if%>
</table>
</td>
</tr>
</table>
<%else
	if strDoughType <> "-1" then
%>
	<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
		<tr>
			<td colspan=''>
				<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
					<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22">
						<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found for second mix.</font></p></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%
	end if
%>
<%end if' This is the end for second mix report%>

<%end if ' This is the end for Request("Search")%>
<%
if IsArray(recarray) then erase recarray
if IsArray(arrIName) then erase arrIName
if IsArray(recsecondmixarray) then erase recsecondmixarray
if IsArray(arrSecondMixIName) then erase arrSecondMixIName
next 'This is the end of the loop for different dough types

If strDoughType = "-1" Then
	If  (IsDataAvailbale = 0) and (IsSecondMixDataAvailbale = 0) Then
%>
<table border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="0" cellpadding="2">
	<form method="post"name="frmReport">
	<tr>
		<td width="100%"  colspan='' height="40"><b>Dough <%if vDoughType = 1 then response.Write("Direct") else response.Write("48 Hours") end if%> Mixing Report - Scenario 2 From &lt;<%=fromdt%>&gt; To &lt;<%=todt%>&gt; Dough Name - <label id="lblDoughType">All</label></b></td>
	</tr>
	</form>
	<tr>
		<td colspan=''>
			<table bgcolor="#999999" border="0" width="100%" style="font-family: Verdana; font-size: 8pt" cellspacing="1" cellpadding="3">  
				<tr bgcolor="#FFFFFF" style="font-weight:bold" height="22">
					<td bgcolor="#FFFFFF"><p align="center"><font color="#FF0000">There are no records found.</font></p></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
	End If
End If
%>
</td>
</tr>
</table>
<br><br>
</body>
</html>